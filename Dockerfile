FROM cradlepoint-engops-docker-all.jfrog.io/nctf-base:19
MAINTAINER Matthew Norman <mnorman@cradlepoint.com>
USER root
COPY . /tmp/nctf
RUN pip3 install --process-dependency-links /tmp/nctf
RUN useradd -ms /bin/bash -d /home/nctf-user nctf-user
USER nctf-user
RUN mkdir /home/nctf-user/nctf
WORKDIR /home/nctf-user/nctf
