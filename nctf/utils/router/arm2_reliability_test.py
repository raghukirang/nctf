import logging

from nctf.test_lib.core.arm2.interface import ARMInterface
from nctf.utils.router.apikey import API_KEY
"""
This is a network/REST API reliability test. 
It can be used to see if we're losing connections to routers 
over remote admin through the corporate network.
"""

if __name__ == "__main__":
    logging.basicConfig(level='DEBUG')
    urllib3_logger = logging.getLogger('urllib3').setLevel(999)

    exceptions = []
    arm2 = ARMInterface(host='http://10.2.113.233', port=80, api_path='/api/v3/', api_key=API_KEY)
    router = arm2.get_router(physical_only=True)
    print(router)

    try:
        for i in range(100000):
            try:
                router.reboot()
                router.wait_for_api_reachable()
                fw_info = router.api.status_fw_info.get()
            except Exception as e:
                print('****************Unhandled Exception seen!****************')
                exceptions.append(e)
            finally:
                print(f"{len(exceptions)} exceptions out of {i+1} loops so far for {router}")

    except KeyboardInterrupt:
        pass
    finally:
        # Print any exceptions that occurred.
        for e in exceptions:
            print(e)

        # Teardown arm2 fixture.
        print('Tearing down ARMv2 fixture...')
        arm2.teardown()
