import copy
import logging

from nctf.test_lib.core.arm2.interface import ARMInterface
from nctf.utils.router.apikey import API_KEY
"""
This is a network/REST API reliability test. 
It can be used to see if we're losing connections to routers 
over remote admin through the corporate network.
"""

if __name__ == "__main__":
    logging.basicConfig(level='DEBUG')
    urllib3_logger = logging.getLogger('urllib3').setLevel(999)

    exceptions = []
    arm2 = ARMInterface(host='http://10.2.113.233', port=80, api_path='/api/v3/', api_key=API_KEY)
    router = arm2.get_wifi_router()
    api = router.get_rest_client()
    print(router)

    try:
        for i in range(10000):
            try:
                ssids = [
                    "this is a test", " this is a test", "this is a test ", "                                ",
                    "foo\0bar\0baz", "(╯°□°）╯︵ ┻━┻", ":hankey:", "`logger hello from evil ssid`",
                    "$(logger hello from evil ssid)", "René Decartes", "Académie française",
                    "01234567890123456789012345678901", "<script>alert('hi');</script>", "; DROP TABLE passwords;", " or 1=1",
                    chr(27) + "["
                ]

                wifi_radios = api.config_wlan.get(name="radio")
                for ssid in ssids:
                    for radio_index, wifi_radio in enumerate(wifi_radios["data"]):
                        if wifi_radio['bss']:
                            original = copy.deepcopy(wifi_radio)
                            wifi_radio["enabled"] = True
                            wifi_radio["bss"][0]["enabled"] = True
                            enabled = api.config_wlan.update(name="radio/{}".format(radio_index), update=wifi_radio)
                            original_ssid = copy.deepcopy(wifi_radio["bss"][0]["ssid"])
                            new_ssid = api.config_wlan.update(name="radio/{}/bss/0".format(radio_index), update={"ssid": ssid})
                            validate = api.config_wlan.get(name="radio/{}/bss/0".format(radio_index))["data"]
                            reset_ssid = api.config_wlan.update(
                                name="radio/{}/bss/0".format(radio_index), update={"ssid": original_ssid})
                            default = api.config_wlan.update(name="radio/{}".format(radio_index), update=original)
            except Exception as e:
                print('****************Unhandled Exception seen!****************')
                exceptions.append(e)
            finally:
                print(f"{len(exceptions)} exceptions out of {i+1} loops so far for {router}")

    except KeyboardInterrupt:
        pass
    finally:
        # Print any exceptions that occurred.
        for e in exceptions:
            print(e)

        # Teardown arm2 fixture.
        print('Tearing down ARMv2 fixture...')
        arm2.teardown()
