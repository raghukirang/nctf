# Provides the ability to create an account on either a QA or Ephemeral stack with the requested entitlements.
import argparse
import io
import sys

from attrdict import AttrDict
import yaml

from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.test_lib.core.email.fixture import EmailFixture
from nctf.test_lib.core.email.fixture import EmailService
from nctf.test_lib.netcloud.fixtures.netcloud_account.fixture import ActivationMethod
from nctf.test_lib.netcloud.fixtures.netcloud_account.fixture import CreationMethod
from nctf.test_lib.netcloud.fixtures.netcloud_account.fixture import NetCloudAccountFixture
from nctf.test_lib.core.config.fixture import ConfigFixtureFactory
import account_add_ecm_features


def create_qa_account(entitlements, ncm_target):
    # retrieve correct run/target config files.
    run_config_path = "./configs/{}-run.yaml".format(ncm_target)
    target_config_path = "./configs/{}-target.yaml".format(ncm_target)

    cff = ConfigFixtureFactory()
    config = cff.create(
        run_config=run_config_path,
        target_config=target_config_path,
        version=1)

    mailinator_protocol = config.run.fixtures.email.api.protocol
    mailinator_host = config.run.fixtures.email.api.hostname
    mailinator_api_root = "{}://{}".format(mailinator_protocol, mailinator_host)
    mailinator_token = config.run.fixtures.email.api.token
    mailinator_timeout = config.run.fixtures.email.api.timeout
    email_fixture = EmailFixture(EmailService.MAILINATOR, mailinator_api_root, mailinator_token, mailinator_timeout)
    activate = ActivationMethod.API
    creation_method = CreationMethod.SALESFORCE

    ncaf = NetCloudAccountFixture(
        creation_method=creation_method,
        email_fixture=email_fixture,
        config_fixture=config,
        use_activation_email=True,
        ncm_api_protocol=config.target.services.ncm.protocol,
        ncm_api_hostname=config.target.services.ncm.hostname,
        ncm_api_port=config.target.services.ncm.port,
        accounts_api_protocol=config.target.services.accounts.protocol,
        accounts_api_hostname=config.target.services.accounts.hostname,
        accounts_api_port=config.target.services.accounts.port,
        timeout=30.0)

    account_admin = ncaf.create(
        activate=activate, accept_tos="api", entitlements=entitlements, sf_account_name_prefix="EXPLORATORY_")
    login_url = "{}://{}".format(config.target.services.accounts.protocol, config.target.services.ncm.hostname)
    print("Stack: {}\nUsername: {}\nPassword: {}".format(login_url, account_admin.username, account_admin.password))

    return account_admin


def create_kube_account(ncm_target):
    with io.open('kube_stack_default.yaml', 'r') as stream:
        base_config = AttrDict(yaml.load(stream))
    assert base_config, "Failed to load the default kube_stack config."

    creation_method = CreationMethod.ACCOUNTS
    accounts_privileged_username = "localServiceAccount@cradlepoint.com"
    accounts_privileged_password = "Password1!"

    mailhog_host = str(base_config.fixtures.email.api.hostname).format(ncm_target)
    mailhog_protocol = base_config.fixtures.email.api.protocol
    mailhog_api_root = "{}://{}".format(mailhog_protocol, mailhog_host)
    mailhog_timeout = base_config.fixtures.email.api.timeout
    email_fixture = EmailFixture(service=EmailService.MAILHOG, api_root=mailhog_api_root, timeout=mailhog_timeout)
    ncm_api_protocol = base_config.services.ncm.protocol
    ncm_api_hostname = str(base_config.services.ncm.hostname).format(ncm_target)
    ncm_api_port = base_config.services.ncm.port
    accounts_api_protocol = base_config.services.accounts.protocol
    accounts_api_hostname = str(base_config.services.accounts.hostname).format(ncm_target)
    accounts_api_port = base_config.services.accounts.port
    activate = ActivationMethod.API
    use_activation_email = True

    ncaf = NetCloudAccountFixture(
        creation_method=creation_method,
        email_fixture=email_fixture,
        use_activation_email=use_activation_email,
        ncm_api_protocol=ncm_api_protocol,
        ncm_api_hostname=ncm_api_hostname,
        ncm_api_port=ncm_api_port,
        accounts_api_protocol=accounts_api_protocol,
        accounts_api_hostname=accounts_api_hostname,
        accounts_api_port=accounts_api_port,
        accounts_privileged_username=accounts_privileged_username,
        accounts_privileged_password=accounts_privileged_password,
        timeout=30.0)

    account_admin = ncaf.create(activate=activate, accept_tos="api")
    login_url = "{}://{}".format(accounts_api_protocol, accounts_api_hostname)
    print("Stack: {}\nUsername: {}\nPassword: {}".format(login_url, account_admin.username, account_admin.password))
    return account_admin


def create_bsf_mock_kube_account(entitlements, ncm_target):
    run_config_path = "./configs/kube-run.yaml"
    target_config_path = "./configs/kube-target.yaml"

    cff = ConfigFixtureFactory()
    config = cff.create(
        run_config=run_config_path,
        target_config=target_config_path,
        version=1)

    # Update Services Hostnames:
    config.target.services.bsf.sfconnector.hostname = str(config.target.services.bsf.sfconnector.hostname).format(
        ncm_target)
    config.target.services.bsf.entitlementService.hostname = str(
        config.target.services.bsf.entitlementService.hostname).format(ncm_target)
    config.target.services.bsf.licenseService.hostname = str(config.target.services.bsf.licenseService.hostname).format(
        ncm_target)
    config.target.services.bsf.productSubscriptionService.hostname = str(
        config.target.services.bsf.productSubscriptionService.hostname).format(ncm_target)
    config.target.services.bsf.provisioningService.hostname = str(
        config.target.services.bsf.provisioningService.hostname).format(ncm_target)
    config.target.services.bsf.orderService.hostname = str(config.target.services.bsf.orderService.hostname).format(
        ncm_target)
    config.target.services.accounts.hostname = str(config.target.services.accounts.hostname).format(ncm_target)
    config.target.services.ncm.hostname = str(config.target.services.ncm.hostname).format(ncm_target)
    config.target.services.stream.hostname = str().format(ncm_target)
    config.target.services.als.hostname = str().format(ncm_target)
    config.target.services.nydus.hostname = str().format(ncm_target)

    creation_method = CreationMethod.SALESFORCE_MOCK
    mailhog_host = str(config.run.fixtures.email.api.hostname).format(ncm_target)
    mailhog_protocol = config.run.fixtures.email.api.protocol
    mailhog_api_root = "{}://{}".format(mailhog_protocol, mailhog_host)
    mailhog_timeout = config.run.fixtures.email.api.timeout

    email_fixture = EmailFixture(service=EmailService.MAILHOG, api_root=mailhog_api_root, timeout=mailhog_timeout)
    activate = ActivationMethod.API

    ncaf = NetCloudAccountFixture(
        creation_method=creation_method,
        email_fixture=email_fixture,
        config_fixture=config,
        use_activation_email=True,
        ncm_api_protocol=config.target.services.ncm.protocol,
        ncm_api_hostname=str(config.target.services.ncm.hostname).format(args.ncm_target),
        ncm_api_port=config.target.services.ncm.port,
        accounts_api_protocol=config.target.services.accounts.protocol,
        accounts_api_hostname=str(config.target.services.accounts.hostname).format(args.ncm_target),
        accounts_api_port=config.target.services.accounts.port,
        timeout=30.0)

    account_admin = ncaf.create(
        activate=activate, accept_tos="api", entitlements=entitlements, sf_account_name_prefix="EXPLORATORY_")
    login_url = "{}://{}".format(config.target.services.accounts.protocol, config.target.services.ncm.hostname)
    print("Stack: {}\nUsername: {}\nPassword: {}".format(login_url, account_admin.username, account_admin.password))

    return account_admin


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ncm_target", help="e.g. \'qa1\' or kube: \'username-temp-340\'\n", required=True)
    ent_help = "The entitlement products you wish your account to have, in a non-spaced comma separated string.\n" \
               "e.g. NETCLOUD_BRANCH_ROUTERS_1YR,NETCLOUD_BRANCH_ROUTERS_ADVANCED_UPGRADE_1YR" \
               "You can see the supported available products here:\n" \
               "https://engops-prod-gitlab-v1.private.aws.cradlepointecm.com/Infrastructure/nctf/" \
               "blob/master/nctf/libs/salesforce/common_lib.py\n" \
               "NOTE: Available products will vary from one stack to another."
    parser.add_argument("--entitlements", help=ent_help)
    ecm_feature_help = "The ECM features that you wish to be added to the account." \
                       "You can see the supported available features in the enum \"BSFBlacklistedFeatureUUIDs\", here:\n" \
                       "https://engops-prod-gitlab-v1.private.aws.cradlepointecm.com/Infrastructure/nctf/" \
                       "blob/master/nctf/libs/salesforce/common_lib.py\n" \
                       "Note: You can request either ALL or NONE rather than specifying features."
    parser.add_argument("--ecm_features", default="all", help=ecm_feature_help)
    parser.add_argument(
        "--sf_user", help="SF Username for the requested stack. This is not necessary for Kube or QA "
        "stacks in Jenkins.")
    parser.add_argument(
        "--sf_password",
        help="SF Password for the requested stack. This is not necessary for Kube or "
        "QA stacks in Jenkins.")
    parser.add_argument(
        "--sf_token", help="SF API Token for the requested stack. This is not necessary for Kube or QA "
        "stacks in Jenkins.")
    args = parser.parse_args()
    ncm_target = str.lower(args.ncm_target)
    ecm_features = args.ecm_features

    # Verify requested entitlements
    ents = args.entitlements
    entitlements = str.split(ents, ',')
    for e in entitlements:
        if e not in ProductCodes.__members__:
            sys.exit("Invalid Product Code: {}".format(e))

    # Target the correct call based on what was requested:
    if 'qa' in ncm_target:
        try:
            admin = create_qa_account(entitlements=entitlements, ncm_target=ncm_target)
        except Exception as e:
            print("Verify the requested entitlements are available on the requested stack.")
            raise e
    elif "-temp-" in args.ncm_target:
        try:
            admin = create_bsf_mock_kube_account(entitlements=entitlements, ncm_target=ncm_target)
        except Exception as e:
            print("Failed to entitle the account on the requested Kube stack.")
            raise e
    else:
        sys.exit("Unrecognized Account Creation Method.")

    # Add any requested ECM Features
    if ecm_features:
        account_add_ecm_features.entitle_account(
            ncm_target=ncm_target,
            ecm_only_features=ecm_features,
            admin_username=admin.username,
            admin_password=admin.password
        )
