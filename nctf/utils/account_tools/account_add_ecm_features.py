import argparse
import os
import sys

import requests
from attrdict import AttrDict

from nctf.libs.salesforce.common_lib import ECMOnlyFeatureUUIDs
from nctf.libs.salesforce.common_lib import BSFBlacklistedFeatureUUIDs
from nctf.libs.salesforce.common_lib import ProdECMRootAccountFeatureUUIDs
from nctf.libs.salesforce.common_lib import FeatureUUIDs
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture
from nctf.test_lib.core.config.fixture import ConfigFixtureFactory


def retrieve_admin_session(base_config: AttrDict) -> requests.Session:
    ncrf = NetCloudRESTFixture(
        accounts_protocol=base_config.target.services.accounts.protocol,
        accounts_hostname=base_config.target.services.accounts.hostname,
        accounts_port=base_config.target.services.accounts.port,
        ncm_protocol=base_config.target.services.ncm.protocol,
        ncm_hostname=base_config.target.services.ncm.hostname,
        ncm_port=base_config.target.services.ncm.port)

    client = ncrf.get_client()
    return client


def retrieve_ecm_root_admin_session(base_config: AttrDict, ncm_target: str) -> requests.Session:
    ncrf = NetCloudRESTFixture(
        accounts_protocol=base_config.target.services.accounts.protocol,
        accounts_hostname=base_config.target.services.accounts.hostname,
        accounts_port=base_config.target.services.accounts.port,
        ncm_protocol=base_config.target.services.ncm.protocol,
        ncm_hostname=base_config.target.services.ncm.hostname,
        ncm_port=base_config.target.services.ncm.port)

    if 'qa' in ncm_target:
        ecm_root_username = base_config.target.predefinedAuth.ncmRoot.username
        ecm_root_password = base_config.target.predefinedAuth.ncmRoot.password
        client = ncrf.get_client()
        client.session.headers.update(
            {'Authorization': 'SecurityToken {}:{}'.format(ecm_root_username, ecm_root_password)})
    else:
        ecm_root_username = base_config.target.predefinedAuth.ncmRoot.username
        ecm_root_password = base_config.target.predefinedAuth.ncmRoot.password
        client = ncrf.get_client()
        client.authenticate(username=ecm_root_username, password=ecm_root_password)

    return client


def retrieve_kube_stack_config(ncm_target: str) -> AttrDict:
    run_config_path = "./configs/kube-run.yaml"
    target_config_path = "./configs/kube-target.yaml"

    cff = ConfigFixtureFactory()
    config = cff.create(
        run_config=run_config_path,
        target_config=target_config_path,
        version=1)

    # Update Services Hostnames:
    config.target.services.bsf.sfconnector.hostname = str(config.target.services.bsf.sfconnector.hostname).format(
        ncm_target)
    config.target.services.bsf.entitlementService.hostname = str(
        config.target.services.bsf.entitlementService.hostname).format(ncm_target)
    config.target.services.bsf.licenseService.hostname = str(config.target.services.bsf.licenseService.hostname).format(
        ncm_target)
    config.target.services.bsf.productSubscriptionService.hostname = str(
        config.target.services.bsf.productSubscriptionService.hostname).format(ncm_target)
    config.target.services.bsf.provisioningService.hostname = str(
        config.target.services.bsf.provisioningService.hostname).format(ncm_target)
    config.target.services.bsf.orderService.hostname = str(config.target.services.bsf.orderService.hostname).format(
        ncm_target)
    config.target.services.accounts.hostname = str(config.target.services.accounts.hostname).format(ncm_target)
    config.target.services.ncm.hostname = str(config.target.services.ncm.hostname).format(ncm_target)
    config.target.services.stream.hostname = str().format(ncm_target)
    config.target.services.als.hostname = str().format(ncm_target)
    config.target.services.nydus.hostname = str().format(ncm_target)

    return config


def retrieve_qa_stack_config(ncm_target: str) -> AttrDict:
    run_config_path = "./configs/{}-run.yaml".format(ncm_target)
    target_config_path = "./configs/{}-target.yaml".format(ncm_target)

    cff = ConfigFixtureFactory()
    config = cff.create(
        run_config=run_config_path,
        target_config=target_config_path,
        version=1)

    ecm_root_username = os.environ.get("ECM_ROOT_USERNAME", default=None)
    ecm_root_password = os.environ.get("ECM_ROOT_PASSWORD", default=None)

    if not ecm_root_username:
        sys.exit("Failed to retrieve the ECM ROOT USERNAME.")
    if not ecm_root_password:
        sys.exit("Failed to retrieve the ECM ROOT PASSWORD.")

    config.target.predefinedAuth.ncmRoot.username = ecm_root_username
    config.target.predefinedAuth.ncmRoot.password = ecm_root_password

    return config


def verify_available_ecm_features(session):
    params = {"fields": "name"}
    stack_features = session.ncm.get(path='/api/v1/features/', params=params).json()['data']
    found_features = []
    for d in stack_features:
        found_features.append(d["name"])

    expected_features = []
    for feature in ECMOnlyFeatureUUIDs.__members__:
        expected_features.append(feature)
    for feature in FeatureUUIDs.__members__:
        expected_features.append(feature)
    for feature in ProdECMRootAccountFeatureUUIDs.__members__:
        expected_features.append(feature)

    # Ensure found features are all in expected_features
    unexpected_features = []
    for feature in found_features:
        if feature not in expected_features:
            unexpected_features.append(feature)

    if unexpected_features:
        print("\n\n****************************************************************\n")
        print("Discovered the following features that NCTF does not know about:\n\n{}".format(unexpected_features))
        print("\n\n****************************************************************\n")


def get_ecm_feature_id(session, feature_name) -> str:
    params = {"name": feature_name}
    r = session.ncm.get(path='/api/v1/features/', params=params)
    assert r.status_code == 200
    response = r.json()['data']
    assert len(response) == 1, "Failed to locate the ECM ID."
    return response[0]['resource_uri']


def entitle_account(ncm_target, ecm_only_features, admin_username, admin_password):
    # If all features are requested, update the list.
    if ecm_only_features.lower() == "all":
        all_ecm_features = ""
        for feature in BSFBlacklistedFeatureUUIDs.__members__:
            all_ecm_features += feature + ','

        ecm_features = all_ecm_features.rstrip(',')
        features = str.split(ecm_features, ',')
    elif ecm_only_features.lower() == "none":
        print("No additional ECM features were requested, exiting.")
        return
    else:
        # Verify requested entitlements
        unrecognized_features = []
        features = str.split(ecm_only_features, ',')
        for f in features:
            # TODO: Another BSFBlackListedFeatureUUIDs
            if f not in BSFBlacklistedFeatureUUIDs.__members__:
                unrecognized_features.append(f)
        if len(unrecognized_features) > 0:
            sys.exit("Features must be on the BSF Blacklist.\nInvalid Product Codes: {}".format(unrecognized_features))

    # Based on what stack is requested, retrieve the creds to start making ECM Root calls to an account.
    if 'qa' in ncm_target:
        config = retrieve_qa_stack_config(ncm_target=ncm_target)
        admin_session = retrieve_admin_session(config)
        admin_session.authenticate(admin_username, admin_password)
        root_session = retrieve_ecm_root_admin_session(config, ncm_target)
    elif "-temp-" in ncm_target:
        config = retrieve_kube_stack_config(ncm_target=ncm_target)
        admin_session = retrieve_admin_session(config)
        admin_session.authenticate(admin_username, admin_password)
        root_session = retrieve_ecm_root_admin_session(config, ncm_target)
    else:
        print("Unrecognized stack target.")
        return

    # Verify if available features match expected features
    verify_available_ecm_features(admin_session)

    # Retrieve the customer ID
    accounts = admin_session.accounts.accounts.list().json()['data']
    assert len(accounts) == 1, "Retrieved more than the expected account."
    customer_id = accounts[0]['id']

    # Get the ECM Account ID via the ECM customer endpoint
    params = {"customer_id": customer_id}
    customer_data = admin_session.ncm.get(path='/api/v1/customers/', params=params).json()['data']
    assert len(customer_data) == 1, "Retrieved an unexpected number of customers."
    accounts_uri = customer_data[0]['account']

    # Add requested features
    failed_features = []
    for f in features:
        try:
            feature_id = get_ecm_feature_id(session=root_session, feature_name=f)
            payload = {
                "account": accounts_uri,
                "feature": feature_id,
                "enabled": True,
                "tos_accepted": True
            }
            r = root_session.ncm.post(path='/api/v1/featurebindings/', json=payload, timeout=60)
            assert r.status_code == 201, "Received status code {} when POSTING feature: {}".format(r.status_code, f)
        except Exception as e:
            print("Failed to entitle for feature: {}, error: {}".format(f, e))
            failed_features.append(f)

    if len(failed_features) > 0:
        print("\n\nFailed to enable the following features:\n{}".format(failed_features))
    else:
        print("\n\nSuccessfully added requested ECM Features.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ncm_target", help="e.g. \'qa1\' or kube: \'username-temp-340\'\n", required=True)
    feature_help = "The entitlement products you wish your account to have, in a non-spaced comma separated string.\n" \
                   "e.g. NETCLOUD_BRANCH_ROUTERS_1YR,NETCLOUD_BRANCH_ROUTERS_ADVANCED_UPGRADE_1YR" \
                   "You can see the supported avaiable products here:\n" \
                   "https://engops-prod-gitlab-v1.private.aws.cradlepointecm.com/Infrastructure/nctf/" \
                   "blob/master/nctf/libs/salesforce/common_lib.py\n"
    parser.add_argument("--ecm_features", help=feature_help)
    parser.add_argument("--account_admin_username", help="The Account Admin for the requested account.")
    parser.add_argument("--account_admin_password", help="The Account Admin Password for the requested account.")

    args = parser.parse_args()
    ncm_target = str.lower(args.ncm_target)
    ecm_features = args.ecm_features
    acct_username = args.account_admin_username
    acct_password = args.account_admin_password

    if not acct_username:
        sys.exit("Failed to provide the Account Admin Username")
    if not acct_password:
        sys.exit("Failed to provide the Account Admin Password")

    entitle_account(ncm_target=ncm_target,
                    ecm_only_features=ecm_features,
                    admin_username=acct_username,
                    admin_password=acct_password)
