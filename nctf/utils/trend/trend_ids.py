"""Parses Trend json files for App and Cat IDs"""

import json
from typing import Dict

import requests


"""
These are the Production URLs for reference.
APP_URL = "http://www.cradlepoint.com/files/uploads/UTMv2/ips_application.json"
CAT_URL = "http://www.cradlepoint.com/files/uploads/UTMv2/ips_app_category.json"
"""
# Urls used for FW older than 6.5.0
APP_URL_PRE_650 = "http://www.cradlepoint.com/files/uploads/TESTING/ips_application.json"
CAT_URL_PRE_650 = "http://www.cradlepoint.com/files/uploads/TESTING/ips_app_category.json"

APP_URL = "http://www.cradlepoint.com/files/uploads/UTMv3/ips_application.json"
CAT_URL = "http://www.cradlepoint.com/files/uploads/UTMv3/ips_app_category.json"

def _normalize_id(catid: str) -> int:
    """Trend offsets their Category IDs by 128 for some reason."""
    # TODO: make this work for pre-6.5.0 FW
    # return int(catid) - 128
    return int(catid)

def _normalize_app_catids(apps: Dict) -> Dict:
    """Modify the catid of an application in place to match ECM."""
    for app in apps.values():
        app['catid'] = _normalize_id(app['catid'])
    return apps


def _normalize_catids(cats: Dict) -> Dict:
    """Create a new dictionary of catids with normalized IDs."""
    new_cats = {}
    for cat_id, cat in cats.items():
        new_cats[_normalize_id(cat_id)] = cat['name']
    return new_cats


def trend_apps() -> Dict:
    """Gets curent list of Trend Applications."""
    res = requests.get(APP_URL)
    assert res.status_code == 200, 'Failed to get list of Trend Applications.' \
        ' Received {}: {}.'.format(res.status_code, res.text)
    url = res.json()['url']
    apps = requests.get(url).json()
    return _normalize_app_catids(apps)


def trend_apps_by_name() -> Dict:
    """Gets curent list of Trend Applications keyed by name."""
    apps = trend_apps()
    return {app['name']: {'appid': int(app['appid']), 'catid': app['catid']} for app in apps.values()}


def trend_cats() -> Dict:
    """Gets curent list of Trend Categories."""
    res = requests.get(CAT_URL)
    assert res.status_code == 200, 'Failed to get list of Trend Categories.' \
        ' Received {}: {}.'.format(res.status_code, res.text)
    url = res.json()['url']
    cats = requests.get(url).json()
    return _normalize_catids(cats)


def main() -> None:
    """Prints The Apps and Cats we got from Trend."""
    apps = trend_apps()
    cats = trend_cats()
    apps_by_name = trend_apps_by_name()

    print("\nApplications with normalized Cat IDs\n")
    print(json.dumps(apps, sort_keys=True, indent=4))
    print("\nCategories with normalized Cat IDs\n")
    print(json.dumps(cats, sort_keys=True, indent=4))
    print("\nApplications keyed by name with normalized Cat IDs\n")
    print(json.dumps(apps_by_name, sort_keys=True, indent=4))


if __name__ == '__main__':
    main()
