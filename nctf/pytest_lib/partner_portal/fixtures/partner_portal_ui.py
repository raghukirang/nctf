import logging

import pytest

from nctf.test_lib.portal.fixtures import PartnerPortalUIFixture

logger = logging.getLogger('partner_portal_ui_fixture')


@pytest.yield_fixture
def partner_portal_ui_fixture(base_ui_fixture):
    """Netcloud UI Fixture

    This yield fixture sets up a virtual display via the base UI fixture. It yields on the PartnerPortalUIFixture
    object, returning the object to the calling function. The PartnerPortalUIFixture supports Once the calling function is finished, it returns to this
    object which cleans up as necessary.

    Yields:
        PartnerPortalUIFixture: fixture that contains the web driver set up by the selenium fixture.
    """

    yield PartnerPortalUIFixture(get_driver=base_ui_fixture.get_driver)
