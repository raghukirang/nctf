import pytest

from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.accounts.rest.fixture import AccServRESTFixture


@pytest.fixture(name='accserv_rest_fixture')
def setup_accserv_rest_fixture(config_fixture: ConfigFixture):
    accounts_protocol = config_fixture.target.services.accounts.protocol
    accounts_hostname = config_fixture.target.services.accounts.hostname
    accounts_port = config_fixture.target.services.accounts.port

    accserv_rest = AccServRESTFixture(accounts_protocol, accounts_hostname, accounts_port)
    return accserv_rest
