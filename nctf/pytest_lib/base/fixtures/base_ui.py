import logging
import os
import time

import _pytest.fixtures
import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.base.fixtures.ui_fixture import BaseUIFixture
import nctf.test_lib.base.pages as JWP
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.core.proxy.server import BrowserMobProxyFixture

logger = logging.getLogger('base_ui_fixture')


@pytest.yield_fixture
def base_ui_fixture(config_fixture: ConfigFixture, proxy: BrowserMobProxyFixture, request: _pytest.fixtures.FixtureRequest):
    """Base UI Fixture

    This fixture instantiates a JourneyWebDriver according to configuration got from the config_fixture.  It sets up a
    virtual display if necessary. It sets up a proxy and gets it running if necessary. It yields on the BaseUIFixture
    object, returning the object to the calling function. Once the calling function is finished, it returns to this
    object which cleans up the journey base UI web driver, the virtual display and proxy.

    Args:
        config_fixture: the configuration fixture
        proxy: a proxy that typically records web traffic
        request: a fixture request object

    Returns:
        BaseUIFixture: fixture that contains the journey web driver as well as an optional virtual display and proxy.
    """
    logger.info('Setting up journey base ui fixture.')

    display_height = config_fixture.run.fixtures.selenium.displaySizeH
    display_width = config_fixture.run.fixtures.selenium.displaySizeW
    display_visible = config_fixture.run.fixtures.selenium.browserVisible
    browser_name = config_fixture.run.fixtures.selenium.get('browserName') or "chrome"
    browser_version = config_fixture.run.fixtures.selenium.get('browserVersion')
    browser_platform = config_fixture.run.fixtures.selenium.get('browserPlatform')
    browser_debug = config_fixture.run.fixtures.selenium.get('browserDebug') or False
    browser_profile = config_fixture.run.fixtures.selenium.get('browserProfile')

    timeout = config_fixture.run.fixtures.selenium.timeout

    # Disable proxy and recording for now...unless a need arises to create har files.
    use_proxy = False  #  config_fixture.run.fixtures.selenium.useProxy
    record_traffic = False  # config_fixture.run.fixtures.selenium.recordTraffic
    assert not (record_traffic and not use_proxy), "Cannot record traffic without proxy running."

    # Argument setup.
    artifacts_dir = None
    if hasattr(pytest, 'config') and pytest.config.getoption("--artifacts"):
        artifacts_dir = pytest.config.getoption("--artifacts")

    proxy_client = None
    if use_proxy:
        if not proxy.is_running:
            proxy.start(artifacts_dir=artifacts_dir)
            if hasattr(pytest, '_journey_current_test_func'):
                test_name = pytest._journey_current_test_func
            else:
                test_name = 'UnknownTest'
            # TODO: a magic sleep since the get_client seems to fail some of the time due to a connection reset.
            time.sleep(3)
            proxy_client = proxy.get_client(test_name, record_traffic).selenium_proxy

    # transfer config from the config_fixture to the config dictionary starting from a DEFAULT copy
    config = JWP.UIWebDriverConfig.DEFAULT.copy()
    config['display_height'] = display_height
    config['display_width'] = display_width
    config['timeout'] = timeout
    config['artifacts_dir'] = artifacts_dir
    config['display_visible'] = display_visible
    config['browser_name'] = browser_name
    config['browser_version'] = browser_version
    config['browser_platform'] = browser_platform
    config['browser_debug'] = browser_debug
    config['browser_debug_path'] = None
    if browser_debug:
        config['browser_debug_path'] = "{}/{}/run_{}".format(
            config['artifacts_dir'], os.environ.get('PYTEST_CURRENT_TEST').split(':')[-1].split(' ')[0],
            time.strftime("%Y%m%d-%H%M%S"))
    if browser_profile and browser_profile == "$PYTEST_CURRENT_TEST":
        browser_profile =  "{}/{}/run_{}/profile".format(
            config['artifacts_dir'], os.environ.get('PYTEST_CURRENT_TEST').split(':')[-1].split(' ')[0],
            time.strftime("%Y%m%d-%H%M%S"))
    config['browser_profile'] = browser_profile

    services_info = JWP.ServicesInfo(config_fixture.target.services)

    logger.debug('Getting a WebDriver')

    ui_fixture = BaseUIFixture(config=config, services_info=services_info, proxy=proxy_client)
    yield ui_fixture

    # If the test failed.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, ui_fixture)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of ui_fixture.')
            return

    logger.info('Tearing down journey web driver.')
    ui_fixture.teardown()
