from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.netcloud.fixtures.rest import NetCloudRESTFixture
import pytest


@pytest.fixture(name='netcloud_rest_fixture')
def setup_netcloud_rest_fixture(config_fixture: ConfigFixture):
    try:
        accounts_protocol = config_fixture.target.services.accounts.protocol
        accounts_hostname = config_fixture.target.services.accounts.hostname
        accounts_port = config_fixture.target.services.accounts.port
    except AttributeError:
        accounts_protocol = None
        accounts_hostname = None
        accounts_port = None

    try:
        ncm_protocol = config_fixture.target.services.ncm.protocol
        ncm_hostname = config_fixture.target.services.ncm.hostname
        ncm_port = config_fixture.target.services.ncm.port
    except AttributeError:
        ncm_protocol = None
        ncm_hostname = None
        ncm_port = None

    try:
        papi_protocol = config_fixture.target.services.papi.protocol
        papi_hostname = config_fixture.target.services.papi.hostname
        papi_port = config_fixture.target.services.papi.port
    except AttributeError:
        papi_protocol = None
        papi_hostname = None
        papi_port = None

    try:
        als_protocol = config_fixture.target.services.als.protocol
        als_hostname = config_fixture.target.services.als.hostname
        als_port = config_fixture.target.services.als.port
    except AttributeError:
        als_protocol = None
        als_hostname = None
        als_port = None

    try:
        overmind_protocol = config_fixture.target.services.nydus.protocol
        overmind_hostname = config_fixture.target.services.nydus.hostname
        overmind_port = config_fixture.target.services.nydus.port
    except AttributeError:
        overmind_protocol = None
        overmind_hostname = None
        overmind_port = None

    try:
        pdp_protocol = config_fixture.target.services.pdp.protocol
        pdp_hostname = config_fixture.target.services.pdp.hostname
        pdp_port = config_fixture.target.services.pdp.port
    except AttributeError:
        pdp_protocol = None
        pdp_hostname = None
        pdp_port = None

    try:
        x_cp_api_id = config_fixture.target.predefinedAuth.ncmAPIv2['X-CP-API-ID']
        x_cp_api_key = config_fixture.target.predefinedAuth.ncmAPIv2['X-CP-API-KEY']
    except AttributeError:
        x_cp_api_id = None
        x_cp_api_key = None

    netcloud_rest = NetCloudRESTFixture(accounts_protocol, accounts_hostname, accounts_port, ncm_protocol, ncm_hostname,
                                        ncm_port, papi_protocol, papi_hostname, papi_port, als_protocol, als_hostname,
                                        als_port, overmind_protocol, overmind_hostname, overmind_port, pdp_protocol,
                                        pdp_hostname, pdp_port, x_cp_api_id, x_cp_api_key)
    return netcloud_rest
