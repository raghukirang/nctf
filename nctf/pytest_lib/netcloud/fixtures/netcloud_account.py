import logging

import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.netcloud.fixtures import netcloud_account as NCA

logger = logging.getLogger("pytest_lib.netcloud.account")


@pytest.fixture(scope="function", name="netcloud_account_fixture")
def setup_netcloud_account_fixture(config_fixture: ConfigFixture, email_fixture, request):

    ncm_root = config_fixture.target.predefinedAuth.get('ncmRoot')

    netcloud_account = NCA.NetCloudAccountFixture(
        creation_method=NCA.CreationMethod[config_fixture.run.fixtures.netCloudAccount.creationMethod.upper()],
        email_fixture=email_fixture,
        config_fixture=config_fixture,
        use_activation_email=True,
        ncm_api_protocol=config_fixture.target.services.ncm.protocol,
        ncm_api_hostname=config_fixture.target.services.ncm.hostname,
        ncm_api_port=config_fixture.target.services.ncm.port,
        ncm_api_id=ncm_root.get('username') if ncm_root else None,
        ncm_api_token=ncm_root.get('password') if ncm_root else None,
        accounts_api_protocol=config_fixture.target.services.accounts.protocol,
        accounts_api_hostname=config_fixture.target.services.accounts.hostname,
        accounts_api_port=config_fixture.target.services.accounts.port,
        accounts_privileged_username=config_fixture.run.fixtures.netCloudAccount.get('accountsPrivilegedUsername'),
        accounts_privileged_password=config_fixture.run.fixtures.netCloudAccount.get('accountsPrivilegedPassword'),
        timeout=60.0)
    yield netcloud_account

    # If the test failed.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, netcloud_account)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of netcloud_account_fixture.')
            return

    try:
        netcloud_account.teardown()
    except Exception as e:
        logger.warning(f"Exception {e} occurred during teardown!")
