import logging

import pytest

from nctf.test_lib.netcloud.fixtures.netcloud_ui import NetCloudUIFixture

logger = logging.getLogger('netcloud_ui_fixture')


@pytest.yield_fixture
def netcloud_ui_fixture(base_ui_fixture):
    """Netcloud UI Fixture

    This yield fixture sets up a virtual display via the base UI fixture. It yields on the NetcloudUIFixture
    object, returning the object to the calling function. The NetcloudUIFixture supports Once the calling function is finished, it returns to this 
    object which cleans up as necessary.

    Yields:
        NetcloudUIFixture: fixture that contains the web driver set up by the selenium fixture.
    """

    yield NetCloudUIFixture(get_driver=base_ui_fixture.get_driver)
