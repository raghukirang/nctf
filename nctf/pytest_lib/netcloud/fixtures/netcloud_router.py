import logging

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.arm4.fixture import ARMv4Fixture
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.netcloud.fixtures import netcloud_router as NCR
import pytest

logger = logging.getLogger("pytest_lib.netcloud.router")


@pytest.fixture(scope="function", name="netcloud_router_fixture")
def setup_netcloud_router_fixture(config_fixture: ConfigFixture, request, arm4_fixture: ARMv4Fixture):
    logger.warning("WORK IN PROGRESS: netcloud_router_fixture is currently under development. "
                   "Its API may change unexpectedly.")

    netcloud_router = NCR.NetCloudRouterFixture(
        config_fixture=config_fixture,
        creation_method=NCR.CreationMethod[config_fixture.run.fixtures.netCloudRouter.creationMethod.upper()],
        request=request,
        arm4_fixture=arm4_fixture)
    yield netcloud_router

    # If the test failed.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, netcloud_router)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of netcloud_router_fixture.')
            return

    try:
        netcloud_router.teardown()
    except Exception as e:
        logger.warning(f"Exception {e} occurred during teardown!")
