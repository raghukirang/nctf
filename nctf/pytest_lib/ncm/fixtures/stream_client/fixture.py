import logging

import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.ncm.fixtures.stream_client import StreamClientFixture

logger = logging.getLogger('stream_client.fixture')


@pytest.yield_fixture()
def stream_client_fixture(config_fixture: ConfigFixture, request):
    s = StreamClientFixture(
        url=config_fixture.target.services.stream.hostname, port=config_fixture.target.services.stream.port)

    yield s

    # If the test failed.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, s)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of stream_client_fixture.')
            return

    s.teardown()
