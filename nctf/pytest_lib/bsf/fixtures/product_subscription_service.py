"""Fixture and Interface to interact with BSF Product Subscription Microservice through it's API.

This module provides an interface to connect and interact with the BSF Product Subscription Microservice
through their API. This interface establishes an authenticated requests connection to the Product Subscription Service.

In order to provide a connection to the Product Subscription Service, this module requires the following
configuration values within the specified stack:
- BSF.URLS.PRODUCT
- BSF.USER
- BSF.PASS

Example:
    Retrieving HTTP Status of Service::

        def test_connect_product_subscription_service_get_health(bsf_product_subscription_fixture):
            health = bsf_product_subscription_fixture.health()
            assert health == 200

    GET Operation on an Endpoint::

        def test_connect_product_subscription_service_get_device_entitlement(bsf_product_subscription_fixture):
            sf_device_entitlement_id = '01t50000001NzGV'
            response = bsf_product_subscription_fixture.get_device_entitlement(sf_device_entitlement_id, 'SALESFORCE')

    Utilizing the Requests *session* Attribute::

        def test_connect_product_subscription_service_invalid_update(bsf_product_subscription_fixture):
            url = '/api/v1/deviceEntitlements/01t50000001NzGV'
            payload = {'value': 'blah'}
            response = bsf_product_subscription_fixture.session.update(url, data=payload)
            assert response.status_code == 503

See Also:
    Product Subscription Service API Documentation:
    https://product-subscription-service-dev.public.aws.cradlepointecm.com/swagger-ui.html
"""
from nctf.test_lib.bsf.fixtures.product_subscription_service import ProductSubscriptionServiceSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_product_subscription_fixture(config_fixture: ConfigFixture):
    """BSF Product Subscription Service API Fixture

    This fixture establishes and maintains singleton connection to a specified BSF Product Subscription Service.

    Returns:
        ProductSubscriptionServiceSession: A class that abstracts and provides helper functions for connecting and
        interacting with the Product Subscription Service API.
    """
    return ProductSubscriptionServiceSession(config_fixture)
