"""Fixture and Interface to interact with BSF License Microservice through it's API.

This module provides an interface to connect and interact with the BSF License Microservice
through their API. This interface establishes an authenticated requests connection to the License Service.

In order to provide a connection to the License Service, this module requires the following
configuration values within the specified stack:
- BSF.URLS.LICENSE
- BSF.USER
- BSF.PASS

Example:
    Retrieving HTTP Status of Service::

        def test_connect_license_service_get_health(bsf_license_fixture):
            health = bsf_license_fixture.health()
            assert health == 200

    GET Operation on an Endpoint::

        def test_connect_license_service_get_license(bsf_license_fixture):
            sf_license_id = '01t50000001NzGV'
            response = bsf_license_fixture.get_license(sf_device_id, 'SALESFORCE')

    Utilizing the Requests *session* Attribute::

        def test_connect_license_service_invalid_update(bsf_license_fixture):
            url = '/api/v1/subscriptions/01t50000001NzGV'
            payload = {'value': 'blah'}
            response = bsf_license_fixture.session.update(url, data=payload)
            assert response.status_code == 503

See Also:
    SF Connector API Documentation: https://sfconnector-qa.devecm.com/swagger-ui.html
"""
from nctf.test_lib.bsf.fixtures.license_service import LicenseServiceSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_license_fixture(config_fixture: ConfigFixture):
    """BSF License Service API Fixture

    This fixture establishes and maintains singleton connection to a specified BSF License Service.

    Returns:
        LicenseServiceSession: A class that abstracts and provides helper functions for connecting and interacting with the License Service API.
    """
    return LicenseServiceSession(config_fixture)
