import pytest
from nctf.test_lib.bsf.fixtures.view_layer_solution import ViewLayerSolutionSession


@pytest.fixture(scope="module")
def bsf_vls_fixture(config_fixture):
    """BSF View Layer Solution Fixture

    This fixture establishes and maintains a connection to a specified BSF View Layer Solution API.

    Returns:
        ViewLayerSolutionSession: A class that abstracts and provides helper functions for connecting and interacting
        with the BSF View Layer Solution API.
    """
    return ViewLayerSolutionSession(config_fixture)

