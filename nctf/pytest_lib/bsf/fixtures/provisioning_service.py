"""Fixture and Interface to interact with BSF Provisioning Service Microservice through it's API.

This module provides an interface to connect and interact with the BSF Provisioning Microservice
through their API. This interface establishes an authenticated requests connection to the Provisioning Service.

In order to provide a connection to the Provisioning Service, this module requires the following
configuration values within the specified stack:
- BSF.URLS.PROVISIONING
- BSF.USER
- BSF.PASS

Example:
    Retrieving HTTP Status of Service::

        def test_provisioning_service_get_health(bsf_provisioning_service_fixture):
            health = bsf_provisioning_service_fixture.health()
            assert health == 200

    DELETE Operation on an Endpoint::

        def test_provisioning_service_delete_device(bsf_provisioning_service_fixture):
            entity_id = '01t50000001NzGV'
            response = bsf_provisioning_service_fixture.delete_device(entity_id)

    Utilizing the Requests *session* Attribute::

        def test_provisioning_service_invalid_update(bsf_provisioning_service_fixture):
            url = '/api/v1/device'
            payload = {'value': 'blah'}
            response = bsf_provisioning_service_fixture.session.update(url, data=payload)
            assert response.status_code == 503

See Also:
    Provisioning Service API Documentation:
    https://eps-dev.public.aws.cradlepointecm.com/swagger-ui.html
"""
from nctf.test_lib.bsf.fixtures.provisioning_service import ProvisioningServiceSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_provisioning_service_fixture(config_fixture: ConfigFixture):
    """BSF Provisioning Service API Fixture

    This fixture establishes and maintains singleton connection to a specified BSF Provisioning Service.

    Returns:
        ProvisioningServiceSession: A class that abstracts and provides helper functions for connecting and
        interacting with the Provisioning Service API.
    """
    return ProvisioningServiceSession(config_fixture)
