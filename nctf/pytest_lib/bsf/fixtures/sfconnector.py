"""Fixture and Interface to interact with BSF Salesforce Connector Microservice through it's API.

This module provides an interface to connect and interact with the BSF Salesforce Connector Microservice
through it's API. This interface establishes an authenticated requests connection to the Salesforce Connector.

In order to provide a connection to the Salesforce Connector, this module requires the following
configuration values within the specified stack:
- BSF.URLS.SFCONNECTOR
- BSF.USER
- BSF.PASS

Example:
    Retrieving HTTP Status of Service::

        def test_connect_sfconnector_get_health(bsf_sfconnector_fixture):
            health = bsf_sfconnector_fixture.health()
            assert health == 200

    GET Operation on an Endpoint::

        def test_connect_sfconnector_get_device(bsf_sfconnector_fixture):
            sf_device_id = '01t50000001NzGV'
            response = bsf_sfconnector_fixture.get_device(sf_device_id)

    Utilizing the Requests *session* Attribute::

        def test_connect_sfconnector_invalid_update(sbsf_sfconnector_fixture):
            url = '/api/v1/entitlements/01t50000001NzGV'
            payload = {'value': 'blah'}
            response = bsf_sfconnector_fixture.session.update(url, data=payload)
            assert response.status_code == 503

See Also:
    SF Connector API Documentation: https://sfconnector-qa.devecm.com/swagger-ui.html
"""
from nctf.test_lib.bsf.fixtures.sfconnector import SFConnectorSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_sfconnector_fixture(config_fixture):
    """BSF Salesforce Connector API Fixture

    This fixture establishes and maintains a connection to a specified BSF Salesforce Connector.

    Returns:
        SFConnectorSession: A class that abstracts and provides helper functions for connecting and interacting with the Salesforce Connector API.
    """
    return SFConnectorSession(config_fixture)
