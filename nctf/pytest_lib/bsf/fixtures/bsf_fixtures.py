from nctf.test_lib.bsf.fixtures.bsf_fixtures import BSFFixturesSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_fixtures(config_fixture: ConfigFixture):
    """
    Fixture that wraps all bsf fixtures into a single fixture.
    """
    return BSFFixturesSession(config_fixture)
