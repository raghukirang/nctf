"""Fixture and Interface to interact with BSF Entitlement Microservice through it's API.

This module provides an interface to connect and interact with the BSF Entitlement Microservice
through their API. This interface establishes an authenticated requests connection to the Entitlement Service.

In order to provide a connection to the Entitlement Service, this module requires the following
configuration values within the specified stack:
- BSF.URLS.ENTITLEMENT
- BSF.USER
- BSF.PASS

Example:
    Retrieving HTTP Status of Service::

        def test_connect_entitlement_service_get_health(bsf_entitlement_fixture):
            health = bsf_entitlement_fixture.health()
            assert health == 200

    GET Operation on an Endpoint::

        def test_connect_entitlement_service_get_device(bsf_entitlement_fixture):
            sf_device_id = '01t50000001NzGV'
            response = bsf_entitlement_fixture.get_device(sf_device_id, 'SALESFORCE')

    Utilizing the Requests *session* Attribute::

        def test_connect_entitlement_service_invalid_update(bsf_entitlement_fixture):
            url = '/api/v1/entitlements/01t50000001NzGV'
            payload = {'value': 'blah'}
            response = bsf_entitlement_fixture.session.update(url, data=payload)
            assert response.status_code == 503

See Also:
    SF Connector API Documentation: https://sfconnector-qa.devecm.com/swagger-ui.html
"""
from nctf.test_lib.bsf.fixtures.entitlement_service import EntitlementServiceSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_entitlement_fixture(config_fixture: ConfigFixture):
    """BSF Entitlement Service API Fixture

    This fixture establishes and maintains singleton connection to a specified BSF Entitlement Service.

    Returns:
        EntitlementServiceSession: A class that abstracts and provides helper functions for connecting and interacting with the Entitlement Service API.
    """
    return EntitlementServiceSession(config_fixture)
