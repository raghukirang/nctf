"""Fixture and Interface to interact with BSF Order Microservice through it's API.

This module provides an interface to connect and interact with the BSF Order Microservice
through their API. This interface establishes an authenticated requests connection to the Order Service.

In order to provide a connection to the Order Service, this module requires the following
configuration values within the specified stack:
- BSF.URLS.Order
- BSF.USER
- BSF.PASS

Example:
    Retrieving HTTP Status of Service::

        def test_connect_Order_service_get_health(bsf_Order_fixture):
            health = bsf_Order_fixture.health()
            assert health == 200
"""
from nctf.test_lib.bsf.fixtures.order_service import OrderServiceSession
from nctf.test_lib.core.config import ConfigFixture
import pytest


@pytest.fixture(scope="module")
def bsf_order_fixture(config_fixture: ConfigFixture):
    """BSF Order Service API Fixture

    This fixture establishes and maintains singleton connection to a specified BSF Order Service.

    Returns:
        OrderServiceSession: A class that abstracts and provides helper functions for connecting and interacting with the Order Service API.
    """
    return OrderServiceSession(config_fixture)
