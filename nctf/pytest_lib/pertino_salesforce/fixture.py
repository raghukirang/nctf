import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.pertino_salesforce.fixture import _PertinoSalesforceConnection


@pytest.yield_fixture(scope="function")
def pertino_salesforce_fixture(config_fixture: ConfigFixture, request):
    """Pertino Salesforce API Yield Fixture

    This fixture establishes and maintains singleton connection to a specified Salesforce stack. If the connection is
    successful, it returns a *simple_salesforce* Salesforce object, which can be used to query and perform crud operations
    on Salesforce through their REST API. Once the test is halted/finished, it will automatically clean up and delete any object
    that was created in Salesforce.

    Note:
        If a test run is completely terminated (like, SIGKILL), then this fixture will not get the chance to clean up objects
        that were created in Salesforce. It also won't be able to clean up objects that weren't created with the provided
        Salesforce Object classes (in the ./objects/ subfolder).

    Note:
        It is possible and advised to clean up Salesforce Objects manually where needed. If the cleanup process fails and throws
        an exception, it can fail to clean up any other dependencies (failed 'chain' deletion). You may have to bug someone to
        eventually cleanup the objects manually in Salesforce (don't blame me! It's *like* an NP problem! :P)

    Note:
        **You can use this interface to interact with the Salesforce API directly, but this is extremely ill-advised unless you
        completely understand how all of the Salesforce objects interact and work.** It is recommended you utilize the available
        Salesforce objects within *'/journey/fixtures/salesforce/objects/'* when creating/reading/updating/deleting records in
        Salesforce.

    See Also:
        https://pypi.python.org/pypi/simple-salesforce

    Yields:
        _SalesforceConnection: A singleton object that manages a connection to Salesforce.
    """
    pertino_sf_connection = _PertinoSalesforceConnection(config_fixture)
    yield pertino_sf_connection

    # If the test failed
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, pertino_sf_connection)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            pertino_sf_connection.logger.warning('Skipping teardown of pertino_salesforce_fixture.')
            return

    pertino_sf_connection.logger.debug('##### Cleaning up Pertino SF Objects... #####')
    pertino_sf_connection._cleanup()
    pertino_sf_connection.logger.debug('##### Cleaned up *KNOWN* Pertino SF Objects! #####\n\n')
