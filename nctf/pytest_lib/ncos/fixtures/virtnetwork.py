import logging

import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.config.fixture import ConfigFixture
from nctf.test_lib.ncos.fixtures.virtnetwork.fixture import VirtRouterConsole
from nctf.test_lib.ncos.fixtures.virtnetwork.fixture import VirtualNetwork

logger = logging.getLogger('ncos.fixture')


@pytest.fixture(name="virtnetwork_fixture")
def virtnetwork_fixture(config_fixture: ConfigFixture, request):
    logger.info("Setting up the network fixture.")
    hwmap = config_fixture.run.fixtures.virtNetwork.hardwareMap
    this_network = VirtualNetwork(config_fixture, request, hwmap)

    yield this_network

    # If the test failed.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, this_network)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of virtnetwork_fixture.')
            return

    this_network.teardown()


@pytest.yield_fixture(name="virtconsole_fixture")
def virtconsole_fixture():
    logger.info("Setting up Virt console fixture.")

    virt_console_handler = VirtRouterConsole()

    yield virt_console_handler

    logger.debug("Finished yielding virt_console_handler")
    virt_console_handler.teardown()


def pytest_addoption(parser):
    parser.addoption(
        "--syslog", action="store", default=None, help="Enable syslog server for router log output for virtnetwork_fixture")

    parser.addoption(
        "--virtnet_sandbox",
        action="store_true",
        help="Tell the virtnetwork fixture to not timeout interactive router consoles")

    parser.addoption(
        "--coverage",
        action="store_true",
        help="Option to make sure that we gather the coverage from the test")

    parser.addoption(
        "--gatherlogs",
        action="store_true",
        help="Option to make sure that we gather router logs only when we have the option")
