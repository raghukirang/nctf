import logging

import pytest

from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture

logger = logging.getLogger('ncos.fixture')


@pytest.fixture(name="wifi_client_fixture")
def wifi_client_fixture():
    logger.info("Setting up WiFiClient Fixture.")
    yield WiFiClientFixture()
