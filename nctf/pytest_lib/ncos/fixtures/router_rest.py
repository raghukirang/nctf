import logging

import pytest

from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPIFixture

logger = logging.getLogger('ncos.fixture')


@pytest.fixture(name="router_rest_fixture")
def router_rest_fixture():
    logger.info("Setting up RouterRESTAPI Fixture.")
    yield RouterRESTAPIFixture()
