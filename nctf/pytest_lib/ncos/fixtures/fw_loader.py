import logging

import pytest

from nctf.test_lib.core.config.fixture import ConfigFixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import BranchType
from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import FirmwareLoaderFixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import ArtifactoryFwGetter
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2

logger = logging.getLogger('ncos.fixture')


@pytest.fixture(name="firmware_loader_fixture")
def firmware_loader_fixture(config_fixture: ConfigFixture):
    guido_branch = config_fixture.run.fixtures.fwLoader.guidoBranch
    if not guido_branch:
        guido_branch = "NONE"
    guido_path = config_fixture.run.fixtures.fwLoader.guidoPath
    guido_date = config_fixture.run.fixtures.fwLoader.guidoDate
    local_path = config_fixture.run.fixtures.fwLoader.localFullPath
    img_location = config_fixture.run.fixtures.fwLoader.imgLocation
    load_method = config_fixture.run.fixtures.fwLoader.loadMethod

    device_firmware = FirmwareLoaderFixture(
        guido_branch=BranchType[guido_branch],
        local_full_path=local_path,
        guido_path=guido_path,
        guido_date=guido_date,
        img_location=img_location,
        load_method=load_method)
    yield device_firmware


@pytest.fixture(name="fwloader_v2_fixture")
def setup_fwloader_v2_fixture(config_fixture: ConfigFixture):

    img_location = config_fixture.run.fixtures.fwLoader2.imgLocation

    if img_location == "artifactory":

        af_api_key = config_fixture.run.fixtures.fwLoader2.apiKey
        af_repo = config_fixture.run.fixtures.fwLoader2.repository

        ncos_build_type = config_fixture.target.services.ncos.buildType
        ncos_build_date = config_fixture.target.services.ncos.buildDate
        ncos_git_branch = config_fixture.target.services.ncos.gitBranch
        ncos_git_sha = config_fixture.target.services.ncos.gitSha
        ncos_release_ver = getattr(config_fixture.target.services.ncos, 'releaseVer', None)

        af_getter = ArtifactoryFwGetter(
            af_api_key,
            af_repo,
            git_branch=ncos_git_branch,
            git_sha=ncos_git_sha,
            build_date=ncos_build_date,
            build_type=ncos_build_type,
            release_ver=ncos_release_ver)

        fwloader_v2 = FwLoaderFixtureV2(fw_getter=af_getter)

    else:  # img_location == "local"
        raise NotImplementedError()

    yield fwloader_v2
