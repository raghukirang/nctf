import logging

from nctf.test_lib.core.arm2.interface import ARMInterface
from nctf.test_lib.meta.fixtures import WifiMetaFixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPIFixture
from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture
import pytest

logger = logging.getLogger("pytest_lib.meta.fixtures.wifi")


@pytest.fixture(name="wifi_meta_fixture")
def setup_wifi_meta_fixture(tlogger: logging.Logger, arm_fixture: ARMInterface, router_rest_fixture: RouterRESTAPIFixture,
                            wifi_client_fixture: WiFiClientFixture, fwloader_v2_fixture: FwLoaderFixtureV2):

    wifif_meta_fixture = WifiMetaFixture(tlogger, arm_fixture, router_rest_fixture, wifi_client_fixture,
                                         fwloader_v2_fixture)
    yield wifif_meta_fixture
    wifif_meta_fixture.teardown()
