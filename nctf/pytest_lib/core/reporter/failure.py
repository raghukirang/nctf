# Standard
import logging
import os
import sys

from _pytest.fixtures import FixtureRequest

PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..')
sys.path.append(PATH)

logger = logging.getLogger('reporter.failure')


def report_failure(request: FixtureRequest, fixture_object):

    logger.info('#-' + '-' * len(request.node.nodeid) + '-#')
    logger.info('| {} |'.format(request.node.nodeid))
    logger.info('#-' + '-' * len(request.node.nodeid) + '-#')

    report = fixture_object.get_failure_report()
    if report:
        report_lines = report.splitlines()

        for line in report_lines:
            logger.info(line)
