import logging
import logging.config
import math
import os
import random
import string
import traceback

from _pytest.runner import Skipped
import pytest


@pytest.hookimpl(tryfirst=True)
def pytest_runtest_setup(item):
    """Log our test name with beautiful formatting!"""
    opid = ''.join([random.choice(string.ascii_uppercase + string.digits) for _ in range(6)])
    os.environ["NCTF_CURRENT_TEST"] = item.nodeid
    os.environ["NCTF_CURRENT_OPID"] = opid

    logger = logging.getLogger('tlogger')
    logger.info('#-' + '-' * len(item.nodeid) + '-#')
    logger.info('| {} |'.format(item.nodeid))
    logger.info('|' + ' ' * (math.floor((len(item.nodeid) - 13) / 2) + 1) + 'OPID = ' + opid + ' ' * (math.ceil(
        (len(item.nodeid) - 13) / 2) + 1) + '|')
    logger.info('#-' + '-' * len(item.nodeid) + '-#')

    # TODO: Remove me! BMP fixture taps into this. There must be a better way.
    pytest._journey_current_test = item.nodeid
    pytest._journey_current_test_func = pytest._journey_current_test.split('::')[-1:][0]


@pytest.hookimpl(hookwrapper=True)
def pytest_pyfunc_call(pyfuncitem):
    """Log beneficial information about the state of test execution."""
    logger = logging.getLogger('tlogger')
    try:
        # Execute all other hooks to obtain the report object.
        outcome = yield
        outcome.get_result()

    except KeyboardInterrupt:
        logger.warning('KeyboardInterrupt detected.')
        raise

    except Skipped as s:
        if hasattr(s, 'message'):
            logger.warning(s.message)
        else:
            logger.warning(s)
        raise

    except Exception as e:
        if hasattr(e, 'message'):
            logger.error(e.message)
            logger.error(traceback.format_exc())
        else:
            logger.error(e)
            logger.error(traceback.format_exc())
        raise

    # TODO: Remove me! BMP fixture taps into this. There must be a better way.
    pytest._journey_current_test = None
    pytest._journey_current_test_func = None


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    """Log beneficial information about the state of test execution."""
    logger = logging.getLogger('tlogger')

    # Execute all other hooks to obtain the report object.
    outcome = yield
    rep = outcome.get_result()

    # Handle setup step of test.
    if rep.when == "setup":
        if rep.failed is True:
            logger.info('TEST SETUP FAILED')

    # Handle call step of test.
    elif rep.when == "call":
        if rep.skipped is True:
            logger.info('TEST CALL SKIPPED')
        elif rep.failed is True:
            logger.info('TEST CALL FAILED')
        elif rep.failed is False:
            logger.info('TEST CALL PASSED')

    # Handle teardown step of test.
    elif rep.when == "teardown":
        if rep.failed is True:
            logger.info('TEST TEARDOWN FAILED')

    else:
        logger.error('UNDEFINED TEST STATE')
        raise Exception('UNDEFINED TEST STATE')

    # Set a report attribute for each phase of the test. This allows fixtures to know the status.
    setattr(item, "rep_" + rep.when, rep)
