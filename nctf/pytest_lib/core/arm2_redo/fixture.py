import logging
import os

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.arm2_redo.interface import ARMInterfaceFake
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import ArtifactoryFwGetter
import pytest

logger = logging.getLogger('pytest_lib.core.arm2_redo')


@pytest.fixture(name="arm2_redo_fixture")
def arm2_redo_fixture(config_fixture, request, netcloud_rest_fixture):
    logger.info('Setting up arm2_redo_fixture.')

    host = f"{config_fixture.run.fixtures.arm4.protocol}://{config_fixture.run.fixtures.arm4.hostname}"

    if hasattr(pytest, 'config') and pytest.config.getoption("--arm-api-key", None):
        logger.debug("Using CLI arg --arm-api-key for arm2_redo_fixture configuration.")
        api_key = pytest.config.getoption("--arm-api-key")
    else:
        api_key = config_fixture.run.fixtures.arm4.apiKey

    if hasattr(pytest, 'config') and pytest.config.getoption("--arm-lease-wait", None):
        logger.debug("Using CLI arg --arm-lease-wait for arm2_redo_fixture configuration.")
        lease_wait = pytest.config.getoption("--arm-lease-wait", None)
    else:
        lease_wait = config_fixture.run.fixtures.arm.leaseWait

    path = request.config.getoption("--artifacts", None)
    if path:
        path = os.path.join(path, f"{request.node.name}_router.log")

    # If the proper config has been provided then create a FwGetter to make it easy to load firmware.
    fw_getter = None
    af_api_key = config_fixture.run.fixtures.fwLoader2.apiKey
    af_repo = config_fixture.run.fixtures.fwLoader2.repository
    if hasattr(config_fixture.run.fixtures, "fwLoader2") and hasattr(config_fixture.target.services, "ncos"):
        if config_fixture.run.fixtures.fwLoader2.imgLocation == "artifactory":
            ncos_build_type = config_fixture.target.services.ncos.buildType
            ncos_build_date = config_fixture.target.services.ncos.buildDate
            ncos_git_branch = config_fixture.target.services.ncos.gitBranch
            ncos_git_sha = config_fixture.target.services.ncos.gitSha

            fw_getter = ArtifactoryFwGetter(
                af_api_key,
                af_repo,
                git_branch=ncos_git_branch,
                git_sha=ncos_git_sha,
                build_date=ncos_build_date,
                build_type=ncos_build_type)

        elif config_fixture.run.fixtures.fwLoader2.imgLocation == "local":
            raise NotImplementedError()

    arm = ARMInterfaceFake(
        host=host,
        port=config_fixture.run.fixtures.arm4.port,
        api_path='/api/v4/',
        api_key=api_key,
        lease_wait=lease_wait,
        router_log_path=path,
        default_reason=request.node.name,
        default_pool_id=config_fixture.run.fixtures.arm4.defaultPoolId,
        default_fw_getter=fw_getter,
        netcloud_rest_fixture=netcloud_rest_fixture,
        artifactory_api_key=af_api_key,
        artifactory_repo=af_repo)

    yield arm

    # If the test failed
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, arm)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of arm2_redo_fixture, '
                           'but the ARM service will reclaim the router within 30 minutes.')
            return

    logger.info('Tearing down arm2_redo_fixture.')

    arm.teardown()
