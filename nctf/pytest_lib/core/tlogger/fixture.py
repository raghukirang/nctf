import logging
import logging.config
import os
import sys

import pytest
import requests


@pytest.fixture(name='tlogger')
def setup_tlogger():
    logger = logging.getLogger('tlogger')
    yield logger


def pytest_addoption(parser):
    def directory(str_d):
        if not (os.path.isdir(str_d)):
            raise IOError("Path {} is not a directory.".format(str_d))
        if not (os.path.exists(str_d)):
            raise IOError("Path {} artifacts does not exist.".format(str_d))
        return str_d

    parser.addoption("--artifacts", action="store", type=directory, help="The path where test artifacts shall be saved.")

    parser.addoption(
        "--log-sys-out",
        action="store_true",
        help="Direct all logging to stdout. "
        "This should be used in conjunction with '-s' to prevent pytest from capturing stdout.")

    # Teardown options.
    parser.addoption("--no-teardown-on-fail", action="store_true")


def pytest_configure(config):
    if not config.option.artifacts:
        raise pytest.UsageError("--artifacts is a required argument")
    artifacts = config.option.artifacts
    log_level = config.option.log_level
    log_level = "INFO" if log_level is None else log_level
    log_sys_out = config.option.log_sys_out

    worker = os.environ.get("PYTEST_XDIST_WORKER", default="master")  # pytest-xdist support
    log_file_path = os.path.join(artifacts, f"nctf-{worker}.log")

    setup_logging(log_file_path=log_file_path, log_level=log_level, log_sys_out=log_sys_out)


def setup_logging(log_file_path: str, log_level: str = "INFO", log_sys_out: bool = False) -> None:
    """Setup Logging Configuration for NCTF"""

    if log_level:
        if log_level.isdigit():
            log_level_val = int(log_level)
        else:
            log_level_val = {
                "DEBUG": logging.DEBUG,
                "INFO": logging.INFO,
                "WARNING": logging.WARNING,
                "ERROR": logging.ERROR,
                "CRITICAL": logging.CRITICAL
            }[log_level.upper()]
    else:
        log_level_val = logging.INFO

    requests.packages.urllib3.disable_warnings()

    root_logger = logging.getLogger()
    root_logger.setLevel(log_level_val)
    formatter = logging.Formatter("%(asctime)s [%(levelname)8s] [%(name)16.16s] %(message)s")

    nctf_log_file_handler = logging.FileHandler(log_file_path)
    nctf_log_file_handler.setFormatter(formatter)
    nctf_log_file_handler.setLevel(log_level_val)
    root_logger.addHandler(nctf_log_file_handler)

    if log_sys_out:
        stdout_stream_handler = logging.StreamHandler(sys.stdout)
        stdout_stream_handler.setFormatter(formatter)
        stdout_stream_handler.setLevel(log_level_val)
        root_logger.addHandler(stdout_stream_handler)

    tlogger = logging.getLogger("nctf")
    tlogger.info("Logging setup complete.")
