import logging
import os

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.arm2.interface import ARMInterface
import pytest

logger = logging.getLogger('pytest_lib.core.arm')


@pytest.yield_fixture
def arm_fixture(config_fixture, request):
    logger.info('Setting up arm_fixture.')

    host = f"{config_fixture.run.fixtures.arm.protocol}://{config_fixture.run.fixtures.arm.hostname}"

    if hasattr(pytest, 'config') and pytest.config.getoption("--arm-api-key", None):
        logger.debug("Using CLI arg --arm-api-key for arm_fixture configuration.")
        api_key = pytest.config.getoption("--arm-api-key")
    else:
        api_key = config_fixture.run.fixtures.arm.apiKey

    if hasattr(pytest, 'config') and pytest.config.getoption("--arm-lease-wait", None):
        logger.debug("Using CLI arg --arm-lease-wait for arm_fixture configuration.")
        lease_wait = pytest.config.getoption("--arm-lease-wait", None)
    else:
        lease_wait = config_fixture.run.fixtures.arm.leaseWait

    path = request.config.getoption("--artifacts", None)
    if path:
        path = os.path.join(path, f"{request.node.name}_router.log")

    arm = ARMInterface(
        host=host,
        port=config_fixture.run.fixtures.arm.port,
        api_path=config_fixture.run.fixtures.arm.path,
        api_key=api_key,
        lease_wait=lease_wait,
        router_log_path=path)

    yield arm

    # If the test failed
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, arm)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of arm_fixture, '
                           'but the ARM service will reclaim the router within 30 minutes.')
            return

    logger.info('Tearing down arm_fixture.')

    arm.teardown()
    arm.unregister_all_routers_from_ecm()
    arm.release_all_routers()
    arm.release_all_wifi_routers()
    arm.release_all_wifi_clients()


def pytest_addoption(parser):
    # Automation Resource Manager configurations
    parser.addoption(
        "--arm-api-key",
        action="store",
        default=None,
        help="The Automation Resource Manager (ARM) API key required for leasing real devices.")

    parser.addoption(
        "--arm-lease-wait",
        action="store",
        default=30,
        type=int,
        help="The maximum amount time to wait for an ARM resource to become available.")
