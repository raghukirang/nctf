import logging

import pytest

logger = logging.getLogger('pytest_lib.core.testrail')


@pytest.hookimpl(hookwrapper=True, trylast=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    inject_testrail_special_xml_property(item, call)


def inject_testrail_special_xml_property(item, call):
    """Inject a TestRail Test Case ID property into the junitXML report using pytest.mark.trtc[0-9]+

    We'll always favor the pytest.mark on the test function over the class definition.

    Example output:

        <testsuite errors=0 failures=0 name=pytest skips=0 tests=2 time=2.0>
            <testcase file=project/tests/unit/foobar/tests.py
            classname=project.tests.unit.foobar.tests.TestCaseFoo
            name=test_foo_is_foo>
                <properties>
                    <property name="trtc" value="123"/>
                </properties>
            </testcase>
            <testcase file=project/tests/unit/foobar/tests.py
            classname=project.tests.unit.foobar.tests.TestCaseFoo
            name=test_bar_is_bar>
                <properties>
                    <property name="trtc" value="456"/>
                </properties>
            </testcase>
        </testsuite>

    """
    if call.when == 'setup':
        # I wish there was a better API for this.
        class_markers = item.cls.pytestmark if item.cls and hasattr(item.cls, 'pytestmark') else []
        test_markers = item.keywords._markers

        class_trtc_case_ids = [mark.name.replace('trtc', '') for mark in class_markers if 'trtc' in mark.name]
        test_trtc_case_ids = [mark.replace('trtc', '') for mark in test_markers if 'trtc' in mark]

        xml = getattr(item.config, "_xml", None)
        if xml:
            if test_trtc_case_ids:
                if len(test_trtc_case_ids) > 1:
                    logger.warning('Found multiple "trtc" marks on test for {}. '
                                   'This is currently unsupported.'.format(item.nodeid))
                node_reporter = xml.node_reporter(item.nodeid)
                node_reporter.add_property('trtc', test_trtc_case_ids[0])
            elif class_trtc_case_ids:
                if len(class_trtc_case_ids) > 1:
                    logger.warning('Found multiple "trtc" marks on class for {}. '
                                   'This is currently unsupported.'.format(item.nodeid))
                node_reporter = xml.node_reporter(item.nodeid)
                node_reporter.add_property('trtc', class_trtc_case_ids[0])
            else:
                # No 'trtc' marks found.
                pass
