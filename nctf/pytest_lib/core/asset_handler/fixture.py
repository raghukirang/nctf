import logging
import os

from nctf.test_lib.core.asset_handler import AssetHandlerFixture
import pytest

logger = logging.getLogger('pytest_lib.core.asset_handler')


@pytest.fixture(scope='function', name='asset_handler_fixture')
def setup_asset_handler_fixture(request):
    logger.debug('Setting up asset_handler fixture...')

    test_dir = request.fspath.dirname

    if hasattr(request.config.option, "assets"):
        project_assets_dir = request.config.getoption("--assets")
    else:
        project_assets_dir = None

    asset_handler = AssetHandlerFixture(test_dir=test_dir, project_assets_dir=project_assets_dir)

    yield asset_handler

    asset_handler.teardown()


def pytest_addoption(parser):
    def directory(str_d):
        if not (os.path.isdir(str_d)):
            raise IOError("Path {} is not a directory.".format(str_d))
        if not (os.path.exists(str_d)):
            raise IOError("Path {} artifacts does not exist.".format(str_d))
        return str_d

    parser.addoption(
        "--assets",
        action="store",
        required=False,
        type=directory,
        default=None,
        help="The path where test assets shall live.")
