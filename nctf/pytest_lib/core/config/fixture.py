import logging
import os

from nctf.libs.common_library import is_valid_directory
from nctf.test_lib.core.config import ConfigFixtureFactory
import pytest

logger = logging.getLogger("pytest_lib.core.config")


def pytest_addoption(parser):
    parser.addoption(
        "--target-conf",
        action="store",
        required=False,
        type=str,
        help="The configuration that defines the system under test. Specify using a fully qualified path "
        "ending in .yaml or by specifying a configuration in your invocation directory using "
        "only the filename without the extension.")

    parser.addoption(
        "--run-conf",
        action="store",
        required=False,
        type=str,
        help="The configuration that defines how nctf runs. "
        "Specify using a fully qualified path ending in .yaml or by specifying a configuration in "
        "your invocation directory using only the filename without the extension.")

    parser.addoption(
        "--set-conf",
        action="append",
        required=False,
        help="Set any values in the run or target configurations files using dot notation syntax. "
        "e.g. foo.bar=baz")


def handle_run_conf(config):
    test_root_directory = config.invocation_dir.strpath
    run_conf = config.option.run_conf

    # If the --run-conf specified was an alias to the invocation dir construct the full path and save it back.
    if not run_conf.endswith(".yaml"):
        run_conf_path = os.path.join(test_root_directory, run_conf + ".yaml")
        if not is_valid_directory(run_conf_path):
            raise FileNotFoundError("Could not find run config {} at path {}.".format(run_conf, run_conf_path))
        else:
            config.option.run_conf = run_conf_path

    # If the --run-conf specified was a fully qualified path check it and do nothing.
    if run_conf.endswith(".yaml") and not is_valid_directory(run_conf):
        raise FileNotFoundError("Could not find run config at {}.".format(run_conf))


def handle_target_conf(config):
    test_root_directory = config.invocation_dir.strpath
    target_conf = config.option.target_conf

    # If the --target-conf specified was an alias to the invocation dir construct the full path and save it back.
    if not target_conf.endswith(".yaml"):
        target_conf_path = os.path.join(test_root_directory, target_conf + ".yaml")
        if not is_valid_directory(target_conf_path):
            raise FileNotFoundError("Could not find target config {} at path {}.".format(target_conf, target_conf_path))
        else:
            config.option.target_conf = target_conf_path

    # If the --target-conf specified was a fully qualified path check it and do nothing.
    if target_conf.endswith(".yaml") and not is_valid_directory(target_conf):
        raise FileNotFoundError("Could not find target config at {}.".format(target_conf))


@pytest.fixture(scope="session")
def config_fixture(request):
    if not request.config.option.run_conf or not request.config.option.target_conf:
        raise pytest.UsageError("--run-conf and --target-conf must both be specified when using config_fixture")
    handle_run_conf(request.config)
    handle_target_conf(request.config)
    run_conf = request.config.option.run_conf
    target_conf = request.config.option.target_conf
    overrides = request.config.option.set_conf
    config_fixture = ConfigFixtureFactory().create(run_conf, target_conf, overrides=overrides, use_env_vars=True)
    return config_fixture
