import logging

import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.core.email import EmailFixture
from nctf.test_lib.core.email import EmailService

logger = logging.getLogger('pytest_lib.core.email')


@pytest.yield_fixture()
def email_fixture(config_fixture: ConfigFixture, request):
    service = EmailService(config_fixture.run.fixtures.email.api.service)
    protocol = config_fixture.run.fixtures.email.api.protocol
    hostname = config_fixture.run.fixtures.email.api.hostname
    port = config_fixture.run.fixtures.email.api.port
    token = config_fixture.run.fixtures.email.api.token if service == EmailService.MAILINATOR else None
    timeout = config_fixture.run.fixtures.email.api.timeout

    api_root = "{}://{}:{}".format(protocol, hostname, port)

    email = EmailFixture(service, api_root, token, timeout)
    yield email

    # If the test failed.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, email)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of email_fixture.')
            return

    email.teardown()
