import logging

import pytest

from nctf.pytest_lib.core.reporter.failure import report_failure
from nctf.test_lib.core.proxy import BrowserMobProxyFixture

logger = logging.getLogger('pytest_lib.proxy')

# TODO: ======================================================================
# TODO: We need two fixtures - one session-scoped one to provide just the
# TODO: proxy server, and another that uses the proxy server (fixture) to
# TODO: hand out proxy clients.
# TODO: This way, we don't need to spin up a proxy for every tests, but
# TODO: can still use the request.node method to get HAR files on failure.
# TODO: ======================================================================


@pytest.yield_fixture(name='proxy')
def setup_proxy_fixture(request):

    logger.info("Initializing proxy server (not starting it)")
    proxy_serv = BrowserMobProxyFixture()

    yield proxy_serv

    # On test failure, dumps HAR files.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed:
        report_failure(request, proxy_serv)

        # If we are configured to not clean up on a failure
        if hasattr(pytest, 'config') and pytest.config.getoption("--no-teardown-on-fail"):
            logger.warning('Skipping teardown of proxy fixture.')
            return

    logger.info("Tearing down proxy server")
    proxy_serv.teardown()


def pytest_addoption(parser):
    parser.addoption("--disable-proxy", action="store_false", dest='browser_mob_proxy', help="Disable the Browsermob proxy.")

    parser.addoption("--enable-proxy", action="store_true", dest='browser_mob_proxy', help="Enable the Browsermob proxy.")
