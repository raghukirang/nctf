import logging

import os
import pwd
import pytest
import socket

from nctf.test_lib.core.arm4 import ARMv4Fixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import ArtifactoryFwGetter

logger = logging.getLogger('pytest_lib.core.arm4')


@pytest.fixture(name='arm4_fixture')
def setup_arm4_fixture(request, config_fixture):
    logger.debug('Setting up arm4_fixture...')

    host = f"{config_fixture.run.fixtures.arm4.protocol}://{config_fixture.run.fixtures.arm4.hostname}"
    port = config_fixture.run.fixtures.arm4.port
    api_key = config_fixture.run.fixtures.arm4.apiKey
    default_pool_id = config_fixture.run.fixtures.arm4.defaultPoolId

    artifacts_dir = None
    if hasattr(pytest, 'config') and pytest.config.getoption("--artifacts"):
        artifacts_dir = pytest.config.getoption("--artifacts")

    # If the proper config has been provided then create a FwGetter to make it easy to load firmware.
    fw_getter = None
    if hasattr(config_fixture.run.fixtures, "fwLoader2") and hasattr(config_fixture.target.services, "ncos"):
        if config_fixture.run.fixtures.fwLoader2.imgLocation == "artifactory":

            af_api_key = config_fixture.run.fixtures.fwLoader2.apiKey
            af_repo = config_fixture.run.fixtures.fwLoader2.repository

            ncos_build_type = config_fixture.target.services.ncos.buildType
            ncos_build_date = config_fixture.target.services.ncos.buildDate
            ncos_git_branch = config_fixture.target.services.ncos.gitBranch
            ncos_git_sha = config_fixture.target.services.ncos.gitSha

            fw_getter = ArtifactoryFwGetter(
                af_api_key,
                af_repo,
                git_branch=ncos_git_branch,
                git_sha=ncos_git_sha,
                build_date=ncos_build_date,
                build_type=ncos_build_type)

        elif config_fixture.run.fixtures.fwLoader2.imgLocation == "local":
            raise NotImplementedError()

    test_host = socket.gethostname()
    user = pwd.getpwuid(os.getuid()).pw_name  # os.getlogin() won't work in some docker containers
    test = request.node.name
    default_reason = f"{test_host}:{user}:{test}"
    arm4 = ARMv4Fixture(host=host, api_key=api_key, port=port,
                        default_pool_id=default_pool_id,
                        default_fw_getter=fw_getter,
                        default_reason=default_reason)

    yield arm4

    # If the test failed and we have a place to save our router logs.
    if hasattr(request.node, 'rep_call') and request.node.rep_call.failed and artifacts_dir:
        arm4.collect_triage_logs(filename_prefix=test, directory=artifacts_dir)

    logger.debug('Tearing down arm4_fixture...')

    arm4.teardown()
