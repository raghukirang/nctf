# Standard
from ast import literal_eval
import inspect
import numbers
import operator
import os
import random
import string
import time
from typing import Any
from typing import Callable
from typing import List
import uuid
from xml.etree import ElementTree

from webcolors import rgb_to_hex


class WaitTimeoutError(Exception):
    """Raised when an expected result is not obtained within a timeout"""


class timer(object):
    """General purpose context manager for timing blocks of code."""

    def __init__(self):
        self.start_ts = None
        self.end_ts = None

    @property
    def duration(self):
        end = self.end_ts or time.monotonic()
        return end - self.start_ts

    def __enter__(self):
        self.start_ts = time.monotonic()
        return self

    def __exit__(self, *args):
        self.end_ts = time.monotonic()


def require_type(obj, expected_types):
    """Convenience function to raise TypeError with a helpful message.

    The message will be of the form,
        'Function <func_name> expected <expected_type>, not <given_type>'

    Args:
        obj: The object to check the type of.
        expected_types: The expected type, or tuple of types.

    Raises:
        TypeError: If isinstance(obj, expected_types) == False
    """

    def _type_info(type_or_class):
        return "{} ({})".format(type_or_class.__name__, type_or_class.__module__)

    if not isinstance(expected_types, type):
        if isinstance(expected_types, tuple):
            for expected_type in expected_types:
                if not isinstance(expected_type, type):
                    type_info = _type_info(type(expected_type))
                    raise TypeError("expected_types parameter must be a type or tuple of types, but got tuple "
                                    "containing {}".format(type_info))
        else:
            type_info = _type_info(type(expected_types))
            raise TypeError("expected_types parameter must be a type or tuple of types, not {}".format(type_info))

    if not isinstance(obj, expected_types):
        if isinstance(expected_types, type):
            expected_type_info = _type_info(expected_types)
        else:  # tuple of types
            expected_type_info_list = []
            for expected_type in expected_types:
                expected_type_info_list.append(_type_info(expected_type))
            expected_type_info = ' or '.join(expected_type_info_list)

        obj_type_info = _type_info(type(obj))
        frame = inspect.currentframe().f_back
        caller = frame.f_code.co_name
        if 'self' in frame.f_locals:
            caller = "{} in {}".format(caller, frame.f_locals['self'].__class__.__name__)

        raise TypeError("Function {} expected {}, not {}".format(caller, expected_type_info, obj_type_info))


def wait_for(timeout_sec: numbers.Number, expected_result: Any, func: Callable[..., Any], *args, **kwargs) -> Any:
    """General retry/timeout method for expected return values.

    Calls the given function repeatedly (no sleep interval) until the expected
    result is obtained, or the timeout is reached.

    Guaranteed to call the function at least once.

    See Also:
        wait_for_with_interval for usage examples.

    Args:
        timeout_sec: Seconds until timeout exception is raised.
        expected_result: The desired result of the call to 'func'.
        func: A callable function that returns a value.
        *args: Positional arguments for 'func'.
        **kwargs: Keyword arguments for 'func'.

    Keyword Args:
        _operator: Optional parameter to override the expected_result and
            function return check. Method provided must return a boolean
            value given two values to compare. Defaults to equality (operator.eq).

            Note that expected_result is the right-hand side of the operator.
            For example, if _operator=operator.gt, the effective evaluation is:
                >>> func(*args, **kwargs) > expected_result
            The method is called until the result is True.

    Returns:
        The expected result, if successful.

    Raises:
        WaitTimeoutError: If the expected result is not attained within the timeout.
    """
    return wait_for_with_interval(timeout_sec, 0, expected_result, func, *args, **kwargs)


def wait_for_with_interval(timeout_sec: numbers.Number,
                           interval_sec: numbers.Number,
                           expected_result: Any,
                           func: Callable[..., Any],
                           *args,
                           **kwargs) -> Any:
    """General retry/timeout method for expected return values.

    Calls the given function repeatedly (with a given interval between calls)
    until the expected result is obtained, or the timeout is reached.

    Guaranteed to call the function at least once.

    Args:
        timeout_sec: Seconds until timeout exception is raised.
        interval_sec: Time to wait between calls to 'func'.
        expected_result: The desired result of the call to 'func'.
        func: A callable function that returns a value.
        *args: Positional arguments for 'func'.
        **kwargs: Keyword arguments for 'func'.

    Keyword Args:
        _operator: Optional parameter to override the expected_result and
            function return check. Method provided must return a boolean
            value given two values to compare. Defaults to equality (operator.eq).

            Note that expected_result is the right-hand side of the operator.
            For example, if _operator=operator.gt, the effective evaluation is:
                >>> func(*args, **kwargs) > expected_result
            The method is called until the result is True.

    Returns:
        The expected result, if successful.

    Raises:
        WaitTimeoutError: If the expected result is not attained within the timeout.
    """
    require_type(timeout_sec, numbers.Number)
    require_type(interval_sec, numbers.Number)
    if not callable(func):
        raise TypeError("Cannot wait for non-callable object. Got '{}' type instead.".format(type(func).__name__))

    oper = kwargs.pop('_operator', operator.eq)
    if not callable(oper):
        raise TypeError("Provided operator must be a callable. Got '{}' type instead.".format(type(oper).__name__))

    ret = func(*args, **kwargs)
    last_possible_call = time.monotonic() + timeout_sec - interval_sec
    while not oper(ret, expected_result) and time.monotonic() < last_possible_call:
        time.sleep(interval_sec)
        ret = func(*args, **kwargs)

    if not oper(ret, expected_result):
        # Nicely format the func args for the exception message.
        args_str = ', '.join([arg.__repr__() for arg in args])
        kwargs_str = ', '.join(['{}={}'.format(k, v.__repr__()) for k, v in kwargs.items()])
        if args_str and kwargs_str:
            param_str = "{}, {}".format(args_str, kwargs_str)
        else:
            param_str = args_str + kwargs_str

        if oper is operator.eq:
            raise WaitTimeoutError('Function call "{}({})" timed out (timeout={} sec): Got final result of "{}", '
                                   'did not get expected result of "{}"'.format(func.__name__, param_str, timeout_sec,
                                                                                ret.__repr__(),
                                                                                expected_result.__repr__()))
        else:
            raise WaitTimeoutError('Function call "{}({})" timed out (timeout={} sec): Got final result of "{}", '
                                   'expected operator {} to evaluate to True with "{}"'.format(
                                       func.__name__, param_str, timeout_sec,
                                       ret.__repr__(), oper, expected_result.__repr__()))

    return ret


def __random_unicode__(length):
    """Generates a random unicode string based off a series of byte ranges.

    See Also:
        http://stackoverflow.com/a/21666621

    Args:
        length (int): Length of unicode string to generate.

    Returns:
        str: Random unicode string
    """
    get_char = chr

    # Update this to include code point ranges to be sampled
    include_ranges = [(0x0021, 0x0021), (0x0023, 0x0026), (0x0028, 0x007E), (0x00A1, 0x00AC), (0x00AE, 0x00FF), (
        0x0100, 0x017F), (0x0180, 0x024F), (0x2C60, 0x2C7F), (0x16A0, 0x16F0), (0x0370, 0x0377), (0x037A, 0x037E),
                      (0x0384, 0x038A), (0x038C, 0x038C)]

    alphabet = [
        get_char(code_point)
        for current_range in include_ranges for code_point in range(current_range[0], current_range[1] + 1)
    ]
    return ''.join(random.choice(alphabet) for i in range(length))


def random_string(length, prefix='', unicode=False):
    """Generates a random string based

    Args:
        length (int): Length of string to generate.
        prefix (str): Prefix of the generated string.
        unicode (bool): If True, generate a unicode string.

    Returns:
        str: Random string

    Raises:
        ValueError: Prefix was longer than the specified length.
    """
    if len(prefix) >= length:
        raise ValueError('Prefix \'{}\' is larger than the string length \'{}\''.format(prefix, length))

    if unicode is True:
        return u'{}{}'.format(prefix, __random_unicode__(length - (len(prefix))))
    else:
        return '{}{}'.format(prefix, ''.join(
            random.choice(string.ascii_letters) for i in range(length - (len(prefix)))))


def random_hex_string(length):
    """Generates a random hex string

    Args:
        length (int): Length of string to generate.

    Returns:
        str: Random hex string
    """
    return ''.join(random.choice(string.hexdigits) for i in range(length))


def random_oct_string(length):
    """Generates a random oct string

    Args:
        length (int): Length of string to generate.

    Returns:
        str: Random oct string
    """
    return ''.join(random.choice(string.octdigits) for i in range(length))


def random_digit_string(length):
    """Generates a random digit string

    Args:
        length (int): Length of string to generate.

    Returns:
        str: Random digit string
    """
    return ''.join(random.choice(string.digits) for i in range(length))


def random_uuid():
    """Generates a random UUID (GUID)

    Returns:
        uuid: Random UUID
    """
    return uuid.uuid4()


def random_mac():
    """
    Returns a randomized MAC Address starting with '0042'

    Note: MAC Addresses are required by the FW server, but they aren't validated. [12/7/15]
        - The MAC Address validation was deprecated with CP stopped charging for FW upgrades under 'CradleCare'.
        - MAC Addresses were used to determine if the requester (router) was tied to a valid 'CradleCare' license.
        - The 'is_eligible' Flag the FW server returns is always 'True' now, regardless of the MAC Address.
        - Salesforce parses the MAC Address field as a number, not a string. Anytime there is an 'E', it converts it to
          an exponential number. As a workaround converting the random_mac that is returned to lower case using .lower()

    :return: Randomized MAC Address String
    """
    return '0042{}'.format(random_hex_string(8)).lower()


def random_serial():
    """
    Returns a randomized Serial Number starting with 'MM'

    :return: Randomized Serial Number String
    """
    return 'MM{}'.format(random_digit_string(12))


def random_imei(length=0, salesforce_fix=False):
    """
    Returns a randomized IMEI starting

    Note: IMEI must be a string of length 8, 15, or 16 [01/19/16]

    Note: Salesforce parses an IMEI as a number, not a string. Anytime there is an 'E', it converts it to an exponential number.
        Set 'salesforce_fix' to True if you want an IMEI number without an 'E' in it.

    :return: Randomized IMEI String
    """
    imei_length = random.choice([8, 15, 16]) if length == 0 else length

    if salesforce_fix:
        return random_hex_string(imei_length).replace('E', '0')
    else:
        return random_hex_string(imei_length)


def is_valid_directory(d):
    str_d = str(d)
    if not (os.path.isdir(str_d)) and not (os.path.exists(str_d)):
        return False
    return True


def parse_junit_xml(junit_xml_file):
    # Reference: http://fruch.github.io/blog/2014/10/30/ELK-is-fun/
    tests_list = []

    xml_test = open(junit_xml_file, 'r').read()
    et = ElementTree.fromstring(xml_test)

    for test in et.iter('testcase'):
        item = dict(**test.attrib)
        failure = test.find("failure")
        error = test.find("error")
        skipped = test.find("skipped")

        if failure is not None:
            item['failure_message'] = failure.text
            outcome = 'failure'

        elif skipped is not None:
            item['skip_message'] = skipped.attrib['message']
            outcome = 'skipped'

        elif error is not None:
            item['error_message'] = error.text
            outcome = 'error'

        else:
            outcome = 'success'

        item['outcome'] = outcome
        item['run_time'] = item['time']

        # Time is a builtin in Logstash
        del item['time']

        tests_list.append(item)

    return tests_list


def get_color_from_webelement(element):
    """
    Converts the web element's CSS color property to a tuple.

    eg. "rgba(R,G,B,A)" becomes (R,G,B)
    """
    color_tuple = literal_eval(element.value_of_css_property('color')[4:])[:-1]
    return rgb_to_hex(color_tuple)


def read_csv(path: str) -> List[str]:
    """Reads a CSV file

    Args:
        path:
    """
    # contents = [client_obj, client_obj, client_obj...]
    # client_obj = [field, field, field...]
    contents = []
    with open(path) as file:
        lines = file.read().splitlines()
    for line in lines:
        line = line.replace(u'\ufeff', '')
        fields = line.split(",")
        client_obj = fields
        contents.append(client_obj)
    return contents
