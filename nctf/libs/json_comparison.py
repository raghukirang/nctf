__author__ = 'audrey mccormick, jstohler'


def json_compare(current, master):
    """Compares two JSON dictionaries and returns the difference between the two

    The recursive method compares two dict/list objects piece by piece.
    In a dict, keys and values must match, but value types are not
    required to match (i.e. 1 and '1' will return true)

    Dicts and lists are compared differently, as dicts need to compare
    keys to match values, but lists are just sorted to determine if the
    contents match

    Differences are returned as a tuple of keys and values with
    the path preserved
    ({"current": value}, {"master": value})

    Args:
        current (dict): JSON data to compare with.
        master (dict): JSON data to compare against.

    Returns:
        bool: Current === Master
        dict: Current and Master are different.
    """
    if current is None and master is None:
        return True

    current_type = type(current)
    master_type = type(master)

    # Both items are dictionaries
    if current_type is dict and master_type is dict:
        differences = {}

        for item in current:
            if item in master.keys():

                # Recursively compare the values in the dictionaries (as a dict can contain a dict/list).
                result = json_compare(current[item], master[item])

                # If the comparison fails, add the differences to the dictionary.
                if result is not True:
                    differences[item] = result

            # If item doesn't exist in master, return the differences.
            else:
                differences[item] = {"*current*": current[item]}, {"*master*": None}

        # If there are differences, return them.
        if len(differences) != 0:
            return differences

    # Both items are lists
    elif current_type is list and master_type is list:
        c1 = sorted(current)
        c2 = sorted(master)

        if len(c1) != len(c2):
            return {"*current*": current}, {"*master*": master}
        else:
            differences = []
            for i in range(len(c1)):

                # Recursively compare the values in the lists (as a list can contain a dict/list).
                result = json_compare(current[i], master[i])

                # If the comparison fails, add the differences to the list.
                if result is not True:
                    differences.append(result)

            # If there are differences, return them.
            if len(differences) != 0:
                return differences

    # Item types don't match
    elif current != master:
        return {"*current*": current}, {"*master*": master}

    # No differences were found in other comparisons
    return True
