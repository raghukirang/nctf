import logging

# Note: Only for backwards compatibility
#   - Please don't use this! Use 'TLogger'!!!
logger = logging.getLogger('deprecated.logger')
