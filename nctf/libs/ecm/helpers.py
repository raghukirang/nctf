import logging
from nctf.fixtures.selenium.interface import SeleniumSessionInterface


def ensure_nce_status_column_visible(logger: logging.Logger, selenium_iface: SeleniumSessionInterface) -> None:
    """Enables the NCE Status column on the Devices page, if it is not already enabled.

    Args:
        logger: A test logger instance.
        selenium_iface: Interface to Selenium. used to check and enable the NCE Status column.
    """
    logger.info('Ensuring the Column Selection includes NetCloud Status')
    selenium_iface.action.click(selenium_iface.ecm.devices_page.routers_panel.table_buttons.column_selection_button())
    checkbox = selenium_iface.ecm.devices_page.routers_panel.table_buttons.column_selection_checkbox('NetCloud Status')
    checkbox_item = selenium_iface.ecm.devices_page.routers_panel.table_buttons.column_selection_item(
        'NetCloud Status')
    checkbox_class = selenium_iface.action.get_attribute('class', checkbox_item)
    if "x-form-cb-checked" not in checkbox_class:
        logger.info("The NetCloud Status Column isn't selected. Selecting it now...")
        selenium_iface.action.click(checkbox)

    # Close the column selection menu and wait for any potential spinners.
    selenium_iface.action.click(selenium_iface.ecm.devices_page.routers_panel.table_buttons.column_selection_close())
    selenium_iface.wait.wait_for_spinner()


def router_nce_icon_status(selenium_iface: SeleniumSessionInterface, router_id: str) -> str:
    """Refreshes the page and returns the class of the NCE Status icon.

    Args:
        selenium_iface: Interface to Selenium, used to refresh the page and fetch the icon data.
        router_id: The ECM ID of the router to check the NCE status of.

    Returns:
        The class of the NCE Status icon (eg. 'tunnel_add_client-icon').
    """
    selenium_iface.driver.refresh()
    selenium_iface.wait.wait_for_spinner()
    nce_status_icon = selenium_iface.ecm.devices_page.routers_panel.device_nce_button(client_id=router_id)
    return selenium_iface.action.get_attribute('class', nce_status_icon)
