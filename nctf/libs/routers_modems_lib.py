#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from collections import namedtuple


# ------------------------------------------
# General Declarations
# ------------------------------------------
test_firmware_url = 'www-qa.cradlepoint.com'
prod_firmware_url = 'www.cradlepoint.com'


# ------------------------------------------
# Device Declarations
# ------------------------------------------
# This file is based on an old design for testing Firmware Downloads.
# ------------------------------------------
device = namedtuple('Device', ['key', 'model', 'series', 'filename'])
xml_device_resp = namedtuple('XML_Device_Response', ['product_name', 'content'])

routers = []
modems = []

# S3 Routers
routers_s3 = [
    device('2100', '2100', 3, '2100/firmware-2100.json'),
    device('AER1600', 'AER1600', 3, 'AER1600/firmware-AER1600.json'),
    device('AER1650', 'AER1650', 3, 'AER1650/firmware-AER1650.json'),
    device('AER3100', 'AER3100', 3, 'AER3100/firmware-AER3100.json'),
    device('AER3150', 'AER3150', 3, 'AER3150/firmware-AER3150.json'),
    device('CBA750B', 'CBA750B', 3, 'CBA750B/firmware-cba750b.json'),
    device('CBA850', 'CBA850', 3, 'CBA850/firmware-cba850.json'),
    device('CBR400', 'CBR400', 3, 'CBR400/firmware-cbr400.json'),
    device('CBR450', 'CBR450', 3, 'CBR450/firmware-cbr450.json'),
    device('CTR35', 'CTR35', 3, 'CTR35/firmware-ctr35.json'),
    device('IBR350', 'IBR350', 3, 'IBR350/firmware-ibr350.json'),
    device('IBR600', 'IBR600', 3, 'IBR600/firmware-ibr600.json'),
    device('IBR650', 'IBR650', 3, 'IBR650/firmware-ibr650.json'),
    device('IBR1100', 'IBR1100', 3, 'IBR1100/firmware-IBR1100.json'),
    device('IBR1150', 'IBR1150', 3, 'IBR1150/firmware-IBR1150.json'),
    device('MBR1200B', 'MBR1200B', 3, 'MBR1200B/firmware-mbr1200b.json'),
    device('MBR1400', 'MBR1400', 3, 'MBR1400/firmware-mbr1400.json'),
    device('MBR1400V2', 'MBR1400V2', 3, 'MBR1400v2/firmware-mbr1400v2.json'),
    device('MBR95', 'MBR95', 3, 'MBR95/firmware-mbr95.json')
]

# S2 Routers
routers_s2 = [
    device('CBA250', 'CBA250', 2, 'firmware-cba250.xml'),
    device('CBA750', 'CBA750', 2, 'firmware-cba750.xml'),
    device('CTR500', 'CTR500', 2, 'firmware-ctr500.xml'),
    device('CTR', 'CTR', 2, 'firmware-ctr.xml'),
    device('CX111', 'CX111', 2, 'firmware-cx111.xml'),
    device('MBR1200', 'MBR1200', 2, 'firmware-mbr1200.xml'),
    device('MBR900', 'MBR900', 2, 'firmware-mbr900.xml'),
    device('MBR90', 'MBR90', 2, 'firmware_mbr90.xml'),
    device('MBR900', 'MBR900', 2, 'firmware-mbr.xml'),
    device('PHS300s', 'PHS300s', 2, 'firmware_sprint_phs.xml'),
    device('MBR800', 'MBR800', 2, 'firmware-hstd.xml'),
    device('MBR1000', 'MBR1000', 2, 'firmware-mbr.xml')
]

# S1 Routers
routers_s1 = [
    device('PHS300CC', 'PHS300CC', 1, 'firmware_ccphs.xml'),
    device('PHS300CW', 'PHS300CW', 1, 'firmware_cphs.xml'),
    device('CTR350', 'CTR350', 1, 'firmware-ctr.xml'),
    device('PHS300IC', 'PHS300IC', 1, 'firmware_ic_phs.xml'),
    device('PHS300', 'PHS300', 1, 'firmware_phs.xml'),
    device('PHS300TW', 'PHS300TW', 1, 'firmware_twc_phs.xml')
]

# Redbox S3 Routers
routers_rb_s3 = [
    device('CBR450RB', 'CBR450RB', 3, 'CBR450RB/firmware-cbr450rb.json'),
    device('IBR600RB', 'IBR600RB', 3, 'IBR600RB/firmware-ibr600rb.json'),
    device('IBR650RB', 'IBR650RB', 3, 'IBR650RB/firmware-ibr650rb.json')
]


# Redbox S2 Routers
routers_rb_s2 = [
    device('CBA250RB', 'CBA250RB', 2, 'firmware_redbox_cba250.xml')
]

# S3 Modems
modems_s3 = [
    device('MC7354VZ', 'MC7354-CP', 3, 'MODEM/firmware-mc7354-cp-VZW.json'),
    device('MC7354SP', 'MC7354-CP', 3, 'MODEM/firmware-mc7354-cp-SPRINT.json'),
    device('MC7354GN', 'MC7354-CP', 3, 'MODEM/firmware-mc7354-cp-GENNA-UMTS.json'),
    device('MC7354AT', 'MC7354-CP', 3, 'MODEM/firmware-mc7354-cp-ATT.json'),
    device('MC7304EU', 'MC7304-CP', 3, 'MODEM/firmware-mc7304-cp-GENEU-4G.json'),
    device('MC5728VZ', 'MC5728V', 3, 'MODEM/firmware-mc5728v-535477.json'),
    device('MC7700AT', 'MC7700', 3, 'MODEM/firmware-mc7700-9901139.json'),
    device('MC7750VZ', 'MC7750', 3, 'MODEM/firmware-mc7750.json'),
    device('MC7710', 'MC7710', 3, 'MODEM/firmware-mc7710-9902268.json'),
    device('MC7455AT', 'MC7455-CP', 3, 'MODEM/firmware-mc7455-cp-ATT.json'),
    device('MC7455GN', 'MC7455-CP', 3, 'MODEM/firmware-mc7455-cp-GENERIC.json'),
    device('MC7455SP', 'MC7455-CP', 3, 'MODEM/firmware-mc7455-cp-SPRINT.json'),
    device('MC7455VZ', 'MC7455-CP', 3, 'MODEM/firmware-mc7455-cp-VERIZON.json'),
    device('TOBYL100', 'TOBY-L100', 3, 'MODEM/firmware-toby-l100.json')
]

# S2 Modems
modems_s2 = [
    device('CBA250', 'CBA250', 2, 'modem_firmware_cba250.xml'),
    device('CBA750', 'CBA750', 2, 'modem_firmware_cba750.xml'),
    device('CTR500', 'CTR500', 2, 'modem_firmware_ctr500.xml'),
    device('CX111', 'CX111', 2, 'modem_firmware_cx111.xml'),
    device('MBR1000', 'MBR1000', 2, 'modem_firmware_mbr1000.xml'),
    device('MBR1200', 'MBR1200', 2, 'modem_firmware_mbr1200.xml'),
    device('MBR800', 'MBR800', 2, 'modem_firmware_mbr800.xml'),
    device('MBR900', 'MBR900', 2, 'modem_firmware_mbr900.xml'),
    device('MBR90', 'MBR90', 2, 'modem_firmware_mbr90.xml')
]

# S1 Modems
modems_s1 = [
    device('PHS300', 'PHS300', 1, 'modem_firmware_phs.xml'),
    device('PHS300CC', 'PHS300CC', 1, 'modem_firmware_ccphs.xml'),
    device('PHS300CW', 'PHS300CW', 1, 'modem_firmware_cphs.xml'),
    device('PHS300S', 'PHS300S', 1, 'modem_firmware_sphs.xml'),
    device('PHS300TW', 'PHS300TW', 1, 'modem_firmware_tphs.xml'),
    device('PHS300IC', 'PHS300IC', 1, 'modem_firmware_ic_phs.xml')
]

# Redbox Modems
modems_rb = [
    device('CBA250RB', 'CBA250RB', 2, 'modem_firmware_redbox_cba250.xml')
]

# Add all categories to overall lists
routers += routers_s3
routers += routers_s2
routers += routers_s1
routers += routers_rb_s3
routers += routers_rb_s2

modems += modems_s3
modems += modems_s2
modems += modems_s1
modems += modems_rb


applications = [
    device('UTM Category', 'UTM Category', 3, 'UTM/ips_category.json'),
    device('UTM Signature', 'UTM Signature', 3, 'UTM/ips_signature.json'),
    device('UTM Rules', 'UTM Rules', 3, 'UTM/rules.json'),
]


# ------------------------------------------
# Device Generic Helper Functions
# ------------------------------------------
def random_mac():
    """
    Returns a randomized MAC Address starting with '0042'

    Note: MAC Addresses are required by the FW server, but they aren't validated. [12/7/15]
        - The MAC Address validation was deprecated with CP stopped charging for FW upgrades under 'CradleCare'.
        - MAC Addresses were used to determine if the requester (router) was tied to a valid 'CradleCare' license.
        - The 'is_eligible' Flag the FW server returns is always 'True' now, regardless of the MAC Address.

    :return: Randomized MAC Address String
    """
    suffix = []
    for i in range(0, 8):
        suffix += random.choice('ABCDEF0123456789')
    random.shuffle(suffix)
    return '0042{}'.format(''.join(suffix))


def random_serial():
    """
    Returns a randomized Serial Number starting with 'MM'

    :return: Randomized Serial Number String
    """
    suffix = []
    for i in range(0, 12):
        suffix += random.choice('0123456789')
    random.shuffle(suffix)
    return 'MM{}'.format(''.join(suffix))


def random_imei(valid=True, length=0):
    """
    Returns a randomized IMEI starting

    Note: IMEI must be a string of length 8, 15, or 16 [01/19/16]

    :return: Randomized IMEI String
    """
    suffix = []
    imei_length = random.choice([8, 15, 16]) if length == 0 else length

    for i in range(0, imei_length):
        # TODO: Add 'E' whenever SF searching issue is fixed (E gets converted to exponent, throws a Numeric Overflow)
        suffix += random.choice('ABCDF0123456789') if valid is True else u''.join(chr(random.choice((0x300, 0x2000)) + random.randint(0, 0xff)))

    random.shuffle(suffix)
    return '{}'.format(''.join(suffix))


def random_firmware_version(series=3):
    """
    Returns a random version string based on the Device Series

    Note: Version Strings are required by the FW server, but they aren't validated. [12/7/15]
        - The Version String validation was deprecated when CP stopped charging for FW upgrades under 'CradleCare'.
        - Version strings used to allow routers to request specific FW versions of their routers.
        - The FW server now only returns the newest FW download version, regardless of the input.

    :param series: Device Series
    :return: FW Version String. None if invalid Device Series.
    """
    if series == 3:
        return '{}_{}_{}'.format(random.randint(3, 5), random.randint(3, 5), random.randint(3, 5))
    elif series == 2 or series == 1:
        return '{}_{}_{}'.format(random.randint(1, 2), random.randint(0, 4), random.randint(0, 4))
    else:
        return None


def firmware_path(series=3):
    """
    Returns FW URI Path based on Device Series

    :param series: Device Series
    :return: FW URI Path. Ex: 'files/uploads'. None if invalid Device Series
    """
    if series == 3:
        return 'files/uploads'
    elif series == 2 or series == 1:
        return 'download'
    else:
        return None


def build_firmware_url(device, protocol='http', prod=False, mac='0000000000000', version='0_0_0'):
    """
    Builds a FW Server URL for a Specified Device

    :param device: Router/Modem Device Tuple
    :param protocol: URL Protocol
    :param prod: Use Production FW URL?
    :param mac: MAC Address String
    :param version: FW Version String
    :return: FW Server URL for specified Device.
    """
    if prod is True:
        return '{}://{}/{}/{}?mac={}&ver={}'.format(protocol, prod_firmware_url, firmware_path(device.series), device.filename, mac, version)
    else:
        return '{}://{}/{}/{}?mac={}&ver={}'.format(protocol, test_firmware_url, firmware_path(device.series), device.filename, mac, version)


def find_device_from_key(device_list, key):
    """
    Returns a device from a specified device_list that matches a specific key.

    :param device_list: List of Devices to parse (modems, routers...)
    :param key: product 'key' (S3) or 'product_name' (S1 or S2) to match.
    :return: device that matches the specified key. None if nothing found.
    """
    for entry in device_list:
        if entry.key.lower == key.lower:
            return entry
    return None


# ------------------------------------------
# Device Definitions
# ------------------------------------------
class DeviceResponseMasterList(object):
    # Note: Device Responses left in JSON or XML form, regardless of redundancy, for readability and ease of modification.

    class Modems(object):

        class S3(object):
            json_modem_resp = namedtuple('JSON_Modem_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'build_version', 'package_version', 'over_write', 'eligible', 'url'])
            json_modem_nobuild_resp = namedtuple('JSON_Modem_Build_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'package_version', 'over_write', 'eligible', 'url'])
            json_modem_nopackage_resp = namedtuple('JSON_Modem_Build_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'build_version', 'over_write', 'eligible', 'url'])
            json_modem_minversion_resp = namedtuple('JSON_Modem_Build_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'min_router_version', 'over_write', 'eligible', 'url'])

            mc7354vz_http = json_modem_resp('MC7354VZ', 'MC7354-CP', 5, 5, 58, 1, '05.05.58.01_VZW,005.030_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_VZ_5_05_58_01_30.mdm')._asdict()
            mc7354vz_https = json_modem_resp('MC7354VZ', 'MC7354-CP', 5, 5, 58, 1, '05.05.58.01_VZW,005.030_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_VZ_5_05_58_01_30.mdm')._asdict()

            mc7354sp_http = json_modem_resp('MC7354SP', 'MC7354-CP', 5, 5, 63, 1, '05.05.63.01_SPRINT,005.035_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_SP_5_05_63_01.mdm')._asdict()
            mc7354sp_https = json_modem_resp('MC7354SP', 'MC7354-CP', 5, 5, 63, 1, '05.05.63.01_SPRINT,005.035_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_SP_5_05_63_01.mdm')._asdict()

            mc7354gn_http = json_modem_resp('MC7354GN', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_GENNA-UMTS,005.025_002', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_GN_eHRPD_05_05_58_00.mdm')._asdict()
            mc7354gn_https = json_modem_resp('MC7354GN', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_GENNA-UMTS,005.025_002', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_GN_eHRPD_05_05_58_00.mdm')._asdict()

            mc7354at_http = json_modem_resp('MC7354AT', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_ATT,005.026_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_AT_5_05_58_00.mdm')._asdict()
            mc7354at_https = json_modem_resp('MC7354AT', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_ATT,005.026_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_AT_5_05_58_00.mdm')._asdict()

            mc7455at_http = json_modem_resp('MC7455AT', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_ATT,002.020_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_AT_02_20_03_00.mdm')._asdict()
            mc7455at_https = json_modem_resp('MC7455AT', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_ATT,002.020_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_AT_02_20_03_00.mdm')._asdict()

            mc7455gn_http = json_modem_resp('MC7455GN', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_GENERIC,002.017_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_GN_02_20_03_00.mdm')._asdict()
            mc7455gn_https = json_modem_resp('MC7455GN', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_GENERIC,002.017_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_GN_02_20_03_00.mdm')._asdict()

            mc7455sp_http = json_modem_resp('MC7455SP', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_SPRINT,002.020_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_SP_02_20_03_22.mdm')._asdict()
            mc7455sp_https = json_modem_resp('MC7455SP', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_SPRINT,002.020_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_SP_02_20_03_22.mdm')._asdict()

            mc7455vz_http = json_modem_resp('MC7455VZ', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_VERIZON,002.026_001', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_VZ_02_20_03_22.mdm')._asdict()
            mc7455vz_https = json_modem_resp('MC7455VZ', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_VERIZON,002.026_001', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_VZ_02_20_03_22.mdm')._asdict()

            mc7304eu_http = json_modem_resp('MC7304EU', 'MC7304-CP', 5, 5, 58, 0, '05.05.58.00_GENEU-4G,005.027_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP3_EU_5_05_58_00_27.mdm')._asdict()
            mc7304eu_https = json_modem_resp('MC7304EU', 'MC7304-CP', 5, 5, 58, 0, '05.05.58.00_GENEU-4G,005.027_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP3_EU_5_05_58_00_27.mdm')._asdict()

            mc5728vz_http = json_modem_nobuild_resp('MC5728VZ', 'MC5728V', 1, 56, 0, '1.56.0,58016,004.011.000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_E_VZ_1_56_0.mdm')._asdict()
            mc5728vz_https = json_modem_nobuild_resp('MC5728VZ', 'MC5728V', 1, 56, 0, '1.56.0,58016,004.011.000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_E_VZ_1_56_0.mdm')._asdict()

            mc7700at_http = json_modem_nopackage_resp('MC7700AT', 'MC7700', 3, 5, 20, 3, False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP_AT_3_05_20_03.mdm')._asdict()
            mc7700at_https = json_modem_nopackage_resp('MC7700AT', 'MC7700', 3, 5, 20, 3, False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP_AT_3_05_20_03.mdm')._asdict()

            mc7750vz_http = json_modem_nopackage_resp('MC7750VZ', 'MC7750', 3, 5, 10, 13, False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LE_VZ_03_05_10_13.mdm')._asdict()
            mc7750vz_https = json_modem_nopackage_resp('MC7750VZ', 'MC7750', 3, 5, 10, 13, False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LE_VZ_03_05_10_13.mdm')._asdict()

            mc7710_http = json_modem_resp('MC7710', 'MC7710', 3, 5, 29, 3, '03.05.29.03_00_generic_000.000_001', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP2_EU_3_05_29_03.mdm')._asdict()
            mc7710_https = json_modem_resp('MC7710', 'MC7710', 3, 5, 29, 3, '03.05.29.03_00_generic_000.000_001', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP2_EU_3_05_29_03.mdm')._asdict()

            tobyl100_http = json_modem_minversion_resp('TOBYL100', 'TOBY-L100', 1, 0, 5, '5.3.1', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_L_VZ_01_00_05.mdm')._asdict()
            tobyl100_https = json_modem_minversion_resp('TOBYL100', 'TOBY-L100', 1, 0, 5, '5.3.1', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_L_VZ_01_00_05.mdm')._asdict()

            s3_http_modems_resp = [mc7354vz_http, mc7354sp_http, mc7354gn_http, mc7354at_http, mc7455at_http, mc7455gn_http, mc7455sp_http, mc7455vz_http, mc7304eu_http, mc5728vz_http, mc7700at_http, mc7750vz_http, mc7710_http, tobyl100_http]
            s3_https_modems_resp = [mc7354vz_https, mc7354sp_https, mc7354gn_https, mc7354at_https, mc7455at_https, mc7455gn_https, mc7455sp_https, mc7455vz_https, mc7304eu_https, mc5728vz_https, mc7700at_https, mc7750vz_https, mc7710_https, tobyl100_https]

        class S2():
            cba250 = xml_device_resp('CBA250', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250_6_1_2.bin</ES></modem>')._asdict()
            cba750 = xml_device_resp('CBA750', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA750_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA750_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA750_6_1_2.bin</ES></modem>')._asdict()
            ctr500 = xml_device_resp('CTR500', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CTR500_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CTR500_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CTR500_6_1_2.bin</ES></modem>')._asdict()
            cx111 = xml_device_resp('CX111', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CX111_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CX111_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CX111_6_1_2.bin</ES></modem>')._asdict()
            mbr1000 = xml_device_resp('MBR1000', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1000_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1000_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1000_6_1_2.bin</ES></modem>')._asdict()
            mbr1200 = xml_device_resp('MBR1200', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1200_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1200_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1200_6_1_2.bin</ES></modem>')._asdict()
            mbr800 = xml_device_resp('MBR800', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR800_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR800_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR800_6_1_2.bin</ES></modem>')._asdict()
            mbr900 = xml_device_resp('MBR900', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR900_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR900_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR900_6_1_2.bin</ES></modem>')._asdict()
            mbr90 = xml_device_resp('MBR90', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR90_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR90_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR90_6_1_2.bin</ES></modem>')._asdict()
            s2_modems_resp = [cba250, cba750, ctr500, cx111, mbr1000, mbr1200, mbr800, mbr90, mbr900]

        class S1(object):
            phs300 = xml_device_resp('PHS300', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300cc = xml_device_resp('PHS300CC', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300cw = xml_device_resp('PHS300CW', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300s = xml_device_resp('PHS300S', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300tw = xml_device_resp('PHS300TW', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300ic = xml_device_resp('PHS300IC', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            s1_modems_resp = [phs300, phs300cc, phs300cw, phs300s, phs300tw, phs300ic]

        class Redbox(object):
            cba250rb = xml_device_resp('CBA250RB', '<modem><major>5</major><minor>2</minor><patch>216</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250RB_5_2_216.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250RB_5_2_216.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250RB_5_2_216.bin</ES></modem>')._asdict()
            rb_modems_resp = [cba250rb]

    class Routers(object):

        class S3(object):
            json_router_resp = namedtuple('JSON_Router_Response', ['product_name', 'major_version', 'minor_version', 'patch_version', 'over_write', 'eligible', 'url'])

            aer1600_http = json_router_resp("AER1600", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer1600_6_2_2_Release_2016_11_18.bin")._asdict()
            aer1600_https = json_router_resp("AER1600", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer1600_6_2_2_Release_2016_11_18.bin")._asdict()

            aer1650_http = json_router_resp("AER1650", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer1650_6_2_2_Release_2016_11_18.bin")._asdict()
            aer1650_https = json_router_resp("AER1650", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer1650_6_2_2_Release_2016_11_18.bin")._asdict()

            aer2100_http = json_router_resp("2100", 6, 2, 1, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_2100_6_2_1_Release_2016_09_29.bin")._asdict()
            aer2100_https = json_router_resp("2100", 6, 2, 1, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_2100_6_2_1_Release_2016_09_29.bin")._asdict()

            aer3100_http = json_router_resp("AER3100", 6, 2, 1, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer3100_6_2_1_Release_2016_09_29.bin")._asdict()
            aer3100_https = json_router_resp("AER3100", 6, 2, 1, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer3100_6_2_1_Release_2016_09_29.bin")._asdict()

            aer3150_http = json_router_resp("AER3150", 6, 2, 1, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer3150_6_2_1_Release_2016_09_29.bin")._asdict()
            aer3150_https = json_router_resp("AER3150", 6, 2, 1, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer3150_6_2_1_Release_2016_09_29.bin")._asdict()

            cba750b_http = json_router_resp("CBA750B", 6, 2, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750b_6_2_3_Release_2017_01_12.bin")._asdict()
            cba750b_https = json_router_resp("CBA750B", 6, 2, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cba750b_6_2_3_Release_2017_01_12.bin")._asdict()

            cba850_http = json_router_resp("CBA850", 6, 2, 0, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba850_6_2_0_Release_2016_09_15.bin")._asdict()
            cba850_https = json_router_resp("CBA850", 6, 2, 0, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cba850_6_2_0_Release_2016_09_15.bin")._asdict()

            ibr350_http = json_router_resp("IBR350", 6, 2, 0, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr350_6_2_0_Release_2016_09_15.bin")._asdict()
            ibr350_https = json_router_resp("IBR350", 6, 2, 0, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr350_6_2_0_Release_2016_09_15.bin")._asdict()

            ibr600_http = json_router_resp("IBR600", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr600_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr600_https = json_router_resp("IBR600", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr600_6_2_2_Release_2016_11_18.bin")._asdict()

            ibr650_http = json_router_resp("IBR650", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr650_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr650_https = json_router_resp("IBR650", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr650_6_2_2_Release_2016_11_18.bin")._asdict()

            ibr1100_http = json_router_resp("IBR1100", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr1100_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr1100_https = json_router_resp("IBR1100", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr1100_6_2_2_Release_2016_11_18.bin")._asdict()

            ibr1150_http = json_router_resp("IBR1150", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr1150_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr1150_https = json_router_resp("IBR1150", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr1150_6_2_2_Release_2016_11_18.bin")._asdict()

            mbr1200b_http = json_router_resp("MBR1200B", 6, 1, 0, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200b_6_1_0_Release_2016_03_18.bin")._asdict()
            mbr1200b_https = json_router_resp("MBR1200B", 6, 1, 0, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr1200b_6_1_0_Release_2016_03_18.bin")._asdict()

            mbr1400_http = json_router_resp("MBR1400", 6, 2, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1400_6_2_3_Release_2017_01_12.bin")._asdict()
            mbr1400_https = json_router_resp("MBR1400", 6, 2, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr1400_6_2_3_Release_2017_01_12.bin")._asdict()

            mbr1400v2_http = json_router_resp("MBR1400V2", 6, 2, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1400v2_6_2_3_Release_2017_01_12.bin")._asdict()
            mbr1400v2_https = json_router_resp("MBR1400V2", 6, 2, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr1400v2_6_2_3_Release_2017_01_12.bin")._asdict()

            cbr400_http = json_router_resp("CBR400", 4, 3, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cbr400_4_3_3_Release_2014_04_09.bin")._asdict()
            cbr400_https = json_router_resp("CBR400", 4, 3, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cbr400_4_3_3_Release_2014_04_09.bin")._asdict()

            cbr450_http = json_router_resp("CBR450", 4, 3, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cbr450_4_3_3_Release_2014_04_09.bin")._asdict()
            cbr450_https = json_router_resp("CBR450", 4, 3, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cbr450_4_3_3_Release_2014_04_09.bin")._asdict()

            ctr35_http = json_router_resp("CTR35", 3, 6, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ctr35_3_6_3_Release_2012_06_22.bin")._asdict()
            ctr35_https = json_router_resp("CTR35", 3, 6, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ctr35_3_6_3_Release_2012_06_22.bin")._asdict()

            mbr95_http = json_router_resp("MBR95", 5, 0, 4, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr95_5_0_4_Release_2014_04_09.bin")._asdict()
            mbr95_https = json_router_resp("MBR95", 5, 0, 4, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr95_5_0_4_Release_2014_04_09.bin")._asdict()

            s3_http_routers_resp = [aer1600_http, aer1650_http, aer2100_http, aer3100_http, aer3150_http, cba750b_http, cba850_http, ibr350_http, ibr600_http, ibr650_http, ibr1100_http, ibr1150_http, mbr1200b_http, mbr1400_http, mbr1400v2_http, cbr400_http, cbr450_http, ctr35_http, mbr95_http]
            s3_https_routers_resp = [aer1600_https, aer1650_https, aer2100_https, aer3100_https, aer3150_https, cba750b_https, cba850_https, ibr350_https, ibr600_https, ibr650_https, ibr1100_https, ibr1150_https, mbr1200b_https, mbr1400_https, mbr1400v2_https, cbr400_https, cbr450_https, ctr35_https, mbr95_https]

        class S2(object):
            cba250 = xml_device_resp('CBA250', '<crkipt><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_2_0_0_Release_2012_04_16.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_2_0_0_Release_2012_04_16.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_2_0_0_Release_2012_04_16.bin</ES></crkipt>')._asdict()
            cba750 = xml_device_resp('CBA750', '<cba750><major>2</major><minor>4</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750_2_4_0_Release_2014_04_01.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750_2_4_0_Release_2014_04_01.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750_2_4_0_Release_2014_04_01.bin</ES></cba750>')._asdict()
            ctr500 = xml_device_resp('CTR500', '<ctr500><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crk_2_0_0_Release_2012_04_16.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crk_2_0_0_Release_2012_04_16.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crk_2_0_0_Release_2012_04_16.bin</ES></ctr500>')._asdict()
            cx111 = xml_device_resp('CX111', '<cx111><major>2</major><minor>2</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cx111_2_2_2_Release_2013_07_18.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cx111_2_2_2_Release_2013_07_18.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cx111_2_2_2_Release_2013_07_18.bin</ES></cx111>')._asdict()
            mbr1000 = xml_device_resp('MBR1000', '<mbr1000><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr_2_0_0_Release_2012_04_16.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr_2_0_0_Release_2012_04_16.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr_2_0_0_Release_2012_04_16.bin</EN></mbr1000>')._asdict()
            mbr1200 = xml_device_resp('MBR1200', '<mbr1200><major>2</major><minor>2</minor><patch>1</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200_2_2_1_Release_2013_03_18.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200_2_2_1_Release_2013_03_18.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200_2_2_1_Release_2013_03_18.bin</EN></mbr1200>')._asdict()
            mbr800 = xml_device_resp('MBR800', '<htsd><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_hstd_2_0_0_Release_2012_04_16.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_hstd_2_0_0_Release_2012_04_16.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_hstd_2_0_0_Release_2012_04_16.bin</ES></htsd>')._asdict()
            mbr90 = xml_device_resp('MBR90', '<mbr90><force>0</force><eligible>1</eligible><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr90_2_0_0_Release_2012_04_16.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr90_2_0_0_Release_2012_04_16.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr90_2_0_0_Release_2012_04_16.bin</EN></mbr90>')._asdict()
            mbr900 = xml_device_resp('MBR900', '<mbr900><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr900_2_0_0_Release_2012_04_16.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr900_2_0_0_Release_2012_04_16.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr900_2_0_0_Release_2012_04_16.bin</EN></mbr900>')._asdict()
            s2_routers_resp = [cba250, cba750, ctr500, cx111, mbr1000, mbr1200, mbr800, mbr90, mbr900]

        class S1(object):
            phs300cc = xml_device_resp('PHS300CC', '<ccphs><major>2</major><minor>5</minor><patch>4</patch><url>http://ftp.cradlepoint.com/download/PHS300CC/u_ccphs_2010_11_09.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300CC/u_ccphs_2010_11_09.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300CC/u_ccphs_2010_11_09.bin</ES></ccphs>')._asdict()
            phs300cw = xml_device_resp('PHS300CW', '<cphs><major>2</major><minor>5</minor><patch>3</patch><EN>http://ftp.cradlepoint.com/download/PHS300CW/u_cphs_2010_05_28.bin</EN><url>http://ftp.cradlepoint.com/download/PHS300CW/u_cphs_2010_05_28.bin</url></cphs>')._asdict()
            phs300ic = xml_device_resp('PHS300IC', '<phs><major>2</major><minor>6</minor><patch>2</patch><url>http://ftp.cradlepoint.com/download/PHS300IC/u_icphs_2011_06_08.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300IC/u_icphs_2011_06_08.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300IC/u_icphs_2011_06_08.bin</ES></phs>')._asdict()
            phs300tw = xml_device_resp('PHS300TW', '<twphs><major>2</major><minor>5</minor><patch>4</patch><url>http://ftp.cradlepoint.com/download/PHS300TW/u_tphs_2010_11_09.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300TW/u_tphs_2010_11_09.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300TW/u_tphs_2010_11_09.bin</ES></twphs>')._asdict()
            phs300 = xml_device_resp('PHS300', '<phs><major>2</major><minor>6</minor><patch>1</patch><url>http://ftp.cradlepoint.com/download/PHS300/u_phs_2011_05_11.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300/u_phs_2011_05_11.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300/u_phs_2011_05_11.bin</ES></phs>')._asdict()
            ctr350 = xml_device_resp('CTR350', '<ctr><major>2</major><minor>5</minor><patch>5</patch><url>http://ftp.cradlepoint.com/download/CTR350/u_ctr_2010_12_07.bin</url><EN>http://ftp.cradlepoint.com/download/CTR350/u_ctr_2010_12_07.bin</EN><ES>http://ftp.cradlepoint.com/download/CTR350/u_ctr_2010_12_07.bin</ES></ctr>')._asdict()
            s1_routers_resp = [phs300, phs300cc, phs300cw, phs300ic, phs300tw, ctr350]

        class Redbox(object):
            redbox_s3_routers_resp = namedtuple('Redbox_JSON_Response', ['product_name', 'major_version', 'minor_version', 'patch_version', 'eligible', 'url'])

            cbr450rb = redbox_s3_routers_resp('CBR450RB', 4, 4, 6, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cbr450rb_4_4_6_Release_2014_04_09.bin")._asdict()
            ibr600rb = redbox_s3_routers_resp('IBR600RB', 4, 4, 6, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr600rb_4_4_6_Release_2014_04_09.bin")._asdict()
            ibr650rb = redbox_s3_routers_resp('IBR650RB', 4, 4, 6, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr650rb_4_4_6_Release_2014_04_09.bin")._asdict()
            cba250rb = xml_device_resp('CBA250RB', '<crkipt><major>1</major><minor>8</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_1_8_2_Release_2011_05_20.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_1_8_2_Release_2011_05_20.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_1_8_2_Release_2011_05_20.bin</ES></crkipt>')._asdict()

            s3_redbox_resp = [cbr450rb, ibr600rb, ibr650rb]
            s2_redbox_resp = [cba250rb]


# ------------------------------------------
# QA Device Definitions
# ------------------------------------------
class DeviceResponseMasterListQA(object):
    # Note: Device Responses left in JSON or XML form, regardless of redundancy, for readability and ease of modification.

    class Modems(object):

        class S3(object):
            json_modem_resp = namedtuple('JSON_Modem_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'build_version', 'package_version', 'over_write', 'eligible', 'url'])
            json_modem_nobuild_resp = namedtuple('JSON_Modem_Build_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'package_version', 'over_write', 'eligible', 'url'])
            json_modem_nopackage_resp = namedtuple('JSON_Modem_Build_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'build_version', 'over_write', 'eligible', 'url'])
            json_modem_minversion_resp = namedtuple('JSON_Modem_Build_Response', ['key', 'product_name', 'major_version', 'minor_version', 'patch_version', 'min_router_version', 'over_write', 'eligible', 'url'])

            mc7354vz_http = json_modem_resp('MC7354VZ', 'MC7354-CP', 5, 5, 58, 1, '05.05.58.01_VZW,005.030_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_VZ_5_05_58_01_30.mdm')._asdict()
            mc7354vz_https = json_modem_resp('MC7354VZ', 'MC7354-CP', 5, 5, 58, 1, '05.05.58.01_VZW,005.030_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_VZ_5_05_58_01_30.mdm')._asdict()

            mc7354sp_http = json_modem_resp('MC7354SP', 'MC7354-CP', 5, 5, 63, 1, '05.05.63.01_SPRINT,005.035_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_SP_5_05_63_01.mdm')._asdict()
            mc7354sp_https = json_modem_resp('MC7354SP', 'MC7354-CP', 5, 5, 63, 1, '05.05.63.01_SPRINT,005.035_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_SP_5_05_63_01.mdm')._asdict()

            mc7354gn_http = json_modem_resp('MC7354GN', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_GENNA-UMTS,005.025_002', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_GN_eHRPD_05_05_58_00.mdm')._asdict()
            mc7354gn_https = json_modem_resp('MC7354GN', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_GENNA-UMTS,005.025_002', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_GN_eHRPD_05_05_58_00.mdm')._asdict()

            mc7354at_http = json_modem_resp('MC7354AT', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_ATT,005.026_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LPE_AT_5_05_58_00.mdm')._asdict()
            mc7354at_https = json_modem_resp('MC7354AT', 'MC7354-CP', 5, 5, 58, 0, '05.05.58.00_ATT,005.026_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LPE_AT_5_05_58_00.mdm')._asdict()

            mc7455at_http = json_modem_resp('MC7455AT', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_ATT,002.020_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_AT_02_20_03_00.mdm')._asdict()
            mc7455at_https = json_modem_resp('MC7455AT', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_ATT,002.020_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_AT_02_20_03_00.mdm')._asdict()

            mc7455gn_http = json_modem_resp('MC7455GN', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_GENERIC,002.017_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_GN_02_20_03_00.mdm')._asdict()
            mc7455gn_https = json_modem_resp('MC7455GN', 'MC7455-CP', 2, 20, 3, 0, '02.20.03.00_GENERIC,002.017_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_GN_02_20_03_00.mdm')._asdict()

            mc7455sp_http = json_modem_resp('MC7455SP', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_SPRINT,002.020_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_SP_02_20_03_22.mdm')._asdict()
            mc7455sp_https = json_modem_resp('MC7455SP', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_SPRINT,002.020_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_SP_02_20_03_22.mdm')._asdict()

            mc7455vz_http = json_modem_resp('MC7455VZ', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_VERIZON,002.026_001', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP6_VZ_02_20_03_22.mdm')._asdict()
            mc7455vz_https = json_modem_resp('MC7455VZ', 'MC7455-CP', 2, 20, 3, 22, '02.20.03.22_VERIZON,002.026_001', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP6_VZ_02_20_03_22.mdm')._asdict()

            mc7304eu_http = json_modem_resp('MC7304EU', 'MC7304-CP', 5, 5, 58, 0, '05.05.58.00_GENEU-4G,005.027_000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP3_EU_5_05_58_00_27.mdm')._asdict()
            mc7304eu_https = json_modem_resp('MC7304EU', 'MC7304-CP', 5, 5, 58, 0, '05.05.58.00_GENEU-4G,005.027_000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP3_EU_5_05_58_00_27.mdm')._asdict()

            mc5728vz_http = json_modem_nobuild_resp('MC5728VZ', 'MC5728V', 1, 56, 0, '1.56.0,58016,004.011.000', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_E_VZ_1_56_0.mdm')._asdict()
            mc5728vz_https = json_modem_nobuild_resp('MC5728VZ', 'MC5728V', 1, 56, 0, '1.56.0,58016,004.011.000', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_E_VZ_1_56_0.mdm')._asdict()

            mc7700at_http = json_modem_nopackage_resp('MC7700AT', 'MC7700', 3, 5, 20, 3, False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP_AT_3_05_20_03.mdm')._asdict()
            mc7700at_https = json_modem_nopackage_resp('MC7700AT', 'MC7700', 3, 5, 20, 3, False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP_AT_3_05_20_03.mdm')._asdict()

            mc7750vz_http = json_modem_nopackage_resp('MC7750VZ', 'MC7750', 3, 5, 10, 13, False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LE_VZ_03_05_10_13.mdm')._asdict()
            mc7750vz_https = json_modem_nopackage_resp('MC7750VZ', 'MC7750', 3, 5, 10, 13, False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LE_VZ_03_05_10_13.mdm')._asdict()

            mc7710_http = json_modem_resp('MC7710', 'MC7710', 3, 5, 29, 3, '03.05.29.03_00_generic_000.000_001', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_LP2_EU_3_05_29_03.mdm')._asdict()
            mc7710_https = json_modem_resp('MC7710', 'MC7710', 3, 5, 29, 3, '03.05.29.03_00_generic_000.000_001', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_LP2_EU_3_05_29_03.mdm')._asdict()

            tobyl100_http = json_modem_minversion_resp('TOBYL100', 'TOBY-L100', 1, 0, 5, '5.3.1', False, True, 'http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/m_L_VZ_01_00_05.mdm')._asdict()
            tobyl100_https = json_modem_minversion_resp('TOBYL100', 'TOBY-L100', 1, 0, 5, '5.3.1', False, True, 'https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/m_L_VZ_01_00_05.mdm')._asdict()

            s3_http_modems_resp = [mc7354vz_http, mc7354sp_http, mc7354gn_http, mc7354at_http, mc7455at_http, mc7455gn_http, mc7455sp_http, mc7455vz_http, mc7304eu_http, mc5728vz_http, mc7700at_http, mc7750vz_http, mc7710_http, tobyl100_http]
            s3_https_modems_resp = [mc7354vz_https, mc7354sp_https, mc7354gn_https, mc7354at_https, mc7455at_https, mc7455gn_https, mc7455sp_https, mc7455vz_https, mc7304eu_https, mc5728vz_https, mc7700at_https, mc7750vz_https, mc7710_https, tobyl100_https]

        class S2(object):
            cba250 = xml_device_resp('CBA250', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250_6_1_2.bin</ES></modem>')._asdict()
            cba750 = xml_device_resp('CBA750', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA750_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA750_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA750_6_1_2.bin</ES></modem>')._asdict()
            ctr500 = xml_device_resp('CTR500', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CTR500_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CTR500_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CTR500_6_1_2.bin</ES></modem>')._asdict()
            cx111 = xml_device_resp('CX111', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CX111_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CX111_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CX111_6_1_2.bin</ES></modem>')._asdict()
            mbr1000 = xml_device_resp('MBR1000', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1000_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1000_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1000_6_1_2.bin</ES></modem>')._asdict()
            mbr1200 = xml_device_resp('MBR1200', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1200_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1200_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR1200_6_1_2.bin</ES></modem>')._asdict()
            mbr800 = xml_device_resp('MBR800', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR800_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR800_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR800_6_1_2.bin</ES></modem>')._asdict()
            mbr900 = xml_device_resp('MBR900', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR900_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR900_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR900_6_1_2.bin</ES></modem>')._asdict()
            mbr90 = xml_device_resp('MBR90', '<modem><major>6</major><minor>1</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR90_6_1_2.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR90_6_1_2.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_MBR90_6_1_2.bin</ES></modem>')._asdict()
            s2_modems_resp = [cba250, cba750, ctr500, cx111, mbr1000, mbr1200, mbr800, mbr90, mbr900]

        class S1(object):
            phs300 = xml_device_resp('PHS300', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300cc = xml_device_resp('PHS300CC', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300cw = xml_device_resp('PHS300CW', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300s = xml_device_resp('PHS300S', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300tw = xml_device_resp('PHS300TW', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            phs300ic = xml_device_resp('PHS300IC', '<modem><major>5</major><minor>2</minor><patch>226</patch><url>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</url><EN>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</EN><ES>http://ftp.cradlepoint.com/download/MODEM/u_modem_5_2_226.bin</ES></modem>')._asdict()
            s1_modems_resp = [phs300, phs300cc, phs300cw, phs300s, phs300tw, phs300ic]

        class Redbox(object):
            cba250rb = xml_device_resp('CBA250RB', '<modem><major>5</major><minor>2</minor><patch>216</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250RB_5_2_216.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250RB_5_2_216.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_modem_CBA250RB_5_2_216.bin</ES></modem>')._asdict()
            rb_modems_resp = [cba250rb]

    class Routers(object):

        class S3(object):
            json_router_resp = namedtuple('JSON_Router_Response', ['product_name', 'major_version', 'minor_version', 'patch_version', 'over_write', 'eligible', 'url'])

            aer1600_http = json_router_resp("AER1600", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer1600_6_2_2_Release_2016_11_18.bin")._asdict()
            aer1600_https = json_router_resp("AER1600", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer1600_6_2_2_Release_2016_11_18.bin")._asdict()

            aer1650_http = json_router_resp("AER1650", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer1650_6_2_2_Release_2016_11_18.bin")._asdict()
            aer1650_https = json_router_resp("AER1650", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer1650_6_2_2_Release_2016_11_18.bin")._asdict()

            aer2100_http = json_router_resp("2100", 6, 2, 1, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_2100_6_2_1_Release_2016_09_29.bin")._asdict()
            aer2100_https = json_router_resp("2100", 6, 2, 1, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_2100_6_2_1_Release_2016_09_29.bin")._asdict()

            aer3100_http = json_router_resp("AER3100", 6, 2, 1, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer3100_6_2_1_Release_2016_09_29.bin")._asdict()
            aer3100_https = json_router_resp("AER3100", 6, 2, 1, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer3100_6_2_1_Release_2016_09_29.bin")._asdict()

            aer3150_http = json_router_resp("AER3150", 6, 2, 1, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_aer3150_6_2_1_Release_2016_09_29.bin")._asdict()
            aer3150_https = json_router_resp("AER3150", 6, 2, 1, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_aer3150_6_2_1_Release_2016_09_29.bin")._asdict()

            cba750b_http = json_router_resp("CBA750B", 6, 2, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750b_6_2_3_Release_2017_01_12.bin")._asdict()
            cba750b_https = json_router_resp("CBA750B", 6, 2, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cba750b_6_2_3_Release_2017_01_12.bin")._asdict()

            cba850_http = json_router_resp("CBA850", 6, 2, 0, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba850_6_2_0_Release_2016_09_15.bin")._asdict()
            cba850_https = json_router_resp("CBA850", 6, 2, 0, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cba850_6_2_0_Release_2016_09_15.bin")._asdict()

            ibr350_http = json_router_resp("IBR350", 6, 2, 0, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr350_6_2_0_Release_2016_09_15.bin")._asdict()
            ibr350_https = json_router_resp("IBR350", 6, 2, 0, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr350_6_2_0_Release_2016_09_15.bin")._asdict()

            ibr600_http = json_router_resp("IBR600", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr600_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr600_https = json_router_resp("IBR600", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr600_6_2_2_Release_2016_11_18.bin")._asdict()

            ibr650_http = json_router_resp("IBR650", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr650_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr650_https = json_router_resp("IBR650", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr650_6_2_2_Release_2016_11_18.bin")._asdict()

            ibr1100_http = json_router_resp("IBR1100", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr1100_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr1100_https = json_router_resp("IBR1100", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr1100_6_2_2_Release_2016_11_18.bin")._asdict()

            ibr1150_http = json_router_resp("IBR1150", 6, 2, 2, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr1150_6_2_2_Release_2016_11_18.bin")._asdict()
            ibr1150_https = json_router_resp("IBR1150", 6, 2, 2, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ibr1150_6_2_2_Release_2016_11_18.bin")._asdict()

            mbr1200b_http = json_router_resp("MBR1200B", 6, 1, 0, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200b_6_1_0_Release_2016_03_18.bin")._asdict()
            mbr1200b_https = json_router_resp("MBR1200B", 6, 1, 0, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr1200b_6_1_0_Release_2016_03_18.bin")._asdict()

            mbr1400_http = json_router_resp("MBR1400", 6, 2, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1400_6_2_3_Release_2017_01_12.bin")._asdict()
            mbr1400_https = json_router_resp("MBR1400", 6, 2, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr1400_6_2_3_Release_2017_01_12.bin")._asdict()

            mbr1400v2_http = json_router_resp("MBR1400V2", 6, 2, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1400v2_6_2_3_Release_2017_01_12.bin")._asdict()
            mbr1400v2_https = json_router_resp("MBR1400V2", 6, 2, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr1400v2_6_2_3_Release_2017_01_12.bin")._asdict()

            cbr400_http = json_router_resp("CBR400", 4, 3, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cbr400_4_3_3_Release_2014_04_09.bin")._asdict()
            cbr400_https = json_router_resp("CBR400", 4, 3, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cbr400_4_3_3_Release_2014_04_09.bin")._asdict()

            cbr450_http = json_router_resp("CBR450", 4, 3, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cbr450_4_3_3_Release_2014_04_09.bin")._asdict()
            cbr450_https = json_router_resp("CBR450", 4, 3, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_cbr450_4_3_3_Release_2014_04_09.bin")._asdict()

            ctr35_http = json_router_resp("CTR35", 3, 6, 3, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ctr35_3_6_3_Release_2012_06_22.bin")._asdict()
            ctr35_https = json_router_resp("CTR35", 3, 6, 3, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_ctr35_3_6_3_Release_2012_06_22.bin")._asdict()

            mbr95_http = json_router_resp("MBR95", 5, 0, 4, False, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr95_5_0_4_Release_2014_04_09.bin")._asdict()
            mbr95_https = json_router_resp("MBR95", 5, 0, 4, False, True, "https://31f97bc708990d18e24a-5aa8c563aaca2fd601559335cd0de9e6.ssl.cf2.rackcdn.com/u_mbr95_5_0_4_Release_2014_04_09.bin")._asdict()

            s3_http_routers_resp = [aer1600_http, aer1650_http, aer2100_http, aer3100_http, aer3150_http, cba750b_http, cba850_http, ibr350_http, ibr600_http, ibr650_http, ibr1100_http, ibr1150_http, mbr1200b_http, mbr1400_http, mbr1400v2_http, cbr400_http, cbr450_http, ctr35_http, mbr95_http]
            s3_https_routers_resp = [aer1600_https, aer1650_https, aer2100_https, aer3100_https, aer3150_https, cba750b_https, cba850_https, ibr350_https, ibr600_https, ibr650_https, ibr1100_https, ibr1150_https, mbr1200b_https, mbr1400_https, mbr1400v2_https, cbr400_https, cbr450_https, ctr35_https, mbr95_https]

        class S2(object):
            cba250 = xml_device_resp('CBA250', '<crkipt><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_2_0_0_Release_2012_04_16.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_2_0_0_Release_2012_04_16.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_2_0_0_Release_2012_04_16.bin</ES></crkipt>')._asdict()
            cba750 = xml_device_resp('CBA750', '<cba750><major>2</major><minor>4</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750_2_4_0_Release_2014_04_01.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750_2_4_0_Release_2014_04_01.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cba750_2_4_0_Release_2014_04_01.bin</ES></cba750>')._asdict()
            ctr500 = xml_device_resp('CTR500', '<ctr500><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crk_2_0_0_Release_2012_04_16.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crk_2_0_0_Release_2012_04_16.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crk_2_0_0_Release_2012_04_16.bin</ES></ctr500>')._asdict()
            cx111 = xml_device_resp('CX111', '<cx111><major>2</major><minor>2</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cx111_2_2_2_Release_2013_07_18.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cx111_2_2_2_Release_2013_07_18.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cx111_2_2_2_Release_2013_07_18.bin</ES></cx111>')._asdict()
            mbr1000 = xml_device_resp('MBR1000', '<mbr1000><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr_2_0_0_Release_2012_04_16.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr_2_0_0_Release_2012_04_16.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr_2_0_0_Release_2012_04_16.bin</EN></mbr1000>')._asdict()
            mbr1200 = xml_device_resp('MBR1200', '<mbr1200><major>2</major><minor>2</minor><patch>1</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200_2_2_1_Release_2013_03_18.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200_2_2_1_Release_2013_03_18.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr1200_2_2_1_Release_2013_03_18.bin</EN></mbr1200>')._asdict()
            mbr800 = xml_device_resp('MBR800', '<htsd><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_hstd_2_0_0_Release_2012_04_16.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_hstd_2_0_0_Release_2012_04_16.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_hstd_2_0_0_Release_2012_04_16.bin</ES></htsd>')._asdict()
            mbr90 = xml_device_resp('MBR90', '<mbr90><force>0</force><eligible>1</eligible><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr90_2_0_0_Release_2012_04_16.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr90_2_0_0_Release_2012_04_16.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr90_2_0_0_Release_2012_04_16.bin</EN></mbr90>')._asdict()
            mbr900 = xml_device_resp('MBR900', '<mbr900><major>2</major><minor>0</minor><patch>0</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr900_2_0_0_Release_2012_04_16.bin</url><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr900_2_0_0_Release_2012_04_16.bin</ES><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_mbr900_2_0_0_Release_2012_04_16.bin</EN></mbr900>')._asdict()
            s2_routers_resp = [cba250, cba750, ctr500, cx111, mbr1000, mbr1200, mbr800, mbr90, mbr900]

        class S1(object):
            phs300cc = xml_device_resp('PHS300CC', '<ccphs><major>2</major><minor>5</minor><patch>4</patch><url>http://ftp.cradlepoint.com/download/PHS300CC/u_ccphs_2010_11_09.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300CC/u_ccphs_2010_11_09.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300CC/u_ccphs_2010_11_09.bin</ES></ccphs>')._asdict()
            phs300cw = xml_device_resp('PHS300CW', '<cphs><major>2</major><minor>5</minor><patch>3</patch><EN>http://ftp.cradlepoint.com/download/PHS300CW/u_cphs_2010_05_28.bin</EN><url>http://ftp.cradlepoint.com/download/PHS300CW/u_cphs_2010_05_28.bin</url></cphs>')._asdict()
            phs300ic = xml_device_resp('PHS300IC', '<phs><major>2</major><minor>6</minor><patch>2</patch><url>http://ftp.cradlepoint.com/download/PHS300IC/u_icphs_2011_06_08.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300IC/u_icphs_2011_06_08.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300IC/u_icphs_2011_06_08.bin</ES></phs>')._asdict()
            phs300tw = xml_device_resp('PHS300TW', '<twphs><major>2</major><minor>5</minor><patch>4</patch><url>http://ftp.cradlepoint.com/download/PHS300TW/u_tphs_2010_11_09.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300TW/u_tphs_2010_11_09.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300TW/u_tphs_2010_11_09.bin</ES></twphs>')._asdict()
            phs300 = xml_device_resp('PHS300', '<phs><major>2</major><minor>6</minor><patch>1</patch><url>http://ftp.cradlepoint.com/download/PHS300/u_phs_2011_05_11.bin</url><EN>http://ftp.cradlepoint.com/download/PHS300/u_phs_2011_05_11.bin</EN><ES>http://ftp.cradlepoint.com/download/PHS300/u_phs_2011_05_11.bin</ES></phs>')._asdict()
            ctr350 = xml_device_resp('CTR350', '<ctr><major>2</major><minor>5</minor><patch>5</patch><url>http://ftp.cradlepoint.com/download/CTR350/u_ctr_2010_12_07.bin</url><EN>http://ftp.cradlepoint.com/download/CTR350/u_ctr_2010_12_07.bin</EN><ES>http://ftp.cradlepoint.com/download/CTR350/u_ctr_2010_12_07.bin</ES></ctr>')._asdict()
            s1_routers_resp = [phs300, phs300cc, phs300cw, phs300ic, phs300tw, ctr350]

        class Redbox(object):
            redbox_s3_routers_resp = namedtuple('Redbox_JSON_Response', ['product_name', 'major_version', 'minor_version', 'patch_version', 'eligible', 'url'])

            cbr450rb = redbox_s3_routers_resp('CBR450RB', 4, 4, 6, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_cbr450rb_4_4_6_Release_2014_04_09.bin")._asdict()
            ibr600rb = redbox_s3_routers_resp('IBR600RB', 4, 4, 6, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr600rb_4_4_6_Release_2014_04_09.bin")._asdict()
            ibr650rb = redbox_s3_routers_resp('IBR650RB', 4, 4, 6, True, "http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_ibr650rb_4_4_6_Release_2014_04_09.bin")._asdict()
            cba250rb = xml_device_resp('CBA250RB', '<crkipt><major>1</major><minor>8</minor><patch>2</patch><url>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_1_8_2_Release_2011_05_20.bin</url><EN>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_1_8_2_Release_2011_05_20.bin</EN><ES>http://1746ac1e3e4f982e6b23-5aa8c563aaca2fd601559335cd0de9e6.r74.cf2.rackcdn.com/u_crkipt_1_8_2_Release_2011_05_20.bin</ES></crkipt>')._asdict()

            s3_redbox_resp = [cbr450rb, ibr600rb, ibr650rb]
            s2_redbox_resp = [cba250rb]

    class Application(object):
        pass
