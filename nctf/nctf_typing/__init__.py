"""
    isort:skip_file
"""
from sys import platform
# fixtures
from nctf.test_lib.accounts.rest.fixture import AccServRESTFixture
from nctf.test_lib.base.fixtures.fixture import FixtureBase

from nctf.test_lib.core.arm2.interface import ARMException
from nctf.test_lib.core.arm2.interface import ARMInterface
from nctf.test_lib.core.arm4 import ARMv4Fixture
from nctf.test_lib.core.arm4.fixture import ARMv4Exception
from nctf.test_lib.core.asset_handler import AssetHandlerFixture
from nctf.test_lib.core.config.fixture import ConfigFixture
from nctf.test_lib.core.email.fixture import EmailFixture
from nctf.test_lib.ncos.base.client import NCOSClientError
from nctf.test_lib.ncos.base.client import NCOSNetworkClient
from nctf.test_lib.ncos.base.client import NCOSLANClient
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError
from nctf.test_lib.ncos.base.firmware_info import NCOSFirmwareInfo
from nctf.test_lib.ncos.base.router import NCOSRouter
from nctf.test_lib.ncos.base.router import NCOSRouterLog
from nctf.test_lib.ncos.base.network import NCOSNetwork
from nctf.test_lib.ncos.base.network import NCOSNetworkProvider
from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import FirmwareLoaderFixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import ArtifactoryFwGetter
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import LocalFwGetter
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPIFixture
from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture
from nctf.test_lib.ncm.fixtures.stream_client.fixture import StreamClientFixture
from nctf.test_lib.netcloud.fixtures.netcloud_account.fixture import NetCloudAccountFixture
from nctf.test_lib.netcloud.fixtures.netcloud_ui.fixture import NetCloudUIFixture
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTClient
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture
from nctf.test_lib.portal.fixtures.fixture import PartnerPortalUIFixture
from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.utils.waiter import wait
from nctf.test_lib.base.pages import *
import nctf.test_lib.accounts.pages as accts_pages
import nctf.test_lib.als.pages as als_pages
import nctf.test_lib.ncm.pages as ncm_pages
import nctf.test_lib.ncp.pages as ncp_pages
import nctf.test_lib.sockeye.pages as sockeye_pages
from nctf.test_lib.netcloud.fixtures.netcloud_router.fixture import NetCloudRouterFixture
from nctf.test_lib.netcloud.fixtures.netcloud_router.fixture import NetCloudRouterFixtureException
from nctf.test_lib.netcloud.fixtures.netcloud_router import NetCloudRouter
from nctf.test_lib.core.arm2_redo.interface import ARMInterfaceFake

# virtnetwork is only compatible with Linux (due to KVM+QEMU). Windows and macOS should not import this code.
if platform == "linux":
    from nctf.test_lib.ncos.fixtures.virtnetwork.fixture import VirtRouterConsole
    from nctf.test_lib.ncos.fixtures.virtnetwork.fixture import VirtualNetwork
# We'll assign the names to None to prevent test breakage on type-hints when on non-linux platforms.
else:
    VirtRouterConsole = None
    VirtualNetwork = None

# meta fixtures
from nctf.test_lib.meta.fixtures import WifiMetaFixture

# useful classes
from nctf.test_lib.netcloud.fixtures.netcloud_account import NetCloudActor, AcceptTOSMethod, ActivationMethod, Roles
from nctf.test_lib.netcloud.fixtures.netcloud_account import NetCloudAccount
