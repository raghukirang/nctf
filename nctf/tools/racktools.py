import argparse
import datetime
import os
import pwd
import requests
import socket
import time
import urllib3

# for the imports below to work from the command line, we need this.
import sys
sys.path.append("./")

from arm4.client import ARMv4Client
from arm4.serialization.cloud_routers import CloudRouter
from arm4.serialization.routers import RouterControl
from nctf.test_lib.core.arm4.fixture import ARMv4LanClient
from nctf.test_lib.core.arm4.fixture import ARMv4Router
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI

# we don't care about warnings that router is using a self-signed certificate
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class ARMv4RackTools:
    def __init__(self,
                 api_username: str,
                 api_key: str,
                 api_host: str,
                 max_lease_seconds: int = 1800,
                 api_port: int = 80,
                 api_version: str = 'v4'):
        self.api_username = api_username
        self.api_key = api_key
        self.api_host = api_host
        self.api_port = api_port
        self.api_version = api_version
        self.max_lease_delta = datetime.timedelta(seconds=max_lease_seconds)
        self.client = ARMv4Client(self.api_host, self.api_key, self.api_port)
        self.logger = None
        self.router_controllers = None

    def get_router_detail(self, thin_router):
        if isinstance(thin_router, CloudRouter):
            raw_router = self.client.cloud_routers.detail(id_=thin_router.id)
        else:
            raw_router = self.client.routers.detail(id_=thin_router .id)
        return ARMv4Router(router_data=raw_router, fw_getter=None)

    def get_routers(self, pool_id: str, router_id: str, include_cloud: bool, include_physical: bool) -> list:
        routers = []
        if include_physical:
            routers.extend([x for x in self.client.routers.list(pool_id=pool_id)
                            if (router_id and x.id == router_id or not router_id)])
        if include_cloud:
            routers.extend([x for x in self.client.cloud_routers.list(pool_id=pool_id)
                            if (router_id and x.id == router_id or not router_id)])
        return routers

    def fetch_router_controllers(self):
        """ARM4 does not implement a client API for this because of the potential for abuse.  For router management
        purposes, we really need this information conveniently located sometimes, so go fetch it via the web API"""

        target = self.api_host + ":" + str(self.api_port) + "/api/v4/router_controls"
        params = dict()
        params['api_key'] = None  # self.api_key
        response = requests.get(target, params=params)
        if response.status_code == 200:
            json = response.json()
            self.router_controllers = dict()
            for x in json["data"]:
                rc = RouterControl(**x)
                self.router_controllers[rc.router_id] = ARMv4LanClient(rc)

    def get_router_control(self, router_id: str) -> ARMv4LanClient:
        if not self.router_controllers:
            self.fetch_router_controllers()

        result = self.router_controllers.get(router_id, None) if self.router_controllers else None
        return result

    @staticmethod
    def get_firmware_via_lan(router, router_control):
        fw_info = router_control.router_get_fw_info(router=router)
        return fw_info

    def dump_firmware(self, routers: list, router_id: str, pool_id: str):
        print(f"Routers {router_id} in pool {pool_id} at {self.api_host}")
        print("id, \t model, \thealthy, \tclean, \tlease_id, \tversion, \ttype, \t sha, \tdate")
        for thin_router in routers:
            router = self.get_router_detail(thin_router)
            try:
                # print(f"{router.router_id}.get_fw_info")
                if router.get_router_capabilities().default_lan_only:
                    fw_info = self.get_firmware_via_lan(router, self.get_router_control(router.router_id))
                else:
                    fw_info = router.get_fw_info()
            except OSError as exc:
                fw_info = str(exc)
            except urllib3.exceptions.MaxRetryError as mre:
                fw_info = str(mre)
            except requests.exceptions.ConnectionError as ce:
                fw_info = str(ce)
            except NotImplementedError as nie:
                fw_info = str(nie)

            print(f"{router.router_id}, \t{router.capability_id}, \t{router.data.healthy}, \t{router.data.clean}, \t"
                  f"{router.data.lease_id}, \t{fw_info}")

    def get_info(self, routers: list, router_id: str, pool_id: str = ""):
        # intent is that this can give a script or user everything they need to use a router.
        # wan ip, lan ip, client ip, router user/password, http port, https port, ssh port, client user, client pass
        # client ip, client ssh port, and maybe more.
        # Could also be expanded translated into "open router shell" or "open shell on client"
        print(f"Routers {router_id} in pool {pool_id} at {self.api_host}")
        print("id,\t model,\t admin_user,\t admin_pass,\t cp_root_pass,\t ip,\t web port,\t ssh port")
        for thin_router in routers:
            if (router_id and thin_router.id == router_id) or not router_id:
                # only get id and model from API above -- expand to useful data
                router = self.get_router_detail(thin_router=thin_router)

                if isinstance(router, CloudRouter):
                    ssh_port = router.ssh_port
                else:
                    ssh_port = router.remote_admin_ssh_port

                router_control = self.get_router_control(router_id=router.router_id)
                if router_control:
                    rc_info = f"[{router_control.ssh_user}, {router_control.ssh_pass}, " \
                        f"{router_control.ssh_host}, {router_control.ssh_port}, " \
                        f"{router_control.linux_namespace}]"
                else:
                    rc_info = "No router control data available"

                try:
                    cpr = router.cproot_password
                except:
                    cpr = "not available"

                print(f"{router.router_id}, {router.capability_id}, {router.admin_user}, {router.admin_password}, "
                      f"{cpr}, {router.remote_admin_host}, {router.remote_admin_port}, "
                      f"{ssh_port}, {rc_info}")

    def lease_router_by_id(self, router_id: str, lease_minutes=2*60):
        if not router_id:
            print("ERROR: you must specify a specific router to lease by ID!")
            exit(1)
        from nctf.test_lib.core.arm4.fixture import ARMv4Fixture
        test_host = socket.gethostname()
        user = pwd.getpwuid(os.getuid()).pw_name  # os.getlogin() won't work in some docker containers
        default_reason = f"{test_host}:{user}:racktools manual router lease"
        arm = ARMv4Fixture(default_reason=default_reason,
                           host=self.api_host,
                           api_key=self.api_key,
                           port=self.api_port)

        renewal_interval = 10  # minutes
        try:
            for remaining in range(lease_minutes, 0, -int(renewal_interval)):
                if router_id.startswith("i-"):
                    router = arm.lease_cloud_router_by_id(cloud_router_id=router_id,
                                                          reason=f"racktools manual lease {remaining}")
                else:
                    router = arm.lease_router_by_id(router_id=router_id, reason=f"racktools manual lease {remaining}")
                print(f"{router.router_id} leased, time remaining {remaining} minutes. "
                      "hit ctrl-c to return router immediately")
                time.sleep(int(60*renewal_interval))
        except KeyboardInterrupt:
            print("interrupt detected stopping")
        except Exception as exc:
            # there are many ways this could fail.  We don't really care here.  Tell the user and get out.
            print(f"{type(exc)}, {exc}")
        arm.teardown()
        print("router returned")

    def list_pools(self):
        pools = self.client.device_pools.list()
        print(f"ARM4 pools hosted at {self.api_host}")
        for pool in pools:
            print(f"{pool.id}, \t{pool.description}")

    def list_routers(self, routers: list, pool_id: str = None, router_id: str = None):
        print(f"Routers in pool {pool_id} at {self.api_host}")
        print("id, \t model,\t healthy,\t clean,\t lease_id")
        for router in routers:
            if not router_id or router.id == router_id:
                print(f"{router.id},\t {router.capability_id},\t {router.healthy},\t {router.clean},\t "
                      f"{router.lease_id}")

    def set_clean(self, router: str):
        self.client.routers.mark_clean(id_=router)

    def set_healthy(self, router: str, healthy: bool):
        if healthy:
            self.client.routers.mark_healthy(id_=router)
        else:
            self.client.routers.mark_unhealthy(id_=router)
            self.client.routers.mark_dirty(id_=router)

    def quick_status(self, routers: list):
        for thin_router in routers:
            router = self.get_router_detail(thin_router=thin_router)
            rest = RouterRESTAPI(api_root=f"https://{router.remote_admin_host}:{router.remote_admin_port}", timeout=2)
            if router.get_router_capabilities().default_lan_only:
                rc = self.get_router_control(router.router_id)
                result = rc.router_get_value(router, value_path="/status/system/bootid")
            else:
                try:
                    rest.authenticate(user_name=router.admin_user, password=router.admin_password)
                    response = rest.get(api_uri="/api/status/system/bootid")
                    rj = response.json()
                    if rj.get("success", False):
                        result = rj["data"]
                    else:
                        result = response.text
                except Exception as exc:
                    result = f"{type(exc)}"
            print(f"{router.router_id}, {router.remote_admin_host}:{router.remote_admin_port}, {result}")


if __name__ == "__main__":

    arm_host = "https://testinfra-arm.private.aws.cradlepointecm.com"
    api_port = 443

    help_handler = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(description='test rack command line utility',
                                     conflict_handler='resolve', formatter_class=help_handler)

    # who or what to act upon (scope)
    parser.add_argument("-p", "--pool_id", dest="pool_id", type=str, default="",
                        help="name of the pool to use")
    parser.add_argument("-r", "--router_id", dest="router_id", type=str, default="",
                        help="name of the router to use")
    parser.add_argument("-ah", "--arm_host", dest="arm_host", type=str, default=arm_host,
                        help="URL of the ARM instance to use including 'https://'")
    parser.add_argument("-m", "--use_minikube", dest="use_minikube", action="store_true", default=False,
                        help="name of the router to use")

    # credentials
    parser.add_argument("-ak", "--api_key", dest="api_key", type=str, default=None,
                        help="ARM4 api key to use")

    # actions
    parser.add_argument("-g", "--get_info", action="store_true", default=False, dest="get_info",
                        help="get useful information for a router")
    parser.add_argument("-lp", "--list_pools", action="store_true", default=False, dest="list_pools",
                        help="List ARM4 router pools")
    parser.add_argument("-lr", "--list_routers", action="store_true", default=False, dest="list_routers",
                        help="List ARM4 routers in a pool (without -p will list all pools!)")
    parser.add_argument("-sc", "--set_clean", action="store_true", default=False, dest="set_clean",
                        help="set -r router clean")
    parser.add_argument("-sh", "--set_healthy", action="store_true", default=False, dest="set_healthy",
                        help="set -r router healthy")
    parser.add_argument("-su", "--set_unhealthy", action="store_true", default=False, dest="set_unhealthy",
                        help="set -r router unhealthy")
    parser.add_argument("-fw", "--firmware", action="store_true", default=False, dest="firmware",
                        help="query currently loaded fw_info (sort of a basic health check)")
    parser.add_argument("-co", "--checkout", action="store_true", default=False, dest="checkout",
                        help="Check Out == lease a router from the database for a period of time")
    parser.add_argument("-cr", "--cloud_routers", action="store_true", default=False, dest="cloud_routers",
                        help="include cloud routers in query results")
    parser.add_argument("-np", "--no_physical_routers", action="store_false", default=True, dest="physical_routers",
                        help="exclude physical routers in query results")
    parser.add_argument("-q", "--quick_status", action="store_true", default=False, dest="quick_status",
                        help="quick health check of routers (shortened response timeout)")

    args = parser.parse_args()

    if args.arm_host:
        arm_host = args.arm_host

    if args.use_minikube:
        arm_host = "http://arm.minikube.info"
        api_port = 80

    tools = ARMv4RackTools(api_username="",
                           api_key=args.api_key,
                           api_host=arm_host,
                           api_port=api_port)

    if args.set_clean:
        tools.set_clean(router=args.router_id or None)

    if args.set_healthy:
        tools.set_healthy(router=args.router_id or None, healthy=True)
    if args.set_unhealthy:
        tools.set_healthy(router=args.router_id or None, healthy=False)

    # show the state *after* any changes that were made
    if args.list_pools:
        tools.list_pools()

    router_list = tools.get_routers(router_id=args.router_id or None, pool_id=args.pool_id or None,
                                    include_cloud=args.cloud_routers,
                                    include_physical=args.physical_routers)

    if args.list_routers:
        tools.list_routers(routers=router_list, pool_id=args.pool_id or None, router_id=args.router_id or None)

    if args.firmware:
        tools.dump_firmware(routers=router_list, router_id=args.router_id or None, pool_id=args.pool_id or None)

    if args.get_info:
        tools.get_info(routers=router_list, router_id=args.router_id or None, pool_id=args.pool_id or None)

    if args.checkout:
        tools.lease_router_by_id(router_id=args.router_id or None, lease_minutes=2*60)

    if args.quick_status:
        tools.quick_status(routers=router_list)
