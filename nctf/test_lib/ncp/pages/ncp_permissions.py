from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By

from .ncp_base import NCPBasePage


class NCPPermissionsPage(NCPBasePage):
    URL_TEMPLATE = "/#/ecm/people/?tenantId={tenant_id}"
    DEFAULT_TIMEOUT = 60

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CLASS_NAME, locator="loading-spinner")

    @property
    def ncp_permissions_table(self):
        return NCPPermissionsTable(self)

    @property
    def add_user_table(self):
        return AddUserTable(self)

    @property
    def edit_user_table(self):
        return EditUserTable(self)


class NCPPermissionsTable(pages.EmberTable):
    ROOT_LOCATOR = (By.ID, 'ecm-people-table')

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=By.CLASS_NAME, locator="loading-spinner")

    @property
    def add_button(self):
        return pages.UIElement(
            name="NCP Add button",
            parent=self,
            strategy=By.CSS_SELECTOR,
            locator='[data-test-add-user]')

    @property
    def edit_button(self):
        return pages.UIElement(
            name="NCP Edit button",
            parent=self,
            strategy=By.CSS_SELECTOR,
            locator='[data-test-edit-user]')

    @property
    def delete_button(self):
        return pages.UIRegionLinkElement("Account Users Delete button", self, DeleteUserRegion, By.ID, "deleteButton']")


class AddUserTable(pages.EmberTable):
    ROOT_LOCATOR = (By.CSS_SELECTOR, "[data-test-account-user-table]")

    @property
    def next_button(self):
        return pages.UIElement(
            name='Next button',
            parent=self,
            strategy=By.CSS_SELECTOR,
            locator="[data-test-add-users]")


class EditUserTable(pages.EmberTable):
    ROOT_LOCATOR = (By.CSS_SELECTOR, "[data-test-edit-user-table]")

    @property
    def save_button(self):
        return pages.UIElement(
            name='Save button',
            parent=self,
            strategy=By.CSS_SELECTOR,
            locator="[data-test-save-user]")

    @property
    def cancel_button(self):
        return pages.UIElement(
            name="Cancel button",
            parent=self,
            strategy=By.CSS_SELECTOR,
            locator="[data-test-cancel-user-edit]")


# Not tested
class DeleteUserRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//div[contains(@class, 'cp-message-box')]")

    def wait_for_region_to_load(self):
        self.yes_button.wait_for_clickable()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def yes_button(self):
        return pages.UIPageLinkElement(
            name="Yes Button",
            parent=self,
            page_cls=NCPPermissionsPage,
            frame=self.frame,
            strategy=By.XPATH,
            locator="//span[text()='Yes']/..",
            tenant_id='{tenant_id}')

    @property
    def no_button(self):
        return pages.UIPageLinkElement(
            name="No Button",
            parent=self,
            page_cls=NCPPermissionsPage,
            frame=self.frame,
            strategy=By.XPATH,
            locator="//span[text()='No']/..",
            tenant_id='{tenant_id}')
