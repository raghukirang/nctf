import logging
from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.common.side_nav_bar import SideNavBar

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class NCPBasePage(pages.UIPage):

    WEB_SERVICE = 'archer'
    DEFAULT_TIMEOUT = 30

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def side_nav_bar(self):
        self.wait_for_page_to_load()
        return SideNavBar(
            self,
            strategy=pages.By.XPATH,
            locator="//*[contains(@class, 'x-panel x-border-item x-box-item x-panel-navbar')]")

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self
