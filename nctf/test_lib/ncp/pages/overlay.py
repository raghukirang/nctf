import logging

from nctf.test_lib.base import pages
from selenium.webdriver.common.by import By

from .ncp_base import NCPBasePage

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class NetworksTable(pages.EmberTable):
    ROOT_LOCATOR = (pages.By.ID, "network-list-table")

    @property
    def select_all_checkbox(self):
        return pages.UIElement(
            name='select all networks checkbox',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator='th.cp-table__select-column')


class OverlayPage(NCPBasePage):
    URL_TEMPLATE = "/#/home"
    DEFAULT_TIMEOUT = 15

    @property
    def networks_table(self):
        return NetworksTable(self)
