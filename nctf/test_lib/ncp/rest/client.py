import logging

from requests import Response
from requests import Session

from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
from nctf.test_lib.base.rest import checked_status

logger = logging.getLogger('test_lib.ncp.rest')

DEVICES_ENDPOINT = '/api/v1/devices'
NETWORKS_ENDPOINT = '/api/v1/networks'
NOTIFICATIONS_ENDPOINT = '/api/v1/notifications'
ORGS_ENDPOINT = '/api/v1/orgs'
USERS_ENDPOINT = '/api/v1/users'
CHECK_LOGIN_ENDPOINT = 'api/checkLogin'


class PAPIv1RESTClient(RESTClient):
    def __init__(self, protocol: str, hostname: str, port: int, timeout: float = 30.0, session: Session = None):
        super().__init__(protocol, hostname, port, timeout, session)
        """NCP REST API client"""

        self.devices = DevicesEndpoint(self, DEVICES_ENDPOINT)
        self.networks = NetworksEndpoint(self, NETWORKS_ENDPOINT)
        self.notifications = NotificationsEndpoint(self, NOTIFICATIONS_ENDPOINT)
        self.orgs = OrgsEndpoint(self, ORGS_ENDPOINT)
        self.users = UsersEndpoint(self, USERS_ENDPOINT)
        self.check_login = CheckLoginEndpoint(self, CHECK_LOGIN_ENDPOINT)

    def authenticate(self, *args, **kwargs):
        pass


class PAPIv1Endpoint(RESTEndpoint):

    @checked_status(200)
    def detail(self, resource_id: str, *args, **kwargs) -> Response:
        return self._get(resource_id, *args, **kwargs)

    @checked_status(200)
    def list(self, *args, **kwargs) -> Response:
        return self._get(*args, **kwargs)

    def delete(self, resource_id: str, *args, **kwargs) -> Response:
        return self._delete(resource_id, *args, **kwargs)


class DevicesEndpoint(PAPIv1Endpoint):
    pass


class NetworksEndpoint(PAPIv1Endpoint):

    def list(self, tenant_id, *args, **kwargs) -> Response:
        query_params = {'tenant_id': tenant_id}
        return super().list(params = query_params, *args, **kwargs)


class NotificationsEndpoint(PAPIv1Endpoint):
    pass


class OrgsEndpoint(PAPIv1Endpoint):
    @checked_status(200)
    def create_network(self, org_id: str, network_name: str, *args, **kwargs) -> Response:
        payload = {
            "name": network_name
        }
        return self._post(resource_id=org_id, *args, data=payload, **kwargs)


class UsersEndpoint(PAPIv1Endpoint):
    pass

class CheckLoginEndpoint(PAPIv1Endpoint):
    pass