import requests

from .decorators import jsonify
from .decorators import ResponseDict
from .rest_endpoint import JSONEndpoint
from .rest_endpoint import RESTAPIEndpoint


class RESTAPIResource(object):
    def __init__(self, endpoint: RESTAPIEndpoint, resource_id: str, checked: bool = True):
        self._endpoint = endpoint
        self._rid = resource_id
        self._checked = checked

    @property
    def uri(self) -> str:
        """Returns the resource's URI, relative to the host."""
        return "{}/{}".format(self._endpoint.uri, self._rid).replace('//', '/')

    @property
    def url(self) -> str:
        """Returns the full URL for the resource."""
        return "{}/{}".format(self._endpoint.url, self._rid).replace('//', '/')

    def request(self, method: str, **kwargs) -> requests.Response:
        return self._endpoint._request(method, self._rid, **kwargs)

    def get(self, **kwargs) -> requests.Response:
        return self._endpoint._get(self._rid, **kwargs)

    def post(self, **kwargs) -> requests.Response:
        return self._endpoint._post(self._rid, **kwargs)

    def put(self, **kwargs) -> requests.Response:
        return self._endpoint._put(self._rid, **kwargs)

    def patch(self, **kwargs) -> requests.Response:
        return self._endpoint._patch(self._rid, **kwargs)

    def delete(self, **kwargs) -> requests.Response:
        return self._endpoint._delete(self._rid, **kwargs)


class JSONResource(RESTAPIResource):
    """Representation of a resource from a REST API, with methods returning JSON-like objects."""

    def __init__(self, endpoint: RESTAPIEndpoint, resource_id: str, checked: bool = True):
        assert not isinstance(endpoint, JSONEndpoint)
        super().__init__(endpoint, resource_id, checked)

    @jsonify
    def request(self, method: str, **kwargs) -> ResponseDict:
        return super().request(method, **kwargs)

    @jsonify
    def get(self, **kwargs) -> ResponseDict:
        return super().get(**kwargs)

    @jsonify
    def post(self, **kwargs) -> ResponseDict:
        return super().post(**kwargs)

    @jsonify
    def put(self, **kwargs) -> ResponseDict:
        return super().put(**kwargs)

    @jsonify
    def patch(self, **kwargs) -> ResponseDict:
        return super().patch(**kwargs)

    @jsonify
    def delete(self, **kwargs) -> ResponseDict:
        return super().delete(**kwargs)
