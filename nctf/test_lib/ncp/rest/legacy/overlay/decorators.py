import functools
import json
from typing import Callable

import requests

from ..decorators import ResponseDict


def json_transform(responder: Callable[..., requests.Response]):
    """If a response has any content (even non-JSON), transform it into a JSON-/dict-like type (ie. ResponseDict).

    Though the response is dict-like, it has `.status_code` and `.headers`
    member variables in order to preserve raw `requests.Response` usability.

    An empty server response will be treated as an empty dict instead (and the
    returned ResponseDict will still contain the headers and status code).

    A non-JSON response from the server will be converted like so,
        ```{
            "html_text": response.text,
            "raw_content": response.content
        }```
    That is, if the response is not JSON, you will be able to access the
    text-content and binary-content via keys "html_text" and "raw_content"
    respectively.
    """

    @functools.wraps(responder)
    def func_wrapper(*args, **kwargs) -> ResponseDict:
        r = responder(*args, **kwargs)
        try:
            jsonified = ResponseDict(r.json()) if r.content else ResponseDict()
        except json.decoder.JSONDecodeError:
            jsonified = ResponseDict({"html_text": r.text, "raw_content": r.content})
        jsonified.headers = r.headers
        jsonified.status_code = r.status_code
        return jsonified

    return func_wrapper
