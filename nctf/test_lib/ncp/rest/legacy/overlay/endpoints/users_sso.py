from typing import Dict

from ...decorators import ResponseDict
from ...rest_resource import RESTAPIResource
from .base import BasicOverlayEndpoint


class SSOUserResource(RESTAPIResource):
    def __init__(self, rest_api: BasicOverlayEndpoint, okta_id: str, checked: bool = True):
        super().__init__(rest_api, okta_id, checked)

    def detail(self, tenant_id: str, **kwargs) -> ResponseDict:
        kwargs.setdefault('params', {}).update({'tenantId': tenant_id})
        return self.get(**kwargs)

    def update(self, update_params: Dict, **kwargs) -> ResponseDict:
        return self.patch(json=update_params, **kwargs)


class SSOUsersEndpoint(BasicOverlayEndpoint):
    def resource(self, okta_id: str) -> SSOUserResource:
        return SSOUserResource(self, okta_id, self._checked)

    def create(self, tenant_id: str, first_name: str, last_name: str, okta_id: str, **kwargs) -> ResponseDict:
        new_user_params = {"tenantId": tenant_id, "oktaId": okta_id, "firstName": first_name, "lastName": last_name}
        return super().create(new_user_params, **kwargs)

    def detail(self, okta_id: str, tenant_id: str, **kwargs) -> ResponseDict:
        return self.resource(okta_id).detail(tenant_id, **kwargs)

    def update(self, okta_id: str, update_params: Dict, **kwargs) -> ResponseDict:
        return super().update_patch(okta_id, update_params, **kwargs)
