from ...decorators import ResponseDict
from .base import BasicOverlayEndpoint


class AccountFeatureEndpoint(BasicOverlayEndpoint):
    def enable(self, **kwargs) -> ResponseDict:
        return self.extend('isEnabled').create({}, **kwargs)

    def disable(self, **kwargs) -> ResponseDict:
        return self.extend('isEnabled').delete('', **kwargs)

    def create(self, **kwargs) -> ResponseDict:
        return super().create({}, **kwargs)

    def delete(self, **kwargs) -> ResponseDict:
        return super().delete('', json={}, **kwargs)


class AccountsEndpoint(BasicOverlayEndpoint):
    def user_access(self, tenant_id: str) -> BasicOverlayEndpoint:
        return self.extend("{}/userAccess".format(tenant_id))

    def features(self, tenant_id: str) -> BasicOverlayEndpoint:
        return self.extend("{}/feature".format(tenant_id))

    def feature(self, tenant_id: str, feature_id: str) -> AccountFeatureEndpoint:
        return AccountFeatureEndpoint(self._rest_api, "{}/{}/feature/{}".format(self.uri, tenant_id, feature_id))
