from ...rest_endpoint import ResponseDict
from ...rest_endpoint import RESTAPIEndpoint
from ...rest_endpoint import RESTAPIInterface
from ..decorators import json_transform


class JSONTransformEndpoint(RESTAPIEndpoint):
    """Specialized RESTAPIEndpoint for PAPI & MC APIs.

    The underlying _get, _post, etc. methods are converted to return a
    ResponseDict, which has dict-like access but also includes `status_code`
    and `headers` members to preserve the data from a `requests.Response` object.

    The responses will always be transformed into JSON. See `json_transform` for more info.
    """

    def __init__(self, rest_api: RESTAPIInterface, api_uri: str, checked: bool = True):
        super().__init__(rest_api, api_uri, checked)

    @json_transform
    def _request(self, method: str, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._request(method, resource_id, *args, **kwargs)

    @json_transform
    def _get(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._get(resource_id, *args, **kwargs)

    @json_transform
    def _post(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._post(resource_id, *args, **kwargs)

    @json_transform
    def _put(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._put(resource_id, *args, **kwargs)

    @json_transform
    def _patch(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._patch(resource_id, *args, **kwargs)

    @json_transform
    def _delete(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._delete(resource_id, *args, **kwargs)
