from typing import Dict

from ...decorators import checked_status
from ...decorators import ResponseDict
from ...rest_resource import RESTAPIResource
from .base import BasicOverlayEndpoint


class OrgResource(RESTAPIResource):
    def __init__(self, rest_api: BasicOverlayEndpoint, okta_id: str, checked: bool = True):
        super().__init__(rest_api, okta_id, checked)

    @checked_status(200)
    def create_network(self, network_name: str, **kwargs):
        return self.post(json={'name': network_name}, **kwargs)


class OrgsEndpoint(BasicOverlayEndpoint):
    def resource(self, org_id: str) -> OrgResource:
        return OrgResource(self, org_id, self._checked)
