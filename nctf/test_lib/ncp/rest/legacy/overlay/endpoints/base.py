from typing import Dict

from ...decorators import checked_status
from ...rest_endpoint import ResponseDict
from .json_transform import JSONTransformEndpoint


# TODO: Provide automatic check that r.status_code == r.json()[..]['statusCode']
class BasicOverlayEndpoint(JSONTransformEndpoint):
    def resource(self, resource_id):
        return NotImplemented

    @checked_status((200, 201, 204))
    def create(self, creation_params: Dict, **kwargs) -> ResponseDict:
        """Generic POST method with JSON-ified return."""
        return self._post(json=creation_params, **kwargs)

    @checked_status(200)
    def detail(self, resource_id: str, **kwargs) -> ResponseDict:
        """Generic GET method for a specific resource, with JSON-ified return."""
        return self._get(resource_id, **kwargs)

    @checked_status(200)
    def list(self, **kwargs) -> ResponseDict:
        """Generic GET method for a list of resources, with JSON-ified return."""
        return self._get(**kwargs)

    @checked_status((200, 204))
    def update_patch(self, resource_id: str, update_params: Dict, **kwargs) -> ResponseDict:
        """Generic PATCH method with JSON-ified return."""
        return self._patch(resource_id, json=update_params or {}, **kwargs)

    @checked_status((200, 204))
    def update_put(self, resource_id: str, update_params: Dict, **kwargs) -> ResponseDict:
        """Generic PUT method with JSON-ified return."""
        return self._put(resource_id, json=update_params or {}, **kwargs)

    @checked_status((200, 204))
    def delete(self, resource_id: str, **kwargs) -> ResponseDict:
        """Generic DELETE method with JSON-ified return."""
        return self._delete(resource_id, **kwargs)

    def extend(self, uri: str) -> 'BasicOverlayEndpoint':
        return BasicOverlayEndpoint(self._rest_api, '{}/{}'.format(self.uri, uri))
