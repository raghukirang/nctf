from typing import Optional

from ..exceptions import CannotFulfillRequestError
from .interface import OverlayAPIInterface


class PAPIClient(object):
    """Happy-path convenience implementations for otherwise complicated actions.

    This is assumed to operate at a correct authentication level for the
    underlying interface.
    """

    def __init__(self, papi_iface: OverlayAPIInterface):
        self.api = papi_iface
        self._tenant_id = None

    def set_play_session(self) -> None:
        self.api.get('/api/checkLogin', 200)

    def create_network(self, network_name: str) -> None:
        """Create a named network for a given tenant.

        Args:
            network_name: The name to be given to the new network.
        """
        default_network = self.get_default_network()
        if default_network is None:
            raise CannotFulfillRequestError("Could not auto-discover default network for getting Org ID.")

        org_id = self.api.networks.detail(default_network)['data']['organizationId']
        self.api.orgs.resource(org_id).create_network(network_name)

    def get_default_network(self, tenant_id: Optional[str] = None) -> Optional[int]:
        """Using the provided authenticated API interface, find the default network ID within the tenant specified.

        Args:
            tenant_id: If provided, include the tenant ID in the request filter.

        Returns:
            The default network ID, if found. Otherwise, None is returned.
        """
        network_meta = self.api.networks.list(params={'tenantId': tenant_id})['meta']
        try:
            return network_meta['defaultNetworkId']['id']
        except KeyError:
            return None
