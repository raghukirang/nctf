import logging
import numbers
from typing import Optional

import requests

from nctf.test_lib.ncp.rest.legacy.rest_interface import InjectedModifier
from nctf.test_lib.ncp.rest.legacy.rest_interface import RESTAPIInterface

from . import endpoints

logger = logging.getLogger('papi.interface')

ACCOUNTS_ENDPOINT = 'api/v1/accounts'
DEVICES_ENDPOINT = 'api/v1/devices'
NETWORKS_ENDPOINT = 'api/v1/networks'
NOTIFICATIONS_ENDPOINT = 'api/v1/notifications'
ORGS_ENDPOINT = 'api/v1/orgs'
SSO_USERS_ENDPOINT = 'api/v2/users/sso'
USERS_V1_ENDPOINT = 'api/v1/users'
USERS_V2_ENDPOINT = 'api/v2/users'


class OverlayAPIInterface(RESTAPIInterface):
    """API mapping to the Overlay Service of NetCloud (aka PAPI, or API Server).

    QA2 REST API documentation: https://api-overlay-qa2.cradlepointecm.com/documentation

    All endpoint members (networks, sso_users, etc.) have methods which return
    JSON-like `ResponseDict` objects for convenience.

    Args:
        session: Session to be used for REST API.
        api_root: Root of the REST API to be used.
        timeout: Number of seconds to be used as the Session's timeout.

    Attributes:
        networks: /api/v1/networks endpoint actions.
            For an auth'd user, this will act with the networks they can manage (admin) or are part of (members).
        accounts: /api/v1/accounts endpoint actions.
            Not really sure yet to be honest, but it's that endpoint. Features can be accessed/modified here.
        sso_users: /api/v2/users/sso endpoint actions.
            Current way to create, update, and access SSO (i.e. NetCloud) user representations in the MC.
            Likely to be replaced by /api/v2/users.
    """

    def __init__(self, api_root: str, timeout: numbers.Number = 30.0, session: Optional[requests.Session] = None):
        super().__init__(api_root, timeout, session)

        self.__auth = OverlayASAuthModifier()

        self.accounts = endpoints.AccountsEndpoint(self, ACCOUNTS_ENDPOINT)
        self.devices = endpoints.BasicOverlayEndpoint(self, DEVICES_ENDPOINT)
        self.networks = endpoints.BasicOverlayEndpoint(self, NETWORKS_ENDPOINT)
        self.notifications = endpoints.BasicOverlayEndpoint(self, NOTIFICATIONS_ENDPOINT)
        self.orgs = endpoints.OrgsEndpoint(self, ORGS_ENDPOINT)
        self.sso_users = endpoints.SSOUsersEndpoint(self, SSO_USERS_ENDPOINT)
        self.users_v1 = endpoints.BasicOverlayEndpoint(self, USERS_V1_ENDPOINT)
        self.users_v2 = endpoints.BasicOverlayEndpoint(self, USERS_V2_ENDPOINT)

    def authenticate(self, *args, **kwargs) -> None:
        raise NotImplementedError("Use the Accounts Service to authenticate as a user.")

    def authenticate_with_keys(self, key_id: str, key_secret: str) -> None:
        self.__auth = OverlayKeyAuthModifier(key_id, key_secret)

    def request_as(self,
                   session: requests.Session,
                   method: str,
                   api_uri: str,
                   expected_status_code: int = None,
                   err_msg: str = None,
                   **kwargs):
        return super().request_as(session, method, api_uri, expected_status_code, err_msg, auth=self.__auth, **kwargs)


class OverlayASAuthModifier(InjectedModifier):
    def __init__(self):
        super().__init__()

    def __call__(self, request: requests.PreparedRequest) -> requests.PreparedRequest:
        request.auth = None
        request.headers.update({'Content-Type': 'application/json'})
        return request


class OverlayKeyAuthModifier(InjectedModifier):
    def __init__(self, key_id: str, key_secret: str):
        super().__init__()
        self.__key_id = key_id
        self.__key_secret = key_secret

    def __call__(self, request: requests.PreparedRequest) -> requests.PreparedRequest:
        request.auth = None
        request = requests.auth.HTTPBasicAuth(self.__key_id, self.__key_secret)(request)
        request.headers.update({'Content-Type': 'application/json'})
        return request
