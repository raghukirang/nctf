from typing import Union
from urllib.parse import urljoin

import requests

from .decorators import jsonify
from .decorators import ResponseDict
from .rest_interface import RESTAPIInterface


class RESTAPIEndpoint(object):
    def __init__(self, rest_api: RESTAPIInterface, api_uri: str, checked: bool = True):
        """Base endpoint class. Establishes python mappings to the basic functionality of a REST API endpoint.

        Args:
            rest_api: The REST API we'll be using to perform requests.
            api_uri: The endpoint URL segment. When concatenated to an API Root it forms the full path to the resource.
            checked: Enables/disables the default status code checks, if defined. See :func:`set_status_checking()`.
        """
        self._rest_api = rest_api
        self._api_uri = "{}{}".format('/' if isinstance(api_uri, str) and not api_uri.startswith('/') else '', api_uri)
        self._checked = checked

    @property
    def uri(self) -> str:
        """Returns the API's root URL from endpoint setup.

        Returns:
            The API's URI.
        """
        return self._api_uri

    @property
    def url(self) -> str:
        """Returns the endpoint's full URL.

        Returns:
            Returns the endpoint's full URL, eg. https://cradlepointecm.com/api/v1/users/
        """
        return urljoin(self._rest_api.base_url, self.uri)

    def set_status_checking(self, checked: bool) -> None:
        """Set whether or not to automatically check status codes on certain endpoint methods.

        Setting this to true will enable the default status checks, if defined.
        Unexpected status codes on following requests will raise UnexpectedStatusErrors.
        """
        self._checked = checked

    def extend(self, uri: str) -> 'RESTAPIEndpoint':
        return RESTAPIEndpoint(self._rest_api, '{}/{}'.format(self.uri, uri))

    def _format_target(self, resource_id: Union[int, str] = None) -> str:
        if resource_id is not None and resource_id != '':
            return '{}{}'.format(self._api_uri, resource_id) if self._api_uri.endswith('/') else '{}/{}'.format(
                self._api_uri, resource_id)
        return self._api_uri

    def _request(self, method: str, resource_id: str = None, *args, **kwargs) -> requests.Response:
        target = self._format_target(resource_id)
        return self._rest_api.request(method, target, *args, **kwargs)

    def _get(self, resource_id: str = None, *args, **kwargs) -> requests.Response:
        target = self._format_target(resource_id)
        return self._rest_api.get(target, *args, **kwargs)

    def _post(self, resource_id: str = None, *args, **kwargs) -> requests.Response:
        target = self._format_target(resource_id)
        return self._rest_api.post(target, *args, **kwargs)

    def _put(self, resource_id: str = None, *args, **kwargs) -> requests.Response:
        target = self._format_target(resource_id)
        return self._rest_api.put(target, *args, **kwargs)

    def _patch(self, resource_id: str = None, *args, **kwargs) -> requests.Response:
        target = self._format_target(resource_id)
        return self._rest_api.request("PATCH", target, *args, **kwargs)

    def _delete(self, resource_id: str = None, *args, **kwargs) -> requests.Response:
        target = self._format_target(resource_id)
        return self._rest_api.delete(target, *args, **kwargs)


class JSONEndpoint(RESTAPIEndpoint):
    """Specialized RESTAPIEndpoint for JSON APIs.

    The underlying _get, _post, etc. methods are converted to return a
    ResponseDict, which has dict-like access but also includes `status_code`
    and `headers` members to preserve the data from a `requests.Response` object.
    """

    def __init__(self, rest_api: RESTAPIInterface, api_uri: str, checked: bool = True):
        super().__init__(rest_api, api_uri, checked)

    @jsonify
    def _request(self, method: str, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._request(method, resource_id, *args, **kwargs)

    @jsonify
    def _get(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._get(resource_id, *args, **kwargs)

    @jsonify
    def _post(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._post(resource_id, *args, **kwargs)

    @jsonify
    def _put(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._put(resource_id, *args, **kwargs)

    @jsonify
    def _patch(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._patch(resource_id, *args, **kwargs)

    @jsonify
    def _delete(self, resource_id: str = None, *args, **kwargs) -> ResponseDict:
        return super()._delete(resource_id, *args, **kwargs)
