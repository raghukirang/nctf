import functools
from typing import Callable
from typing import Dict
from typing import Tuple
from typing import Union

import requests


class ResponseDict(Dict):
    """Provides a dict-like wrapper for JSON responses.

    Though this is used as a regular dict, it also has `.status_code` and
    `.headers` member variables in order to preserve raw `requests.Response`
    usability.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.status_code = None
        self.headers = None


def jsonify(responder: Callable[..., requests.Response]):
    """If a response has content, turns it into a JSON-/dict-like type (ie. ResponseDict).

    Though the response is dict-like, it has `.status_code` and `.headers`
    member variables in order to preserve raw `requests.Response` usability.

    An empty server response will be treated as an empty dict instead (and the
    returned ResponseDict will still contain the headers and status code).
    """

    @functools.wraps(responder)
    def func_wrapper(*args, **kwargs) -> ResponseDict:
        r = responder(*args, **kwargs)
        jsonified = ResponseDict(r.json()) if r.content else ResponseDict()
        jsonified.headers = r.headers
        jsonified.status_code = r.status_code
        return jsonified

    return func_wrapper


def checked_status(expected_status_codes: Union[int, Tuple[int, ...]]):
    """Inserts a status check into the request iff the interface's  ._checked member is truthy.

    If the caller provides an expected status, the caller-specified status will override this one.
    """

    def decorator(responder: Callable[..., Union[requests.Response, ResponseDict]]):
        @functools.wraps(responder)
        def func_wrapper(self, *args, **kwargs):
            if self._checked and 'expected_status_code' not in kwargs:
                kwargs['expected_status_code'] = expected_status_codes
            return responder(self, *args, **kwargs)

        return func_wrapper

    return decorator
