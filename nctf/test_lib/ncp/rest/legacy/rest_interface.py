import collections
import logging
import numbers
from typing import Any
from typing import Dict
from typing import Optional
from typing import Tuple
from typing import Union
from urllib.parse import urlsplit
from urllib.parse import urlunsplit

import requests

from nctf.libs.common_library import require_type
from nctf.test_lib.ncp.rest.legacy.exceptions import UnexpectedStatusError

logger = logging.getLogger('restapi.interface')


class InjectedModifier(requests.auth.AuthBase):
    """Use the requests' auth injection feature to modify requests on call."""

    def __init__(self):
        super().__init__()

    def __call__(self, request):
        return request


class RESTAPIInterface(object):
    """Base class for REST API interfaces

    Args:
        session: Session to be used for REST API.
        api_root: Root of the REST API to be used.
        timeout: Number of seconds to be used as the Session's timeout.
    """

    def __init__(self, api_root: str, timeout: numbers.Number, session: Optional[requests.Session] = None):
        require_type(session, (requests.Session, type(None)))
        require_type(api_root, str)
        require_type(timeout, numbers.Number)
        self._session = session if session is not None else requests.Session()
        self._api_root = api_root
        self._timeout = timeout

    @property
    def base_url(self) -> str:
        """Returns the API's root URL from interface setup.

        The URL will be normalized to match what a web browser would show. For example,
        default ports (eg. 443 for HTTPS) will be stripped from the result.

        Returns:
            The API's root URL, normalized.
        """
        return RESTAPIInterface.normalize_url(self._api_root)

    """ Requests Helpers """

    def request_as(self,
                   session: requests.Session,
                   method: str,
                   api_uri: str,
                   expected_status_code: Union[int, Tuple[int, ...]] = None,
                   err_msg: str = None,
                   **kwargs) -> requests.Response:
        """Common request method.

        This is intended for use in other objects (ECM Users, etc.) so that a specific session may be used.
        All requests should eventually go through this call to ensure consistent request usage.

        Optionally checks the response's status code. If the status code is unexpected,
        an error is raised and the request/response details are logged.

        Other requests exceptions are caught and re-raised with additional logging.

        Args:
            session: Session to be used to communicate with the REST API.
            method: HTTP method ('GET', 'PUT', 'POST', etc.).
            api_uri: The API URI (eg. '/api/v1/users/').
            expected_status_code: The expected HTTP status code. May be a single int or a tuple/list of ints.
            err_msg: Message to be displayed in the event an error occurs.
            **kwargs: Additional requests arguments.

        Returns:
            The response, if no exceptions were raised.

        Raises:
            UnexpectedStatusError: If the response status code != expected_status_code.
        """
        require_type(method, str)
        require_type(api_uri, str)
        require_type(session, requests.Session)
        require_type(err_msg, (type(None), str))
        require_type(expected_status_code, (type(None), int, collections.Iterable))

        exp_status_codes = (expected_status_code,) if isinstance(expected_status_code, int) else expected_status_code

        # Default timeout
        args_to_pass = {'timeout': self._timeout}  # type: Dict[str, Any]
        args_to_pass.update(kwargs)

        logger.debug("{} {}".format(method, api_uri))
        if 'data' in kwargs:
            logger.debug("%s data: %s", method, kwargs['data'])
        if 'json' in kwargs:
            logger.debug("%s data: %s", method, kwargs['json'])

        try:
            r = session.request(method, self._api_root + api_uri, **args_to_pass)
        except requests.exceptions.RequestException as e:
            msg = " Exception raised on '{} {}'.\n".format(method, api_uri)
            msg += "---Request params (next line):\n\t{}\n".format(kwargs if kwargs else "")
            msg += "---Exception (next line):\n\t{}".format(str(e))
            msg = str(e) + msg
            exception_type = type(e)
            raise exception_type(msg) from e

        if expected_status_code is not None and r.status_code not in exp_status_codes:
            msg = "Unexpected response from '{} {}'. ".format(method, api_uri)
            msg += "Status code: {} ({}).\n".format(r.status_code, r.reason)
            msg += "---Request params (next line):\n\t{}\n".format(kwargs)
            msg += "---Response content (next line):\n\t{}\n".format(r.content)
            if err_msg:
                msg += "---Error Message: {}".format(err_msg)
            raise UnexpectedStatusError(msg, r)

        return r

    def get_as(self, session: requests.Session, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform GET as the `method` for :func:`RESTAPIInterface.request_as`."""
        return self.request_as(session, "GET", api_uri, expected_status_code, **kwargs)

    def put_as(self, session: requests.Session, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform PUT as the `method` for :func:`RESTAPIInterface.request_as`."""
        return self.request_as(session, "PUT", api_uri, expected_status_code, **kwargs)

    def post_as(self, session: requests.Session, api_uri: str, expected_status_code: int = None,
                **kwargs) -> requests.Response:
        """Perform POST as the `method` for :func:`RESTAPIInterface.request_as`."""
        return self.request_as(session, "POST", api_uri, expected_status_code, **kwargs)

    def delete_as(self, session: requests.Session, api_uri: str, expected_status_code: int = None,
                  **kwargs) -> requests.Response:
        """Perform DELETE as the `method` for :func:`RESTAPIInterface.request_as`."""
        return self.request_as(session, "DELETE", api_uri, expected_status_code, **kwargs)

    def patch_as(self, session: requests.Session, api_uri: str, expected_status_code: int = None,
                 **kwargs) -> requests.Response:
        """Perform PATCH as the `method` for :func:`RESTAPIInterface.request_as`."""
        return self.request_as(session, "PATCH", api_uri, expected_status_code, **kwargs)

    def request(self, method: str, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Common request method using test session authentication.

        All test session requests should go through this call to ensure consistent request usage.

        Optionally checks the response's status code. If the status code is unexpected,
        an error is raised and the request/response details are logged.

        Other requests exceptions are caught and re-raised with additional logging.

        In the case of privileged test setups, this is usually a root-auth request.

        Args:
            method: HTTP method ('GET', 'PUT', 'POST', etc.).
            api_uri: The API URI (eg. '/api/v1/users/').
            expected_status_code: The expected HTTP status code.
            **kwargs: Additional requests arguments.

        Returns:
            The response, if no exceptions were raised.

        Raises:
            UnexpectedStatusError: If the response status code != expected_status_code.
        """
        return self.request_as(self._session, method, api_uri, expected_status_code, **kwargs)

    def get(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform GET as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        return self.request("GET", api_uri, expected_status_code, **kwargs)

    def put(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform PUT as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        return self.request("PUT", api_uri, expected_status_code, **kwargs)

    def post(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform POST as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        return self.request("POST", api_uri, expected_status_code, **kwargs)

    def delete(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform DELETE as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        return self.request("DELETE", api_uri, expected_status_code, **kwargs)

    def patch(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Perform PATCH as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        return self.request("PATCH", api_uri, expected_status_code, **kwargs)

    def update_headers(self, new_hdrs: dict) -> None:
        """Updates the Session's headers with new_hdrs

        Args:
            new_hdrs: The new headers to be associated with the session.
        """
        self._session.headers.update(new_hdrs)

    def authenticate(self, *args, **kwargs) -> None:
        """Authenticate with the REST API"""
        raise NotImplementedError()

    @staticmethod
    def normalize_url(url: str) -> str:
        """Returns the given URL in a normalized form.

        The URL will be normalized to match what a modern web browser would show. For example,
        default ports (eg. 443 for HTTPS) will be stripped from the result.

        Args:
            url: The URL to normalize.

        Returns:
            The provided URL, normalized.
        """
        default_ports = [('https', 443), ('http', 80)]
        parsed = urlsplit(url)
        if (parsed.scheme, parsed.port) in default_ports:
            return urlunsplit((parsed.scheme, parsed.hostname, parsed.path, parsed.query, parsed.fragment))
        else:
            return urlunsplit(parsed)
