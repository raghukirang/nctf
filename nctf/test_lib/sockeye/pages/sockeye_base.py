import logging

from nctf.test_lib.base import pages

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class SockeyeBasePage(pages.UIPage):

    WEB_SERVICE = 'sockeyeUI'
    DEFAULT_TIMEOUT = 60

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CLASS_NAME, locator="spinner")

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner(timeout=self.DEFAULT_TIMEOUT)
        return self


class SockeyeBaseRegion(pages.UIRegion):

    DEFAULT_TIMEOUT = 30

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CLASS_NAME, locator="spinner")

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.spinner.wait_for_spinner(timeout=self.DEFAULT_TIMEOUT)
        return self
