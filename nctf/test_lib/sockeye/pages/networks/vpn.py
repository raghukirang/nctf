"""This module is a mapping of the NETWORKS > VPN page."""

import logging
import time

from nctf.libs.common_library import WaitTimeoutError
from nctf.test_lib.base import pages
from nctf.test_lib.base.pages.exceptions import ActionNotPossibleError
from nctf.test_lib.sockeye.pages.sockeye_base import SockeyeBasePage
from nctf.test_lib.sockeye.pages.sockeye_base import SockeyeBaseRegion
from nctf.test_lib.utils.waiter import wait

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class NetworksTable(pages.EmberTable, pages.UIPagingTable):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, ".network-table")
    SCROLL_CONTENT = None
    SCROLL_HANDLE = None
    IS_SCROLLABLE = False
    PAGING_TOOLBAR = (pages.By.CLASS_NAME, 'cp-table__footer')
    FIRST_PAGE = (pages.By.CLASS_NAME, 'cp-pagination__first-page')
    PREVIOUS_PAGE = (pages.By.CLASS_NAME, 'cp-pagination__prev-page')
    GOTO_PAGE = (pages.By.TAG_NAME, 'input')
    TOTAL_PAGE = (pages.By.CLASS_NAME, 'cp-pagination__total-pages-label')
    NEXT_PAGE = (pages.By.CLASS_NAME, 'cp-pagination__next-page')
    LAST_PAGE = (pages.By.CLASS_NAME, 'cp-pagination__last-page')

    @property
    def select_all_checkbox(self):
        return pages.UIElement(
            name='select all networks checkbox',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator='th.cp-table__select-column')


class DashboardHubTable(pages.EmberTable):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-dashboard-hubs-table]")


class DashboardSpokesTable(pages.EmberTable):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-dashboard-spokes-table]")


class NetworkDashboard(SockeyeBasePage):
    """The Network Dashboard page."""

    URL_TEMPLATE = ""

    @property
    def networks_breadcrumb_button(self):
        return pages.UIPageLinkElement(
            name='networks breadcrumb button',
            parent=self,
            page_cls=NetworksHomePage,
            frame=self.frame,
            strategy=pages.By.XPATH,
            locator="//h1[text()='Networks']")


class AddSpokesDialog(SockeyeBaseRegion):
    """The 'Add Spokes' dialog."""

    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-add-spokes-dialog]")

    class SpokeSelectionTable(pages.EmberTable):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-spoke-selection-table]")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner(timeout=10)
        self.cancel_button.wait_for_clickable()
        return self

    @property
    def cancel_button(self):
        """Return the Cancel button on the Add Spokes dialog."""
        return pages.UIPageLinkElement(
            name='add spoke cancel button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-add-spokes-cancel-button]")

    @property
    def add_button(self):
        """Return the Add button on the Add Spokes dialog."""
        return pages.UIPageLinkElement(
            name='add spoke add button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-add-spokes-add-button]")

    @property
    def primary_lan_table_row(self):
        """Return the Primary LAN table row."""
        return pages.UIElement(
            name='primary lan table row',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[@data-test-lan-table]//div[text()='Primary LAN']")

    @property
    def spoke_search_field(self):
        """Return the 'Network Name' text-box element."""
        return pages.UIElement(
            name='add spoke search field',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator="input[data-test-toolbar-search]")

    @property
    def lan_selected(self):
        """Used to determine whether or not a LAN has been selected for a spoke."""
        return pages.UIElement(
            name='lan table selected row',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[@data-test-lan-table]//tr[contains(@class, 'is-selected')]")

    def spoke_table_row(self, spoke_name: str):
        """Return the cell element with the given name in the Add Spokes dialog.

        Args:
            spoke_name: The name of the device to select as a spoke.

        """
        return pages.UIElement(
            name='network spoke',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[contains(@class, 'add-spokes-dialog')]//td[contains(., '{}')]".format(spoke_name))


class EditSpokesDialog(SockeyeBaseRegion):
    """The 'Edit Spokes' dialog."""

    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-edit-spokes-dialog]")

    class SpokeSelectionTable(pages.EmberTable):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-edit-spokes-table]")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner(timeout=10)
        self.cancel_button.wait_for_clickable()
        return self

    @property
    def cancel_button(self):
        """Return the Cancel button on the Edit Spokes dialog."""
        return pages.UIPageLinkElement(
            name='edit spoke cancel button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-edit-spokes-cancel-button]")

    @property
    def save_button(self):
        """Return the Save button on the Edit Spokes dialog."""
        return pages.UIPageLinkElement(
            name='edit spoke save button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-edit-spokes-save-button]")

    @property
    def primary_lan_table_row(self):
        """Return the Primary LAN table row."""
        return pages.UIElement(
            name='primary lan table row',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[@data-test-lan-table]//div[text()='Primary LAN']")

    @property
    def guest_lan_table_row(self):
        """Return the Guest LAN table row."""
        return pages.UIElement(
            name='guest lan table row',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[@data-test-lan-table]//div[text()='Guest LAN']")

    @property
    def lan_selected(self):
        """Used to determine whether or not a LAN has been selected for a spoke."""
        return pages.UIElement(
            name='lan table selected row',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[@data-test-lan-table]//tr[contains(@class, 'is-selected')]")


class AddHubDialog(SockeyeBaseRegion):
    """The 'Add Hub' dialog."""

    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-add-hub-dialog]")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner(timeout=10)
        self.cancel_button.wait_for_clickable()
        return self

    @property
    def cancel_button(self):
        """Return the Cancel button on the Add Hub dialog."""
        return pages.UIPageLinkElement(
            name='add hub cancel button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-add-hub-cancel-button]")

    @property
    def add_button(self):
        """Return the Add button on the Add Hub dialog."""
        return pages.UIPageLinkElement(
            name='add hub add button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-add-hub-add-button]")

    @property
    def hub_search_field(self):
        """Return the 'Network Name' text-box element."""
        return pages.UIElement(
            name='add hub search field',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator="input[data-test-toolbar-search]")

    def hub_table_row(self, hub_name: str):
        """Return the element that contains the given name on the hub grid.

        Args:
            hub_name: The name of the device to select as a hub.

        """
        return pages.UIElement(
            name='network hub',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[contains(@class, 'add-hub-dialog')]//td[contains(., '{}')]".format(hub_name))


class NetworkSummaryDialog(SockeyeBaseRegion):
    """The summary displayed after clicking the Build button."""

    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "div.confirm-dialog")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner()
        self.confirm_button.wait_for_clickable()
        return self

    @property
    def confirm_button(self):
        """Return the Confirm button element."""
        return pages.UIPageLinkElement(
            name='network summary confirm button',
            parent=self,
            page_cls=NetworkDashboard,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-confirm-submit-button]")

    def get_summary_details(self):
        """Return the summary dialog text."""
        dialog_element = pages.UIElement(
            name='summary dialog', parent=self.parent, strategy=self.ROOT_LOCATOR[0], locator=self.ROOT_LOCATOR[1])
        return dialog_element.get_text()


class NetworksAddNetworkPage(SockeyeBasePage):
    """The 'Add VPN Network' page."""

    URL_TEMPLATE = ""
    DEFAULT_TIMEOUT = 60

    # should be URL_TEMPLATE = "edit/new" but won't be until Bill figures out the routes

    @property
    def cancel_button(self):
        """Return the Cancel button."""
        return pages.UIPageLinkElement(
            name='cancel button',
            parent=self,
            page_cls=NetworksHomePage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-edit-cancel-button]")

    @property
    def build_button(self):
        """Return the Build button."""
        return pages.UIRegionLinkElement(
            name='build button',
            parent=self,
            region_cls=NetworkSummaryDialog,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-edit-build-button]",
            region_parent=self)

    @property
    def network_name_text_field(self):
        """Return the 'Network Name' text-box element."""
        return pages.UIElement(
            name='network name text field',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-edit-network-name] input")

    @property
    def encryption_dropdown(self):
        """Return the 'Encryption Profile' drop-down element."""
        return pages.UIButtonElement(
            name='encryption dropdown', parent=self, strategy=pages.By.CSS_SELECTOR, locator="[data-test-edit-encryption]")

    @property
    def tunnel_mode_dropdown(self):
        """Return the 'Tunnel Mode' drop-down element."""
        return pages.UIButtonElement(
            name='tunnel mode dropdown', parent=self, strategy=pages.By.CSS_SELECTOR, locator="[data-test-edit-tunnel-mode]")

    @property
    def add_hub_button(self):
        """The 'Add' button on the Hub table."""
        return pages.UIRegionLinkElement(
            name='add hub button',
            parent=self,
            region_cls=AddHubDialog,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-hub-table-add-button]",
            region_parent=self)

    @property
    def add_spoke_button(self):
        """The 'Add' button on the Spokes table."""
        return pages.UIRegionLinkElement(
            name='add spoke button',
            parent=self,
            region_cls=AddSpokesDialog,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-spoke-table-add-button]",
            region_parent=self)

    @property
    def remove_spoke_button(self):
        """The 'Remove' button on the Spokes table."""
        return pages.UIButtonElement(
            name='remove spoke button',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-spoke-table-delete-button]")

    @property
    def remove_hub_button(self):
        """The 'Remove' button on the Hub table."""
        return pages.UIButtonElement(
            name='remove hub button',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-hub-table-delete-button]")

    @property
    def edit_spoke_button(self):
        """The 'Edit' button on the Spokes table."""
        return pages.UIRegionLinkElement(
            name='edit spoke button',
            parent=self,
            region_cls=EditSpokesDialog,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-spoke-table-edit-button]",
            region_parent=self)

    def set_encryption_profile(self, profile_name: str):
        """Selects the given profile from the encryption drop-down.

        Args:
            profile_name: The encryption profile to select e.g. PCI.

        """
        self.encryption_dropdown.click()
        menu_item = pages.UIButtonElement(
            name='encryption profile',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//li[contains(.,'{}')]".format(profile_name))
        menu_item.click()

    def set_tunnel_mode(self, mode_name: str):
        """Selects the given tunnel mode from the mode drop-down.

        Args:
            mode_name: The tunnel mode to select e.g. "Full Tunnel" or "Split Tunnel".

        """
        self.tunnel_mode_dropdown.click()
        menu_item = pages.UIButtonElement(
            name='tunnel mode', parent=self, strategy=pages.By.XPATH, locator="//li[contains(.,'{}')]".format(mode_name))
        menu_item.click()

    def add_spoke(self, spoke_name: str):
        """Adds a spoke from the 'Add Spokes' dialog."""
        add_spokes_dialog = self.add_spoke_button.click()
        add_spokes_dialog.spoke_search_field.set_text('{}'.format(spoke_name))
        add_spokes_dialog.spinner.wait_for_spinner()
        add_spokes_dialog.spoke_table_row(spoke_name).click()
        if not add_spokes_dialog.lan_selected.is_present():
            add_spokes_dialog.primary_lan_table_row.click()
        add_spokes_dialog.add_button.click()
        self.wait_for_page_to_load()

    def add_hub(self, hub_name: str):
        """Adds a hub from the 'Add Hub' dialog."""
        add_hub_dialog = self.add_hub_button.click()
        add_hub_dialog.hub_search_field.set_text('{}'.format(hub_name))
        add_hub_dialog.spinner.wait_for_spinner()
        add_hub_dialog.hub_table_row(hub_name).click()
        add_hub_dialog.add_button.click()
        self.wait_for_page_to_load()

    def delete_hub(self):
        """Deletes a hub from the hub list"""
        self.remove_hub_button.click()
        self.wait_for_page_to_load()

    def delete_spoke(self, spoke_name: str):
        """Delete a spoke from the spokes list"""
        self.spoke_table_row(spoke_name).click()
        self.remove_spoke_button.click()
        self.wait_for_page_to_load()

    def edit_spoke(self, spoke_name: str):
        """Edits a spoke's lan selection to Guest LAN from the 'Edit Spokes' dialog."""
        self.spoke_table_row(spoke_name).click()
        edit_spoke_dialog = self.edit_spoke_button.click()
        edit_spoke_dialog.primary_lan_table_row.click()
        edit_spoke_dialog.guest_lan_table_row.click()
        edit_spoke_dialog.save_button.click()
        self.wait_for_page_to_load()

    def spoke_table_row(self, spoke_name: str):
        """Return the cell element with the given name in Spoke table.

        Args:
            spoke_name: The name of the device to select as a spoke.

        """
        return pages.UIElement(
            name='network spoke',
            parent=self,
            strategy=pages.By.XPATH,
            locator="//div[contains(@class, 'spoke-table')]//td[contains(., '{}')]".format(spoke_name))


class DeleteNetworkDialog(SockeyeBaseRegion):
    """The prompt displayed after clicking the Delete button on NetworksHomePage."""

    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "[data-test-confirm-delete-network-dialog]")

    @property
    def delete_button(self):
        """Return the Delete button on the Delete Network confirmation popup."""
        return pages.UIPageLinkElement(
            name='delete network confirmation button',
            parent=self,
            page_cls=NetworksHomePage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-submit-button]")


class NetworksHomePage(SockeyeBasePage):
    """The page presented after clicking the 'NETWORKS' level 100 navigation tab."""

    URL_TEMPLATE = ""

    @property
    def add_button(self):
        """Return the Add button on the NETWORKS home page."""
        return pages.UIPageLinkElement(
            name='add network button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-network-table-add-button]")

    @property
    def edit_button(self):
        """Return the Edit button on the NETWORKS home page."""
        return pages.UIPageLinkElement(
            name='edit network button',
            parent=self,
            page_cls=NetworksAddNetworkPage,
            frame=self.frame,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-network-table-edit-button]")

    @property
    def delete_button(self):
        """Return the Delete button on the NETWORKS (VPN) home page."""
        return pages.UIRegionLinkElement(
            name='delete network button',
            parent=self,
            region_cls=DeleteNetworkDialog,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-network-table-delete-button]",
            region_parent=self)

    @property
    def networks_table(self):
        return NetworksTable(self)

    def clear_networks_table(self):
        """Deletes all existing networks from the Networks Table."""
        net_table = self.networks_table
        if net_table.num_rows > 0:
            net_table.select_all_checkbox.click()
            confirm_dialog = self.delete_button.click()
            confirm_dialog.delete_button.click()
            self.spinner.wait_for_spinner()

    def network_checkbox(self, network_name: str):
        """Return the row selection checkbox on the networks grid."""
        locator = "//a[contains(text(), '{}')]/../../..//span[contains(@class, 'checkbox')]".format(network_name)
        return pages.UIElement(name='network checkbox', parent=self, strategy=pages.By.XPATH, locator=locator)

    def network_status(self, network_name: str):
        """Return the online icon on the networks grid."""
        locator = "//a[contains(text(), '{}')]/../../..//i[contains(@class, 'online')]".format(network_name)
        return pages.UIElement(name='network status', parent=self, strategy=pages.By.XPATH, locator=locator)

    @property
    def network_refresh(self):
        """The 'Remove' button on the Hub table."""
        return pages.UIButtonElement(
            name='network refresh button',
            parent=self,
            strategy=pages.By.CSS_SELECTOR,
            locator="[data-test-toolbar-refresh-button]")

    def network_online(self, network_name: str):
        self.network_refresh.click()
        self.spinner.wait_for_spinner()
        try:
            status = self.network_status(network_name).is_present()
        except WaitTimeoutError:
            return False
        except ActionNotPossibleError:
            return False
        except Exception as ex:
            logger.warning('Encountered unexpected exception! {}'.format(ex))
            return False

        return status

    def delete_network(self, network_name: str):
        """Delete a network from the networks grid."""
        wait(150, 10).for_call(self.network_online, network_name).to_be(True)
        self.network_checkbox(network_name).click()
        confirm_dialog = self.delete_button.click()
        confirm_dialog.delete_button.click()
        self.spinner.wait_for_spinner()
