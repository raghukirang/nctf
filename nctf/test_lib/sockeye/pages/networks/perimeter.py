"""This module is a mapping of the NETWORKS > VPN page."""

import logging
import time

from nctf.libs.common_library import WaitTimeoutError
from nctf.test_lib.base import pages
from nctf.test_lib.base.pages.exceptions import ActionNotPossibleError
from nctf.test_lib.sockeye.pages.sockeye_base import SockeyeBasePage
from nctf.test_lib.sockeye.pages.sockeye_base import SockeyeBaseRegion
from nctf.test_lib.utils.waiter import wait

logger = logging.getLogger(pages.make_short_qualified_name(__name__))
