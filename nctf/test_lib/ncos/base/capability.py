from nctf.test_lib.base.objects import NCTFObject
from arm4.serialization.routers import RouterCapability


class NCOSRouterCapabilities(NCTFObject):
    def __init__(self):
        self.id = None
        self.ieee_802_1q = None
        self.ieee_802_1x = None
        self.bgp = None
        self.fips = None
        self.full_ntp = None
        self.gre = None
        self.ippt = None
        self.ips_ids = None
        self.ipsec = None
        self.lan_ports = None
        self.onebuttonupgrade = None
        self.openvpn = None
        self.pbr = None
        self.qos = None
        self.sfp_ports = None
        self.snmp = None
        self.static_routing = None
        self.vpn = None
        self.vrrp = None
        self.wan_ports = None
        self.wifi = None
        self.wifi_as_wan = None
        self.wireless_client = None
        self.zfw = None
        self.autovpn = None
        self.default_lan_ip = None
        self.default_lan_only = None
        self.default_serial_baud = None
        self.dfs = None
        self.gpio_10 = None
        self.gpio_2 = None
        self.ignition_sense = None
        self.platform = None
        self.upgrade_method = None
        self.webroot = None
        self.wifi_2_4 = None
        self.wifi_5 = None
        self.zscaler_is = None
        self.zscaler_swg = None

    def load_from_capabilities(self, capabilities: RouterCapability):
        self.id = capabilities.id
        self.ieee_802_1q = capabilities.ieee_802_1q
        self.ieee_802_1x = capabilities.ieee_802_1x
        self.bgp = capabilities.bgp
        self.fips = capabilities.fips
        self.full_ntp = capabilities.full_ntp
        self.gre = capabilities.gre
        self.ippt = capabilities.ippt
        self.ips_ids = capabilities.ips_ids
        self.ipsec = capabilities.ipsec
        self.lan_ports = capabilities.lan_ports
        self.onebuttonupgrade = capabilities.onebuttonupgrade
        self.openvpn = capabilities.openvpn
        self.pbr = capabilities.pbr
        self.qos = capabilities.qos
        self.sfp_ports = capabilities.sfp_ports
        self.snmp = capabilities.snmp
        self.static_routing = capabilities.static_routing
        self.vpn = capabilities.vpn
        self.vrrp = capabilities.vrrp
        self.wan_ports = capabilities.wan_ports
        self.wifi = capabilities.wifi
        self.wifi_as_wan = capabilities.wifi_as_wan
        self.wireless_client = capabilities.wireless_client
        self.zfw = capabilities.zfw
        self.autovpn = capabilities.autovpn
        self.default_lan_ip = capabilities.default_lan_ip
        self.default_lan_only = capabilities.default_lan_only
        self.default_serial_baud = capabilities.default_serial_baud
        self.dfs = capabilities.dfs
        self.gpio_10 = capabilities.gpio_10
        self.gpio_2 = capabilities.gpio_2
        self.ignition_sense = capabilities.ignition_sense
        self.platform = capabilities.platform
        self.upgrade_method = capabilities.upgrade_method
        self.webroot = capabilities.webroot
        self.wifi_2_4 = capabilities.wifi_2_4
        self.wifi_5 = capabilities.wifi_5
        self.zscaler_is = capabilities.zscaler_is
        self.zscaler_swg = capabilities.zscaler_swg
