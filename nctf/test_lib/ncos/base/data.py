import json


class NCOSFirmwareInfo:
    """ I am questioning the major value of this, but I like it for the autocomplete it provides """

    def __init__(self, jsonstring: str = None, fw_info: dict = None):
        if jsonstring:
            self.data = json.loads(jsonstring)
        elif fw_info:
            self.data = fw_info
        else:
            assert False, "You must provide either json string or fw_info dict"

        self.build_date = self.data["build_date"]
        self.build_type = self.data["build_type"]
        self.build_version = self.data["build_version"]  # for CP builds this is a git SHA of the build
        self.custom_defaults = self.data["custom_defaults"]
        self.fw_update_available = self.data["fw_update_available"]
        self.major_version = self.data["major_version"]
        self.manufacturing_upgrade = self.data["manufacturing_upgrade"]
        self.minor_version = self.data["minor_version"]
        self.patch_version = self.data["patch_version"]
        self.upgrade_major_version = self.data["upgrade_major_version"]
        self.upgrade_minor_version = self.data["upgrade_minor_version"]
        self.upgrade_patch_version = self.data["upgrade_patch_version"]

    def is_dev(self) -> bool:
        return "dev" in self.build_type.lower()

    def is_field(self) -> bool:
        return "field" in self.build_type.lower()


class NCOSManufacturingInfo:
    def __init__(self, jsonstring: str = None, manufacturing: dict = None):
        if jsonstring:
            self.data = json.loads(jsonstring)
        elif manufacturing:
            self.data = manufacturing
        else:
            assert False, "You must provide either json string or manufacturing dict"
        self.board_ID = self.data["board_ID"]
        self.mftr_date = self.data["mftr_date"]
        self.serial_num = self.data["serial_num"]


class NCOSProductInfo:
    """ helper for product_info type things. provides auto-complete """

    def __init__(self, jsonstring: str = None, product_info: dict = None):
        if jsonstring:
            self.data = json.loads(jsonstring)
        elif product_info:
            self.data = product_info
        else:
            assert False, "You must provide either json string or product_info dict"

        self.company_name = self.data["company_name"]
        self.company_url = self.data["company_url"]
        self.copyright = self.data["copyright"]
        self.mac0 = self.data["mac0"]
        self.manufacturing = NCOSManufacturingInfo(self.data["manufacturing"])
        self.product_name = self.data["product_name"]
