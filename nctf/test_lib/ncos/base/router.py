from datetime import datetime
import logging
import re
import types
from typing import Callable
from typing import List
from typing import Union

import requests

from nctf.test_lib.base.objects import NCTFObject
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPIFixture
from nctf.test_lib.utils.sshclient.sshclient_paramiko import SSHShellClient
from nctf.test_lib.utils.waiter import wait
from nctf.test_lib.utils.waiter import WaitTimeoutError
from paramiko import AutoAddPolicy
from paramiko import SSHClient
from paramiko import SSHException
from semantic_version import Version

from .capability import NCOSRouterCapabilities
from .firmware_info import NCOSFirmwareInfo

# have been unable after several tries to get this to work.  Try not to use NCOSLANClient in this file.
# from .client import NCOSLANClient

logger = logging.getLogger("ncos.router")


class NCOSRouterException(Exception):
    pass


class NCOSRouterLog(NCTFObject):
    def __init__(self, time: int, level: str, source: str, message: str):
        """Data class for NCOS Router Logs"""
        self.time = datetime.fromtimestamp(time)
        self.level = level
        self.source = source
        self.message = message

    @staticmethod
    def filter_log(search: Union[Callable, str], logs: List["NCOSRouterLog"]) -> \
            List["NCOSRouterLog"]:
        """Filter router logs and returns matching lines.  Can search by lambda or message substring

        Args:
            search:  message substring to match OR a lambda that accepts NCOSRouterLog items and returns True on match
            logs: list[NCOSRouterLog] to search for matches
        Returns:
            list of matches (empty list if no matches)"""

        if isinstance(search, str):
            def searcher(x: "NCOSRouterLog"):
                return search.lower() in x.message.lower()
        elif isinstance(search, types.FunctionType):
            searcher = search
        else:
            raise TypeError("Search must be a string or callable that accepts NCOSRouterLog!")
        assert logs

        matches = list()
        for log in logs:
            if searcher(log):
                matches.append(log)
        return matches


class NCOSRouter(NCTFObject):
    """Class that defines the common router interface.

    This should be the base class for every router class specialization for physical and virtual routers
    regardless of where they exist (developer's desk, AWS virtual, test rack etc.)

    """

    def __init__(self,
                 remote_admin_host: str = '192.168.0.1',
                 remote_admin_port: int = 443,
                 remote_admin_ssh_port: int = 22,
                 admin_user: str = 'admin',
                 admin_password: str = 'welcome2cp',
                 cproot_password: str = '',
                 default_lan_ip: str = '192.168.0.1',
                 product: str = '3200v',
                 default_web_protocol: str = 'https'):
        # default ip address for administering device.  This is required for IPPT-default products such as CBA550.
        # Tests using CBA550 need to know the IP address to use for talking to the router's LAN because it is
        # IP Passthrough mode by default which means the client's address has *nothing* to do with the LAN IP
        # address the router will respond to.  As a convenient abstraction, if tests always use this to track
        # the LAN IP address of a router then the NCOSRouter object will know what LAN IP to use in various scenarios.

        # these data elements must be provided by the concrete class that knows how to get them.
        self.remote_admin_host = remote_admin_host
        self.remote_admin_port = remote_admin_port
        self.remote_admin_ssh_port = remote_admin_ssh_port
        self.admin_user = admin_user
        self.admin_password = admin_password
        self._cproot_password = cproot_password
        self.default_lan_ip = default_lan_ip
        self.product = product
        self.default_web_protocol = default_web_protocol
        # if a router has an associated client that can be used, it could be set here, but it is optional.
        self.default_client = None  # no type specified here, but must be NCOSLANClient interface
    
    @property
    def cproot_password(self) -> str:
        if not self._cproot_password:
            return f'1415{self.mac.replace(":","")[-4:]}'
        else:
            return self._cproot_password

    @property
    def bootid(self) -> str:
        client = self.get_rest_client()
        result = client.status_system.get(name='bootid')['data']
        return result

    @property
    def fw_version(self) -> Version:
        client = self.get_rest_client()
        fw_info = client.status_fw_info.get()['data']
        fw_str = f"{fw_info['major_version']}.{fw_info['minor_version']}.{fw_info['patch_version']}"
        return Version(fw_str)

    @property
    def client_analytics_enabled(self) -> bool:
        client = self.get_rest_client()
        client_analytics = client.config_stats_client_usage.get(name='enabled')['data']
        return client_analytics

    @property
    def ncm_state(self) -> str:
        client = self.get_rest_client()
        ecm_state = client.status_ecm_state.get()['data']
        return ecm_state

    @property
    def sdk_mode(self) -> Union[str, None]:
        """Checks the data for status/system/sdk/mode"""
        client = self.get_rest_client()
        sdk_mode = client.status_system.get(name='sdk')['data']['mode']
        return sdk_mode

    @property
    def ncm_client_id(self) -> str:
        client = self.get_rest_client()
        client_id = client.status_ecm_client_id.get()['data']
        return client_id

    @property
    def pgateway_substate(self) -> str:
        client = self.get_rest_client()
        sub_state = client.status_overlay_pgateway.get(name='sub_state')['data']
        return sub_state

    @property
    def lans(self) -> list:
        client = self.get_rest_client()
        lans = client.config_lan.get()['data']
        return lans

    @property
    def wan_state_history(self) -> list:
        client = self.get_rest_client()
        wan_states = client.status_stats.get(name='wan_state_history/utc_history/states')['data']
        return wan_states

    @property
    def mac(self) -> str:
        client = self.get_rest_client()
        mac = client.status_product_info.get(name='mac0')['data']
        return mac

    def exec_command(self, command: Union[List[str], str],
                     user: str = None, password: str = None, port: int = 0) -> str:
        """Run a command on the router.

        Run a command on the router by any means appropriate CLI, router_control, API, direct SSH
        and return the resulting output as a string.
        Params:
            command: single command as string or list of commands to be run in a single session
        Returns:
              output from commands
        """

        # did we get one command or a list of them?
        if not isinstance(command, list):
            commands = [command]
        else:
            commands = command

        user = user or self.admin_user
        password = password or self.admin_password
        port = port or self.remote_admin_ssh_port

        client = SSHShellClient(host=self.remote_admin_host, username=user, password=password,
                                port=port)
        results = client.do_complex_cmd(cmd=commands)
        result = "\r\n".join(results)
        return result

    def get_fw_info(self, protocol: str = None) -> NCOSFirmwareInfo:
        """ this should be a real implementation in this base class.
        :return: fw_info structure obtained from router
        """
        protocol = self.default_web_protocol if protocol is None else protocol

        # beware -- a CBA850 can have wan_ports but by default in factory_reset condition it does not!
        if self.get_router_capabilities().default_lan_only:
            # LAN-ONLY products require a proxy indirection through the LAN (client).
            # A test can define an arbitrary topology in which architecturally we cannot (currently) know which
            # client to use unless it is provided by the caller.  For now, I am not providing magic routines, and
            # not providing router-provided self.router_control_client, though I like the concept.
            # For now, test authors should explicitly use a client to invoke this for LAN-only products.
            raise NotImplementedError("get_fw_info for default_lan_only products must be invoked from a client")
        else:
            rest_client = self.get_rest_client(protocol=protocol)
            json_result = rest_client.status_fw_info.get()

        fwinfo = NCOSFirmwareInfo()
        success = fwinfo.load_json_response(json_result)
        if not success:
            fwinfo = None
        return fwinfo

    def get_rest_client(self, protocol: str = None) -> RouterRESTAPI:
        """Get a REST Client to the router to begin communication.

        Note: Currently executes over remote admin.

        """
        protocol = self.default_web_protocol if protocol is None else protocol

        fixture = RouterRESTAPIFixture()

        client = fixture.connect(protocol, self.remote_admin_host, self.remote_admin_port)
        client.authenticate(self.admin_user, self.admin_password)
        return client

    def get_router_capabilities(self, *args, **kwargs) -> NCOSRouterCapabilities:
        """Get the device capabilities. e.g. {'lan_ports': 4}"""
        # this must be overridden in the subclasses since they know where and how to access the capabilities for a
        # particular router instance.  i.e. ARM4 knows where the capability originates for ARM4
        raise NotImplementedError()

    def get_ssh_client(self) -> SSHClient:
        """Get an SSH Client to the router in order to execute arbitrary commands on the router."""

        ssh_port = self.remote_admin_ssh_port
        ssh_host = self.remote_admin_host
        ssh_user = self.admin_user
        ssh_pass = self.admin_password

        client = "did not get an ssh client!"
        attempts = 0
        max_attempts = 3
        while attempts < max_attempts:
            attempts = attempts + 1
            logger.debug(f"attempt {attempts} of {max_attempts} to obtain a useful SSHClient")
            try:
                client = SSHClient()
                client.set_missing_host_key_policy(AutoAddPolicy())
                client.connect(
                    username=ssh_user,
                    password=ssh_pass,
                    hostname=ssh_host,
                    port=ssh_port,
                    allow_agent=False,
                    look_for_keys=False)
                break
            except SSHException as sse:
                # why would this fail? I've had at least one failure in a case where it should not have.
                # SSHException('Error reading SSH protocol banner')
                if attempts < max_attempts and "Error reading SSH protocol banner" in str(sse):
                    logger.error(f"SSHException: {ssh_user}, {ssh_pass}, {ssh_host}, {ssh_port}")
                    continue
                raise

        return client

    def enable_cproot(self):
        """Enables cproot SSH access to a router."""
        payload = {'data': 'true'}

        client = self.get_rest_client()

        if self.fw_version < Version('6.1.0'):
            client.put('/api/config/firewall/remote_admin/techsupport_access', 200, data=payload)
        else:
            client.put('/api/control/system/techsupport_access', 200, data=payload)

    def get_value(self, value_path: str, protocol: str = None):
        """Does API get from router
        Args:
             value_path (str): path to the value to get. Should start with '/' such as '/config'
             protocol (str): http or https to use in the api request
        Returns:
             value obtained without curl/REST response wrapper or None if there was an error or failure
        """
        protocol = self.default_web_protocol if protocol is None else protocol

        rest = self.get_rest_client(protocol=protocol)
        raw_json = rest.root.get(f"api{value_path}")
        if not raw_json["success"]:
            logger.warning(f"get_value({value_path}) did not succeed! {str(raw_json)}")
            return None
        value = raw_json["data"]
        logger.info(f"get_value({value_path}) = {value}")
        return value

    def get_base_router_url(self, protocol: str = None, use_lan: bool = False) -> str:
        protocol = self.default_web_protocol if protocol is None else protocol

        ip = self.remote_admin_host
        port = self.remote_admin_port
        if use_lan:
            ip = self.default_lan_ip
            port = 80 if protocol == "http" else 443
        url = f"{protocol}://{ip}:{port}"
        return url

    def set_value(self, value_path, value, protocol: str = None):
        protocol = self.default_web_protocol if protocol is None else protocol

        rest = self.get_rest_client(protocol=protocol)
        rest.root.update(name=f"api{value_path}", update=value)

    def set_stream_server(self, stream_host: str):
        client = self.get_rest_client()
        client.config_ecm_server_host.update(update=f"{stream_host}")
        logger.info(f'Successfully set stream host to {stream_host}')

    def register(self, ncm_username: str, ncm_password: str):
        client = self.get_rest_client()
        client.control_ecm_register.update(update={'username': ncm_username, 'password': ncm_password})

        try:
            wait(60, 5).for_call(lambda: self.ncm_state).to_equal('connected')
        except WaitTimeoutError as e:
            raise NCOSRouterException(f'{self} timed out waiting for \'connected\' state on '
                                      f'/api/status/ecm/state: \n{e}')
        logger.debug(f'{self} shows as \'connected\' for /api/status/ecm/state')
        logger.info(f'{self} successfully registered to NCM')

    def unregister(self):
        client = self.get_rest_client()
        client.control_ecm_unregister.update(update='true')

    @staticmethod
    def format_log_data(log_data: list) -> List[NCOSRouterLog]:
        logs = []
        for l in log_data:
            logs.append(NCOSRouterLog(time=l[0], level=l[1], source=l[2], message=l[3]))
        return logs

    def get_logs(self) -> List[NCOSRouterLog]:
        """Retrieve the list of the router's logs"""
        client = self.get_rest_client()
        r = client.status_log.get()

        logs = self.format_log_data(log_data=r['data'])
        return logs

    def license_service_started(self) -> bool:
        rest = self.get_rest_client()
        state = rest.status_system.get(name="services/license/state")["data"]
        return "started" in state

    def clear_logs(self) -> None:
        """Clear the router's logs"""
        client = self.get_rest_client()
        client.control_log.update('', {'clear': True})

    def write_logs_to_file(self, path_to_file: str, lan_client=None):
        """Write the router's logs to a given path, because this is intended also for post-test triage,
        attempt to use the associated default_client if one is present and necessary.

        Args:
             path_to_file:  path including filename
             lan_client:  optional host to use to obtain logs from the router."""

        if lan_client or self.get_router_capabilities().default_lan_only:
            lan_client = lan_client or self.default_client
            logs = lan_client.router_get_logs(router=self)
        else:
            logs = self.get_logs()

        with open(path_to_file, 'w+') as f:
            for log in logs:
                f.write(f'{log.time} {log.level} {log.source} {log.message}\n')

    def stabilize_freshly_booted_router_wan(self, timeout=300):
        """Routers with SIMs installed are quite unpredictable for connectivity after rebooting.  Here's a control
        point to allow us to poll the device while we wait for the modem connection to stabilize and enable WAN-based
        connectivity.

        For routers with wired WAN, we don't want the WAN primary device to be modem.
        For routers without wired WAN, I'm not sure what the right answer is, so not doing anything yet.
        For AP, not doing anything (what would we need?)

        If the router is not freshly booted, this should quickly return."""

        if self.get_router_capabilities().lan_ports == 0:
            return  # doesn't apply to AP at this time (might in the future)

        if self.get_router_capabilities().wan_ports == 0:
            # most products that have default_lan_only hit here, ibr350, ibr200, cba550.  CBA850 doesn't.
            # no warning necessary here
            return

        if self.get_router_capabilities().default_lan_only:
            # CBA850 has 1 wan+1 lan or 2 lan2 possible as config.
            # Test could configure router into a state where waiting below would be appropriate.
            # For now assume default state - add check later when it becomes necessary.
            logger.info("stabilize_freshly_booted_router_wan() is returning without waiting because default_lan_only.")
            return

        # yes, we are not actually looking for a SIM. This should be quick to check even without it.
        # would need to standardize on router tag values if we want to check for SIM presence without talking to the
        # router.  This is really fast if the router has no SIM (about as fast as looking for a SIM and simpler to
        # read)

        def wan_state_is_correct():
            try:
                wan_device = self.get_value(value_path="/status/wan/primary_device")
                wan_state = self.get_value(value_path="/status/wan/connection_state")
                return "ethernet" in wan_device.lower() and wan_state.lower() == "connected"
            except Exception as exc:
                # yes I really do mean to eat everything because this is intentionally ignoring bad states
                logger.warning(f"Ignoring exception {type(exc)} while waiting for WAN to be connected to ethernet")
                return False

        logger.info("Waiting for wired ethernet wan connection to be established")
        wait(timeout, 10).for_call(lambda: wan_state_is_correct()).to_equal(True)

    def wait_for_reboot(self, bootid_before: str = None, timeout: int = 300, interval: int = 5,
                        wait_for_wired_wan: bool = True):
        bootid_before = bootid_before or self.bootid

        def bootid_check(default: str):
            bootid_after = default
            try:
                bootid_after = self.bootid
            except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as requests_exception:
                logger.debug(f"bootid_check ignoring {str(requests_exception)}")
            logger.info(f"bootid is {bootid_after}")
            return bootid_after

        wait(timeout, interval).for_call(lambda: bootid_check(bootid_before)).to_not_equal(bootid_before)
        # the check below does *NOT* guarantee the router is completely ready, but will instead filter out
        # major class of intermittent failures.  Specific exceptions can be failover/failback and/or WAN connection
        # "adjustments" as the SIM/modem connection competes with or holds off the wired WAN connection.
        logger.info("bootid changed, system rebooted.  Waiting for router services to start")
        wait(60, 5).for_call(lambda: self.license_service_started()).to_be(True)

        if wait_for_wired_wan:
            self.stabilize_freshly_booted_router_wan()

    def reboot(self):
        logger.info(f'{self} rebooting')
        client = self.get_rest_client()
        bootid_before = self.bootid
        client.control_system.update(name='reboot', update='true')
        self.wait_for_reboot(bootid_before=bootid_before)
        logger.info(f'{self} came back from reboot and is now available')

    def sdk_data(self) -> Union[dict, None]:
        """ Returns the data for status/system/sdk """
        client = self.get_rest_client()
        sdk_data = client.status_system.get(name='sdk')['data']
        return sdk_data

    def trend_data(self) -> Union[dict, None]:
        """ Returns the ata for status/security/ips """
        client = self.get_rest_client()
        trend_data = client.status_security.get(name='ips')['data']
        return trend_data

    def app_analytics_data(self) -> Union[dict, None]:
        client = self.get_rest_client()
        analytics_data = client.control_netflow_ulog.get(name='analytics')['data']
        return analytics_data

    def enable_mac_filtering(self):
        data = {'enabled': 'true'}
        client = self.get_rest_client()
        client.config_firewall.update(name='macfilter', update=data)

    def disable_mac_filtering(self):
        data = {'enabled': 'false'}
        client = self.get_rest_client()
        client.config_firewall.update(name='macfilter', update=data)

    def restart_ncm(self):
        client = self.get_rest_client()
        client.control_ecm_restart.update(update='true')

    def disable_ncm(self):
        client = self.get_rest_client()
        client.config_ecm.update(name='enabled', update='false')

    def features(self) -> dict:
        client = self.get_rest_client()
        features = client.status_feature.get()['data']
        return features

    def ping(self, target: str, interface: str = "eth0", loss_threshold: int = 49, count: int = 3) -> bool:
        """pings target from router
        Args:
            target: ip / name to ping
            interface: client interface to use for ping (some clients can have more than one outbound interface)
            loss_threshold: must be <= threshold% lost pings to succeed
            count: number of pings to attempt before deciding success/failure
        Returns
            True if ping is successful >= threshold% of attempts
            False otherwise"""
        ping_cmd = f"ping -I {interface} -c {count} {target}"
        out = self.exec_command(ping_cmd)

        # 18.04 gives "name or service"  mint and older ubuntu gives "unknown host". This is ony informational.
        if "Name or service not known" in out or "Unreachable" in out or "unknown host" in out.lower():
            logger.warning(f"{ping_cmd} gave output: {out}")

        # what's the right percentage here?  We'll start with perfection and adjust when needs require it.
        percent_lost = 100
        match = re.search(" ([0-9]+)% packet loss", out)
        if match:
            percent_lost = int(match.group(1))
        logger.info(f"ping {target} lost {percent_lost}% packets")
        return percent_lost <= loss_threshold

    # TODO: Standardize on the function signatures here before exposing them.

    # def load_specific_fw(self,
    #                      git_branch: str = None,
    #                      build_type: str = None,
    #                      version_number: str = None,
    #                      git_sha: str = None,
    #                      build_date: str = None,
    #                      local_dir: str = None,
    #                      factory_reset: bool = False):
    #     """ Load a specific version of firmware onto the router.
    #
    #     For use when a test knows it needs 6.1.0 or 4.5.3 or some other specific version of firmware for upgrade,
    #     downgrade, or migration testing. Specify the parts you know and the system will do the best it can
    #     to meet the need
    #
    #     """
    #     raise NotImplementedError()
    #
    # def send_config_file(self, config_file_path: str, reboot: bool = False, binary: bool = False) -> None:
    #     """ read file from filesystem (assets search?) and send as config
    #         if reboot is true, will turn off readonly config explicitly first.
    #         reboot of true will accept versioned config files.
    #         reboot of false will not change readonly config setting.
    #         Raises exception if any failure detected.
    #         Implementation question -- is this text file or binary file or both? """
    #     raise NotImplementedError
    #
    # def send_config(self, config: str, reboot: bool = False, binary: bool = False) -> None:
    #     """ give a config blob (string or bytes) send it to the router.
    #         if reboot is true, will turn off readonly config explicitly first.
    #         reboot of true will accept versioned config files.
    #         reboot of false will not change readonly config setting.
    #         Raises exception if any failure detected.
    #         Implementation question -- is this text file or binary file or both?
    #         Implementation question -- when is this necessary?
    #     """
    #     raise NotImplementedError
    #
    # def get_installed_options(self, *args, **kwargs):
    #     """Get a list of installed options on the device. e.g. 'vzw modem'"""
    #     raise NotImplementedError()
    #
    #
    # """ Convenience Routines.  Sure, you can roll your own, but should you have to? """
    #
    # def factory_reset(self, *args, **kwargs) -> bool:
    #     """ tell the router to do factory reset.  Waits for router to come to ready before returning.
    #         might use router_control and/or API and/or CLI depending on router type. """
    #
    #     raise NotImplementedError
    #
    # def get_product_info(self, *args, **kwargs) -> NCOSProductInfo:
    #     """ this should be a real implementation in this base class.
    #     :return: product_info structure obtained from router
    #     """
    #     raise NotImplementedError
    #
