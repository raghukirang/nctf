import json
import logging
from paramiko import AutoAddPolicy
from paramiko import SSHClient
from paramiko import SSHException
import re
from scp import SCPClient
from typing import List
from typing import Tuple
from typing import Union
from typing import Dict

from nctf.test_lib.base.objects import NCTFObject
from nctf.test_lib.utils.sshclient.sshclient_paramiko import SSHShellClient
from nctf.test_lib.utils.waiter import wait
from nctf.test_lib.utils.waiter import WaitTimeoutError

from .firmware_info import NCOSFirmwareInfo
from .router import NCOSRouter
from .router import NCOSRouterLog


logger = logging.getLogger("ncos.client")


class NCOSClientError(Exception):
    pass


class NCOSNetworkClient(NCTFObject):
    """Base class for any object representation of a Network Client"""

    def get_ssh_client(self) -> SSHClient:
        raise NotImplementedError()

    def exec_command(self, command: str, timeout: int = None, user: str = None, password: str = None
                     ) -> Tuple[str, str]:
        raise NotImplementedError("Must be implemented by concrete class")

    def copy_local_file_to_client(self, local_path: str, remote_path: str):
        raise NotImplementedError("Must be implemented by concrete class")

    def router_get_fw_info(self, router: NCOSRouter, protocol: str)->NCOSFirmwareInfo:
        raise NotImplementedError("NCOSNetworkClient doesn't know how to talk to routers?  Use NCOSLANClient or ???")

    def router_get_value(self, router: NCOSRouter, value_path: str, protocol: str = "https"):
        raise NotImplementedError("NCOSNetworkClient doesn't know how to talk to routers?  Use NCOSLANClient or ???")

    def router_set_value(self, router: NCOSRouter, value_path: str, value, protocol: str = "https"):
        raise NotImplementedError("NCOSNetworkClient doesn't know how to talk to routers?  Use NCOSLANClient or ???")


class NCOSLANClient(NCOSNetworkClient):
    """Base class for any object representation of a Network Client hardwired and on the LAN of an NCOSRouter"""
    def __init__(self):
        self.linux_namespace = ""
        self.id = ""
        self.ssh_port = 0
        self.ssh_host = ""
        self.ssh_user = ""
        self.ssh_pass = ""

    def router_exec_command(self, command: Union[List[str], str],
                            router: NCOSRouter,
                            host: str = None, user: str = None, password: str = None, port: int = 0,
                            timeout: int = 30, auto_dhcp=True) -> str:
        """Use this client to send cli commands to router."""

        ssh_cmd = f"sshpass -p {router.admin_password} ssh {router.admin_user}@{router.default_lan_ip} " \
                  f'-o "ConnectTimeout=15" ' \
                  f'-o "UserKnownHostsFile=/dev/null" ' \
                  f'-o "ServerAliveInterval=5" ' \
                  f'-o "ServerAliveCountMax=5" ' \
                  f'-o "stricthostkeychecking=no"'

        if self.linux_namespace != "":
            # switch into the namespace before ssh onto the router
            commands = [f"sudo ip netns exec {self.linux_namespace} bash", ssh_cmd]
        else:
            commands = [ssh_cmd]

        if isinstance(command, str):
            command = [command]

        commands.extend(command)

        user = user or self.ssh_user
        password = password or self.ssh_pass
        port = port or self.ssh_port
        host = host or self.ssh_host

        shell_client = SSHShellClient(host=host, username=user, password=password, port=port)
        shell_client.change_timeout(changeto=timeout)  # there is more than one timeout, this is the timeout reading.
        result = "\r\n".join(shell_client.do_complex_cmd(cmd=commands))

        if "curl: (7) Couldn't connect to server" in result and auto_dhcp:
            logger.warning("AUTO RETRY! Will retry command after attempting to renew DHCP")
            self.refresh_dhcp()
            result = "\r\n".join(shell_client.do_complex_cmd(cmd=commands))

        return result

    def router_get_fw_info(self, router: NCOSRouter, protocol: str = "https") -> NCOSFirmwareInfo:
        """uses router's LAN address and router credentials to obtain fw_info"""
        json_result = self.router_get_value(router=router, value_path="/status/fw_info", protocol=protocol)
        fwinfo = NCOSFirmwareInfo()
        success = fwinfo.load_json_response(json_result)
        if not success:
            fwinfo = None
        return fwinfo

    def router_get_logs(self, router: NCOSRouter, protocol: str = "https") -> List[NCOSRouterLog]:
        """obtain logs from the router, return as a list of """
        r = self.router_get_value(router=router, value_path="/status/log", protocol=protocol)
        logs = router.format_log_data(log_data=r)
        return logs

    @staticmethod
    def _extract_json_data_value(raw: str) -> str:
        """Given shell session as string that contains some curl output, grab the value of the curl data element
        as a python value"""
        success_start = '{"success": '  # could look for success==True here, but skipping for now
        json_index = raw.rfind(success_start)
        assert json_index >= 0
        last_brace = raw.rfind("}")
        clean_json_string = raw[json_index:last_brace + 1]
        json_value = json.loads(clean_json_string)

        # unsuccessful yields no result.  Caller must be able to accommodate appropriately.
        result = None
        if "data" in json_value:
            result = json_value["data"]

        return result

    def router_get_value(self, router: NCOSRouter, value_path: str, protocol: str = "https",
                         user: str = None, password: str = None, target_ip: str = None,
                         timeout=600) -> Union[List, str, Dict]:
        """get a value from router using a client to do the interaction.  Useful for LAN-only products or
        tests that need to work on all products the same.
        TODO: for protocol other than https, there is a API hole that there is not (yet) a standard way
        for the router to say 'my remote_admin_port_http value is 12345'
        Args:
            router: the router to access
            value_path: the config store path for the item to get without /api, such as /status/fw_info
            protocol: the protocol 'http' or 'https' to use.
            user: optional user if other than router is to be used
            password: optional password if other than router.admin_password
            target_ip: optional ip address if other than router.default_lan_ip
            timeout: how long to attempt before giving up

        Returns:
            whatever the router gives us, stripped of curl return dictionary.  i.e. no {'success':true, 'data':x} """
        user = user or router.admin_user
        password = password or router.admin_password
        target_ip = target_ip or router.default_lan_ip
        value_path = value_path.lstrip("/")

        curl_command = f"curl -G --insecure -u {user}:{password} {protocol}://{target_ip}/api/{value_path}"

        # side-effect captures
        last_error = ""
        output = ""

        # router with SIM especially may require retries.  Hopefully most of the time this should not require them.
        def attempt_curl(cmd: str) -> bool:
            nonlocal last_error
            nonlocal output
            output = self.exec_command(command=cmd, auto_dhcp=True)
            if 'success' not in output:
                last_error = f"curl did not return 'success' in output.  output={output}"
                return False
            if '"success": false' in output:
                last_error = f"curl returned 'success: false' in output.  output={output}"
                return False
            return True

        try:
            wait(timeout=timeout, interval=5).for_call(lambda: attempt_curl(curl_command)).to_equal(True)
        except WaitTimeoutError:
            logger.warning(last_error)
            # don't remap the error, may need ability to distinguish by users of this method between
            # "timed out" and "the client is dead"
            raise

        result = self._extract_json_data_value(output)
        return result

    @staticmethod
    def convert_value_for_curl(value):
        if isinstance(value, bool) or isinstance(value, int) or isinstance(value, float):
            # literal numbers are ok
            # lower case for bool
            return str(value).lower()
        if isinstance(value, str):
            escape_quotes = value.replace('"', '\"')
            quote_wrapped = f'"{escape_quotes}"'
            return quote_wrapped
        if isinstance(value, dict):
            separator = "{"
            result = ""
            for itemkey, itemvalue in value.items():
                curl_value = NCOSLANClient.convert_value_for_curl(itemvalue)
                result = f'{result}{separator}"{itemkey}":{curl_value}'
                separator = ","
            result = f"{result}}}"
            return result
        if isinstance(value, list):
            separator = '['
            result = ""
            for item in value:
                curl_value = NCOSLANClient.convert_value_for_curl(item)
                result = f'{result}{separator}{curl_value}'
                separator = ','
            result = f'{result}]'
            return result

        # what did the caller give us?
        raise NotImplementedError(f"convert_value_for_curl: Don't have a special case for value of type {type(value)}"
                                  f" while formatting value {value}")

    def router_set_value(self, router: NCOSRouter, value_path: str, value, protocol: str = "https"):
        value_path = value_path.lstrip("/")
        curl_value = self.convert_value_for_curl(value)
        curl_command = f"curl -X PUT --insecure -u {router.admin_user}:{router.admin_password} " \
                       f"{protocol}://{router.default_lan_ip}/api/{value_path} -d data='{curl_value}'"
        output = self.exec_command(command=curl_command, auto_dhcp=True)

        if "success" not in output:
            raise NCOSClientError(f"curl did not return 'success' in output.  output={output}")
        if '"success": false' in output:
            raise NCOSClientError(f"curl returned 'success: false' in output.  output={output}")

        result = self._extract_json_data_value(output)
        return result

    def get_ssh_client(self) -> SSHClient:
        """Construct and return an SSHClient to the ARMv4NetworkClient"""

        client = "did not get an ssh client!"
        attempts = 0
        max_attempts = 3
        while attempts < max_attempts:
            attempts = attempts + 1
            logger.debug(f"attempt {attempts} of {max_attempts} to obtain a useful SSHClient "
                         f"{self.ssh_user}@{self.ssh_host} -p {self.ssh_port} ({self.ssh_pass})")
            try:
                client = SSHClient()
                client.set_missing_host_key_policy(AutoAddPolicy())
                client.connect(
                    username=self.ssh_user, password=self.ssh_pass, hostname=self.ssh_host, port=self.ssh_port,
                    allow_agent=False, look_for_keys=False)
                break
            except SSHException as sse:
                # why would this fail? I've had at least one failure in a case where it should not have.
                # SSHException('Error reading SSH protocol banner')
                if attempts < max_attempts and "Error reading SSH protocol banner" in str(sse):
                    logger.error(f"SSHClient used: {self.ssh_user}, {self.ssh_pass}, {self.ssh_host}, {self.ssh_port}")
                    continue
                raise

        return client

    def exec_command(self, command: Union[List[str], str], timeout: int = None,
                     host: str = None, user: str = None, password: str = None, port: int = 0, auto_dhcp=False) -> str:
        """Send commands to execute on the LANClient"""

        # did we get one command or a list of them?
        commands = command if isinstance(command, list) else [command]

        if self.linux_namespace != "":
            # switch into the namespace with the first command for the list of commands
            commands.insert(0, f"sudo ip netns exec {self.linux_namespace} bash")
            commands.append("exit")

        user = user or self.ssh_user
        password = password or self.ssh_pass
        port = port or self.ssh_port
        host = host or self.ssh_host

        client = SSHShellClient(host=host, username=user, password=password, port=port)
        client.change_timeout(changeto=timeout)
        multiple_result = client.do_complex_cmd(cmd=commands)
        result = "\r\n".join(multiple_result)
        if "curl: (7) Couldn't connect to server" in result and auto_dhcp:
            # single retry after updating dhcp (to help IPPT products if the WAN connection establishes mid-test).
            # this is deliberately a single retry.  Do not add more retries here without understanding root cause.
            logger.warning("AUTO RETRY! Will retry command after attempting to renew DHCP")
            self.refresh_dhcp()
            multiple_result = client.do_complex_cmd(cmd=commands)
            result = "\r\n".join(multiple_result)
        return result

    def copy_local_file_to_client(self, local_path: str, remote_path: str):
        ssh = self.get_ssh_client()

        logger.debug(f"copying {local_path} to {remote_path}")
        with SCPClient(ssh.get_transport()) as scp:
            scp.put(local_path, remote_path)

    def get_host_ip(self, interface: str = "eth0"):
        """obtains LAN IP for a specific interface on the client.
        Args:
            interface (str): name of the interface to obtain ipv4 address default is eth0 which is a rack design target.

        Returns:
            str: "192.168.0.1" (typical, but can vary by configuration)
        """
        command = f"ip -o -4 address show dev {interface} "\
            r"| grep -oE '([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})' | head -1"  # no f formatting here!
        output = self.exec_command(command=command)
        if "device" in output.lower():
            raise NCOSClientError(f"Could not obtain client ip for {interface}: "
                                  f"output {output}")
        logging.info(f"get_host_ip -> {output}")
        return output.strip()

    def ping(self, target: str, interface: str = "eth0", loss_threshold: int = 49, count: int = 3) -> bool:
        """pings target from client
        Args:
            target: ip / name to ping
            interface: client interface to use for ping (some clients can have more than one outbound interface)
            loss_threshold: must be <= threshold% lost pings to succeed
            count: number of pings to attempt before deciding success/failure
        Returns
            True if ping is successful >= threshold% of attempts
            False otherwise"""
        ping_cmd = f"ping -I {interface} -c {count} {target}"
        out = self.exec_command(ping_cmd)

        # 18.04 gives "name or service"  mint and older ubuntu gives "unknown host". This is ony informational.
        if "Name or service not known" in out or "Unreachable" in out or "unknown host" in out.lower():
            logger.warning(f"{ping_cmd} gave output: {out}")

        # what's the right percentage here?  We'll start with perfection and adjust when needs require it.
        percent_lost = 100
        match = re.search(" ([0-9]+)% packet loss", out)
        if match:
            percent_lost = int(match.group(1))
        logger.info(f"ping {target} lost {percent_lost}% packets")
        return percent_lost <= loss_threshold

    def refresh_dhcp(self, interface: str = "eth0") -> bool:
        """Does whatever might be necessary to request a new dhcp address on the given interface.  Typically should
        be used after changing the router configuration to have a new LAN ip address or after rebooting a router.

        Returns
            True on 'success' (for use by wait().for_call().to_equal())
            False on command failure"""

        # For those tempted to split this into separate lines, recall that sometimes you're resetting
        # the network link you're using to send commands to the client, and you can take the client
        # entirely offline if you mess up here.
        logger.info("Refreshing DHCP lease on the client")
        refresh_cmd = [f'bash -c "sudo dhclient -r -v {interface} ; '
                       f'sudo rm /var/lib/dhcp/dhclient.* ; '
                       f'sudo dhclient -v {interface}"']

        out = self.exec_command(command=refresh_cmd)
        if "bound to " in out or "RTNETLINK answers: File exists" in out:
            # we have a dhcp address
            return True
        logger.warning(f"{refresh_cmd} returned: {out}")
        return False


class NCOSWiFiClient(NCOSNetworkClient):
    """Base class for any object representation of a Network Client on the WiFi of an NCOSRouter"""
    def get_ssh_client(self) -> SSHClient:
        raise NotImplementedError()

    def exec_command(self, command: str, timeout: int = None, user: str = None, password: str = None
                     ) -> Tuple[str, str]:
        raise NotImplementedError("Must be implemented by concrete class")

    def copy_local_file_to_client(self, local_path: str, remote_path: str):
        raise NotImplementedError("Must be implemented by concrete class")

    def router_get_fw_info(self, router: NCOSRouter, protocol: str)->NCOSFirmwareInfo:
        raise NotImplementedError("NCOSNetworkClient doesn't know how to talk to routers?  Use NCOSLANClient or ???")

    def router_get_value(self, router: NCOSRouter, value_path: str, protocol: str = "https"):
        raise NotImplementedError("NCOSNetworkClient doesn't know how to talk to routers?  Use NCOSLANClient or ???")

    def router_set_value(self, router: NCOSRouter, value_path: str, value, protocol: str = "https"):
        raise NotImplementedError("NCOSNetworkClient doesn't know how to talk to routers?  Use NCOSLANClient or ???")
