import logging
from nctf.test_lib.base.objects import NCTFObject

logger = logging.getLogger("ncos.firmware_info")


class NCOSFirmwareInfo(NCTFObject):
    """Firmware version and utilities in a nice autocomplete-able form"""
    def __init__(self):
        self.major_version = None
        self.minor_version = None
        self.patch_version = None
        self.build_date = None
        self.build_version = None
        self.build_type = None
        self.upgrade_major_version = None
        self.upgrade_minor_version = None
        self.upgrade_patch_version = None
        self.custom_defaults = None
        self.manufacturing_upgrade = None

    def load_json_response(self, json_value):
        """rest API returns a reply json dictionary -- use it as a source of information
        can also just accept json of this object itself"""
        if "major_version" in json_value:
            data = json_value
        else:
            if "success" not in json_value or not json_value["success"]:
                logger.warning(f"tried to read fw_info from unsuccessful REST result. (ok if that was expected) {json_value}")
                return False
            data = json_value["data"]

        self.major_version = data["major_version"]
        self.minor_version = data["minor_version"]
        self.patch_version = data["patch_version"]
        self.build_date = data["build_date"]
        self.build_version = data["build_version"]
        self.build_type = data["build_type"]
        self.upgrade_major_version = data["upgrade_major_version"]
        self.upgrade_minor_version = data["upgrade_minor_version"]
        self.upgrade_patch_version = data["upgrade_patch_version"]
        try:
            self.custom_defaults = data["custom_defaults"]
            self.manufacturing_upgrade = data["manufacturing_upgrade"]
        except KeyError:
            # ignoring if these two are missing -- should we?
            pass
        return True

    def __str__(self):
        """how to format this to print for users to see"""
        result = (f"{self.major_version}.{self.minor_version}.{self.patch_version}, {self.build_type},"
                  f"{self.build_version}, {self.build_date}")
        return result
