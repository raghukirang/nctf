from nctf.test_lib.base.objects import NCTFObject


class NCOSNetworkProvider(NCTFObject):
    """Base class for any system delivering NCOSRouters and NCOSNetClients

    Example inheritors include the ARM (Automation Resource Manager) and VirtNetwork

    """

    # TODO: Standardize on the function signatures here before exposing them.

    # def get_network(self, *args, **kwargs):
    #     """Deliver a complex network of arbitrary complexity"""
    #     raise NotImplementedError()
    #
    def get_simple_network(self, *args, **kwargs):
        """Deliver a simple network with a single NCOSRouter with a single NCOSLanClient"""
        raise NotImplementedError()


class NCOSNetwork(dict):
    """Collection of NCOSRouters, NCOSNetClients, etc"""
    pass
