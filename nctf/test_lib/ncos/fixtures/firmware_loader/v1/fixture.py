from enum import Enum
import logging
import os
from pathlib import Path
from subprocess import call

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.libs.bin_ops import BinOperations
from nctf.test_lib.libs.device_capabilities import DeviceModel
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI

logger = logging.getLogger('firmware_loader.interface')


class BranchType(Enum):
    """Enum containing the different build branches.

    """
    ESCALATION = "escalation"
    FIELD = "field"
    HEAD = "head"
    RELEASE = "Release"
    NONE = ""


class FirmwareLoadError(Exception):
    """
    Exception raised when the sha1 of the firmware loaded does not match the expected sha1.
    """
    pass


class FirmwareLoaderFixture(FixtureBase):
    def __init__(self,
                 guido_branch: BranchType = BranchType.NONE,
                 local_full_path: str = None,
                 guido_path: str = None,
                 guido_date: str = None,
                 img_location: str = None,
                 load_method: str = None):
        """Class for loading firmware.  If neither firmware_path or branch_type are specified no firmware will be loaded.

        Args:
            guido_branch: Set this to load a revision of firmware based on the branch type.
                If local_path is specified this will be ignored.
                e.g. BranchType.FIELD will load a field branch.
            local_full_path: Set this attribute load firmware using a fully qualified path.
                e.g. /home/myhome/firmware/aer1600.bin
            guido_path: Set the mount path.  If the mount does not exist it will be created.
                e.g. /home/myhome/guido
            guido_date: NOT IMPLEMENTED.  THE LATEST REVISION WILL ALWAYS BE LOADED.
                Set this to load a firmware revision from a specific date.
                If no date is specified then the latest guido_branch will be loaded.
        """
        logger.warning("FirmwareLoaderFixture (v1) has been deprecated. Please use v2!")
        self.guido_branch = guido_branch
        self.local_full_path = local_full_path
        self.guido_path = guido_path
        self.guido_date = guido_date
        self.img_location = img_location
        self.load_method = load_method

    def mount_firmware(self) -> None:
        """Create firmware network mount.

        """
        dir_path = os.path.dirname(os.path.realpath(__file__))
        script_path = os.path.abspath(os.path.join(dir_path, "..", "get_firmware_files.sh"))
        call([script_path, self.guido_path])

    def get_latest_firmware(self, device: DeviceModel, branch_type: BranchType, file_type: str = "bin") -> str:
        """Get the latest firmware available.

        Args:
            device: The device of interest.
            branch_type: The build branch of interest.
            file_type: The file type of interest.  Ex: "bin" or "raw"

        Returns:
            The path to the latest firmware file including the firmware file name.

        """
        logger.debug("Firmware path is: {}".format(self.guido_path))
        root = next(os.walk(self.guido_path))[1]
        folders = sorted([folder for folder in root if branch_type.value in folder], reverse=True)
        for fw in folders:
            path = os.path.join(self.guido_path, fw)
            file = [file for file in os.listdir(path) if "{}_".format(device.guido) in file if ".{}".format(file_type) in file]
            if file:
                full_path = os.path.abspath(os.path.join(path, file[0]))
                logger.info("Firmware file found: {}".format(full_path))
                return full_path
        raise FileNotFoundError("Unable to find firmware file for {} on {}!".format(device.name, branch_type.name))

    def load_firmware(self, device_type: DeviceModel, host_name: str, user_name: str, password: str) -> None:
        """Load firmware to a device using the rest api.

        Args:
            device_type: The device type of interest.
            host_name: The host name of the device.
            user_name: The username for the device.
            password: The password for the device.

        """
        if self.guido_date:
            raise NotImplementedError("Loading firmware by date is not yet implemented.  Leave this field blank.")
        if (self.guido_branch != BranchType.NONE and not self.guido_path) or (self.guido_branch == BranchType.NONE
                                                                              and self.guido_path):
            raise ValueError("Attributes guido_branch and guido_path must both have a value or neither have a value.")
        if not (self.guido_branch.value or self.local_full_path):
            logger.info("No guido_branch or local_full_path specified.  Firmware will not be loaded.")
        else:
            load_firmware_flag = True
            if self.img_location == "guido":
                self.mount_firmware()
                full_path = self.get_latest_firmware(device_type, self.guido_branch)
                self.local_full_path = full_path
            elif self.img_location != "local":
                raise NotImplementedError("Deriving firmware file from {} is not implemented.".format(self.img_location))

            revision = self._get_expected_sha()
            if revision:
                build_version = self._get_loaded_firmware_revision(host_name, user_name, password)
                logger.info("Current router firmware revision sha1 is {}".format(build_version))
                load_firmware_flag = revision != build_version

            if load_firmware_flag:
                logger.info("Loading {}".format(self.local_full_path))
                if self.load_method == "rest":
                    read_only_config = self._get_read_only_config(host_name, user_name, password)
                    self._load_firmware_rest(host_name, user_name, password)
                    if read_only_config:
                        self._set_read_only_config(host_name, user_name, password)
                else:
                    raise NotImplementedError("Loading firmware with {} is not implemented.".format(self.load_method))
                if revision:
                    build_version = self._get_loaded_firmware_revision(host_name, user_name, password)
                    if revision != build_version:
                        raise FirmwareLoadError(
                            "Current router revision sha1 {} does not match expected {} after firmware was loaded".format(
                                build_version, revision))

    def _get_loaded_firmware_revision(self, host_name: str, user_name: str, password: str) -> str:
        """Get the firmware sha1 of the device.

        Args:
            host_name: The host name of the device.
            user_name: The user name of the device.
            password: The password of the device.

        Returns:
            The sha1 loaded on the device.

        """
        api = RouterRESTAPI(host_name)
        api.authenticate(user_name, password)
        build_version = api.status_fw_info.get("build_version")["data"]
        return build_version

    def _load_firmware_rest(self, host_name: str, user_name: str, password: str):
        """Load firmware to a device based on the derived path using the rest api.

        Args:
            host_name: The host name of the device.
            user_name: The user name of the device.
            password: The password of the device.

        """
        rest_load = BinOperations(host_name=host_name, user_name=user_name, password=password)
        rest_load.load_firmware_bin_file(file=self.local_full_path)

    def _get_read_only_config(self, host_name: str, user_name: str, password: str) -> bool:
        """Get readonlyconfig..

        Args:
            host_name: The host name of the device.
            user_name: The user name of the device.
            password: The password of the device.

        Returns:
            A boolean value of the readonlyconfig state.

        """

        read_only_config = RouterRESTAPI(api_root=host_name)
        read_only_config.authenticate(user_name=user_name, password=password)
        value = read_only_config.control_system.get(name="readonlyconfig")["data"]
        logger.debug("Current readonlyconfig value is set to {}.".format(value))
        return value

    def _set_read_only_config(self, host_name: str, user_name: str, password: str):
        """Set readonlyconfig to True using the rest api.

        Args:
            host_name: The host name of the device.
            user_name: The user name of the device.
            password: The password of the device.

        """
        logger.debug("Setting readonlyconfig to True.")
        read_only_config = RouterRESTAPI(api_root=host_name)
        read_only_config.authenticate(user_name=user_name, password=password)
        read_only_config.control_system.update(update={"readonlyconfig": True})

    def _get_expected_sha(self) -> str:
        """Get the firmware revision sha1 from "git_sha1.txt" in the firmware repository.

        Returns:
            The string read from "git_sha1.txt" in the firmware repository.

        """
        revision = ""
        folder = os.path.dirname(os.path.abspath(self.local_full_path))
        revision_file = os.path.join(folder, "git_sha.txt")
        if os.path.isfile(revision_file):
            logger.info("Verifying requested firmware is not already loaded.")
            with open(revision_file, 'r') as f:
                revision = f.read().rstrip()
        return revision

    def get_failure_report(self):
        msg = "FirmwareLoaderInterface Failure Report\n"
        msg += "Guido Branch is: {}\n".format(self.guido_branch)
        msg += "Local Path is: {}\n".format(self.local_full_path)
        msg += "Guido Path is: {}\n".format(self.guido_path)
        msg += "Guido Date is: {}\n".format(self.guido_date)
        return msg

    def teardown(self):
        logger.info("No teardown to perform")
