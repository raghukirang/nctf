#!/usr/bin/env bash

#!/usr/bin/env bash

DIR=$1
if find $1 -mindepth 1 -print -quit | grep -q .;then
    echo "Guido mount exists at $DIR"
else
    echo "Create Guido mount at $DIR"
    command -v mount.nfs >/dev/null 2>&1 || { echo >&2 "Requires mount.nfs but it's not installed. Attempting to install with apt-get."
        sudo apt-get -y install nfs-common; }
    sudo mkdir -p ${DIR}
    sudo mount -t nfs -o proto=tcp,port=2049 build:/nightlies ${DIR}
fi
