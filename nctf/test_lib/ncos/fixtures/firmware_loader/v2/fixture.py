from enum import Enum
import json
import logging
import os
import re
import shutil
import time
from typing import Dict
from typing import List

from artifactory import ArtifactoryPath
import requests

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.libs.bin_ops import BinOperations
from nctf.test_lib.ncos.base import NCOSLANClient
from nctf.test_lib.ncos.base import NCOSRouter
from nctf.test_lib.ncos.base import NCOSRouterLog
from nctf.test_lib.ncos.base import NCOSFirmwareInfo
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI
from nctf.test_lib.utils.waiter import wait
from nctf.test_lib.utils.waiter import WaitTimeoutError

logger = logging.getLogger('fwloader2')

CP_ARTIFACTORY = 'https://cradlepoint.jfrog.io/cradlepoint'
# Hercules (cr4250 with HYPMGR) requires web site that can serve as intermediary to artifactory and provide
# all the right API keys and headers etc.  (router must pull the .vmp file)  The proxy below is a server for
# that purpose.
CP_HERCULES_PROXY = "https://herc-proxy.private.aws.cradlepointecm.com/firmware/cradlepoint"


class FwBuildType(Enum):
    FIELD = "field"
    HEAD = "head"
    RELEASE = "release"


class FwLocation(Enum):
    LOCAL = "local"
    ARTIFACTORY = "artifactory"


class FwBinData:
    """Container for important Firmware .bin file information"""

    def __init__(self,
                 local_path: str = None,
                 artifactory_path: str = None,
                 product: str = None,
                 build_type: str = None,
                 git_sha: str = None,
                 release_ver: str = None):
        self.local_path = local_path
        self.artifactory_path = artifactory_path
        self.product = product
        self.build_type = build_type
        self.git_sha = git_sha
        self.release_ver = release_ver

    @staticmethod
    def _safe_load(key: str, source: dict, default: str)->str:
        try:
            result = source[key]
        except KeyError:
            logger.info(f"Using value {default} for FwBinData key {key} due to missing key in input json (probably ok)")
            result = default

        return result

    def load_dict(self, json_dict: dict):
        self.local_path = self._safe_load("local_path", json_dict, self.local_path)
        self.artifactory_path = self._safe_load("artifactory_path", json_dict, self.artifactory_path)
        self.product = self._safe_load("product", json_dict, self.product)
        self.build_type = self._safe_load("build_type", json_dict, self.build_type)
        self.git_sha = self._safe_load("git_sha", json_dict, self.git_sha)
        self.release_ver = self._safe_load("release_ver", json_dict, self.release_ver)

    def __del__(self):
        pass  # TODO: Implement a toggle-able cleanup/delete on the file.


class FwGetterError(Exception):
    pass


class FwGetter:
    def get_fw(self, repo_path: str, product: str, extension: str, **kwargs) -> FwBinData:
        raise NotImplementedError()

    def find_fw(self, product: str, extension: str) -> FwBinData:
        raise NotImplementedError()


class LocalFwGetter(FwGetter):
    """
    When using the LocalFwGetter the expectation is there is a local directory full of
    FW Images named for the product/device model they were built for. e.g. aer2100

    The idea being that if we can have the same interface to local images as
    Artifactory tests can point at one or the other with zero code change.

    """

    def __init__(self, fw_image_dir: str = None):
        super().__init__()
        self.fw_image_dir = fw_image_dir

    def find_fw(self, repo_path: str = None, product: str = None, extension: str = None) -> FwBinData:
        # find/get is all the same for LocalFwGetter
        return self.get_fw(repo_path, product, extension)

    def get_fw(self, repo_path: str = None, product: str = None, extension: str = None, **kwargs) -> FwBinData:
        result = FwBinData()
        result.local_path = self._get_fw_from_local_folder(file_path=repo_path, product=product, extension=extension)

        # we can get the build version or build type reliably here from a fw_bin_data.json file!
        bin_data_file = os.path.join(os.path.dirname(result.local_path), "fw_bin_data.json")
        if os.path.isfile(bin_data_file):
            with open(bin_data_file, "r") as f:
                data_json = json.load(f)
            result.load_dict(data_json)
            # if we have the fw_bin_data that is all we need.  Early return.
            return result

        # if the directory of files contains a git_sha.txt file load it and use it as the target SHA (guido)
        sha_file = os.path.join(os.path.dirname(result.local_path), "git_sha.txt")
        if os.path.isfile(sha_file):
            with open(sha_file, "r") as f:
                result.git_sha = f.readline().strip()

        return result

    def _get_fw_from_local_folder(self, product: str, extension: str = "bin", file_path: str = None):
        fw_img_dir = file_path or self.fw_image_dir
        if fw_img_dir is None:
            raise FwGetterError("Unable to get FW from local folder. No default or parameter file_path provided!")

        # compatibility with AF and local -- the code below doesn't handle wildcards.  Drop the *
        extension = extension.replace("*", "").replace(".", "")

        # Query the fw_img_dir for a matching fw_img.
        files = os.listdir(fw_img_dir)
        matching_files = [f for f in files if f.endswith(extension) and product.lower() in f.lower()]

        if len(matching_files) <= 0:
            raise FwGetterError(f"No fw images found with product='{product}' extension='{extension}' "
                                f"in '{fw_img_dir}'")

        if len(matching_files) > 1:
            raise FwGetterError(f"Multiple fw images found with product='{product}' extension='{extension}' "
                                f"in '{fw_img_dir}'")

        # If exactly 1 found we're good.
        fw_img = os.path.join(fw_img_dir, matching_files[0])
        return self._get_fw_from_local_path(fw_img)

    @staticmethod
    def _get_fw_from_local_path(path):
        path = os.path.abspath(path)
        if not os.path.isfile(path):
            raise FwGetterError(f"No fw image found at '{path}'")

        return os.path.abspath(path)


class ArtifactoryFwGetter(FwGetter):
    """
    When using the ArtifactoryFwGetter the expectation is an Artifactory repository is provided with FW builds that
    contain queryable properties such as GIT_SHA, GIT_BRANCH and BUILD_TYPE.

    This allows us find the proper builds without relying on stitching together a path string.

    """

    def __init__(self,
                 api_key: str,
                 repository: str,
                 git_branch: str = None,
                 build_type: str = None,
                 git_sha: str = None,
                 build_date: str = None,
                 local_dir: str = None,
                 release_ver: str = None):

        super().__init__()

        # Artifactory auth
        self.api_key = api_key
        self.repository = repository

        # Artifactory query optional defaults
        self.git_branch = git_branch
        self.git_sha = git_sha
        self.build_type = build_type
        self.build_date = build_date
        self.release_ver = release_ver

        # Local dir to save FW to.
        if local_dir:
            self.local_dir = local_dir
        else:
            self.local_dir = os.path.abspath(os.path.dirname(__file__))

    @staticmethod
    def _artifactory_product_name(unfixed_product: str) -> str:
        """The router reports AER2200-FIPS. ARM4 knows it as AER2200FIPS (python variable-ish)
        Artifactory thinks the product is AER2200.FIPS.  This fixes for artifactory the product
        name to use.  """
        if ".fips" in unfixed_product.lower():
            return unfixed_product  # already fixed
        if "-fips" in unfixed_product.lower():
            return unfixed_product.lower().replace("-fips", ".fips")
        if "0fips" in unfixed_product.lower():
            return unfixed_product.lower().replace("0fips", "0.fips")
        return unfixed_product

    @staticmethod
    def _choose_best_artifact(artifacts: list) -> dict:
        if len(artifacts) == 0:
            raise FwGetterError(f"No FW artifact could be found with matching params.")

        artifact = artifacts[-1]  # The last artifact should be the latest.
        if len(artifacts) > 1:
            logger.warning(f"Multiple FW artifacts found matching params. Defaulting to latest: {artifact}")
        return artifact

    @staticmethod
    def _add_artifact_to_bin_data(bin_data: FwBinData, artifact: dict):
        # When we have an AQL response we can add these values.
        bin_data.release_ver = next(
            item['value'] for item in artifact['properties'] if 'RELEASE_VERSION' in item.values())
        bin_data.product = next(item['value'] for item in artifact['properties'] if 'PRODUCT' in item.values())
        bin_data.git_sha = next(item['value'] for item in artifact['properties'] if 'GIT_SHA' in item.values())
        bin_data.build_type = next(item['value'] for item in artifact['properties']
                                   if 'BUILD_TYPE' in item.values())

    def find_fw(self, product: str = None, extension: str = "*.vmp") -> FwBinData:
        """Query artifactory for a FW image, but don't download it.  Just get the data for us to examine.
        """
        if not product:
            raise ValueError('product or extension or af_repo path must be provided')

        product = self._artifactory_product_name(product)
        matching_artifacts = self._query_artifactory(product, extension, self.api_key)
        artifact = self._choose_best_artifact(matching_artifacts)

        af_path = f"{CP_ARTIFACTORY}/{self.repository}/{artifact['path']}/{artifact['name']}"

        bin_data = FwBinData(local_path="nothing downloaded, query only", artifactory_path=af_path)
        self._add_artifact_to_bin_data(bin_data, artifact)
        return bin_data

    def get_fw(self, repo_path: str = None, product: str = None, extension: str = None, **kwargs) -> FwBinData:
        """Download a FW image from Artifactory either by querying with product and/or extension or by
        using the repo path"""
        if repo_path:
            bin_data = self._download_from_artifactory_w_repo_path(repo_path, self.api_key)
            return bin_data

        elif product or extension:
            product = self._artifactory_product_name(product)
            artifacts = self._query_artifactory(product, extension, self.api_key)
            artifact = self._choose_best_artifact(artifacts)
            bin_data = self._download_from_artifactory_w_aql_response(artifact, self.api_key)
            return bin_data

        else:
            raise ValueError('product or extension or af_repo path must be provided')

    def _download_from_artifactory_w_aql_response(self, artifact_dict: dict, api_key: str = None) -> FwBinData:
        """Use the path provided by the AQL response to download the FW file"""

        if api_key is None:
            api_key = self.api_key

        name = artifact_dict['name']
        path = artifact_dict['path']
        af_path = f"{self.repository}/{path}/{name}"

        bin_data = self._download_from_artifactory_w_repo_path(af_repo_path=af_path, api_key=api_key)
        self._add_artifact_to_bin_data(bin_data, artifact_dict)
        return bin_data

    def _download_from_artifactory_w_repo_path(self, af_repo_path: str, api_key: str = None) -> FwBinData:
        """Use the relative path of Cradlepoint's artifactory to download the FW file"""

        if api_key is None:
            api_key = self.api_key

        s = requests.Session()
        s.headers['X-JFrog-Art-Api'] = api_key

        af_path = f"{CP_ARTIFACTORY}/{af_repo_path}"

        r = s.get(af_path, stream=True)
        local_name = af_repo_path.replace('/', '_')
        local_path = os.path.join(self.local_dir, local_name)

        logger.debug(f"Downloading {af_path} to {local_path}")
        with open(local_path, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

        bin_data = FwBinData(local_path=local_path, artifactory_path=af_path)
        return bin_data

    def _query_artifactory(self,
                           product: str,
                           name_exp: str,
                           api_key: str = None,
                           repository: str = None,
                           git_branch: str = None,
                           git_sha: str = None,
                           build_type: str = None,
                           build_date: str = None,
                           release_ver: str = None) -> List[Dict]:
        """Query Artifactory for a list of firmware artifacts matching the given criteria via their properties
        and file name.

        Note: The last artifact in the list should be the most recently created.

        """
        q_product = product.lower()
        q_name_exp = name_exp
        q_repository = repository if repository else self.repository
        q_git_branch = git_branch if git_branch else self.git_branch
        q_git_sha = git_sha if git_sha else self.git_sha
        q_build_type = build_type if build_type else self.build_type
        q_build_date = build_date if build_date else self.build_date
        q_release_ver = release_ver if release_ver else self.release_ver

        query = dict()
        query["repo"] = q_repository
        query["name"] = {"$match": q_name_exp}
        query["@PRODUCT"] = {"$eq": q_product}
        if q_git_branch:
            query['@GIT_BRANCH'] = {"$match": q_git_branch}
        if q_git_sha:
            query['@GIT_SHA'] = {"$eq": q_git_sha}
        if q_build_type:
            query['@BUILD_TYPE'] = {"$eq": q_build_type}
        if q_build_date:
            query['@BUILD_DATE'] = {"$eq": q_build_date}
        if q_release_ver:
            query['@RELEASE_VERSION'] = {"$eq": q_release_ver}

        s = requests.Session()
        s.headers['X-JFrog-Art-Api'] = api_key
        af = ArtifactoryPath(CP_ARTIFACTORY, session=s)
        af._drv = CP_ARTIFACTORY  # For some reason we have to do this.

        logger.info(f"Querying Artifactory for firmware using query '{query}'")
        artifacts = af.aql("items.find", query, '.include("property.*")')
        # TODO: Set this to debug when we figure out why we sometimes get junk.
        logger.info(f"Artifactory query result = {artifacts}")
        return artifacts


class FwLoaderFixtureV2Error(Exception):
    pass


class FwLoaderFixtureV2(FixtureBase):
    # class level constant so we can shorten for unit testing
    _stabilize_delay = 30

    def __init__(self, fw_getter: FwGetter):
        """Test fixture for loading firmware onto Cradlepoint Routers."""
        self.fw_getter = fw_getter

    def load(self):
        pass

    @staticmethod
    def _is_fw_loaded(router: NCOSRouter, target_version: FwBinData,
                      client: NCOSLANClient = None, precheck: bool = True,
                      protocol: str = "https",
                      host_url: str=None, username: str=None, password: str=None) -> bool:
        """validates that firmware on the target matches a specific target version.  Note that the answer may be
        dependent on the inputs available and may be ambiguous.  For that reason, precheck is used to determine if
        comparison is likely to say "not loaded yet, please load it" or "I have no idea, I think it may have loaded ok"
        during the upgrade process.  """

        # if we don't have any of these to compare below, assume an answer based on precheck
        sha_ok = not precheck
        type_ok = not precheck

        # if we can't talk to the router we can't proceed from here.  (hard fail)
        current_fw_info = FwLoaderFixtureV2._get_loaded_firmware_data(router=router, client=client, protocol=protocol,
                                                                      host_url=host_url,
                                                                      username=username,
                                                                      password=password)

        if target_version.build_type:
            regex = re.compile('[^a-zA-Z]')
            current_bt = regex.sub('', current_fw_info.build_type).lower()
            type_ok = ((target_version.build_type == current_bt)
                       or (target_version.build_type == "head" and current_bt == "dev"))
            logger.info(f"expected build_type: {target_version.build_type}, actual: {current_bt}, result: {type_ok}")
        else:
            logger.warning(f"No target build_type available to validate correct firmware is loaded. Assuming {type_ok}")

        if target_version.git_sha:
            current_sha = current_fw_info.build_version.zfill(7)
            sha_ok = target_version.git_sha.startswith(current_sha)
            logger.info(f"expected fw SHA: {target_version.git_sha} "
                        f"actual fw SHA: {current_sha}, result: {sha_ok}")
            if not sha_ok:
                logger.warning(f"target git_sha: {target_version.git_sha}, actual: {current_sha}, result: {sha_ok}")
        else:
            logger.warning(f"No target git_sha available to validate correct firmware is loaded. Assuming {sha_ok}")

        return sha_ok and type_ok

    @staticmethod
    def _get_loaded_firmware_data(router: NCOSRouter, client: NCOSLANClient = None, protocol: str = "https",
                                  host_url: str = None, username: str = None, password: str = None) -> NCOSFirmwareInfo:
        """Get the firmware sha1 of the device.

        Args:
            router: The router to query
            client: optional. When a client must be used to talk to the router, the client to use

        Returns:
            NCOSFirmwareInfo of version properties of the device.

        """
        if client:
            fw_info = client.router_get_fw_info(router=router, protocol=protocol)
        elif router:
            fw_info = router.get_fw_info(protocol=protocol)
        else:
            api = RouterRESTAPI(host_url)  # protocol is embedded in the host_url
            api.authenticate(username, password)
            build_version = api.status_fw_info.get()["data"]
            fw_info = NCOSFirmwareInfo()
            fw_info.load_json_response(build_version)

        return fw_info

    @staticmethod
    def _get_read_only_config(router: NCOSRouter, client: NCOSLANClient = None, protocol: str = "https",
                              host_url: str = None, username: str=None, password: str=None) -> bool:
        """Get readonlyconfig..

        Args:
            router: The router to query
            client: optional. When a client must be used to talk to the router, the client to use
            protocol: optional. Use http or https

        Returns:
            A boolean value of the readonlyconfig state.

        """
        get_path = "/control/system/readonlyconfig"
        if client:
            value = client.router_get_value(router, get_path, protocol=protocol)
        elif router:
            value = router.get_value(get_path, protocol=protocol)
        else:
            api = RouterRESTAPI(host_url)
            api.authenticate(username, password)
            value = api.control_system.get("readonlyconfig")["data"]
        logger.debug(f"Current readonlyconfig value is set to {value}.")
        return value

    @staticmethod
    def _set_read_only_config(router: NCOSRouter, client: NCOSLANClient = None, protocol: str = "https",
                              host_url: str = None, username: str=None, password: str=None):
        """Set readonlyconfig to True using the rest api.

        Args:
            router: the router to use
            client: optional if the router requires a client to do the set from LAN side which client?

        """
        logger.debug("Setting readonlyconfig to True.")
        set_path = "/control/system/readonlyconfig"
        if client:
            client.router_set_value(router, set_path, True, protocol=protocol)
        elif router:
            router.set_value(set_path, True, protocol=protocol)
        else:
            api = RouterRESTAPI(host_url)  # protocol is embedded in the host_url
            api.authenticate(username, password)
            api.control_system.update(update={"readonlyconfig": True})

    @staticmethod
    def _indirect_get_boot_id(client: NCOSLANClient, router: NCOSRouter, timeout=30) -> str:
        """try to get the boot id, but don't try too hard -- let the caller decide how persistent to be."""
        try:
            result = client.router_get_value(router=router, value_path="/status/system/bootid", timeout=timeout)
        except WaitTimeoutError as wte:
            logger.warning("NO BOOTID RETURNED")
            return None
        return result

    def _load_via_hypmgr_proxy(self, router: NCOSRouter, getter: FwGetter, protocol: str):
        if isinstance(getter, LocalFwGetter):
            raise NotImplementedError("Hercules requires a proxy web server to host the .vmp file. "
                                      "That is not presently implemented for the use case of LocalFwGetter.")

        # Determine if the config is read only so we can turn it back on after the load.
        read_only_config = self._get_read_only_config(router=router, protocol=protocol)

        # don't download firmware since we can't use it locally anyway here and the file is huge.
        fw_bin = getter.find_fw(router.product, extension="*.vmp")

        # Determine if this firmware is already loaded and if so then skip
        fw_loaded = self._is_fw_loaded(router=router, target_version=fw_bin, precheck=True, protocol=protocol)
        if fw_loaded:
            logger.info("Firmware revision is already loaded.  No operation is required.")
        else:
            logger.info(f"Loading firmware via hypmgr pull download to {fw_bin.product} "
                        f"at {router.get_base_router_url(protocol=protocol)}")

            bootid_before = router.bootid
            full_proxy_path = fw_bin.artifactory_path.replace(CP_ARTIFACTORY, CP_HERCULES_PROXY)
            router.set_value(value_path="/control/hypmgr/upgrade", value=full_proxy_path)

            if True:
                # Single retry in the event we get a 504 on the hercules fw upgrade proxy.  Workaround for
                # NTI-763 to reduce frequency of failures -- this cannot and will not eliminate them.
                time.sleep(30)
                logs = router.get_logs()
                timeouts = NCOSRouterLog.filter_log(search="HTTP Error 504: GATEWAY_TIMEOUT", logs=logs)
                if len(timeouts) > 0:
                    # FUTURE: repeated retries will either need to clear the router logs or
                    logger.warning("Single RETRY due to 504 error:")
                    router.set_value(value_path="/control/hypmgr/upgrade", value=full_proxy_path)

            router.wait_for_reboot(bootid_before=bootid_before, timeout=600)

            self._stabilize_after_upgrade()

            if read_only_config:
                self._set_read_only_config(router=router, protocol=protocol)

        # Sanity check -- Did the firmware actually get loaded?
        if not self._is_fw_loaded(router=router, target_version=fw_bin, precheck=False, protocol=protocol):
            raise FwLoaderFixtureV2Error("Incorrect firmware version detected after firmware loaded.")

        logger.info("Finished loading firmware!")

    def load_fw(self, router: NCOSRouter, protocol: str = 'https',
                client: NCOSLANClient = None, getter: FwGetter = None):
        """Loads fw onto a router using the specified FwGetter.  Can indirect through a client if necessary.
        TODO: for protocol other than https, there is a API hole that there is not (yet) a standard way
        for the router to say 'my remote_admin_port_http value is 12345'
        Args:
            router: the router to upgrade
            client: the client to use to apply the upgrade (optional on ap22, mandatory on LAN-only products)
            getter: if you need a special firmware getter, pass it here, otherwise will use the run default
            protocol: 'http' or 'https' """

        # test may need to use multiple getters rather than only from external configuration.  Let them override here.
        getter = getter or self.fw_getter

        if getter is None:
            raise Exception(f"Unable to load FW to {router}. No fw_getter was provided!")

        # TODO: Check if this router is already on the target version of firmware. (need fw_getter to determine the
        # version we're actually going to load so we can compare it to the device)

        method = router.get_router_capabilities().upgrade_method
        if method == "HYPMGR":
            self._load_via_hypmgr_proxy(router=router, getter=getter, protocol=protocol)
            return

        elif method != "NORMAL":
            raise NotImplementedError(f"I don't know how to upgrade with method {method} ")

        if not client and (router.get_router_capabilities().lan_ports == 1 or
                           router.get_router_capabilities().default_lan_only):
            # there are many cases where a client might be required but not provided.  This is
            # only one of many cases that we can detect.  For a CBA850, the default state is both ports as LAN
            # but one can be reconfigured to be WAN, so the warning might be false, but it is test
            # configuration dependent whether this is a failure or not.
            logger.warning("load_fw() invoked with no client on product that probably needs one?")

        if client:
            # LAN-only products require us to use a client.  Some network topologies might also require a client.
            # if one is provided, let's use it for the firmware upgrade.
            self.load_indirect(client=client,  # lan client to use for indirection
                               router=router,
                               product=router.product,
                               getter=getter)
        else:
            self.load_over_rest(router=router, protocol=protocol, getter=getter)

    def load_indirect(self, client: NCOSLANClient, router: NCOSRouter, product: str = None,
                      repo_path: str = None, getter: FwGetter = None):

        lan_ip = router.default_lan_ip
        username = router.admin_user
        password = router.admin_password
        boot_id_before = self._indirect_get_boot_id(client=client, router=router)
        logger.info(f"bootid before: {boot_id_before}")
        assert boot_id_before  # we should never fail, but if we do, let's raise that before moving forward.

        bin_data = self._get_firmware_bin_file(repo_path=repo_path, product=product, getter=getter)

        # save some time if this is already loaded
        if FwLoaderFixtureV2._is_fw_loaded(client=client, router=router, target_version=bin_data, precheck=True):
            logger.info(f"Not upgrading because target firmware is already loaded.")
            return

        local_fw_bin = bin_data.local_path
        remote_fw_bin = os.path.basename(local_fw_bin)
        logger.info(f"sending firmware bin file to client: {remote_fw_bin}")
        client.copy_local_file_to_client(local_path=local_fw_bin, remote_path=remote_fw_bin)

        curl_command = f"curl --basic -u {username}:{password} -F fw_file=@{remote_fw_bin} " \
                       f"http://{lan_ip}/fw_upgrade?factory_reset=false --connect-timeout 5 --retry 3 --retry-delay 20"
        output = client.exec_command(command=curl_command, timeout=60)
        logger.info(f"upgrade curl output: {output}")
        if 'success' not in output:
            raise FwLoaderFixtureV2Error(f"curl error sending upgrade to router {output}")
        if '"success": false' in output:
            raise FwLoaderFixtureV2Error(f"load_indirect failed! stdout: {output}")

        # sometimes we need to keep the bin file for debugging upgrade issues -- How to know we should keep it?
        # For now the files will accumulate on the test host which should be ephemeral workers or a developer's
        # pc where they can respond to out of disk space.  The clients are not currently ephemeral so we need
        # to delete the file there.
        cleanup_command = f"rm -f {remote_fw_bin}"
        client.exec_command(command=cleanup_command)

        logger.info(f"waiting for router to reboot after upgrade")
        self._indirect_wait_for_reboot_complete(client=client, router=router, initial_id=boot_id_before)

        self._stabilize_after_upgrade()

        # the lack of retries and error handling here is deliberate.  In theory, _stabilize_after_upgrade() if
        # working correctly, should leave the router in a ready-to-use state.
        boot_id_after = self._indirect_get_boot_id(client=client, router=router)
        assert boot_id_after  # _indirect_get_boot_id can return None, but shouldn't. Check that here.

        if boot_id_after == boot_id_before:
            raise FwLoaderFixtureV2Error(f"load_indirect: firmware upgrade did NOT reboot router! {boot_id_before}")
        logger.info(f"bootid after: {boot_id_after}")

        # Sanity check -- Did the firmware actually get loaded?
        if not FwLoaderFixtureV2._is_fw_loaded(client=client, router=router, target_version=bin_data, precheck=False):
            raise FwLoaderFixtureV2Error("Incorrect firmware version detected after firmware loaded.")

    @staticmethod
    def _indirect_wait_for_license_service(client: NCOSLANClient, router: NCOSRouter) -> bool:
        state = client.router_get_value(router=router, value_path="/status/system/services/license/state")
        return "started" in state

    @staticmethod
    def _indirect_wait_for_reboot_complete(client: NCOSLANClient, router: NCOSRouter, initial_id: str = None,
                                           timeout=900):
        """ uses a client as the actor to determine if a router has rebooted or not. To avoid race conditions
        between the reboot action and capturing an initial bootid value, the initial bootid should be captured by
        the caller before initiating the reboot of the router.
        s
        Because the client is assumed to be on the LAN of the router, this method may not be applicable to WAN-only
        products such as AP22.

        Raises exception as necessary if timeout isn't met.

        Args:
            client: client actor
            router: router to monitor (assumed to be a LAN-connected router)
            initial_id: bootid before the router reboot was started.

        """
        initial_id = initial_id or FwLoaderFixtureV2._indirect_get_boot_id(client=client, router=router)
        assert initial_id, "Caller failed to provide an initial bootid value to check for.  This can lead to race " \
                           "conditions between the router reboot and checking for boot id.  " \
                           "Please provide an initial_id."

        current_id = initial_id
        probably_rebooted = False  # a nice diagnostic -- bad firmware bin files won't reboot at all when good should
        fail_time = time.monotonic() + timeout
        while time.monotonic() <= fail_time:
            try:
                client.refresh_dhcp()
                current_id = FwLoaderFixtureV2._indirect_get_boot_id(client=client, router=router, timeout=10) or \
                             initial_id
                if current_id and initial_id and current_id != initial_id:
                    # we probably got rebooted -- that's what we need!
                    break
            except FwLoaderFixtureV2Error as err:
                probably_rebooted = True
                logger.info(f"target may have rebooted! waiting for response.  {str(err)}")
            time.sleep(5)  # reduce log spam

        # triage hints -- did we reboot?  if not, what was in the router log?
        if current_id == initial_id:
            if probably_rebooted:
                logger.warning("router seemed to not respond at least once, but didn't give new bootid")
            logger.warning(f"failed to get fresh boot_id before timeout {timeout} seconds")

        wait(300, 5).for_call(lambda: FwLoaderFixtureV2._indirect_wait_for_license_service(
            client=client, router=router)).to_be(True)

        # there may be a case where we don't really want the router logs captured here and the enclosing test
        # context should capture them.  For now, I'm putting this here because I don't have a good
        # enclosing test context to use for grabbing router logs.
        try:
            log = client.router_get_value(router, "/status/log")
            if log:
                logger.info("Router contents:")
                for line in log:
                    output = "\t ".join([str(x) for x in line]) if isinstance(line, list) else line
                    logger.info(output)
        except Exception as ignored:
            logger.warning(f"Unable to get log from router: {str(ignored)}")

    def load_over_rest(self, router: NCOSRouter=None, protocol: str="https",
                       af_repo_path: str = None, getter: FwGetter = None,
                       hostname: str = None, username: str = None, password: str = None, product: str = None):
        if not router and not hostname:
            raise FwLoaderFixtureV2Error("You must provide either a router or a host url to use for the upgrade")

        host_url = hostname or router.get_base_router_url(protocol=protocol)
        username = username or router.admin_user
        password = password or router.admin_password
        product = product or (router.product if router else None)
        getter = getter or self.fw_getter

        # Determine if the config is read only so we can turn it back on after the load.
        read_only_config = self._get_read_only_config(router=router, protocol=protocol,
                                                      host_url=host_url, username=username, password=password)

        # TODO: Optimize these by preventing the actual download from occurring.
        fw_bin = self._get_firmware_bin_file(af_repo_path, product, getter=getter)

        # Determine if this firmware is already loaded and if so then skip
        fw_loaded = self._is_fw_loaded(router=router, target_version=fw_bin, precheck=True, protocol=protocol,
                                       host_url=host_url, username=username, password=password)
        if fw_loaded:
            logger.info("Firmware revision is already loaded.  No operation is required.")
        else:
            logger.info(f"Loading firmware via REST to {fw_bin.product} at {username}@{host_url}")
            rest_load = BinOperations(host_name=host_url, user_name=username, password=password)
            rest_load.load_firmware_bin_file(file=fw_bin.local_path)

            self._stabilize_after_upgrade()

            if read_only_config:
                self._set_read_only_config(router=router, protocol=protocol,
                                           host_url=host_url, username=username, password=password)

        # Sanity check -- Did the firmware actually get loaded?
        if not self._is_fw_loaded(router=router, target_version=fw_bin, precheck=False, protocol=protocol,
                                  host_url=host_url, username=username, password=password):
            raise FwLoaderFixtureV2Error("Incorrect firmware version detected after firmware loaded.")

        logger.info("Finished loading firmware!")

    @staticmethod
    def _stabilize_after_upgrade():
        # After the router has rebooted, we've seen some random communication issues within a second or two
        # of when the router is rebooted. We can talk to it to get the fresh bootid and maybe the sha,
        # then the router refuses connections later.
        # This sleep below would be better suited by a hard synchronization, but we don't have such a thing
        # for all the services in the router.  The problem is very rare -- I can't reproduce it.
        # This is partly a characterization -- all we know is something failed after this point when it
        # shouldn't have.  By adding this sleep, we slow down test cases that load firmware, but we
        # will have hopefully have better insight into whether the router was "ready" or not.
        delay = FwLoaderFixtureV2._stabilize_delay
        logger.info(f"Sleeping {delay} seconds to let router stabilize after upgrade.  "
                    "If router won't talk after this, it should be a bug.")
        time.sleep(delay)

    @staticmethod
    def _get_firmware_bin_file(repo_path: str, product, getter: FwGetter) -> FwBinData:
        """uses the getter to obtain a firmware file.  (does not apply to .vmp files or .img files)
        :param repo_path local subdirectory to obtain .bin file OR URL to artifactory repo. Can also come from
        config yaml.
        :param product which product to look for.  Can also come from config yaml.
        :param getter  What method should we use to obtain the firmware file?
        :return FwBinData describing the file in the local computer's filesystem.  """
        if not repo_path and not product:
            raise ValueError('product and local path or artifactory repo_path (using ArtifactoryFwGetter)'
                             ' must be provided')
        fw_bin = getter.get_fw(repo_path=repo_path, product=product, extension="*.bin")
        return fw_bin

    def teardown(self):
        pass

    def get_failure_report(self):
        pass


if __name__ == "__main__":
    # Example code.
    import logging
    import sys

    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    afg = ArtifactoryFwGetter(
        api_key="YourApiKey",
        repository="platform-firmware-private",
        git_branch="release",
        build_date="2018_06_21",
        build_type="field")

    loader = FwLoaderFixtureV2(fw_getter=afg)

    demo_router = NCOSRouter()
    demo_router.default_lan_ip = "192.168.0.1"
    demo_router.admin_password = "44276b68"
    loader.load_over_rest(router=demo_router,
                          protocol="https",
        af_repo_path='platform-firmware-private/releases/6.6.2/aer1600_release_2018_08_30'
                     '/aer1600_6_6_2_Release_2018_08_30.bin')
    # loader.load_over_rest("http://192.168.0.1", "admin", "44276b68", product="aer1600")
