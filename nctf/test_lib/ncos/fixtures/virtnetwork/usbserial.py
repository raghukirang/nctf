import serial
import logging
import time

logger = logging.getLogger("usbserial")

class USBSerial(object):
    """Essentially a wrapper for pyserial for init, reading, and writing to USB serial connected devices.
    Requires root privileges

    Usage:
    conn = USBSerial('/dev/ttyUSB0')
    conn.write('log')
    conn.write('\n')
    resp = conn.read_data()
    conn.close()

    Changing timeout:
    conn = USBSerial('/dev/ttyUSB0', baudrate=115200, timeout=5)
    conn.timeout = 15
    conn.write_timeout = 30
    """
    def __init__(self, port,
                 baudrate=115200,
                 timeout=10,
                 bytesize=serial.EIGHTBITS,
                 parity=serial.PARITY_NONE,
                 stopbits=serial.STOPBITS_ONE,
                 xonxoff=False,
                 rtscts=False,
                 dsrdtr=False,
                 write_timeout=None,
                 inter_byte_timeout=None):

        try:
            self._conn = serial.Serial(port=port, baudrate=baudrate, timeout=timeout, bytesize=bytesize,
                                       parity=parity, stopbits=stopbits, xonxoff=xonxoff, rtscts=rtscts, dsrdtr=dsrdtr,
                                       write_timeout=write_timeout, inter_byte_timeout=inter_byte_timeout)
        except Exception as e:
            logger.warning("{} on USBSerial init: {}".format(type(e).__name__, e))
            raise e
        else:
            logger.debug("USBSerial created: {}".format(self._conn))

    @property
    def timeout(self):
        return self._conn.timeout

    @timeout.setter
    def timeout(self, timeout):
        try:
            self._conn.timeout = timeout
        except (ValueError, TypeError):
            logger.error("Not a valid timeout: {}".format(timeout))

    @property
    def write_timeout(self):
        return self._conn.write_timeout

    @write_timeout.setter
    def write_timeout(self, write_timeout):
        try:
            self._conn.write_timeout = write_timeout
        except (ValueError, TypeError):
            logger.error("Not a valid write timeout: {}".format(write_timeout))

    def read_stream(self, size=1024):
        """
        Stream data from serial connection until timeout is reached
        :param size:
        :return: data: str
        """

        if not self._conn.isOpen():
            logger.warning("Port {} is not open, cannot read".format(self._conn.port))
            raise serial.SerialException

        # pyserial supports setting a blocking value timeout=None, but doing so will likely cause read_stream() to hang
        if self.timeout is None:
            logger.warning("WARNING: read_stream() with timeout=None is a bad idea")

        data = b''
        try:
            for chunk in iter(lambda: self._conn.read(size), b''):
                data += chunk
        except Exception as e:
            logger.warning("{} on self._conn.read(): {}".format(type(e).__name__, e))

        try:
            return data.decode()
        except UnicodeDecodeError as e:
            logger.warning(e)
            raise

    def read_data(self, size=1024):
        """
        Read size bytes from serial connection and return decoded string
        :param size: int
        :return: data: str
        """
        if not self._conn.isOpen():
            logger.warning("Port {} is not open, cannot read".format(self._conn.port))
            raise serial.SerialException

        data = b''
        try:
            data = self._conn.read(size)
        except Exception as e:
            logger.warning("{} on self._conn.read(): {}".format(type(e).__name__, e))

        try:
            return data.decode()
        except UnicodeDecodeError as e:
            logger.warning(e)
            raise

    def write(self, msg):
        try:
            return self._conn.write(msg.encode())
        except Exception as e:
            logger.warning("{} in USBSerial.write() port[{}]| {}".format(type(e).__name__, self._conn.port, e))
            raise e

    def close(self):
        self._conn.close()


class CPUSBSerialException(Exception):
    pass


class CPUSBSerial(USBSerial):
    """ A Helper class for doing things with CP physical routers connected via console over USB Serial adaptors
    Requires root privileges

    Usage:
    conn = CPUSBSerial('/dev/ttyUSB0', auth=secretkey)
    conn.write('log')
    conn.write('\n')
    resp = conn.read()
    conn.close()

    Get():
    log = conn.get('log')

    Load config:
    confjson = json.load(myconfigfile.json)  <--**pseudo code:**
    conn = CPUSBSerial('/dev/ttyUSB0', auth=secretkey)
    conn.load_config(confjson)


    """
    def __init__(self, port, auth=None, baudrate=115200, timeout=3, xonxoff=False, rtscts=False, dsrdtr=False,
                 write_timeout=None, inter_byte_timeout=None):
        try:
            super().__init__(port=port, baudrate=baudrate, timeout=timeout, xonxoff=xonxoff, rtscts=rtscts, dsrdtr=dsrdtr, write_timeout=write_timeout, inter_byte_timeout=inter_byte_timeout)
        except Exception as e:
            logger.warning("{} on USBSerial init: {}".format(type(e).__name__, e))
        else:
            logger.debug("CPUSBSerial created: {}".format(self._conn))
        self.auth = auth or None
        self.router_name = None
        self._conn.flush()
        self._conn.reset_input_buffer()
        self._conn.reset_output_buffer()

        # Perform auth steps if auth is provided
        if self.auth:
            # logger.debug("Doing auth")
            self._auth_serial()

    def flush(self):
        if self._conn.is_open:
            self._conn.reset_input_buffer()
            self._conn.reset_output_buffer()
            self._conn.flush()

    def read(self, size=1024):
        """
        read_stream() with a try catch.
        :param size: int
        :return: resp
        """
        resp = ""
        try:
            resp = self.read_stream(size)
        except serial.SerialException:
            pass
        except UnicodeDecodeError:
            logger.error("UnicodeDecodeError: check baudrate for mismatch!")
            pass
        return resp

    def _auth_serial(self):
        """
        Verify we are authenticated with the cp router for console access. Return true if we successfully authenticate
        or it we do not require authentication (HEAD builds). Return False if we are unable to authenticate
        :return: bool
        """
        timeout1 = self.timeout
        self.timeout = 2

        self.write('\n')
        rd = self.read()
        if '[console@' in rd:
            # Expected on HEAD builds
            logger.debug("already authenticated. likely HEAD build or session persisted")
            self.timeout = timeout1
            return True
        elif 'Enter password:' in rd:
            logger.debug("Authentication needed")
            self.send(self.auth)
            rd1 = self.read()
            if '[console@' in rd1:
                logger.debug('Serial authentication success')
                self.timeout = timeout1
                return True
            elif 'Enter password:' in rd1:
                logger.debug("WARNING: Auth unsuccessful. Attempt #2")
                self.flush()
                self.send(self.auth)
                rd2 = self.read()
                if '[console@' in rd2:
                    logger.debug('2nd attempt Auth serial success')
                    self.timeout = timeout1
                    return True
        else:
            logger.warning("no response from router serial on port {}!".format(self._conn.port))
            return False

    def router_ready(self):
        """
        Are we at the cli? Return True if the router is at the console and False if we don't appear to ever reach
         the console
        :return: bool
        """
        timeout1 = self.timeout
        self.timeout = 1
        check = 3
        while check:
            self.write('\n')
            rd = self.read()
            if '[console@' in rd:
                self.timeout = timeout1
                return True
            else:
                check -= 1
                time.sleep(.5)

        self.timeout = timeout1
        logger.warning("Router never appeared ready over serial console")
        return False

    def get(self, msg):
        """
        Combine send() and read() into one function
        :param msg: str
        :return: resp: str
        """

        self.send(msg)
        resp = self.read()
        return resp

    def send(self, msg):
        """
        Wrapper that writes msg and '\n' over serial
        :param msg:
        :return:
        """
        try:
            self.write(msg)
            self.write('\n')
        except Exception as e:
            logger.warning("{} in CPUSBSerial.send() port[{}]| {}".format(type(e).__name__, self._conn.port, e))
            return False
        return True

    def ctrlc(self):
        # Send Ctrl+c keyboard interrupt to router
        self.send('\x03')

    def ctrld(self):
        # Send Ctrl+d keyboard interrupt to router
        self.send('\x04')

    @staticmethod
    def format_config(confjson):
        """
        Make appropriate formatting changes to the confjson string that are needed for setting configs over cli
        :param confjson:
        :return: confjson
        """
        confjson = confjson.replace("'", "\"")
        confjson = confjson.replace("False", "false")
        confjson = confjson.replace("True", "true")
        confjson = confjson.replace("None", "null")
        confjson = confjson.replace("|", "\|")

        return confjson

    def _format_set_config(self, confjson):
        """
        Take a router config file formatted in the style of a saved bin, and format it for config setting over cli
        :param confjson:
        :return: setstr
        """
        setstr = 'set config {}'.format(confjson)
        setstr = self.format_config(setstr)

        return setstr

    def load_config(self, confjson):
        """
        Load a config on the attached router on this CPUSBSerial interface. The format of confjson is a raw
        decoded .bin saved from the router.

        :param confjson:
        :return: tuple (int, str): return code, and return message
        
        Return Codes:
        0 - SUCCESS
        1 - Problem reading configuration file
        2 - Router not ready
        3 - IndexError in config deletion
        4 - Error: invalid value
        5 - Invalid command
        
        """
        confdels = []
        confadds = None

        # Are there deletions?
        try:
            confdels = confjson[1]
        except KeyError:
            pass

        # Are there additions?...there should be
        try:
            confadds = confjson[0]['config']
        except KeyError:
            return 1, "Could not get config additions from json"

        # Are we in a state to begin sending config diff?
        if not self.router_ready():
            return 2, "Err: Router never appeared ready over console to receive config"

        # Do them deletes
        if confdels:
            # Trusting for now that the deletes in the config are in proper reverse applicable order
            for i in reversed(confdels):
                delstr = ""
                for a in i:
                    delstr = delstr + str(a) + "/"
                self.send("delete {}".format(delstr))
            # Check for index errors
            resp = self.read()
            if "IndexError" in resp:
                return 3, "INDEX ERROR in deletion of {}".format(delstr)
            elif "Error: invalid value" in resp:
                return 4, "{}".format(resp)

        # Do them additions
        self.send(self._format_set_config(confadds))
        resp = self.read()
        if "Error: invalid value" in resp:
            return 4, "{}".format(resp)
        elif "Invalid command" in resp:
            return 5, "{}".format(resp)
        return 0, "Successfully loaded config"

    def set_readonly(self):
        """
        Set readconlyconfig to true
        :return:
        """
        self.send('set control/system/readonlyconfig true')

    def factory_reset(self):
        """
        # Factory reset the router
        :return:
        """
        self.send('factory_reset')

    def reboot(self, verify=False):
        # TODO Handle bootloader output
        """
        Reboot the router via console and optionally, verify the reboot was successful
        Note: verify could produce false results on routers that print bootloader output

        :param verify: bool
        :return: suc: bool True or False on success
        """
        self.flush()

        suc = True
        ot = self.timeout
        self.timeout = 20
        self.send('set control/system/reboot true')
        if verify:
            data = b''
            try:
                data = self.read_data(256)
            except UnicodeDecodeError as e:
                logger.warning("UnicodeDecodeError while checking for reboot verification")
                logger.warning(e)
                logger.debug(str(data))
                return False
            # logger.debug(str(data))
            if isinstance(data, str):
                if 'Begin rebootHandler(True)' not in data:
                    logger.warning("Did not see router reboot on port {}".format(self._conn.port))
                    suc = False
                else:
                    logger.debug("router reboot verification success on port {}".format(self._conn.port))
        self.timeout = ot
        return suc
