# Preseed environment for interactive mode
# 'code' must be specified for config or license to be preseeded
preseed_interactive = {
	'config': 'wanbasic.json',  # Config store filename to grab from /preseed/config folder
	'license': 'license.json',  # License filename to grab from /preseed/license folder
	'code': 'preseed',          # Preseed code filename to grab from /preseed/code folder
	'dynamic': None             # Folder containing dynamic content to grab from /preseed/dynamic folder
}
