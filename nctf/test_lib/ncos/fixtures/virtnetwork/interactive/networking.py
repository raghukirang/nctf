# networking environments useful for setting up and running lethe in a networked environment interactively.
# For example, running two routers connected to a fake "internet" running VPN between eachother.  To run a network
# environment use this command:
#   sudo make interactive NETWORK=basic
# 'basic' can be replaced with whatever python dict defined in this file

# This is the traditional rtr setup (i.e make interactive) using a configuration file.
# Top level keys are the names of the object created and Type is the type of object, currently supports:
# router - lethe instance
# switch - a switch-like object where each link can arp eachother
# cloud - an internet like object where each interface has a different ip address. DHCP and all that soon to come
# host - A host will launch in the namespace, should be able to ping a connected router
# localbridge - a predefined bridge interface so you can link virtrouter to a real interface (in the default ns)
basic = {
	"nodes": {
		"rtr": {  # NOTE! Every key the top level must be less than 9 CHARS (sorry, veth names are short)
			"type": "router",
			"options": {  # these are the options passed into the VirtNamespace constructor. for a 'router' you can set config
				"config": "wanbasic.json"  # Config store filename to grab from /preseed/config folder
			},
		},
		"wc": {
			"type": "host",
			"options": {
				"route": [{
					"dest": "fd00:dead:beef:0002::5",
					"gateway": "fd00:dead:beef:0001::10"
				}]
			}
		},
		"lc": {
			"type": "host",
			"options": {
				"route": [{
					"dest": "default",
					"gateway": "192.168.0.1"
				}, {
					"dest": "default",
					"gateway": "fd00:dead:beef:0002::1",
					"metric": 512
				}]
			}
		}
	},
	"links": {
		"rtr": [  # other links defined by name that will be plugged into this device
			{
				"source": {
					"options": {
						# options to be applied on my end of a link plugged into this object. 'router' only has port
						"port": 0  # corresponds with wan port
					}
				},
				"destination": {
					"name": "wc",
					"options": {  # these are the args passed to other end the link (in this case 'wc')
						"ip_address": ["192.168.1.5/24", "fd00:dead:beef:0001::5/64"]
					}
				}
			},
			{
				"source": {
					"options": {
						"port": 1  # corresponds with lan port
					}
				},
				"destination": {
					"name": "lc",
					"options": {  # these are the args passed to other end the link (in this case 'lc')
						"ip_address": ["192.168.0.5/24", "fd00:dead:beef:0002::5/64"]
					}
				}
			}
		]
	}
}

# this is how you bridge lethe to a real interface.  First create bridges br0 and br1.  br0 will be the "wan" so set
# the master of your internet connection to br0 (e.g. eth0). br1 represent the link plugged into lan port 1 on the
# router.  Bridge eth1 (or a tap device) to br1. Now you can access the router locally and it will have internet. Here's
# the typical set of commands:
#   ip link add br0 type bridge
#   ip link set eth0 master br0
#   ip link set br0 up
#   dhclient br0
#   ip link add br1 type bridge
#   ip link set eth1 master br1
#   ip link set br1 up
# NOTE! BE PATIENT it can take half a minute for connectivity to establish
bridge = {
	"nodes": {
		"rtr": {
			"type": "router",
			"options": {
				"config": "config.json"
			}
		},
		"br0": {
			"type": "localbridge"  # get bridge ties into existing bridges, it's a bad idea to try to pass link args to them
		},
		"br1": {
			"type": "localbridge"
		}
	},
	"links": {
		"rtr": [
			{
				"source": {
					"options": {
						"port": 0
					},
				},
				"destination": {
					"name": "br0"
				}
			},
			{
				"source": {
					"options": {
						"port": 1
					}
				},
				"destination": {
					"name": "br1"
				}
			}
		]
	}
}

# this is client-->router-->cloud-->router-->client interconnect
interconnect = {
	"nodes": {
		"rtr1": {
			"type": "router",
			"options": {
				"config": "interactive/cloud1.json"
			}
		},
		"rtr2": {
			"type": "router",
			"options": {
				"config": "interactive/cloud2.json"
			}
		},
		"cl1": {
			"type": "host",
			"options": {
				"route": {
					"dest": "default",
					"gateway": "192.168.0.1"
				}
			}
		},
		"cl2": {
			"type": "host",
			"options": {
				"route": {
					"dest": "default",
					"gateway": "192.168.1.1"
				}
			}
		},
		"cloud": {
			"type": "cloud"
		}
	},
	"links": {
		"rtr1": [
			{
				"source": {
					"options": {
						"port": 0  # rtr addr is 15.0.0.1/30
					}
				},
				"destination": {
					"name": "cloud",
					"options": {
						"port": 0  # cloud addr is 15.0.0.2/30
					}
				}
			},
			{
				"source": {
					"options": {
						"port": 1
					}
				},
				"destination": {
					"name": "cl1",
					"options": {
						"ip_address": "192.168.0.5/24"
					}
				}
			}
		],
		"rtr2": [
			{
				"source": {
					"options": {
						"port": 0  # rtr addr is 25.0.0.1/30
					}
				},
				"destination": {
					"name": "cloud",
					"options": {
						"port": 1  # cloud addr is 25.0.0.2/30
					}
				}
			},
			{
				"source": {
					"options": {
						"port": 1
					}
				},
				"destination": {
					"name": "cl2",
					"options": {
						"ip_address": "192.168.1.5/24"
					}
				}
			}
		]
	}
}

vpn = {
	"nodes": {
		"rtr1": {
			"type": "router",
			"options": {
				"config": "ipsec/vpn1.json"
			}
		},
		"rtr2": {
			"type": "router",
			"options": {
				"config": "ipsec/vpn2.json"
			}
		},
		"cl1": {
			"type": "host",
			"options": {
				"cmd": "roxterm -n '%h' --tab -T 'VIRTTEST'",
				"route": {
					"dest": "default",
					"gateway": "192.168.0.1"
				}
			}
		},
		"cl2": {
			"type": "host",
			"options": {
				"cmd": "roxterm -n '%h' --tab -T 'VIRTTEST'",
				"route": {
					"dest": "default",
					"gateway": "192.168.1.1"
				}
			}
		},
		"cloud": {
			"type": "cloud"
		}
	},
	"links": {
		"rtr1": [
			{
				"source": {
					"options": {
						"port": 0  # router addr is 15.0.0.1/30
					},
				},
				"destination": {
					"name": "cloud",
					"options": {
						"port": 0  # cloud addr is 15.0.0.2/30
					}
				}
			},
			{
				"source": {
					"options": {
						"port": 1
					},
				},
				"destination": {
					"name": "cl1",
					"options": {
						"ip_address": "192.168.0.5/24"
					}
				}
			}
		],
		"rtr2": [
			{
				"source": {
					"options": {
						"port": 0  # rtr addr is 25.0.0.1/30
					},
				},
				"destination": {
					"name": "cloud",
					"options": {
						"port": 1  # cloud addr is 25.0.0.2/30
					}
				}
			},
			{
				"source": {
					"options": {
						"port": 1
					},
				},
				"destination": {
					"name": "cl2",
					"options": {
						"ip_address": "192.168.1.5/24"
					}
				}
			}
		]
	}
}
