"""
virtservers.py - convert json server configuration into running servers in a namespace

Copyright (c) 2017 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""
import ipaddress
from copy import deepcopy

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.dhcpserver import DHCPd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.dnsserver import DNSd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.ecmserver import ECMd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.ntpserver import NTPd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.uberserver import UBERd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.httpserver import HTTPd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.radiusserver import RADIUSd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.ftpserver import FTPd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.dumb_smtpd import NSSMTPD


class VirtServers(object):

	server_map = {
		'ntp': NTPd,
		'dns': DNSd,
		'dns6': DNSd,
		'http': HTTPd,
		'http6': HTTPd,
		'ecm': ECMd,
		'uber': UBERd,
		'dhcp': None,
		'radius': RADIUSd,
		'ftp': FTPd,
		'smtp': NSSMTPD
	}

	def __init__(self, ns):
		self.ns = ns
		self.servers = []
		self.dhcpd = None
		self.radiusd = None

	def start_servers(self, servers):
		for name, svr in self.server_map.items():
			try:
				options = deepcopy(servers[name])
				ips = [options['ip_address']] if isinstance(options['ip_address'], str) else options['ip_address']
				family = 6 if name.endswith('6') else 4
				ips = [ip for ip in ips if ipaddress.ip_interface(ip).version == family]
				options.pop('ip_address')
			except KeyError:
				continue
			if name == 'dhcp':
				if not self.dhcpd:
					self.dhcpd = DHCPd(self.ns)
				for ip in ips:
					self.dhcpd.add_dhcp_pool(ip, options.get('nameserver'))
				if options.get('reservations'):
					self.dhcpd.add_reservations(options['reservations'])
				continue
			if name == 'radius':
				# FreeRADIUS only supporting 1 IP currently
				ip = ips[0].split('/')[0]
				if not self.radiusd:
					self.radiusd = RADIUSd(self.ns, ip)
					self.radiusd.start()
				continue
			for ip in ips:
				ip = ip.split('/')[0]
				run = svr(ip, **options)
				self.servers.append(run)
				run.start(self.ns)

	def stop_servers(self):
		for server in self.servers:
			server.stop()
		del self.servers[:]

		if self.dhcpd:
			self.dhcpd.stop()
		self.dhcpd = None

		if self.radiusd:
			self.radiusd.stop()
		self.radiusd = None
