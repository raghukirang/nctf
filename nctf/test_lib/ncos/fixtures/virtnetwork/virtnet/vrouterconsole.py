"""
vrouterconsole.py - console for virtual router

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import re
import socket
import logging

logger = logging.getLogger(__name__)


class VRouterConsole:

	def __init__(self, rtr=None, quiet=False):
		logger.info("Init VRouterConsole")
		self.quiet = quiet
		self.host = None
		self.password = None
		if rtr:
			self.host = rtr.stdio_socket
			mac = rtr.mac
			if not mac:
				logger.warning("Mac not provided for router {}. Assuming default mac as 00:00:00:00:00:00".format(rtr))
				mac = "00:00:00:00:00:00"

			tmp_lst = mac.split(":")
			mac_last_four = "".join(tmp_lst[4:])

			self.password = "1415" + mac_last_four

		self.socket_conn = None
		self.warn_msg = "Warning: Remote Access is enabled along with techsupport_access, this is a high risk to security!\r\n"

	def __del__(self):
		self.deinit()

	def socket_connect(self, host=None, password=None):

		"""
			Connect to virtconsole socket
			:param host: stdio_socket_value
			:param password: str

		"""
		if host is None:
			host = self.host
		if password is None:
			password = self.password

		self.socket_conn = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

		try:
			self.socket_conn.connect(str(host))
			self.socket_conn.send(b'\r\n')
			data, no_data = self.__read_data(2)

			if "Enter password:" in data:
				self.socket_conn.send((str(password)).encode())
				self.socket_conn.send(b'\r\n')
			elif "[console@" not in data:
				logger.warning("Couldn't find password or console prompt trying to connect to socket {}".format(host))

			self.socket_conn.send(b'\r\n')
			data, no_data = self.__read_data(1)
		except Exception as e:
			logger.error("Socket error connecting to file {}".format(host))
			raise Exception(e)


	def socket_close(self, rtr=None):
		rtr = rtr or self.host
		logger.debug("Closing virtconsole socket to {}".format(rtr))
		self.socket_conn.send(b'\r\n')
		data, no_data = self.__read_data(1)

		on_shell = True
		count = 0
		while on_shell:
			if not no_data:
				if data:
					try:
						match = re.search("\[console", data)
					except Exception as e:
						logger.warning("Exception thrown:\n{}".format(e))

					if match is None:
						self.socket_conn.send("exit".encode())
						self.socket_conn.send(b'\r\n')
						data, no_data = self.__read_data(1)
						count += 1
					else:
						on_shell = False
				if count > 10:
					logger.error("socket_close: Couldn't return to console prompt on router {}".format(rtr))
					raise Exception("socket_close: Couldn't return to console prompt on router {}".format(rtr))
			else:
				on_shell = False
		self.socket_conn.send("exit".encode())
		self.socket_conn.send(b'\r\n')

		self.socket_conn.shutdown(socket.SHUT_RDWR)
		self.socket_conn.close()
		self.socket_conn = None


	def set_data(self, set_path, set_value, oper):

		"""
			Set config store values using virtconsole
			:param set_path: str
			:param set_value: str
			:Returns: True/False, error if any
		"""

		return_value = True
		cmd = str(oper) + " " + str(set_path) + " " + str(set_value)
		try:
			self.socket_conn.send(cmd.encode())
			self.socket_conn.send(b'\r\n')
		except Exception as e:
			logger.error("Error trying to set {} to {}".format(set_path, set_value))
			raise Exception(e)

		data, no_data = self.__read_data(2)

		logger.debug("No data received after set (True/False)? {}".format(no_data))
		logger.debug("Set Response is {}".format(data))

		if data:
			data_new = data.lstrip(cmd)
			data_new = data_new.replace(self.warn_msg, "")
			match_pat = "(\r\n)?" + "\[console"
			try:
				match = re.search(match_pat, data_new)
			except Exception as e:
				logger.warning("Exception thrown:\n{}".format(e))
				match = None

			# if match's span does not start at 0, something printed before it--probably a msg about how things were unsuccessful
			# If it isn't a failure msg, setting a value in config store should not result in printing something & should be fixed.
			if match and match.span()[0] != 0:
				return False, data_new.strip().strip("\r\n")

			if match is None:
				match_str = "\r\n(.*?)\r\n" + "\[console"
				try:
					match_new = re.search(match_str, data_new, re.DOTALL)
				except Exception as e:
					logger.warning("Exception thrown retrieving the set error:\n{}".format(e))
					match_new = None

				if match_new is None:
					return False, "Could not retrieve set error"
				else:
					error_msg = match_new.group(1)
					error_msg = error_msg.strip().strip("\r\n")
					return False, error_msg

		return True, "Success"


	def get_data(self, get_path):

		"""
			Get output of get command on virtconsole
			:param get_path: str
			:Returns: True/False, get output
		"""

		cmd = "get " + str(get_path)
		try:
			self.socket_conn.send(cmd.encode())
			self.socket_conn.send(b'\r\n')
		except Exception as e:
			logger.error("Error trying to get value at {}".format(get_path))
			raise Exception(e)

		data, no_data = self.__read_data(2)

		logger.debug("No data received from get (True/False)? {}".format(no_data))
		if not self.quiet:
			logger.debug("Get Response is {}".format(data))

		if no_data:
			return False, "Could not read data from virtconsole"

		if data:
			data_new = data.replace(self.warn_msg, "")
			match_pat = cmd + "\r\n(.*?)\r\n" + "\[console"
			try:
				match = re.search(match_pat, data_new, re.DOTALL)
			except Exception as e:
				logger.warning("Exception thrown:\n{}".format(e))
				match = None

			if match is None:
				return False, "No data retrieved from get"

		return True, match.group(1)


	def run_cmd(self, command):

		"""
			Run any command (excluding set/get) on virtconsole
			:param command: str
			:Returns: True/False, Error/Command output
		"""

		try:
			self.socket_conn.send(command.encode())
			self.socket_conn.send(b'\r\n')

		except Exception as e:
			logger.error("Error trying to run command {}".format(command))
			raise Exception(e)

		if command.split(" ")[0] in ("tcpdump", "unittest"):
			timeout = 10
		else:
			timeout = 2

		data, no_data = self.__read_data(timeout)

		logger.debug("No data received on running command (True/False)? {}".format(no_data))
		logger.debug("Run command output is \n{}".format(data))

		if no_data:
			return False, "Could not read data from virtconsole"

		if data:
			data_new = data.lstrip(command).strip()
			# Verifying if there is no output from command
			match_pat = "^\[console"
			try:
				match = re.search(match_pat, data_new)
			except Exception as e:
				logger.warning("Exception thrown:\n{}".format(e))
				match = None

			if match is None:
				match_str = "(.*?)(\r\n)?" + "\[console"
				try:
					match_new = re.search(match_str, data_new, re.DOTALL)
				except Exception as e:
					logger.warning("Exception thrown retrieving output of command:\n{}".format(e))
					match_new = None

				if match_new is None:
					match_str1 = "(.*)"
					try:
						match_new1 = re.search(match_str1, data_new, re.DOTALL)
					except Exception as e:
						logger.warning("Exception thrown retrieving output of command:\n{}".format(e))
						match_new1 = None
					if match_new1 is None:
						return True, "No output from running command {}".format(command)
					else:
						command_output = match_new1.group(1)
						command_output = command_output.strip().strip("\r\n")
						return True, command_output
				else:
					# Storing command response when prompt is console
					command_output = match_new.group(1)
					command_output = command_output.strip().strip("\r\n")
					return True, command_output
			else:
				return True, "No output from executing command {}".format(command)


	def __read_data(self, mytimeout):
		"""
			Read data from socket

			:Returns: Output read from socket
		"""
		orig_timeout = self.socket_conn.gettimeout()
		self.socket_conn.settimeout(mytimeout)
		data = ''
		size = 1024
		loop = True
		no_data = False

		while loop:
			try:
				packet = self.socket_conn.recv(size)
			except Exception as e:
				if data == '':
					no_data = True
					logger.info('No data received from socket [%s]' % e)
				packet = ''
				loop = False

			if not packet:
				loop = False
			else:
				data += packet.decode()
		self.socket_conn.settimeout(orig_timeout)
		return data, no_data

	def deinit(self):
		if self.socket_conn:
			logger.info("Closing socket")
			self.socket_conn.close()
			self.socket_conn = None
