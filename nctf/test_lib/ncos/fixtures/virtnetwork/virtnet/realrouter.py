"""
realrouter.py - Router hardware device control

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""
# Standard
import time
import logging
from json import load
import json
import zlib
import os
import re
import shutil
from nctf.test_lib.ncos.fixtures.virtnetwork.usbserial import CPUSBSerial
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.netns import Bridge, NsIface
from nctf.test_lib.ncos.fixtures.virtnetwork.lib import footilities as ut

logger = logging.getLogger('realrouter')

# for console_config()
CONFIG_ERRORS = [
"Error: invalid value",
"Invalid command",
"IndexError"
]

BAUDRATE_MAP = {
	"AER1600": 115200,
	"AER1650": 115200,
	"AER2100": 115200,
	"AER2200": 115200,
	"AER3100": 115200,
	"AER3150": 115200,
	"AP22": 115200,
	"CBA850": 57600,
	"IBR1100": 57600,
	"IBR1150": 57600,
	"IBR1700": 115200,
	"IBR1700-FIPS": 115200,
	"IBR200": 57600,
	"IBR250": 57600,
	"IBR350": 57600,
	"IBR600B": 57600,
	"IBR650B": 57600,
	"IBR600C": 115200,
	"IBR650C": 115200,
	"IBR900": 115200,
	"IBR900-FIPS": 115200,
	"IBR950": 115200,
	"IBR950-FIPS": 115200,
	"MBR1200B": 57600
}

def read_json_file(path):
	try:
		with open(path) as f:
			retval = load(f)
	except Exception as e:
		logger.debug('Got an Exception Reading JSON {}'.format(e))
		return {}
	return retval


class RealRouter(object):
	"""
	A real hardware router, connected through a physical interface on the host system
	"""
	def __init__(self, name, mac=None, interfaces=None, config=None, preseed=False, write=False, reboot=False,
				 serial=None, type=None, client_config=True):
		self.name = name
		self.mac = mac or None
		self.write = write or False
		self.preseed = preseed or False
		self.config = config or None
		self.interfaces = interfaces or []
		self.serial = serial or None
		self.serialconn = None
		self.reboot = reboot or False
		self.type = type or None
		self.client_config = client_config
		self.ip = None

		# For real routers, bridges would be synonymous with ports since there should be a 1:1 relationship
		self.bridges = []
		self.links = []

	def start(self):

		#Initially, only create the bridge and plug the phy iface into it. Wait until links to do any plugging
		for interface in self.interfaces:
			logger.debug("Creating bridge for {}".format(interface))
			thisbridge = Bridge("{}".format(interface))
			thisbridge.up()

			# Create Nsiface
			thisiface = NsIface(interface)
			thisbridge.addif(thisiface)
			self.bridges.append(thisbridge)

		# Create USBSerial connection to physical router
		if self.serial:
			try:
				baud = BAUDRATE_MAP[self.type]
			except KeyError:
				baud = 115200
			self.serialconn = CPUSBSerial(self.serial, auth=ut.mac2auth(self.mac), baudrate=baud, timeout=2)

		# Router config
		if self.config:

			if self.client_config:
				# client_config can be overridden by setting client_config : False in router options
				logger.debug("[{}:{}] client_config detected, shorting serial config".format(self.name, self.mac))
				return
			if not self.serialconn:
				logger.warning("client_config:False serialconn:None!! Nothing config router with!")
				return

			# TODO: get ahf working here to help find these configs in case they're nested.
			confjson = read_json_file(self.config)
			# Could we find the config file?
			if not confjson:
				logger.warning("Error, improper format or we could not find config {}".format(self.config))
				return

			# PRESEED - Load and boot router with desired config. Useful for some testing scenarios
			# WRITE - Enable writing config to flash
			# Preseed will superseed write if set
			if not self.write and not self.preseed:
				# Setting readonlyconfig by default
				self.serialconn.set_readonly()
			elif self.preseed:
				if self.write: logger.debug("Warning: Preseed option superseeding write option")
				self.serialconn.send('set control/system/readonlyconfig false')

			code, msg = self.serialconn.load_config(confjson)
			if code != 0:
				logger.warning("Error loading config through serial to router {}".format(self.mac))
				logger.warning("Error msg: {}".format(msg))
				return

			logger.debug(msg + " for router {}".format(self.mac))
			if self.preseed:
				logger.info("Rebooting router for preseed")
				self.serialconn.reboot()
				#TODO in the future do something to remove this wait, or impliment preseed in bootloader
				time.sleep(45)
				self.serialconn.redo_auth()
				self.serialconn.router_ready()

			#Debug print
			#logger.debug(self.serialconn.get('diff'))

	def plug(self, nsif, port=0):
		# Use port number as index for bridge to connect interface to
		logger.debug("plugging {} into {}".format(nsif, self.bridges[port]))
		self.bridges[port].addif(nsif.iface)

		self.links.append(nsif.iface)
		nsif.up()

	def after_start(self):
		# Placeholder for after_start options to be added later on
		pass

	def down(self):
		pass

	def deinit(self):
		for l in self.links:
			l.deinit()
		for bridge in self.bridges:
			bridge.deinit()
		if self.serialconn:
			logger.debug(self.serialconn._conn)
			# deinit is getting called twice per single realrouter. is_open check as workaround
			if self.serialconn._conn.is_open:
				#if self.preseed: # Commenting out this preseed factory reset for now. this might need to be tweaked
				#	logger.info("Factory resetting router {}".format(self.mac))
				#	self.serialconn.factory_reset()
				#el
				if self.reboot:
					logger.info("Rebooting router {}".format(self.mac))
					self.serialconn.reboot(verify=True)
					logger.debug("[{}] reboot complete. sleeping 60 seconds".format(self.mac))
					time.sleep(60)
				self.serialconn.ctrlc()
				self.serialconn.ctrld()
			self.serialconn.flush()
			self.serialconn.close()

	def console_config(self, set_path, set_value, oper):

		cmd = str(oper) + " " + str(set_path) + " " + str(set_value)
		cmd = self.serialconn.format_config(cmd)
		self.serialconn.send(cmd)
		data = self.serialconn.read()

		# logging if we see any error
		if "Error" in data:
			logger.warning("console config produced error:\n{}".format(data))

		# real configuration errors
		for e in CONFIG_ERRORS:
			if e in data:
				logger.debug(data)
				return False, data
		else:
			return True, 'Successful config'

	@staticmethod
	def format_config(confjson):

		confjson = confjson.replace("'", "\"")
		confjson = confjson.replace("False", "false")
		confjson = confjson.replace("True", "true")
		confjson = confjson.replace("None", "null")
		#confjson = confjson.replace("|", "\|")

		return confjson

	def clientside_config(self, cl, ahf, poll=False, ip="192.168.0.1", port="80"):
		# for now, making assumption routers are in factory default state of 192.168.0.1 LAN side IP
		if not self.config:
			return

		confjson = ahf.get_asset_json(self.config)

		# Could we find the config file?
		if not confjson:
			logger.warning("Error, improper format or we could not find config {}".format(self.config))
			return
		# logger.debug(confjson)

		# CHECK TO SEE IF LAN IP CHANGES
		try:
			newip = confjson[0]['config']['lan']["0"]["ip_address"]
			logger.debug(newip)
			if newip != "192.168.0.1":
				self.ip = newip
		except KeyError:
			pass

		# CASE 1: Do a bin push
		if self.preseed:
			logger.debug("clientside_config(): CASE 1")
			return self.clientside_bin(cl, confjson, ip, port, poll=poll)

		# CASE 2: Piecemeal apply config to router config API
		logger.debug("clientside_config(): CASE 2")

		# Throw out a ping for ARP
		cl.ns.nsexec("ping {} -c 1".format(ip))

		# Handling self.write
		if not self.write:
			logger.debug('doing readonly')
			if not self.serialconn:
				#confstr = "{\"system\": {\"readonlyconfig\": True}}"
				#resp = self.curl_config(cl, str(confstr), top="control")
				self.set_readonly_true(cl, ip, port)
			else:
				self.serialconn.set_readonly()

		confdels = None
		# Are there deletions?
		try:
			confdels = confjson[1]
		except KeyError:
			pass

		if confdels:
			for i in reversed(confdels):
				delstr = ""
				for a in i:
					delstr = delstr + str(a) + "/"
				delcmd = 'curl --basic -u admin:{} -X DELETE http://{}:{}/api/{}'.format(self.mac[4:], ip, port, delstr)
				logger.debug(delcmd)
				resp = cl.ns.nsexec(delcmd)
				logger.debug(resp)

		# Do a PUT to api with data string
		conf = self.format_config(str(confjson[0]['config']))
		logger.debug(conf)

		resp = self.curl_config(cl, str(confjson[0]['config']), ip, port)
		if not resp:
			logger.warning("Problem issuing curl command to router {}. response:".format(self.mac))
			logger.warning(resp)

	def clientside_bin(self, cl, conf, ip="192.168.0.1", port="80", poll=True):
		"""
		Load an encoded bin file on the router
		:param cl: client namespace init object
		:param conf: json conf string that will be encoded and compressed into bin
		:param ip: IP address
		:param port: Port
		:param poll: bool: if true, attempt to ping the router while it's unpacking the bin to determine when it comes back up
		:return:
		"""

		try:
			os.mkdir("tmp_bins", mode=0o775)
		except FileExistsError:
			pass

		with open('tmp_bins/test.bin', 'wb') as f:
			f.write(zlib.compress(json.dumps(conf).encode()))

		# Throw out a ping for ARP
		cl.ns.nsexec("ping {} -c 1".format(ip))

		cmd = 'curl --basic -u admin:{} http://{}:{}/config_save -F config_file=@tmp_bins/test.bin'.format(self.mac[4:],
														ip, port)
		resp = cl.ns.nsexec(cmd)
		# logger.debug(resp)  # verbose logging
		if resp:
			if not b"\"success\": true, \"data\": \"valid\"" in resp:
				logger.warning("PROBLEM WITH CONFIG BIN PUSH!!!!!!!!!")
				logger.debug(resp)
				return 0
		else:
			logger.warning("clientside_bin(): router response: None")
			return 0

		if poll:
			logger.debug('Sleeping waiting for bin to load and router to reboot')
			time.sleep(20)
			down = True

			# Do things to try and handle the new IP address that was just configured on the router
			if self.ip:
				ip = self.ip
				clip = self.ip
				foo = clip.split('.')
				foo[3] = str(int(foo[3]) + 1)

				clip = foo[0] + "." + foo[1] + "." + foo[2] + "." + foo[3]
				# logger.debug(clip)

				# Thus far this is only used by network.setup_physical_routers(), which guarantees naming
				# convention for the veth. This could be made more dynamic to support use by a test.
				cl.ns.nsexec('ip add add {}/24 dev veth-v-client0'.format(clip))

			# ping poll the router until it comes back up
			ct = 0
			while down:
				ct += 1
				time.sleep(5)
				resp = cl.ns.nsexec('ping {} -c 1'.format(ip))
				if ct > 45:
					logger.warning("Router never recovered from config load")
					return 0
				if resp:
					down = False

		try:
			shutil.rmtree("tmp_bins", ignore_errors=True)
		except Exception as e:
			logger.warning("problem removing tmp_bins")
			logger.warning(e)
			pass
		return 1

	def curl_config(self, cl, conf, ip="192.168.0.1", port="80", top="config"):
		"""

		:param cl: client namespace init object
		:param conf: json to load onto router
		:param ip: IP address
		:param port: port
		:return:
		"""

		cmd = "curl --basic -u admin:{} http://{}:{}/api/{} -X PUT -d data='{}'".format(self.mac[4:], ip, port, top,
												self.format_config(str(conf)))
		# logger.debug(cmd)  # Verbose
		return cl.ns.nsexec(cmd)

	def get_build_version(self):
		# Get the build version from the router over serial

		if not self.serialconn:
			logger.debug("No serial connection to get build info with")
			return None
		cmd = "cat /status/fw_info/build_version"
		self.serialconn.set_timeout(1)

		bv = self.serialconn.get(cmd)
		bv = bv.lstrip(cmd)

		match_pat = "\r\n(.*?)\r\n" + "\[console"
		try:
			match = re.search(match_pat, bv)
		except Exception as e:
			logger.warning("Exception thrown:\n{}".format(e))
			match = None

		self.serialconn.reset_timeout()

		if match:
			return match.group(1)
		else:
			logger.warning("failed to get router build version")
			return None

	def set_readonly_true(self, client, ip="192.168.0.1", port="80"):
		# Helper for setting control/system/readonlyconfig true

		readonlystr = "{\"system\": {\"readonlyconfig\": True}}"
		self.curl_config(client, str(readonlystr), ip=ip, port=port, top="control")
