"""
virtrouter.py - Simple virtual router abstraction

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import logging
import random
import time

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.procmon import ProcessMonitor
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.vmcontrol import Qmp
from .netns import *

logger = logging.getLogger(__name__)
class KvmController(object):
	""" Wraps a KVM instance+associated veth/tap/bridges to create virtual networks
	for testing. KvmController should have MINIMAL INTELLIGENCE and remain useable outside the
	test framework. By default it wraps qemu with the minimal structure to create a virtual
	router. All else must be specified in the arguments as is done by base/basenet.py and
	the tests that inherit from it.

	More info: https://redmine.cradlepoint.com/projects/coconut/wiki/Virtual_Router_Testing
	"""

	qemu_vers = {'default': 'qemu-system-i386', 'gentoo': 'qemu-system-x86_64', 'x86_64': 'qemu-system-x86_64'}
	qemu_args = '-enable-kvm -m {mem} -cpu host {out} {display} {kernel} -device usb-ehci,id=ehci {net} '
	qemu_qmp = '-qmp unix:{socket},server,nowait '
	qemu_drive = '-drive file={file},if=virtio,{snapshot} '
	tapstr = '-netdev tap,id={0},ifname={0},vhost=on,script=no -device virtio-net,netdev={0},mac={1} '
	sportstr = '-device virtio-serial -chardev socket,id=tcp_socket0,host=127.0.0.1,port={},server,nowait -device virtserialport,chardev=tcp_socket0,name=org.virtrouter.port.0 '
	stdiostr = '-chardev socket,id=gstdio,path={},server,nowait,mux=on -serial chardev:gstdio -monitor chardev:gstdio '

	def __init__(self,
	             name,
	             kernel=None,
	             output=None,
	             display=None,
	             drive=None,
	             qmpsock=None,
	             mem=256,
	             virtsport=None,
	             nets=2,
	             ns=None,
	             stdio_socket=None,
	             qemu_vers='default',
	             snapshot=False,
	             **kwargs):
		# logger.debug("init KVMcontroller")
		self.name = name
		self.kernel = "-kernel {}".format(kernel) if kernel else ''
		self.out = output or ''
		self.drive = drive
		self.snapshot = snapshot
		self.qmpsock = qmpsock
		self.mem = mem
		self.nets = nets
		self.ns = ns or NetNs('{}-r'.format(name))  # we get our own namespace, -r is just historical
		self.stdio_socket = None

		uname = os.uname()
		if any('gentoo' in x for x in list(uname)):
			qemu = self.qemu_vers['gentoo']
		else:
			qemu = self.qemu_vers[qemu_vers]

		self.display = '-nographic' if (display == 'nographic' or display is None) else '-display %s' % display
		self.kvmstr = ' '.join([qemu, self.qemu_args])
		self.kvmproc = None

		# Enable QMP control
		if self.qmpsock:
			self.kvmstr += self.qemu_qmp.format(socket=self.qmpsock)

		# Enable virtual serial port
		if virtsport:
			self.kvmstr += self.sportstr.format(virtsport)

		if stdio_socket:
			self.stdio_socket = stdio_socket
			try:
				os.remove(self.stdio_socket)
			except FileNotFoundError:
				pass
			self.kvmstr += self.stdiostr.format(stdio_socket)

		file = self.drive
		# todo in some cases it would be convenient to automatically create a snapshot of an image

		self.kvmstr += self.qemu_drive.format(
			file=file, snapshot='index=0,media=disk' if self.snapshot else 'format=raw')

		# Loopback is necessary for normal usage, bring it up
		NsIface('lo', ns=self.ns).up()

		# Per-net namespaces
		self.virtnets = []

		try:
			num_nets = len(nets)
		except TypeError:
			num_nets = nets

		for i in range(num_nets):
			net = {}

			# Tap and virtio interfaces for KVM (just placeholders, KVM creates them)
			net['tap'] = KvmTap('{}.{}'.format(name, i), ns=self.ns)
			net['virtio'] = VirtIo('.{}'.format(i), qmpsock=self.qmpsock, ns=self.ns)

			self.virtnets.append(net)

	def getnet(self, idx):
		return self.virtnets[idx]

	def start(self):
		tapspec = []
		def rand_mac():
			return '00:30:44:ff:%02x:%02x' % (random.randint(0x00, 0xff), random.randint(0x00, 0xff))
		for i, net in enumerate(self.virtnets):
			try:
				mac = self.nets[i].get('mac') or rand_mac()
			except TypeError:
				mac = rand_mac()
			tapspec.append(self.tapstr.format(net['tap'].ifname, mac))

		c = self.kvmstr.format(kernel=self.kernel, out=self.out,
				       display=self.display, net=' '.join(tapspec), mem=self.mem)

		self.kvmproc = ProcessMonitor(cmd=c, autoRestart=False, ns=self.ns)

		self.kvmproc.start()
		for t in (net['tap'] for net in self.virtnets):
			# Wait to ensure links come up within 4 seconds
			for i in range(20):
				if t.exists():
					break
				time.sleep(0.2)

	def stop(self):
		if self.kvmproc:
			# helps in ns cleanup
			for iface in self.tap_links + self.virtio_links:
				iface.deinit()
			self.kvmproc.stop()
			self.kvmproc = None
		try:
			os.remove(self.stdio_socket)
		except FileNotFoundError:
			pass
		except TypeError:
			pass
		self.stdio_socket = None

	def reboot(self):
		Qmp(self.qmpsock).send_command('{ "execute":"system_reset" }')

	def deinit(self):
		self.stop()

		if self.qmpsock:
			f = os.path.basename(self.qmpsock)
			if os.path.exists(f):
				os.remove(f)
			self.qmpsock = None

		# TODO separate out this class creating a namespace?
		if self.ns:
			self.ns.deinit()
			self.ns = None

		del self.virtnets[:]

	@property
	def tap_links(self):
		return [x['tap'] for x in self.virtnets]

	@property
	def virtio_links(self):
		return [x['virtio'] for x in self.virtnets]
