"""
virtlink.py - Wraps veth devices and keeps track of which namespace we are "connected" to. "Plug" these links into
other VirtNamespaces

Copyright (c) 2010-2016 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""
from abc import ABCMeta, abstractmethod
from copy import deepcopy

from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.netns import NsIface, NetNs
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.udhcpc import udhcpc_lease
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtservers import VirtServers


class LinkException(Exception):
	pass


class VirtLink(metaclass=ABCMeta):

	def __init__(self, name):
		self.name = name
		self._dest_ns = None
		self.options = {}
		# TODO deprecated use of server options
		self.servers = None

	def __del__(self):
		self.deinit()

	@abstractmethod
	def up(self):
		pass

	@abstractmethod
	def down(self):
		pass

	@abstractmethod
	def deinit(self):
		if self.servers:
			self.servers.stop_servers()
		self.servers = None
		self._dest_ns = None

	@property
	@abstractmethod
	def iface(self):
		return None

	@property
	def peer(self):
		"""override to return the peer for a device that supports it, otherwise returns this object"""
		return self

	@property
	def plugged_namespace(self):
		return self._dest_ns

	def plug(self, virtnamespace):
		"""
		Plug can be called twice.  The first namespace that calls us is set as _source_ns the second is _dest_ns.
		TODO: This is actually only true with veths, simplify this logic by only having one (_source_ns or _dest_ns) in this base class.
		"""
		if self._dest_ns:
			raise LinkException("Link is already connected to %s" % self._dest_ns)

		self._dest_ns = virtnamespace

		if virtnamespace.ns:
			virtnamespace.ns.addnsif(self.iface)

		return self

	def unplug(self):
		"""Unplugs this link from virtnamespace (moves it back to default ns)."""
		iface = self.iface
		if iface:
			self._dest_ns.ns.remnsif(iface)
		self._dest_ns = None
		return self

	def link_options(self, **options):
		"""Update link options dict using key word args"""
		self.options = options

	def apply_options(self):
		return self._apply_options(self.iface, **self.options)

	def _apply_options(self, nsif, ip_address=None, bridge=None, nat=False, mac=None, mtu=None, **options):
		# if this is a bridge, apply options to the bridge, except mtu only applies to members of the bridge
		if bridge:
			if mtu:
				nsif.mtu = mtu

			bridge.addif(nsif)
			nsif = bridge
		elif mtu:
			nsif.mtu = mtu

		# the order is important, sometimes we want to set the mac before we dhcp
		if mac:
			nsif.macadd(mac)

		if ip_address:
			if isinstance(ip_address, str):
				ip_address = [ip_address]
			for ip in ip_address or []:
				if ip == 'dhcp':
					print('Obtain dhcp lease for %s on %s' % (nsif.ns, nsif.ifname))
					output = udhcpc_lease(nsif.ns, nsif.ifname)
					try:
						for line in output.decode().split('\n'):
							print(line)
					except AttributeError:
						print('Failed to obtain dhcp lease')
				else:
					nsif.addradd(ip)

		if nat:
			nsif.ns.nsexec('iptables -t nat -A POSTROUTING -o ' + nsif.ifname + ' -j MASQUERADE')

		# TODO deprecated use of per link servers
		server_options = {}
		for name, server_cfg in ((k, v) for k, v in options.items() if k in VirtServers.server_map):
			server_options[name] = deepcopy(server_cfg if isinstance(server_cfg, dict) else {})
			server_options[name]['ip_address'] = deepcopy(ip_address)
		if server_options:
			self.servers = VirtServers(nsif.ns)
			self.servers.start_servers(server_options)

		return self

	def __repr__(self):
		return '%s dest=%s' % (self.name, self._dest_ns)


class PhysLink(VirtLink):
	"""Generically manipulate the namespace of existing links. DANGER moving real interfaces like eth0 into a different
	has the risk of being lost into oblivion if the namespace is somehow lost before deinit is called. This requires a
	reboot to fix. Sorry."""

	def __init__(self, name):
		super().__init__(name)
		self._iface = None

	@property
	def iface(self):
		if not self._iface:
			self._iface = NsIface(self.name, NetNs('default', default=True))
		return self._iface

	def up(self):
		self.iface.up()

	def down(self):
		self.iface.down()

	def deinit(self):
		if self._iface:
			if self._iface.ns:
				self._iface.ns.remnsif(self._iface)
			self._iface.deinit()
			self._iface = None
