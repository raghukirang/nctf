"""
kvmnode.py - Simple virtual router abstraction

Copyright (c) 2010-2018 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import os

from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.kvmcontroller import KvmController
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtnamespace import InteractiveNamespace
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtrouter import TapLink


class KvmNode(InteractiveNamespace):
    postfix = 'n'

    stdio_socket_base = '/tmp/kvmnode/'

    def __init__(self, name, image, stdio_socket=None, mem=256, nets=2, **kwargs):
        super().__init__(name, **kwargs)

        if stdio_socket:
            self.stdio_socket = stdio_socket
        else:
            self.stdio_socket = self.stdio_socket_base + self.ns.name
            try:
                os.makedirs(self.stdio_socket_base)
            except FileExistsError:
                pass

        self.cmd = 'socat -,raw,echo=0,escape=0x1f {}'.format(self.stdio_socket)

        self.kvm = KvmController(
            name,
            ns=self.ns,
            drive=image,
            qemu_vers='x86_64',
            stdio_socket=self.stdio_socket,
            mem=mem,
            nets=nets,
            snapshot=True)

    def start(self):
        super().start()
        self.kvm.start()

    def stop(self):
        super().stop()
        self.kvm.stop()

    def _new_link(self, port, **options):
        return TapLink(self.name + str(port), self.kvm.tap_links[port])
