"""
realrouter.py - Router hardware device control

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""
# Standard
import time
import logging
from json import load
import json
import zlib
import os
import re
import shutil

logger = logging.getLogger('remoterouter')

# for console_config()
CONFIG_ERRORS = [
"Error: invalid value",
"Invalid command",
"IndexError"
]

def read_json_file(path):
	try:
		with open(path) as f:
			retval = load(f)
	except Exception as e:
		logger.debug('Got an Exception Reading JSON {}'.format(e))
		return {}
	return retval


class RemoteRouter(object):
	"""
	A remote hardware router, connected through gretap interface on the host system
	"""
	def __init__(self, name, remote_dest=None, interface_id=None, config=None, mac=None):
		self.name = name
		self.remote_dest = remote_dest or None
		self.interface_id = interface_id or None
		self.config = config or None
		self.mac = mac or None
		self.ip = None

	def start(self):
		pass

	def after_start(self):
		pass

	def down(self):
		pass

	def deinit(self):
		pass

	def cleanup(self,cl):
		cmd = "ip route | grep default"
		gatewaytxt = cl.ns.nsexec(cmd)
		ip = str(gatewaytxt).split("default via ")[1].split(" dev")[0]
		resp = self.curl_set_data(cl, "control/system/factory_reset", True, ip)
		if b"\"success\": true" in resp:
			time.sleep(30)
			cl.ns.nsexec('ip addr add 192.168.0.2/24 dev veth-v-client0')
			self.wait_for_router_online(cl, "192.168.0.1", logtxt='factory reset')
		else:
			logger.warning("Factory reset failed in Teardown!!!!!!!!!")
			return 0

	def clientside_config(self, cl, ahf, poll=False, ip="192.168.0.1", port="80"):
		# Assuming routers are in factory default state of 192.168.0.1 LAN side IP
		if not self.config:
			return

		confjson = ahf.get_asset_json(self.config)
		if not confjson:
			logger.warning("Error, improper format or we could not find config {}".format(self.config))
			return

		# CHECK TO SEE IF LAN IP CHANGES
		try:
			newip = confjson[0]['config']['lan']["0"]["ip_address"]
			if newip != "192.168.0.1":
				self.ip = newip
		except KeyError:
			pass

		# Ping for ARP
		cl.ns.nsexec("ping {} -c 1".format(ip))
		logger.debug("Uploading config using binpush")
		return self.clientside_bin(cl, confjson, ip, port, poll=poll)


	def clientside_bin(self, cl, conf, ip="192.168.0.1", port="80", poll=True):
		"""
		Load an encoded bin file on the router
		:param cl: client namespace init object
		:param conf: json conf string that will be encoded and compressed into bin
		:param ip: IP address
		:param port: Port
		:param poll: bool: if true, attempt to ping the router while it's unpacking the bin to determine when it comes back up
		:return:
		"""

		try:
			os.mkdir("tmp_bins", mode=0o775)
		except FileExistsError:
			pass

		with open('tmp_bins/test.bin', 'wb') as f:
			f.write(zlib.compress(json.dumps(conf).encode()))

		# Ping for ARP
		cl.ns.nsexec("ping {} -c 1".format(ip))

		cmd = 'curl --basic -u admin:{} http://{}:{}/config_save -F config_file=@tmp_bins/test.bin'.format(self.mac[4:],
														ip, port)
		resp = cl.ns.nsexec(cmd)
		logger.debug(resp)
		if resp:
			if not b"\"success\": true, \"data\": \"valid\"" in resp:
				logger.warning("PROBLEM WITH CONFIG BIN PUSH!!!!!!!!!")
				return 0
		else:
			logger.warning("clientside_bin(): router response: None")
			return 0

		if poll:
			logger.debug('Sleeping waiting for bin to load and router to reboot')
			time.sleep(30)

			# Handle the new IP address that was configured on the router
			if self.ip:
				ip = self.ip
				clip = self.ip
				foo = clip.split('.')
				foo[3] = str(int(foo[3]) + 1)

				clip = foo[0] + "." + foo[1] + "." + foo[2] + "." + foo[3]
				cl.ns.nsexec('ip addr add {}/24 dev veth-v-client0'.format(clip))
			self.wait_for_router_online(cl, ip, 'config load')
		try:
			shutil.rmtree("tmp_bins", ignore_errors=True)
		except Exception as e:
			logger.warning("problem removing tmp_bins")
			logger.warning(e)
			pass
		return 1

	def wait_for_router_online(self, cl, ip, logtxt=''):
		# ping poll the router until it comes back up
		ct = 0
		down = True
		while down:
			ct += 1
			time.sleep(5)
			resp = cl.ns.nsexec('ping {} -c 1'.format(ip))
			if ct > 45:
				logger.warning("Router never recovered from {}".format(logtxt))
				return 0
			if resp:
				logger.debug("Router is online now after {}".format(logtxt))
				down = False
		logger.debug("Wait till services are started so that the router could be used immediately in the test. "
					 "ECM service is one of the last ones to start")
		down = True
		ct = 0
		path = 'status/system/services/ecm/state'
		state = None
		while down:
			resp = self.curl_get_data(cl, path, ip)
			if resp:
				if b"\"success\": true" in resp:
					match_return = re.search('"data":\\s"([a-z]+)"', str(resp))
					if match_return:
						state = match_return.group(1)
				else:
					logger.warning("Failed to retrieve ecm service state!!!!!!!!!")

			logger.debug("ECM State is:{}".format(state))
			if state == 'started':
				down = False
			ct += 1
			time.sleep(1)
			if ct > 20:
				logger.warning("ECM service never got started")
				down = False

	def curl_get_data(self, cl, path=None, ip="192.168.0.1", port="80"):
		"""

		:param cl: client namespace init object
		:param path: path in config store
		:param ip: IP address
		:param port: port
		:return:
		"""
		cmd = "curl --basic -u admin:{} -X GET http://{}:{}/api/{}".format(self.mac[4:], ip, port, path)
		return cl.ns.nsexec(cmd)

	def curl_set_data(self, cl, path=None, conf=None, ip="192.168.0.1", port="80"):
		"""

		:param cl: client namespace init object
		:param path: path in config store
		:param conf: json to load onto router
		:param ip: IP address
		:param port: port
		:return:
		"""
		cmd = "curl --basic -u admin:{} http://{}:{}/api/{} -X PUT -d data='{}'".format(self.mac[4:], ip, port, path,
																						self.format_config(str(conf)))
		return cl.ns.nsexec(cmd)

	@staticmethod
	def format_config(confjson):
		confjson = confjson.replace("'", "\"")
		confjson = confjson.replace("False", "false")
		confjson = confjson.replace("True", "true")
		confjson = confjson.replace("None", "null")
		return confjson