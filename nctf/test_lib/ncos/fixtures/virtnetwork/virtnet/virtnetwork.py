# Standard
import os
import sys
import netifaces
import ipaddress

import inspect
import subprocess

#### JOURNEY IMPORTS ####
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.kvmnode import KvmNode
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtlink import PhysLink
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtrouter import VirtCoconut
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtnamespace import VirtCloud, VirtHost, VirtSwitch, RemoteBridge
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.realrouter import RealRouter
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.remoterouter import RemoteRouter

# ADDED BY TESTOPS
import logging

logger = logging.getLogger("virtnetwork")
#logging.basicConfig(level=logging.INFO, format='%(processName)s[%(process)d]%(threadName)s[%(thread)d] %(message)s')


class LocalBridge(VirtSwitch):
	"""This is a VirtSwitch that can have existing physical interfaces plugged into it. Alternatively, you can manage an
	existing bridge if name matches an existing bridge name and no interfaces are defined. There's a small risk of losing
	a physical interface to a namespace if somehow the namespace is destroyed before the links are deinitialized. The only
	way to fix this is to reboot the computer."""

	def __init__(self, name, interfaces=None, **options):
		self.interfaces = interfaces or []
		self.bridged_ifaces = {}
		super().__init__(name, create=self.interfaces, default=True, **options)

	def start(self):
		if netifaces.gateways().get('default'):
			try:
				gateway, gw_interface = netifaces.gateways().get('default')[netifaces.AF_INET]
			except KeyError:
				# no existing ipv4 addr
				gw_interface = None
		else:
			gw_interface = None
		logger.debug("Gateway: %s GW Interface: %s Ifname: %s"%(gateway, gw_interface, self.bridge.ifname))
		for interface in self.interfaces:
			try:
				interface_info = netifaces.ifaddresses(interface)[netifaces.AF_INET]
			except KeyError:
				# no exixting ipv4 addr
				interface_info = None
			phy_iface = PhysLink(interface)
			self.plug(phy_iface)
			phy_iface.iface.addrflush()
			if interface_info and interface_info[0].get('addr') and interface_info[0].get('netmask'):
				self.bridged_ifaces[interface] = ipaddress.ip_interface(interface_info[0]['addr']+'/'+interface_info[0]['netmask'])
		if self.bridged_ifaces:
			self.bridge.addradd(str(list(self.bridged_ifaces.values())[0]))
		super().start()
		if gw_interface in self.interfaces:
			cmd = "ip route add default via {} dev {}".format(gateway, self.bridge.ifname)
			try:
				return subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
			except Exception as e:
				logger.debug('{} failed with {}'.format(cmd, e))

	def stop(self):
		super().stop()
		for interface, ip_addr in self.bridged_ifaces.items():
			phy_iface = PhysLink(interface)
			phy_iface.iface.addradd(str(ip_addr))
			phy_iface.up()


def realrouter_options(virtnetwork, node, options):
	node['priority'] = node.get('priority') or 10
	return basedir_options(virtnetwork, options)

def remoterouter_options(virtnetwork, node, options):
	node['priority'] = node.get('priority') or 10
	return basedir_options(virtnetwork, options)

def coconut_options(virtnetwork, node, options):
	"""Manipulate virtnetwork options for VirtCoconut"""
	num_links = len(virtnetwork.networking['links'].get(node['name']) or [])
	if num_links > 5:
		options["nets"] = num_links
	node['priority'] = node.get('priority') or 10
	return basedir_options(virtnetwork, options)


def basedir_options(virtnetwork, options, key='config'):
	if virtnetwork.networking.get('defaults') and virtnetwork.networking['defaults'].get('basedir'):
		if options.get(key) and not os.path.exists(options[key]) and os.path.exists(
				virtnetwork.networking['defaults']['basedir'] + '/' + options[key]):
			options[key] = virtnetwork.networking['defaults']['basedir'] + '/' + options[key]
	return options


def kvm_options(virtnetwork, node, options):
	return basedir_options(virtnetwork, options, key='image')


class VirtNetwork(object):

	virt_type = {
		"router": {
			"init": VirtCoconut,
			"options": coconut_options
		},
		"remoterouter": {
			"init": RemoteRouter,
			"options": remoterouter_options
		},
		"realrouter": {
			"init": RealRouter,
			"options": realrouter_options
		},
		"localbridge": {
			"init": LocalBridge,
		},
		"remotebridge": {
			"init": RemoteBridge
		},
		"cloud": {
			"init": VirtCloud,
		},
		"switch": {
			"init": VirtSwitch,
		},
		"host": {
			"init": VirtHost,
		},
		"kvm": {
			"init": KvmNode,
			"options": kvm_options
		}
	}

	@property
	def sorted_nodes(self):
		"""retrun k, v tuple of the nodes sorted by the 'priority' key. 100 is the default priority if not specified"""
		return sorted(self.networking['nodes'].items(), key=lambda t: t[1].get('priority') or 100)

	@property
	def node_names(self):
		"""list of node names"""
		return [k for k, v in self.networking['nodes'].items()]

	def __init__(self, networking):
		self.networking = networking or {}
		# convenient self referential key (not really, it's just a dirty workaround)
		self.networking['virtnetwork_init'] = self

		# we can be defined as a list for convenience, but convert the list to dict
		if isinstance(networking['nodes'], list):
			new_dict = {}
			for value in networking['nodes']:
				new_dict[value['name']] = value
			networking['nodes'] = new_dict

		if isinstance(networking['links'], list):
			new_dict = {}
			for value in networking['links']:
				if not new_dict.get(value['source']['name']):
					new_dict[value['source']['name']] = []
				new_dict[value['source']['name']].append(value)
			networking['links'] = new_dict

		# init and start virt objects
		for k, v in self.networking['nodes'].items():
			if len(k) > 9:
				print(k, "VirtNamespace name cannot be longer than 9 chars (sorry)")
				continue
			if self.virt_type.get(v['type']):
				v['name'] = k  # convenience
				virt_type = self.virt_type[v['type']]
				v['init'] = virt_type['init'](name=k, **self.node_options(v, virt_type))
			else:
				print("Unrecognized type: ", v['type'])

		#keep track of port fwds
		self.port_fwds = []

	def node_options(self, node, virt_type):
		node_params = {p for cls in inspect.getmro(virt_type['init']) for p in inspect.signature(cls).parameters.keys()}
		options = {k: v for k, v in self.networking.get('defaults', {}).items() if k in node_params}
		options.update(node.get('options') or {})

		# Hook for adding special option handling
		options_func = virt_type.get('options')
		if options_func:
			options = options_func(self, node, options)

		if options:
			node['options'] = options
			return node['options']
		else:
			return {}

	def start(self):
		for k, v in self.sorted_nodes:
			logger.debug("Starting {}".format(v['init']))
			v['init'].start()
		# execute links
		for k, v in self.networking['links'].items():
			self.virt_links(k)
		# hooks for additional configuration after full network is up
		for k, v in self.sorted_nodes:
			v['init'].after_start()

	def deinit(self):

		print('Stopping interactive virtual router')
		for p in self.port_fwds:
			p.kill()
			try:
				p.wait(timeout=3)
			except subprocess.TimeoutExpired:
				print('timed out waiting for port_fwd', p.pid)
		del self.port_fwds[:]
		for k, v in self.networking['nodes'].items():
			if v.get('init'):
				v['init'].deinit()

	def virt_links(self, key):
		cfg = self.networking['nodes'][key]
		instance = cfg.get('init')
		if not instance:
			print(key, " is not initialized")
			return

		# handle links
		link_cfg = self.networking['links'].get(key) or []
		if not link_cfg:
			print("Can't find", key, "link - naked links not supported yet")
			return
		for link in link_cfg:
			if not link.get('destination'):
				print(key, "has no destination")
				return
			plug = self.networking['nodes'].get(link['destination']['name'])
			if not plug or not plug.get('init'):
				print(link['destination']['name'], " is not initialized")
				continue
			plug_init = plug['init']
			link_args = self.link_options(link.get('source') or {}, key)
			plug_args = self.link_options(link['destination'])
			plug_init.plug(instance.getlink(**link_args), **plug_args)

	def link_options(self, link, name=None):
		# merge toplevel options with actual calling args only looks for 'port' right now
		defaults = self.networking['nodes'][link.get('name') or name].get('link_options') or {}
		options = {}
		options.update(defaults)
		options.update(link.get('options') or {})
		if link.get('port') is not None and options.get('port') is None:
			options['port'] = link['port']
			link['options'] = options
		return options

	def get_nsobj(self, key):
		try:
			return self.networking['nodes'][key]['init']
		except KeyError:
			return None

	def get_node(self, key):
		"""just a get_nsobj alias"""
		return self.get_nsobj(key)

	def shell_cmd(self, key):
		try:
			return self.get_nsobj(key).shell_cmd
		except AttributeError:
			return None

	def run_interactive(self, node_or_list_of_nodes=None):
		nodes = [node_or_list_of_nodes] if isinstance(node_or_list_of_nodes, str) else node_or_list_of_nodes or self.node_names
		for n in nodes:
			try:
				self.get_nsobj(n).run_interactive()
			except AttributeError:
				continue

	def port_forward(self, local_port, client_node_name, ip_port=None):
		"""port forwards into namespaces, you can use this to connect to a router ui for example
		pepe.port_forward(9999, cl1, (192.168.0.1, 80)), then point your browser to localhost:9999
		uses ncat so make sure that's installed"""
		client_ns_name = self.get_node(client_node_name).ns.nsname
		ipa, port = ip_port or ('127.0.0.1', local_port)
		cmd = ['ncat', '-k', '-l', '-p', str(local_port), '-c', "ip netns exec %s ncat %s %s" % (client_ns_name, ipa, port)]
		self.port_fwds.append(subprocess.Popen(cmd))

