"""
virtrouter.py - Simple virtual router abstraction

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import json
import os
import sys
import time
from tempfile import NamedTemporaryFile

from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.kvmcontroller import KvmController
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.netns import *
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtlink import VirtLink
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.vrouterconsole import VRouterConsole
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtnamespace import InteractiveNamespace
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.csclient import config
from nctf.test_lib.ncos.fixtures.virtnetwork.preseed.utils import ps2img, psclean, psmerge

PATH = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))

import logging

logger = logging.getLogger(__name__)


NEW_FWIMAGE = "framework/assets/router_images/coconut.img"
class VirtRouter(KvmController):
	def __init__(self, name, vdisk_size, image=NEW_FWIMAGE, networking=True, etc_dir=None, preseed=None, preseed_lock=False, **kwargs):
		self.preseed = preseed
		self.preseed_lock = preseed_lock
		self.preseedimg = None
		if self.preseed:
			self.dopreseed(name, preseed, vdisk_size)
		super().__init__(name, image, drive=self.preseedimg, **kwargs)
		self.routerns = self.ns

		# TODO networking concept needs to be separate
		self.networking = networking

		for i, net in enumerate(self.virtnets):

			# Namespaces and tap<->veth and veth<->ns bridges
			net['router_bridge'] = Bridge('{}-r{}'.format(name, i), ns=self.routerns)
			if networking:
				net['router_veth'] = VirtEth('{}-r{}'.format(name, i))
				net['ns'] = NetNs('{}-{}'.format(name, i), etc_dir=etc_dir)
				net['net_bridge'] = Bridge('{}-n{}'.format(name, i))
				net['net_veth'] = VirtEth('{}-n{}'.format(name, i))

		# veths created for bridging
		self.brveths = []

	def start(self):
		super().start()

		for n in self.virtnets:
			# Bridge kvm tap<->router veth
			n['router_bridge'].addif(n['tap'])

			if self.networking:
				# Add a veth to router ns
				self.routerns.addif(n['router_veth'])
				# Add veth to bridge
				n['router_bridge'].addif(n['router_veth'].virt_if)
				# Add a veth to network ns
				n['ns'].addif(n['net_veth'])

				# Bridge router veth<->net veth
				n['net_bridge'].addif(n['router_veth'].host_if)
				n['net_bridge'].addif(n['net_veth'].host_if)

			# Bring it all up
			n['tap'].up()
			n['router_bridge'].up()
			if self.networking:
				n['router_veth'].up()
				n['net_veth'].up()
				n['net_bridge'].up()

	def bridge(self, idx, vr, vidx):
		v = VirtEth('{}-b{}'.format(self.name, len(self.brveths)))
		self.virtnets[idx]['net_bridge'].addif(v.host_if)
		vr.virtnets[vidx]['net_bridge'].addif(v.virt_if)
		v.up()
		self.brveths.append(v)

	def plug(self, nsif, port=0):
		"""Plug other veth devices into this router, specify which virtnet interface to plug it into"""
		self.virtnets[port]['router_plug'] = nsif
		if not nsif.ns:
			self.routerns.addnsif(nsif)
		self.virtnets[port]['router_bridge'].addif(nsif)
		nsif.up()

	def deinit(self):
		for i, n in enumerate(self.virtnets):
			n['router_bridge'].deinit()
			if self.networking:
				n['router_veth'].deinit()
				n['net_veth'].deinit()
				n['net_bridge'].deinit()
				n['ns'].deinit()

		for v in self.brveths:
			v.deinit()

		del self.brveths[:]

		if self.preseed:
			psclean(self.preseed, self.preseedimg)
			self.preseed = None

		super().deinit()

	def dopreseed(self, name, preseed, vdisk_size):
		# generate target image
		self.preseedimg = 'preseed-{}.img'.format(name)
		self.preseed = psmerge(preseed)
		ps2img(self.preseed, self.preseedimg, vdisk_size, pslock=self.preseed_lock)
		#logger.debug("KVM got past preseed stuff")
		return self.preseedimg


class VirtCoconut(InteractiveNamespace):
	"""Wrapper for VirtRouter to be used in virtual networking"""
	postfix = 'r'

	stdio_socket_base = '/tmp/virtcoconut/'

	@property
	def shell_cmd(self):
		print('Note: Hit enter to get the prompt! Ctrl + / leaves the shell!')
		return 'socat -,raw,echo=0,escape=0x1f {}'.format(self.stdio_socket)

	@property
	def console(self):
		self._console = self._console or VRouterConsole(self)
		return self._console

	def __init__(self, name, default=False, interactive=False, term=None, config=None, router=None, nets=5, mem=256,
				 image=NEW_FWIMAGE, preseed=None, mac=None, stdio_socket=None, vdisk_size=10, serial=None, interfaces=None, **kwargs):
		self.stdioproc = None
		self.mac = mac
		self._console = None
		super().__init__(name, default=True if router else default, interactive=interactive, term=term, **kwargs)
		#logger.debug("VirtCoconut received image: {}".format(image))
		#logger.debug("VirtCoconut received preseed: {}".format(preseed))
		#logger.debug("VirtCoconut received config: {}".format(config))
		#logger.debug("VirtCoconut received mac: {}".format(mac))
		#logger.debug(NEW_FWIMAGE)
		if router:
			self.name = router.name
			self.rtr = router
			self.ns = router.routerns
		else:
			#logger.debug("HIT VirtCoconut if not preseed case")
			if not isinstance(config, str):
				# Generate router preseed file on the fly
				# logger.debug("HIT VirtCoconut if not isinstance case")
				config, tmp_file = self.write_temp_config_file(config)
			elif config.endswith('.bin'):
				import zlib
				with open(config, 'rb') as f:
					j = json.loads(zlib.decompress(f.read()).decode())
				config, tmp_file = self.write_temp_config_file(j)
			failsafe_preseed = {
				"config": config,
				"license": "license.json",
				"code": "preseed",
				"dynamic": None,
				"folder": "ips_rules"
			}
			#logger.debug("Past preseed building")
			if mac:
				tmpboard = NamedTemporaryFile('w')
				tmpboard.file.write('{ "mac" : "%s" }\n' % mac)
				tmpboard.flush()
				failsafe_preseed['board'] = tmpboard.name

			if stdio_socket:
				self.stdio_socket = stdio_socket
			else:
				self.stdio_socket = self.stdio_socket_base + self.ns.name
				try:
					os.makedirs(self.stdio_socket_base)
				except FileExistsError:
					pass

			if preseed:
				failsafe_preseed.update(preseed)

			preseed = failsafe_preseed

			self.rtr = VirtRouter(self.name, networking=False, virtsport=7777, preseed=preseed, display='nographic', mem=mem,
			                      ns=self.ns, nets=nets, image=image, qmpsock='./qmpsock_{}'.format(self.name), stdio_socket=self.stdio_socket, vdisk_size=vdisk_size, **kwargs)

	def stop(self):
		super().stop()
		if self.rtr:
			self.rtr.stop()

	def start(self):
		self.rtr.start()

	def run_interactive(self):
		# Wait upto 3 seconds for the socket to be created
		for i in range(15):
			if os.path.exists(self.stdio_socket):
				break
			time.sleep(0.2)
		else:
			#  TODO add logging
			return
		self.cmd = 'socat -,raw,echo=0,escape=0x1f {}'. format(self.stdio_socket)
		super().run_interactive()

	def plug(self, link, port=0, **link_options):
		super().plug(link, port)  # No link options supported when plugging into a router
		nsif = link.iface
		self.rtr.plug(nsif, port)

	def deinit(self):
		super().stop()
		self.teardown()
		if self.rtr:
			self.rtr.stop()
			self.rtr.deinit()  # don't call super deinit because rtr.deinit will handle ns deletion
			self.rtr = None

	def config(self, method, path, **kwargs):
		return config(self.ns, method, path, **kwargs)

	def _new_link(self, port, **options):
		return QmpLink(self.name + str(port), self.rtr.tap_links[port], self.rtr.virtio_links[port])

	def write_temp_config_file(self, config):
		""" write a temp config file return the filename"""
		f = NamedTemporaryFile(mode='w', dir='{}/preseed/config'.format(PATH))
		json.dump(config, f)
		f.flush()
		head, tail = os.path.split(f.name)
		return tail or os.path.basename(head), f

class TapLink(VirtLink):

	def __init__(self, name, tap_iface):
		super().__init__(name)
		self.tap_iface = tap_iface

	@property
	def iface(self):
		return self.tap_iface

	def up(self):
		self.tap_iface.up()

	def down(self):
		self.tap_iface.down()

	def __del__(self):
		super().__del__()

	def deinit(self):
		self.tap_iface = None
		super().deinit()


class QmpLink(TapLink):
	""" Hook QMP so links can be brought up and down. """

	def __init__(self, name, tap_iface, virtio):
		super().__init__(name, tap_iface)
		self.virtio = virtio

	def up(self):
		super().up()
		self.virtio.linkup()

	def down(self):
		super().down()
		self.virtio.linkdown()

	def deinit(self):
		self.virtio = None
		super().deinit()

