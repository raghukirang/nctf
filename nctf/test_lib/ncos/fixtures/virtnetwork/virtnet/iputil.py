"""
iputil.py -  'ip' utility interaction helper

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import subprocess
import logging

logger = logging.getLogger('virtnet/iputil')


class IpUtil(object):

	iputil = 'ip'

	@classmethod
	def ipexec(cls, cmd):
		try:
			return subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
		except Exception as e:
			logger.debug('{} failed with {}'.format(cmd, e))
			#print('{} failed with {}'.format(cmd, e))

	@classmethod
	def netnsexec(cls, cmd):
		return cls.ipexec(cls.netnscmd(cmd))

	@classmethod
	def netnscmd(cls, cmd):
		return '{} netns {}'.format(cls.iputil, cmd)

	@classmethod
	def linkexec(cls, cmd):
		cls.ipexec(cls.linkcmd(cmd))

	@classmethod
	def linkcmd(cls, cmd):
		return '{} link {}'.format(cls.iputil, cmd)

	@classmethod
	def addrexec(cls, cmd):
		cls.ipexec(cls.addrcmd(cmd))

	@classmethod
	def addrcmd(cls, cmd):
		return '{} addr {}'.format(cls.iputil, cmd)
