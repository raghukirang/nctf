"""Collection of useful test methods"""

import functools
import time

import subprocess
from select import select

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.utils import ping
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.virtexec import virtexec
from nctf.test_lib.utils.sshclient.sshclient_paramiko import *

from paramiko import SSHClient, AutoAddPolicy
from scp import SCPClient, SCPException

def send_file(**kwargs):
	if not all(arg in kwargs for arg in ('target_ip', 'username', 'password', 'file_path', 'url')):
		for k,v in kwargs.items():
			print("K: %s V: %s" % (k,v))
		raise Exception('Missing required args')
	ssh_client = SSHClient()
	ssh_client.set_missing_host_key_policy(AutoAddPolicy())
	ssh_client.connect(
		hostname=kwargs.get('target_ip'),
		username=kwargs.get('username'),
		password=kwargs.get('password')
	)
	with SCPClient(ssh_client.get_transport()) as scp_conn:
		try:
			# put() calls _recv_confirm when it's done sending files. Waits to receive up to 512 bytes
			# from the channel. Unless we change the sessionHandler in sshserver.py (not worth it),
			# it will get nothing after and time out after 5 seconds.
			scp_conn.put(kwargs.get('file_path'), remote_path=kwargs.get('url'))
		except SCPException:
			pass
	ssh_client.close()



def stream_exec(cmd, timeout=60, callback=None, binary=False, chunk_size=1024):
	"""uses subprocess.Popen and streams stdout and stderr into callback.
	Timeout is the total time the call is allowed to run"""
	def handle_binary(stdout):
		chunk = stdout.read(chunk_size)
		if chunk:
			callback(chunk)

	def handle_text(stdout):
		line = stdout.readline().rstrip()
		if line:
			callback(line.decode())

	if not callback:
		callback = lambda x: print(x)
	start_time = time.time()
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=0)
	while proc.poll() is None:
		time_left = timeout - (time.time() - start_time)
		if time_left <= 0 or not select([proc.stdout], [], [], time_left)[0]:
			if not binary:
				callback('stream_exec: timed out')
			proc.terminate()
			break

		if binary:
			handle_binary(proc.stdout)
		else:
			handle_text(proc.stdout)

	proc.wait(timeout=3)
	return proc.returncode


def wait_for_ip(vhost, version=4, timeout=10):
	""" returns an ip or assert fail """
	ipf = functools.partial(vhost.__class__.__dict__['ip4_address' if version == 4 else 'ip6_address'].__get__, vhost)
	rval = ipf()
	while not rval and timeout > 0:
		time.sleep(1)
		timeout -= 1
		rval = ipf()
	assert rval
	return rval


def ping_test(src_vhost, dst_address, version=4, timeout=10, expected=True, ttl=63, quiet=False):
	""" Assert if we have successful ping connectivity within timeout. Also waits for ip address (useful if using dhcp)"""

	src_address = wait_for_ip(src_vhost, version=version, timeout=timeout)
	assert src_address, "could not acquire ip from %s" % src_vhost

	rval = (0 == virtexec(src_vhost.ns, ping, 3, src_address, dst_address,
			timeout=timeout + 15, kwargs={'timeout': timeout + 10, 'deadline': max(1, timeout),
			'ttl': ttl, 'quiet': quiet, 'ver': 'ipv4' if version == 4 else 'ipv6'}))
	assert expected == rval, "ping failed. rval: %s is not expected: %s" % (rval, expected)


def communication_test(src_vhost, dst_vhost, timeout=10, count=1, version=4):
	"""dst_vhost: virtual namespace of destination"""
	dst_ip = wait_for_ip(dst_vhost, version=version, timeout=timeout)
	assert dst_ip
	my_ip = (wait_for_ip(src_vhost, version=version, timeout=timeout))
	assert my_ip
	while timeout > 0:
		start = time.time()
		if virtexec(src_vhost.ns, ping, count, my_ip, dst_ip, interval=1, quiet=True) == 0:
			return
		dur = time.time() - start
		if dur < 1:
			time.sleep(1 - dur)
		timeout -= max(1, dur)
	else:
		assert False
