"""
virtobject.py - Simple virtual network abstractions using namespaces

Copyright (c) 2010-2016 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""
import logging
import os
import subprocess
import psutil
import shlex
import ipaddress
from abc import abstractmethod
from os.path import basename
from shutil import copyfile

from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.netns import NetNs, Bridge, VirtEth, NsIface, GreTap, Vxlan
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtlink import VirtLink
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtservers import VirtServers
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.procmon import ProcessMonitor
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.utils import local_ip
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.virtexec import virtexec

logger = logging.getLogger('virtnetwork.virtnet.virtnamespace')


class VethLink(VirtLink):
	def __init__(self, name):
		super().__init__(name)
		self._veth = None
		self._peer = None

	def __del__(self):
		if self._veth:
			self.deinit()

	def up(self):
		self._veth.up()

	def down(self):
		self._veth.down()

	def unplug(self):
		# Only the peer end a a veth can be 'unplugged'
		self._peer.deinit()
		self.deinit()

	def deinit(self):
		if self._veth:
			self._veth.deinit()
			self._veth = None
		self._peer = None
		super().deinit()

	@property
	def peer(self):
		"""Returns a VethLink peer object"""
		if not self._peer:
			self._peer = VethPeer(self)
		return self._peer

	@property
	def iface(self):
		if not self._veth:
			self._veth = VirtEth(self.name)
		return self._veth.virt_if

	def __repr__(self):
		return '%s veth=%s' % (super().__repr__(), self._veth)


class VethPeer(VirtLink):
	def __init__(self, peer):
		super().__init__(peer.name)
		self._veth = peer._veth
		self._peer = peer

	def up(self):
		self._veth.up()

	def down(self):
		self._veth.down()

	def deinit(self):
		# The underlying veth is removed by the other end
		self._veth = None
		self._peer = None
		super().deinit()

	@property
	def iface(self):
		if self._veth:
			return self._veth.host_if

	def __repr__(self):
		return '%s(peer) veth=%s' % (super().__repr__(), self._veth)


class FakeVirtNamespaceBase(object):  # http://bugs.python.org/issue672115
	pass


class VirtNamespaceBase(FakeVirtNamespaceBase):
	""" VirtNamespace is just a namespace with the concept that you can create links conveniently with
	getlink() and then plug them into other namespaces with a call to plug(). Extend to create convenient network objects.
	The base isn't intend to be called it's an abstract base class. You can read more about namespaces and veth devices
	here: http://blog.scottlowe.org/2013/09/04/introducing-linux-network-namespaces/
	"""
	postfix = 'obj'

	@property
	def next_link_name(self):
		return self.name + str(len(self.links))

	@property
	def next_port_number(self):
		if self._port_ctr is None:
			self._port_ctr = 0
		else:
			self._port_ctr += 1
		return self._port_ctr

	def __init__(self, name, default=False, route=None, dns=None, servers=None, **options):
		self.default_link_options = {}  # override in child to assign default options to links. Key is port number e.g:
		# self.default_link_options = {0: {"ip_address": "15.0.0.2/30"}, 1: {"ip_address": "25.0.0.2/30"}

		if route and isinstance(route, dict):
				route = [route]

		self.routes = route
		self.name = name
		self.links = {}  # mapped by port number
		self._port_ctr = None  # support arbitrary amount of links
		self.ns = NetNs('{}-{}'.format(self.name, self.postfix), default=default, etc_dir=True)

		if dns:
			dns = [dns] if not isinstance(dns, list) else dns
			data = "".join("nameserver %s\n" % d for d in dns)
			self.write_etc(filename='resolv.conf', data=data)

		self.servers_cfg = servers
		self.servers = None

	def link_ifname(self, port):
		try:
			return self.links[port].iface.ifname
		except (KeyError, AttributeError):
			pass

	@abstractmethod
	def plug(self, link, port=None, **link_options):
		"""
		Takes adapter from one VirtNamespace and attaches it to this one. For example, call .getlink from one VirtualObject
		and pass it as a link to 'plug' it into this namespace.

		link_options are whatever needs to 'happen' to MY end of the link when plug is called.
		"""
		link.plug(self)

		if port is not None and self.links.get(port):
			self.links[port].unplug()
		npn = port if port is not None else self.next_port_number
		self._on_link_action(link, npn, **link_options)
		self.links[npn] = link

	@abstractmethod
	def getlink(self, port=None, **link_options):
		"""
		Pulls a link from this namespace to be plugged into another. One link end is connected to this namespace
		(link), the other floats in the default namespace until it is "plugged" into another VirtNamespace (peer). port can
		be passed to retrieve a link that was previously created. link_options is passed to _link_options and represents
		actions need to happen to MY end of the link.
		"""
		if port is not None:
			try:
				return self.links[port]
			except KeyError:
				pass  # this means we actually create a port with specified port number

		npn = port if port is not None else self.next_port_number
		link = self._new_link(npn, **link_options)
		self.links[npn] = link
		return link.peer

	@abstractmethod
	def start(self):
		""" Run this object. Useful mostly for child classes to perform actions separate from __init__"""
		for link in (l for l in self.links.values() if l.plugged_namespace == self):  # only start links plugged into me
			link.up()

	@abstractmethod
	def after_start(self):
		""" A hook for adding functionality arbitrarily, generally after all nodes in a network are up"""
		for link in (l for l in self.links.values() if l.plugged_namespace == self):  # only apply to links plugged into me
			link.apply_options()
		if self.routes:
			for route in self.routes or []:
				self.add_route(**route)
		if self.servers_cfg:
			self.servers = VirtServers(self.ns)
			self.servers.start_servers(self.servers_cfg)


	@abstractmethod
	def stop(self):
		""" Stop this object but don't clean it up, by default just stops the link ends plugged into it"""
		for link in (l for l in self.links.values() if l.plugged_namespace == self):  # only stop links plugged into me
			link.down()

	@abstractmethod
	def deinit(self):
		""" Destroys this object"""
		self.stop()
		self.teardown()

		if self.ns:
			#attempt to kill all processes in a namespace
			try:
				pids = subprocess.check_output(['ip', 'netns', 'pid', self.ns.nsname]).decode().split()
			except:
				pids = []
			for pid in pids:
				try:
					p = psutil.Process(int(pid))
					p.terminate()
				except Exception as e:
					pass
			self.ns.deinit()
			self.ns = None

	@abstractmethod
	def teardown(self):
		""" Called on deinit, provides a place for child object to cleanup before the ns is destroyed """
		if self.servers:
			self.servers.stop_servers()
		if self.ns:
			for link in self.links.values():
				if link.plugged_namespace == self:
					link.unplug()  # orphan plugged devices
				else:
					link.deinit()  # destroy links created by me
		self.links.clear()

		self._port_ctr = None

	@abstractmethod
	def _new_link(self, port, **options):
		""" Hook for generating a new link. Defaults to Veth but can be overridden"""
		link = VethLink(self.name + str(port))

		# We 'plug' the local end of the veth into this namespace so it can receive link options
		link.plug(self)

		self._on_link_action(link, port, **options)

		return link

	@abstractmethod
	def _on_link_action(self, link, port, **link_options):
		""" Hook for link options to be applied on plug or getlink"""
		lo = self.default_link_options.get(port) or {}
		lo.update(link_options)
		link.link_options(**lo)
		link.up()

	def add_route(self, dest, gateway=None, dev=None, metric=None):
		""" Create a route to dest via gateway """
		gw = " via {}".format(gateway) if gateway else ""
		d = " dev {}".format(dev) if dev else ""
		m = " metric {}".format(metric) if metric else ""
		self.ns.nsexec("ip route add {}{}{}{}".format(dest, gw, d, m))

	def write_etc(self, filename=None, source=None, data=None):
		"""if source is specified then it will be copied into dir otherwise 'data' is written to the file"""
		if self.ns.etc_dir:
			if source:
				filename = filename or basename(source)
				copyfile(source, '{}/{}'.format(self.ns.etc_dir, filename))
			else:
				with open('{}/{}'.format(self.ns.etc_dir, filename), 'w') as f:
					f.write(data)

	def __del__(self):
		self.deinit()

	def __str__(self):
		return "%s (%s) " % (self.name, type(self).__name__)


class InteractiveNamespace(VirtNamespaceBase):
	# some preconfigured terms
	term_map = {
		'gnome-terminal': ["dbus-launch", "gnome-terminal"],
		'roxterm': ["dbus-launch", "roxterm", "--tab", "-T", "'%h'"],
		'rxvt': ["rxvt", "-T", "%h"],
		'urxvt': ["urxvt", "-T", "%h"],
		'xterm': ["xterm", "-T", "%h"]
	}

	@property
	def shell_cmd(self):
		return self.ns.nsexeccmd(self.shell)

	def __init__(self, name, interactive=False, term=None, **kwargs):
		super().__init__(name, **kwargs)
		self.interactive = interactive
		self.con = None
		self.shell = os.environ.get('SHELL') or 'sh'
		self.term = self.term_format(
			self.term_map.get(os.environ.get('CPTERM')) or
			shlex.split(os.environ.get('CPTERM') or '') or
			shlex.split(term or '') or
			["dbus-launch", "gnome-terminal"]
		)
		self.cmd = ''  # can be overridden in child

	def run_interactive(self):
		cmd = self.term + ['-e', self.shell]
		if self.cmd:
			cmd.extend([
				'-c' if 'gnome-terminal' not in self.term else '-e',  # gnome-terminal is weird,
				self.cmd
			])
		self.con = ProcessMonitor(cmd=cmd, autoRestart=False, ns=self.ns)
		self.con.start()

	def term_format(self, cmd):
		""" Replace placeholders with known values:
		%h = name of this host
		(more to come)
		"""
		replace_map = {
			'%h': self.name
		}

		def replacer(v):
			for r in replace_map.keys():
				v = v.replace(r, replace_map[r])
			return v

		return [replacer(str(v)) for v in cmd]

	def after_start(self):
		super().after_start()
		if self.interactive:
			self.run_interactive()

	def stop(self):
		if self.con:
			self.con.stop()
			self.con = None
		super().stop()


class VirtCloud(VirtNamespaceBase):
	""" Creates a simple virtual cloud device for convenience in testing.  This works by creating a namespace
	and a link configuration for each 'internet' port based on the config passed in. Any namespace then adopting the peer
	link device will be connected to the cloud.  However, the responsibility of configuring connected devices, for example
	ip addresses and routes lies on the connected device itself. More services (namely dhcp) to come
	"""

	postfix = 'cld'

	def __init__(self, name, **options):
		super().__init__(name, **options)
		self.default_link_options = {
			0: {"ip_address": ["15.0.0.2/30", "2001::2/64"]},
			1: {"ip_address": ["25.0.0.2/30", "2002::2/64"]},
			2: {"ip_address": ["35.0.0.2/30", "2003::2/64"]},
			3: {"ip_address": ["45.0.0.2/30", "2004::2/64"]},
			4: {"ip_address": ["55.0.0.2/30", "2005::2/64"]},
			5: {"ip_address": ["65.0.0.2/30", "2006::2/64"]},
			6: {"ip_address": ["75.0.0.2/30", "2007::2/64"]},
			7: {"ip_address": ["85.0.0.2/30", "2008::2/64"]},
			8: {"ip_address": ["95.0.0.2/30", "2009::2/64"]},
		}
		# We are a cloud, so we need ip_forwarding
		self.ns.nsexec('sysctl -w net.ipv4.conf.all.forwarding=1')
		self.ns.nsexec('sysctl -w net.ipv6.conf.all.forwarding=1')
		self.ns.nsexec('sysctl -w net.ipv6.conf.default.accept_dad=0')  # no duplicate address detection


class VirtSwitch(VirtNamespaceBase):
	""" Create's a simple virtual switch device for convenience in testing.  This works by creating a switch namespace
	and any number of link devices on that switch which are bridged together.  Any namespace then adopting the peer
	link device will be connected to the switch.
	"""

	postfix = 'sw'

	def __init__(self, name, create=True, default=False):
		super().__init__(name, default=default)
		self.bridge = Bridge(name, self.ns, create=create)

	def _on_link_action(self, link, port, **link_options):
		super()._on_link_action(link, port, bridge=self.bridge, **link_options)

	def start(self):
		self.bridge.up()
		super().start()

	def stop(self):
		if self.bridge:
			self.bridge.down()
		super().stop()

	def teardown(self):
		super().teardown()
		if self.bridge:
			self.bridge.deinit()
			self.bridge = None


class VirtHost(InteractiveNamespace):
	""" Virtual node. Just a namespace that you can connect to other things. Can start an interactive terminal
	useful for interactive testing
	"""
	postfix = 'hst'

	def __init__(self, name, **options):
		super().__init__(name, **options)
		loopback = NsIface('lo', ns=self.ns)
		loopback_ip_address = options.get('loopback_ip_address')
		if loopback_ip_address is not None:
			loopback.addradd(loopback_ip_address)
		loopback.up()  # Bring up loopback iface (it's down by default)
		self.ns.nsexec('sysctl -w net.ipv6.conf.default.accept_dad=0')  # no duplicate address detection

	@property
	def ip_addresses(self):
		return virtexec(self.ns, local_ip)

	@property
	def ip_address(self):
		return self.ip4_address

	@property
	def ip4_address(self):
		return next((ip4 for ip4 in virtexec(self.ns, local_ip) if ipaddress.ip_address(ip4).version == 4), None)

	@property
	def ip6_address(self):
		return next((ip4 for ip4 in virtexec(self.ns, local_ip) if ipaddress.ip_address(ip4).version == 6), None)



class RemoteBridge(VirtSwitch):
	postfix = 'l2'

	def __init__(self, name, remote=None, key=None, encap='vxlan', **options):
		if not remote or not key:
			raise ValueError('remote and key needs to be specified')
		super().__init__(name, default=True)
		tech = GreTap if encap == 'gretap' else Vxlan
		self.l2b = tech(name, remote, key)
		self.bridge.addif(self.l2b)

	def start(self):
		if self.l2b:
			super().start()
		self.l2b.up()

	def stop(self):
		if self.l2b:
			self.l2b.down()
		super().stop()

	def teardown(self):
		if self.l2b:
			self.l2b.deinit()
			self.l2b = None
		super().teardown()
