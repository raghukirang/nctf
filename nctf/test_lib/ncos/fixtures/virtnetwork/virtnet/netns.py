"""
netns.py - Namespace and virtual device abstraction

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import os
import subprocess
from shutil import rmtree
from .iputil import IpUtil as ip
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.virtexec import virtexec
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.vmcontrol import Qmp


class NsIface(object):

	@property
	def mtu(self):
		return self._mtu

	@mtu.setter
	def mtu(self, value):
		self._mtu = value
		self.ifexec("set {} mtu {}".format(self.ifname, self._mtu))

	def __init__(self, ifname, ns=None):
		self.ifname = ifname
		self.ns = ns
		self._mtu = None

	def ifexec(self, cmd):
		if self.ns and not self.ns.nsname == '1':
			self.ns.nsexec(ip.linkcmd(cmd))
		else:
			ip.linkexec(cmd)

	def addrexec(self, cmd):
		if self.ns and not self.ns.nsname == '1':
			self.ns.nsexec(ip.addrcmd(cmd))
		else:
			ip.addrexec(cmd)

	def up(self):
		self.ifexec('set {} up'.format(self.ifname))

	def down(self):
		self.ifexec('set {} down'.format(self.ifname))

	def addradd(self, ipaddr):
		self.addrexec('add {} dev {}'.format(ipaddr, self.ifname))

	def addrdel(self, ipaddr):
		self.addrexec('del {} dev {}'.format(ipaddr, self.ifname))

	def addrflush(self):
		self.addrexec('flush dev {}'.format(self.ifname))

	def macadd(self, mac):
		self.ifexec('set {} address {}'.format(self.ifname, mac))

	def deinit(self):
		self.down()
		self.ns = None

	def exists(self):
		cmd = ip.linkcmd('show {}'.format(self.ifname))
		if self.ns:
			cmd = self.ns.nsexeccmd(cmd)
		try:
			subprocess.check_output(cmd, shell=True, stderr=subprocess.DEVNULL)
			return True
		except subprocess.CalledProcessError:
			return False

	def __str__(self):
		return "[%s ns=%s]" % (self.ifname, self.ns)

class NetNs(object):

	def __init__(self, name, etc_dir=False, default=False):
		self.name = name
		self.nsname = '1' if default else 'ns-{}'.format(name)
		self.etc_dir = None
		if not default:
			self.etc_dir = self.create_etc_dir() if etc_dir else None
			ip.netnsexec('add {}'.format(self.nsname))

	def addif(self, veth):
		nsif = veth.virt_if if not veth.virt_if.ns else veth.host_if
		return self.addnsif(nsif)

	def addnsif(self, nsif):
		cmd = ip.linkcmd('set {} netns {}'.format(nsif.ifname, self.nsname))
		NetNs.nsipexec(cmd, nsif.ns)
		nsif.ns = self
		return nsif

	def remif(self, veth):
		nsif = veth.host_if if veth.host_if.ns else veth.virt_if
		self.remnsif(nsif)

	def remnsif(self, nsif):
		if nsif.ns and not nsif.ns.nsname == '1':
			cmd = ip.linkcmd('set {} netns 1'.format(nsif.ifname))
			self.nsexec(cmd)
			nsif.ns = None

	def nsexec(self, cmd):
		return ip.netnsexec('exec {} {}'.format(self.nsname, cmd))

	def nsexeccmd(self, cmd):
		return ip.netnscmd('exec {} {}'.format(self.nsname, cmd))

	def deinit(self):
		if self.nsname != '1':
			rmtree(self.etc_dir, ignore_errors=True)
			self.etc_dir = None
			ip.netnsexec('delete {}'.format(self.nsname))

	@classmethod
	def nsipexec(cls, cmd, ns=None):
		fn = ns.nsexec if ns and not ns.nsname == '1' else ip.ipexec
		fn(cmd)

	def __str__(self):
		return 'default' if self.nsname == '1' else self.nsname

	def create_etc_dir(self):
		ns_etc_dir = '/etc/netns/%s' % self.nsname
		try:
			if os.listdir(ns_etc_dir):
				raise FileExistsError('Can\'t create namespace etc dir {}, it already exists and it\'s not empty!'.format(ns_etc_dir))
		except FileNotFoundError:
			os.makedirs(ns_etc_dir, exist_ok=True)
		return ns_etc_dir

class VirtEth(object):

	@property
	def if_pair(self):
		return self.virt_if, self.host_if

	def __init__(self, name):
		self.name = name
		self.host_if = NsIface('veth-h-{}'.format(name))
		self.virt_if = NsIface('veth-v-{}'.format(name))
		ip.linkexec('add {} type veth peer name {}'.format(
			self.host_if.ifname, self.virt_if.ifname))

	def up(self):
		self.host_if.up()
		self.virt_if.up()

	def down(self):
		self.host_if.down()
		self.virt_if.down()

	def deinit(self):
		self.down()
		cmd = ip.linkcmd('delete {}'.format(self.host_if.ifname))
		NetNs.nsipexec(cmd, self.host_if.ns)
		self.host_if.ns = None
		self.virt_if.ns = None

	def __str__(self):
		return '%s, %s' % (self.host_if, self.virt_if)

class Bridge(NsIface):

	def __init__(self, name, ns=None, create=True):
		self.name = name
		self.create = create
		iface = 'vbr-{}'.format(self.name) if create else self.name
		NsIface.__init__(self, iface, ns)
		if self.create:
			self.ifexec('add {} type bridge'.format(self.ifname))

	def addif(self, iface):
		self.ifexec('set {} master {}'.format(iface.ifname, self.ifname))

	def down(self):
		if self.create:
			NsIface.down(self)

	def deinit(self):
		if self.create:
			NsIface.down(self)
			self.ifexec('delete {}'.format(self.ifname))

class Dummy(NsIface):

	def __init__(self, name, ns=None):
		self.name = name
		NsIface.__init__(self, 'dummy-{}'.format(self.name), ns)
		self.ifexec('add {} type dummy'.format(self.ifname))

	def deinit(self):
		NsIface.down(self)
		self.ifexec('delete {}'.format(self.ifname))


class KvmTap(NsIface):

	def __init__(self, name, ns=None):
		self.name = name
		NsIface.__init__(self, 'tap-{}'.format(self.name), ns)
		# no creation, KVM will add it

class GreTap(NsIface):

	def __init__(self, name, remote, key, ns=None):
		self.name = name
		self.remote = remote
		self.key = key
		super().__init__('gre-{}'.format(self.name), ns)
		self.ifexec('add {} type gretap remote {} key {}'.format(self.ifname, self.remote, self.key))

	def deinit(self):
		self.down()
		self.ifexec('del {}'.format(self.ifname))
		self.ns = None

class Vxlan(NsIface):

	def __init__(self, name, remote, key, ns=None):
		self.name = name
		self.remote = remote
		self.key = key
		super().__init__('vx-{}'.format(self.name), ns)
		self.ifexec('add {} type vxlan remote {} id {} dstport 4789'.format(self.ifname, self.remote, self.key, self.key))

	def deinit(self):
		self.down()
		self.ifexec('del {}'.format(self.ifname))
		self.ns = None

class VirtIo(object):

	def __init__(self, name, qmpsock, ns=None):
		self.name = 'virtio-net-pci{}'.format(name) # Todo: generate 'virtio-net-pci'?
		self.qmpsock = qmpsock
		self.ns = ns
		# no creation, KVM will add it
		
	def deinit(self):
		pass
		# no deletion, KVM will remove it

	def linkup(self):
		return virtexec(self.ns, lambda: Qmp(self.qmpsock).set_link(self.name, True))

	def linkdown(self):
		return virtexec(self.ns, lambda: Qmp(self.qmpsock).set_link(self.name, False))
