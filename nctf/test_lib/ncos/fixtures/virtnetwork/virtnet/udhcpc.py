"""
udhcpc.py - executes udhcpc.py in a namespace

Copyright (c) 2016 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import os
import stat
from tempfile import NamedTemporaryFile

DHCP_PID_FILE = '/var/run/udhcpc-{}.pid'  # format me
DHCP_EXEC_CMD = 'udhcpc -fbq -t 15 -A 1 -i {} -p {} -s {}'

DHCP_SCRIPT = """#!/bin/sh
[ -z "$1" ] && echo "Error: should be called from udhcpc" && exit 1

IP=/sbin/ip
RESOLV={}

[ -n "$subnet" ] && NETMASK="$subnet"

case "$1" in
	deconfig)
		echo "--deconfig"
		;;

	renew|bound)
		echo --$IP addr add $ip/$NETMASK dev $interface
		$IP addr add $ip/$NETMASK dev $interface

		if [ -n "$router" ] ; then
			for i in $router ; do
				echo --$IP route add default via $i dev $interface
				$IP route add default via $i dev $interface
			done
		fi

		[ -n "$domain" ] && echo --search $domain
		for i in $dns ; do
			echo --dns $i
			echo --resolve $RESOLV
			if [ -n "$RESOLV" ] ; then
				echo nameserver $i >> $RESOLV
			fi
		done
		;;
esac

exit 0
"""


def udhcpc_lease(ns, iface):
	script = NamedTemporaryFile(mode='w', delete=False)
	os.chmod(script.name, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
	script.write(DHCP_SCRIPT.format(ns.etc_dir + '/resolv.conf\n' if ns.etc_dir else '\n'))
	script.flush()
	script.close()
	pidfile = DHCP_PID_FILE.format(iface)
	cmd = DHCP_EXEC_CMD.format(iface, pidfile, script.name)
	rval = ns.nsexec(cmd)
	os.unlink(script.name)
	return rval
