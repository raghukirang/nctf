import json
import logging
import os
import pytest
import random
import shutil
import subprocess
import threading
import time
import copy

from nctf.test_lib.ncos.fixtures.virtnetwork.lib import footilities as utils
from nctf.test_lib.ncos.fixtures.virtnetwork.lib import syslogd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtnetwork import VirtNetwork
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.vrouterconsole import VRouterConsole
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.csclient import config
from nctf.test_lib.utils.sshclient import SSHShellClient as ssh
from nctf.test_lib.base.fixtures.fixture import FixtureBase

logger = logging.getLogger('virtnetwork.fixture')


IGNORE_PORTS = ["*", "x", "X", "-"]

def json_file_to_dict(path):
    net = {}
    if os.path.exists(path):
        with open(path) as file:
            net = json.load(file)
    return net


class VirtualNetwork(FixtureBase):

    def __init__(self, config_fixture, request, hwmap_str):
        logger.debug("init VirtualNetwork class")
        self.configfixture = config_fixture

        self.gpath = None
        self.network = None
        self.network_type = ""
        self.routers = []
        self.network_config = ""
        self.virtnet_instance = None
        self.interactive = False
        self.use_guido = False
        self.saved_image = ""
        self.asset_handler = None
        self.request = request
        self.hwmap = None
        self.hwmap_str = hwmap_str or None
        self.phy_routers = False
        self.remote_routers = False
        self.artifacts_dir = pytest.config.getoption("--artifacts") if hasattr(pytest, 'config') and pytest.config.getoption("--artifacts") else ""

        if self.artifacts_dir:
            try:
                original_umask = os.umask(0)
                os.makedirs(name=self.artifacts_dir+"/coverage", mode=0o775, exist_ok=True)
            finally:
                os.umask(original_umask)


        try:
            self.gpath = config_fixture.run.fixtures.virtNetwork.guidoPath
        except AttributeError as e:
            logger.warning("guido_path error {}".format(e))
        logger.debug("GUIDO_PATH: {}".format(self.gpath))

        # Setup syslogd
        if pytest.config.getoption("--syslog"):
            asset_path = pytest.config.getoption("--artifacts")
            if not os.path.exists(asset_path+"/routerlogs"):
                logger.debug("Making routerlogs directory")
                os.mkdir(asset_path+"/routerlogs")
            syslogdguy = threading.Thread(target=syslogd.journey_syslogd, args=["{}/routerlogs".format(asset_path)])
            syslogdguy.daemon = True
            syslogdguy.start()
            logger.info("You are now a syslog server. Router logs are being saved at {}".format(asset_path+"/routerlogs"))

    def setup(self, network_config, asset_handler, validate=False):
        self.asset_handler = asset_handler
        self.network_config = network_config

        # Find hardware map
        if self.hwmap_str:
            self.hwmap = self.asset_handler.get_asset_json(self.hwmap_str)
            logger.debug(self.hwmap)
        self.__parse_netconf()
        logger.debug("POST PARSE CONFIG:")
        logger.debug(self.network_config)

        # Handle remote routers
        if self.remote_routers:
            self.setup_remote_routers(validate=True)

        # Do additional steps for physical routers
        if self.phy_routers:
            self.setup_physical_routers(validate=validate)

        self.__setup_virtual_network()

        return self.virtnet_instance

    def setup_with_firmware(self, network_config, asset_handler, firmware):
        srcpath = ""
        dstpath = ""
        lethe_dir = ""
        self.asset_handler = asset_handler
        self.network_config = network_config
        if not self.gpath:
            logger.warning("GUIDO_PATH not set in run-config")
            return
        self.use_guido = True

        # Strip tailing / from guido path if present
        opsplit = os.path.split(self.gpath)
        if not len(opsplit[1]):
            self.gpath = self.gpath.rstrip("/")

        srcpath = "{}/{}".format(self.gpath, firmware)
        lethe_dir = self.__lethe_dir(srcpath)
        if not lethe_dir:
            logger.warning("Firmware {} does not have a lethe build!!".format(firmware))
            return
        srcpath = srcpath+"/{}/coconut.img".format(lethe_dir)
        logger.debug("Grabbing firmware @ [{}]".format(srcpath))

        self.saved_image = "tmp.guido/coco_copy.img"
        self.__grab_guido_image(srcpath, self.saved_image)

        self.__parse_netconf()
        logger.info(self.network_config)

        self.__setup_virtual_network()

        return self.virtnet_instance

    def __flag_interactive(self):
        logger.debug("__flag_interactive")

        foo = utils.search_dict3(self.network_config, "interactive", True)
        if foo:
            logger.info('Interactive flagged')
            self.interactive = True

    def __setup_virtual_network(self):
        if not self.network_config:
            logger.warning("No network_config set, exiting")
        else:
            self.virtnet_instance = VirtNetwork(self.network_config)
            self.virtnet_instance.start()
            logger.info("complete: __setup_virtual_network")
            logger.debug(self.virtnet_instance)
            logger.debug("Virtnet instance networking info: {}".format(self.virtnet_instance.networking))

    def setup_remote_routers(self, validate=False):
        """
        When self.setup() is called, IF there are 'remoterouters', iterate through them, create a temporary
        remotertr_setup_net for each
        :return:
        """

        if not self.network_config:
            logger.warning("No network_config set, exiting")
        else:
            #
            setnet = self.asset_handler.get_asset_json('remotertr_setup_net.json')
            for k, v in self.network_config["nodes"].items():
                if v['type'] == 'remoterouter':

                    # Only do the setup if the there was a config specified
                    try:
                        v['options']['config']
                    except KeyError:
                        logger.info("skipping setup_remote_router [{}] no config specified".format(k))
                    else:
                        this_sn = copy.deepcopy(setnet)
                        this_sn['nodes']['r1']['options'].update(v['options'])

                        lanoptions = {"remote": None, "key": None}
                        try:
                            v['options']['remote_dest']
                        except KeyError:
                            logger.info("skipping setup_remote_router [{}] no remote_dest specified".format(k))
                        else:
                            lanoptions['remote'] = v['options']['remote_dest']

                        try:
                            v['options']['interface_id']
                        except KeyError:
                            logger.info("skipping setup_remote_router [{}] no interface_id specified".format(k))
                        else:
                            lanoptions['key'] = v['options']['interface_id']

                        this_sn['nodes']['remotelan']['options'].update(lanoptions)
                        logger.debug("Configuring node {} !!!".format(k))
                        self.setup_this_router(this_sn, validate)

    def teardown_remote_routers(self):
        """
        When self.teardown() is called, IF there are 'remoterouters', iterate through them, create a temporary
        remotertr_teadown_net for each
        :return:
        """
        if not self.network_config:
            logger.warning("No network_config set, exiting")
        else:
            setnet = self.asset_handler.get_asset_json('remotertr_teardown_net.json')
            for k, v in self.network_config["nodes"].items():
                if v['type'] == 'remoterouter':
                    this_sn = copy.deepcopy(setnet)
                    this_sn['nodes']['r1']['options'].update(v['options'])
                    lanoptions = {"remote": None, "key": None}
                    try:
                        v['options']['remote_dest']
                    except KeyError:
                        logger.info("skipping teardown_remote_router [{}] no remote_dest specified".format(k))
                    else:
                        lanoptions['remote'] = v['options']['remote_dest']

                    try:
                        v['options']['interface_id']
                    except KeyError:
                        logger.info("skipping teardown_remote_router [{}] no interface_id specified".format(k))
                    else:
                        lanoptions['key'] = v['options']['interface_id']

                    this_sn['nodes']['remotelan']['options'].update(lanoptions)
                    logger.debug("Cleaning node {} !!!".format(k))
                    self.clean_this_router(this_sn)

    def setup_physical_routers(self, validate=False):
        """
        When self.setup() is called, IF there are 'realrouters', iterate through them, create a temporary
        setup_net for each that contains a client
        :return:
        """

        if not self.network_config:
            logger.warning("No network_config set, exiting")
        else:
            #
            setnet = self.asset_handler.get_asset_json('setup_net.json')
            for k, v in self.network_config["nodes"].items():
                if v['type'] == 'realrouter':

                    # Only do the setup if the there was a config specified
                    try:
                        v['options']['config']
                    except KeyError:
                        logger.debug("skipping setup_this_router [{}] no config specified".format(k))
                    else:
                        this_sn = copy.deepcopy(setnet)
                        # logger.debug(v['options'])  # verbose logging
                        this_sn['nodes']['r1']['options'].update(v['options'])
                        # logger.debug("this setup network:")  # verbose logging
                        # logger.debug(this_sn)

                        if v['options']['config']:
                            # If client_config is forced to False, then skip setup_this_router
                            #  and allow serial config later on via realrouter.start()
                            try:
                                v['options']['client_config']
                            except KeyError:
                                # client_config has not been set, so no override. pre-setup this router
                                self.setup_this_router(this_sn, validate)
                            else:
                                # If client_config is there, check to to see if it's True or False
                                if v['options']['client_config']:
                                    self.setup_this_router(this_sn, validate)
                                else:
                                    logger.debug(
                                        "Skipping setup_this_router [{}] because client_config is false".format(k))

    def clean_this_router(self, teardown_net):
        """
        Polling is false by default.

        :param teardown_net:
        :param poll:
        :return:
        """

        setup_inst = VirtNetwork(teardown_net)
        setup_inst.start()
        rinit = setup_inst.get_nsobj('r1')
        cinit = setup_inst.get_nsobj('client')
        rinit.cleanup(cinit)
        setup_inst.deinit()

    def setup_this_router(self, setup_net, poll=False):
        """
        Polling is false by default.

        :param setup_net:
        :param poll:
        :return:
        """

        setup_inst = VirtNetwork(setup_net)
        setup_inst.start()
        rinit = setup_inst.get_nsobj('r1')
        cinit = setup_inst.get_nsobj('client')

        rinit.clientside_config(cinit, self.asset_handler, poll=poll)
        logger.debug("Router [{}]: DONE WITH CLIENT SIDE CONFIG!".format(rinit.mac))
        time.sleep(.5)
        setup_inst.deinit()


    def gen_virtnet_from_map(self, asset_handler):
        """
        Generate a virtnet json file of real routers solely from the hardware map

        :param asset_handler:
        :return: template: network json to be consumed for setup
        """

        if not self.hwmap_str:
            logger.error('no hardware map found in run config. exiting')
            return

        self.asset_handler = asset_handler
        template = self.asset_handler.get_asset_json('template.json')

        # Find hardware map
        self.hwmap = self.asset_handler.get_asset_json(self.hwmap_str)

        logger.debug("Generating virtnet json file from hardware map: {}".format(self.hwmap_str))
        # logger.debug(self.hwmap)  # verbose

        # Other options from the hardware map will be added during parse_netconf
        # So this inital network config only contains type
        for k, v in self.hwmap["PRODUCT_ROUTER"].items():
            template['nodes'].update({k: {"type": "realrouter", "options": {"client_config": True}}})
        logger.debug(template)  # verbose

        return template

    def __parse_netconf(self):
        if not self.network_config:
            logger.error("No network_config set, cannot parse")
        defaultimg = ""

        # Checking for defaults
        if "defaults" in self.network_config:
            logger.debug("defaults found in network_config")
            for k, v in self.network_config["defaults"].items():
                if "image" in k:
                    defaultimg = v
                    logger.debug("using default router image: {}".format(defaultimg))
                    def_image_path = self.__check_router_image(defaultimg)
                    if def_image_path:
                        logger.debug("replacing default image in net_config with full path")
                        self.network_config["defaults"]["image"] = def_image_path

        for k, v in self.network_config["nodes"].items():
            thisnode = v
            is_virtrouter = False
            is_realrouter = False
            is_remoterouter = False
            for k1, v1 in v.items():
                if "type" in k1:
                    if "router" == v1:
                        is_virtrouter = True
                    elif "remoterouter" == v1:
                        is_remoterouter = True
                    elif "realrouter" == v1:
                        is_realrouter = True
            if is_virtrouter:
                if not thisnode.get('options'):
                    thisnode['options'] = {}
                if 'image' not in thisnode['options']:
                    logger.debug("Using default image config of 'coconut.img' on {}".format(k))
                    thisnode['options']['image'] = 'coconut.img'
                for k1, v1 in thisnode["options"].items():
                    if "config" in k1:
                        if isinstance(v1, list):
                            logger.debug('config {} is json config'.format(v1))
                            continue
                        if os.path.isabs(v1):
                            logger.debug("config {} is abs path".format(v1))
                            if os.path.isfile(v1):
                                logger.debug("Stuffing router config {} into netconfig".format(v1))
                                self.network_config["nodes"][k]["options"]["config"] = json_file_to_dict(v1)
                            else:
                                logger.warning("could not find file @ {}".format(v1))
                        else:
                            findconfig = self.asset_handler.get_asset_json(v1)
                            logger.debug("Stuffing router config {} into netconfig".format(v1))
                            self.network_config["nodes"][k]["options"]["config"] = findconfig
                    elif "image" in k1:
                        image_path = self.__check_router_image(v1)
                        if image_path:
                            logger.debug("Setting image path for {} @ {}".format(k, image_path))
                            self.network_config["nodes"][k]["options"]["image"] = image_path
            elif is_remoterouter:
                self.remote_routers = True
            elif is_realrouter:
                self.phy_routers = True
                if not thisnode.get('options'):
                    thisnode['options'] = {}

                # Do substitutions with the hardware map if it exists
                if self.hwmap:

                    if k in self.hwmap["PRODUCT_ROUTER"]:

                        # Handle router type
                        if 'type' not in thisnode['options']:
                            if self.hwmap["PRODUCT_ROUTER"][k]["type"]:
                                self.network_config["nodes"][k]["options"]["type"] = self.hwmap["PRODUCT_ROUTER"][k]["type"]

                        # Handle MAC address
                        if 'mac' not in thisnode['options']:
                            if self.hwmap["PRODUCT_ROUTER"][k]["mac"]:
                                self.network_config["nodes"][k]["options"]["mac"] = self.hwmap["PRODUCT_ROUTER"][k]["mac"]

                        # Handle serial ports
                        if 'serial' not in thisnode['options']:
                            if self.hwmap["PRODUCT_ROUTER"][k]["serial"]:
                                self.network_config["nodes"][k]["options"]["serial"] = self.hwmap["PRODUCT_ROUTER"][k]["serial"]

                        # Handle interfaces
                        if 'interfaces' not in thisnode['options']:
                            # See if router has a switch map to reference
                            if not self.hwmap["PRODUCT_ROUTER"][k]["switch_map"]:
                                logger.warning("{} had no switch_map to reference."
                                               "This is required when using real routers without interfaces".format(self.hwmap["PRODUCT_ROUTER"][k]))
                            else:
                                new_ifs = []
                                mysw = self.hwmap["PRODUCT_ROUTER"][k]["switch"]
                                if mysw not in self.hwmap["SWITCH"]:
                                    logger.warning("{}'s switch {} not found in hardware map".format(
                                        self.hwmap["PRODUCT_ROUTER"][k], mysw))
                                else:
                                    for p in self.hwmap["PRODUCT_ROUTER"][k]["switch_map"]:
                                        if p not in IGNORE_PORTS:
                                            new_ifs.append(self.hwmap["SWITCH"][mysw]["vlan_map"][p])
                                    self.network_config["nodes"][k]["options"]["interfaces"] = new_ifs

    @staticmethod
    def __grab_guido_image(src, dst):
        """
        Args:
            src (str): The path of the guido image you want to grab
            dst (str): The destination of the copied image

        Returns:
        """

        cmd = "python3 tools/interact_guido_mount.py --src {} --dst {} -x".format(src, dst)

        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        resp = p.communicate()
        logger.debug(resp)

        if not os.path.exists(dst):
            logger.error("Problem copying router file to {}".format(dst))

    def __check_router_image(self, image):
        """
        Args:
            image (str): The path of the lethe image you think you should use for virtnetwork

        Returns:
            image (str): The path of the lethe image you should use for virtnetwork
        """
        if self.use_guido:
            return self.saved_image
        elif os.path.isabs(image):
            logger.debug("Router image is already abs path")
            if self.gpath in image:
                logger.debug("Found config_fixture.run.fixtures.virtNetwork.guidoPath in image")
                randnum = "%8x" % random.getrandbits(32)
                self.saved_image = "tmp.guido/coco_copy_{}.img".format(randnum)
                self.__grab_guido_image(image, self.saved_image)
                return self.saved_image
            return image
        else:
            # For now, using <fixture_dir>/router_images for default place to check for file
            img_file = self.asset_handler.get_asset_file(image)
            if img_file:
                logger.info("router image file found @ {}".format(img_file.name))
                return img_file.name
            else:
                logger.warning("Could not find router image file specified. Make sure file is located in framework/assets/router_images directory")
                return 0

    def __lethe_dir(self, path):
        # Return the lethe directory name
        match = ""
        lethe_dir_list = os.listdir(path)
        for l in lethe_dir_list:
            if os.path.isdir("{}/{}".format(path, l)):
                if l.find("lethe") != -1:
                    logger.debug(" LETHE MATCH: {}".format(l))
                    match = l
        return match

    def gather_coverage_data(self):
        for k,v in self.network_config['nodes'].items():
            if 'router' == v['type']:
                logger.debug("gathering logs for {}".format(k))
                config(v['init'].ns, 'put', 'control.coverage', value='stop')
                data = config(v['init'].ns, 'get', 'status.coverage.cov_file')
                if data != "null":
                    logger.debug("found coverage data, creating .cov file...")
                    if data:
                        # Some tests have paths as args, make sure they don't break coverage gathering
                        test_name = self.request.node.name.replace('/', '.')
                        fpath = self.artifacts_dir + '/coverage/' + test_name + '_' + str(k) + '.cov'
                        with open(fpath, 'w') as f:
                            logger.debug("Saving coverage data in {}".format(fpath))
                            f.write(data)
                        os.chmod(fpath,0o666)

    def teardown(self):

        # Grab the logs of all the routers if there was a failure
        if hasattr(self.request.node, 'rep_call') and self.request.node.rep_call.failed and pytest.config.getoption("--gatherlogs"):
            t1 = time.time()
            try:
                for k,v in self.network_config['nodes'].items():
                    if 'router' == v['type']:
                        logger.debug("gathering logs for {}".format(k))
                        vrc1 = VirtRouterConsole()
                        vc_h = vrc1.get_virtconsole_handler(coi=self.virtnet_instance.networking, rtr=k)
                        vc_h.run_cmd('log')
                        vrc1.teardown()
                    elif 'realrouter' == v['type']:
                        logger.debug("gathering logs for {}".format(k))
                        node_init = self.virtnet_instance.networking["nodes"][k]["init"]
                        logger.debug(node_init.serialconn.get("log"))
            except Exception:
                logger.debug("exception in failure log gather")
                pass
            logger.debug("Took {:3f} seconds grabbing router logs".format((time.time() - t1)))
        elif self.virtnet_instance and pytest.config.getoption("--coverage"):
            logger.info("Attempting to gather coverage data")
            self.gather_coverage_data()
        logger.info("Tearing down VirtNetwork instance:{}".format(self.virtnet_instance))
        if self.virtnet_instance:
            self.virtnet_instance.deinit()
        try:
            shutil.rmtree("tmp.guido")
        except FileNotFoundError:
            pass
        pass

        if self.remote_routers:
            self.teardown_remote_routers()


    def get_failure_report(self):
        pass


class SSHInterface(object):
    # SSH connection to server
    # Fixture wrapper for sshclient_paramiko

    def __init__(self, host="192.168.0.1", username="admin", password="00000000"):
        logger.info("init SSHInterface")

        self.in_namespace = False
        self.this_interface = ssh(host, username, password)


    def connect(self):
        logger.debug("starting ssh connection")
        self.this_interface.connect()

    def sendcmd(self, cmd):
        logger.debug("sending command:{}".format(cmd))


    def teardown(self):
        logger.info("tearing down SSHInterface:{}".format())
        self.this_interface.close()
        pass

    def get_failure_report(self):
        pass


# TODO convert to use POM
# class SeleniumHostUI(FixtureBase):
#     """
#     The main motive of this class is
#     being able to create an interface
#     for creating Firefox browser instances
#     running in different namespaces
#     """
#
#     def __init__(self, CONFIG_FIXTURE):
#         logger.info("Init SeleniumHostUI")
#         self.display = None #pyvirtualdisplay
#         self.interface = None #SeleniumSessionInterface
#         self.CONFIG_FIXTURE = CONFIG_FIXTURE #CONFIG_FIXTURE
#
#
#     def start_virt_display(self):
#         logger.info('Starting virtual display.')
#         with open('/tmp/journey-xephyr-init.lock', 'w') as lock:
#             fcntl.lockf(lock, fcntl.LOCK_EX)
#             self.display = Display(visible=self.CONFIG_FIXTURE.RUN.SELENIUM.VIRTUAL_DISPLAY_VISIBLE, size=(self.CONFIG_FIXTURE.RUN.SELENIUM.DISPLAY_SIZE_W, self.CONFIG_FIXTURE.RUN.SELENIUM.DISPLAY_SIZE_H))
#             self.display.start()
#
#
#     def stop_virt_display(self):
#         logger.info('Stopping virtual display.')
#         if self.display:
#             self.display.stop()
#             self.display = None
#
#
#     def start_session(self, disable_cookies=False):
#         logger.info('Starting Selenium Interface.')
#         timeout = self.CONFIG_FIXTURE.RUN.SELENIUM.TIMEOUT
#         display_width = self.CONFIG_FIXTURE.RUN.SELENIUM.DISPLAY_SIZE_W
#         display_height = self.CONFIG_FIXTURE.RUN.SELENIUM.DISPLAY_SIZE_H
#         needle_save_baseline = self.CONFIG_FIXTURE.RUN.SELENIUM.NEEDLE_SAVE_BASELINE
#         artifacts_dir = None
#         if hasattr(pytest, 'config') and pytest.config.getoption("--artifacts"):
#             artifacts_dir = pytest.config.getoption("--artifacts")
#
#         self.interface = SeleniumSessionInterface(display_width=display_width,
#                                              display_height=display_height,
#                                              needle_save_baseline=needle_save_baseline,
#                                              timeout=timeout,
#                                              artifacts_dir=artifacts_dir)
#         self.interface.start_session(disable_cookies)
#
#
#     def quit_session(self):
#         logger.info('Stop Selenium Interface.')
#         if self.interface:
#             self.interface.quit_session()
#             self.interface = None
#
#
#     def get_failure_report(self):
#         pass
#
#
#     def teardown(self):
#         self.quit_session()
#         self.stop_virt_display()


class VirtRouterConsole(FixtureBase):

    @staticmethod
    def get_virtconsole_handler(coi, rtr):
        """
            Open socket connection to router's virtconsole and return the handler

            Args:
                coi: Dictionary of available network nodes and their information
                rtr: Router name from network config

            Returns:
                Handler to virtconsole
        """
        if not isinstance(coi, VirtNetwork):
            # it's probably the virtnetwork.networking dictionary
            logger.warning('passing virtnetwork.networking is deprecated, just pass in virtnetwork')
            coi = coi['virtnetwork_init']

        router_init = coi.get_node(rtr)
        if not router_init:
            raise Exception('{} has no {} router node'.format(coi, rtr))

        try:
            quiet = pytest.config.getoption("--coverage")
        except AttributeError:
            quiet = False
        virtconsole_handler = VRouterConsole(rtr=router_init, quiet=quiet)
        logger.debug("Opening virtconsole socket to {}".format(rtr))
        virtconsole_handler.socket_connect()

        return virtconsole_handler

    def teardown(self):
        pass

    def get_failure_report(self):
        pass

