#!/usr/bin/env python3

import smtpd
import asyncore
from threading import Thread, Event

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.setns import netns


class DumbSMTPD(smtpd.SMTPServer):
    def __init__(self, localaddr, remoteaddr, **kwargs):
        self.received = []
        super().__init__(localaddr, remoteaddr, **kwargs)

    def process_message(self, clientip, envfrom, envto, data, **kwargs):
        msg = {
            'clientip': clientip,
            'envfrom': envfrom,
            'envto': envto,
            'data': data
        }
        self.received.append(msg)
        print(
            'From clientIP: %(clientip)s\n'
            'Envelope From: %(envfrom)s\n'
            'Envelope To  : %(envto)s\n'
            'Data         :\n\n%(data)s\n\n'
            % msg
        )


class NSSMTPD:
    def __init__(self, ip, port=25, **options):
        self.local = (ip, port)
        self.options = options
        self.event = Event()
        self.received = []

    def start(self, ns):
        with netns(ns.nsname, etc_dir=ns.etc_dir):
            self.srv_thread = Thread(target=self.run_server, args=(self.local, self.event, self.received, ns), kwargs=self.options)
            self.srv_thread.daemon = True
            self.srv_thread.start()

    def run_server(self, local, event, messages, ns, **options):
        server = DumbSMTPD(local, None, **options)
        while not event.is_set():
            with netns(ns.nsname, etc_dir=ns.etc_dir):
                asyncore.loop(count=1, timeout=5)
            messages.extend(server.received)

    def stop(self):
        self.event.set()
        self.srv_thread.join(timeout=15)
        if self.srv_thread.is_alive():
            print(self, 'FAILED TO JOIN')
        return self.received


if __name__ == '__main__':

    server = DumbSMTPD(('127.0.0.1', 9925), None)

    while not server.received:
        try:
            asyncore.loop(count=1)
            #asyncore.loop(timeout=1)
        except KeyboardInterrupt:
            print()
            break

    for msg in server.received:
        print(
            'From clientIP: %(clientip)s\n'
            'Envelope From: %(envfrom)s\n'
            'Envelope To  : %(envto)s\n'
            'Data         :\n\n%(data)s\n\n'
            % msg
        )
