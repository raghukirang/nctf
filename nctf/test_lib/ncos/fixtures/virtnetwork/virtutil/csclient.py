"""
csclient.py - Wrapper for simplified csclient access to config in a namespace

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import json
import re
import socket
from time import sleep

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.virtexec import virtexec


class CSClient(object):

	END_OF_HEADER = b"\r\n\r\n"
	STATUS_HEADER_RE = re.compile(b"status: \w*")
	CONTENT_LENGTH_HEADER_RE = re.compile(b"content-length: \w*")
	MAX_PACKET_SIZE = 8192

	def __init__(self, host, port, timeout=30):

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.settimeout(timeout)
		self.sock.connect((host, port))

	def deinit(self):

		self.sock.close()

	def _csclient_send(self, data):

		self.sock.sendall(data.encode())
		self.sock.sendall(b"\n")

	def _csclient_recv(self):

		data = b""
		eoh = -1
		while eoh < 0:
			buf = self.sock.recv(self.MAX_PACKET_SIZE)
			if len(buf) == 0:
				break

			data += buf
			eoh = data.find(self.END_OF_HEADER)

		status_hdr = self.STATUS_HEADER_RE.search(data).group(0)[8:]
		content_len = self.CONTENT_LENGTH_HEADER_RE.search(data).group(0)[16:]
		remaining = int(content_len) - (len(data) - eoh - len(self.END_OF_HEADER))

		while remaining > 0:
			buf = self.sock.recv(self.MAX_PACKET_SIZE)
			if len(buf) == 0:
				break

			data += buf
			remaining -= len(buf)

		body = data[eoh:].decode()

		try:
			result = json.loads(body)
		except Exception:
			# config store receiver doesn't give back
			# proper json for 'put' ops, body
			# contains verbose error message
			# so putting the error msg in result
			result = body.strip()

		return {"status": status_hdr.decode(), "data": result}

	def get(self, base=None, query=None, tree=False):

		base = base.replace('.', '/') if base else ""
		query = query if query else ""

		self._csclient_send("get")
		self._csclient_send(base)
		self._csclient_send(query)
		self._csclient_send("1" if tree else "0")

		return self._csclient_recv()

	def put(self, base=None, query=None, tree=False, value=None):

		base = base.replace('.', '/') if base else ""
		query = query if query else ""
	
		self._csclient_send("put")
		self._csclient_send(base)
		self._csclient_send(query)
		self._csclient_send("1" if tree else "0")
		self._csclient_send(json.dumps(value))

		return self._csclient_recv()

	def post(self, base=None, query=None, value=None):

		base = base.replace('.', '/') if base else ""
		query = query if query else ""
	
		self._csclient_send("post")
		self._csclient_send(base)
		self._csclient_send(query)
		self._csclient_send(json.dumps(value))

		return self._csclient_recv()

	def delete(self, base=None, query=None):

		base = base.replace('.', '/') if base else ""
		query = query if query else ""

		self._csclient_send("delete")
		self._csclient_send(base)
		self._csclient_send(query)

		return self._csclient_recv()


def config(ns, method, *args, virthost='127.0.0.1', virtport=7777, strict=False, timeout=30, **kwargs):
	""" Simple wrapper to execute a config store client method in a namespace """

	# Wrap up info in a closure
	def runCsCmd():
		cs = CSClient(virthost, virtport, timeout=timeout)
		ret = getattr(cs, method)(*args, **kwargs)
		cs.deinit()
		sleep(1)

		if ret['status'] != 'ok':
			if strict:
				raise Exception("Config Store Error (%s)" % ret)
			else:
				print("Warning: Config Store Error (%s)" % ret)

		return ret['data']

	return virtexec(ns, runCsCmd)
