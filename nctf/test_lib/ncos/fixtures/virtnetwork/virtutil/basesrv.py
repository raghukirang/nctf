import logging
from threading import Thread, currentThread

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.setns import netns

class BaseSrv(object):

	""" Namespace aware base server class """

	def __init__(self, ip, port, family='inet', timeout=None):
		self.ip = ip
		self.port = port
		self.family = family
		self.timeout = timeout or 60
		self.server = None


	def _create_server(self):
		raise NotImplementedError("child class must implement _create_server")

	def start(self, ns):

		with netns(ns.nsname, etc_dir=ns.etc_dir):
			self.server = self._create_server()
			self.srv_thread = Thread(target=self.server.serve_forever)
			self.srv_thread.daemon = True
			self.srv_thread.start()


	def stop(self):

		self.server.shutdown()
		self.srv_thread.join(timeout=self.timeout)
		if self.srv_thread.is_alive():
			print_error(self, 'FAILED TO JOIN')

		self.server = None


def print_error(obj, type):
	print('%s %s [%s]' % (type, obj, currentThread()))
	import traceback
	traceback.print_stack()


def getSrvLogger(name, level=logging.INFO):
	logger = logging.getLogger(name)
	console = logging.StreamHandler()
	formatter = logging.Formatter('%(created).3f [%(levelname)s] [%(name)s] %(message)s')
	console.setFormatter(formatter)
	logger.addHandler(console)
	logger.setLevel(level)
	return logger
