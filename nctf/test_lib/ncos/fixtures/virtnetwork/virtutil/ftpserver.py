from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import ThreadedFTPServer
from pyftpdlib.authorizers import DummyAuthorizer
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv, getSrvLogger

logger = getSrvLogger('ftp')


class FtpServer(ThreadedFTPServer):

    def shutdown(self):
        self.close_all()


class FTPd(BaseSrv):
    def __init__(self, ip, port=21, masq_addr=None, **kwargs):
        super().__init__(ip, port, **kwargs)
        self.masq_addr = masq_addr

    def _create_server(self, masq_addr=None):
        authorizer = DummyAuthorizer()
        authorizer.add_user('user', '12345', '.', perm='elradfmwMT')
        handler = FTPHandler
        handler.authorizer = authorizer
        # specify range of ports to use for passive connections.
        if self.masq_addr:
            handler.masquerade_address = self.masq_addr
        handler.passive_ports = range(60000, 65535)
        return FtpServer((self.ip, self.port), handler)



