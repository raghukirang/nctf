"""
config.py - Namespace aware TCP/UDP test servers

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import logging
import socket
import socketserver
import time
from threading import Lock, Thread

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv, print_error
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.setns import netns

logger = logging.getLogger(__name__)
_testdata = b'0123456789'*128

def genTestData():
	""" Very simple data generator for transfer tests. """
	return _testdata


def runTestSrvTx(srv_ns, client_ns, srv_ipaddr, srv_port,
		 conn_ipaddr, conn_port, proto, family, txfer_time, **kwargs):
	""" Basic transfer test that establishes a connection from client_ns
	to srv_ns, and transmits data for txfer_time, and returns
	the mbps and bytes received by the server. """

	srv = startTestSrv(srv_ns, srv_ipaddr, srv_port, proto, family, 'tx', **kwargs)

	starttime = time.time()
	startTestClient(client_ns, conn_ipaddr, conn_port, proto, family, txfer_time, 'tx', **kwargs)
	endtime = time.time()

	bytesrecv = stopTestSrv(srv)

	mbitpersec = (((bytesrecv/(endtime-starttime))*8)/1024)/1024

	return mbitpersec, bytesrecv


def runTestSrvRx(srv_ns, client_ns, srv_ipaddr, srv_port,
		 conn_ipaddr, conn_port, proto, family, txfer_time):
	""" Basic transfer test that establishes a connection from client_ns
	to srv_ns, and receives data for txfer_time, and returns
	the mbps and bytes received by the client. """

	srv = startTestSrv(srv_ns, srv_ipaddr, srv_port, proto, family, 'rx')

	starttime = time.time()
	bytesrecv = startTestClient(client_ns, conn_ipaddr, conn_port, proto, family, txfer_time, 'rx')
	endtime = time.time()

	stopTestSrv(srv)

	mbitpersec = (((bytesrecv/(endtime-starttime))*8)/1024)/1024

	return mbitpersec, bytesrecv


def startTestSrv(srv_ns, srv_ipaddr, srv_port, proto, family, direction, **kwargs):
	""" Start a test server that listens for incoming connections. """

	perfhandler = {
		'tx': { 'udp': SimpleUDPRecv,
			'tcp': SimpleTCPRecv },
		'rx': { # TODO 'udp': SimpleUDPSend,
			'tcp': SimpleTCPSend }
	}[direction][proto]

	srv = TestSrv(srv_ipaddr, srv_port, proto, family, **kwargs)
	srv.start(srv_ns, perfhandler)

	return srv


def stopTestSrv(srv):
	""" Stops a test server and returns bytes received if
	available (only in tx mode) """

	results = srv.stop()
	return srv.testdata['bytesrecv']


def startTestClient(client_ns, conn_ipaddr, conn_port, proto, family, txfer_time, direction, **kwargs):
	""" Starts a test client that runs for txfer_time, and returns bytes received if
	available (only in rx mode) """
	logger.debug("starting startTestClient()")
	client = TestClient(conn_ipaddr, conn_port, proto, family, **kwargs)

	starttime = time.time()

	if direction == 'tx':
		def genTraffic():
			if (time.time() - starttime) < txfer_time:
				return genTestData()
			else:
				return False

		client.transfer(client_ns, genTraffic, 'tx')
	elif direction == 'rx':
		results= {'bytesrecv': 0}
		def recvTraffic(d):
			results['bytesrecv'] += len(d)
			if (time.time() - starttime) < txfer_time:
				return True
			else:
				return False

		ctrans = client.transfer(client_ns, recvTraffic, 'rx')

	if not "success" in ctrans:
		logger.warning("client transfer test did not appear successful")
	logger.debug("ending startTestClient()")
	if direction == 'rx':
		return results['bytesrecv']
	else:
		return 0


class TestSrv(BaseSrv):

	""" Namespace aware test server class """

	def __init__(self, srvip, srvport, proto, family, **kwargs):
		super().__init__(srvip, srvport, family, **kwargs)
		self.proto = proto
		self.testdata = {'bytesrecv': 0}

	def start(self, ns, handler):
		srvtype = {
			'udp': { 'inet': TestUDPServer,
				 'inet6': TestUDPServerIPv6 },
			'tcp': { 'inet': TestTCPServer,
				 'inet6': TestTCPServerIPv6 },
		}[self.proto][self.family]
		# setting _create_server to a lambda here allows closure on 'handler' and
		# 'testdata' but the code will execute within BaseSrv's runSrv() method
		# which is in the proper namespace.  If we execute srvtype() here we
		# aren't in the namespace and so we won't bind correctly.
		self._create_server = lambda: srvtype(self.testdata, (self.ip, self.port), handler)
		super().start(ns)


class TestClient(object):

	""" Namespace aware test client class """

	max_packet_size = 8192
	sock_timeout = 10

	def __init__(self, connip, connport, proto, family, timeout=None):
		self.connip = connip
		self.connport = connport
		self.proto = proto
		self.family = family
		self.timeout = timeout or 60


	def transfer(self, ns, handler, direction='tx'):
		""" In 'tx' mode, this will repeatedly call handler and send what
		it provides until handler returns False.  In 'rx' mode, this will
		recv on the socket and provide the data to handler until the
		socket is closed or handler returns False. """

		result = None
		def runClient():
			sockproto = {
				'udp': socket.SOCK_DGRAM,
				'tcp': socket.SOCK_STREAM
			}[self.proto]
			sockfamily = {
				'inet': socket.AF_INET,
				'inet6': socket.AF_INET6
			}[self.family]
			sock = socket.socket(sockfamily, sockproto)
			try:
				sock.settimeout(self.sock_timeout)

				if self.proto == 'tcp':
					sock.connect((self.connip, self.connport))

				if direction == 'tx':
					transfunc = self.__tx_data
				elif direction == 'rx':
					transfunc = self.__rx_data

				while transfunc(handler, sock):
					# Nothing to do here
					pass

				result = 'success'
			except socket.timeout:
				result = 'timeout'
			except Exception as e:
				result = 'unknown (%s)' % e
			finally:
				sock.close()

		t = Thread(target=runClient)
		t.daemon = True
		with netns(ns.nsname, etc_dir=ns.etc_dir):
			t.start()

		t.join(timeout=self.timeout)
		if t.is_alive():
			print_error(self, 'FAILED TO JOIN')

		return result


	def __tx_data(self, handler, sock):
		d = handler()
		if d is False:
			return False

		if self.proto == 'tcp':
			sock.sendall(d)
		elif self.proto == 'udp':
			sock.sendto(d, (self.connip, self.connport))

		return True


	def __rx_data(self, handler, sock):
		d = sock.recv(self.max_packet_size)
		recvlen = len(d)
		if recvlen <= 0:
			# remote shut down
			return False

		if handler(d) is False:
			return False

		return True


class TestTCPServer(socketserver.ThreadingTCPServer):

	allow_reuse_address = True

	def __init__(self, testdata, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.testdata = testdata
		self.testdata_lock = Lock()


class TestTCPServerIPv6(TestTCPServer):

	address_family = socket.AF_INET6


class TestUDPServer(socketserver.ThreadingUDPServer):

	allow_reuse_address = True

	def __init__(self, testdata, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.testdata = testdata
		self.testdata_lock = Lock()


class TestUDPServerIPv6(TestUDPServer):

	address_family = socket.AF_INET6


class SimpleTCPRecv(socketserver.BaseRequestHandler):

	max_packet_size = 8192
	timeout_val = 20

	def setup(self):
		self.request.settimeout(self.timeout_val)

	def handle(self):
		if self.request:
			while True:
				d = self.request.recv(self.max_packet_size)
				if not d:
					print("Breaking over")
					break
				recvlen = len(d)
				with self.server.testdata_lock:
					self.server.testdata['bytesrecv'] += recvlen
		else:
			return


class SimpleTCPSend(socketserver.BaseRequestHandler):

	max_packet_size = 8192

	def handle(self):
		while True:
			try:
				self.request.sendall(genTestData())
			except Exception as e:
				print("SimpleTCPSend exception: %s", str(e))
				# remote shut down
				break


class SimpleUDPRecv(socketserver.BaseRequestHandler):

	def handle(self):
		d = self.request[0]
		recvlen = len(d)
		with self.server.testdata_lock:
			self.server.testdata['bytesrecv'] += recvlen


class UDPServerIPv6(socketserver.UDPServer):
	address_family = socket.AF_INET6
