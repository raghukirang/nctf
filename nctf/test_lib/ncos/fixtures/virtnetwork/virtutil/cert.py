"""
cert.py - Utilities for generating 'interesting' certs for test cases

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

Much of this code is lifted from lib/python-pyOpenSSL/examples

and mashed up with the certmgmt.py to get things in the correct format for the config store

"""

from OpenSSL import crypto
from OpenSSL.crypto import X509Extension
import uuid

def createKeyPair(type, bits):
    """
    Create a public/private key pair.

    Arguments: type - Key type, must be one of TYPE_RSA and TYPE_DSA
               bits - Number of bits to use in the key
    Returns:   The public/private key pair in a PKey object
    """
    pkey = crypto.PKey()
    pkey.generate_key(type, bits)
    return pkey


def createCertRequest(pkey, digest="md5", **name):
    """
    Create a certificate request.

    Arguments: pkey   - The key to associate with the request
               digest - Digestion method to use for signing, default is md5
               **name - The name of the subject of the request, possible
                        arguments are:
                          C     - Country name
                          ST    - State or province name
                          L     - Locality name
                          O     - Organization name
                          OU    - Organizational unit name
                          CN    - Common name
                          emailAddress - E-mail address
    Returns:   The certificate request in an X509Req object
    """
    req = crypto.X509Req()
    subj = req.get_subject()

    for (key,value) in name.items():
        setattr(subj, key, value)

    req.set_pubkey(pkey)
    req.sign(pkey, digest)
    return req


def createCertificate(req, issuerCert, issuerKey, serial, notBefore, notAfter, digest="md5", is_ca=False):
    """
    Generate a certificate given a certificate request.

    Arguments: req        - Certificate reqeust to use
               issuerCert - The certificate of the issuer
               issuerKey  - The private key of the issuer
               serial     - Serial number for the certificate
               notBefore  - Timestamp (relative to now) when the certificate
                            starts being valid
               notAfter   - Timestamp (relative to now) when the certificate
                            stops being valid
               digest     - Digest method to use for signing, default is md5
    Returns:   The signed certificate in an X509 object
    """
    extensions = []
    cert = crypto.X509()
    cert.set_serial_number(serial)
    cert.gmtime_adj_notBefore(notBefore)
    cert.gmtime_adj_notAfter(notAfter)
    cert.set_issuer(issuerCert.get_subject())
    cert.set_subject(req.get_subject())
    cert.set_pubkey(req.get_pubkey())
    if is_ca:
        extensions.append(X509Extension('basicConstraints'.encode(), False, 'CA:true'.encode()))
    if extensions:
        cert.add_extensions(extensions)
    cert.sign(issuerKey, digest)
    return cert


def genCAandCerts(ncerts=1, cname='CP virtrouter cert-{}'):
	""" generate and return a CA a server crt and key and n number of client
	crt and keys.  Initially this code makes the assumption that you are going
	to stuff the CA in all devices and the certs in ncerts number of devices in
	n unique routers so it formats the output values in a way that can be dropped
	directly into the /config/certmgmt/certs array in the config store. """
	cakey = createKeyPair(crypto.TYPE_RSA, 1024)
	careq = createCertRequest(cakey, CN='CP Certificate Authority')
	cacert = createCertificate(careq, careq, cakey, 0, 0, 60*60*24*365*5, is_ca=True) # five years
	# simulate the CS _genuuid algo based at 1 since the CPSC CA is first
	cauuid = "%08x" % 1 + str(uuid.uuid3(uuid.NAMESPACE_URL, "config.certmgt.certs"))[8:]
	certuuid = "%08x" % 2 + str(cauuid[8:])

	ca = {
		"_id_": cauuid,
		"ca_uuid": "None",
		"name": "CP virtrouter CA",
		"key": crypto.dump_privatekey(crypto.FILETYPE_PEM, cakey).decode(),
		"x509": crypto.dump_certificate(crypto.FILETYPE_PEM, cacert).decode()
	}
	certs = []
	for i in range(ncerts):
		pkey = createKeyPair(crypto.TYPE_RSA, 1024)
		req = createCertRequest(pkey, CN=cname.format(i))
		cert = createCertificate(req, cacert, cakey, 1, 0, 60*60*24*365*5) # five years
		certs.append({
			"_id_": certuuid,
			"name": cname.format(i),
			"ca_uuid": cauuid,
			"key": crypto.dump_privatekey(crypto.FILETYPE_PEM, pkey).decode(),
			"x509": crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode()
		})
	return ca, certs
