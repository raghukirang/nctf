"""
radiusserver.py - A series of classes for implementing RADIUS servers as virtservers for virtnetwork

RADIUSd - based on freeradius, started and stopped by ProcessMonitor
PYRADd - Python RADIUS "servers" using pyrad

virtservers.py by default uses RADIUSd
"""
import logging

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.procmon import ProcessMonitor
from pyrad import server, packet, dictionary

logger = logging.getLogger('radiusd')

class RADIUSd(BaseSrv):
    """
    A RADIUS authentication server powered by freeradius

    At time of initial commit, there is still no means of controlling users and secret keys on the server, so this
    implementation is largely for test environments until full control can be added.


    default freeradius config files are:
        /etc/freeradius/users
        /etc/freeradius/clients.conf

    tools/freeradius_setup.sh will add some boilerplate user and client configs until something better is established

    """

    RADIUS_CMD = ['freeradius', '-l', 'radiuslog', '-X']

    def __init__(self, ns=None, ip=None):

        self.ip = ip
        self.ns = ns
        self.radiusd = None
        self.radius_cmd = self.RADIUS_CMD

    def start(self):
        logger.debug('starting radiusd')

        if self.ip:
            self.radius_cmd.append('-i')
            self.radius_cmd.append('{}'.format(self.ip))
            self.radius_cmd.append('-p')
            self.radius_cmd.append('1812')

        self.radiusd = ProcessMonitor(cmd=self.radius_cmd, ns=self.ns, autoRestart=False)
        self.radiusd.start()

    def stop(self):
        logger.debug('stopping radiusd')

        if self.radiusd:
            self.radiusd.stop()
            self.radiusd = None


class Pyrad(server.Server):
    """
    At time of initial commit, this very hacked together server implementation only works with
    basic Administrative-User authentication (CP router use case Advanced User Auth w/ RADIUS).
    WPA2-Enterprise did not work and wired 802.1x was not tested

    see pyrad github page for more examples: https://github.com/wichert/pyrad/tree/master/example

    """
    def _HandleAuthPacket(self, pkt):
        #print("doing MY _HandleAuthPacket")

        server.Server._HandleAuthPacket(self, pkt)
        #print("Received an authentication request")
        #print("Attributes: ")
        #print(pkt)
        #for attr in pkt.keys():
        #    print("%s: %s" % (attr, pkt[attr]))
        username = pkt['User-Name'][0]
        #print("%s" % pkt['User-Password'])

        pwd = pkt.PwDecrypt(pkt['User-Password'][0])
        #print(pwd)

        reply = self.CreateReplyPacket(pkt, **{
            "Service-Type": "Administrative-User",
            "Session-Timeout": '3600',
            "Idle-Timeout": '3600'
        })
        # Set to reject before matching secret key
        reply.code = packet.AccessReject

        for k, v in self.hosts.items():
            if pkt.source[0] in v.address:
                #print("address source match!")
                if username in v.name:
                    #print("username match!")
                    if pwd in str(v.secret):
                        print("secret key match! Auth success for {}".format(username))
                        reply.code = packet.AccessAccept

        self.SendReplyPacket(pkt.fd, reply)

    def _CloseSockets(self):

        if self.acctfds:
            for fd in self.acctfds:
                fd.close()
                fd = None
        if self.authfds:
            for fd in self.authfds:
                fd.close()
                fd = None

        #if self._socket:
        #    self._socket.close()
        #    self._socket = None


class PYRADd(BaseSrv):

    def __init__(self, addresses, auth_port=1812, acct_port=1813):
        self.running = False
        self.srv = Pyrad(addresses=addresses, acctport=acct_port, authport=auth_port, dict=dictionary.Dictionary("dictionary"))

    def add_host(self, ip, name, key):
        self.srv.hosts['{}'.format(ip)] = server.RemoteHost(ip, key, name)

    def start(self):
        self.srv.Run()
        self.running = True

    def stop(self):
        self.srv._CloseSockets()
        if self.running:
            self.running = False


if __name__ == '__main__':
    SERVERIP = "192.168.0.50"
    CLIENTIP = "192.168.0.1"

    mysrv = PYRADd(addresses=[SERVERIP])
    mysrv.add_host(CLIENTIP, 'user1', b'test1234')
    mysrv.srv.Run()