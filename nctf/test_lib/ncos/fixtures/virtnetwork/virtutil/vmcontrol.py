"""
vmcontrol.py - Programmatic control of the virtual machine
"""

__copyright__ = """
Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties."""

import socket

#import sys

success = b'{"return": {}}\r\n'

# QMP json command strings (ref: http://stuff.mit.edu/afs/athena/course/6/6.828/src/qemu/QMP/qmp-commands.txt)
cmdstr =        '{ "execute": "qmp_capabilities" }'
netdev_delstr = '{ "execute": "netdev_del", "arguments": { "id": "%s" } }'
set_linkstr =   '{ "execute": "set_link", "arguments": { "name": "%s", "up": %s } }'

class Qmp(object):
    """ Control the QEMU virtual machine using QMP.
    Example usage: Qmp('./socket').set_link('virtio-net-pci.0', False)
    """
    
    def __init__(self, qmpsock):
        self.sock = connect(qmpsock)
        
        # Flush initial capabilities banner
        if self.sock:
            reply = self.sock.recv(1024)
                         
    def send_command_mode(self):
        """ Enter command mode """
        return check(send(self.sock, cmdstr))
    
    def send_command(self, command):
        """ Send a command """
        self.send_command_mode()
        return check(send(self.sock, command))
        
    def netdev_del(self, id):
        c = netdev_delstr % id
        return self.send_command(c)
    
    def set_link(self, name, up):
        sup = 'true' if up else 'false'
        c = set_linkstr % (name, sup)
        return self.send_command(c)      

# Helper methods
def connect(host):
    """ Connect to socket """
    s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    #s = socket(AF_INET, SOCK_STREAM)
    #h = host.split(':')

    if s:
        try:    
            s.settimeout(2)
            #s.connect((h[0],int(h[1])))
            s.connect(host)
            return s
        except (socket.error, socket.timeout):
            #print("connect failed: ", sys.exc_info())
            pass
    return None

def send(sock, payload):
    """ Send payload to socket """
    if sock:
        try:
            sock.send(bytes(payload, 'UTF-8'))
            reply = sock.recv(1024)
            return reply
        except socket.error:
            return None
    return None

def check(ret):
    """ Check return value """
    return ret == success
