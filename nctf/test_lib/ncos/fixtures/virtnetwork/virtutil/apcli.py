"""
Copyright (c) 2015-2017 CradlePoint, Inc. <www.cradlepoint.com>.  All rights
reserved.

This file contains confidential information of CradlePoint, Inc. and your use
of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of this
file is subject to civil and criminal penalties.

"""

import base64
import hashlib
import json
import logging
import os
import selectors
import socket
import struct
import threading
import time


_DUR_TO_SECONDS = 60


class ApDiscAgent(object):

    wakeup_addr = '127.0.0.1'
    wakeup_port = 9876

    def __init__(self, multicast_address='239.0.0.12', multicast_port=11100,
                 renew_timeout_minutes=1,
                 ip_addr=None, ifindex=socket.INADDR_ANY, secret=None,
                 roaming_info=None, mac=None, target_mac=None, target_ip=None,
                 product='test'):
        self.ifindex = ifindex
        self.ip_addr = ip_addr
        self.mac = mac
        self.target_mac = target_mac
        self.target_ip = target_ip
        self.multicast_address = multicast_address
        self.multicast_port = multicast_port
        self.product = product
        self.renew_timeout_minutes = renew_timeout_minutes
        self.roaming_info = roaming_info
        self.secret = secret

        self.selector = selectors.DefaultSelector()

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.IP_MULTICAST_LOOP, 0)
        group = struct.pack("=4s4si", socket.inet_aton(multicast_address),
                            socket.inet_aton(self.ip_addr), self.ifindex)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, group)
        self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF,
                             socket.inet_aton(self.ip_addr))
        self.sock.bind((multicast_address, multicast_port))
        self.selector.register(self.sock, selectors.EVENT_READ)

    def sign(self, data):
        """Get a signature for the data."""
        # Hash the combined salt, data and shared secret.
        salt = os.urandom(4)
        h = hashlib.sha256(salt + self.secret + data).hexdigest()
        # Append the salt to the signature.
        sig = h + ':' + base64.b64encode(salt).decode('utf-8')
        return sig

    def verify(self, sig, data):
        """Verify the signature for the data."""
        h, s = sig.split(':')
        salt = base64.b64decode(s)
        h2 = hashlib.sha256(salt + self.secret + data).hexdigest()
        return h == h2

    def read_message(self):
        """Read, verify and validate a message.

        Returns the message as a dict and the remote ip address as a
        pair. Returns None for the message if it failed to validate.

        """
        # Read the message.
        data, addr = self.sock.recvfrom(1024)  # XXX: can this raise?
        msg = json.loads(data.decode())

        # Validate it has the minimum required keys.
        for k in ('sig', 'mac', 'msg'):
            assert k in msg

        # Verify the signature.
        sig = msg.pop('sig')
        assert self.verify(sig, msg['mac'].encode())
        return msg, addr

    def get_message(self, timeout=3):
        """Wait for a message not from ourselves."""
        while True:
            results = self.selector.select(timeout=timeout)
            if not results:
                return None
            msg, addr = self.read_message()
            if addr[0] != self.ip_addr:
                return msg

    def send_message(self, msg, payload=None):
        """Send a discovery message."""
        payload = payload or {}
        payload.update({
            'sig': self.sign(self.mac.encode()),
            'mac': self.mac,
            'msg': msg,
        })
        if self.target_mac and self.target_ip:
            payload.update({
                'target_mac': self.target_mac,
                'target_ip': self.target_ip
            })
        data = json.dumps(payload).encode('utf-8')
        self.sock.sendto(data, (self.multicast_address, self.multicast_port))
        self.last_send_ts = time.time()

    def send_notice(self, msg):
        """Advertise our presence on the network.

        `msg` should be one of "announce" or "update".

        An "announce" message lets neighbors know that we just booted up and
        don't know about any of them yet, so they should all send an "update"
        message so we can learn about them.

        An "update" message lets neighbors know we are still alive. New
        neighbors also learn about us for the first time via this
        message. Neighbors do not need to respond to this message. If we are on
        more than one roaming list, we can send this to drop ourselves from one
        or more or change them.

        """
        payload = {
            'product': self.product,
            'duration': self.renew_timeout_minutes,
            'roam': self.roaming_info
            }
        self.send_message(msg, payload)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', default='192.168.0.5')
    parser.add_argument('--multicast_address', default='239.0.0.12')
    parser.add_argument('--key', default='deadbeef')
    parser.add_argument('--mac', default='00:11:22:33:44:55')
    parser.add_argument('--timeout', type=int, default=60, help='Seconds to wait for a reply')
    parser.add_argument('operation', choices=('announce', 'drop', 'await', 'update', 'establish'))

    # args for when apcli functions as a remote modem controller
    # ip address I want remote modem to assign to an interface (controller will connect to it)
    # iface mac address that sent "announce" (used to validate the multicasted "establish")
    parser.add_argument('--target_mac', default='00:00:00:00:00:00')
    parser.add_argument('--target_ip', default='192.168.0.5')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)

    r = [('74:d0:2b:29:ac:76', 30)]
    agent = ApDiscAgent(multicast_address=args.multicast_address,
                        ip_addr=args.host, roaming_info=r, mac=args.mac,
                        target_mac=args.target_mac, target_ip=args.target_ip,
                        secret=args.key.encode())
    msg = None
    _await = args.operation in ('announce', 'update', 'await')

    if args.operation in ('announce', 'update'):
        agent.send_notice(args.operation)
    elif args.operation in ['drop', 'establish']:
        agent.send_message(args.operation)

    if _await:
        msg = agent.get_message(timeout=args.timeout)

    if msg:
        print(json.dumps(msg))
