"""
utils.py 

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import socket
import subprocess
import shlex
import threading
import time
import fcntl
import struct
import array
import sys
import netifaces

def cat(fspec, title=None, header=True):
	if header:
		print('%s\n%s:' % (title or fspec, '-'*30))
	try:
		with open(fspec, 'r') as f:
			for l in f:
				print(l)
	except Exception as e:
		print('cat FAIL: %s' % e)
	if header:
		print('-'*30)


def dns(hostname, count=1, interval=0.25, timeout=1, quiet=False, query_func=None, **query_args):
	query_func = query_func or socket.gethostbyname
	answer = None
	n = 0
	start = time.time()
	while True:
		n += 1
		try:
			answer = query_func(hostname, **query_args)
		except socket.gaierror as e:
			print('%d: dns(%s) FAIL: %s' % (n, hostname, e))
		else:
			count -= 1
			if not quiet:
				print('%d: dns(%s) => %s' % (n, hostname, answer))
		if count > 0 and (time.time() - start) < (timeout - interval):
			time.sleep(interval)
			continue
		break
	return None if count > 0 else answer


def ping_msg(*args, msg='...', timeout=1, **kwargs):
	print("Waiting up to %0.1f seconds for %s" % (timeout, msg))
	st = int(time.time())
	rv = ping(*args, timeout=timeout, **kwargs)
	et = int(time.time())
	status = 'Success' if rv == 0 else 'Failed(%s)' % rv
	print('Waited %d seconds: %s' % (et - st, status))
	return rv


def ping(count, src_ip, dst_ip, interval=0.25, timeout=1, ttl=63, deadline=None, quiet=False, ver='ipv4'):
	cmdline = ['ping' if ver=='ipv4' else 'ping6', '-c', str(count), dst_ip, '-I', src_ip, '-i', str(interval)]
	if deadline:
		cmdline.extend(['-w', str(deadline)])
	# need timeout at the end to change it later
	cmdline.extend(['-W', '%0.1f' % timeout])
	while True:
		#print(cmdline)
		start = time.time()
		rv = subprocess.call(cmdline, stdout=subprocess.DEVNULL if quiet else None)
		timeout -= (time.time() - start + interval)
		if rv and timeout > 0:
			time.sleep(interval)
			if not quiet:
				print('ping: retry for %0.1f' % timeout)
			cmdline[-1] = '%0.1f' % timeout
			continue
		break
	#print('ping after loop(timeout = %s, rv = %s)' % (timeout, rv))
	return rv


def traceroute(addr, max_hops=30, wait_time=3):
	cmd = ['traceroute', '-n', '-m', str(max_hops), '-w', str(wait_time), addr]
	output = subprocess.check_output(cmd).decode().split('\n')
	return [line.split()[1] for i, line in enumerate(output) if len(line.split()) > 0 and i != 0]  #  creates a list of just the hops


def shcmd(cmd, shell=False):
	subprocess.call(shlex.split(cmd) if not shell else cmd, stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL, shell=shell)


def local_ip():
	"""Query local ip addresses"""
	return [_[1] for _ in all_interfaces() if _[0] != 'lo']

def skip_cls_method_unless(condition):
	"""
	Need to do this because setUpClass and tearDownClass are called even on skip
	https://github.com/nose-devs/nose/issues/946
	"""
	def wrapper(func):
		def check_function(*args, **kwargs):
			if condition:
				return func(*args, **kwargs)
		return check_function

	return wrapper

class ThreadHelper(object):

	def __init__(self, threads):
		self.threads = threads
		for thread in threads:
			# try to let the main process exit with running threads
			# daemon threads should be killed automatically on exit
			thread.daemon = True

	def start(self):
		for thread in self.threads:
			thread.start()

	def join(self, timeout=None):
		# force a timeout (override even if specified None)
		if timeout is None:
			timeout = 10

		# quick (short timeout) pass
		to = timeout / 10
		for thread in (t for t in self.threads if t.is_alive()):
			thread.join(timeout=to)

		# full (long timeout) pass
		for thread in (t for t in self.threads if t.is_alive()):
			thread.join(timeout=timeout)

		# now at least 110% of the time allotted has passed, handle any still living
		daemons = [t for t in self.threads if t.is_alive()]
		if daemons:
			import traceback
			for thread in daemons:
				traceback.print_stack()
				print('(thread_helper) FAILED TO JOIN %s' % thread)

			# arrange to kill stranded daemon threads after I'm supposed to be dead
			suicide = threading.Thread(target=self.harakiri, args=(daemons,), name='harakiri', daemon=True)
			suicide.start()

	@staticmethod
	def harakiri(threads):
		from os import getpid, kill
		from signal import SIGTERM, SIGINT, SIGHUP, SIGKILL
		from time import sleep
		# I am supposed to die, give me time...
		sleep(120)
		# if I wake up, evidently I did not die
		pid = getpid()
		threads = [t for t in threads if t.is_alive()]
		print("(thread_helper::harakiri) I'm alive, pid: %s w/%s threads:\n  %s" % (
			pid,
			len(threads),
			'\n  '.join(str(t) for t in threads),
		))
		# if any problem threads are still alive, I need to die
		if threads:
			for sig in (SIGTERM, SIGINT, SIGHUP, SIGKILL, SIGKILL):
				print('(thread_helper::harakiri) time to die: %s' % sig)
				kill(pid, sig)
				sleep(1)


def all_interfaces():
	ifaces = []
	for iface_name in netifaces.interfaces():
			try:
				for iface_addr in netifaces.ifaddresses(iface_name)[netifaces.AF_INET]:
					ifaces.append((iface_name, iface_addr['addr']))
			except KeyError:
				print("Couldn't find any ipv4 addresses")
			try:
				for iface_addr in netifaces.ifaddresses(iface_name)[netifaces.AF_INET6]:
					ifaces.append((iface_name, iface_addr['addr']))
			except KeyError:
				print("Couldn't find any ipv6 addresses")
	return ifaces
