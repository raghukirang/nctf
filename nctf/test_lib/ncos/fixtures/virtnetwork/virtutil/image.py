"""
image.py - util methods for disk generation

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import logging
import os

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.utils import shcmd
#PATH = (os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir))
#sys.path.append(PATH)


# offsets
P1OFF = 0x1be
SIGOFF1 = 0x1fe
SECSIZE = 512
STARTLBA = 2048

logger = logging.getLogger(__name__)
def dir2img(path, target, sz=2):

	# get the current filepath and make horrible assumptions about where some
	# other tools are relative to this path. Don't do this kids.
	cwd = os.path.dirname(__file__)
	#logger.debug("dir2img.cwd;{}".format(cwd))

	shcmd('dd if=/dev/zero of=partition1.img bs=1M count={}'.format(sz))
	shcmd('mkfs -t ext4 -F partition1.img')
	shcmd('sh {}/populate-extfs.sh {} partition1.img'.format(cwd, path))

	# Create mountable image with the partition in it
	shcmd('dd if=partition1.img of={} bs={} seek={}'.format(target, SECSIZE, STARTLBA))

	# Add boot sector
	with open(target, 'r+b') as f:
		# write partition entry #1
		f.seek(P1OFF)
		f.write(b'\x00')  # no status byte
		f.write(b'\x20')  # start head 32
		f.write(b'\x21')  # start sec 33
		f.write(b'\x00')  # start cyl 0
		f.write(b'\x83')  # FS type 83
		f.write(b'\x00')  # end head XXX
		f.write(b'\x00')  # end sec XXX
		f.write(b'\x00')  # end cyl XXX
		f.write((STARTLBA).to_bytes(4, byteorder='little'))  # start lba
		f.write((2048*sz).to_bytes(4, byteorder='little'))  # num sectors
		# write the boot signature
		f.seek(SIGOFF1)
		f.write(b'\x55\xAA')

	shcmd('rm partition1.img')
