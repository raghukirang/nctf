import ipaddress
import socket
import socketserver

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv, getSrvLogger
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.testsrv import UDPServerIPv6

logger = getSrvLogger('dnsd')
DNS_TYPE_A =  b'\x00\x01'
DNS_TYPE_AAAA = b'\x00\x1c'
class DNSd(BaseSrv):

	def __init__(self, ip, port=53, hosts=None, quiet=False, **kwargs):
		super().__init__(ip, port, **kwargs)
		if quiet:
			logger.setLevel("CRITICAL")
		self.hosts = hosts

	def _create_server(self):

		# closure to grab the ip we want to use
		class DNSd_handler(socketserver.BaseRequestHandler):
			def handle(instance):
				data = instance.request[0]
				s = instance.request[1]
				p = DNSQuery(data)
				domain = p.domain[:-1]
				logger.info('query for {} from {}'.format(domain, instance.client_address[0]))
				resps = [self.ip]
				if self.hosts and self.hosts.get(domain):
					resps = [self.hosts[domain]] if isinstance(self.hosts[domain], str) else self.hosts[domain]
				responded = False
				for resp in resps:
					family = ipaddress.ip_address(resp).version
					# respond with only with correct family
					if (p.type == DNS_TYPE_A and family == 4) or (p.type == DNS_TYPE_AAAA and family == 6):
						logger.info('   responding with {}'.format(resp))
						responded = True
						s.sendto(p.respuesta(resp, family), instance.client_address)
				if not responded:
					# the 'correct' behavior is reply with no answer if you don't have one vs. timing out
					logger.info('   no answer, nak')
					s.sendto(p.nak(), instance.client_address)

		udpserver = socketserver.UDPServer if self.family == 'inet' else UDPServerIPv6
		return udpserver((self.ip, self.port), DNSd_handler)


# DNSQuery class from http://code.activestate.com/recipes/491264-mini-fake-dns-server/
class DNSQuery(object):

	def __init__(self, data):
		self.domain = ''

		op = (data[2] >> 3) & 0xf  # opcode
		if op == 0:                # we only support query
			ini = 12                 # starts at byte 12
			lon = data[ini]
			while lon != 0:
				self.domain += data[ini+1:ini+lon+1].decode()+'.'
				ini += lon+1
				lon = data[ini]
			# trim any extra data
			self.data = data[:ini+5]
			self.type = self.data[-4:-2]  # A or AAAA

	def respuesta(self, ip, family=None):
		family = family or ipaddress.ip_address(ip).version
		packet = b''
		if self.domain:
			packet += self.data[:2] + b'\x81\x80'
			packet += self.data[4:6] + self.data[4:6] + b'\x00\x00\x00\x00'  # Questions and Answers Counts
			packet += self.data[12:]                                         # Original Domain Name Question
			packet += b'\xc0\x0c'                                            # Pointer to domain name
			packet += DNS_TYPE_A if family == 4 else DNS_TYPE_AAAA           # response type
			packet += b'\x00\x01\x00\x00\x00\x3c'                            # 0x0001 + ttl
			packet += b'\x00\x04' if family == 4 else b'\x00\x10'            # resource data length
			packet += socket.inet_pton(socket.AF_INET if family == 4 else socket.AF_INET6, ip)
		return packet

	def nak(self):
		packet = b''
		if self.domain:
			packet += self.data[:2] + b'\x81\x80'
			packet += self.data[4:6] + self.data[4:6] + b'\x00\x00\x00\x00'  # Questions and Answers Counts
			packet += self.data[12:]                                         # Original Domain Name Question
			packet += b'\xc0\x0c'                                            # Pointer to domain name
		return packet

if __name__ == '__main__':
	dnsd = DNSd('192.168.0.5', 8053)
	server = dnsd._create_server()
	try:
		server.serve_forever()
	except KeyboardInterrupt:
		pass
