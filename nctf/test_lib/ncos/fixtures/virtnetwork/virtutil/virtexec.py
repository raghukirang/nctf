"""
virtexec.py - Utilities for executing python code in a namespace

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.setns import netns

def virtexec(ns, f, *ve_args, args=[], kwargs={}, **ve_kwargs):
	""" Execute a function f(args, kwargs) in the context of
	namespace ns """

	args = args or ve_args
	kwargs = kwargs or ve_kwargs

	result = None
	with netns(ns.nsname, etc_dir=ns.etc_dir):
		try:
			result = f(*args, **kwargs)
		except Exception as e:
			result = None
			print('nsExecFunc(%s(%s, %s) in %s) exception: %s' % (f, args, kwargs, ns.nsname, e))
	return result
