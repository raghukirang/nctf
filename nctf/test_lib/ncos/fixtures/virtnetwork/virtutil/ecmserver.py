import os
import socketserver
import ssl

from nctf.test_lib.ncos.fixtures.virtnetwork.cpclient.client.ecm.stream import StreamTranscoderV1
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv, getSrvLogger

logger = getSrvLogger('ecmd')

class ECMStreamServer(socketserver.TCPServer):

	def get_request(self):
		newsocket, fromaddr = self.socket.accept()
		path = os.path.dirname(os.path.realpath(__file__)) + '/'
		connstream = ssl.wrap_socket(newsocket,
                                     server_side=True,
                                     certfile = path + 'ecm_cert.pem',
                                     keyfile = path + 'ecm_key.pem',
                                     ssl_version = ssl.PROTOCOL_SSLv23)
		return connstream, fromaddr

class ECMHandler(socketserver.BaseRequestHandler, StreamTranscoderV1):

	def handle(self):
		ver = self.request.recv(1)[0]
		if ver != 1:
			logger.warning('unhandled stream protocol version {}'.format(ver))
			return

		while True:
			data = self.request.recv(4096)
			if len(data) == 0:
				return
			try:
				size, cmd_id = self.header_decode(data[0:9])
				payload = self.decode(data[9:])
				if payload['command'] == 'check_activation':
					msg = {
						'success': False,
						'exception': 'notregistered',
						'message': 'Client is not registered'
					}
					reply_data = self.encode(msg)
					reply_head = self.header_encode(len(reply_data), cmd_id)
					logger.info('unmanaged device at {} with secrethash {}'.format(self.client_address[0], payload['args']['secrethash']))
					self.request.sendall(reply_head + reply_data)
					return
				else:
					logger.warning('unhandled command: {}'.format(payload))
			except Exception as e:
				logger.critical('Exception e: {}  data: {}'.format(e, data))


class ECMd(BaseSrv):

	def __init__(self, ip, port=8001, **kwargs):
		super().__init__(ip, port, **kwargs)

	def _create_server(self):
		return ECMStreamServer((self.ip, self.port), ECMHandler)


if __name__ == '__main__':
	ecmd = ECMd('192.168.7.1')
	class fakens(object):
		nsname = 'ns-rtr0-0'
	ecmd.start(fakens())
	print('hit any key to stop server')
	input()
	ecmd.stop()
