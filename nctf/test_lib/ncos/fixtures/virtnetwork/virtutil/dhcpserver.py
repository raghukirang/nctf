import ipaddress
import logging
from tempfile import NamedTemporaryFile

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.procmon import ProcessMonitor

logger = logging.getLogger('virtutil.DHCPd')


class DHCPd(object):
	"""DHCP server powered by dnsmasq"""
	DHCP_SERVER_CMD = ('dnsmasq', '--keep-in-foreground', '--dhcp-authoritative', '--port=0')

	def __init__(self, ns=None):
		self.ns = ns
		self.dnsmasq = None
		self.ntf = None
		self.dnsmasq_cmd = list(self.DHCP_SERVER_CMD)
		self.count = 0

	def add_dhcp_pool(self, ip_network, nameserver=None):
		ipi = ipaddress.ip_interface(ip_network)
		# todo support ipv6
		if ipi.version != 4:
			logger.warning('ip version of %s is not 4' % ipi)
			return
		self.count += 1
		if not self.dnsmasq:
			self.start()

		ipn = ipi.network
		ipi = ipi.ip
		self.dnsmasq_cmd.append('--listen-address={}'.format(ipi))
		self.dnsmasq_cmd.append(
			'--dhcp-range=set:{},{},{}'.format(self.count, ipn.network_address + 1, ipn.broadcast_address - 1))
		self.dnsmasq_cmd.append('--dhcp-option=tag:{},3,{}'.format(self.count, ipi))
		nameserver = nameserver if nameserver else ipi
		self.dnsmasq_cmd.append('--dhcp-option=tag:{},6,{}'.format(self.count, nameserver))
		self._start()

	def add_reservations(self, reservations: dict):
		if not self.dnsmasq:
			self.start()
		for address, ip in (reservations or {}).items():
				self.dnsmasq_cmd.append('--dhcp-host={},{}'.format(address, ip))
		self._start()

	def _start(self):
		self.dnsmasq.setCmd(self.dnsmasq_cmd)
		if self.dnsmasq.STARTED:
			self.dnsmasq.stop()
		self.dnsmasq.start()

	def start(self):
		self.ntf = NamedTemporaryFile()
		self.dnsmasq_cmd.extend(['-C', self.ntf.name, '--dhcp-leasefile={}'.format(self.ntf.name)])
		self.dnsmasq = ProcessMonitor(cmd=self.dnsmasq_cmd, ns=self.ns, autoRestart=False)

	def stop(self):
		if self.dnsmasq:
			self.dnsmasq.stop()
			self.dnsmasq = None
			self.ntf = None
			self.dnsmasq_cmd = list(self.DHCP_SERVER_CMD)
