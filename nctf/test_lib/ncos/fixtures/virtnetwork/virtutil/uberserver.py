# a class to run all 'interesting' services on an IP

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.dnsserver import DNSd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.ecmserver import ECMd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.ntpserver import NTPd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.httpserver import HTTPd


class UBERd(object):

	def __init__(self, ip, **kwargs):

		# DNS
		self.dnsd = DNSd(ip)

		# NTP
		self.ntpd = NTPd(ip)

		# ECM
		self.ecmd = ECMd(ip)

		# FW Upgrade Check
		self.httpd = HTTPd(ip)

	def start(self, ns):
		self.dnsd.start(ns)
		self.ntpd.start(ns)
		self.ecmd.start(ns)
		self.httpd.start(ns)

	def stop(self):
		self.httpd.stop()
		self.ecmd.stop()
		self.ntpd.stop()
		self.dnsd.stop()
