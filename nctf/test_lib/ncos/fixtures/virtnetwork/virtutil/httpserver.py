import http.server
import ipaddress
import json
import os
import socket
import ssl
import time
import urllib.request

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import getSrvLogger

try:
    from nctf.test_lib.core.asset_handler.fixture import AssetHandler
except ImportError:
    AssetHandler = None

logger = getSrvLogger('httpd')


class WebrootHandler(http.server.BaseHTTPRequestHandler):
    def do_POST(self):
        length = int(self.headers['Content-Length'])
        post_data = urllib.parse.parse_qs(self.rfile.read(length).decode('utf-8'))
        # TODO use the xml below as a template for other sites. Possiblly pass custom confience & reputation scores via handler?
        # Webroot reply for amazon.com
        reply = "<?BrightCloud version=bcap/1.1?><bcap><seqnum>1</seqnum><response><status>200</status><statusmsg>OK</statusmsg><item><uri>amazon.com</uri><lcp>amazon.com</lcp><match>amazon.com</match><categories><cat><catid>7</catid><conf>93</conf></cat></categories><bcri>81</bcri><a1cat>0</a1cat><rtap>1</rtap></item></response></bcap>".encode(
        )
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.send_header('Content-Length', len(reply))
        self.end_headers()
        self.wfile.write(reply)


class DummyDataHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        reply = json.dumps({'reply': 'very yes'}).encode()

        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.send_header('Content-Length', len(reply))
        self.end_headers()

        self.wfile.write(reply)


class CRLHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        parent = self.server.parent
        if parent.crl_file:
            size = os.path.getsize(parent.crl_file)

            self.send_response(200)
            self.send_header('Content-type', 'application/pkix-crl')
            self.send_header('Content-Length', size)
            self.end_headers()

            with open(parent.crl_file, 'rb') as f:
                self.wfile.write(f.read())


class EicarHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        reply = "X5O!P%@AP[4\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*".encode()

        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.send_header('Content-Length', len(reply))
        self.end_headers()

        self.wfile.write(reply)


class FacebookHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        reply = "www.facebook.com facebook post messenger marketplace".encode()

        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.send_header('Content-Length', len(reply) * 50)
        self.end_headers()

        for _i in range(50):
            time.sleep(2)
            self.wfile.write(reply)


class FWRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):

        parent = self.server.parent

        if self.path.startswith('/files/uploads/3200v/firmware-3200v.json'):

            reply = json.dumps({
                'product_name': '3200v',
                'major_version': 5,
                'minor_version': 2,
                'patch_version': 0,
                'force_upgrade': False,
                'eligible': True,
                'url': 'http://nowheresville.usa/youwish.bin'
            }).encode()

            self.send_response(200)
            self.send_header('Content-type', 'text/plain')
            self.send_header('Content-Length', len(reply))
            self.end_headers()

            self.wfile.write(reply)

        elif parent.handle_ips and '/files/uploads/UTMv3/rules.json' in self.path:

            reply = json.dumps(parent.ips_json).encode()

            self.send_response(200)
            self.send_header('Content-type', 'text/plain')
            self.send_header('Content-Length', len(reply))
            self.end_headers()

            self.wfile.write(reply)

        elif parent.handle_ips and '/rules_' in self.path:

            if AssetHandler:
                tmp_file = parent.asset_handler.get_asset_file(parent.ips_rules_file, 'rb')
                size = os.fstat(tmp_file.fileno()).st_size
            else:
                # send whatever we want here
                size = os.path.getsize(parent.ips_rules_file)

            self.send_response(200)
            self.send_header('Content-type', 'application/octet-stream')
            self.send_header('Content-Length', size)
            self.end_headers()

            if AssetHandler:
                self.wfile.write(tmp_file.read())
            else:
                with open(parent.ips_rules_file, 'rb') as f:
                    self.wfile.write(f.read())

        else:
            logger.warning('unhandled path {}'.format(self.path))
            self.send_response(404)
            self.end_headers()

    def log_request(self, code='-', size='-'):
        logger.info('%s - "%s" %s %s', self.address_string(), self.requestline, str(code), str(size))


class Handler(object):
    types = {
        'dummy': DummyDataHandler,
        'fwrequest': FWRequestHandler,
        'webroot': WebrootHandler,
        'crl': CRLHandler,
        'eicar': EicarHandler,
        'facebook': FacebookHandler
    }

    @classmethod
    def factory(cls, handler_type):
        try:
            return cls.types[handler_type]
        except KeyError as e:
            logger.warning('%s. Choosing FWRequestHandler', e)
            return FWRequestHandler


class HTTPServerV6(http.server.HTTPServer):
    address_family = socket.AF_INET6


class HTTPd(BaseSrv):
    def __init__(self, ip, port=80, handler='fwrequest', handle_ips=False, secure=False, crl_path=None, **kwargs):
        family = 'inet' if ipaddress.ip_interface(ip).version == 4 else 'inet6'
        super().__init__(ip, port, family, **kwargs)
        self.secure = secure
        self.handler = Handler.factory(handler)
        self.handle_ips = handle_ips
        if handle_ips:
            self.init_ips()
        self.init_crl(crl_path)

    def _create_server(self):
        ip_server = http.server.HTTPServer if ipaddress.ip_interface(self.ip).version == 4 else HTTPServerV6
        server = ip_server((self.ip, self.port), self.handler)
        if self.secure:
            path = os.path.dirname(os.path.realpath(__file__))  # .pem files should be in the same folder as this file
            server.socket = ssl.wrap_socket(
                server.socket,
                certfile=os.path.join(path, 'ecm_cert.pem'),  # Use the ecm pem becuase it's available
                keyfile=os.path.join(path, 'ecm_key.pem'),
                ssl_version=ssl.PROTOCOL_SSLv23)
        setattr(server, 'parent', self)
        return server

    # this should be called if you expect the IPS calls to work
    def init_ips(self):
        if AssetHandler:
            test_path = os.getcwd()[1:].split('/')
            tests_in_cwd_str = '/..' * (len(test_path) - 1 - test_path.index("coconut_testing"))
            self.asset_handler = AssetHandler(
                test_dir=os.getcwd(), framework_assets_dir=os.getcwd() + tests_in_cwd_str + '/framework/assets/')
            self.ips_json = self.asset_handler.get_asset_json('rules.json')
            self.ips_rules_file = os.path.basename(self.ips_json['url'])
            self.handle_ips = True
        else:
            tests_in_cwd_str = '/..' if '/tests' in os.getcwd() else ''  #Fix directory path while running interactive
            basepath = os.getcwd() + tests_in_cwd_str + '/framework/assets/ips_defs/'
            self.ips_json_file = basepath + 'rules.json'

            try:
                with open(self.ips_json_file, 'r') as f:
                    self.ips_json = json.loads(f.read())
                self.ips_rules_file = basepath + os.path.basename(self.ips_json['url'])
            except FileNotFoundError:
                logger.info('attempting to download latest IPS rules json')
                self.ips_json = json.loads(
                    urllib.request.urlopen('http://www.cradlepoint.com/files/uploads/UTMv3/rules.json').read()
                    .decode())
                with open(self.ips_json_file, 'w') as f:
                    f.write(json.dumps(self.ips_json, indent=2))
                self.ips_rules_file = basepath + os.path.basename(self.ips_json['url'])

            if not os.path.exists(self.ips_rules_file):
                try:
                    urllib.request.urlretrieve(self.ips_json['url'], self.ips_rules_file)
                except:
                    pass

            if os.path.exists(self.ips_rules_file):
                self.handle_ips = True
                logger.info('Initialized IPS rules: ' + os.path.basename(self.ips_rules_file))
            else:
                logger.warning('Unable to initialize IPS rules')

    def init_crl(self, crl_path):
        if crl_path:
            basepath = os.path.dirname(os.path.realpath(__file__)) + '/../../../../../../../../../../'
            self.crl_file = basepath + crl_path
        else:
            self.crl_file = None


if __name__ == '__main__':
    httpd = HTTPd('192.168.7.1')

    class fakens(object):
        nsname = 'ns-rtr0-0'

    httpd.start(fakens())
    print('hit any key to stop server')
    input()
    httpd.stop()
