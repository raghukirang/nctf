"""
procmon.py

TODO: This is ripped out of service_manager/__init__.py in order to reduce
turmoil during the QA cycle. It is also extended to allow running a process
in a namespace.
"""
import errno
import logging
import shlex
import threading
import time
from subprocess import Popen


class ProcessMonitor(object):
	"""Class used to start and manage a process (daemons are not supported)
	The goal is to provide a easy to use and deterministic control over any process
	even if it is misbehaving."""

	DEFERRED = 'DEFERRED'
	STARTING = 'STARTING'
	STARTED = 'STARTED'
	STOPPING = 'STOPPING'
	STOPPED = 'STOPPED'

	null = open('/dev/null', 'wb')

	def __init__(self, cmd=None, autoRestart=True, onError=None, onSuccess=None, ns=None, env=None, defer=0, stopTimeout=2):
		"""
		cmd: program to execute (string or list)
		autoRestart: restart program when it dies checking respawn limits too; like inittab respawn
		onError: callback if error
		onSuccess: if autoRestart is False, and this is set, we call when program returns
		stopTimeout: seconds before we consider a stopping process hung and send SIGKILL
		"""

		self.p = None
		self.state = self.STOPPED
		self.ns = ns
		self.setCmd(cmd)
		self.autoRestart = autoRestart
		self.onError = onError
		self.onSuccess = onSuccess
		self.env = env
		self.stopTimeout = stopTimeout
		self.monitorThread = None
		self.restartThread = None
		self.error = None
		self.defer = defer
		self.delay = 1
		self.maxDelay = 30
		self.monitorEvt = threading.Event()
		self.lock = threading.Lock()

	def setCmd(self, cmd):

		if isinstance(cmd, str):
			cmd = shlex.split(cmd)

		if self.ns and self.ns.nsname != '1':
			netns_cmd = ['ip', 'netns', 'exec', self.ns.nsname]
			self.cmd = netns_cmd + cmd
		else:
			self.cmd = cmd

		logname = cmd[0].split('/')[-1] if cmd else '_unset_'
		self.logger = logging.getLogger('psmon:%s' % logname)

	def _call(self, fn, *args, **kwargs):
		"""Run fn() but only after verifing it's callable"""

		if hasattr(fn, '__call__'):
			return fn(*args, **kwargs)

	def _completionMonitor(self, p):
		"""Call onError or onSuccess when program finishes"""

		retcode = p.wait()  # block until process exits
		with self.lock:
			self.logger.debug("Process (%s[%d]) exited with code: %d", self.cmd[0], p.pid, retcode)
			self.state = self.STOPPED

		if retcode == 0:
			self._call(self.onSuccess, self, retcode=retcode)
		else:
			self._call(self.onError, self, retcode=retcode)

	def _restartMonitor(self, p):
		"""Handle autoRestart functionality"""

		retcode = p.wait()  # block until process exits
		self.logger.debug("Process (%s[%d]) exited with code: %d", self.cmd[0], p.pid, retcode)
		if self.state != self.STARTED or p.pid != self.p.pid:
			return

		self.delay = min(self.maxDelay, self.delay * 1.1)
		self.logger.debug("Restarting (%s) in %f seconds", self.cmd[0], self.delay)

		if self.monitorEvt.wait(self.delay):
			return

		with self.lock:
			if self.state != self.STARTED or p.pid != self.p.pid:
				return
			self._start()

	def start(self):

		# Wait until stop is finished
		if self.state == self.STOPPING and self.monitorThread:
			self.monitorThread.join()

		with self.lock:
			self.state = self.DEFERRED

			# kick process monitor
			self.monitorEvt.set()

			if self.defer:
				time.sleep(self.defer)

			self.delay = 1
			self._start()

	# called with self.lock already acquired
	def _start(self):

		self.state = self.STARTING
		self.p = Popen(self.cmd)

		self.logger.debug("Started process %d: %s", self.p.pid, self.cmd)
		self.state = self.STARTED

		# don't use looped task because of the uneeded overhead, e.g. scheduling
		if self.autoRestart:
			self.monitorEvt.clear()

			self.restartThread = threading.Thread(target=self._restartMonitor, args=(self.p,))
			self.restartThread.daemon = True
			self.restartThread.start()

		elif self.onSuccess or self.onError:
			self.monitorEvt.clear()
			self.monitorThread = threading.Thread(target=self._completionMonitor, args=(self.p,))
			self.monitorThread.daemon = True
			self.monitorThread.start()

	def _terminate(self, forcekill=False):
		if forcekill:
			self.logger.warning("Sending SIGKILL to hung process %d: %s", self.p.pid, self.cmd[0])
			# sends sigkill (-9) which removes the pid from the process table
			killfunc = self.p.kill
		else:
			# sends sigint (-15)
			killfunc = self.p.terminate
		try:
			killfunc()
		except OSError as err:
			# ignore "No Such Process"
			if (err.errno == errno.ESRCH):
				self.state = self.STOPPED
			else:
				self.logger.error("ProcessMonitor exception: %s %s", self, self.state, exc_info=True)

		if not forcekill:
			start_timer = int(time.time())
			while self.p.poll() is None and self.state != self.STOPPED:
				time.sleep(1/10)
				if int(time.time()) - start_timer > self.stopTimeout:
					self._terminate(True)

	def stop(self, forcekill=False, wait=False):

		with self.lock:

			if self.state != self.STARTED:
				self.logger.debug("tried to stop an unstarted process: %s %s", self.cmd[0], self.state)
				if not forcekill:
					return

			self.state = self.STOPPING
			self.logger.debug("Stopping process %d: %s", self.p.pid, self.cmd[0])

			# kick process monitor
			self.monitorEvt.set()

			self._terminate(forcekill)

			monitorThread = self.monitorThread
			if monitorThread:
				self.logger.debug("Process pending stop %d: %s", self.p.pid, self.cmd[0])
			else:
				self.state = self.STOPPED

		# allow completion monitor to set the state to stopped
		if monitorThread and wait:
			monitorThread.join()

		self.logger.debug("Stopped process %d: %s", self.p.pid, self.cmd[0])

	def restart(self):

		self.logger.debug("Restarting process %d: %s", self.p.pid, self.cmd[0])

		self.stop()
		self.start()

		self.logger.debug("Restarted process %d: %s", self.p.pid, self.cmd[0])
