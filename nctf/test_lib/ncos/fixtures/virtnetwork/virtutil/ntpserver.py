# Based on https://github.com/limifly/ntpserver
# License - Unknown
# converted to py3k via 2to3
# modify to use multiprocessing to get deal with net namespaces 
# rewrite entirely only keeping NTP packet stuff around

import datetime
import socketserver
import struct
import time
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.basesrv import BaseSrv, getSrvLogger

logger = getSrvLogger('ntpd')

class NTPd(BaseSrv):

	def __init__(self, ip, port=123, preseed_time=None, **kwargs):
		super().__init__(ip, port, **kwargs)
		self.preseed_time = preseed_time
		if self.preseed_time:
			logger.info("Preseeding time to %s"%str(system_to_ntp_time(preseed_time)))

	def _create_server(self):
		server = socketserver.UDPServer((self.ip, self.port), self.NTPd_handler)
		setattr(server, 'parent', self)
		return server

	class NTPd_handler(socketserver.BaseRequestHandler):

		def handle(self):
			parent = self.server.parent
			data = self.request[0]
			socket = self.request[1]
			if parent.preseed_time:
				rxtime = system_to_ntp_time(parent.preseed_time)
			else:
				rxtime = system_to_ntp_time(time.time())
			rxpkt = NTPPacket()
			rxpkt.from_data(data)
			timeh,timel = rxpkt.GetTxTimeStamp()
			txpkt = NTPPacket(version=3,mode=4)
			txpkt.stratum = 2
			txpkt.poll = 10
			txpkt.ref_timestamp = rxtime-5
			txpkt.SetOriginTimeStamp(timeh,timel)
			txpkt.recv_timestamp = rxtime
			if parent.preseed_time:
				# Added 5 to make sure we are are at a higher time than wehnthe packet came in
				txpkt.tx_timestamp = system_to_ntp_time(parent.preseed_time + 5)
			else:
				txpkt.tx_timestamp = system_to_ntp_time(time.time())
			socket.sendto(txpkt.to_data(),self.client_address)
			logger.info('query from {}'.format(self.client_address[0]))


def system_to_ntp_time(timestamp):
	"""Convert a system time to a NTP time.

	Parameters:
	timestamp -- timestamp in system time

	Returns:
	corresponding NTP time
	"""
	_SYSTEM_EPOCH = datetime.date(*time.gmtime(0)[0:3])
	_NTP_EPOCH = datetime.date(1900, 1, 1)
	NTP_DELTA = (_SYSTEM_EPOCH - _NTP_EPOCH).days * 24 * 3600

	return timestamp + NTP_DELTA

def _to_frac(timestamp, n=32):
	"""Return the fractional part of a timestamp.

	Parameters:
	timestamp -- NTP timestamp
	n		 -- number of bits of the fractional part

	Retuns:
	fractional part
	"""
	return int(abs(timestamp - int(timestamp)) * 2**n)

def _to_time(integ, frac, n=32):
	"""Return a timestamp from an integral and fractional part.

	Parameters:
	integ -- integral part
	frac  -- fractional part
	n	 -- number of bits of the fractional part

	Retuns:
	timestamp
	"""
	return integ + float(frac)/2**n	


class NTPException(Exception):
	"""Exception raised by this module."""
	pass

class NTPPacket(object):
	"""NTP packet class.

	This represents an NTP packet.
	"""
	
	_PACKET_FORMAT = "!B B B b 11I"

	def __init__(self, version=2, mode=3, tx_timestamp=0):
		"""Constructor.

		Parameters:
		version	  -- NTP version
		mode		 -- packet mode (client, server)
		tx_timestamp -- packet transmit timestamp
		"""
		self.leap = 0
		self.version = version
		self.mode = mode
		self.stratum = 0
		self.poll = 0
		self.precision = 0
		self.root_delay = 0
		self.root_dispersion = 0
		self.ref_id = 0
		self.ref_timestamp = 0
		self.orig_timestamp = 0
		self.orig_timestamp_high = 0
		self.orig_timestamp_low = 0
		self.recv_timestamp = 0
		self.tx_timestamp = tx_timestamp
		self.tx_timestamp_high = 0
		self.tx_timestamp_low = 0
		
	def to_data(self):
		"""Convert this NTPPacket to a buffer that can be sent over a socket.

		Returns:
		buffer representing this packet

		Raises:
		NTPException -- in case of invalid field
		"""
		try:
			packed = struct.pack(NTPPacket._PACKET_FORMAT,
				(self.leap << 6 | self.version << 3 | self.mode),
				self.stratum,
				self.poll,
				self.precision,
				int(self.root_delay) << 16 | _to_frac(self.root_delay, 16),
				int(self.root_dispersion) << 16 |
				_to_frac(self.root_dispersion, 16),
				self.ref_id,
				int(self.ref_timestamp),
				_to_frac(self.ref_timestamp),
				#Change by lichen, avoid loss of precision
				self.orig_timestamp_high,
				self.orig_timestamp_low,
				int(self.recv_timestamp),
				_to_frac(self.recv_timestamp),
				int(self.tx_timestamp),
				_to_frac(self.tx_timestamp))
		except struct.error:
			raise NTPException("Invalid NTP packet fields.")
		return packed

	def from_data(self, data):
		"""Populate this instance from a NTP packet payload received from
		the network.

		Parameters:
		data -- buffer payload

		Raises:
		NTPException -- in case of invalid packet format
		"""
		try:
			unpacked = struct.unpack(NTPPacket._PACKET_FORMAT,
					data[0:struct.calcsize(NTPPacket._PACKET_FORMAT)])
		except struct.error:
			raise NTPException("Invalid NTP packet.")

		self.leap = unpacked[0] >> 6 & 0x3
		self.version = unpacked[0] >> 3 & 0x7
		self.mode = unpacked[0] & 0x7
		self.stratum = unpacked[1]
		self.poll = unpacked[2]
		self.precision = unpacked[3]
		self.root_delay = float(unpacked[4])/2**16
		self.root_dispersion = float(unpacked[5])/2**16
		self.ref_id = unpacked[6]
		self.ref_timestamp = _to_time(unpacked[7], unpacked[8])
		self.orig_timestamp = _to_time(unpacked[9], unpacked[10])
		self.orig_timestamp_high = unpacked[9]
		self.orig_timestamp_low = unpacked[10]
		self.recv_timestamp = _to_time(unpacked[11], unpacked[12])
		self.tx_timestamp = _to_time(unpacked[13], unpacked[14])
		self.tx_timestamp_high = unpacked[13]
		self.tx_timestamp_low = unpacked[14]

	def GetTxTimeStamp(self):
		return (self.tx_timestamp_high,self.tx_timestamp_low)

	def SetOriginTimeStamp(self,high,low):
		self.orig_timestamp_high = high
		self.orig_timestamp_low = low
