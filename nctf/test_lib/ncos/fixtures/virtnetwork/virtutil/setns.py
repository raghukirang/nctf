"""
setns.py - Thread-Aware Context Managed Net Namespace Management

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.


"""

import ctypes
import errno
import logging
import os
import threading

netns_lock = threading.Lock()

# from ipnetns.c
NETNS_RUN_DIR = "/var/run/netns"
NETNS_ETC_DIR = "/etc/netns"

# magic from linux/sched.h
CLONE_NEWNS = 0x00020000
CLONE_NEWNET = 0x40000000

# magic from linux/fs.h
MS_SLAVE = (1<<19)
MS_BIND = 4096
MS_REC = 16384

# magic from sys/mount.h
MNT_DETACH = 0x00000002

# load libc (use find_library?)
_libc = ctypes.CDLL("libc.so.6", use_errno=True)

# int setns(int fd, int nstype);
_setns = _libc.setns
_setns.restype = ctypes.c_int
_setns.argtypes = (ctypes.c_int, ctypes.c_int)

# int unshare(int flags);
_unshare = _libc.unshare
_unshare.restype = ctypes.c_int
_unshare.argtypes = (ctypes.c_int,)

# int mount(const char *source, const char *target,
#           const char *filesystemtype, unsigned long mountflags,
#           const void *data);
_mount = _libc.mount
_mount.restype = ctypes.c_int
_mount.argtypes = (ctypes.c_char_p, ctypes.c_char_p,
		   ctypes.c_char_p, ctypes.c_ulong,
		   ctypes.c_char_p)

# int umount2(const char *target, int flags);
_umount2 = _libc.umount2
_umount2.restype = ctypes.c_int
_umount2.argtypes = (ctypes.c_char_p, ctypes.c_int)



logger = logging.getLogger('setns')
logger.setLevel(logging.WARNING)
ch = logging.StreamHandler()
ch.setFormatter(logging.Formatter('%(processName)s[%(process)d]%(threadName)s[%(thread)d] %(message)s'))
logger.addHandler(ch)

def get_tid():
	"""
	Returns the caller's thread/task id.  See man procfs for more information

	This is the libc syscall 186, which corresponds to the x86 32 bit version of SYS_gettid().  The 32 bit version is chosen because it will work with the x64 bit version, but x64 won't work on 32 bit systems.  If, one day, we change decide to use a MIPS or ARM architecture for the tests, we'll need to change this to the syscall number for gettid on those platforms.
	"""
	return _libc.syscall(186)

def bind_etc(name, etc_dir=NETNS_ETC_DIR):
	if not etc_dir:
		return
	etc_netns_path = '{}/{}'.format(etc_dir, name)
	logger.info("etc_netns_path %s", etc_netns_path)
	try:
		for entry in os.listdir(etc_netns_path):
			netns_name = '{}/{}'.format(etc_netns_path, entry)
			etc_name = '/etc/{}'.format(entry)
			logger.info('bind mounting %s -> %s ',netns_name, etc_name )
			if _mount(netns_name.encode(), etc_name.encode(), b"none", MS_BIND, None) != 0:
				e = ctypes.get_errno()
				logger.warning('bind mounting %s-> %s failed with %s : %s', netns_name, etc_name, e, errno.errorcode[e])
	except FileNotFoundError:
		pass
		#this is okay, just means this netns DOEsn't have an /etc/netns directory

def unbind_etc(name, etc_dir=NETNS_ETC_DIR):
	if not etc_dir:
		return
	etc_netns_path = '{}/{}'.format(etc_dir, name)
	try:
		for entry in os.listdir(etc_netns_path):
			etc_name = '/etc/{}'.format(entry)
			netns_name = '{}/{}'.format(etc_netns_path, entry)
			logger.info('unbind mounting %s', etc_name)
			if _umount2(etc_name.encode(), MNT_DETACH) != 0:
					e = ctypes.get_errno()
					logger.warning('unbinding etc_name %s in %s failed with %s : %s', etc_name, netns_name, e, errno.errorcode[e])
	except FileNotFoundError:
		logger.debug('etc dir %s for netns %s not found', etc_dir, name)

def get_task_status():
	task_status = {}
	with open('/proc/self/status') as f:
		for line in f:
			(key, val) = line.split(':')
			val = val.strip()
			task_status[key] = val
	return task_status

def get_netns_fd(nns=None):
	"""
	Opens and then returns a file descriptor associated with a netns.

	"""
	logger.debug('getting nns fd for %s', (nns or ('this thread ' + str(get_tid()))))
	fd = None
	if not nns:
		mytid = get_tid()
		procfn = '/proc/self/task/{}/ns/net'.format(mytid)
	else:
		try:
			procfn = '/proc/{}/ns/net'.format(int(nns))
		except ValueError:
			procfn = '{}/{}'.format(NETNS_RUN_DIR, str(nns))

	try:
		logger.debug('opening %s', procfn)
		fd = os.open(procfn, os.O_RDONLY)
	except OSError:
		logger.error('FAILED TO OPEN %s', procfn)
		raise
	return fd

def set_ns(nsname, etc_dir=NETNS_ETC_DIR):
	""" Switches the current process to the namespace provided. Algorithm
	derived from iproute2/ip/ipnetns.c.
	TODO: error checking
	"""

	netns_fd = get_netns_fd(nsname)
	if netns_fd is None:
		logger.warning('set_ns unable to open fd associated with %s', nsname)
		return False

	if _setns(netns_fd, 0) != 0:
		e = ctypes.get_errno()
		logger.warning('_setns libc call failed with errono %s %s', e, errno.errorcode[e])

	# TODO netlink handles /sys binding, do we need to?

	bind_etc(nsname, etc_dir)

	return netns_fd

def netns_add(nsname, etc_dir):
	netns_path = '{}/{}'.format(NETNS_RUN_DIR, nsname)
	if not os.path.is_dir(NETNS_RUN_DIR):
		os.mkdir(NETNS_RUN_DIR, os.S_IRWXU | os.S_IRGRP | os.S_IXGRP | os.S_IROTH | os.S_IXOTH)
	fd = os.open(netns_path, os.O_RDONLY | os.O_CREAT | os.O_EXCL, 0)
	os.close(fd)
	if _unshare(CLONE_NEWNET) != 0:
		e = ctypes.get_errno()
		logger.warning('unsharing the netns failed with errno %s %s', e, errno.errorcode[e])
	if _mount('/proc/self/ns/net', netns_path, "none", MS_BIND, None) != 0:
		e = ctypes.get_errno()
		logger.warning('_mount /proc/self/ns/net failed with %s %s', e, errno.errorcode[e])

def netns_delete(nsname):
	netns_path = '{}/{}'.format(NETNS_RUN_DIR,nsname)
	_umount2(netns_path, MNT_DETACH)
	if os.unlink(netns_path) != 0:
		logger.warning('Unable to unlink netnspath')

class netns(object):
	def __init__(self, netns_name, etc_dir=NETNS_ETC_DIR):
		self.netns_name = netns_name
		self.etc_dir = etc_dir

	def __enter__(self):
		self.current_fd = get_netns_fd(None)
		logger.info("entering %s from %s", self.netns_name, self.current_fd)
		self.netns_fd = set_ns(self.netns_name, self.etc_dir)

	def __exit__(self, type, value, tb):
		logger.info('leaving %s', self.netns_name)
		unbind_etc(self.netns_name, self.etc_dir)
		os.close(self.netns_fd)
		if _setns(self.current_fd, 0) != 0:
			e = ctypes.get_errno()
			logger.warning('_setns libc call failed with errno %s %s', e, errno.errorcode[e])
		os.close(self.current_fd)
