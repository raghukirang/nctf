# footilities is a basic set of tool functions with no dependancies, pure python
import re

def strip_periods(string):

    return "".join(string.split('.'))


def strip_colons(string):

    return "".join(string.split(':'))


def add_periods_fw(string):
    f = string[:1] + '.'
    s = string[1:2] + '.'
    t = string[2:3]
    return f + s + t


def add_colons(string):
    first = string[:2] + ':'
    second = string[2:4] + ':'
    third = string[4:6] + ':'
    four = string[6:8] + ':'
    five = string[8:10] + ':'
    six = string[10:12]

    return first + second + third + four + five + six


def add_dashes(string):
    first = string[:2] + '-'
    second = string[2:4] + '-'
    third = string[4:6] + '-'
    four = string[6:8] + '-'
    five = string[8:10] + '-'
    six = string[10:12]

    return first + second + third + four + five + six


def pretty(d, indent=0):
    for key, value in d.iteritems():
        print ('\t' * indent + str(key) + ':')
        if isinstance(value, dict):
            pretty(value, indent+1)
        else:
            print ('\t' * (indent+2) + "{}".format(str(value)))


def validate_ip(s):
    # Validates the string is a proper IPv4 address
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


def validate_cidr(s):
    # Validates the string is a proper IPv4 address
    foo = s.split('/')
    net = int(foo[1])
    if net < 0 or net > 32:
        return False
    a = foo[0].split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

def is_valid_ipv6(ip):
    """Validates IPv6 addresses. Thanks stackexchange
    """
    pattern = re.compile(r"""
        ^
        \s*                         # Leading whitespace
        (?!.*::.*::)                # Only a single wildcard allowed
        (?:(?!:)|:(?=:))            # Colon iff it would be part of a wildcard
        (?:                         # Repeat 6 times:
            [0-9a-f]{0,4}           #   A group of at most four hexadecimal digits
            (?:(?<=::)|(?<!::):)    #   Colon unless preceeded by wildcard
        ){6}                        #
        (?:                         # Either
            [0-9a-f]{0,4}           #   Another group
            (?:(?<=::)|(?<!::):)    #   Colon unless preceeded by wildcard
            [0-9a-f]{0,4}           #   Last group
            (?: (?<=::)             #   Colon iff preceeded by exacly one colon
             |  (?<!:)              #
             |  (?<=:) (?<!::) :    #
             )                      # OR
         |                          #   A v4 address with NO leading zeros
            (?:25[0-4]|2[0-4]\d|1\d\d|[1-9]?\d)
            (?: \.
                (?:25[0-4]|2[0-4]\d|1\d\d|[1-9]?\d)
            ){3}
        )
        \s*                         # Trailing whitespace
        $
    """, re.VERBOSE | re.IGNORECASE | re.DOTALL)
    return pattern.match(ip) is not None


def mac2auth(mac):
    # return cproot auth creds from either 12 char or 8 char mac address
    if len(mac) is 12:
        mac = mac.lstrip('0030')
    return '1415' + mac[4:8]

## These are all really bad
def search_dict_contains(d, kmatch, vmatch):
    #print 'search_dict_contains'
    # Return the value of a key,value pair that contains vmatch
    top = ''
    if type(d) != dict:
        print ('Error, this is not a dictionary')
        return 0

    for k,v in d.items():
        #print 'k:{}'.format(k)
        #print 'v:{}'.format(v)
        if len(v) and len(k):
            if type(v) is dict:
                top = search_dict_contains(v, kmatch, vmatch)
                if top:
                    return top
            else:
                if k == kmatch:
                    #print (v.find(vmatch))
                    if v.find(vmatch) != -1:
                        #print 'cont match!'
                        return v

def search_dict_contains2(d, kmatch, vmatch, exclude=''):
    #print 'search_dict_contains2'
    # Return the value of a key,value pair that contains vmatch
    # Version 2 allows for keys to be skipped via exclude
    top = ''
    if type(d) != dict:
        print ('Error, this is not a dictionary')
        return 0

    for k,v in d.items():
        #print 'k:{}'.format(k)
        #print 'v:{}'.format(v)
        if len(v) and len(k) and k != exclude:
            if type(v) is dict:
                top = search_dict_contains(v, kmatch, vmatch)
                if top:
                    return top
            else:
                if k == kmatch:
                    #print (v.find(vmatch))
                    if v.find(vmatch) != -1:
                        #print 'cont match!'
                        return v


def search_dict(d, kmatch, vmatch, prevkey=''):
    #print 'search_dict'
    # Return the previous key of a key,value pair that matches vmatch
    top = ''
    if type(d) != dict:
        print ('Error, this is not a dictionary')
        return 0

    for k,v in d.items():
        #print 'k:{}'.format(k)
        #print 'v:{}'.format(v)
        if len(v) and len(k):
            if type(v) is dict:
                top = search_dict(v, kmatch, vmatch, k)
                if top:
                    return top
            else:
                if k == kmatch:
                    if v == vmatch:
                        return prevkey

def search_dict2(d, kmatch, vmatch, exclude='', prevkey=''):
    #print 'search_dict2'
    # Return the previous key of a key,value pair that matches vmatch
    # Version 2 allows for keys to be skipped via exclude
    top = ''
    if type(d) != dict:
        print('Error, this is not a dictionary')
        return 0

    for k,v in d.items():
        #print 'k:{}'.format(k)
        #print 'v:{}'.format(v)
        if len(v) and len(k) and k != exclude:
            if type(v) is dict:
                top = search_dict(v, kmatch, vmatch, k)
                if top:
                    return top
            else:
                if k == kmatch:
                    if v == vmatch:
                        return prevkey

def search_dict3(d, kmatch, vmatch):
    top = False
    if type(d) != dict:
        #print('Error, this is not a dictionary')
        return 0
    for k,v in d.items():
        #print('k:{}'.format(k))
        #print('v:{}'.format(v))
        if k or len(k):
            if type(v) is dict:
                top = search_dict3(v, kmatch, vmatch)
                if top:
                    return top
            else:
                if k == kmatch:
                    if v == vmatch:
                        return True
    return top