#!/usr/bin/env python
##########
# listens for syslog messages on UDP port 514
##########
# messages are output to stdout, I like to use 'tee':
#   syslog-listener.py | tee -a /tmp/$(date +%Y%m%d-%H%M).log
# but a simple redirect also works:
#   syslog-listener.py >> /tmp/$(date +%Y%m%d-%H%M).log
#
# on linux needs to be run as root (directly or via 'sudo')
#
##########
# on the router I usually:
#  * turn off UTF-8 BOM
#  * turn on the SystemID


##########
# set RAWOUT to True to NOT parse and format the messages
RAWOUT = False

import sys
import socket
import time
import re
import threading
import signal
import logging
logger = logging.getLogger(__name__)

LEVELS = [
    'EMERGENCY',
    'ALERT',
    'CRITICAL',
    'ERROR',
    'WARNING',
    'NOTICE',
    'INFO',
    'DEBUG',
]


def LOGLEVEL(p):
    return (p & 0x07)


def LOGLEVEL_STR(p):
    return LEVELS[LOGLEVEL(p)]

LOGLOCATION = ''
loglocation = ''

FACILITYS = [
    'KERN',
    'USER',
    'MAIL',
    'DAEMON',
    'AUTH',
    'SYSLOG',
    'LPR',
    'NEWS',
    'UUCP',
    'CRON',
    'AUTHPRIV',
    'FTP',
    'RSVD1',
    'RSVD2',
    'RSVD3',
    'RSVD4',
]


def LOGFACILITY(p):
    return ((p >> 3) & 0x7f)


def LOGFACILITY_STR(p):
    try:
        return FACILITYS[LOGFACILITY(p)]
    except IndexError:
        return 'LOCAL_%x' % (LOGFACILITY(p) - len(FACILITYS))


ROUTERFMT = "[%*s] [%14s] %s"
ROUTERWID = 1 + max(map(len, LEVELS)) + max(map(len, FACILITYS))
TIGHTFMT = "S=%s %*s %s -- %s"
TIGHTWID = 1

##########
# assign desired Format and Width to USEFMT and USEWID respectively
USEFMT = TIGHTFMT
USEWID = TIGHTWID



# msg=r"<15>\xef\xbb\xbf(IBR600-057) cpevt:blargh: {'dormant': False, 'event': 'DORMANT', 'ifc': 'ttyCP0'}"
LEVEL = r'<(?P<level>\d\d?)>'
BOM = r'(?P<bom>' + '\xef\xbb\xbf)'
SYSID = r'(\((?P<sysid>[^\)]+?)\) )'
SOURCE = r'((?P<source>((:[^ ])|([^:]))+?): )'
LOGREGEX = re.compile('^' + LEVEL + BOM + '?' + SYSID + '?' + SOURCE + r'?(?P<msg>.*)$', re.DOTALL)


class _unbuffer:
    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)


sys.stdout = _unbuffer(sys.stdout)


def fmt_msg(facility, level, bom, source, sys_id, msg):
    if source is None: source = 'unk'
    if sys_id is None: sys_id = ''
    return USEFMT % (sys_id, USEWID, '%s.%s' % (LOGFACILITY_STR(facility), LOGLEVEL_STR(level)), source, msg)


# strip out:  ['', '', 'ffff', '192.168.3.1']
def simpleip(ip):
    p = ip.split(':')
    return p[3] if len(p) == 4 and p[0] == '' and p[1] == '' and p[2] == 'ffff' else ip


lastsender = None

def recvd_syslog(sender, msg):
    global lastsender
    stime = time.gmtime()
    #timestr = "%04d-%02d-%02d %02d:%02d:%02d" % (stime[0], stime[1], stime[2], stime[3], stime[4], stime[5])
    timestr = "{}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}".format(stime[0], stime[1], stime[2], stime[3], stime[4], stime[5])
    msg = str(msg)
    #logger.debug("before strip {}".format(msg))
    try:
        msg = msg.strip(' \t\r\n\0')
    except Exception as e:
        logger.error(e)
        pass
    #logger.debug("after strip {}".format(msg))
    if not RAWOUT:
        m = LOGREGEX.match(msg)
        #logger.debug("m:{}".format(m))
        if m:
            # print m.groups(); return
            args = [int(m.group('level')), int(m.group('level'))]
            for n in ['bom', 'source', 'sysid']:
                try:
                    args.append(m.group(n))
                except IndexError:
                    args.append(None)
            args.append(m.group('msg').strip())
            msg = fmt_msg(*args)
    #if sender != lastsender:
        #lastsender = sender
        #print("\n%*s %s:%d %s" % (len(timestr), 8 * '_', simpleip(sender[0]), sender[1], 8 * '_'))
        #f.write("\n%*s %s:%d %s" % (len(timestr), 8 * '_', simpleip(sender[0]), sender[1], 8 * '_'))

    #logger.debug("{} {}".format(timestr, msg))
    try:
        f = open('{}/{}'.format(loglocation, simpleip(sender[0])), 'a')
        f.write("\n{} {}".format(timestr, msg))
        f.close()
    except Exception:
        logger.error("Problem writing to file!!")
        pass


def syslogdudp(ip, port):
    for fam in [socket.AF_INET6, socket.AF_INET]:
        try:
            s = socket.socket(fam, socket.SOCK_DGRAM)
            s.bind((ip, port))
            break
        except socket.error as e:
            # print e.errno
            # -9: family not supported
            # 101: network unreachable, e.g. no IPv6
            if e.errno != -9 and e.errno != 101:
                raise
    while True:
        recvd = s.recvfrom(16384)
        recvd_syslog(recvd[1], recvd[0])


# tcp not tested!!!
def syslogdtcp(ip, port):
    while True:
        for fam in [socket.AF_INET6, socket.AF_INET]:
            try:
                s = socket.socket(fam, socket.SOCK_STREAM)
                s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                s.bind((ip, port))
                break
            except socket.error as e:
                # print e.errno
                # -9: family not supported
                # 101: network unreachable, e.g. no IPv6
                if e.errno != -9 and e.errno != 101:
                    raise
        s.listen(2)
        while True:
            client = s.accept()[0]
            for l in client:
                recvd_syslog(client, l)
            client.close()


def syslogd(host, port, tcp=False):
    # host = socket.gethostbyname(host)
    return syslogdtcp(host, port) if tcp else syslogdudp(host, port)


def journey_syslogd(log_location):
    global loglocation
    loglocation = log_location
    return syslogdudp(ip='', port=514)

def watch_for(msg, file):

    checking = True
    exit = False
    while checking:
        f = open('{}'.format(file), 'r')
        for l in f:
            if msg in l:
                print('found msg!:{}'.format(msg))
                exit = True
        f.close()
        if exit:
            checking = False
        else:
            time.sleep(3)

    return 1





#   Threaded class attempt

class Syslogd(threading.Thread):

    def __init__(self, log_location):
        # starting syslogd
        print('starting syslogd')
        self.log_location = log_location
        self.thisguy = None
        threading.Thread.__init__(self)

    def run(self):
        #self.thisguy = aurora_syslogd()
        self.thisguy = self.syslogdudp(ip='', port=514)

    def watch_for(self, msg):
        print("watch for:{}".format(msg))


    def syslogdudp(self, ip, port):
        for fam in [socket.AF_INET6, socket.AF_INET]:
            try:
                s = socket.socket(fam, socket.SOCK_DGRAM)
                s.bind((ip, port))
                break
            except socket.error as e:
                # print e.errno
                # -9: family not supported
                # 101: network unreachable, e.g. no IPv6
                if e.errno != -9 and e.errno != 101:
                    raise
        while True:
            recvd = s.recvfrom(16384)
            self.recvd_syslog(recvd[1], recvd[0])

    def recvd_syslog(self, sender, msg):
        #f = open('lib/testwrite1', 'a')
        # print f
        global lastsender
        stime = time.gmtime()
        timestr = "%04d-%02d-%02d %02d:%02d:%02d" % (stime[0], stime[1], stime[2], stime[3], stime[4], stime[5])
        msg = msg.strip(' \t\r\n\0')
        if not RAWOUT:
            m = LOGREGEX.match(msg)
            if m:
                # print m.groups(); return
                args = [int(m.group('level')), int(m.group('level'))]
                for n in ['bom', 'source', 'sysid']:
                    try:
                        args.append(m.group(n))
                    except IndexError:
                        args.append(None)
                args.append(m.group('msg').strip())
                msg = fmt_msg(*args)
        if sender != lastsender:
            lastsender = sender
            print("\n%*s %s:%d %s" % (len(timestr), 8 * '_', simpleip(sender[0]), sender[1], 8 * '_'))
            #f.write("\n%*s %s:%d %s" % (len(timestr), 8 * '_', simpleip(sender[0]), sender[1], 8 * '_'))

        print("%s %s" % (timestr, msg))
        f = open('{}/{}'.format(self.log_location, simpleip(sender[0])), 'a')
        f.write("\n{} {}".format(timestr, msg))
        f.close()


if __name__ == '__main__':
    tcp = False
    udp = True
    daemon = False
    port = 514
    host = ''

    while len(sys.argv) > 1:
        try:
            port = int(sys.argv[1])
        except ValueError:
            host = sys.argv[1]
        del (sys.argv[1])
        if host == 'tcp':
            host = ''
            tcp = True
            udp = False
        elif host == 'udp':
            host = ''
            tcp = False
            udp = True
    syslogd(host, port, tcp)