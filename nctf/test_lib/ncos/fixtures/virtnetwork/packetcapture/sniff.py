import os
import select
import socket
import struct
import subprocess
import time
from socket import inet_ntoa
from threading import Thread, Lock

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.setns import netns

ETH_P_ALL = 0x0003
ETH_P_IP  = 0x0800

class IPSniffer(object):
    max_packet_size = 8192

    def __init__(self, iface, packet_cb):
        self.iface = iface
        self.packet_cb = packet_cb
        self.capture = True

        self.pipe = os.pipe()

        self.rawsock = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(ETH_P_ALL))
        self.rawsock.settimeout(0)
        self.rawsock.bind((self.iface, ETH_P_ALL))

        self.poll = select.poll()
        self.poll.register(self.pipe[0], select.POLLIN)
        self.poll.register(self.rawsock.fileno(), select.POLLIN)

        self.__set_promisc(True)

    def __set_promisc(self, promisc):
        subprocess.call('ip link set %s promisc %s' % (self.iface, 'on' if promisc else 'off'), shell=True)

    def __process_ipframe(self, ip_header, payload):
        # Extract the 20 bytes IP header, ignoring the IP options
        fields = struct.unpack("!BBHHHBBHII", ip_header)

        ip_hdrlen = fields[0] & 0xf
        ip_len = fields[2]

        ip_src = ip_header[12:16]
        ip_dst = ip_header[16:20]
        ip_frame = payload[0:ip_len]

        self.packet_cb(self.iface, ip_src, ip_dst, ip_frame)

    def recv(self):
        while self.capture:
            for fd, event in self.poll.poll():
                if fd == self.pipe[0]:
                    break

                pkt = self.rawsock.recvfrom(self.max_packet_size)[0]
                if len(pkt) <= 0:
                    break

                eth_header = struct.unpack("!6s6sH", pkt[0:14])
                if eth_header[2] != ETH_P_IP:
                    continue

                ip_header = pkt[14:34]
                payload = pkt[14:]

                self.__process_ipframe(ip_header, payload)

    def deinit(self):
        self.capture = False
        os.write(self.pipe[1], b'1')
        self.__set_promisc(False)
        os.close(self.pipe[1])
        os.close(self.pipe[0])
        self.rawsock.close()

class NsIPSnifferHandler(object):
    """
    Launch a thread in the particular namespace.
    A namespace "aware" thread. Each thread
    created has a namespace thread attached
    to an interface.
    """
    def __init__(self, ns, if_name, sniffer_test_obj):
        """
        - Pass along the namespace (NetNs object) as the first argument.
        - The second argument is the interface name.
        - The third is a sniffer function which has all the hooks for
        packet analysis.
        """
        self.ns = ns #Namespace where we want to run the sniffer
        self.if_name = if_name #Name of the interface to sniff on
        self.packet_filter_function = sniffer_test_obj.packet_filter_function #callback function used by IPSniffer for packet analysis
        self.sniffer = None
        self.sniffer_th = None #actual process launched into the namespace during sniffing

    def start(self):
        with netns(self.ns.nsname, None):
            self.sniffer = IPSniffer(self.if_name, self.packet_filter_function)
            self.sniffer_th = Thread(target=self.sniffer.recv)
            self.sniffer_th.start()
        #Check if the child thread is ready to roll and block till it is :)
        time.sleep(3) #Need some time so that everything is properly started.

    def stop(self):
        with netns(self.ns.nsname, None):
            self.sniffer.deinit()
            self.sniffer_th.join(3)
            if self.sniffer_th.is_alive():
                print('Namespace Aware Worker Thread :: FAILED TO JOIN')
#             else:
#                 print('Namespace Aware Thread :: KILLED OFF')
        self.sniffer = None
        self.sniffer_th = None


class Protocol:
    """ [outer, optional inner] """
    ICMP = [1]
    TCP = [6]
    UDP = [17]
    GRE = [47]
    ESP = [50]
    AH = [51]
    IPv6Route = [43]
    GRE_ICMP = [47, 1]
    GRE_TCP = [47, 6]
    GRE_UDP = [47, 17]


class SniffResult(object):
    def __init__(self, iface, ip_src, ip_dst, ip_frame):
        self.iface = iface      # The interface from which the sniffed packet came (useful if sniffing more than one port)
        self.outer_proto = ip_frame[9]
        self.outer_src = ip_src
        self.outer_dst = ip_dst
        self.inner_proto = None
        self.inner_src = None
        self.inner_dst = None
        self.sport = None
        self.dport = None
        self.unpack_inner(ip_frame)

    @classmethod
    def offset(self, ip_frame):
        offset = (ip_frame[0] & 0xf) * 4
        if ip_frame[9] == Protocol.GRE[0]:
            # TODO: determine GRE hdr size with options
            offset += 4
        return offset

    def unpack_inner(self, ip_frame):
        if self.outer_proto in [47]:    # TODO: support other types of encapsulation in addition to GRE
            iphdr = ip_frame[self.offset(ip_frame):]
            self.inner_proto = iphdr[9]
            self.inner_src = inet_ntoa(iphdr[12:16])
            self.inner_dst = inet_ntoa(iphdr[16:20])
            proto = self.inner_proto
        else:
            # No encapsulation
            iphdr = ip_frame
            proto = self.outer_proto
        if proto in [6, 17]:
            olen = self.offset(iphdr)
            self.sport = iphdr[olen] << 8 | iphdr[olen+1]
            self.dport = iphdr[olen+2] << 8 | iphdr[olen+3]


class SniffFilter(object):
    """
    This class describes the properties
    of the sniffer and what kind of packets
    are supposed to be analyzed.
    """

    def __init__(self, protos):
        self.protos = protos
        self.tLock = Lock()

    def packet_filter_function(self, iface, ip_src, ip_dst, ip_frame):
        for proto in self.protos:
            if ip_frame[9] != proto[0]:
                continue
            try:
                if ip_frame[SniffResult.offset(ip_frame) + 9] != proto[1]:
                    continue
            except IndexError:
                pass
            with self.tLock:
                self.packet_cb(SniffResult(iface, inet_ntoa(ip_src), inet_ntoa(ip_dst), ip_frame))
