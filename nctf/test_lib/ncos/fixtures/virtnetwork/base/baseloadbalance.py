"""
Base class for firewall tests
"""

__copyright__ = """
Copyright (c) 2014 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. """

import time

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.csclient import config
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.netns import Dummy
from nctf.test_lib.ncos.fixtures.virtnetwork.base.basenet import NetTest
from nctf.test_lib.ncos.fixtures.virtnetwork.nose.plugins.attrib import attr

@attr(prio=1)
class LoadBalanceTest(NetTest):

	virt_router_cfg = [{
		'nets': 3
	}]

	txfer_time = 5
	virt_wan_ip = '192.168.1.5'
	router_wan_ip = '192.168.1.10'
	virt_lan_ip = '192.168.0.5'
	router_lan_ip = '192.168.0.1'
	virt_secondary_wan_ip = '192.168.2.5'
	router_secondary_wan_ip = '192.168.2.10'
	dummy_ip = '10.10.10.1'

	@classmethod
	def setUpClass(cls):

		super().setUpClass()

		cls.secondary_wan = cls.virt_routers[0]['lans'][1]

		cls.dummy_iface_wan = Dummy('lb', cls.wan['ns'])
		cls.dummy_iface_wan.up()

		cls.dummy_iface_secondary_wan = Dummy('lb', cls.secondary_wan['ns'])
		cls.dummy_iface_secondary_wan.up()


	@classmethod
	def tearDownClass(cls):

		cls.dummy_iface_secondary_wan.deinit()
		cls.dummy_iface_wan.deinit()

		super().tearDownClass()


	def setUp(self):

		self.wan['net_veth'].virt_if.addradd("%s/24" % self.virt_wan_ip)
		self.lan['net_veth'].virt_if.addradd("%s/24" % self.virt_lan_ip)
		self.lan['ns'].nsexec('ip route add default via %s' % self.router_lan_ip)

		static_cfg = {
			"priority": 1,
			"ip_mode": "static",
			"static": {
			    "dns": [
				{
				    "ip_address": self.virt_wan_ip
				},
				{}
			    ],
			    "gateway": self.virt_wan_ip,
			    "ip_address": self.router_wan_ip,
			    "netmask": "255.255.255.0"
			},
			"loadbalance": True
		}

		config(self.routerns, 'put', 'config.wan.rules2.0', value=static_cfg)

		# change one of the lans to a wan
		vlan_cfg = [{
			"mode": "wan",
			"ports": [{
				"mode": "untagged",
				"port": 0
			}],
			"uid": "wan",
			"vid": 1
		}, {
			"mode": "lan",
			"ports": [{
				"mode": "untagged",
				"port": 1
			}],
			"uid": "lan",
			"vid": 2
		}, {
			"mode": "wan",
			"ports": [{
				"mode": "untagged",
				"port": 2
			}],
			"uid": "wan2",
			"vid": 3
		}]

		config(self.routerns, 'put', 'config.vlan', value=vlan_cfg)

		static_2_cfg = {
			"priority": 0,
			"ip_mode": "static",
			"static": {
			    "dns": [
				{
				    "ip_address": self.virt_secondary_wan_ip
				},
				{}
			    ],
			    "gateway": self.virt_secondary_wan_ip,
			    "ip_address": self.router_secondary_wan_ip,
			    "netmask": "255.255.255.0"
			},
			"loadbalance": True,
			"trigger_name": "Ethernet-wan2",
			"trigger_string": "type|is|ethernet%uid|is|wan2"
		}

		config(self.routerns, 'put', 'config.wan.rules2.1', value=static_2_cfg)

		# let the new config take
		time.sleep(2)

		self.secondary_wan['net_veth'].virt_if.addradd("%s/24" % self.virt_secondary_wan_ip)

		self.dummy_iface_wan.addradd("%s/24" % self.dummy_ip)
		self.dummy_iface_secondary_wan.addradd("%s/24" % self.dummy_ip)

		# finally let everything settle
		time.sleep(2)


	def tearDown(self):

		self.dummy_iface_secondary_wan.addrdel("%s/24" % self.dummy_ip)
		self.dummy_iface_wan.addrdel("%s/24" % self.dummy_ip)

		self.secondary_wan['net_veth'].virt_if.addrdel("%s/24" % self.virt_secondary_wan_ip)

		self.lan['ns'].nsexec('ip route del default via %s' % self.router_lan_ip)
		self.wan['net_veth'].virt_if.addrdel("%s/24" % self.virt_wan_ip)
		self.lan['net_veth'].virt_if.addrdel("%s/24" % self.virt_lan_ip)
