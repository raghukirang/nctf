"""
Base for routing protocol testing
"""
# JOURNEY ADDS

__copyright__ = """
Copyright (c) 2014 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. """

from threading import Thread

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.testsrv import runTestSrvTx
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.utils import ThreadHelper
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.csclient import config
from nctf.test_lib.ncos.fixtures.virtnetwork.base.basenet import NetTest
from nctf.test_lib.ncos.fixtures.virtnetwork.nose.plugins.attrib import attr

@attr(prio=2)
class VrpTestBase(NetTest):

	# Configure 3 router instances and bridge LAN2s together
	virt_router_cfg = [{
		'name': 'rtr0',
		'nets': 3
	}, {
		'name': 'rtr1',
		'nets': 3
	}, {
		'name': 'rtr2',
		'nets': 3
	}]

	wan0_ip6_prefix = 'fd00:dead:beef:0010::'
	wan1_ip6_prefix = 'fd00:dead:beef:0011::'
	wan2_ip6_prefix = 'fd00:dead:beef:0012::'
	lan10_ip6_prefix = 'fd00:dead:beef:0020::'
	lan11_ip6_prefix = 'fd00:dead:beef:0021::'
	lan12_ip6_prefix = 'fd00:dead:beef:0022::'
	lan2_ip6_prefix = 'fd00:dead:beef:0002::'

	ipinfo = [{
		'gwip': '192.168.1.1',
		'wanip': '192.168.1.10',
		'lanip': '10.0.1.1',
		'lanclient': '10.0.1.5',
		'lan2ip': '10.10.1.1',
	    'gwip6': wan0_ip6_prefix + '1',
	    'wanip6': wan0_ip6_prefix + '10',
	    'lanip6prefix': lan10_ip6_prefix,
	    'lanip6': lan10_ip6_prefix + '1',
	    'lanclient6': lan10_ip6_prefix + '5',
	    'lan2ip6': lan2_ip6_prefix + '1'
	}, {
		'gwip': '192.168.2.1',
		'wanip': '192.168.2.10',
		'lanip': '10.0.2.1',
		'lanclient': '10.0.2.5',
		'lan2ip': '10.10.1.2',
	    'gwip6': wan1_ip6_prefix + '1',
	    'wanip6': wan1_ip6_prefix + '10',
	    'lanip6prefix': lan11_ip6_prefix,
	    'lanip6': lan11_ip6_prefix + '1',
	    'lanclient6': lan11_ip6_prefix + '5',
	    'lan2ip6': lan2_ip6_prefix + '2'
	}, {
		'gwip': '192.168.3.1',
		'wanip': '192.168.3.10',
		'lanip': '10.0.3.1',
		'lanclient': '10.0.3.5',
		'lan2ip': '10.10.1.3',
	    'gwip6': wan2_ip6_prefix + '1',
	    'wanip6': wan2_ip6_prefix + '10',
	    'lanip6prefix': lan12_ip6_prefix,
	    'lanip6': lan12_ip6_prefix + '1',
	    'lanclient6': lan12_ip6_prefix + '5',
	    'lan2ip6': lan2_ip6_prefix + '3'
	}]

	secondary_lan_id = '00000001-0d93-319d-8220-4a1fb0372b51'

	txfer_time = 10
	txfer_mmin = 10
	txfer_bmin = 1 * 1024 * 1024

	timeout = 60

	@classmethod
	def setUpClass(cls):
		super().setUpClass()

		# some simple list accessors
		cls.rtr = [v['router'] for v in cls.virt_routers]
		cls.rtrns = [v['routerns'] for v in cls.virt_routers]
		cls.wan = [v['wans'][0] for v in cls.virt_routers]
		cls.lan = [v['lans'][0] for v in cls.virt_routers]
		cls.lan2 = [v['lans'][1] for v in cls.virt_routers]

		# bridge the LAN2 networks
		cls.rtr[0].bridge(2, cls.rtr[1], 2)
		cls.rtr[0].bridge(2, cls.rtr[2], 2)

		# setup lan configs amongst 3 routers
		for i, rtr in enumerate(cls.rtr):

			cfg = {
				"wan": {
					"rules2": {
						"0": {
							"ip_mode": "static",
							"static": {
								"dns": {
									"0": {
										"ip_address": cls.ipinfo[i]['gwip']
									}
								},
								"gateway": cls.ipinfo[i]['gwip'],
								"ip_address": cls.ipinfo[i]['wanip'],
								"netmask": "255.255.255.0"
							},
						    "dns6": [
							    {
								    "ip_address": cls.ipinfo[i]['gwip6']
							    }
						    ],
						    "ip6_mode": "static6",
						    "static6": {
							    "gateway": cls.ipinfo[i]['gwip6'],
								"ip_address": cls.ipinfo[i]['wanip6'],
						        "prefixlen": 64
						    },
							# "pd6": [  # Todo: what is this?
							# 	{
							# 	"prefix": "fd00:dead:beef:0003::",
							# 	"prefixlen": 64
							# 	},
							# 	{}
							# ]
						}
					}
				},
				"firewall": {
					"wan_ping": True
				},
				"lan": {
					"0": {
						"ip_address": cls.ipinfo[i]['lanip'],
					    "ip6_address": cls.ipinfo[i]['lanip6'],
					    "ip6_mode": "static",
					    "ip6_prefixlen": 64
					},
					"1": {
						"_id_": cls.secondary_lan_id,
						"devices": [
						    {
							"type": "ethernet",
							"uid": "secondary_lan"
						    }
						],
						"dhcpd": {
						    "enabled": False
						},
						"enabled": True,
						"ip_address": cls.ipinfo[i]['lan2ip'],
						"name": "Secondary LAN",
						"netmask": "255.255.255.0",
					    "ip6_address": cls.ipinfo[i]['lan2ip6'],
					    "ip6_mode": "static",
					    "ip6_prefixlen": 64,
						"schedule": {
						    "enabled": False
						}
					}
				},
				"vlan": {
					"2": {
						"mode": "lan",
						"ports": [{
							"mode": "untagged",
							"port": 2
						}],
						"uid": "secondary_lan",
						"vid": 3
					}
				},
				"security": {
					"zfw": {
						"zones": {
							"3": {
								"devices": {
									"1": {
										 "trigger_group": "lan",
										 "trigger_field": "config_id",
										 "trigger_neg": False,
										 "trigger_predicate": "is",
										 "trigger_value": cls.secondary_lan_id
									}
								}
							}
						}
					}
				}
			}

			# set configuration

			# Free up port Todo: switch this to preseed
			config(cls.rtrns[i], 'delete', 'config.vlan.1.ports.1')

			config(cls.rtrns[i], 'put', 'config', value=cfg)

			# now setup the actual network configuration
			cls.wan[i]['net_veth'].virt_if.addradd('%s/24' % cls.ipinfo[i]['gwip'])
			cls.lan[i]['net_veth'].virt_if.addradd('%s/24' % cls.ipinfo[i]['lanclient'])
			cls.lan[i]['ns'].nsexec('ip route add default via %s' % cls.ipinfo[i]['lanip'])

			cls.wan[i]['net_veth'].virt_if.addradd('%s/64' % cls.ipinfo[i]['gwip6'])
			cls.lan[i]['net_veth'].virt_if.addradd('%s/64' % cls.ipinfo[i]['lanclient6'])
			cls.wan[i]['ns'].nsexec('ip route add %s/64 via %s' % (cls.ipinfo[i]['lanip6prefix'], cls.ipinfo[i]['wanip6']))
			cls.lan[i]['ns'].nsexec('ip route add default via %s metric 512' % cls.ipinfo[i]['lanip6'])

			# Note we don't assign any veth addrs for lan2, as it exists only as
			# a bridged LAN between the router instances

	def RoutedIPv4LANTraffic(self, proto_name):

		res0 = {'mbps': 0, 'recv': 0}
		def tx0():
			m, b = runTestSrvTx(self.lan[1]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[1]['lanclient'], 1234,
			                    self.ipinfo[1]['lanclient'], 1234,
			                    'tcp', 'inet', self.txfer_time, timeout=self.timeout)
			res0['mbps'] = m
			res0['recv'] = b

		res1 = {'mbps': 0, 'recv': 0}
		def tx1():
			m, b = runTestSrvTx(self.lan[2]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[2]['lanclient'], 1235,
			                    self.ipinfo[2]['lanclient'], 1235,
			                    'tcp', 'inet', self.txfer_time, timeout=self.timeout)
			res1['mbps'] = m
			res1['recv'] = b

		res2 = {'mbps': 0, 'recv': 0}
		def tx2():
			m, b = runTestSrvTx(self.lan[2]['ns'], self.lan[1]['ns'],
			                    self.ipinfo[2]['lanclient'], 1236,
			                    self.ipinfo[2]['lanclient'], 1236,
			                    'tcp', 'inet', self.txfer_time, timeout=self.timeout)
			res2['mbps'] = m
			res2['recv'] = b

		th = ThreadHelper([
			Thread(target=tx0),
			Thread(target=tx1),
			Thread(target=tx2)
		])

		th.start()
		th.join(timeout=self.timeout)

		print('Test %s LAN 0 -> LAN 1 IPv4 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res0['recv'], res0['mbps']))
		print('Test %s LAN 0 -> LAN 2 IPv4 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res1['recv'], res1['mbps']))
		print('Test %s LAN 1 -> LAN 2 IPv4 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res2['recv'], res2['mbps']))

		# all networks should have received a reasonable amount of data
		self.assertGreaterEqual(min(res0['recv'], res1['recv'], res2['recv']), self.txfer_bmin)

		# every network's speed wasn't overly horrible
		self.assertGreaterEqual(min(res0['mbps'], res1['mbps'], res2['mbps']), self.txfer_mmin)

	def RoutedIPv4WANTraffic(self, proto_name):

		res0 = {'mbps': 0, 'recv': 0}
		def tx0():
			m, b = runTestSrvTx(self.wan[1]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[1]['gwip'], 1234,
			                    self.ipinfo[1]['gwip'], 1234,
			                    'tcp', 'inet', self.txfer_time, timeout=self.timeout)
			res0['mbps'] = m
			res0['recv'] = b

		res1 = {'mbps': 0, 'recv': 0}
		def tx1():
			m, b = runTestSrvTx(self.wan[2]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[2]['gwip'], 1235,
			                    self.ipinfo[2]['gwip'], 1235,
			                    'tcp', 'inet', self.txfer_time, timeout=self.timeout)
			res1['mbps'] = m
			res1['recv'] = b

		res2 = {'mbps': 0, 'recv': 0}
		def tx2():
			m, b = runTestSrvTx(self.wan[2]['ns'], self.lan[1]['ns'],
			                    self.ipinfo[2]['gwip'], 1236,
			                    self.ipinfo[2]['gwip'], 1236,
			                    'tcp', 'inet', self.txfer_time, timeout=self.timeout)
			res2['mbps'] = m
			res2['recv'] = b

		th = ThreadHelper([
			Thread(target=tx0),
			Thread(target=tx1),
			Thread(target=tx2)
		])

		th.start()
		th.join(timeout=self.timeout)

		print('Test %s LAN 0 -> WAN 1 IPv4 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res0['recv'], res0['mbps']))
		print('Test %s LAN 0 -> WAN 2 IPv4 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res1['recv'], res1['mbps']))
		print('Test %s LAN 1 -> WAN 2 IPv4 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res2['recv'], res2['mbps']))

		# all networks should have received a reasonable amount of data
		self.assertGreaterEqual(min(res0['recv'], res1['recv'], res2['recv']), self.txfer_bmin)

		# every network's speed wasn't overly horrible
		self.assertGreaterEqual(min(res0['mbps'], res1['mbps'], res2['mbps']), self.txfer_mmin)

	def RoutedIPv6LANTraffic(self, proto_name):

		res0 = {'mbps': 0, 'recv': 0}
		def tx0():
			m, b = runTestSrvTx(self.lan[1]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[1]['lanclient6'], 1234,
			                    self.ipinfo[1]['lanclient6'], 1234,
			                    'tcp', 'inet6', self.txfer_time, timeout=self.timeout)
			res0['mbps'] = m
			res0['recv'] = b

		res1 = {'mbps': 0, 'recv': 0}
		def tx1():
			m, b = runTestSrvTx(self.lan[2]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[2]['lanclient6'], 1235,
			                    self.ipinfo[2]['lanclient6'], 1235,
			                    'tcp', 'inet6', self.txfer_time, timeout=self.timeout)
			res1['mbps'] = m
			res1['recv'] = b

		res2 = {'mbps': 0, 'recv': 0}
		def tx2():
			m, b = runTestSrvTx(self.lan[2]['ns'], self.lan[1]['ns'],
			                    self.ipinfo[2]['lanclient6'], 1236,
			                    self.ipinfo[2]['lanclient6'], 1236,
			                    'tcp', 'inet6', self.txfer_time, timeout=self.timeout)
			res2['mbps'] = m
			res2['recv'] = b

		th = ThreadHelper([
			Thread(target=tx0),
			Thread(target=tx1),
			Thread(target=tx2)
		])

		th.start()
		th.join(timeout=self.timeout)

		print('Test %s LAN 0 -> LAN 1 IPv6 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res0['recv'], res0['mbps']))
		print('Test %s LAN 0 -> LAN 2 IPv6 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res1['recv'], res1['mbps']))
		print('Test %s LAN 1 -> LAN 2 IPv6 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res2['recv'], res2['mbps']))

		# all networks should have received a reasonable amount of data
		self.assertGreaterEqual(min(res0['recv'], res1['recv'], res2['recv']), self.txfer_bmin)

		# every network's speed wasn't overly horrible
		self.assertGreaterEqual(min(res0['mbps'], res1['mbps'], res2['mbps']), self.txfer_mmin)

	def RoutedIPv6WANTraffic(self, proto_name):

		res0 = {'mbps': 0, 'recv': 0}
		def tx0():
			m, b = runTestSrvTx(self.wan[1]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[1]['gwip6'], 1234,
			                    self.ipinfo[1]['gwip6'], 1234,
			                    'tcp', 'inet6', self.txfer_time, timeout=self.timeout)
			res0['mbps'] = m
			res0['recv'] = b

		res1 = {'mbps': 0, 'recv': 0}
		def tx1():
			m, b = runTestSrvTx(self.wan[2]['ns'], self.lan[0]['ns'],
			                    self.ipinfo[2]['gwip6'], 1235,
			                    self.ipinfo[2]['gwip6'], 1235,
			                    'tcp', 'inet6', self.txfer_time, timeout=self.timeout)
			res1['mbps'] = m
			res1['recv'] = b

		res2 = {'mbps': 0, 'recv': 0}
		def tx2():
			m, b = runTestSrvTx(self.wan[2]['ns'], self.lan[1]['ns'],
			                    self.ipinfo[2]['gwip6'], 1236,
			                    self.ipinfo[2]['gwip6'], 1236,
			                    'tcp', 'inet6', self.txfer_time, timeout=self.timeout)
			res2['mbps'] = m
			res2['recv'] = b

		th = ThreadHelper([
			Thread(target=tx0),
			Thread(target=tx1),
			Thread(target=tx2)
		])

		th.start()
		th.join(timeout=self.timeout)

		print('Test %s LAN 0 -> WAN 1 IPv6 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res0['recv'], res0['mbps']))
		print('Test %s LAN 0 -> WAN 2 IPv6 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res1['recv'], res1['mbps']))
		print('Test %s LAN 1 -> WAN 2 IPv6 TCP transferred total %d bytes, %d mbits/s' % (proto_name, res2['recv'], res2['mbps']))

		# all networks should have received a reasonable amount of data
		self.assertGreaterEqual(min(res0['recv'], res1['recv'], res2['recv']), self.txfer_bmin)

		# every network's speed wasn't overly horrible
		self.assertGreaterEqual(min(res0['mbps'], res1['mbps'], res2['mbps']), self.txfer_mmin)
