"""
Simple base networking test class
"""

__copyright__ = """
Copyright (c) 2016 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. """

# Journey adds
import ipaddress
import json
import os
import time
import unittest

from nctf.test_lib.ncos.fixtures.virtnetwork.nose.plugins.attrib import attr
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtnetwork import VirtNetwork
from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtnamespace import VirtNamespaceBase
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.utils import local_ip, ping
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.testsrv import runTestSrvTx
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.virtexec import virtexec
from nctf.test_lib.ncos.fixtures.virtnetwork.packetcapture.sniff import NsIPSnifferHandler

@attr(prio=1)
class VirtTest(unittest.TestCase):

	network = {}

	@classmethod
	def setUpClass(cls):
		unittest.installHandler()
		if cls.network:
			if isinstance(cls.network, str):
				cls.network = cls.json_file_to_dict(cls.network)
			cls.network = VirtNetwork(cls.network)
			cls.network.start()
			for k in (cls.network.networking.get('nodes') or {}).keys():
				setattr(cls, k, cls.get_nsobj(k))
		time.sleep(2) #TODO Fix me

	@classmethod
	def json_file_to_dict(cls, path):
		net = {}
		if os.path.exists(path):
			with open(path) as file:
				net = json.load(file)
		return net

	@classmethod
	def shutdown(cls):
		unittest.removeHandler()
		if cls.network:
			cls.network.deinit()
			for k in (cls.network.networking.get('nodes') or {}).keys():
				try:
					delattr(cls, k)
				except AttributeError:
					continue
		cls.network = None

	@classmethod
	def tearDownClass(cls):
		cls.shutdown()

	@classmethod
	def get_nsobj(cls, key):
		try:
			return cls.network.networking['nodes'][key]['init']
		except KeyError:
			return None


class VirtNamespaceTests(object):
	"""Functionality for running tests based on virtnamespace objects"""

	@property
	def ip_address(self):
		return virtexec(self.ns, local_ip)

	def _wait_for_ip(self, timeout=5):
		while not self.ip_address and timeout > 0:
			time.sleep(1)
			timeout -= 1
		return self.ip_address, timeout

	def wait_for_ip(self, timeout=5):
		""" Return list of IP's when non-empty, or empty list if timeout """
		return self._wait_for_ip(timeout)[0]

	def ping_test(self, dst_address, timeout=30):
		""" Return true if we have successful ping connectivity within timeout. This includes waiting for a source
		ip address"""

		ipl, timeout = self._wait_for_ip(timeout)
		if not ipl:
			return False

		return 0 == virtexec(self.ns, ping, 3, ipl[0], dst_address,
				timeout=timeout + 15, kwargs={'timeout': timeout + 10, 'deadline': max(1, timeout)})

	def communication_test(self, dst_vns, timeout=30, count=1):
		"""dst_vns: virtual namespace of destination"""
		dst_ip = (dst_vns.wait_for_ip(timeout) or [None])[0]
		if not dst_ip:
			return False
		my_ip = (self.wait_for_ip(timeout) or [None])[0]
		if not my_ip:
			return False
		while timeout > 0:
			start = time.time()
			if virtexec(self.ns, ping, count, my_ip, dst_ip, interval=1, quiet=True) == 0:
				return True
			end = time.time()
			if end - start < 1:
				time.sleep(1 - (end-start))
			timeout -= 1
		else:
			return False

	def run_test_server(self, client, port=1234, proto='tcp', duration=10, timeout=30):
		server_ip = self.wait_for_ip()
		if not server_ip:
			return None, None
		server_ip = ipaddress.ip_address(server_ip[0])
		family = 'inet' if server_ip.version == 4 else 'inet6'
		return runTestSrvTx(self.ns, client.ns, str(server_ip), port, str(server_ip), port, proto, family, duration, timeout=timeout)

	def start_sniff(self, port_nums, sniffer_test_obj):
		""" Get the port numbers and start sniffing """
		self.nssniffers = []
		if isinstance(port_nums, list):
			#Provides the much needed mapping from port_numbers to interface and corresponding namespace
			#A subtle point here is that because tap devices are actually pushed to other different namespace
			#the 'destination' namespace may change (mostly happens in cases of links from router to other devices).
			sniffer_test_dict = {self.getlink(port_num).iface.ifname:self.getlink(port_num).iface.ns for port_num in port_nums}
		elif isinstance(port_nums, int):
			sniffer_test_dict = {self.getlink(port_nums).iface.ifname:self.getlink(port_nums).iface.ns}
		else:
			print("Provide either a list of port numbers or just the port number")
			return False

		for k, v in sniffer_test_dict.items():
			print("Running NsSniffer for Interface: "+ k + " Namespace: " + v.nsname)
			self.nssniffers.append(NsIPSnifferHandler(v, k, sniffer_test_obj))

		for ns_sniffer in self.nssniffers:
			ns_sniffer.start()

		return True

	def stop_sniff(self):
		"""
		Stop the sniffers
		"""
		for ns_sniffer in self.nssniffers:
			ns_sniffer.stop()

#  Dynamic mixin separates test logic added to namespaces from namespaces, useful for doing things like
#  namespaceobject.run_test_server(othernamespaceobject)
VirtNamespaceBase.__bases__ = (VirtNamespaceTests, object, )
