"""
Base class for firewall tests
"""

__copyright__ = """
Copyright (c) 2014 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. """

import time

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.csclient import config
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.testsrv import runTestSrvTx
from nctf.test_lib.ncos.fixtures.virtnetwork.base.basenet import NetTest
from nctf.test_lib.ncos.fixtures.virtnetwork.nose.plugins.attrib import attr

@attr(prio=1)
class FirewallTest(NetTest):

	virt_router_cfg = [{
		'nets': 3
	}]

	txfer_time = 5
	virt_wan_ip = '192.168.1.5'
	router_wan_ip = '192.168.1.10'
	virt_lan_ip = '192.168.0.5'
	router_lan_ip = '192.168.0.1'
	virt_secondary_lan_ip = '192.168.10.5'
	router_secondary_lan_ip = '192.168.10.1'
	fake_wan_ip = '1.2.3.4'

	secondary_lan_id = '00000001-0d93-319d-8220-4a1fb0372b51'

	def setUp(self):

		self.wan['net_veth'].virt_if.addradd("%s/24" % self.virt_wan_ip)
		self.lan['net_veth'].virt_if.addradd("%s/24" % self.virt_lan_ip)
		self.lan['ns'].nsexec('ip route add default via %s' % self.router_lan_ip)

		static_cfg = {
			"ip_mode": "static",
			"static": {
			    "dns": [
				{
				    "ip_address": self.virt_wan_ip
				},
				{}
			    ],
			    "gateway": self.virt_wan_ip,
			    "ip_address": self.router_wan_ip,
			    "netmask": "255.255.255.0"
			}
		}

		config(self.routerns, 'put', 'config.wan.rules2.0', value=static_cfg)

		vlan_cfg = [{
			"mode": "wan",
			"ports": [{
				"mode": "untagged",
				"port": 0
			}],
			"uid": "wan",
			"vid": 1
		}, {
			"mode": "lan",
			"ports": [{
				"mode": "untagged",
				"port": 1
			}],
			"uid": "lan",
			"vid": 2
		}, {
			"mode": "lan",
			"ports": [{
				"mode": "untagged",
				"port": 2
			}],
			"uid": "secondary_lan",
			"vid": 3
		}]

		config(self.routerns, 'put', 'config.vlan', value=vlan_cfg)

		secondary_lan_cfg = {
			"_id_": self.secondary_lan_id,
			"admin_access": True,
			"devices": [
			    {
				"type": "ethernet",
				"uid": "secondary_lan"
			    }
			],
			"dhcpd": {
			    "enabled": True
			},
			"dhcprelay": {
			    "enabled": False
			},
			"enabled": True,
			"ip6_mode": "delegated",
			"ip_address": self.router_secondary_lan_ip,
			"multicast": False,
			"multicast_altnet": [],
			"multicast_quick": True,
			"name": "Secondary LAN",
			"netmask": "255.255.255.0",
			"passthrough_subnet_mode": "auto",
			"route_mode": "nat",
			"schedule": {
			    "day": {
				"dow_1": 0,
				"dow_2": 130816,
				"dow_3": 130816,
				"dow_4": 130816,
				"dow_5": 130816,
				"dow_6": 130816,
				"dow_7": 0
			    },
			    "enabled": False
			},
			"stp": {
			    "enabled": False
			},
			"upnp": True,
			"vrrp": {
			    "enabled": False
			},
			"webfilter_ip_filter": False,
			"webfilter_whitelist": False,
			"wired_8021x": {
			    "enabled": False
			}
		}

		config(self.routerns, 'put', 'config.lan.1', value=secondary_lan_cfg)

		# let the new config take
		time.sleep(2)

		self.secondary_lan = self.virt_routers[0]['lans'][1]
		self.secondary_lan['net_veth'].virt_if.addradd("%s/24" % self.virt_secondary_lan_ip)
		self.secondary_lan['ns'].nsexec('ip route add default via %s' % self.router_secondary_lan_ip)


	def tearDown(self):

		self.secondary_lan['ns'].nsexec('ip route del default via %s' % self.router_secondary_lan_ip)
		self.secondary_lan['net_veth'].virt_if.addrdel("%s/24" % self.virt_secondary_lan_ip)

		self.lan['ns'].nsexec('ip route del default via %s' % self.router_lan_ip)
		self.wan['net_veth'].virt_if.addrdel("%s/24" % self.virt_wan_ip)
		self.lan['net_veth'].virt_if.addrdel("%s/24" % self.virt_lan_ip)

	
	def checkPort(self, port, direction='wan_to_lan', blocked=True):

		directions = {
			'wan_to_lan': {
				'client_ns': self.wan['ns'],
				'srv_ns': self.lan['ns'],
				'srv_ipaddr': self.virt_lan_ip,
				'conn_ipaddr': self.virt_lan_ip
			},
			'wan_to_portfwd': {
				'client_ns': self.wan['ns'],
				'srv_ns': self.lan['ns'],
				'srv_ipaddr': self.virt_lan_ip,
				'conn_ipaddr': self.router_wan_ip
			},
			'lan_to_wan': {
				'client_ns': self.lan['ns'],
				'srv_ns': self.wan['ns'],
				'srv_ipaddr': self.virt_wan_ip,
				'conn_ipaddr': self.virt_wan_ip
			},
			'lan_to_portfwd': {
				'client_ns': self.lan['ns'],
				'srv_ns': self.lan['ns'],
				'srv_ipaddr': self.virt_lan_ip,
				'conn_ipaddr': self.router_wan_ip
			},
			'lan_to_portproxy': {
				'client_ns': self.lan['ns'],
				'srv_ns': self.wan['ns'],
				'srv_ipaddr': self.virt_wan_ip,
				'conn_ipaddr': self.fake_wan_ip
			},
			'lan_to_lan': {
				'client_ns': self.secondary_lan['ns'],
				'srv_ns': self.lan['ns'],
				'srv_ipaddr': self.virt_lan_ip,
				'conn_ipaddr': self.virt_lan_ip
			}
		}

		mbitpersec, bytesrecv = runTestSrvTx(srv_port=port, conn_port=port,
						     proto='tcp', family='inet',
						     txfer_time=self.txfer_time,
						     timeout = self.txfer_time*1.5,
						     **directions[direction])

		if blocked:
			# make sure no data got through
			self.assertEqual(bytesrecv, 0)
		else:
			# received a reasonable amount of data
			self.assertGreaterEqual(bytesrecv, 1024*1024*1)

			# speed wasn't overly horrible
			self.assertGreaterEqual(mbitpersec, 10)
