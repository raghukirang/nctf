"""
Simple base networking test class
"""

__copyright__ = """
Copyright (c) 2014 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. """

import os
import unittest
import subprocess

from time import sleep

from nctf.test_lib.ncos.fixtures.virtnetwork.virtnet.virtrouter import VirtRouter
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.csclient import config
from nctf.test_lib.ncos.fixtures.virtnetwork.nose.plugins.attrib import attr
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.procmon import ProcessMonitor

# TODO: this 'interactive' handling should be in its own base module

# 'interactive' handling is module rather than class so it can be used by
# test modules before they instantiate a class instance.

# interactive expects an environment variable (in a root shell)
# Simple case:  export interactive_=0123456789_presupertearDownClass
# with:
#	[0-9]+ instance ID - the number is the zero-based router instance to make interactive
#	 - multiple numbers can be present to make multiple instances interactive
#	([-_][a-z]+)* wait tag - ID (or tag) where in the test to wait for interaction
#	 - tags be preceeded by dash (-) or by underscore (_)
#	 - multiple tags can be present
# e.g.:  export interactive_bug24542=2-postHTTP-postsupersetUpClass-postsetUpClass-presupertearDownClass
#        	as utilized in test_bug24252.py to make router 2 interactive with 3 wait points
#        export interactive_bug24542=02-postHTTP-postsetUpClass
#        	to make routers 0 and 2 interactive with 2 wait points
_interactive_wait = []
def setUpInteractive(testid=''):
	interactive = os.environ.get('interactive_%s' % (testid,), '')
	if interactive:
		del _interactive_wait[:]
		_interactive_wait.extend(
			interactive.split('-') if '-' in interactive else
			interactive.split('_') if '_' in interactive else
			[interactive]
		)
		print('Interactive_%s routers: %s' %
			(testid or '{default}', (''.join(i for i in _interactive_wait[0] if i in '0123456789') if interactive else 'None')))
		print('Interactives wait at: %s' %
			((', '.join(s for s in _interactive_wait[1:]) if _interactive_wait else 'None')),)
	elif testid:
		print('No interactive routers or waits specific to %s' % (testid,))

def checkForWait(waitid):
	if waitid in _interactive_wait:
		print('\n%s\nWaiting at "%s" for interaction.\nPress <ENTER> to continue'%('-'*20, waitid))
		input()
		print('%s\nContinuing...'%('-'*20))

def checkInteractive(n):
	return _interactive_wait and str(n) in _interactive_wait[0]

@attr(prio=1)
class NetTest(unittest.TestCase):

	# Virtual router configuration, can be
	# overridden in base classes. Only specify
	# what you need to override.  If you only
	# need one router, this may be nothing.
	virt_router_cfg = [{
		# 'name': 'rtr0',
		# 'image': 'fwimage.img',
		# 'outfile': 'serial_{name}.out',
		# 'serial': None,
		# 'output': '-serial file:{outfile}',
		# 'display': 'none',
		# 'preseed': {'license': 'license.json'},
		# 'qmpsock': './qmpsock_{name}',
		# 'interactive': False,
		# 'nets': 2,
		# 'wans': [0],
		# 'etc_dir': None # specifies which etc dir to bind, it binds each netns's /etc to {etc_dir}/{nsname} defaults to /etc/netns/{nsname}
		# 'networking': Set to false disables the creation of networking through host (no wan or lan namespaces)
	}]

	debug_flag = os.environ.get('VIRTROUTER_DEBUG', 'not found')

	@classmethod
	def setUpRouter(cls, cfg):
		""" default in to the virtrouter config any missing arguments that typical tests require.
		"""
		# used further on in this file
		cfg['nets'] = cfg.get('nets', 2)
		cfg['wans'] = cfg.get('wans', (0,))
		# override virtrouter default
		cfg['interactive'] = cfg.get('interactive', False)
		# supply a default
		cfg['image'] = cfg.get('image', 'fwimage.img')
		cfg['name'] = cfg.get('name', 'rtr0')
		cfg['preseed'] = cfg.get('preseed', {'license': 'license.json'})
		cfg['qmpsock'] = cfg.get('qmpsock', 'qmpsock_%s' % cfg['name'])
		cfg['virtsport'] = cfg.get('virtsport', 7777)
		cfg['networking'] = cfg.get('networking', True)
		cfg['display'] = cfg.get('display', 'nographic')
		socket_path = '/tmp/virtcoconut/'
		if not os.path.exists(socket_path):
			os.makedirs(socket_path)
		stdio_socket = socket_path + cfg['name']
		cfg['stdio_socket'] = stdio_socket

		# clean up keys that virtrouter cannot handle
		for k in ('serial', 'outfile'):
			if k in cfg:
				del cfg[k]
		return VirtRouter(**cfg)


	@classmethod
	def setUpClass(cls):
		# check for default interactive
		if not _interactive_wait:
			setUpInteractive()
		if _interactive_wait:
			for n,r in enumerate(cls.virt_router_cfg):
				r['interactive'] = bool(checkInteractive(n))

		unittest.installHandler()

		cls.virt_routers = []
		for r in cls.virt_router_cfg:
			success = False
			for n in range(10):
				vr = { 'router': cls.setUpRouter(r) }

				def exec_after():
					if r.get('interactive'):
						term = os.environ.get('CPTERM') or 'dbus-launch gnome-terminal'
						shell = os.environ.get('SHELL') or 'sh'
						cmd = 'sudo socat -,raw,echo=0,escape=0x1f {}'.format(r.get('stdio_socket'))
						execstr = '{term} -t %h {cmd}'
						cmd = execstr.format(cmd='-e {shell} -e \'{cmd}\''.format(shell=shell, cmd=cmd) if cmd else '', term=term)
						vr['con'] = ProcessMonitor(cmd=cmd, autoRestart=False, ns=vr['router'].routerns)
						vr['con'].start()

				vr['router'].start(exec_after=exec_after)
				vr['routerns'] = vr['router'].routerns

				# Ensure router is working by trying to read status from it
				if cls.debug_flag != 'not found':
					print("***Router:{} try:{}".format(vr['routerns'].nsname,n))
				try:
					fw_info = config(vr['routerns'], 'get', 'status.fw_info')
				except:
					# Socket exception if virtrouter never comes up, try again
					try:
						vr['router'].deinit()
					except:
						pass
					sleep(1)
					continue
				if fw_info.get('build_date') != None:
					success = True
					break
				# This pass failed, try again
				vr['router'].deinit()
				sleep(1)
			if success:
				vr['wans'] = [vr['router'].virtnets[x] for x in r['wans']]
				vr['lans'] = [vr['router'].virtnets[x] for x in range(r['nets']) if x not in r['wans']]
				cls.virt_routers.append(vr)
			else:
				print("No communication with router")
				cls.shutdown()
				raise Exception("No communication with router")
				break

		# Some simple accessors for 'primary' router
		cls.routerns = cls.virt_routers[0]['routerns']
		cls.wan = cls.virt_routers[0]['wans'][0] if cls.virt_routers[0]['wans'] else None
		cls.lan = cls.virt_routers[0]['lans'][0] if cls.virt_routers[0]['lans'] else None

		checkForWait('postsupersetUpClass')

	@classmethod
	def shutdown(cls):
		for vr in cls.virt_routers:
			# Debug output
			if cls.debug_flag != 'not found':
				fn = vr['router'].out[13:]
				print('{}:'.format(fn))
				subprocess.call('cat {}'.format(fn), shell=True)

			vr['router'].deinit()
			if vr.get('con'):
				vr['con'].stop()
				del vr['con']

		cls.virt_routers.clear()

		unittest.removeHandler()
		del _interactive_wait[:]

	@classmethod
	def tearDownClass(cls):
		checkForWait('presupertearDownClass')
		cls.shutdown()

class SimpleNetTest(NetTest):

	virt_router_cfg = [{
		'name': 'rtr0',
		'nets': 2,
	}]

	virt_wan_ip = '192.168.1.5'
	router_wan_ip = '192.168.1.10'
	virt_lan_ip = '192.168.0.5'
	router_lan_ip = '192.168.0.1'

	@classmethod
	def setUpClass(cls):
		super().setUpClass()
		cls.wan['net_veth'].virt_if.addradd("%s/24" % cls.virt_wan_ip)
		cls.lan['net_veth'].virt_if.addradd("%s/24" % cls.virt_lan_ip)
		cls.lan['ns'].nsexec('ip route add default via %s' % cls.router_lan_ip)

	@classmethod
	def tearDownClass(cls):
		cls.wan['net_veth'].virt_if.addrdel("%s/24" % cls.virt_wan_ip)
		cls.lan['net_veth'].virt_if.addrdel("%s/24" % cls.virt_lan_ip)
		super().tearDownClass()
