'''
Client API for Cradlepoint Routers.

This is a simple Python abstraction for the REST interface used in Cradlepoint
Series 3 and newer routers.
'''

__copyright__ = '''
Copyright (c) 2012 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. '''

__author__ = 'Cradlepoint, Inc.'

import argparse
import json
import re
import sys
try:
    # py3k imports and wrappers
    import http.client as http_client
    from urllib.request import HTTPDigestAuthHandler
    bytes2str = lambda x: x.decode()
except ImportError:
    # python 2.7 failback (other versions untested)
    import httplib as http_client
    from urllib2 import HTTPDigestAuthHandler
    bytes2str = lambda x: x


class AuthException(Exception):
    pass


class ValidationException(Exception):
    pass


class CSConnection(http_client.HTTPSConnection):
    """ An authenticated ConfigStore connection.  This is a mashup of the
    python http client module and the urllib digest auth handler.  This
    mashup gives us persistent connections.  Digest auth tokens are cached
    for speedup of repeated requests. """

    def __init__(self, host, username=None, password=None, **kwargs):
        self.username = username if username else 'admin'
        self.password = password
        self.auth_cache = {}
        http_client.HTTPSConnection.__init__(self, host, **kwargs)

    def execute(self, method, url, data=None):
        """ Complete a transaction and return the results. If auth is
        required we will do that too. """

        headers = self.auth_cache.get((method, url), {}).copy()
        if data is not None:
            headers['Content-Type'] = 'application/x-www-form-urlencoded'

        self.request(method, url, body=data, headers=headers)
        resp = self.getresponse()
        if resp.status == http_client.UNAUTHORIZED:
            resp.read()  # flush and close
            if not self._digest_auth(resp, method, url):
                raise AuthException('auth failed')
            return self.execute(method, url, data=data)
        return bytes2str(resp.read())

    def _digest_auth(self, resp, method, url):
        """ Compute the digest auth header value based on url.
        Returns True if a new auth header was created. """

        auth = resp.getheader('WWW-Authenticate')
        chal = {
            'realm': re.search('realm="(.*?)"', auth).group(1),
            'nonce': re.search('nonce="(.*?)"', auth).group(1),
            'algo': re.search('algorithm="(.*?)"', auth).group(1),
            'qop': re.search('qop="(.*?)"', auth).group(1)
        }

        class DummyReq(object):
            data = None
            full_url = url
            get_method = lambda self: method
            get_full_url = lambda x: url
            has_data = lambda x: False
            get_selector = lambda x: url
            selector = url

        auth_handler = HTTPDigestAuthHandler()
        auth_handler.add_password(chal['realm'], url, self.username,
                                  self.password)
        digest = auth_handler.get_authorization(DummyReq(), chal)
        ret = not self.auth_cache.get((method, url))
        self.auth_cache[(method, url)] = {
            'Authorization': 'Digest %s' % digest
        }
        return ret


class ConfigStoreClient(object):
    """ A client frontend for a remote ConfigStore instance.  The
    ConfigStore is the heart of a Cradlepoint router, providing read/write
    access to all configuration items as well as runtime status and
    runtime controls.  The standard layout is as follows:

        /config:  Persistent settings stored in flash and strong validation.
        /status:  Runtime status, logs, and all information about the
                  running system.
        /control: Runtime controls such as reseting device stats or router
                  reboots, etc.

    The main methods for interaction are using a REST based HTTP API.  For
    more information on this protocol see docs/router_rest_spec.md. The
    methods follow the normal HTTP verbs as follows:

        post():   (C)reate new data, usually as an append to an array.
        get():    (R)ead data from the store.
        put():    (U)pdate data in the store.
        delete(): (D)elete delete.

    In addition to the normal CRUD methods described above there is
    provisioning for some extra functionality such as doing firmware
    updates, saving and restoring a config delta, and viewing the DTD and
    defaults for the config.
    """

    exc_map = {
        'key': KeyError,
        'server': Exception,
        'builtinvalidation': ValidationException,
        'servicevalidation': ValidationException,
    }

    def __init__(self, host=None, port=None, username=None, password=None):

        self.conn = CSConnection(host, port=port, username=username,
                                 password=password)

    def crud(method):
        """ Decorator for CRUD methods to parse the json response
        into native python data structures and handle errors in a
        consistent way. """

        def f(instance, *args, **kwargs):
            result = json.loads(method(instance, *args, **kwargs))
            if not result['success']:
                exc = result['data']['exception']
                ExcClass = instance.exc_map[exc]
                raise ExcClass(result['data'])
            return result['data']

        f.__name__ = method.__name__
        f.__doc__ = method.__doc__
        f.__dict__.update(method.__dict__)
        return f

    @crud
    def get(self, base=None, query=None, tree=False, shallow=False):
        """ Get a value and/or dict from the remote ConfigStore.

        base: Sets offset into config structure for results.
              The accessor of sorts.
              Accepts:
                  * string using json accessor notation
                    eg. 'config.system.timezone'

                  * ordered list of nodes to accces
                    eg. ['config', 'system', 'timezone']

        query: JSONPath or list of JSONPath queries to apply after
               the base argument has been traversed.
               See: http://goessner.net/articles/JsonPath/
               eg. '$..foo' or ['$.lan', '$.wan'])

        tree: If set to True requests using multiple jsonpath queries
              will be merged into a single tree instead of a unique
              result set for each query.

        shallow: Only return key value pairs from the first level of
                 the result.  Values that would normally contain a
                 dictionary will just return and empty dictionary or
                 an empty list for list types.

        Returns: Config offset requested or None """

        url = self._restURL(base, query, tree, shallow)
        return self.conn.execute('GET', url)

    @crud
    def put(self, base=None, query=None, tree=False, shallow=False,
            value=None):
        """ Put (UPDATE) a value into the remote ConfigStore.  The
        arguments and general usage are generally the same as the
        get() method with the addition of the 'value' argument.

        Returns: The complete data set located at this offset. """

        url = self._restURL(base, query, tree, shallow)
        data = 'data=%s' % json.dumps(value)
        return self.conn.execute('PUT', url, data=data)

    @crud
    def post(self, base=None, query=None, tree=False, value=None):
        """ Post (APPEND) a new value in the remote ConfigStore. This
        method should only be used to add a new entry to the end of an
        array type.

        Returns: The offset of the newly posted value. """

        url = self._restURL(base, query, tree, None)
        data = 'data=%s' % json.dumps(value)
        return self.conn.execute('POST', url, data=data)

    @crud
    def delete(self, base=None, query=None):
        """ Delete a path or value from the remote ConfigStore.

        Returns: True """

        url = self._restURL(base, query, None, None)
        return self.conn.execute('DELETE', url)

    def _restURL(self, base, query, tree, shallow):
        """ Convert standard arguments into a URL for the REST APIs. """

        args = []
        if tree:
            args.append(('tree', '1'))
        if shallow:
            args.append(('shallow', '1'))
        if query:
            if isinstance(query, str):
                query = [query]
            args.extend([('q', x) for x in query])

        base = base.split('.') if isinstance(base, str) else base
        args = '&'.join('='.join(x) for x in args)
        return '/api/%s?%s' % ('/'.join(base), args)


def run_cmd():
    help_formatter = argparse.ArgumentDefaultsHelpFormatter
    main = argparse.ArgumentParser(description='CradlePoint Series 3 Router '
                                   'Config Store Client',
                                   conflict_handler='resolve',
                                   formatter_class=help_formatter)

    main.add_argument('command', metavar='COMMAND',
                      choices=('get', 'put', 'post', 'delete'),
                      help='Router Command (get|put|post|delete)')
    main.add_argument('path', metavar='PATH', help='Config Store Path')

    main.add_argument('--hostname', metavar='HOSTNAME', default='localhost',
                      help='Hostname of router')
    main.add_argument('--port', metavar='PORT', type=int, default=10443,
                      help='HTTPS port of router')
    main.add_argument('--username', metavar='USERNAME', default='admin',
                      help='Admin username for router login')
    main.add_argument('--password', metavar='PASSWORD', default='00000000',
                      help='Admin password for router login')

    cs_options = main.add_argument_group(title='Config Store Options')
    cs_options.add_argument('--query', nargs='+', help='JSONPath query')
    cs_options.add_argument('--tree', action='store_true',
                            help="Merge results into a single tree")
    cs_options.add_argument('--shallow', action='store_true',
                            help='Return shallow respresentation of the data')

    set_options = main.add_argument_group(title='Put or Post Options')
    g = set_options.add_mutually_exclusive_group()
    g.add_argument('-d', '--data', metavar='RAW_JSON',
                   help='Raw JSON data for put and post value')
    g.add_argument('-f', '--filename', metavar='JSON_FILE',
                   type=argparse.FileType('r'), dest='fhandle',
                   help='File containing raw JSON for put and post '
                   'value')

    args = main.parse_args()
    opts = {'base': args.path}
    for x in 'query', 'tree', 'shallow':
        val = getattr(args, x)
        if val:
            opts[x] = val
    if args.command in ('put', 'post'):
        if args.data is not None:
            data = json.loads(args.data)
        elif args.fhandle:
            data = json.load(args.fhandle)
        else:
            print("Data or filename argument required for: %s" % args.command)
            main.print_usage()
            sys.exit(1)
        opts['value'] = data

    csc = ConfigStoreClient(host=args.hostname, port=args.port,
                            username=args.username, password=args.password)
    print(json.dumps(getattr(csc, args.command)(**opts), indent=4))

if __name__ == '__main__':
    run_cmd()
