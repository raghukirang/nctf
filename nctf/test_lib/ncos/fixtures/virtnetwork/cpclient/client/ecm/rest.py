'''
Client REST API for CradlePoint ECM service.

This is a simple Python abstraction for the REST interface used in the
CradlePoint ECM service.
'''

from __future__ import print_function, division

__copyright__ = '''
Copyright (c) 2012,2013 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. '''

import base64
import collections
import gzip
import io
import logging
import platform
import socket
from . import base, serialization
try:
    # py3k
    import http.client as http_client
    import urllib.parse as urlparse
    urlquote = urlparse.quote
except ImportError:
    import httplib as http_client
    import urlparse
    from urllib import quote as urlquote

API_VERSION = 1
DEFAULT_HOST = 'www.cradlepointecm.com'
VERSION = '4.0.1'


class Redirect(Exception):
    """ Used internally to handle HTTP redirects. """

    def __init__(self, url_raw):
        self.url = urlparse.urlsplit(url_raw)
        super(Redirect, self).__init__(url_raw)


class ECMConnectionMixin:  # XXX: Must be old style to work with py2k httplib.
    """ An authenticated ECM connection. """

    user_agent = 'CradlePointECM/%s (Python %s; %s %s)' % (VERSION,
                 platform.python_version(), platform.system(),
                 platform.machine())

    def setup(self, username, password, compress):
        self.auth_header = self.authHeader(username, password)
        self.compress = compress

    def execute(self, method, url, data=None, headers=None):
        """ Complete a transaction and return the results. """
        headers = headers or {}
        headers.update({
            'Accept': 'application/json',
            'Connection': 'keep-alive',
            'User-Agent': self.user_agent
        })
        if self.auth_header:
            headers['Authorization'] = self.auth_header
        if self.compress:
            headers['Accept-Encoding'] = 'gzip'
        if data is not None:
            headers['Content-Type'] = 'application/json'

        self.request(method, url, body=data, headers=headers)
        resp = self.getresponse()
        data = resp.read()
        if resp.status in (http_client.FOUND, http_client.MOVED_PERMANENTLY):
            raise Redirect(resp.getheader('location'))
        if self.compress and resp.getheader('content-encoding') == 'gzip':
            with gzip.GzipFile(fileobj=io.BytesIO(data)) as f:
                data = f.read()
        return data.decode()

    def authHeader(self, username, password):
        if None not in (username, password):
            creds = '%s:%s' % (username, password)
            return '%s %s' % ('Basic',
                   base64.b64encode(creds.encode()).decode())


class ECMClient(object):
    """ A client frontend for a remote ECM service. """

    def __init__(self, serializer='json', headers=None, username=None,
                 password=None, compress=True, ssl=True, **conn_config):
        if 'host' not in conn_config:
            conn_config['host'] = DEFAULT_HOST
        self.conn_config = conn_config
        self.serializer = serialization.serializers[serializer]
        self.headers = headers
        self.compress = compress
        self.ssl = ssl
        self.username = username
        self.password = password
        self.connect()

    def connect(self):
        HTTPConnection = http_client.HTTPSConnection if self.ssl else \
                         http_client.HTTPConnection

        class ECMConnection(ECMConnectionMixin, HTTPConnection):
            pass
        self.conn = ECMConnection(**self.conn_config)
        self.conn.setup(self.username, self.password, self.compress)
        self.conn.connect()

    def crud(method):
        """ Decorator for CRUD methods to parse args and the response into
        native python data structures and handle errors. """

        def f(instance, *args, **kwargs):
            raw = kwargs.pop('raw', False)
            timeout = kwargs.pop('timeout', False)
            if timeout is not False:
                timeout_save = instance.conn.sock.gettimeout()
                instance.conn.sock.settimeout(timeout)
            try:
                v = method(instance, *args, **kwargs)
            finally:
                if timeout is not False:
                    instance.conn.sock.settimeout(timeout_save)
            try:
                result = instance.serializer['decode'](v)
            except:
                logging.critical("API BUG: Please report this:\n%s" % v)
                logging.critical("Args: %s" % str(args))
                logging.critical("Keyword Args: %s" % str(kwargs))
                raise
            if not result['success']:
                logging.debug("Exception response:\n%s" % v)
                raise base.exc_map.get(result['exception'],
                                       base.RootException)(result)
            return result if raw else result.get('data')

        f.__name__ = method.__name__
        f.__doc__ = method.__doc__
        f.__dict__.update(method.__dict__)
        return f

    def _restURL(self, resource=None, resource_id=None, subresource=None,
                 expand=None, limit=None, offset=None, fields=None, uri=None,
                 **filters):
        """
        Convert standard arguments into a URL for the REST APIs.

        resource:
            A top level resource name such as alerts, accounts, etc.

        resource_id:
            Optional identifier of a resource to return a single
            entry.

        subresource:
            Optional entity for viewing a subset of resources such
            as working with only routers for a particular account.
            Depends on resource_id being set.

        uri:
            Override for resource, resource_id, and subresource.
            Mostly useful if you already have the uri as created by
            the ECM service.

        expand:
            May contain an iterable with field names that you want
            an expanded data set for.  This applies only to
            non-primative fields.

        limit:
            Paging control to limit the number of items returned
            per request.

        offset:
            Paging control to specify the offset for a paged
            request.

        fields:
            May contain an iterable that specifies the fields that
            should be returned.  Aka. Partial Response.

        **filters:
            All extra keyword args are interpreted as filters to the query.
        """
        args = []
        if expand is not None:
            args.append(('expand', ','.join(expand)))
        if limit is not None:
            args.append(('limit', limit))
        if offset is not None:
            args.append(('offset', offset))
        if fields is not None:
            args.append(('fields', ','.join(fields)))
        for key, value in filters.items():
            try:
                iter(value)
            except TypeError:
                pass
            else:
                if not hasattr(value, 'lower'):  # not a string
                    value = ','.join(map(str, value))
            args.append((key, value))
        if not uri:
            uri = ['api', 'v%d' % API_VERSION, resource]
            if resource_id is not None:
                uri.append(resource_id)
                if subresource is not None:
                    uri.append(subresource)
            uri = '/%s/' % '/'.join(map(str, uri))
        if args:
            args = '?%s' % '&'.join('='.join(map(str, x)) for x in args)
        return '%s%s' % (uri, args or '')

    def _conn_exec(self, method, url, **kwargs):
        """ Execute a connection request. """
        max_retries = 3
        logging.debug('%s %s %s' % (method, url, kwargs))
        while max_retries:
            try:
                return self.conn.execute(method, url, headers=self.headers,
                                         **kwargs)
            except Redirect as e:
                if e.url.hostname:
                    self.conn_config['host'] = e.url.hostname
                if e.url.port:
                    self.conn_config['port'] = e.url.port
                elif e.url.scheme == 'https':
                    self.conn_config['port'] = 443
                elif e.url.scheme == 'http':
                    raise ValueError("SSL Required")
                self.close()
                logging.debug("Redirecting to: %s" % e.args[0])
                self.connect()
                url = '%s?%s' % (e.url.path, e.url.query)
            except (http_client.HTTPException, IOError):
                logging.debug("Reconnecting to ECM")
                self.close()
                self.connect()
            max_retries -= 1
        raise IOError("Max retries")

    @crud
    def _get(self, *args, **kwargs):
        """ Get a value and/or list from the remote ECM server.  """
        url = self._restURL(*args, **kwargs)
        return self._conn_exec('GET', url)

    @crud
    def _put(self, *args, **kwargs):
        """ Put (UPDATE) a value into ECM.  The arguments and general
        usage are mostly the same as the get() method with the addition
        of the 'value' argument.

        Returns: The resultant object that is created. """
        value = kwargs.pop('value')
        encoder = kwargs.pop('encoder', self.encoder)
        url = self._restURL(*args, **kwargs)
        data = encoder(value)
        return self._conn_exec('PUT', url, data=data)

    @crud
    def _post(self, *args, **kwargs):
        """ Post (APPEND) a new value in ECM.

        Returns: The resultant object that is created.
        """
        value = kwargs.pop('value')
        url = self._restURL(*args, **kwargs)
        encoder = kwargs.pop('encoder', self.encoder)
        data = encoder(value)
        return self._conn_exec('POST', url, data=data)

    @crud
    def _delete(self, *args, **kwargs):
        """ Delete a path or value from ECM.

        Returns: The resultant object that is created.
        """
        url = self._restURL(*args, **kwargs)
        return self._conn_exec('DELETE', url)

    def query(self, resource_name, *args, **kwargs):
        """
        Return a ResourcesView for this request;  The view can be
        used like a dict view and will handling lazy loading
        from the ECM service as needed.  If the 'id' was in the request
        we return the Resource instance instead.
        """
        page_size = kwargs.pop('page_size', ResourcesView.page_size)
        if page_size > 250:
            raise ValueError("Page size must be <= 250.")
        result = self._get(resource_name, *args, raw=True, limit=page_size,
                           **kwargs)
        if not isinstance(result['data'], list):
            r = Resource(resource_name, result['data'])
            r.bind(self)
            return r
        else:
            view = ResourcesView(resource_name, ecm=self, seed=result,
                                 api_args=args, api_kwargs=kwargs)
            view.page_size = page_size
            return view

    def remote(self, path='', method='get', ignore_result=False, **kwargs):
        """ Perform an operation on the 'remote' resource. These typically
        represent CRUD operations on a client device's config store. """
        call = getattr(self, '_%s' % method)
        limit = ResourcesView.page_size
        path = path.replace('.', '/')
        if ignore_result:
            offset = 0
            filters = list(kwargs.pop('filters', []))
            filters.append('ignore_result = true')
            while True:
                r = call('remote', path, raw=True,
                         limit=limit, offset=offset, filters=filters,
                         **kwargs)
                if not r['meta']['next']:
                    return
                offset += limit
        result = call('remote', path, raw=True, limit=limit, **kwargs)
        kwargs['resource_id'] = path
        return list(ResourcesView('remote', ecm=self, seed=result,
                                  api_kwargs=kwargs, api_method=method))

    def save(self, resource):
        """ Commit resource changes to the ECM service. """
        data = self._put(uri=resource['resource_uri'],
                         value=resource.changes())
        resource.update(data)
        resource.mark_clean()
        return resource

    def create(self, resource):
        """ Commit a new resource to the ECM service. """
        data = self._post(resource.name, value=resource)
        resource.update(data)
        resource.mark_clean()
        if not resource.ecm:
            resource.ecm = self
        return resource

    def delete(self, resource):
        """ Delete an existing resource from the ECM service. """
        self._delete(uri=resource['resource_uri'])

    def close(self):
        if self.conn.sock:
            try:
                self.conn.sock.shutdown(socket.SHUT_RDWR)
                self.conn.close()
            except IOError:
                pass


class ResourcesView(collections.Sequence):
    """ A resources view gives a dynamic window into a group of objects.  It
    supports common iteration patterns and set tests but its contents are
    only fetched on demand to facilitate working with large datasets in a
    lazy load fashion.  """

    page_size = 20

    class Cache(collections.defaultdict):
        def __missing__(self, key):
            return self.default_factory(key)

    def __init__(self, resource, ecm=None, data=None, seed=None,
                 api_args=None, api_kwargs=None, api_method='get'):
        self.resource = resource
        self.ecm = ecm
        self.api_args = api_args
        self.api_kwargs = api_kwargs
        self.api_method = getattr(ecm, '_%s' % api_method)
        self.cache = self.Cache(self.pageFaultHandler)

        if seed is not None:
            self.storePage(seed)
            self.length = seed['meta']['total_count']
        else:
            meta = self.fetchPage()
            self.length = meta['total_count']

    def __repr__(self):
        """ Show the first page. """
        page_size = min(self.length, self.page_size)
        if self.cache and 'id' in self.cache[0]:
            first_page_ids = [self.cache[i]['id'] for i in range(page_size)]
        else:
            first_page_ids = ['<resources without ids>'] if self.cache else []
        return '%s([%s%s], length=%d, cached=%d%%)' % (self.resource,
               ', '.join(first_page_ids),
               ', ...' if self.length > self.page_size else '', self.length,
               ((len(self.cache) / self.length) * 100) if self.length else 0)

    def fetchPage(self, page=0):
        """ Fetch a page of data from ECM and update the cache. Returns the
        request metadata.  """
        result = self.api_method(self.resource, *self.api_args, raw=True,
                                 offset=self.page_size * page,
                                 limit=self.page_size, **self.api_kwargs)
        self.storePage(result)
        return result['meta']

    def storePage(self, result):
        """ Store a page in our cache.  """
        offset = result['meta']['offset']
        page = dict((i, Resource(self.resource, x))
                    for i, x in enumerate(result['data'], offset))
        for x in page.values():
            x.bind(self.ecm)
        self.cache.update(page)

    def pageFaultHandler(self, key):
        """ Fill the cache with the page required for this key.  """
        self.fetchPage(key // self.page_size)
        if key not in self.cache:
            # This happens if the backend changed on us (got smaller)
            # The user should just throw this instance away and start
            # over with a fresh view.
            raise ValueError("Inconsistent ResourcesView")
        return self.cache.get(key)

    def __len__(self):
        return self.length

    def __getitem__(self, x):
        if isinstance(x, slice):
            indices = x.indices(self.length)
            return list(map(self.__getitem__, range(*indices)))
        elif x >= self.length:
            raise IndexError(self.length)
        elif x < 0:
            x = self.length + x
        return self.cache[x]

    def __contains__(self, test):
        """ Check for an match in the cache first, then attempt a single
        resource query for this object if it is the correct type.  """
        if test in self.cache.values():
            return True
        elif isinstance(test, int):
            # Look for this resource ID
            try:
                self.ecm._get(self.resource, resource_id=test)
            except:
                pass
            else:
                return True
        elif isinstance(test, Resource):
            try:
                self.ecm._get(uri=test['resource_uri'])
            except:
                pass
            else:
                return True
        return False


class Resource(dict):
    """ A dictionary like model for an ECM resource. Eg. Router, Account, etc.
    The dict properties are used for storage and submission to ECM. The fetch
    method can be used to pull in subresources dynamically. """

    def __init__(self, name, *args, **kwargs):
        self.name = name
        self.ecm = None
        self.bound = False
        self.changed_keys = set()
        super(Resource, self).__init__(*args, **kwargs)
        self.mark_clean()

    def __setitem__(self, item, value):
        super(Resource, self).__setitem__(item, value)
        self.changed_keys.add(item)

    def bind(self, ecm):
        """ Bind this resource to an ecm client connection so our save()
        method can write to ECM. """
        self.bound = True
        self.ecm = ecm

    def update(self, new):
        super(Resource, self).update(new)
        for x in new:
            self.changed_keys.add(x)

    def fetch(self, subresource):
        """ Fetch a subresources from ECM. """
        if not self.ecm:
            raise RuntimeError("Fetch requires ECMClient binding.")
        uri = self[subresource]
        if not uri:
            raise KeyError("Empty or invalid subresource")
        return self.ecm.query(subresource, uri=uri)

    def mark_clean(self):
        """ Mark our current state as clean so future calls to
        changes() will reflect only new changes. """
        self.changed_keys.clear()

    def changes(self):
        """ Return a dict that represents only changed data. """
        return dict((x, self[x]) for x in self.changed_keys)

    def save(self):
        if not self.ecm:
            raise RuntimeError("Save requires ECMClient binding.")
        if not self.get('resource_uri'):
            return self.ecm.create(self)
        else:
            return self.ecm.save(self)

    def delete(self):
        if not self.ecm:
            raise IOError("ECMClient connection required")
        return self.ecm.delete(self)

    def refresh(self):
        """ Get fresh data from the server for this resource. """
        self.update(self.ecm._get(uri=self['resource_uri']))
        self.mark_clean()
