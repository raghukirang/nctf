'''
Abstractions for poll-like system calls.
'''

from __future__ import print_function

__copyright__ = '''
Copyright (c) 2012,2013 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. '''

import collections
import select

try:
    select.epoll
except AttributeError:
    has_epoll = False
else:
    has_epoll = True


if has_epoll:

    class EPollEventHub(object):

        READABLE = select.EPOLLIN
        WRITABLE = select.EPOLLOUT
        ERROR = select.EPOLLERR
        HANGUP = select.EPOLLHUP

        def __init__(self):
            self.epoll = select.epoll()
            self.register = self.epoll.register
            self.unregister = self.epoll.unregister
            self.modify = self.epoll.modify

        def poll(self, timeout=None, maxevents=None):
            timeout = -1 if timeout is None else timeout
            maxevents = -1 if maxevents is None else maxevents
            return self.epoll.poll(timeout, maxevents)


class SelectEventHub(object):

    READABLE = 1 << 0
    WRITABLE = 1 << 1
    ERROR = 1 << 2
    HANGUP = 1 << 3

    def __init__(self):
        self.read_fds = []
        self.write_fds = []
        self.exc_fds = []
        self.backlog = []

    def register(self, fd, event_mask=READABLE | WRITABLE):
        if hasattr(fd, 'fileno'):
            fd = fd.fileno()
        if fd in self.exc_fds:
            raise Exception("fd already registered")
        if event_mask & (self.ERROR | self.HANGUP):
            self.exc_fds.append(fd)
        if event_mask & self.READABLE:
            self.read_fds.append(fd)
        if event_mask & self.WRITABLE:
            self.write_fds.append(fd)

    def modify(self, fd, event_mask):
        self.unregister(fd)
        self.register(fd, event_mask)

    def unregister(self, fd):
        if hasattr(fd, 'fileno'):
            fd = fd.fileno()
        try:
            self.read_fds.remove(fd)
        except ValueError:
            pass
        try:
            self.write_fds.remove(fd)
        except ValueError:
            pass
        try:
            self.exc_fds.remove(fd)
        except ValueError:
            pass

    def poll(self, timeout=None, maxevents=None):
        if self.backlog:
            events = self.backlog[:maxevents]
            self.backlog = self.backlog[len(events):]
            return events
        fds = select.select(self.read_fds, self.write_fds, self.exc_fds,
                            timeout)
        events = collections.OrderedDict()
        for x in fds[0]:
            events[x] = self.READABLE
        for x in fds[1]:
            if x in events:
                events[x] |= self.WRITABLE
            else:
                events[x] = self.WRITABLE
        for x in fds[2]:
            if x in events:
                events[x] |= self.ERROR
            else:
                events[x] = self.ERROR
        events = [x for x in events.items()]
        if maxevents and len(events) > maxevents:
            self.backlog.extend(events[maxevents:])
        return events[:maxevents]

if has_epoll:
    EventHub = EPollEventHub
else:
    EventHub = SelectEventHub
