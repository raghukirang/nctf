'''
Add some extentions to the normal json serializer to handle ECM style types.
'''

from __future__ import print_function

__copyright__ = '''
Copyright (c) 2013 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. '''


import datetime
import json
import re

API_DATE_RE = re.compile('(?P<dt>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})'
                         '(?P<utc_offset>[\-+]\d{2}:\d{2})')
API_DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'


class ECMJSONEncoder(json.JSONEncoder):
    """ Provide support for more native python field types like datetime. """

    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime(API_DATE_FORMAT) + '+00:00'
        else:
            return super(ECMJSONEncoder, self).default(obj)


class ECMJSONDecoder(json.JSONDecoder):
    """ Provide support for more native python field types like datetime. """

    def __init__(self):
        super(ECMJSONDecoder, self).__init__(object_hook=self.parse_object)

    def parse_object(self, data):
        """ Look for datetime looking strings. """
        for key, value in data.items():
            try:
                value = API_DATE_RE.match(value).groupdict()
                dt = datetime.datetime.strptime(value['dt'], API_DATE_FORMAT)
            except (AttributeError, TypeError, ValueError):
                pass
            else:
                hours, mins = map(int, value['utc_offset'].split(':'))
                if hours < 0:
                    mins *= -1
                if hours:
                    dt -= datetime.timedelta(hours=hours+(mins/60))
                data[key] = dt
        return data


serializers = {
    'json': {
        'encode': ECMJSONEncoder().encode,
        'decode': ECMJSONDecoder().decode
    }
}
