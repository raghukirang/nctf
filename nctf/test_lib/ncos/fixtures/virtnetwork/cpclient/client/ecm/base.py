'''
Base functionality for both the REST and Stream APIs.
'''

from __future__ import print_function

__copyright__ = '''
Copyright (c) 2012,2013 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. '''


class RootException(Exception):
    def __init__(self, result):
        self.result = result
        self.msg = result['message']
        self.name = result['exception']
        super(Exception, self).__init__(self.msg)

    def __str__(self):
        return self.msg


class MissingKey(RootException):
    def __str__(self):
        return '%s: %s' % (self.msg, self.result['key'])


class NotFound(RootException):
    pass


class Unauthorized(RootException):
    pass


class Disabled(RootException):
    pass


class PreconditionFailed(RootException):
    pass


class NotAllowed(RootException):
    pass


class BadRequest(RootException):
    pass


class Forbidden(RootException):
    pass


class NotRegistered(RootException):
    pass


class Timeout(RootException):
    def __init__(self):
        result = {
            'exception': 'timeout',
            'message': 'network timeout',
        }
        super(Timeout, self).__init__(result)


class ServerException(RootException):
    pass


exc_map = {
    'key': MissingKey,
    'notfound': NotFound,
    'integrity': BadRequest,
    'notallowed': NotAllowed,
    'forbidden': Forbidden,
    'server': ServerException,
    'notregistered': NotRegistered,
    'precondition_failed': PreconditionFailed,
    'unauthorized': Unauthorized,
    'disabled': Disabled,
}
