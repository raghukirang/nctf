"""
psutil.py - utility methods for preseeding

Copyright (c) 2010-2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

"""

import logging
import os
from os.path import isfile, join

from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.utils import shcmd
from nctf.test_lib.ncos.fixtures.virtnetwork.virtutil.image import dir2img

logger = logging.getLogger(__name__)
PRESEED_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)))              # 'lib/preseed'
STAGING_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'staging')   # 'lib/preseed/staging'

# The preseed defaults are only invoked if virt_router_cfg['preseed'] != None
# A filename specified in virt_router_cfg will override the default filename
preseed_default = {
	'config': None,			# Default config store filename to grab from /preseed/config folder, ex: config.json
	'license': None,		# Default license filename to grab from /preseed/license folder, ex: license.json
	'code': 'preseed',      # Default preseed code filename to grab from /preseed/code folder
	'dynamic': None         # Folder containing dynamic content to grab from /preseed/dynamic folder
}

def ps2img(preseed, target, vdisk_size, pslock=None):
	""" Copy specified preseed files to staging directory """

	if pslock:  # TODO remove need for lock, use filesystem better
		with pslock:
			return ps2img(preseed, target, vdisk_size, pslock=None)

	#logger.debug("ps2img.preseed:{}".format(preseed))
	#logger.debug("ps2img.target:{}".format(target))
	#logger.debug("PRESEED_PATH: {}".format(PRESEED_PATH))
	#logger.debug("STAGING_PATH: {}".format(STAGING_PATH))

	for (k, v) in preseed.items():
		if k == 'raw':
			shcmd('cp {} {}/{}'.format(v, STAGING_PATH, os.path.basename(v)))
		elif k == 'dynamic':
			if v:
				#logger.debug("cp {}/{}/{}/* {}/dynamic".format(PRESEED_PATH, k, v, STAGING_PATH))
				shcmd('cp {}/{}/{}/* {}/dynamic'.format(PRESEED_PATH, k, v, STAGING_PATH), shell=True)
		elif k == 'folder':
			if v:
				shcmd('cp -r {}/{} {}'.format(PRESEED_PATH, v, STAGING_PATH), shell=True)
		elif v:
			dest = psdest(k)
			if os.path.exists(v):
				path = v
			else:
				path = '{}/{}/{}'.format(PRESEED_PATH, k, v)
			#logger.debug("shcmd string:")
			#logger.debug('cp {} {}/staging/{}'.format(path, PRESEED_PATH, dest))
			shcmd('cp {} {}/staging/{}'.format(path, PRESEED_PATH, dest))
	dir2img(STAGING_PATH, target, vdisk_size)
	psclean(preseed=preseed, preseedimg=None)

def psclean(preseed, preseedimg=None):
	""" Clean preseed files from directory """

	if preseedimg:
		if os.path.isfile(preseedimg):
			os.remove(preseedimg)

	for (k, v) in preseed.items():
		if k == 'dynamic' and v:
			# Remove only the files we put there
			path = '{}/dynamic/{}'.format(PRESEED_PATH, v)
			files = [f for f in os.listdir(path) if isfile(join(path,f))]
			for f in files:
				f = '{}/dynamic/{}'.format(STAGING_PATH, f)
				if os.path.isfile(f):
					os.remove(f)
		if k == 'folder' and v:
			shcmd('rm -rf {}'.format(STAGING_PATH + '/'+ v))
		if k == 'mac':
			if v:
				shcmd('rm {}'.format(STAGING_PATH + '/board.json'))
		else:
			f = psdest(k)
			f = '{}/{}'.format(STAGING_PATH, f)
			if os.path.isfile(f):
				os.remove(f)

def psdest(key):
	""" Return destination filename from preseed key """

	if key == 'code':
		dest = 'preseed'
	else:
		dest = key + '.json'
	return dest

def psmerge(preseed):
	ps = dict(preseed_default)
	ps.update(preseed)
	return ps



