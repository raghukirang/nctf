"""
suppr_patch.py

Copyright (c) 2014 CradlePoint, Inc. <www.cradlepoint.com>.  All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your use of
this file is subject to the CradlePoint Software License Agreement distributed with
this file. Unauthorized reproduction or distribution of this file is subject to civil and
criminal penalties.

Desc: Monkey-patch a method to add delay between WAN up and GRE tunnel up.  For suppression testing.

"""

import services

class SupprPatch(services.Service):

	# Start before GRE service to ensure the patch is in before we need it
	__startup__ = 10

	__dyn_svc__ = {
		"uid": "SUPP-00001",
		"desc": "Monkey-patch for suppression testing",
		"ver": "0.1"
	}

	def onStart(self):
		# Replace restartTunnelAll() method
		setattr(services.gre.GRE, restartTunnelAll.__name__, restartTunnelAll)

		print('Installed monkey patch')

def restartTunnelAll(self, *na):
	# Delay bringing up tunnel to test suppression and such
	try:
		self.delayed
	except:
		self.delayed = True
		services.delayedTask().add(self.restartTunnelAll, 1)
		print('delay..', end='')
		return
	del self.delayed
	if not self.running:
		return

	for _uuid, tun in self.cache.items():
		if not tun.get('nemo'):
			self.restartTunnel(_uuid)
			print('..restart')

# tell service manager about our class
services.register(SupprPatch)



