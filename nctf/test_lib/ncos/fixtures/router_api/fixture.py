import json
import logging
import numbers
import re
import time
from typing import Dict
from typing import Optional
from urllib.parse import urlparse

import requests
import urllib3

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.base.rest.client import RESTClient
from nctf.test_lib.base.rest.endpoint import RESTEndpoint
from nctf.test_lib.utils.waiter import wait
from nctf.test_lib.utils.waiter import WaitTimeoutError

logger = logging.getLogger("restapi.router")

CONFIG_FIREWALL_ENDPOINT = "api/config/firewall"
CONFIG_LAN_ENDPOINT = "api/config/lan"
CONFIG_SECURITY_IPS_ENDPOINT = "api/config/security/ips"
CONFIG_SYSTEM_ENDPOINT = "api/config/system"
CONFIG_VPN_TUNNELS_ENDPOINT = "api/config/vpn/"
CONFIG_WAN_ENDPOINT = "api/config/wan"
CONFIG_WLAN_ENDPOINT = "api/config/wlan"
CONFIG_WWAN_ENDPOINT = "api/config/wwan"
CONFIG_WEBFILTER_ENDPOINT = "api/config/webfilter"
CONFIG_ECM_SERVER_HOST_ENDPOINT = "api/config/ecm/server_host"
CONFIG_STATS_ENDPOINT = "api/config/stats"
CONFIG_STATS_CLIENT_USAGE_ENDPOINT = "api/config/stats/client_usage"
CONFIG_ECM_ENDPOINT = "api/config/ecm"
CONTROL_LOG_ENDPOINT = "api/control/log"
CONTROL_SECURITY_IPS_ENDPOINT = "api/control/security/ips"
CONTROL_SYSTEM_ENDPOINT = "api/control/system"
CONTROL_WLAN_ENDPOINT = "api/control/wlan"
CONTROL_ECM_REGISTER_ENDPOINT = "api/control/ecm/register"
CONTROL_ECM_UNREGISTER_ENDPOINT = "api/control/ecm/unregister"
CONTROL_ECM_RESTART_ENDPOINT = "api/control/ecm/restart"
CONTROL_NETFLOW_ULOG_ENDPOINT = "api/control/netflow/ulog"
ROOT_ENDPOINT = ""
STATUS_FW_INFO_ENDPOINT = "api/status/fw_info"
STATUS_LOG_ENDPOINT = "api/status/log"
STATUS_PRODUCT_INFO_ENDPOINT = "api/status/product_info"
STATUS_SYSTEM_ENDPOINT = "api/status/system"
STATUS_WAN_ENDPOINT = "api/status/wan"
STATUS_WLAN_ENDPOINT = "api/status/wlan"
STATUS_LAN_ENDPOINT = "api/status/lan"
STATUS_ECM_CLIENT_ID_ENDPOINT = "api/status/ecm/client_id"
STATUS_ECM_INFO_ENDPOINT = "api/status/ecm/info"
STATUS_ECM_STATE_ENDPOINT = "api/status/ecm/state"
STATUS_SECURITY_ENDPOINT = "api/status/security"
STATUS_OVERLAY_PGATEWAY_ENDPOINT = "api/status/overlay/pgateway"
STATUS_FEATURE_ENDPOINT = "api/status/feature"
STATUS_STATS = "api/status/stats"

# Modify the api root IP to run the doctests. Needs to be a router you have access to admin.
doctest_api_root = "192.168.0.1"
doctest_user = "admin"
doctest_password = "admin"


class RouterRESTAPIFixture(FixtureBase):
    """
    Fixture for connecting to RouterRESTAPI
    """

    def connect(self, scheme, hostname, port):
        return RouterRESTAPI("{}://{}:{}".format(scheme, hostname, port))

    def get_failure_report(self):
        pass

    def teardown(self):
        pass


class RouterRESTAPI(RESTClient):
    """Class for interacting with a router using the REST interface.

    """

    def __init__(self, api_root: str, timeout: numbers.Number = 60.0, session: Optional[requests.Session] = None) -> None:
        """Constructor

        Args:
            api_root: Root of the REST API to be used.  Should contain the scheme, hostname, and port. e.g. http:foo.com:80
            timeout: Number of seconds to be used as the Session"s timeout.
            session: Session to be used for REST API.
        """
        # Disable warnings that the device is using a self-signed certificate.
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        parse_result = urlparse(api_root)
        protocol = parse_result.scheme
        hostname = parse_result.hostname
        port = parse_result.port
        if port is None:
            if protocol == 'https':
                port = 443
            if protocol == 'http':
                port = 80

        super().__init__(protocol=protocol, hostname=hostname, port=port, timeout=timeout, session=session)

        # Backwards compatibility with RESTAPIInterface attributes.
        self._api_root = api_root
        self._timeout = timeout

        self.config_firewall = ConfigureEndpoint(self, CONFIG_FIREWALL_ENDPOINT)
        self.config_lan = ConfigureEndpoint(self, CONFIG_LAN_ENDPOINT)
        self.config_security_ips = ConfigureEndpoint(self, CONFIG_SECURITY_IPS_ENDPOINT)
        self.config_system = ConfigureEndpoint(self, CONFIG_SYSTEM_ENDPOINT)
        self.config_vpn = ConfigureEndpoint(self, CONFIG_VPN_TUNNELS_ENDPOINT)
        self.config_webfilter = ConfigureEndpoint(self, CONFIG_WEBFILTER_ENDPOINT)
        self.config_wan = ConfigureEndpoint(self, CONFIG_WAN_ENDPOINT)
        self.config_wlan = ConfigWlanEndpoint(self, CONFIG_WLAN_ENDPOINT)
        self.config_wwan = ConfigWwanEndpoint(self, CONFIG_WWAN_ENDPOINT)
        self.config_ecm_server_host = ConfigureEndpoint(self, CONFIG_ECM_SERVER_HOST_ENDPOINT)
        self.config_stats = ConfigureEndpoint(self, CONFIG_STATS_ENDPOINT)
        self.config_stats_client_usage = ConfigureEndpoint(self, CONFIG_STATS_CLIENT_USAGE_ENDPOINT)
        self.config_ecm = ConfigureEndpoint(self, CONFIG_ECM_ENDPOINT)
        self.control_security_ips = ConfigureEndpoint(self, CONTROL_SECURITY_IPS_ENDPOINT)
        self.control_system = ConfigureEndpoint(self, CONTROL_SYSTEM_ENDPOINT)
        self.control_wlan = ConfigureEndpoint(self, CONTROL_WLAN_ENDPOINT)
        self.control_ecm_register = ConfigureEndpoint(self, CONTROL_ECM_REGISTER_ENDPOINT)
        self.control_ecm_unregister = ConfigureEndpoint(self, CONTROL_ECM_UNREGISTER_ENDPOINT)
        self.control_ecm_restart = ConfigureEndpoint(self, CONTROL_ECM_RESTART_ENDPOINT)
        self.control_log = ConfigureEndpoint(self, CONTROL_LOG_ENDPOINT)
        self.control_netflow_ulog = ConfigureEndpoint(self, CONTROL_NETFLOW_ULOG_ENDPOINT)
        self.root = ConfigureEndpoint(self, ROOT_ENDPOINT)
        self.status_fw_info = ConfigureEndpoint(self, STATUS_FW_INFO_ENDPOINT)
        self.status_log = ConfigureEndpoint(self, STATUS_LOG_ENDPOINT)
        self.status_product_info = ConfigureEndpoint(self, STATUS_PRODUCT_INFO_ENDPOINT)
        self.status_system = ConfigureEndpoint(self, STATUS_SYSTEM_ENDPOINT)
        self.status_wan = ConfigureEndpoint(self, STATUS_WAN_ENDPOINT)
        self.status_wlan = ConfigureEndpoint(self, STATUS_WLAN_ENDPOINT)
        self.status_lan = ConfigureEndpoint(self, STATUS_LAN_ENDPOINT)
        self.status_ecm_client_id = ConfigureEndpoint(self, STATUS_ECM_CLIENT_ID_ENDPOINT)
        self.status_ecm_info = ConfigureEndpoint(self, STATUS_ECM_INFO_ENDPOINT)
        self.status_ecm_state = ConfigureEndpoint(self, STATUS_ECM_STATE_ENDPOINT)
        self.status_security = ConfigureEndpoint(self, STATUS_SECURITY_ENDPOINT)
        self.status_overlay_pgateway = ConfigureEndpoint(self, STATUS_OVERLAY_PGATEWAY_ENDPOINT)
        self.status_feature = ConfigureEndpoint(self, STATUS_FEATURE_ENDPOINT)
        self.status_stats = ConfigureEndpoint(self, STATUS_STATS)

    @property
    def _session(self) -> requests.Session:
        """"Backwards compatibility with RESTAPIInterface attributes."""
        return self.session

    def get(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Backwards compatibility with RESTAPIInterface.get()"""
        return super().get(api_uri, expected_status_code, **kwargs)

    def put(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Backwards compatibility with RESTAPIInterface.put()"""
        return super().put(api_uri, expected_status_code, **kwargs)

    def post(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Backwards compatibility with RESTAPIInterface.post()"""
        return super().post(api_uri, expected_status_code, **kwargs)

    def delete(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Backwards compatibility with RESTAPIInterface.delete()"""
        return super().delete(api_uri, expected_status_code, **kwargs)

    def patch(self, api_uri: str, expected_status_code: int = None, **kwargs) -> requests.Response:
        """Backwards compatibility with RESTAPIInterface.patch()"""
        return super().patch(api_uri, expected_status_code, **kwargs)

    def authenticate(self, user_name: str, password: str) -> None:
        """Authenticates the session
        Args:
            user_name: The username associated with the session.
            password: The password associated with the session.
        """
        logger.debug("Authenticate using Basic Auth.")
        self._session.auth = requests.auth.HTTPBasicAuth(user_name, password)
        self._session.verify = False
        if self._validate()["success"] is False:
            logger.debug("Authenticate using Digest Auth.")
            self._session.auth = requests.auth.HTTPDigestAuth(user_name, password)
            self._session.verify = False
        if self._validate()["success"] is False:
            raise requests.RequestException("Unable to authenticate")

    def _validate(self):
        try:
            logger.debug("Get fw_info to verify correct authentication.")
            response = self.status_fw_info.get()
        except requests.ConnectTimeout as e:
            logger.debug("First authentication try failed.  Retrying.  Exception: {}".format(e))
            response = self.status_fw_info.get()
        return response


class ConfigureEndpoint(RESTEndpoint):
    """Class for interacting with firmware settings

    Examples:

       Update a single radio setting.
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root))  # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password)  # doctest:
    >>> update = rest_api.config_wan.get(name="rules_migrated_to_rules2")  # doctest:
    >>> assert update["success"] is True  # doctest:
    >>> assert update["data"] is False  # doctest:

    Get the readonlyconfig setting.
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> get_config = rest_api.control_system.get(name="readonlyconfig") # doctest:
    >>> assert get_config["success"] is True # doctest:

    Update the readonlyconfig setting.
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> post = rest_api.control_system.update(update={"readonlyconfig": False}) # doctest:
    >>> assert post["success"] is True # doctest:

    Update the survey setting.
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> post = rest_api.control_wlan.update(update={"survey": True}) # doctest:
    >>> assert post["success"] is True # doctest:

    Get the connected clients and expect none connected.
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> connection_state = rest_api.status_wan.get(name="connection_state") # doctest:
    >>> assert connection_state["data"] == "connected" # doctest:

    Get the connected clients and expect none connected.
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> get_clients = rest_api.status_wlan.get(name="clients") # doctest:
    >>> assert not get_clients["data"] # doctest:

    Get the connected clients and expect none connected.
    >>> logging.basicConfig(level=logging.DEBUG)
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> get_custom_defaults = rest_api.status_fw_info.get(name="custom_defaults") # doctest:
    >>> assert not get_custom_defaults["data"] # doctest:

    Get the router/ap product info
    >>> logging.basicConfig(level=logging.DEBUG)
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> get_product_info = rest_api.status_product_info.get(name="mac0") # doctest:
    >>> macPattern = re.compile('^([0-9a-f]{2}):[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:([0-9a-f]{2})$')
    >>> assert macPattern.match(get_product_info["data"]) is not None # doctest:

    Get ips mode and app id logging states
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_security_ips.get("mode") # doctest:
    >>> assert response["data"] == 'off', response
    >>> response = rest_api.config_security_ips.get("app_id_logging") # doctest:
    >>> assert response["data"] is False, response

    Update the setting state
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_security_ips.update(update={"mode": "ids"}) # doctest:
    >>> assert response["success"] is True, response
    >>> response = rest_api.config_security_ips.update(update={"app_id_logging": True}) # doctest:
    >>> assert response["success"] is True, response

    Verify the setting change
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_security_ips.get("mode") # doctest:
    >>> assert response["data"] == 'ids', response
    >>> response = rest_api.config_security_ips.get("app_id_logging") # doctest:
    >>> assert response["data"] is True, response

    Restore originall setting
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_security_ips.update(update={"mode": "off"}) # doctest:
    >>> assert response["success"] is True, response
    >>> time.sleep(5)
    >>> response = rest_api.config_security_ips.update(update={"app_id_logging": False}) # doctest:
    >>> assert response["success"] is True, response

    Get the cloudservice state and update to cpswf
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_webfilter.get("cloudservice") # doctest:
    >>> assert response["data"] == 'disabled', response # doctest:
    >>> response = rest_api.config_webfilter.update(update={"cloudservice": "webroot"}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_webfilter.get("cloudservice") # doctest:
    >>> assert response["data"] == 'webroot', response # doctest:
    >>> response = rest_api.config_webfilter.update(update={"cloudservice": "disabled"}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_webfilter.get("cloudservice") # doctest:
    >>> assert response["data"] == 'disabled', response # doctest:

    Get the lan route mode
    >>> logging.basicConfig(level=logging.DEBUG)
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_lan.get('0/route_mode') # doctest:
    >>> assert response["data"] == 'nat', response # doctest:
    >>> response = rest_api.config_lan.get('0/devices') # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_lan.update(name='0/devices', update=[{"type": "ethernet", "uid": "lan"}]) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_lan.update(name=0, update={"route_mode": "passthrough"}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_lan.get('0/route_mode') # doctest:
    >>> assert response["data"] == 'passthrough', response # doctest:
    >>> response = rest_api.config_lan.update(name=0, update={"route_mode": "nat"}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_lan.get('0/route_mode') # doctest:
    >>> assert response["data"] == 'nat', response # doctest:

    Verify that an existing VPN can be enabled and disabled
    >>> logging.basicConfig(level=logging.DEBUG)
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_vpn.get("tunnels") # doctest:
    >>> assert not response["data"], response # doctest:
    >>> response = rest_api.config_vpn.update(name="tunnels", update=[{'local_identity': '', 'remote_gateway': 'Cradlepoint', 'dpd_max_requests': 5, 'protocol': 'any', '_id_': '00000000-bb2b-3f16-b8bd-c1264651d390', 'dpd_enabled': True, 'responder_mode': False, 'ike2_encryption': 'aes 128,aes 256', 'fail_back_to': '', 'unique': 'replace', 'enabled': True, 'ike1_exchange_mode': 'main', 'ike2_dh_group': '5', 'remote_port': 500, 'mobike': True, 'interface_ips': [{}, {}], 'ike1_encryption': 'aes 128,aes 256', 'remote_identity': '', 'fail_back_period': 10, 'revocation': 'relaxed', 'dpd_action_restart': False, 'name': 'Test_Tunnel', 'remote_network': [], 'ike_version': 1, 'remote_identity_type': 'auto', 'auth_method': 'pre_shared_key', 'local_network': [], 'dpd_request_freq': 15, 'wan_trigger_field': 'uid', 'wan_trigger_predicate': 'is', 'interface_nat': False, 'dpd_conn_idle_time': 30, 'fail_over_to': '', 'ike1_dh_group': '5', 'vti_routes': [], 'anonymous': False, 'ike2_pfs': True, 'wan_trigger_neg': False, 'router_services': False, 'initiator_mode': 'demand', 'nat_traversal': True, 'ike1_hash': 'sha256,sha384,sha512', 'ike1_key_lifetime': 28800, 'preshared_key': '$0$vhUJJmx8$g5DEer066r5kM9gBOqV9lg==', 'ike2_key_lifetime': 3600, 'ike2_hash': 'hmac_sha256,hmac_sha384,hmac_sha512', 'pools': [{}, {}], 'force_natt': False, 'local_identity_type': 'auto', 'interface_ip_mode': 'local', 'no_dhcp': False, 'ike2_split_ts': False, 'asn1dn': False, 'mode': 'tunnel'}]) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_vpn.update(name='tunnels/0', update={"enabled": True}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_vpn.get('tunnels/0/enabled') # doctest:
    >>> assert response["data"] is True, response # doctest:
    >>> response = rest_api.config_vpn.update(name='tunnels/0', update={"enabled": False}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_vpn.get(name='tunnels/0/enabled') # doctest:
    >>> assert response["data"] is False, response # doctest:

    Get the Memory Available
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.status_system.get('memory/memavailable') # doctest:
    >>> assert int(response["data"]) > 0

    Set Firewall DMZ
    >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
    >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
    >>> response = rest_api.config_firewall.get("dmz/enabled") # doctest:
    >>> assert response["data"] is False, response # doctest:
    >>> response = rest_api.config_firewall.update(name='dmz', update={"enabled" : True}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_firewall.update(name='dmz', update={"ip_address" : "192.168.0.1"}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_firewall.get("dmz/enabled") # doctest:
    >>> assert response["data"] == True, response # doctest:
    >>> response = rest_api.config_firewall.get("dmz/ip_address") # doctest:
    >>> assert response["data"] == '192.168.0.1', response # doctest:
    >>> response = rest_api.config_firewall.update(name='dmz', update={"enabled" : False}) # doctest:
    >>> assert response["success"] is True, response # doctest:
    >>> response = rest_api.config_firewall.update(name='dmz', update={"ip_address" : "100.100.101.134"}) # doctest:
    >>> assert response["success"] is True, response # doctest:

   """

    # Backwards compatibility with RESTAPIEndpoint
    @property
    def uri(self):
        return self.api_path

    def get(self, name=None) -> Dict:
        """

        Args:
            name:  Name of the node to acquire, defaults to the root node

        Returns:  Dictionary of the JSON node specified from the API call

        """
        logger.debug("Get setting for {}/{}".format(self.uri, name))
        try:
            response = self._get(resource_id=name).json()
        except Exception as e:
            logger.debug("Exception occurred when attempting to get {}/{}".format(self.uri, name))
            logger.debug("Exception: {}".format(e))
            logger.debug("Retry get setting for {}/{}".format(self.uri, name))
            response = self._get(resource_id=name).json()

        if response["success"] is False:
            logger.debug(response)
        return response

    def update(self, name=None, update: Dict = None, service_restart_timeout: int = 0) -> Dict:
        """

        Args:
            name:  Name of the node to modify
            update:  JSON data to modify the node with
            service_restart_timeout: After a configuration change the underlying service may restart.  This is
            the duration of time to wait for the service to restart.

        Returns:  JSON Response to the update, a PUT action

        """
        logger.debug("Update {} for {}/{}".format(update, self.uri, name))

        if update is not None:
            try:
                response = self._put(resource_id=name, data={"data": json.dumps(update)}).json()
            except Exception as e:
                logger.debug("Exception occurred when attempting to update {} for {}/{}".format(update, self.uri, name))
                logger.debug("Exception: {}".format(e))
                logger.debug("Retry update {} for {}/{}".format(update, self.uri, name))
                response = self._put(resource_id=name, data={"data": json.dumps(update)}).json()

            if response["success"] is False:
                logger.debug(response)
            if service_restart_timeout:
                self.wait_for_service(service_restart_timeout)
            return response
        else:
            raise ValueError("Update data must have content")

    def wait_for_service(self, timeout: int = 0):
        """
        Wait for timeout seconds for the service to be ready.
        Args:
            timeout: Duration of time to wait for the service to restart.

        """
        logger.debug("Wait for the service to restart after a configuration change.")
        time.sleep(timeout)


class ConfigWlanEndpoint(ConfigureEndpoint):
    """
    Examples:
        Update a single setting
        >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
        >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
        >>> update = rest_api.config_wlan.update(name="radio/0", update={"channel_selection": "user"}) # doctest:
        >>> assert update["success"] is True # doctest:
        >>> assert update["data"]["channel_selection"] == 'user' # doctest:

        Update a single radio setting.
        >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
        >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
        >>> update = rest_api.config_wlan.update(name="radio/0", update={"channel_selection": "user"}) # doctest:
        >>> assert update["success"] is True # doctest:
        >>> assert update["data"]["channel_selection"] == 'user' # doctest:

        Update a single radio setting and expect failure.
        >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
        >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
        >>> update = rest_api.config_wlan.update(name="radio/0", update={"channel_selection": "user"}) # doctest:
        >>> assert update["success"] is True # doctest:
        >>> radio = rest_api.config_wlan.get("radio/0")["data"] # doctest:
        >>> radio["channel"] = 200 # doctest:
        >>> response = rest_api.config_wlan.update(name="radio/0", update=radio) # doctest:
        >>> assert response["success"] is False # doctest:
        >>> assert response["data"]["reason"] == 'You must select channels [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]' # doctest:

        Enable all radios.
        >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
        >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
        >>> radios = rest_api.config_wlan.get(name="radio/") # doctest:
        >>> for radio_index, radio in enumerate(radios["data"]): # doctest:
        ...     result = rest_api.config_wlan.update(name="radio/" + str(radio_index), update={"enabled": True})
        ...     assert result["success"] is True
        ...     assert result["data"]["enabled"] is True

        Update a BSS setting for all BSS for all radios.
        >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
        >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
        >>> all_radios = rest_api.config_wlan.get("radio/")["data"] # doctest:
        >>> for radio_index, radio in enumerate(all_radios): # doctest:
        ...     for bss_index, bss in enumerate(radio["bss"]):
        ...         update_radio = rest_api.config_wlan.update("radio/{}/bss/{}".format(radio_index, bss_index),
        ...                                                        {"enabled": True})
        ...         assert update_radio["success"] is True
        ...         assert update_radio["data"]["enabled"] is True
    """

    def wait_for_service(self, timeout: int = 0):
        """
        Wait for the wlan service to be ready.  Wait for 5 seconds for the wlan debug state to not equal 1 indicating
        that it is not in a ready state.  If after 5 seconds the state still equals 1 then return.  If the state
        does not equal 1 then wait for timeout seconds for the state to equal 1 and then return.
        Args:
            timeout: Duration of time to wait for the service to restart.

        """
        logger.debug("Wait for wlan service to restart after a configuration change.")
        try:
            logger.debug("Wait 5 seconds for the wlan service to restart.")
            wait(5, 1).for_call(
                lambda: json.loads(self.client.get(f"/{STATUS_WLAN_ENDPOINT}/debug/state").text)["data"]).to_not_equal(1)
        except WaitTimeoutError:
            logger.debug("The wlan service did not restart after the configuration change and is ready.")
        else:
            state = json.loads(self.client.get(f"/{STATUS_WLAN_ENDPOINT}/debug/state").text)["data"]
            logger.debug(f"The wlan service is restarting!  Wait {timeout} seconds for the wlan service to to restart.")
            logger.debug(f"The wlan service state is {state}.")
            wait(timeout, 1).for_call(
                lambda: json.loads(self.client.get(f"/{STATUS_WLAN_ENDPOINT}/debug/state").text)["data"]).to_equal(1)


class ConfigWwanEndpoint(ConfigureEndpoint):
    """
    Examples:
        Update a single radio setting.
        >>> rest_api = RouterRESTAPI(api_root="http://{}".format(doctest_api_root)) # doctest:
        >>> rest_api.authenticate(user_name=doctest_user, password=doctest_password) # doctest:
        >>> update = rest_api.config_wwan.get(name="survey_interval") # doctest:
        >>> assert update["success"] is True # doctest:
        >>> assert update["data"] == 60 # doctest:
    """

    def wait_for_service(self, timeout: int = 0):
        """
        Wait for the wlan service which wwan is dependent on to be ready.  Wait for 5 seconds for the wlan debug state
        to not equal 1 indicating that it is not in a ready state.  If after 5 seconds the state
        still equals 1 then return.  If the state does not equal 1 then wait for timeout seconds for the state
        to equal 1 and then return.
        Args:
            timeout: Duration of time to wait for the service to restart.

        """
        logger.debug("Wait for wlan service to restart after a configuration change.")
        try:
            logger.debug("Wait 5 seconds for the wlan service to restart.")
            wait(5, 1).for_call(
                lambda: json.loads(self.client.get(f"/{STATUS_WLAN_ENDPOINT}/debug/state").text)["data"]).to_not_equal(1)
        except WaitTimeoutError:
            logger.debug("The wlan service did not restart after the configuration change and is ready.")
        else:
            state = json.loads(self.client.get(f"/{STATUS_WLAN_ENDPOINT}/debug/state").text)["data"]
            logger.debug(f"The wlan service is restarting!  Wait {timeout} seconds for the wlan service to to restart.")
            logger.debug(f"The wlan service state is {state}.")
            wait(timeout, 1).for_call(
                lambda: json.loads(self.client.get(f"/{STATUS_WLAN_ENDPOINT}/debug/state").text)["data"]).to_equal(1)


if __name__ == "__main__":
    """Example code"""
    fixture = RouterRESTAPIFixture()
    api = fixture.connect(scheme='http', hostname='192.168.0.1', port=80)
    api.authenticate(user_name='admin', password='44276b68')
    print(api.config_wan.get())
