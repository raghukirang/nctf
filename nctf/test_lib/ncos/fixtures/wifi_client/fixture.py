from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.android import AndroidWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.base import WiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.ios import IOSWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.linux import LinuxWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.osx import OSXWiFiClient


class WiFiClientFixtureError(Exception):
    pass


class NoWiFiClientsAvailableError(WiFiClientFixtureError):
    pass


class WiFiClientFixture(FixtureBase):
    """Test Fixture for establishing a common interface with WiFi Clients.

    """

    def __init__(self):
        self.clients = []

    def get_wifi_client(self, connection_config: dict, client_type: str) -> WiFiClient:
        """

        Args:
            connection_config: The connection config from a leased client.
            client_type: The type of WiFi Client to be connected to.

        """
        wifi_client = WiFiClientFixture._construct_wifi_client(client_type=client_type, connection_config=connection_config)
        self.clients.append(wifi_client)
        return wifi_client

    @staticmethod
    def _construct_wifi_client(client_type: str, connection_config: dict) -> WiFiClient:
        """

        Args:
            client_type: The type of WiFiClient we need to connect to.
            connection_config: The configuration that determines how we talk to the client.

        """
        wifi_client_cls = {
            'android': AndroidWiFiClient,
            'ios': IOSWiFiClient,
            'osx': OSXWiFiClient,
            'linux': LinuxWiFiClient
        }.get(client_type)
        if wifi_client_cls is None:
            raise ValueError('Unsupported client type {}'.format(client_type))

        if not wifi_client_cls.validate_connection_config(connection_config):
            raise ValueError('Invalid {} connection configuration {}'.format(wifi_client_cls, connection_config))
        wifi_client = wifi_client_cls(connection_config)

        return wifi_client

    def teardown(self) -> None:
        for client in self.clients:
            client.teardown()
        self.clients = None

    def get_failure_report(self) -> None:
        pass
