import logging
import re
from typing import List

from nctf.test_lib.ncos.fixtures.wifi_client.connectors.base import WiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.base import WiFiClientScanResult
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.base import WiFiClientStatusResult

logger = logging.getLogger('wifi_client.linux')


class LinuxWiFiClient(WiFiClient):
    __client_config_schema = {}

    def __init__(self, client_config: dict) -> None:
        super().__init__(client_config)
        self.ssh = SSHShellClient(client_config['ssh_host'], client_config['ssh_username'], client_config['ssh_password'],
                                  client_config['ssh_port'])

    def wifi_connect_to_ssid(self, ssid: str, interface: str = None, password: str = None) -> bool:
        cmd = 'sudo nmcli d wifi connect {} password {} {}'.format(ssid, password if password else '', 'ifname ' + interface
                                                                   if interface else '')
        return self.ssh.do_cmd(cmd) == ""

    def wifi_disconnect_from_ssid(self, interface: str) -> bool:
        cmd = 'sudo nmcli d disconnect {}'.format(interface)
        return self.ssh.do_cmd(cmd) == ""

    def wifi_status(self, interface: str) -> WiFiClientStatusResult:
        raise NotImplementedError()

    def wifi_enable(self, interface: str = None) -> bool:
        # unfortunately, one cannot specify which wifi interface to turn off
        cmd = 'nmcli radio wifi on'
        return self.ssh.do_cmd(cmd) == ""

    def wifi_disable(self, interface: str = None) -> bool:
        # unfortunately, one cannot specify which wifi interface to turn off
        cmd = 'nmcli radio wifi off'
        return self.ssh.do_cmd(cmd) == ""

    def wifi_scan(self, interface: str = None) -> list:
        # this command returns a list of access points
        # the -t will separate the fields reported with colons
        # the -f will return the fields specified in the order specified
        # the d is selecting the device which is wifi
        cmd = 'nmcli -t -f SSID,BSSID,SIGNAL,SECURITY,CHAN d wifi list {}'.format('ifname ' + interface if interface else '')

        return self._parse_scan(self.ssh.do_cmd(cmd, True, False))

    def teardown(self) -> None:
        raise NotImplementedError()

    def get_failure_report(self) -> None:
        raise NotImplementedError()

    @staticmethod
    def _parse_scan(stdout: str) -> List[WiFiClientScanResult]:
        """Parse the output of an airport scan to a consumable data structure."""

        scan_results = list()

        # iterated through the access points
        for access_point in stdout:
            # this parses the result that was separated by colons
            # (?<!\\\\) is a negative look behind that means we will not consider "\:"s and only ":"s
            #    this is done because colons are sometimes part of the result for the filed so nmcli puts a backslash in
            #      front of them to make the distinction between a field separator and a regular colon
            #    four backslashes are needed because of how re interprets escape characters
            #    we also get rid of the newline at the end of each stdout element here
            access_point_data = re.split('(?<!\\\\):|\n', access_point)

            scan_result = WiFiClientScanResult()

            # the order of elements in access_point_data is dependent on what order the fields were specified in
            #   the wifi_scan method above
            scan_result.ssid = access_point_data[0]
            # BSSID had colons in the result, so get rid of the backslashes nmcli added
            scan_result.bssid = access_point_data[1].replace('\\', '')
            scan_result.rssi = access_point_data[2]
            scan_result.security = access_point_data[3]
            scan_result.channel = int(access_point_data[4])

            scan_results.append(scan_result)

        return scan_results

    @staticmethod
    def validate_connection_config(config: dict) -> bool:
        # this will need to be defined eventually
        return True

    @property
    def client_config_schema(self) -> None:
        raise NotImplementedError()
