from typing import List

from .base import WiFiClient
from .base import WiFiClientScanResult
from .base import WiFiClientStatusResult


class IOSWiFiClient(WiFiClient):
    __client_config_schema = {}

    def __init__(self, client_config: dict) -> None:
        super().__init__(client_config)

    def wifi_connect_to_ssid(self, ssid: str, password: str=None, interface: str=None) -> bool:
        raise NotImplementedError()

    def wifi_disconnect_from_ssid(self, interface: str=None) -> None:
        raise NotImplementedError()

    def wifi_status(self, interface: str=None) -> WiFiClientStatusResult:
        raise NotImplementedError()

    def wifi_enable(self, interface: str=None) -> bool:
        raise NotImplementedError()

    def wifi_scan(self, interface: str=None) -> List[WiFiClientScanResult]:
        raise NotImplementedError()

    def wifi_disable(self, interface: str=None) -> bool:
        raise NotImplementedError()

    def teardown(self) -> None:
        raise NotImplementedError()

    def get_failure_report(self) -> None:
        raise NotImplementedError()

    @staticmethod
    def validate_connection_config(config: dict) -> bool:
        raise NotImplementedError()

    @property
    def client_config_schema(self) -> None:
        raise NotImplementedError()
