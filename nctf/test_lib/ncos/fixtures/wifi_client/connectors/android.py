import logging
import re
import time
from enum import Enum
from typing import Dict
from typing import Optional

from nctf.test_lib.utils.sshclient.sshclient_paramiko import *
from nctf.test_lib.ncos.fixtures.wifi_client.connectors.base import *
from nctf.test_lib.utils.waiter import wait

logger = logging.getLogger('wifi_client.android')


class AndroidWiFiClient(WiFiClient):
    """Class for interacting with Android clients.
    Will need to remove #doctest: +SKIP to allow test to run
    For wifi_scan() and wifi_status, replace +SKIP with +ELLIPSIS to allow for generic output

    Example:
        >>> config = {
        ... "device_id": "f4306a46",
        ... "ssh_username": "pi",
        ... "ssh_password": "cradlepoint",
        ... "ssh_host": "172.19.11.28",
        ... "ssh_port": 2401,
        ... "model": "Galaxy_S7",
        ... }
        >>> api = AndroidWiFiClient(client_config=config) #doctest: +SKIP
        >>> api.wifi_enable() #doctest: +SKIP
        >>> api.wait_for_wifi_state(api.wifi_state.enabled, 30) #doctest: +SKIP
        >>> api.wifi_connect_to_ssid(ssid="AER3100-231-5g", password="441f4231")  #doctest: +SKIP
        >>> api.wait_for_ssid_state(api.wifi_state.connected, 30)  #doctest: +SKIP
        >>> api.download_file() #doctest: +SKIP
        >>> api.wifi_scan() #doctest: +SKIP
        [<WiFiClientScanResult...>]
        >>> api.wifi_status() #doctest: +SKIP
        <WiFiClientStatusResult(ssid=..., state='3')>
        >>> api.teardown()

    """

    LOGCAT_TAG = 'WifiClient'

    _client_config_schema = {
        "device_id": "string",
        "ssh_username": "string",
        "ssh_password": "string",
        "ssh_host": "string",
        "ssh_port": "int",
        "model": "string"
    }

    def __init__(self, client_config: dict) -> None:
        """Constructor

        Args:
            client_config:The dictionary containing client information for connection.
        """
        super().__init__(client_config)
        self.device_id = client_config['device_id']
        self.ssh = SSHShellClient(client_config['ssh_host'], client_config['ssh_username'],
                                  client_config['ssh_password'], int(client_config['ssh_port']))
        self.wifi_state = self.WiFiState()

    def wifi_connect_to_ssid(self, ssid: str, password: str = None, interface: Optional[str] = None) -> None:
        """Connect to a wifi SSID.

        Args:
            ssid:The SSID to connect to.
            password:The password required to connect to the SSID.
            interface:An optional SSH interface.

        """
        logger.debug("Connect to SSID:{} with password:{}".format(ssid, password))
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiConnect ' \
              '-e ssid {} -e password {}'.format(self.device_id, ssid, password)
        self.ssh.do_cmd(cmd)

    def wifi_disconnect_from_ssid(self, interface: Optional[str] = None) -> None:
        """Disconnect from an existing SSID connection.
        Some clients may immediately reconnect to the SSID

        Args:
            interface:An optional SSH interface.

        """
        logger.debug("Disconnect from connected SSID.")
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiDisconnect'.format(self.device_id)
        self.ssh.do_cmd(cmd)

    def wifi_status(self, interface: Optional[str] = None) -> WiFiClientStatusResult:
        """Get the current wifi status of the android client.

        Args:
            interface:An optional SSH interface.

        Returns:
            A WiFiClientStatusResult containing wifi status information.

        """
        logger.debug("Get current wifi status.")
        self._clear_logcat()
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiStatus'.format(self.device_id)
        self.ssh.change_timeout(300)
        self.ssh.do_cmd(cmd)
        wait(30, 1).for_call(lambda: ''.join(self._get_logcat())).to_contain(":ENDRESULT")
        status = self._parse_status(self._get_logcat())
        return status

    def wifi_enable(self, interface: Optional[str] = None) -> None:
        """Enable wifi on the android client.

        Args:
            interface:An optional SSH interface.

        """
        logger.debug("Enable wifi.")
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiToggle -e wifi true'.format(
            self.device_id)
        self.ssh.do_cmd(cmd)

    def wifi_scan(self, interface: Optional[str] = None) -> List[WiFiClientScanResult]:
        """Scan for SSID's.

        Args:
            interface:An optional SSH interface.

        Returns:
            A WiFiClientScanResult list containing SSID's.

        """
        logger.debug("Scan for SSIDs.")
        self._clear_logcat()
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiScan'.format(self.device_id)
        self.ssh.do_cmd(cmd)
        wait(30, 1).for_call(lambda: ''.join(self._get_logcat())).to_contain(":ENDRESULT")
        scan = self._parse_scan(self._get_logcat())
        return scan

    def wifi_disable(self, interface: Optional[str] = None) -> None:
        """Disable wifi on teh android client.

        Args:
            interface:An optional SSH interface.

        """
        logger.debug("Disable wifi.")
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiToggle -e wifi false'.format(
            self.device_id)
        self.ssh.change_timeout(300)
        self.ssh.do_cmd(cmd)

    def wifi_forget_all_networks(self) -> None:
        """Forget all saved wifi networks on the android client.

        """
        logger.debug("Forget all wifi networks.")
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiForgetAllNetworks'.format(
            self.device_id)
        self.ssh.change_timeout(300)
        self.ssh.do_cmd(cmd)

    def download_file(self) -> None:
        """Download a canned file to generate network traffic.

        """
        logger.debug("Download file.")
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiDownloadFile'.format(self.device_id)
        self.ssh.do_cmd(cmd)

    def delete_downloaded_files(self) -> None:
        """Delete all files in the Download directory on the android client.

        """
        logger.debug("Delete downloaded files.")
        cmd = 'adb -s {} shell am start -n com.cradlepoint.androidwificlient/.WifiDeleteDownloadedFiles'.format(
            self.device_id)
        self.ssh.change_timeout(300)
        self.ssh.do_cmd(cmd)

    def _clear_logcat(self) -> bool:
        """Clear all logcat information.

        Returns:
            A boolean true if the log is successfully cleared.

        """
        logger.debug("Clear logcat.")
        cmd = 'adb -s {} logcat -c'.format(self.device_id)
        self.ssh.change_timeout(300)
        r = self.ssh.do_cmd(cmd)
        return r == ""

    def _get_logcat(self) -> str:
        """Get the current logcat information.

        Returns:
            A string list of logcat information.

        """
        logger.debug("Get logcat.")
        cmd = 'adb -s {} logcat -d -v raw -s {}'.format(self.device_id, self.__class__.LOGCAT_TAG)
        self.ssh.change_timeout(300)
        result = self.ssh.do_cmd(cmd)
        logger.debug("Logcat result: {}.".format(result))
        return result

    @staticmethod
    def _parse_status(stdout: str) -> WiFiClientStatusResult:
        """Parse the logcat information to get the wifi status.

        Args:
            stdout:A list of logcat information.

        Returns:
            The WifiClientStatusResult containing the wifi status.

        """
        logger.debug("Parse logcat status result.")
        s = " ".join(stdout)
        start = "ACTION:wifi_status:BEGINRESULT:"
        end = ":ENDRESULT"
        m = re.search('{}[\s\S]*{}'.format(start, end), s)
        status_result = WiFiClientStatusResult()
        try:
            found = m.group(0)

            status_str = found[len(start):-len(end)]
            status_str = str.strip(status_str)
            data = status_str.split('\t')

            status_result.ssid = AndroidWiFiClient._strip_double_quotes(AndroidWiFiClient._strip_single_quotes(data[1]))
            status_result.bssid = AndroidWiFiClient._strip_single_quotes(data[3])
            status_result.wifi_state = AndroidWiFiClient._strip_single_quotes(data[9])
            status_result.ssid_state = AndroidWiFiClient._strip_single_quotes(data[11])
            status_result.security = None  # TODO: Get this value.
            logger.debug("Status result: {}".format(data))
            logger.debug("Parse logcat status result complete.")

        except AttributeError as e:
            logger.debug("Unable to parse status.  Error: {}".format(e))
        return status_result

    @staticmethod
    def _parse_scan(stdout: str) -> List[WiFiClientScanResult]:
        """Parse the logcat information to get the wifi scan result.

        Args:
            stdout:A list of logcat information.

        Returns:
            The WiFiClientScanResult containing the wifi scan result.

        """
        logger.debug("Parse logcat scan result.")
        scan_results = []
        s = " ".join(stdout)
        start = "ACTION:wifi_scan:BEGINRESULT:"
        end = ":ENDRESULT"
        m = re.search('{}[\s\S]*{}'.format(start, end), s)
        if m:
            found = m.group(0)
            scans_str = found[len(start):-len(end)]
            scans_str = str.strip(scans_str)
            scans = scans_str.split('\n')

            for scan in scans:
                scan_result = WiFiClientScanResult()
                data = scan.split('\t')
                logger.debug("Scan: {}".format(scan))
                scan_result.ssid = AndroidWiFiClient._strip_single_quotes(data[1])
                scan_result.bssid = AndroidWiFiClient._strip_single_quotes(data[3])
                scan_result.rssi = AndroidWiFiClient._strip_single_quotes(data[5])
                scan_result.security = AndroidWiFiClient._strip_single_quotes(data[7])
                scan_results.append(scan_result)
            logger.debug("Parse logcat scan result complete.")
        return scan_results

    @staticmethod
    def _strip_single_quotes(s: str) -> str:
        """Strip single quotes from a string.

        Args:
            s:A string containing single quotes.

        Returns:
            A string with the single quotes removed.

        """
        return s[1:-1] if s.endswith("'") and s.startswith("'") else s

    @staticmethod
    def _strip_double_quotes(s: str) -> str:
        """Strip double quotes from a string.

        Args:
            s:A string containing double quotes.

        Returns:
            A string with the double quotes removed.

        """
        return s[1:-1] if s.endswith("\"") and s.startswith("\"") else s

    def teardown(self) -> None:
        """Cleanup of the android client.

        """
        logger.debug("Begin client teardown.")
        self.delete_downloaded_files()
        self.wifi_forget_all_networks()
        self._clear_logcat()
        logger.debug("Client teardown complete.")

    def get_failure_report(self) -> None:
        raise NotImplementedError

    @staticmethod
    def validate_connection_config(config: dict) -> bool:
        logger.warning("TODO: Implement config validation")
        return True

    @classmethod
    def client_config_schema(self) -> Dict:
        """The client config schema required to connect to the android client.

        Returns:
            The dictionary client config schema.

        """
        return self.__class__._client_config_schema

    def wait_for_wifi_state(self, state: str, timeout: int=30) -> None:
        """Wait for a particular state of the wifi device on the android client.

        Args:
            state:The wifi state to wait for.
            timeout:The total time to wait for the wifi state.

        """
        logger.debug("Wait for wifi state {} for {} seconds.".format(state, timeout))
        wait(timeout, 1).for_call(lambda: self.wifi_status().wifi_state).to_equal(state)

    def wait_for_ssid_state(self, state: str, timeout: int=30) -> None:
        """Wait for a particular sssid tate of the wifi device on the android client.

        Args:
            state:The wifi state to wait for.
            timeout:The total time to wait for the wifi state.

        """
        logger.debug("Wait for wifi state {} for {} seconds.".format(state, timeout))
        wait(timeout, 1).for_call(lambda: self.wifi_status().ssid_state).to_equal(state)

    class WiFiState(WiFiClient.WiFiState):
        def __init__(self):
            super().__init__(connected="CONNECTED", disabled="1", disconnected="DISCONNECTED", enabled="3")
