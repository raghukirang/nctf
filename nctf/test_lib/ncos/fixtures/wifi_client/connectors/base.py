import abc
from typing import List
from enum import Enum


class WiFiClientStatusResult(object):
    """The status of a WiFiClient"""
    def __init__(self, ssid: str=None, bssid: str=None, security: str=None, wifi_state: str=None, ssid_state=None) -> None:
        self.ssid = ssid
        self.bssid = bssid
        self.security = security
        self.wifi_state = wifi_state
        self.ssid_state = ssid_state

    def __repr__(self):
        return "<{}(ssid='{}', state='{}')>".format(self.__class__.__name__, self.ssid, self.wifi_state)


class WiFiClientScanResult(object):
    """A singular wifi broadcast data set as delivered by a WiFi Client"""
    def __init__(self, ssid: str=None, bssid: str=None, security: str=None, rssi: str=None, channel: int=None) -> None:
        self.ssid = ssid
        self.bssid = bssid
        self.security = security

        self.rssi = rssi
        self.channel = channel

    def __repr__(self):
        return "<{}(ssid='{}', security='{}')>".format(self.__class__.__name__, self.ssid, self.security)


class WiFiClient(metaclass=abc.ABCMeta):
    def __init__(self, client_config: dict) -> None:
        self.client_config = client_config
        self.wifi_state = self.WiFiState()

    @abc.abstractmethod
    def wifi_connect_to_ssid(self, ssid: str, password: str) -> None:
        """Connect a wlan interface to a wireless network."""
        pass

    @abc.abstractmethod
    def wifi_disconnect_from_ssid(self) -> None:
        """Disconnect a wlan interface from the wireless network."""
        pass

    @abc.abstractmethod
    def wifi_status(self, interface: str) -> WiFiClientStatusResult:
        """Retrieve the wifi status of the client on some interface."""
        pass

    @abc.abstractmethod
    def wifi_enable(self) -> None:
        """Enable wifi on the client."""
        pass

    @abc.abstractmethod
    def wifi_scan(self) -> List[WiFiClientScanResult]:
        """Retrieve a list of networks available to the client"""
        pass

    @abc.abstractmethod
    def wifi_disable(self) -> None:
        """Disable wifi on the client."""
        pass

    @abc.abstractmethod
    def wait_for_wifi_state(self, state: str, timeout: int) -> None:
        """Wait for wifi to achieve a particular state"""
        pass

    @abc.abstractmethod
    def wait_for_ssid_state(self, state: str, timeout: int) -> None:
        """Wait for wifi ssid to achieve a particular state"""
        pass

    @staticmethod
    @abc.abstractstaticmethod
    def validate_connection_config(config: dict) -> bool:
        """Validate the JSON that defines how we connect to the wifi client."""
        pass

    @property
    @abc.abstractmethod
    def client_config_schema(self) -> None:
        pass

    @abc.abstractmethod
    def teardown(self) -> None:
        pass

    @abc.abstractmethod
    def get_failure_report(self) -> None:
        pass

    class WiFiState(metaclass=abc.ABCMeta):
        def __init__(self, connected: str=None, disabled: str=None, disconnected: str=None, enabled: str=None):
            self.connected = connected
            self.disabled = disabled
            self.disconnected = disconnected
            self.enabled = enabled

