import logging
import time
from typing import List, Dict
from enum import Enum

from nctf.test_lib.utils.sshclient.sshclient_paramiko import *

from nctf.test_lib.ncos.fixtures.wifi_client.connectors.base import *
from nctf.test_lib.utils.waiter import wait

logger = logging.getLogger('wifi_client.android')


class OSXWiFiClient(WiFiClient):
    """http://osxdaily.com/2007/01/18/airport-the-little-known-command-line-wireless-utility/"""

    _client_config_schema = {
        "ssh_username": "string",
        "ssh_password": "string",
        "ssh_host": "string",
        "ssh_port": "int",
        "model": "string",
    }

    _airport_path = '/System/Library/PrivateFrameworks/Apple80211.framework/Versions/A/Resources/airport'

    def __init__(self, client_config: dict) -> None:
        super().__init__(client_config)
        self.ssh = SSHShellClient(client_config['ssh_host'], client_config['ssh_username'],
                                  client_config['ssh_password'], client_config['ssh_port'])
        self._interface = self._get_device_port()
        self.wifi_state = self.WiFiState()

    def wifi_connect_to_ssid(self, ssid: str, password: str) -> bool:
        logger.debug("Connect to SSID:{} with password:{}".format(ssid, password))
        cmd = 'networksetup -setairportnetwork {} {} {}'.format(self._interface, ssid, password)
        return self.ssh.do_cmd(cmd) == ""

    def wifi_disconnect_from_ssid(self) -> bool:
        logger.debug("Disconnect from connected SSID.")
        cmd = 'sudo {} --disassociate'.format(self._airport_path)
        return self.ssh.do_cmd(cmd) == ""

    def wifi_status(self, interface: str=None) -> WiFiClientStatusResult:
        logger.debug("Get current wifi status.")
        cmd = '{} --getinfo'.format(self._airport_path)
        ssh = self.ssh.do_cmd(cmd, True, False)
        status = self._parse_status(ssh.decode().splitlines())
        logger.debug("Current status: {}".format(status))
        return status

    def wifi_enable(self) -> bool:
        logger.debug("Enable wifi.")
        cmd = 'networksetup -setairportpower {} on'.format(self._interface)
        enable = self.ssh.do_cmd(cmd)
        # OSX returns the init state when wifi is turned on even if it is not quite ready.  Sleep added to work around.
        time.sleep(5)
        return enable == ""

    def wifi_scan(self, interface: str=None) -> List[WiFiClientScanResult]:
        logger.debug("Scan for SSIDs.")
        cmd = '{} --scan'.format(self._airport_path)
        ssh = self.ssh.do_cmd(cmd, True, False)
        scan = ssh.decode().splitlines()
        return self._parse_scan(scan)

    def wifi_disable(self) -> bool:
        logger.debug("Disable wifi.")
        cmd = 'networksetup -setairportpower {} off'.format(self._interface)
        return self.ssh.do_cmd(cmd) == ""

    def download_file(self) -> None:
        """Download a canned file to generate network traffic.

        """
        logger.debug("Download file.")
        params = '-L --referer ";auto" -o'
        url = 'https://cradlepoint.atlassian.net/wiki/download/attachments/25756212/Mutt_Details.pdf?version=1&modificationDate=1434053864178&cacheVersion=1&api=v2'
        output = '~/Downloads/Mutt_Details.pdf'
        cmd = 'curl {} {} {}'.format(params, output, url)
        self.ssh.do_cmd(cmd)

    def delete_downloaded_files(self) -> None:
        """Delete all files in the Download directory on the android client.

        """
        logger.debug("Delete downloaded files.")
        cmd = 'rm -rf ~/Downloads/*'
        self.ssh.do_cmd(cmd)

    def teardown(self) -> None:
        logger.debug("Begin client teardown.")
        self.delete_downloaded_files()
        self._remove_all_stored_wifi_network_profiles()
        logger.debug("Client teardown complete.")

    def get_failure_report(self) -> None:
        pass

    def wait_for_wifi_state(self, state: str, timeout: int=30) -> None:
        """Wait for a particular state of the wifi device on the OSX client.

        Args:
            state:The wifi state to wait for.
            timeout:The total time to wait for the wifi state.

        """
        wait(timeout, 1).for_call(lambda: self.wifi_status().wifi_state).to_equal(state)

    def wait_for_ssid_state(self, state: str, timeout: int = 30) -> None:
        """Wait for a particular ssid state of the wifi device on the OSX client.

        Args:
            state:The wifi state to wait for.
            timeout:The total time to wait for the wifi state.

        """
        wait(timeout, 1).for_call(lambda: self.wifi_status().ssid_state).to_equal(state)

    def _remove_all_stored_wifi_network_profiles(self) -> str:
        logger.debug("Forget all wifi networks.")
        cmd = 'sudo networksetup -removeallpreferredwirelessnetworks {}'.format(self._interface)
        return self.ssh.do_cmd(cmd)

    @staticmethod
    def _parse_status(stdout: list) -> WiFiClientStatusResult:
        """Parse the output of an airport status call to a consumable data structure."""
        logger.debug("Parse status result.")

        status_result = WiFiClientStatusResult()

        lines = stdout
        for line in lines:
            words = line.split()
            if len(words) >= 2:
                if words[0] == 'state:':
                    status_result.wifi_state = words[1]
                    status_result.ssid_state = words[1]
                if words[0] == 'BSSID:':
                    status_result.bssid = words[1]
                if words[0] == 'SSID:':
                    status_result.ssid = words[1]
                if words[0] == 'link' and words[1] == 'auth:':
                    status_result.security = words[2]
                if words[0] == 'AirPort:' and words[1] == 'Off':
                        status_result.wifi_state = 'disabled'
        logger.debug("Status result: {}".format(lines))
        logger.debug("Parse status result complete.")
        return status_result

    @staticmethod
    def _parse_scan(stdout: list) -> List[WiFiClientScanResult]:
        """Parse the output of an airport scan to a consumable data structure."""
        logger.debug("Parse scan result.")

        scan_results = list()
        lines = stdout
        if len(lines) < 2:
            return scan_results
        else:
            lines = lines[1:]  # Pop the table header.
        for line in lines:
            words = line.split()
            scan = WiFiClientScanResult()
            scan.ssid = words[0]
            scan.bssid = words[1]
            scan.rssi = words[2]
            scan.channel = words[3]
            scan.security = words[6]
            scan_results.append(scan)
            logger.debug("Scan result: {}".format(scan))
        logger.debug("Parse scan result complete.")
        return scan_results

    def _get_device_port(self) -> str:
        logger.debug("Get wifi device port.")
        port = ''
        cmd = 'networksetup -listnetworkserviceorder'
        ports = self.ssh.do_cmd(cmd)
        for line in ports:
            if 'Hardware Port: Wi-Fi' in line:
                line_split = line.split(":")
                port = line_split[2].replace(')', '').strip()
        return port

    @staticmethod
    def validate_connection_config(config: dict) -> bool:
        return True

    @property
    def client_config_schema(self) -> Dict[str, str]:
        return self.__class__._client_config_schema

    class WiFiState(WiFiClient.WiFiState):
        def __init__(self):
            super().__init__(connected="running", disabled="disabled", disconnected="init", enabled="init")
