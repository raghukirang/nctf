import json
import logging
import os
from pathlib import Path
import time
import urllib3

import requests
from requests_toolbelt import MultipartEncoder

import requests
from nctf.libs.common_library import WaitTimeoutError

from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI

logger = logging.getLogger("restapi.router")


class BinOperationsRebootTimeout(Exception):
    pass


class BinOperations(object):
    """Class for interacting with .bin files.

    Examples:
        Save a config bin file.
        >>> bin_ops = BinOperations(host_name="http://192.168.0.1:80", user_name="admin", password="pass") # doctest: +SKIP
        >>> path = "{}{}".format(Path.home(), "/Downloads") # doctest: +SKIP
        >>> bin_ops.save_config_bin_file(destination=path) # doctest: +SKIP

        Load a config bin file.
        >>> bin_ops = BinOperations(host_name="http://192.168.0.1:80", user_name="admin", password="pass") # doctest: +SKIP
        >>> bin_ops.load_config_bin_file(file="{}{}{}".format(Path.home(), "/Downloads/", "config.bin")) # doctest: +SKIP

    """

    def __init__(self, host_name: str, user_name: str, password: str) -> None:
        self.api = RouterRESTAPI(api_root=host_name)
        self.api.authenticate(user_name=user_name, password=password)

    def save_config_bin_file(self, destination: str) -> None:
        """Save a config bin file retrieved from the router to the specified destination.

        Args:
            destination: The file path destination to place the bin file.

        """
        response = self.api.get(api_uri="/config_save", expected_status_code=200)
        name = response.headers['Content-Disposition'].split("=", 1)[1].replace('"', '')
        full_path = os.path.join(destination, name)
        with open(full_path, "wb") as f:
            f.write(response.content)

    def load_config_bin_file(self, file: str, timeout: int = 900) -> None:
        """Load a config bin file to the router.

        Args:
            file: The full path including the bin file to be loaded to the router.
            timeout: The maximum amount of time in seconds to wait for the device to boot with a wan connection.

        """
        boot_id = self.api.status_system.get(name="bootid")["data"]
        with open(file, 'rb') as f:
            data = f.read()
        response = self.api.post(api_uri="/config_save", files={'config_file': data})
        if not (response.json()["success"] or response.json()["data"] == "valid"):
            logger.info(f"config_bin post failed.  response={response} \r\nresponse.json={response.json()}")
            raise requests.exceptions.RequestException("Bin file post was unsuccessful.")
        self._wait_for_reboot_complete(boot_id=boot_id, timeout=timeout)

    def load_firmware_bin_file(self, file: str, timeout: int = 900) -> None:
        """Load a firmware bin file to the router.

        Args:
            file: The full path including the bin file to be loaded to the router.
            timeout: The maximum amount of time in seconds to wait for the device to boot with a wan connection.

        """
        boot_id = self.api.status_system.get(name="bootid")["data"]
        multi_encoder = MultipartEncoder(fields={'fw_file': (file, open(file, 'rb'), 'application/octet-stream')})
        logger.info("Load firmware file {}.".format(file))
        response = self.api.post(
            api_uri='/fw_upgrade',
            data=multi_encoder,
            params={
                'factory_reset': False
            },
            headers={'Content-Type': multi_encoder.content_type})
        if not (response.json()["success"] or response.json()["data"] == "valid"):
            logger.info(f"firmware bin post failed.  response={response} \r\nresponse.json={response.json()}")
            raise requests.exceptions.RequestException("Bin file post was unsuccessful.")
        self._wait_for_reboot_complete(boot_id=boot_id, timeout=timeout)
        logger.info("Firmware file {} was loaded successfully.".format(file))

    def reboot_router(self, reboot_timeout: int = 900):
        """Reboot a router.

        Args:
            reboot_timeout: The maximum amount of time in seconds to wait for the device to boot with a wan connection.

        """
        logger.debug("Reboot device.")
        boot_id = self.api.status_system.get(name="bootid")["data"]
        response = self.api.put(api_uri='/api/control/system', data={'data': json.dumps({"reboot": True})})
        if not (response.json()["success"] or response.json()["data"] == "valid"):
            raise requests.exceptions.RequestException("Reboot request was unsuccessful.")
        self._wait_for_reboot_complete(boot_id=boot_id, timeout=reboot_timeout)

    def _wait_for_reboot_complete(self, boot_id: int, timeout: int):
        logger.debug("Wait for device reboot to complete.")
        logger.debug("Start boot id is {}".format(boot_id))
        start_time = time.time()
        while time.time() < start_time + timeout:
            try:
                response = self.api.status_system.get(name="bootid")
                if "data" not in response:
                    logger.debug("No data available in response.  Retrying.")
                    logger.debug("Response is: {}".format(response))
                    time.sleep(5)
                    continue
                id = response["data"]
                logger.log(level=15, msg="Current boot id is {}".format(id))
                if id == boot_id:
                    time.sleep(5)
                    continue
                wan_connection_state = self.api.status_wan.get("connection_state")["data"]
                logger.debug("Wan connection state: {}".format(wan_connection_state))
                if wan_connection_state == "connected":
                    complete_time = time.time() - start_time
                    logger.debug("Device reboot complete in {} seconds.".format(complete_time))
                    break
            except (requests.ConnectionError, requests.Timeout, urllib3.exceptions.NewConnectionError):
                # A smorgasbord of errors can happen during upgrades and reboots, but they're
                # only (? == engineering judgement) a problem if they
                # are sticky as a symptom that causes the timeout to expire.  Ignoring transients here.
                logger.debug("Device is supposed to be rebooting.")
            time.sleep(2)
        if time.time() >= start_time + timeout:
            raise BinOperationsRebootTimeout("Device did not reboot in {} seconds.".format(timeout))
