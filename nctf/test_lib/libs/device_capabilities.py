from enum import Enum
import json
import logging
import os
from typing import List

ROUTER_CAPABILITIES = os.path.join(os.path.dirname(__file__), 'router_capabilities.json')

logger = logging.getLogger('test_lib.libs.device_capabilities')


class DeviceModel(Enum):
    """Enum containing all device model names.

    """
    AER1600 = "AER1600", "aer1600"
    AER1650 = "AER1650", "aer1650"
    AER2100 = "AER2100", "aer2100"
    AER2200 = "AER2200", "aer2200"
    AER2200FIPS = "AER2200FIPS", "aer2200.fips"
    AER2250 = "AER2250", "aer2250"
    AER2250FIPS = "AER2250FIPS", "aer2250.fips"
    AER3100 = "AER3100", "aer3100"
    AER3150 = "AER3150", "aer3150"
    AP22 = "AP22", "ap22"
    CBA850 = "CBA850", "cba850"
    HERC = "HERC", "herc"
    IBR200 = "IBR200", "ibr200"
    IBR250 = "IBR250", "ibr250"
    IBR350 = "IBR350", "ibr350"
    IBR600 = "IBR600", "ibr600"
    IBR600B = "IBR600B", "ibr600b"
    IBR600C = "IBR600C", "ibr600c"
    IBR650 = "IBR650", "ibr650"
    IBR650B = "IBR650B", "ibr650b"
    IBR650C = "IBR650C", "ibr650c"
    IBR900 = "IBR900", "ibr900"
    IBR900FIPS = "IBR900FIPS", "ibr900.fips"
    IBR950 = "IBR950", "ibr950"
    IBR950FIPS = "IBR950FIPS", "ibr950.fips"
    IBR1100 = "IBR1100", "ibr1100"
    IBR1150 = "IBR1150", "ibr1150"
    IBR1700 = "IBR1700", "ibr1700"
    IBR1700FIPS = "IBR1700FIPS", "ibr1700.fips"
    LETHE = "LETHE", "lethe"
    MBR1200B = "MBR1200B", "mbr1200b"
    VCOCO3200V = "3200v", "3200v"

    def __new__(cls, arm, guido):
        obj = object.__new__(cls)
        obj._value_ = arm
        cls._value2member_map_[guido] = obj
        obj.guido = guido
        return obj


class DeviceCapability(Enum):
    """Device capabilities.

    """
    WIFI = "wifi"
    WIFI_2_4GHZ = "wifi_2.4"
    WIFI_5GHZ = "wifi_5"
    WIFI_AS_WAN = "wifi_as_wan"
    WIRELESS_BRIDGE = "wireless_bridge"
    WIRELESS_CLIENT = "wireless_client"
    DATA_RATES = "data_rates"
    DFS = "dfs"
    DFS_AP = "dfs_ap"
    DFS_CLIENT = "dfs_client"
    WIFI_AS_WAN_HIDDEN_CONNECT = "wifi_as_wan_hidden_connect"
    WIFI_AS_WAN_RADIUS_SUPPORT = "wifi_as_wan_radius_support"


class DeviceCapabilities(object):
    """Class for interacting with device capabilities source files.

    """

    def get_devices_by_capabilities_supported(self, capabilities: List[DeviceCapability]) -> List[DeviceModel]:
        """Get a list of devices that support a list of features.

        Args:
            capabilities: A list of capabilities a device

        Returns:
            A list of device models that support all of the passed in features.

        """
        logger.debug("Getting the list of device models supporting the features: {}.".format(capabilities))
        capability_list = []
        capabilities_values = [capability.value for capability in capabilities]
        with open(ROUTER_CAPABILITIES) as json_file:
            parsed_json = json.load(json_file)
        for mod in parsed_json['devices']:
            parsed_features = {option: mod['features'][option] for option in capabilities_values if option in mod['features']}
            if all(value is True for value in parsed_features.values()):
                capability_list.append(mod['name'])
        logger.debug("The supporting device models are:  {}".format(capability_list))
        return capability_list
