import enum
import logging
import typing

from nctf.test_lib.accounts.rest import AccServRESTClient
import requests

logger = logging.getLogger("fixtures.netcloud_account")


class CreationMethod(enum.Enum):
    ACCOUNTS = "accounts"
    SALESFORCE = "salesforce"
    SALESFORCE_MOCK = "salesforce_mock"


class ActivationMethod(enum.Enum):
    UI = "ui"
    API = "api"


class AcceptTOSMethod(enum.Enum):
    UI = "ui"
    API = "api"


class AccServActor(object):
    """A customer using any NetCloud service.

    The Accounts Service Actor is the object wrapper over the accounts-<SUT>/api/v1/users/<id> endpoint.

    Args:
        username: The username of the actor in the form of an email address. If no password is provided here it must be given
            when calling activate() or authenticate().
        password: The password to log into NetCloud products. If password is not given now, it must be provided on the call to
            activate() and or on the call to authenticate().
        password_token: The token used in activating and setting the initial password of the actor.
        is_activated: A boolean indicating the initial state of the actor wrt being activated. It is defaulted to cause this
            object to query the end point for its state. A conundrum since if the actor isn't activated, it can't be
            authenticated and can't call to get the answer. So, the constructor, likely the account, needs to set this.
        netcloud_account: The creating account in the event that this actor is being created in NetCloud upon initialization.
        accounts_api_protocol: optional protocol for the accounts service. If not provided here, must be given on activate or
        authenticate calls.
        accounts_api_hostname: optional hostname for the accounts service. If not provided here, must be given on activate or
        authenticate calls.
        accounts_api_port: optional port for the accounts service. If not provided here, must be given on activate or
        authenticate calls.
        timeout: optional timeout in seconds for calling the endpoint, default is 30 seconds.

    """

    class Roles(enum.Enum):
        ADMIN = "admin"
        USER = "user"
        ROOT_ADMIN = "rootAdmin"
        # cannot use the following through account.create_user(role=role)
        # FULL_ACCESS = "full_access"
        # READ_ONLY = "read_only"
        # ACCOUNT_READ_ONLY = "account_read_only"
        # REGISTRAR = "registrar"
        # ROUTER = "router"
        # FIRMWARE_UPLOADER = "firmware_uploader"
        # ROOT = "root"
        # FIRMWARE_READER = "firmware_reader"
        # BOONGLER = "boongler"
        # MIGRATER = "migrator"
        # ENTITLER = "entitler"
        # DIAGNOSTIC = "diagnostic"
        # VCOCO_DATA = "vcoco_data"
        # USER_MANAGER = "user_manager"
        # BASIC_USER = "basic_user"
        # TEST_API = "test_api"
        # PROVISIONER = "provisioner"
        # OOBM_SERVICE = "oobm_service"

    class UserStatus(enum.Enum):
        ENABLED = "enabled"
        DISABLED = "disabled"

    def __str__(self):
        return "<{}(username='{}', password='{}')>".format(self.__class__.__name__, self.username, self.password)

    def __init__(self,
                 username: str,
                 password: str = None,
                 password_token: str = None,
                 is_activated: bool = None,
                 accounts_api_protocol: str = None,
                 accounts_api_hostname: str = None,
                 accounts_api_port: int = None,
                 timeout: float = 30):

        self.username = username
        self.password = password
        self.password_token = password_token
        self.accounts_api_protocol = accounts_api_protocol
        self.accounts_api_hostname = accounts_api_hostname
        self.accounts_api_port = accounts_api_port
        self.timeout = timeout
        self._is_activated = is_activated
        # delay the getting of the client to allow for a constructor of an actor time to create the account in NetCloud
        self._accounts_service_rest_client = None

    def get_accounts_user_record(self) -> typing.Union[requests.Response, None]:
        """Get a response object that contains all the accounts service info on this user/actor. """
        return self.authenticate().accounts_service_rest_client.users.get_user(user_id='self')

    def update_accounts_user_record(self,
                                    accounts_user_id: str = None,
                                    email: str = None,
                                    first_name: str = None,
                                    last_name: str = None,
                                    tenant_id: str = None,
                                    applications: list = None,
                                    role: 'AccServActor.Roles' = None,
                                    is_active: bool = None,
                                    mfa_enabled: bool = None,
                                    is_locked: bool = None,
                                    is_federated: bool = None) -> typing.Union[requests.Response, None]:
        """Update the non-None attributes about the current user/actor with the accounts service.

        Args:
            accounts_user_id: The accounts user id of the user to update, by default it is the self user. Provided so that
            an admin or other user can update a property of another user.
            email:
            first_name:
            last_name:
            tenant_id:
            applications:
            role:
            is_active:
            mfa_enabled:
            is_locked:
            is_federated:

        Returns:
            A response object that contains all the accounts service info on this user/actor.
        """
        accounts_user_id = accounts_user_id or self.id
        return self.authenticate().accounts_service_rest_client.users.update(
            user_id=accounts_user_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            tenant_id=tenant_id,
            applications=applications,
            role=None if not role else role.value,
            is_active=is_active,
            mfa_enabled=mfa_enabled,
            is_locked=is_locked,
            is_federated=is_federated)

    @property
    def id(self) -> str:
        """Gets the accounts service user id for this user/actor."""
        accounts_user_id = None
        r = self.get_accounts_user_record()
        if r:
            accounts_user_id = r.json()['data']['id']
        return accounts_user_id

    @property
    def login(self) -> str:
        """Get the login/email address associated with this user - read-only attribute."""
        login = None
        r = self.get_accounts_user_record()
        if r:
            login = r.json()['data']['attributes']['login']
        return login

    @property
    def email(self) -> str:
        """Get the email address associated with this user."""
        email = None
        r = self.get_accounts_user_record()
        if r:
            email = r.json()['data']['attributes']['email']
        return email

    @email.setter
    def email(self, email: str) -> None:
        """Update the email address for the user in the NetCloud account.

        Args:
            email: the email address to update to
        """
        self.update_accounts_user_record(email=email)

    @property
    def first_name(self) -> str:
        """Gets the first name of the user from the accounts services users/self endpoint."""
        first_name = None
        r = self.get_accounts_user_record()
        if r:
            first_name = r.json()['data']['attributes']['firstName']
        return first_name

    @first_name.setter
    def first_name(self, first_name: str):
        """Change the first name of the user to the given name using accounts services users endpoint."""
        self.update_accounts_user_record(first_name=first_name)

    @property
    def last_name(self) -> str:
        """Gets the last name of the user from the accounts services users/self endpoint."""
        return self.get_accounts_user_record().json()['data']['attributes']['lastName']

    @last_name.setter
    def last_name(self, last_name: str):
        """Change the last name of the user to the given name using accounts services users endpoint."""
        self.update_accounts_user_record(last_name=last_name)

    @property
    def tenant_id(self) -> str:
        """Gets the tenant id of the user from the accounts services users/self endpoint."""
        return self.get_accounts_user_record().json()['data']['attributes']['tenantId']

    @tenant_id.setter
    def tenant_id(self, tenant_id: str):
        """Change the tenant_id of the user to the given id using the accounts services users endpoint."""
        self.update_accounts_user_record(tenant_id=tenant_id)

    @property
    def applications(self) -> typing.List[str]:
        """Gets the applications list of the user from the accounts services users/self endpoint."""
        r = self.get_accounts_user_record()
        if r:
            self._applications = r.json()['data']['attributes']['applications']
        return self._applications

    @applications.setter
    def applications(self, applications: typing.List[str]):
        """Change the applications of the user in the accounts services users endpoint."""
        r = self.update_accounts_user_record(applications=applications)
        self._applications = r.json()['data']['attributes']['applications']

    @property
    def role(self) -> 'AccServActor.Roles':
        """Gets the role of the user from the accounts services users/self endpoint."""
        return AccServActor.Roles(self.get_accounts_user_record().json()['data']['attributes']['role'])

    @role.setter
    def role(self, role: 'AccServActor.Roles'):
        """Change the role of the user to the given role using the accounts services users endpoint. """
        self.update_accounts_user_record(role=role)

    @property
    def is_activated(self) -> bool:
        """Gets the whether the user has ever logged in from the accounts services users/self endpoint."""
        if self._is_activated is None:
            return self.last_login is not None
        else:
            return self._is_activated

    @property
    def is_active(self) -> bool:
        """Gets the whether the user is active from the accounts services users/self endpoint."""
        return self.get_accounts_user_record().json()['data']['attributes']['isActive']

    @is_active.setter
    def is_active(self, is_active: bool):
        """Change the whether the user is active using the accounts services users endpoint."""
        self.update_accounts_user_record(is_active=is_active)

    @property
    def user_status(self) -> 'AccServActor.UserStatus':
        """Returns whether the user is enabled or disabled."""
        return AccServActor.UserStatus.ENABLED if self.is_active else AccServActor.UserStatus.DISABLED

    @property
    def mfa_enabled(self) -> bool:
        """Gets the whether the user mfa is enabled from the accounts services users/self endpoint."""
        r = self.get_accounts_user_record()
        if r:
            self._mfa_enabled = r.json()['data']['attributes']['mfaEnabled']
        return self._mfa_enabled

    @mfa_enabled.setter
    def mfa_enabled(self, mfa_enabled: bool):
        """Change whether the user has mfa_enabled using the accounts services users endpoint."""
        self.update_accounts_user_record(mfa_enabled=mfa_enabled)

    @property
    def last_login(self) -> str:
        """Gets the whether the user is locked from the accounts services users/self endpoint."""
        return self.get_accounts_user_record().json()['data']['attributes']['lastLogin']

    @property
    def is_locked(self) -> bool:
        """Gets the whether the user is locked from the accounts services users/self endpoint."""
        return self.get_accounts_user_record().json()['data']['attributes']['isLocked']

    @is_locked.setter
    def is_locked(self, is_locked: bool):
        """Change whether the user is locked using the accounts services users endpoint."""
        self.update_accounts_user_record(is_locked=is_locked)

    @property
    def is_federated(self) -> bool:
        """Gets the whether the user is federated from the accounts services users/self endpoint."""
        return self.get_accounts_user_record().json()['data']['attributes']['isFederated']

    @is_federated.setter
    def is_federated(self, is_federated: bool):
        """Change whether the user is locked using the accounts services users endpoint."""
        self.update_accounts_user_record(is_federated=is_federated)

    @property
    def accounts_service_rest_client(self):
        """Returns the accounts service REST client instance for this actor instance.

        The self._accounts_service_rest_client is either built here based on the endpoint information in the passed on
        construction or has been built during activation or authentication from passed in endpoint information. The session
        gets created here in the AccServRESTClient and is subsequently populated with a JWT that represents this actor on
        the call to authenticate.
        """
        if not self._accounts_service_rest_client:
            if self.accounts_api_port and self.accounts_api_hostname and self.accounts_api_port:
                self._accounts_service_rest_client = AccServRESTClient(
                    protocol=self.accounts_api_protocol,
                    hostname=self.accounts_api_hostname,
                    port=self.accounts_api_port,
                    timeout=self.timeout)
            else:
                raise ValueError("{} needs to have AccServRESTClient endpoints. "
                                 "Call with accounts service api protocol, hostname and port.".format(self))
        return self._accounts_service_rest_client

    def activate(self,
                 password: str,
                 password_token: str = None,
                 accounts_api_protocol: str = None,
                 accounts_api_hostname: str = None,
                 accounts_api_port: int = None,
                 timeout: float = None) -> 'AccServActor':
        """ Activate the new NetCloud user using the netcloud account API.

        Args:
            password: optional password to authenticate with.  Provide the password if it was not passed in on NetCloudActor
            password_token: If not provided, it will use the token gotten from the return response when creating the user.
            It can be provided here if the caller has mined the token from the invitation email.
            accounts_api_protocol: protocol for the accounts service, if not previously provided on init it is required here.
            accounts_api_hostname: hostname for the accounts service, if not previously provided on init it is required here.
            accounts_api_port: port for the accounts service, if not previously provided on init it is required here.
            timeout: optional timeout for communicating with the accounts service, default is 30 secs.

        Returns:
            The activated actor for call chaining.
        """
        self.password_token = password_token or self.password_token
        self.password = password or self.password
        self.accounts_api_protocol = accounts_api_protocol or self.accounts_api_protocol
        self.accounts_api_hostname = accounts_api_hostname or self.accounts_api_hostname
        self.accounts_api_port = accounts_api_port or self.accounts_api_port
        self.timeout = self.timeout if timeout is None else timeout or 30.0

        if not self.password_token:
            raise RuntimeError("{} missing password token".format(self))
        logger.info("Activating new NetCloud user: {} with token: {}".format(self, self.password_token))
        self.accounts_service_rest_client.password_tokens.update(
            self.password_token, self.password, headers={"Content-Type": "application/vnd.api+json"})
        self._is_activated = True
        return self

    def is_authenticated(self) -> bool:
        """Returns whether this actor has been authenticated on the accounts service endpoint."""
        # If there is a rest client, call on it to see if authenticated, otherwise return false.
        if self._accounts_service_rest_client:
            return self._accounts_service_rest_client.authenticated()
        return False

    def authenticate(self,
                     password: str = None,
                     accounts_api_protocol: str = None,
                     accounts_api_hostname: str = None,
                     accounts_api_port: int = None,
                     timeout: float = None) -> 'AccServActor':
        """Authenticates the users credentials with the accounts service.

        Authenticates with the accounts service such that subsequent actions that talk to the accounts service can do so as
        this actor. If the actor was created through an account then no accounts service endpoint information needs to be
        passed in.

        Args:
            password: optional password to authenticate with.  Provide the password if it was not passed in on NetCloudActor
            instantiation or not passed in on a call to activate().
            accounts_api_protocol: protocol for the accounts service, if not previously provided it is required here.
            accounts_api_hostname: hostname for the accounts service, if not previously provided it is required here.
            accounts_api_port: port for the accounts service, if not previously provided it is required here.
            timeout: optional timeout for communicating with the accounts service, default is 30 secs.

        Returns:
            The authenticated actor for call chaining.
        """
        self.password = password or self.password
        self.accounts_api_protocol = accounts_api_protocol or self.accounts_api_protocol
        self.accounts_api_hostname = accounts_api_hostname or self.accounts_api_hostname
        self.accounts_api_port = accounts_api_port or self.accounts_api_port
        self.timeout = self.timeout if timeout is None else timeout or 30.0

        if not self.is_authenticated():
            if not self.username or not self.password:
                raise RuntimeError("{} need to authenticate with a username and password.".format(self))
            self.accounts_service_rest_client.authenticate(self.username, self.password)

        # Force determining if actived by going to the rest endpoint in is_activated property.
        self._is_activated = None
        return self

    def change_password(self, current_password: str, new_password: str) -> 'AccServActor':
        """Change the password of the actor, invalidate its client session to force re-authentication.

        Returns:
            The actor for call chaining.
        """
        self.authenticate(password=current_password)
        self.accounts_service_rest_client.change_password.update(current_password, new_password)
        # Set up the new password and force a new rest client to be built with new credentials
        self.password = new_password
        self._accounts_service_rest_client = None
        return self
