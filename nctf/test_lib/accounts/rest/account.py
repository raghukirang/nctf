import logging
import typing

import requests

from .actor import AccServActor
from .fixture import AccServRESTClient

logger = logging.getLogger("fixtures.netcloud_account")

NEW_ACCOUNT_INVITATION_EMAIL_SUBJECT = "Activate Your Cradlepoint NetCloud User Account"
OLD_ACCOUNT_INVITATION_EMAIL_SUBJECT = "Setup Your Cradlepoint NetCloud Account"


class AccSrvAccount(object):
    """Create a NetCloud Account that represents an account that is existing in NetCloud or is being created by the fixture.

    Args:
        accounts_service_rest_client: The accounts service rest client that is or will be authenticated by the admin. Totally
        up to the caller for making sure this client is authenticated.
    """

    def __str__(self):
        return "<{}(tenant id='{}', admin='{}')>".format(self.__class__.__name__, self.tenant_id, self.name)

    def __init__(self, accounts_service_rest_client: AccServRESTClient, tenant_id: str):
        self.tenant_id = tenant_id
        self.accounts_service_rest_client = accounts_service_rest_client

    def get_account_record(self) -> typing.Union[requests.Response, None]:
        """Get a response object that contains the info about the current account."""
        return self.accounts_service_rest_client.accounts.detail(resource_id=self.tenant_id)

    def update_account_record(self,
                              contact: str = None,
                              session_length: int = None,
                              enhanced_login_security_enabled: bool = None,
                              federated_id: str = None,
                              identity_provider_cert: str = None,
                              mfa_required: bool = None) -> typing.Union[requests.Response, None]:
        """Update the non-None attributes about the current account with the accounts service.

        Returns:
            A response object that contains all the accounts service info on this user/actor.

        Raises:
            Unauthenticated pre-defined NetCloudActors will throw an exception if they haven't been instantiated with
            an account or authenticated with the passed in the accounts service endpoints on authenticate().
        """
        return self.accounts_service_rest_client.accounts.update(
            account_id=self.tenant_id,
            contact=contact,
            session_length=session_length,
            enhanced_login_security_enabled=enhanced_login_security_enabled,
            federated_id=federated_id,
            identity_provider_cert=identity_provider_cert,
            mfa_required=mfa_required)

    @property
    def name(self) -> str:
        """Gets the name of the account from the accounts services accounts endpoint."""
        return self.get_account_record().json()['data']['attributes']['name']

    # @name.setter - cannot set the name

    @property
    def contact(self) -> str:
        """Gets the contact of the account from the accounts services accounts endpoint."""
        return self.get_account_record().json()['data']['attributes']['contact']

    @contact.setter
    def contact(self, contact: str):
        """Change the contact of the account to the given contact using the accounts services users endpoint. """
        self.update_account_record(contact=contact)

    @property
    def session_length(self) -> int:
        """Gets the session length of the account from the accounts services accounts endpoint."""
        return int(self.get_account_record().json()['data']['attributes']['sessionLength'])

    @session_length.setter
    def session_length(self, session_length: int):
        """Change the session length of the account to the given session_length using the accounts services users endpoint. """
        self.update_account_record(session_length=session_length)

    @property
    def enhanced_login_security_enabled(self) -> bool:
        """Gets the whether the account has enabled enhanced login security from the accounts services accounts endpoint."""
        return self.get_account_record().json()['data']['attributes']['enhancedLoginSecurityEnabled']

    @enhanced_login_security_enabled.setter
    def enhanced_login_security_enabled(self, enhanced_login_security_enabled: bool):
        """Set whether the account has enabled enhanced login security using the accounts services users endpoint. """
        self.update_account_record(enhanced_login_security_enabled=enhanced_login_security_enabled)

    @property
    def federated_id(self) -> str:
        """Gets the federated id of the account from the accounts services accounts endpoint."""
        return self.get_account_record().json()['data']['attributes']['federatedId']

    @federated_id.setter
    def federated_id(self, federated_id: str):
        """Change the federated id of the account to the given contact using the accounts services users endpoint. """
        self.update_account_record(federated_id=federated_id)

    @property
    def identity_provider_cert(self) -> str:
        """Gets the contact of the account from the accounts services accounts endpoint."""
        return self.get_account_record().json()['data']['attributes']['identityProviderCertificate']

    @identity_provider_cert.setter
    def identity_provider_cert(self, identity_provider_cert: str):
        """Change the identity provider certificate of the account to the given contact using the accounts services users
        endpoint. """
        self.update_account_record(identity_provider_cert=identity_provider_cert)

    @property
    def mfa_required(self) -> bool:
        """Gets the whether the account requires mfa from the accounts services accounts endpoint."""
        return self.get_account_record().json()['data']['attributes']['mfaRequired']

    @mfa_required.setter
    def mfa_required(self, mfa_required: bool):
        """Set whether the account requires mfa using the accounts services users endpoint. """
        self.update_account_record(mfa_required=mfa_required)

    default_applications = ["ecm"]

    def create_user(self,
                    username: str = None,
                    password: str = "Password1!",
                    first_name: str = None,
                    last_name: str = None,
                    role: AccServActor.Roles = AccServActor.Roles.USER,
                    applications: typing.List[str] = default_applications,
                    is_active: bool = True,
                    mfa_enabled: bool = False,
                    is_locked: bool = False,
                    is_federated: bool = False) -> AccServActor:
        """Create a NetCloudActor that is a user in this Account. Do NOT use this method to create the initial account admin.

        Args:
            username:
            password:
            first_name:
            last_name:
            role:
            applications:
            is_active:
            mfa_enabled:
            is_locked:
            is_federated:
        Returns:
            a NetCloudActor that has been at least CREATED but could be ACTIVATED or AUTHENTICATED.
        """
        if username:
            if not (first_name and last_name):
                raise ValueError("Cannot create a user with a username when there is no first name and last name."
                                 "If trying to create a Actor for an existing user, use account.find_user_by_login().")

        r = self.accounts_service_rest_client.users.create(
            email=username,
            role=role.value,
            tenant_id=self.tenant_id,
            first_name=first_name,
            last_name=last_name,
            applications=applications,
            is_active=is_active,
            is_locked=is_locked,
            mfa_enabled=mfa_enabled,
            is_federated=is_federated)

        password_token = r.headers.get("password_token", None)
        logger.info('Create username={}, password={}, password_token={}, role={}, is_active={}, is_locked={}, mfa_enabled={}, '
                    'is_federated={}'.format(username, password, password_token, role, is_active, is_locked, mfa_enabled,
                                             is_federated))

        return AccServActor(
            username=username,
            password=password,
            password_token=password_token,
            accounts_api_protocol=self.accounts_service_rest_client.protocol,
            accounts_api_hostname=self.accounts_service_rest_client.hostname,
            accounts_api_port=self.accounts_service_rest_client.port,
            timeout=self.accounts_service_rest_client.timeout)

    def delete_user(self, user: AccServActor) -> None:
        """Delete this user from the NetCloud account and neuters the NetCloudActor passed in."""
        self.delete_user_by_id(user_id=user.id)
        user._accounts_service_rest_client = None
        user._accounts_user_id = None

    def delete_user_by_id(self, user_id: str) -> None:
        """Delete the user specified by user_id from this NetCloud account."""
        self.accounts_service_rest_client.users.delete(user_id=user_id)

    def instantiate_existing_user(self, user_record: typing.Dict) -> AccServActor:
        """Instantiate a NetCloudActor given the dictionary defining a user that is got from the json 'users' type."""
        username = user_record["attributes"]["email"]
        is_activated = user_record["attributes"]["lastLogin"] is not None
        return AccServActor(
            username=username,
            is_activated=is_activated,
            accounts_api_protocol=self.accounts_service_rest_client.protocol,
            accounts_api_hostname=self.accounts_service_rest_client.hostname,
            accounts_api_port=self.accounts_service_rest_client.port)

    def get_users(self,
                  status: AccServActor.UserStatus = None,
                  email: str = None,
                  ordering: str = "firstName,lastName",
                  search: str = None,
                  batch_size: int = 25) -> typing.List[AccServActor]:
        """Get a list of accounts service actors in this account.

        Args:
            status: only return actors that match the given status
            email:  only return actors that match the given email
            ordering: order the list by this comma separated string. Default is "firstName,lastName"
            search: only return actors that match the given search string.
            batch_size: only return the given number of actors at one time.

        Returns:
            a list of batch_size AccServActors that match the given criteria in the specified order.
        """
        r = self.accounts_service_rest_client.users.get_users(
            status=None if not status else status.value, email=email, ordering=ordering, search=search, per_page=batch_size)

        users = []
        if r:
            for user_record in r.json()["data"]:
                users.append(self.instantiate_existing_user(user_record))
        return users

    def get_user_by_login(self, email: str) -> AccServActor:
        user_list = self.get_users(email=email)
        return None if len(user_list) == 0 else user_list[0]
