import logging

from nctf.test_lib.base.rest import RESTFixture
from nctf.test_lib.accounts.rest import AccServRESTClient

logger = logging.getLogger('test_lib.accserv.rest')


class AccServRESTFixture(RESTFixture):
    """Factory class to handle simplified construction of the Accounts Service REST Client"""

    def __init__(self, accounts_protocol: str, accounts_hostname: str, accounts_port: int):
        self.accounts_protocol = accounts_protocol
        self.accounts_hostname = accounts_hostname
        self.accounts_port = accounts_port

    def get_client(self) -> AccServRESTClient:
        client = AccServRESTClient(
            self.accounts_protocol, self.accounts_hostname, self.accounts_port,
        )
        return client

    def teardown(self):
        pass

    def get_failure_report(self):
        pass
