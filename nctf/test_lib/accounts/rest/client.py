import logging

from requests import Response
from requests import Session

from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTAPIException
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint

logger = logging.getLogger('test_lib.accounts.rest')

USERS_ENDPOINT = '/api/v1/users'
ACCOUNTS_ENDPOINT = '/api/v1/accounts'
FACTORS_ENDPOINT = '/api/v1/factors'
VERSIONS_ENDPOINT = '/api/v1/versions'
PASSWORD_RESET_ENDPOINT = '/api/v1/passwordreset'
CHANGE_PASSWORD_ENDPOINT = '/api/v1/changepassword'
RESEND_INVITATIONS_ENDPOINT = '/api/v1/resendInvitations'
FEATURE_BINDINGS_ENDPOINT = '/api/v1/featureBindings'
ROLES_ENDPOINT = '/api/v1/roles'
AUTHORIZATIONS_ENDPOINT = '/api/v1/authorizations'
UI_SETTINGS_ENDPOINT = '/api/v1/uiSettings'
FEDERATED_LOGIN_ENDPOINT = '/api/v1/federatedLogin'
NOTIFICATIONS_ENDPOINT = '/api/v1/notifications'
CREDENTIAL_EXCHANGE_ENDPOINT = '/api/v1/credentialExchange'
LEGACY_USER_PROFILES_ENDPOINT = '/api/v1/legacyUserProfiles'
PASSWORD_TOKENS_ENDPOINT = '/api/v1/passwordTokens'


class AuthenticationError(RESTAPIException):
    """Raised when session authorization attempts fail for any reason."""


class AccServRESTClient(RESTClient):
    """Accounts Service REST API client"""

    DEFAULT_HEADERS = {'Accept': 'application/vnd.api+json', 'Content-Type': 'application/vnd.api+json'}

    def __init__(self, protocol: str, hostname: str, port: int, timeout: float = 60.0, session: Session = None):
        super().__init__(protocol, hostname, port, timeout, session)

        self.users = UsersEndpoint(self, USERS_ENDPOINT)
        self.accounts = AccountsEndpoint(self, ACCOUNTS_ENDPOINT)
        self.factors = FactorsEndpoint(self, FACTORS_ENDPOINT)
        self.versions = VersionsEndpoint(self, VERSIONS_ENDPOINT)
        self.password_reset = PasswordResetEndpoint(self, PASSWORD_RESET_ENDPOINT)
        self.change_password = ChangePasswordEndpoint(self, CHANGE_PASSWORD_ENDPOINT)
        self.resend_invitations = ResendInvitationsEndpoint(self, RESEND_INVITATIONS_ENDPOINT)
        self.feature_bindings = FeatureBindingsEndpoint(self, FEATURE_BINDINGS_ENDPOINT)
        self.roles = RolesEndpoint(self, ROLES_ENDPOINT)
        self.authorizations = AuthorizationsEndpoint(self, AUTHORIZATIONS_ENDPOINT)
        self.ui_settings = UiSettingsEndpoint(self, UI_SETTINGS_ENDPOINT)
        self.federated_login = FederatedLoginEndpoint(self, FEDERATED_LOGIN_ENDPOINT)
        self.notifcations = NotificationsEndpoint(self, NOTIFICATIONS_ENDPOINT)
        self.credential_exchange = CredentialExchangeEndpoint(self, CREDENTIAL_EXCHANGE_ENDPOINT)
        self.legacy_user_profile = LegacyUserProfilesEndpoint(self, LEGACY_USER_PROFILES_ENDPOINT)
        self.password_tokens = PasswordTokensEndpoint(self, PASSWORD_TOKENS_ENDPOINT)

    def authenticate(self, username: str, password: str) -> Response:
        """Authenticates this client's session via the Accounts Service.

        Args:
            username: Username for the user to authenticate.
            password: Password for the user.

        Raises:
            AuthenticationError: If the username/password combo is not valid.
            UnexpectedStatusError: If a status other than those for successful/invalid login are received.
        """
        logger.info("Authenticating client session with credentials: {}/{}".format(username, password))

        try:
            r = self.post("/login", 302, data={"username": username, "password": password}, allow_redirects=False)
        except RESTAPIException as e:
            raise AuthenticationError from e

        logger.info("Successful authentication for user '{}'".format(username))
        return r

    def authenticated(self) -> bool:
        """Check our cookies to see if we're authenticated."""
        accounts_session_cookie = self.session.cookies.get('accounts_sessionid', default=None)

        # Either a 'cpAccountsJwt' cookie will be present or a 'jwt' depending on the stack.
        cp_accounts_jwt_cookie = self.session.cookies.get('cpAccountsJwt', default=None)
        jwt_cookie = self.session.cookies.get('jwt', default=None)

        return accounts_session_cookie and (jwt_cookie or cp_accounts_jwt_cookie)


class AccServEndpoint(RESTEndpoint):
    @checked_status(200)
    def detail(self, resource_id: str, *args, **kwargs) -> Response:
        return self._get(resource_id, *args, **kwargs)

    @checked_status(200)
    def list(self, *args, **kwargs) -> Response:
        return self._get(*args, **kwargs)

    @checked_status(204)
    def delete(self, resource_id: str, *args, **kwargs) -> Response:
        return self._delete(resource_id, *args, **kwargs)


class AccountsEndpoint(AccServEndpoint):
    @checked_status(201)
    def create(self, tenant_id: str, name: str, **kwargs) -> Response:
        payload = {
            "data": {
                "type": "accounts",
                "id": tenant_id,
                "attributes": {
                    "name": name,
                }
            }
        }
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._post(json=payload, **kwargs)

    @checked_status(200)
    def update(self,
               account_id: str,
               contact: str = None,
               session_length: int = None,
               enhanced_login_security_enabled: bool = None,
               federated_id: str = None,
               identity_provider_cert: str = None,
               mfa_required: bool = None,
               **kwargs) -> Response:
        """Updates the NetCloud account with the given non-None attributes."""

        attributes_to_update = {
            "contact": contact,
            "sessionLength": session_length,
            "enhancedLoginSecurityEnabled": enhanced_login_security_enabled,
            "federatedId": federated_id,
            "identityProviderCertificate": identity_provider_cert,
            "mfaRequired": mfa_required,
        }
        # Reduce the attributes_to_update dictionary, removing entries that have a None value
        new_attrs = {k: v for k, v in attributes_to_update.items() if v is not None}

        payload = self.detail(resource_id=account_id).json()
        current_attrs = payload["data"]["attributes"]

        # Cool syntax to create a dictionary using the first dictionary and updating entries from the second
        payload["data"]["attributes"] = {**current_attrs, **new_attrs}

        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._put(resource_id=account_id, json=payload, **kwargs)


class UsersEndpoint(AccServEndpoint):
    @checked_status(201)
    def create(self,
               email: str,
               role: str,
               tenant_id: str,
               first_name: str = None,
               last_name: str = None,
               applications: list = None,
               is_active: bool = True,
               is_locked: bool = False,
               mfa_enabled: bool = False,
               is_federated: bool = False,
               **kwargs) -> Response:

        first_name = first_name if first_name else 'Test'
        last_name = last_name if last_name else 'AutoUser'
        applications = applications if applications else ["ecm"]

        payload = {
            "data": {
                "type": "users",
                "attributes": {
                    "email": email,
                    "firstName": first_name,
                    "lastName": last_name,
                    "role": role,
                    "tenantId": tenant_id,
                    "applications": applications,
                    "isActive": is_active,
                    "isLocked": is_locked,
                    "mfaEnabled": mfa_enabled,
                    "isFederated": is_federated
                }
            }
        }
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._post(json=payload, **kwargs)

    @checked_status(200)
    def get_user(self, user_id: str, *args, **kwargs) -> Response:
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._get(resource_id=user_id, *args, **kwargs)

    @checked_status(200)
    def get_users(self,
                  tenant_id: str = None,
                  status: str = None,
                  email: str = None,
                  ordering: str = "firstName,lastName",
                  search: str = None,
                  per_page: int = 25,
                  **kwargs) -> Response:
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "tenantId": tenant_id,
            "status": status,
            "email": email,
            "ordering": ordering,
            "search": search,
            "perPage": per_page,
        }
        reduced_params = {k: v for k, v in params.items() if v is not None}
        return self._get(params=reduced_params, **kwargs)

    def deactivate(self, resource_id: str) -> Response:
        return self.update(user_id=resource_id, is_active=False)

    def reactivate(self, resource_id: str) -> Response:
        return self.update(user_id=resource_id, is_active=True)

    @checked_status(200)
    def update(self,
               user_id: str,
               email: str = None,
               first_name: str = None,
               last_name: str = None,
               role: str = None,
               tenant_id: str = None,
               applications: list = None,
               is_active: bool = None,
               is_locked: bool = None,
               mfa_enabled: bool = None,
               is_federated: bool = False,
               **kwargs) -> Response:
        """Updates the NetCloud user with the given non-None attributes."""

        attributes_to_update = {
            "email": email,
            "firstName": first_name,
            "lastName": last_name,
            "role": role,
            "tenantId": tenant_id,
            "applications": applications,
            "isActive": is_active,
            "isLocked": is_locked,
            "mfaEnabled": mfa_enabled,
            "isFederated": is_federated
        }
        # Reduce the attributes_to_update dictionary, removing entries that have a None value
        new_attrs = {k: v for k, v in attributes_to_update.items() if v is not None}

        payload = self.get_user(user_id=user_id).json()
        current_attrs = payload["data"]["attributes"]

        # Cool syntax to create a dictionary using the first dictionary and updating entries from the second
        payload["data"]["attributes"] = {**current_attrs, **new_attrs}

        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._put(resource_id=user_id, json=payload, **kwargs)

    @checked_status(204)
    def delete(self, user_id: str, *args, **kwargs) -> Response:
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._delete(resource_id=user_id, *args, **kwargs)


class FactorsEndpoint(AccServEndpoint):
    pass


class VersionsEndpoint(AccServEndpoint):
    pass


class PasswordResetEndpoint(AccServEndpoint):
    @checked_status(204)
    def update(self, email: str, **kwargs):
        payload = {"data": {"attributes": {"email": email}, "type": "passwordreset"}}
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._post(json=payload, **kwargs)


class ChangePasswordEndpoint(AccServEndpoint):
    @checked_status(204)
    def update(self, current_password: str, new_password: str, **kwargs):
        payload = {
            "data": {
                "attributes": {
                    "current_password": current_password,
                    "new_password": new_password,
                },
                "type": "changepassword"
            }
        }
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._post(json=payload, **kwargs)


class ResendInvitationsEndpoint(AccServEndpoint):
    pass


class FeatureBindingsEndpoint(AccServEndpoint):
    pass


class RolesEndpoint(AccServEndpoint):
    pass


class AuthorizationsEndpoint(AccServEndpoint):
    def update(self, resource_id: str, payload: dict, **kwargs):
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        return self._put(resource_id=resource_id, json=payload, **kwargs)


class FederatedLoginEndpoint(AccServEndpoint):
    pass


class UiSettingsEndpoint(AccServEndpoint):
    pass


class NotificationsEndpoint(AccServEndpoint):
    pass


class CredentialExchangeEndpoint(AccServEndpoint):
    pass


class LegacyUserProfilesEndpoint(AccServEndpoint):
    pass


class PasswordTokensEndpoint(AccServEndpoint):
    @checked_status(204)
    def update(self, token: str, password: str, **kwargs):
        payload = {
            "data": {
                "id": token,
                "attributes": {
                    "newPassword": password,
                    "expired": False,
                    "activation": True
                },
                "type": "passwordTokens"
            }
        }
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._put(resource_id=token, json=payload, **kwargs)
