from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By

from .accounts_base import AccountsBasePage


class UsersPage(AccountsBasePage):
    URL_TEMPLATE = "/static/dist/index.html#/users?tenantId={tenant_id}"

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def toolbar(self):
        return Toolbar(self)

    @property
    def account_users_table(self):
        return UsersTable(self)


class Toolbar(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CLASS_NAME, "cp-toolbar")

    @property
    def add_dropdown(self):
        return pages.UIRegionLinkElement(
            name="Account Users Add dropdown",
            parent=self,
            region_cls=Toolbar.AddDropdownRegion,
            strategy=By.ID,
            locator="addButton",
            region_parent=self)

    @property
    def edit_user_button(self):
        return pages.UIPageLinkElement(
            name="Account Users Edit button",
            parent=self,
            page_cls=EditUserPage,
            frame=self.frame,
            strategy=By.ID,
            locator="editButton",
            user_id='{user_id}')

    @property
    def edit_collaborator_button(self):
        return pages.UIPageLinkElement(
            name="Account Users Edit button",
            parent=self,
            page_cls=EditCollaboratorPage,
            frame=self.frame,
            strategy=By.ID,
            locator="editButton",
            collab_id='{collab_id}')

    @property
    def delete_button(self):
        return pages.UIRegionLinkElement(
            name="Account Users Delete button",
            parent=self,
            region_cls=ConfirmationRegion,
            strategy=By.ID,
            locator="deleteButton",
            region_parent=self)

    class AddDropdownRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.XPATH, ".//ul[starts-with(@class, 'dropdown-menu')]")

        @property
        def user_button(self):
            return pages.UIPageLinkElement(
                name='Add User Button',
                parent=self,
                page_cls=AddUserPage,
                frame=self.frame,
                strategy=By.ID,
                locator="cp-add-user",
                tenant_id='{tenant_id}')

        @property
        def collaborator_button(self):
            return pages.UIPageLinkElement(
                name='Add Collaborator Button',
                parent=self,
                page_cls=AddCollaboratorPage,
                frame=self.frame,
                strategy=By.ID,
                locator="cp-add-collaborator",
                tenant_id='{tenant_id}')


class UsersTable(pages.EmberTable):
    ROOT_LOCATOR = (By.CLASS_NAME, 'ember-light-table')
    DEFAULT_TIMEOUT = 30

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=By.CLASS_NAME, locator="lt-is-loading")


class EditCollaboratorPage(AccountsBasePage):
    URL_TEMPLATE = '/static/dist/index.html#/collaborator/{collab_id}'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.cancel_button.wait_for_clickable()
        return self

    @property
    def role_dropdown(self):
        return pages.UIRegionLinkElement(
            'User Role dropdown',
            self,
            region_cls=EditCollaboratorPage.RoleSelectionRegion,
            strategy=By.ID,
            locator='cp-role-selector',
            region_parent=self)

    @property
    def save_button(self):
        return pages.UIPageLinkElement(
            name='Save button',
            parent=self,
            page_cls=UsersPage,
            frame=self.frame,
            strategy=By.ID,
            locator="submitButton",
            tenant_id='{tenant_id}')

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            name="Cancel button",
            parent=self,
            page_cls=UsersPage,
            frame=self.frame,
            strategy=By.ID,
            locator="cancelButton",
            tenant_id='{tenant_id}')

    class RoleSelectionRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.XPATH, "//ul[@class='ember-power-select-options ember-view']")

        def role_selector(self, role: str):
            dropdown_options = ['User', 'User Administrator']
            assert role in dropdown_options, "{} is not a valid NCM Role".format(role)
            if role == 'User':
                return self.user_button.click()
            elif role == 'User Administrator':
                return self.user_administrator_button.click()

        @property
        def user_button(self):
            xpath = ".//li[text()='User']"
            return pages.UIPageLinkElement(
                name="User Role button",
                parent=self,
                page_cls=AddUserPage,
                frame=self.frame,
                strategy=By.XPATH,
                locator=xpath,
                tenant_id='{tenant_id}')

        @property
        def user_administrator_button(self):
            xpath = ".//li[text()='User Administrator']"
            return pages.UIPageLinkElement(
                name="User Administrator Role button",
                parent=self,
                page_cls=AddUserPage,
                frame=self.frame,
                strategy=By.XPATH,
                locator=xpath,
                tenant_id='{tenant_id}')

        @property
        def collaborator_button(self):
            xpath = ".//li[text()='User']"
            return pages.UIPageLinkElement(
                name="Collaborator Role button",
                parent=self,
                page_cls=AddCollaboratorPage,
                frame=self.frame,
                strategy=By.XPATH,
                locator=xpath,
                tenant_id='{tenant_id}')

        @property
        def collaborator_administrator_button(self):
            xpath = ".//li[text()='User Administrator']"
            return pages.UIPageLinkElement(
                name="Collaborator Administrator Role button",
                parent=self,
                page_cls=AddCollaboratorPage,
                frame=self.frame,
                strategy=By.XPATH,
                locator=xpath,
                tenant_id='{tenant_id}')


class AddCollaboratorPage(EditCollaboratorPage):
    URL_TEMPLATE = '/static/dist/index.html#/collaborator/new?tenantId={tenant_id}'

    @property
    def email_field(self):
        return pages.UIElement('Email field', self, By.ID, 'email')

    def add_collaborator(self, email: str, admin: bool):

        self.email_field.send_keys(email)
        if admin:
            self.role_dropdown.click().collaborator_administrator_button.click()
        else:
            self.role_dropdown.click().collaborator_button.click()
        return self.save_button.click()


class AddUserPage(AddCollaboratorPage):

    URL_TEMPLATE = '/static/dist/index.html#/new?tenantId={tenant_id}'

    @property
    def first_name_field(self):
        return pages.UIElement('First Name field', self, By.ID, 'firstName')

    @property
    def last_name_field(self):
        return pages.UIElement('Last Name field', self, By.ID, 'lastName')

    def add_user(self, first_name: str, last_name: str, email: str, admin: bool):

        self.first_name_field.send_keys(first_name)
        self.last_name_field.send_keys(last_name)
        self.email_field.send_keys(email)
        if admin:
            self.role_dropdown.click().user_administrator_button.click()
        else:
            self.role_dropdown.click().user_button.click()
        return self.save_button.click()


class EditUserPage(AddUserPage):
    URL_TEMPLATE = '/static/dist/index.html#/user/{user_id}'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def enable_disable_checkbox(self):
        return pages.UIButtonElement("Enable/Disable checkbox", self, strategy=By.ID, locator="enabledCheckbox")

    @property
    def unlock_user_button(self):
        return pages.UIPageLinkElement(
            name="Unlock User Button",
            parent=self,
            page_cls=EditUserPage,
            frame=self.frame,
            strategy=By.ID,
            locator="cp-unlock-button",
            user_id='{user_id}')

    @property
    def clear_mfa_button(self):
        return pages.UIRegionLinkElement(
            name="Clear MFA Button",
            parent=self,
            region_cls=ConfirmationRegion,
            strategy=By.ID,
            locator="cp-disable-mfa-button",
            region_parent=self)


class ConfirmationRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//*[@id='modal-overlays']/div/div/div[contains(@class, 'cp-message-box')]")

    def wait_for_region_to_load(self):
        self.yes_button.wait_for_clickable()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def yes_button(self):
        return pages.UIButtonElement(name="Yes Button",
                                     parent=self,
                                     strategy=By.CSS_SELECTOR,
                                     locator="button.btn.cp-btn-yes")

    @property
    def no_button(self):
        return pages.UIButtonElement(name="No Button",
                                     parent=self,
                                     strategy=By.CSS_SELECTOR,
                                     locator="button.btn.cp-btn-no")
