from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By
from .accounts_base import AccountsBasePage


class AccountSettingsPage(AccountsBasePage):
    URL_TEMPLATE = '/static/dist/index.html#/account/{tenant_id}?parentAccount={parentAccount}&tenantId={tenant_id}'

    @property
    def session_length_dropdown(self):
        return pages.UIRegionLinkElement(name='Session Length dropdown',
                                         parent=self,
                                         region_cls=self.SessionLengthSelectionRegion,
                                         strategy=By.CSS_SELECTOR,
                                         locator="div.cp-session-length-selector div.ember-power-select-trigger")

    @property
    def system_admin_email_dropdown(self):
        return pages.UIRegionLinkElement(name='System Administrator Email dropdown',
                                         parent=self,
                                         region_cls=self.SystemAdminEmailSelectionRegion,
                                         strategy=By.CSS_SELECTOR,
                                         locator="div.cp-sys-admin-selector div.ember-power-select-trigger")

    @property
    def enhanced_login_security_checkbox(self):
        return pages.UIButtonElement("Enhanced Login Security checkbox", self,
                                     strategy=By.ID, locator="enhancedLoginSecurityCheckbox")

    @property
    def force_mfa_checkbox(self):
        return pages.UIButtonElement("Force MFA checkbox", self,
                                     strategy=By.ID, locator="force-mfa-checkbox")

    @property
    def save_button(self):
        return pages.UIRegionLinkElement(name='Save button',
                                         parent=self,
                                         region_cls=self.SaveConfirmationRegion,
                                         strategy=By.ID,
                                         locator="submitButton")

    @property
    def cancel_button(self):
        return pages.UIRegionLinkElement(name="Cancel button",
                                         parent=self,
                                         region_cls=self.page,
                                         strategy=By.ID,
                                         locator="cancelButton")

    class SessionLengthSelectionRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".ember-basic-dropdown-content.ember-power-select-dropdown")

        def select_session_length(self, session_length: str):
            """
            Args:
                session_length: Specifies the name of the session length option
                    options:
                        '5 minutes',
                        '10 minutes',
                        '15 minutes (default)',
                        '30 minutes',
                        '1 hour',
                        '2 hours'

            Returns:
                The specified session length element
            """
            dropdown_options = ['5 minutes',
                                '10 minutes',
                                '15 minutes (default)',
                                '30 minutes',
                                '1 hour',
                                '2 hours']
            assert session_length in dropdown_options, "{} is not a valid session length".format(session_length)
            xpath = "//ul[@class='ember-power-select-options ember-view']/li[text()='{}']".format(session_length)
            return pages.UIButtonElement(name="Session Length of {}".format(session_length),
                                         parent=self,
                                         strategy=By.XPATH,
                                         locator=xpath)

    class SystemAdminEmailSelectionRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".ember-power-select-search")

        @property
        def system_admin_search_dropdown(self):
            return pages.UIButtonElement(name="System Admin Email dropdown",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="li.ember-power-select-option:nth-of-type(1)")

        @property
        def system_admin_field(self):
            return pages.UIElement(name="System Admin text field",
                                   parent=self,
                                   strategy=By.CSS_SELECTOR,
                                   locator=".ember-power-select-search-input")

    class SaveConfirmationRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".cp-message-box.wide")

        @property
        def close_button(self):
            return pages.UIButtonElement(name="Close button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator=".btn.cp-btn-ok")
