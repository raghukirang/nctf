from nctf.test_lib.accounts.pages import accounts_base
from nctf.test_lib.accounts.pages.login.login import LoginHashedPage
from nctf.test_lib.base import pages


class ChangePasswordPage(accounts_base.AccountsBasePage):
    URL_TEMPLATE = '#/passwordTokens/{password_token}'

    @property
    def new_password_field(self):
        return pages.UIElement('New Password field', self, pages.By.ID, "cp-password")

    @property
    def confirm_password_field(self):
        return pages.UIElement('Confirm New Password field', self, pages.By.ID, "cp-confirm")

    @property
    def confirm_password_button(self):
        return pages.UIPageLinkElement(
            name='Send Email button',
            parent=self,
            page_cls=ChangePasswordPage,
            strategy=pages.By.ID,
            locator="cp-changepassword-submit-button",
            password_token={'password_token'})

    @property
    def back_to_login_button(self):
        return pages.UIPageLinkElement(
            name='Back to Login button',
            parent=self,
            page_cls=LoginHashedPage,
            strategy=pages.By.ID,
            locator="cp-back-to-login")
