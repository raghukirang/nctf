import pyotp

from nctf.test_lib.accounts.pages.accounts_base import AccountsBasePage
from nctf.test_lib.accounts.pages.login import LoginRedirectErrorPage
from nctf.test_lib.base import pages
from nctf.test_lib.base.actor import NCTFActorWithCreds


class SetupMfaPage(AccountsBasePage):
    URL_TEMPLATE = '#/setupMfa?redirect_url={redirect_url}'

    @property
    def setup_mfa_form(self) -> pages.UIRegion:
        return SetupMfaRegion(self)

    class ConfirmationDialog(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CLASS_NAME, "cp-message-box")

        @property
        def confirmation_button(self) -> pages.UIButtonElement:
            return pages.UIButtonElement(name='Confirmation button',
                                         parent=self,
                                         strategy=pages.By.CSS_SELECTOR,
                                         locator="button.btn.cp-btn-ok")


class SetupMfaRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CLASS_NAME, 'cp-netcloud-form')

    @property
    def mfa_manual_config_option(self) -> str:
        """'MFA configuration option which contains the configuration key."""
        element = pages.UIElement('MFA Configuration Key',
                                  self,
                                  pages.By.XPATH,
                                  "//div[contains(@class, 'cp-qrcode-flex-container')]//p[contains(@class,'cp-step-text')]")
        return element.get_text()[-32:]

    @property
    def authentication_code_field(self) -> pages.UIElement:
        return pages.UIElement('Authentication Code field', self, pages.By.CLASS_NAME, 'cp-auth-code-input')

    @property
    def finish_button(self) -> pages.UIRegionLinkElement:
        return pages.UIRegionLinkElement(name='Finish button',
                                         parent=self.parent,
                                         region_cls=SetupMfaPage.ConfirmationDialog,
                                         strategy=pages.By.CLASS_NAME,
                                         locator="cp-finish-button")

    def enter_auth_code(self, auth_code: str):
        self.authentication_code_field.send_keys(auth_code)
        return self.finish_button.click()

    def setup_mfa(self, actor: NCTFActorWithCreds) -> LoginRedirectErrorPage:
        mfa_secret = self.mfa_manual_config_option
        totp = pyotp.TOTP(mfa_secret)
        actor.mfa_token = totp.now()
        confirmation_box = self.enter_auth_code(actor.mfa_token)
        confirmation_box.confirmation_button.click()
        page = LoginRedirectErrorPage(self.driver, timeout=self.timeout, error='error', redirect_url='redirect_url')
        return page.wait_for_page_to_load()

    def setup_invalid_mfa(self) -> SetupMfaPage:
        confirmation_box = self.enter_auth_code("WRONG1")
        confirmation_box.confirmation_button.click()
        page = SetupMfaPage(self.driver, timeout=self.timeout, redirect_url='{redirect_url}')
        return page.wait_for_page_to_load()
