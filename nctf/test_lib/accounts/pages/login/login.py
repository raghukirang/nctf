import logging
import typing

# from nctf.test_lib.accounts.pages import login
from nctf.test_lib.accounts import pages as accts_pages
from nctf.test_lib.accounts.pages import accounts_base
from nctf.test_lib.base import pages
from nctf.test_lib.ncm import pages as ncm_pages
from nctf.test_lib.netcloud.fixtures.netcloud_account.actor import NetCloudActor

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class LoginFormRegion(pages.UIRegion):

    ROOT_LOCATOR = (pages.By.CLASS_NAME, 'cp-netcloud-form')

    @property
    def username_field(self) -> pages.UIElement:
        return pages.UIElement('username_field', self, pages.By.ID, 'username-input')

    @property
    def password_field(self) -> pages.UIElement:
        return pages.UIElement('password_field', self, pages.By.ID, 'password-input')

    @property
    def login_button(self) -> pages.UIButtonElement:
        return pages.UIButtonElement('login_button', self, pages.By.ID, "cp-login-submit-button")

    @property
    def forgot_password_link(self) -> pages.UIPageLinkElement:
        return pages.UIPageLinkElement(
            name='Forgot Password Link',
            parent=self,
            page_cls=accts_pages.PasswordResetPage,
            strategy=pages.By.CSS_SELECTOR,
            locator="#cp-login-forgot-password-link")

    def login(self, username: str, password: str) -> None:
        """
        Do the entry of text for username and password and click the login button

        Args:
            username: a string representing the name of the user to login
            password: a string representing the password fo the user to login

        """
        self.username_field.send_keys(username)
        self.password_field.send_keys(password)
        self.login_button.click()


class LoginPage(accounts_base.AccountsBasePage):

    # WARNING this may not be modeled correctly
    @property
    def netcloud_logo(self):
        return pages.UIPageLinkElement(
            name='netcloud_logo_link',
            parent=self,
            page_cls=LoginPage,
            strategy=pages.By.XPATH,
            locator="//a[contains(@class, 'cp-netcloud-logo')]")

    @property
    def login_form(self):
        return LoginFormRegion(self)
        # alternatively we could have done the following construction instead of using ROOT_LOCATOR in LoginFormRegion
        # return LoginFormRegion('LoginFormRegion', self, strategy=By.CLASS_NAME, locator='cp-netcloud-form' )

    def login(self, ncm_user_with_valid_creds: NetCloudActor) -> ncm_pages.RoutersPage:
        """
        Login a valid Netcloud user that has NCM permissions.
        
        This method assumes that the user has been successfully created by the test or has been pre-provisioned, and 
        has been given permission to NCM.  If the caller only has the username and password to an existing NCM user,
        construct the NetCloudActor on the fly with - login_page.login(NetCloudActor(username, password))
        
        Args:
            ncm_user_with_valid_creds: a valid NCM user.  

        Returns: 
            The Devices Routers page.

        """
        self.login_form.login(ncm_user_with_valid_creds.username, ncm_user_with_valid_creds.password)
        page = ncm_pages.RoutersPage(self.driver, timeout=self.timeout)
        logger.info("{} navigating to {}.".format(self, page))
        return typing.cast(ncm_pages.RoutersPage, page.wait_for_page_to_load())

    def login_with_only_ncp_permissions(self, ncp_user_with_valid_creds: NetCloudActor) -> ncm_pages.HomePage:
        """
        Login a valid Netcloud user that has NCP permissions, but does NOT have NCM permissions.

        This method assumes that the user has been already successfully created by the test but has not been given
        permission to NCM.

        Args:
            ncp_user_with_valid_creds: a valid NCP user that does NOT have permissions to use NCM.

        Returns:
            The Dashboard Home page.

        """
        self.login_form.login(ncp_user_with_valid_creds.username, ncp_user_with_valid_creds.password)
        page = ncm_pages.HomePage(self.driver, timeout=self.timeout)
        logger.info("{} navigating to {}.".format(self, page))
        return page.wait_for_page_to_load()

    def login_without_ncm_permissions(self, ncm_user_with_valid_creds: NetCloudActor) -> ncm_pages.NoPermissionsHomePage:
        """
        Login a valid Netcloud user that does NOT have NCM permissions.

        This method assumes that the user has been already successfully created by the test but has not been given 
        permission to NCM.

        Args:
            ncm_user_with_valid_creds: a valid NCM user that does NOT have permissions to use NCM.  

        Returns: 
            The No Permissions Dashboard Home page.

        """
        self.login_form.login(ncm_user_with_valid_creds.username, ncm_user_with_valid_creds.password)
        page = ncm_pages.NoPermissionsHomePage(self.driver, timeout=self.timeout)
        logger.info("{} navigating to {}.".format(self, page))
        return page.wait_for_page_to_load()

    def login_invalid_creds(self, username: str, password: str) -> 'LoginInvalidPage':
        """
        Try to login an invalid Netcloud user whose username doesn't exist or whose password is incorrect.

        This method assumes that the user does not exist in NCM or exists but not with the same password. Since the 
        caller need not have had to create a valid NCM user, this method only takes the username and password.

        Args:
            username: a string representing the name of the user to login
            password: a string representing the password fo the user to login

        Returns: 
            The Devices Routers page.

        """
        self.login_form.login(username, password)
        page = LoginInvalidPage(self.driver, base_url=self.base_url, timeout=self.timeout)
        logger.info("{} navigating to {}.".format(self, page))
        return page.wait_for_page_to_load()

    def login_locked_user(self, ncm_user_with_valid_creds: NetCloudActor) -> 'LoginLockedPage':
        """
        Try to login a valid NetCloud user whose is locked.

        This method assumes that the user has been already successfully created by the test but is currently locked
        in NCM.

        Args:
            ncm_user_with_valid_creds: a valid NCM user that is locked in NCM.

        Returns:
            The LoginLocked page.

        """
        self.login_form.login(ncm_user_with_valid_creds.username, ncm_user_with_valid_creds.password)
        page = LoginLockedPage(self.driver, base_url=self.base_url, timeout=self.timeout, redirect_url='redirect_url')
        logger.info("{} navigating to {}.".format(self, page))
        return page.wait_for_page_to_load()

    def login_with_mfa(self, ncm_user_with_mfa_enabled: NetCloudActor) -> 'accts_pages.MfaLoginPage':
        """
        Login a valid Netcloud user that has NCM permissions and has MFA enabled.

        This method assumes that the user has been successfully created by the test or has been pre-provisioned with
        MFA enabled, and has been given permission to NCM.

        Args:
            ncm_user_with_mfa_enabled: a valid NCM user.

        Returns:
            The MfaLogin page.

        """
        self.login_form.login(ncm_user_with_mfa_enabled.username, ncm_user_with_mfa_enabled.password)
        page = accts_pages.MfaLoginPage(self.driver, timeout=self.timeout, redirect_url='{redirect_url}')
        logger.info("{} navigating to {}.".format(self, page))
        return page.wait_for_page_to_load()

    def login_with_mfa_required_without_mfa(self, ncm_user_with_mfa_required: NetCloudActor) -> 'accts_pages.SetupMfaPage':
        """
        Login a valid Netcloud user that has NCM permissions and has Force MFA enabled, but not enrolled in MFA.

        This method assumes that the user has Force MFA enabled by the test or has been pre-provisioned with
        Force MFA enabled, and has been given permission to NCM.

        Args:
            ncm_user_with_mfa_required: a valid NCM user.

        Returns:
            The SetupMfa page.

        """
        self.login_form.login(ncm_user_with_mfa_required.username, ncm_user_with_mfa_required.password)
        page = accts_pages.SetupMfaPage(self.driver, timeout=self.timeout, redirect_url='{redirect_url}')
        logger.info("{} navigating to {}.".format(self, page))
        return page.wait_for_page_to_load()


class LoginRedirectPage(LoginPage):
    URL_TEMPLATE = '?redirect_url='


class LoginInvalidPage(LoginPage):
    URL_TEMPLATE = 'login?redirect_url='


class LoginLockedPage(LoginPage):
    URL_TEMPLATE = 'login?redirect_url={redirect_url}'


class LoginRedirectErrorPage(LoginPage):
    URL_TEMPLATE = '?error=&redirect_url='

    @property
    def login_form(self):
        return self.RedirectLoginFormRegion(self)

    class RedirectLoginFormRegion(LoginFormRegion):
        ROOT_LOCATOR = (pages.By.CLASS_NAME, 'cp-netcloud-form')

        @property
        def forgot_password_link(self):
            return pages.UIPageLinkElement(
                name='Forgot Password Link',
                parent=self,
                page_cls=accts_pages.PasswordResetRedirectPage,
                strategy=pages.By.CSS_SELECTOR,
                locator="#cp-login-forgot-password-link")


class LoginErrorPage(LoginPage):
    URL_TEMPLATE = '?error={error}&redirect_url={redirect_url}'


class LoginHashedPage(LoginPage):
    URL_TEMPLATE = '#/'
