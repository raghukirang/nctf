from nctf.test_lib.accounts.pages import accounts_base
from nctf.test_lib.base import pages


class PasswordResetPage(accounts_base.AccountsBasePage):
    URL_TEMPLATE = '#/passwordreset'

    @property
    def email_field(self):
        return pages.UIElement('Email Address field', self, pages.By.ID, "email")

    @property
    def send_email_button(self):
        return pages.UIPageLinkElement(
            name='Send Email button',
            parent=self,
            page_cls=PasswordResetPage,
            strategy=pages.By.ID,
            locator="cp-passwordreset-submit-button")


class PasswordResetRedirectPage(PasswordResetPage):
    URL_TEMPLATE = '?error=&redirect_url=#/passwordreset'

    @property
    def send_email_button(self):
        return pages.UIPageLinkElement(
            name='Send Email button',
            parent=self,
            page_cls=PasswordResetRedirectPage,
            strategy=pages.By.ID,
            locator="cp-passwordreset-submit-button")
