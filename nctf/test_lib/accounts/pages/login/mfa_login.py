from nctf.test_lib.accounts.pages import accounts_base
from nctf.test_lib.accounts.pages.login.login import LoginErrorPage
from nctf.test_lib.base import pages
from nctf.test_lib.ncm import pages as ncm_pages


class MfaLoginPage(accounts_base.AccountsBasePage):
    URL_TEMPLATE = '#/validateMfa?redirect_url={redirect_url}'

    @property
    def mfa_token_field(self) -> pages.UIElement:
        return pages.UIElement('MFA Token field', self, pages.By.ID, 'cp-login-mfa-token')

    @property
    def log_in_button(self) -> pages.UIElement:
        return pages.UIElement('Log In button', self, pages.By.ID, 'cp-mfa-login-submit-button')

    def enter_token(self, mfa_token: str) -> ncm_pages.RoutersPage:
        self.mfa_token_field.send_keys(mfa_token)
        self.log_in_button.click()
        page = ncm_pages.RoutersPage(self.driver, timeout=self.timeout)
        return page.wait_for_page_to_load()

    def enter_token_without_ncm_permissions(self, mfa_token: str) -> ncm_pages.NoPermissionsHomePage:
        self.mfa_token_field.send_keys(mfa_token)
        self.log_in_button.click()
        page = ncm_pages.NoPermissionsHomePage(self.driver, timeout=self.timeout)
        return page.wait_for_page_to_load()

    def enter_token_invalid_path(self, invalid_mfa_token: str) -> LoginErrorPage:
        """
            Entering invalid username, password, or mfa token will redirect back to LoginPage with an error.
            If the user is enrolled in MFA, the redirect will not occur until this step
            even if username or password are incorrect.
        """
        self.mfa_token_field.send_keys(invalid_mfa_token)
        self.log_in_button.click()
        page = LoginErrorPage(self.driver, timeout=self.timeout, error='error', redirect_url='redirect_url')
        return page.wait_for_page_to_load()
