import logging
import os
from typing import Tuple
from typing import Union

import requests
import requests.adapters
import requests.auth
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError
from requests.exceptions import InvalidURL
from requests.exceptions import Timeout

from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError


class BSFObject(object):
    """Generic Interface Object for BSF Services (Common Operations)

    This class is a generic BSF interface object, that provides common functionality
        between all of the services (i.e: Health, Version endpoints, HTTP connection, etc)

    See Also:
        (For *session* attribute): http://docs.python-requests.org/en/master/

    Attribute:
        session (requests.session): HTTP Connection to BSF service.
        base_url (str): Base URL of BSF Service (http://entitlement-service.devecm.com)
        username (str): Authentication Username
        password (str): Authentication Password
    """
    session = None
    base_url = None
    username = None
    password = None

    logger = logging.getLogger('bsf.object')

    __health_endpoint = '/manage/health'
    __version_endpoint = '/api/v1/version/'
    __swagger_endpoint = '/swagger-ui.html'

    def __init__(self, url, username, password):
        """Instantiates a connection to a BSF Service with a given URL, Username, and Password)"""
        self.base_url = url
        self.username = username
        self.password = password
        self.session = requests.session()
        self.session.auth = requests.auth.HTTPBasicAuth(self.username, self.password)
        self.session.mount(self.base_url, requests.adapters.HTTPAdapter(max_retries=5))

    def post(self, url: str, json):
        """Generic POST method"""
        return self._request('POST', url, json=json)

    def put(self, url: str, json):
        """Generic PUT method"""
        return self._request('PUT', url, json=json)

    def delete(self, url: str):
        """Generic DELETE method"""
        return self._request('DELETE', url)

    def get(self, url: str):
        """Generic GET method"""
        return self._request('GET', url)

    def health(self):
        """Returns HTTP Status Code of Health of BSF Service"""
        return self._request('get', self.__health_endpoint).status_code

    def version(self):
        """Returns version of BSF Service (in json)"""
        return self._request('get', self.__version_endpoint).json()

    def swagger_health(self):
        """Returns HTTP Status Code of Health of BSF Swagger Page"""
        return self._request('get', self.__swagger_endpoint).status_code

    def build_url(self, endpoint):
        """Returns a complete URL with the service's base_url and a given endpoint"""
        return '{}{}'.format(self.base_url, endpoint)

    def _request(self, operation: str, endpoint: str, expected_status_code: Union[int, Tuple[int, ...]] = None, **kwargs):
        """'Generalised' requests function.

        Generic '*Requests*' function that handles interactions between the BSF Service.
            Provides exception handling and handles HTTP input validation.

        Parameters:
            operation (str): HTTP Operation ('PUT', 'POST', 'GET', 'DELETE')
            endpoint (str): API Endpoint of BSF Service
            expected_status_code: The expected HTTP status code. May be a single int or a tuple/list of ints.
            data (dict): JSON dict of Data to PUT or POST.

        Return:
            *Requests* response from request.
        """
        operation = operation.lower()
        url = self.build_url(endpoint)

        opid = os.environ.get('NCTF_CURRENT_OPID')

        opid_header = {"X-CAUSALITY-ID": opid}
        self.session.headers.update(opid_header)

        if not hasattr(self.session, operation):
            raise ValueError('Requests session doesn\'t have the attribute \'{}\''.format(operation))

        try:
            response = getattr(self.session, operation)(url, **kwargs)

        except InvalidURL as e:
            self.logger.error('Invalid URL: \'{}\'\n\n{}'.format(url, e))
            raise e
        except Timeout as e:
            self.logger.error('Connection Timed Out!:\n\n{}'.format(e))
            raise e
        except HTTPError as e:
            self.logger.error('HTTP Error:\n\n{}'.format(e))
            raise e
        except ConnectionError as e:
            self.logger.error('Connection Error:\n\n{}'.format(e))
            raise e
        except requests.exceptions.RequestException as e:
            self.logger.error('Request Exception:\n\n{}'.format(e))
            raise e

        exp_status_codes = (expected_status_code,) if isinstance(expected_status_code, int) else expected_status_code
        if expected_status_code is not None and response.status_code not in exp_status_codes:
            msg = "Unexpected response from '{} {}'. ".format(operation, endpoint)
            msg += "Status code: {} ({}).\n".format(response.status_code, response.reason)
            msg += "---Request params (next line):\n\t{}\n".format(kwargs)
            msg += "---Response content (next line):\n\t{}\n".format(response.content)
            raise UnexpectedStatusError(msg, response)

        return response
