import logging

from nctf.test_lib.bsf.fixtures.entitlement_service import EntitlementServiceSession
from nctf.test_lib.bsf.fixtures.license_service import LicenseServiceSession
from nctf.test_lib.bsf.fixtures.order_service import OrderServiceSession
from nctf.test_lib.bsf.fixtures.product_subscription_service import ProductSubscriptionServiceSession
from nctf.test_lib.bsf.fixtures.provisioning_service import ProvisioningServiceSession
from nctf.test_lib.bsf.fixtures.sfconnector import SFConnectorSession
from nctf.test_lib.bsf.fixtures.view_layer_solution import ViewLayerSolutionSession


class BSFFixturesSession:

    def __init__(self, config_fixture):
        super(BSFFixturesSession, self)
        self.bsf_entitlement_fixture = EntitlementServiceSession(config_fixture)
        self.bsf_license_fixture = LicenseServiceSession(config_fixture)
        self.bsf_order_fixture = OrderServiceSession(config_fixture)
        self.bsf_product_subscription_fixture = ProductSubscriptionServiceSession(config_fixture)
        self.bsf_provisioning_fixture = ProvisioningServiceSession(config_fixture)
        self.bsf_sfconnector_fixture = SFConnectorSession(config_fixture)
        self.bsf_view_layer_fixture = ViewLayerSolutionSession(config_fixture)
