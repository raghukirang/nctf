import datetime
import json
import logging
import time

from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject
import requests.exceptions


class LicenseServiceSession(BSFObject):
    """Instantiates and provides an connection to a BSF License Service API.

    Note:
        Access should be limited to the 'bsf_license_fixture' fixture only.

        Requires the configuration values:
            - BSF.URLS.LICENSE
            - BSF.USER
            - BSF.PASS
    """
    logger = logging.getLogger('bsf.lic.interface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        license_url = f"{config_fixture.target.services.bsf.licenseService.protocol}://{config_fixture.target.services.bsf.licenseService.hostname}"
        super(LicenseServiceSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username, password=config_fixture.target.predefinedAuth.bsf.password, url=license_url)
        self.license_endpoint = "/api/v1/licenses"
        self.license_summary_endpoint = "/api/v1/licenseSummary"
        self.subscription_endpoint = "/api/v1/subscriptions"
        self.subscription_summary_endpoint = "/api/v1/subscriptionSummary"

    def get_license(self, commerce_id=None):
        """Retrieve a License (via a Commerce ID or All)

        Params:
            commerce_id (str): Commerce ID (defaults to None [Grab all Licenses])

        Returns:
            *Requests* response from request.
        """
        if commerce_id is None:
            endpoint = '/api/v1/licenses'

        else:
            endpoint = '/api/v1/licenses?commerceId={}'.format(commerce_id)

        self.logger.info('[GET] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_licenses_by_subscription_id(self, subscription_id=None):
        """Retrieve all Licenses (via a Subscription Commerce ID)

        Params:
            subscription_id (str): Subscription Commerce ID

        Returns:
            *Requests* response from request.
        """

        endpoint = '/api/v1/licenses?subscriptionId={}'.format(subscription_id)

        self.logger.info('[GET] LICENSES By SubscriptionId: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_license_summary(self, account_id=None, feature_key=None):
        """Retrieve the License Summary Endpoint"""
        if account_id is None or feature_key is None:
            raise ValueError('accountId and featureKey are required')

        endpoint = '/api/v1/licenseSummary?accountId={}&featureKey={}'.format(account_id, feature_key)

        self.logger.info('[GET] LICENSE SUMMARY: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_subscription(self, commerce_id=None):
        """Retrieve a Subscription (via a Commerce ID or All)

        Params:
            commerce_id (str): Commerce ID (defaults to None [Grab all Subscriptions])

        Returns:
            *Requests* response from request.
        """
        if commerce_id is None:
            endpoint = '/api/v1/subscriptions'

        else:
            endpoint = '/api/v1/subscriptions?commerceId={}'.format(commerce_id)

        self.logger.info('[GET] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_subscription_summary(self, account_id=None, subscription_id=None):
        """Retrieve the Subscription Summary Endpoint"""
        if account_id is None or subscription_id is None:
            raise ValueError('accountId and subscriptionId are required')

        endpoint = '/api/v1/subscriptionSummary?accountId={}&subscriptionId={}'.format(account_id, subscription_id)

        self.logger.info('[GET] SUBSCRIPTION SUMMARY: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_subscription_by_license(self, license_id):
        """Retrieve a Subscription (via a license ID)

            Params:
                license_id (str): License ID (defaults to None [Grab all Subscriptions])
                source (str): Source of Subscription (defaults to BSF)

            Returns:
                *Requests* response from request.
            """
        if license_id is None:
            raise ValueError('licenseId is required')

        else:
            endpoint = '/api/v1/subscriptions?licenseId={}'.format(license_id)

        self.logger.info('[GET] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def _retry_connection(self, http_op, endpoint, timeout=5.0):
        """Internal Retry Function

        Note:
            Currently Unused
            - Everything in this function works, but isn't needed at the moment.
            - Re-write this function to be easier to use/read!
            - Max Timeout value recommended (easier to understand)
        """
        response = None

        if timeout is None:
            return self._request(http_op, endpoint)

        if timeout < 0.5:
            raise ValueError('Timeout cannot be less than a half a second (0.5s)!')

        retry_timeout = 0.5
        retries = int(timeout / retry_timeout)

        for i in range(retries):
            response = self._request(http_op, endpoint)

            if response.status_code == 200:
                return response

            time.sleep(retry_timeout)

        error_message = "Couldn't Connect after {} retries! Last HTTP Code: {}".format(retries, response.status_code)
        self.logger.error(error_message)
        raise requests.exceptions.RetryError(error_message)

    def post_rebalance_task(self, account_ids: list = [], timestamp: datetime.date = None) -> requests.Response:
        """Post a rebalance task to license service

        This will start a (potentially) long running task on License Service to look at subscriptions
        that may be due for rebalance. Both args are optional, however, it highly recommended to
        restrict the rebalance task using account id(s) or a timestamp.

        Args:
            account_ids (list): List of SF account Ids to reblance
            timestampt (): timestamp to timebox the query. E.g Records from timestamp to now()


        Returns:
            response: response of the web callout. 202 = server has accepted the request

        """

        assert not isinstance(account_ids, str)
        if timestamp is not None:
            assert isinstance(timestamp, datetime.date)
            timestamp = timestamp.strftime('%Y-%m-%dT00:00:00Z')

        rebalance_task = {'accountIds': account_ids, 'timestamp': timestamp}
        endpoint = '/api/v1/tasks/rebalance'
        return self._request('post', endpoint, json=rebalance_task)

    # ----------------------------------------------------------------------------------------------
    #     The functions below are UNSAFE and AS IS. There are no restrictions on these functions,
    # and you can pass anything into them. These functions are only meant for negative testing!
    #
    #     These functions were written strictly for internal BSF Testing. Requested by jknosp for
    # easily passing in unsafe values, operations, etc. into endpoints.
    # ----------------------------------------------------------------------------------------------

    def get_license_unsafe(self, license_id=None, **kwargs):
        endpoint = '/api/v1/licenses/{}'.format(license_id) if license_id else '/api/v1/licenses'
        return self._request('get', endpoint, **kwargs)

    def delete_license_unsafe(self, license_id=None, **kwargs):
        endpoint = '/api/v1/licenses/{}'.format(license_id) if license_id else '/api/v1/licenses'
        return self._request('delete', endpoint, **kwargs)

    def post_license_unsafe(self, **kwargs):
        return self._request('post', '/api/v1/licenses', **kwargs)

    def put_license_unsafe(self, license_id=None, **kwargs):
        endpoint = '/api/v1/licenses/{}'.format(license_id) if license_id else '/api/v1/licenses'
        return self._request('put', endpoint, **kwargs)

    def get_subscription_unsafe(self, subscription_id=None, **kwargs):
        endpoint = '/api/v1/subscriptions/{}'.format(subscription_id) if subscription_id else '/api/v1/subscriptions'
        return self._request('get', endpoint, **kwargs)

    def delete_subscription_unsafe(self, subscription_id=None, **kwargs):
        endpoint = '/api/v1/subscriptions/{}'.format(subscription_id) if subscription_id else '/api/v1/subscriptions'
        return self._request('delete', endpoint, **kwargs)

    def post_subscription_unsafe(self, **kwargs):
        return self._request('post', '/api/v1/subscriptions', **kwargs)

    def put_subscription_unsafe(self, subscription_id=None, **kwargs):
        endpoint = '/api/v1/subscriptions/{}'.format(subscription_id) if subscription_id else '/api/v1/licenses'
        return self._request('put', endpoint, **kwargs)

    def get_license_summary_unsafe(self, account_id=None, feature_key=None, **kwargs):
        endpoint = '/api/v1/licenseSummary?accountId={}&featureKey={}'.format(account_id, feature_key)
        return self._request('get', endpoint, **kwargs)

    def post_license_summary_unsafe(self, **kwargs):
        endpoint = '/api/v1/licenseSummary'
        return self._request('post', endpoint, **kwargs)

    def put_license_summary_unsafe(self, account_id=None, feature_key=None, **kwargs):
        endpoint = '/api/v1/licenseSummary?accountId={}&featureKey={}'.format(account_id, feature_key)
        return self._request('put', endpoint, **kwargs)

    def delete_license_summary_unsafe(self, account_id=None, feature_key=None, **kwargs):
        endpoint = '/api/v1/licenseSummary?accountId={}&featureKey={}'.format(account_id, feature_key)
        return self._request('delete', endpoint, **kwargs)

    def get_subscription_summary_unsafe(self, account_id=None, subscription_id=None, **kwargs):
        endpoint = '/api/v1/subscriptionSummary?accountId={}&subscriptionId={}'.format(account_id, subscription_id)
        return self._request('get', endpoint, **kwargs)

    def post_subscription_summary_unsafe(self, **kwargs):
        endpoint = '/api/v1/subscriptionSummary'
        return self._request('post', endpoint, **kwargs)

    def put_subscription_summary_unsafe(self, account_id=None, subscription_id=None, **kwargs):
        endpoint = '/api/v1/subscriptionSummary?accountId={}&subscriptionId={}'.format(account_id, subscription_id)
        return self._request('put', endpoint, **kwargs)

    def delete_subscription_summary_unsafe(self, account_id=None, subscription_id=None, **kwargs):
        endpoint = '/api/v1/subscriptionSummary?accountId={}&subscriptionId={}'.format(account_id, subscription_id)
        return self._request('delete', endpoint, **kwargs)

    def _validate_http_op(self, http_op, **kwargs):
        if not hasattr(self.session, http_op):
            raise ValueError('Not a valid HTTP Request Type: \'{}\''.format(http_op))

        if http_op.upper() in ['PUT', 'POST']:
            if 'data' not in kwargs:
                raise ValueError('HTTP {} operation specified with no \'data\' entry!'.format(http_op))

    def _http_op(self, operation, endpoint, **kwargs):
        self._validate_http_op(operation, **kwargs)

        # Retrieve 'data'
        data = None
        if 'data' in kwargs:
            data = kwargs['data']
            del kwargs['data']

        # Build query params with kwargs
        endpoint += '?'
        for key, value in kwargs.items():
            endpoint += '{}={}&'.format(key, value)
        endpoint = endpoint[:-1]

        if data is not None:
            self.logger.info('{}: {}{}, DATA: {}'.format(operation.upper(), self.base_url, endpoint, json.dumps(data)))
            return self._request(operation, endpoint, data=data)

        else:
            self.logger.info('{}: {}{}'.format(operation.upper(), self.base_url, endpoint))
            return self._request(operation, endpoint)
