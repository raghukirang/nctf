import logging

from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


class OrderServiceSession(BSFObject):
    """Instantiates and provides an connection to a BSF Order Service API.

    Note:
        Access should be limited to the 'bsf_Order_fixture' fixture only.

        Requires the configuration values:
            - BSF.URLS.ORDER
            - BSF.USER
            - BSF.PASS
    """
    logger = logging.getLogger('bsf.order.interface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        ordering_url = f"{config_fixture.target.services.bsf.orderService.protocol}://{config_fixture.target.services.bsf.orderService.hostname}"
        super(OrderServiceSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username, password=config_fixture.target.predefinedAuth.bsf.password, url=ordering_url)
        self.demos_endpoint = "/api/v1/demoSubscriptions"
        self.orders_endpoint = "/api/v1/orders"
        self.product_endpoint = "/api/v1/products"
        self.service_contracts_endpoint = "/api/v1/serviceContracts"
