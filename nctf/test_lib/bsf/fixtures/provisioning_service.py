import logging

from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


class ProvisioningServiceSession(BSFObject):
    """Instantiates and provides an connection to a BSF Provisioning Service API.

    Note:
        Access should be limited to the 'provisioning_service_interface' fixture only.

        Requires the configuration values:
            - BSF.URLS.PROVISIONING
            - BSF.USER
            - BSF.PASS
    """
    logger = logging.getLogger('bsf.psinterface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        provisioning_url = f"{config_fixture.target.services.bsf.provisioningService.protocol}://{config_fixture.target.services.bsf.provisioningService.hostname}"
        super(ProvisioningServiceSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username, password=config_fixture.target.predefinedAuth.bsf.password, url=provisioning_url)
        self.device_endpoint = "/api/v1/device"

    def post_device(self, **kwargs):
        """POST a new device to Provisioning Service

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/device'
        self.logger.info('[POST] DEVICE: {}{}'.format(self.base_url, endpoint))
        response = self._request('post', endpoint, **kwargs)
        return response

    def delete_device_entitlement(self, entity_id):
        """Delete Device Entitlement for a Device by entity_id

        Args:
            entity_id (str): : The Commerce ID of the device that has been provisioned

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/device/{}'.format(entity_id)
        self.logger.info('[DELETE] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        response = self._request('delete', endpoint)
        return response
