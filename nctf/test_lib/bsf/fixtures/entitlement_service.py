import json
import logging

from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


class EntitlementServiceSession(BSFObject):
    """Instantiates and provides an connection to a BSF Entitlement Service API.

    Note:
        Access should be limited to the 'entitlement_interface' fixture only.

        Requires the configuration values:
            - BSF.URLS.ENTITLEMENT
            - BSF.USER
            - BSF.PASS
    """
    logger = logging.getLogger('bsf.entlmt.interface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        entitlement_url = f"{config_fixture.target.services.bsf.entitlementService.protocol}://{config_fixture.target.services.bsf.entitlementService.hostname}"
        super(EntitlementServiceSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username, password=config_fixture.target.predefinedAuth.bsf.password, url=entitlement_url)
        self.devices_endpoint = "/api/v1/devices"
        self.entitlement_endpoint = "/api/v1/entitlements"
        self.entitlement_summary_endpoint = "/api/v1/entitlementSummary"

    def get_device(self, commerce_id=None):
        """Retrieve a Device (via a Commerce ID or All)

        Params:
            commerce_id (str): Commerce ID (defaults to None [Grab all Device])

        Returns:
            *Requests* response from request.
        """
        if commerce_id is None:
            endpoint = '/api/v1/devices'

        else:
            endpoint = '/api/v1/devices?commerceId={}'.format(commerce_id)

        self.logger.info('[GET] DEVICE: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_entitlement(self, commerce_id=None):
        """Retrieve a Entitlement (via a Commerce ID or All)

        Params:
            commerce_id (str): Commerce ID (defaults to None [Grab all Entitlements])

        Returns:
            *Requests* response from request.
        """
        if commerce_id is None:
            endpoint = '/api/v1/entitlements'

        else:
            endpoint = '/api/v1/entitlements?commerceId={}'.format(commerce_id)

        self.logger.info('[GET] ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def post_entitlement(self, account_id, **kwargs):
        url = '/api/v1/entitlements?accountId={}'.format(account_id)
        self.logger.info('[POST] ENTITLEMENT: {}{}'.format(self.base_url, url))
        response = self._request('post', url, **kwargs)
        return response

    def delete_entitlement(self, bsfid):
        url = '/api/v1/entitlements/{}'.format(bsfid)
        self.logger.info('[DELETE] ENTITLEMENT: {}{}'.format(self.base_url, url))
        response = self._request('delete', url)
        return response

    # ----------------------------------------------------------------------------------------------
    #     The functions below are UNSAFE and AS IS. There are no restrictions on these functions,
    # and you can pass anything into them. These functions are only meant for negative testing!
    #
    #     These functions were written strictly for internal BSF Testing. Requested by jknosp for
    # easily passing in unsafe values, operations, etc. into endpoints.
    # ----------------------------------------------------------------------------------------------
    def get_device_unsafe(self, device_id=None, **kwargs):
        endpoint = '/api/v1/devices/{}'.format(device_id) if device_id else '/api/v1/devices'
        return self._request('get', endpoint, **kwargs)

    def get_device_by_account_id(self, account_id=None, **kwargs):
        endpoint = '/api/v1/devices?accountId={}'.format(account_id) if account_id else '/api/v1/devices'
        return self._request('get', endpoint, **kwargs)

    def get_device_by_mac_address(self, mac_address=None, **kwargs):
        endpoint = '/api/v1/devices?macAddress={}'.format(mac_address) if mac_address else '/api/v1/devices'
        return self._request('get', endpoint, **kwargs)

    def delete_device_unsafe(self, device_id=None, **kwargs):
        endpoint = '/api/v1/devices/{}'.format(device_id) if device_id else '/api/v1/devices'
        return self._request('delete', endpoint, **kwargs)

    def post_device_unsafe(self, **kwargs):
        return self._request('post', '/api/v1/devices', **kwargs)

    def post_device_urgent(self, **kwargs):
        return self._request('post', '/api/v1/devices/urgent', **kwargs)

    def put_device_unsafe(self, device_id=None, **kwargs):
        endpoint = '/api/v1/devices/{}'.format(device_id) if device_id else '/api/v1/devices'
        return self._request('put', endpoint, **kwargs)

    def get_entitlement_unsafe(self, entitlement_id=None, **kwargs):
        endpoint = '/api/v1/entitlements/{}'.format(entitlement_id) if entitlement_id else '/api/v1/entitlements'
        return self._request('get', endpoint, **kwargs)

    def get_entitlement_by_bsf_id(self, bsf_id=None, **kwargs):
        endpoint = '/api/v1/entitlements/{}'.format(bsf_id) if bsf_id else '/api/v1/entitlements'
        return self._request('get', endpoint, **kwargs)

    def get_entitlement_by_entity_id(self, entity_id=None, **kwargs):
        endpoint = '/api/v1/entitlements?entityId={}'.format(entity_id) if entity_id else '/api/v1/entitlements'
        return self._request('get', endpoint, **kwargs)

    def get_entitlement_by_feature_key(self, feature_key=None, **kwargs):
        endpoint = '/api/v1/entitlements?featureKey={}'.format(feature_key) if feature_key else '/api/v1/entitlements'
        return self._request('get', endpoint, **kwargs)

    def get_entitlement_by_license_id(self, license_id=None, **kwargs):
        endpoint = '/api/v1/entitlements?licenseId={}'.format(license_id) if license_id else '/api/v1/entitlements'
        return self._request('get', endpoint, **kwargs)

    def delete_entitlement_unsafe(self, entitlement_id=None, **kwargs):
        endpoint = '/api/v1/entitlements/{}'.format(entitlement_id) if entitlement_id else '/api/v1/entitlements'
        return self._request('delete', endpoint, **kwargs)

    def post_entitlement_unsafe(self, **kwargs):
        return self._request('post', '/api/v1/entitlements', **kwargs)

    def put_entitlement_unsafe(self, entitlement_id=None, **kwargs):
        endpoint = '/api/v1/entitlements/{}'.format(entitlement_id) if entitlement_id else '/api/v1/entitlements'
        return self._request('put', endpoint, **kwargs)

    def _validate_http_op(self, http_op, **kwargs):
        if not hasattr(self.session, http_op):
            raise ValueError('Not a valid HTTP Request Type: \'{}\''.format(http_op))

        if http_op.upper() in ['PUT', 'POST']:
            if 'data' not in kwargs:
                raise ValueError('HTTP {} operation specified with no \'data\' entry!'.format(http_op))

    def _http_op(self, operation, endpoint, **kwargs):
        self._validate_http_op(operation, **kwargs)

        # Retrieve 'data'
        data = None
        if 'data' in kwargs:
            data = kwargs['data']
            del kwargs['data']

        # Build query params with kwargs
        endpoint += '?'
        for key, value in kwargs.items():
            endpoint += '{}={}&'.format(key, value)
        endpoint = endpoint[:-1]

        if data is not None:
            self.logger.info('{}: {}{}, DATA: {}'.format(operation.upper(), self.base_url, endpoint, json.dumps(data)))
            return self._request(operation, endpoint, data=data)

        else:
            self.logger.info('{}: {}{}'.format(operation.upper(), self.base_url, endpoint))
            return self._request(operation, endpoint)
