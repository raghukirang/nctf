import logging
from nctf.test_lib.base.rest import RESTFixture
from nctf.test_lib.core.config import ConfigFixture
from nctf.test_lib.accounts.rest.client import AccServRESTClient
from nctf.test_lib.ncm.rest.client import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as nca
from nctf.test_lib.bsf.fixtures.vls_rest.client import VlsUserLevelRestClient

logger = logging.getLogger('test_lib.vls.rest')


class VlsUserLevelRESTFixture(RESTFixture):
    """ Factory class to handle simplified construction of the VLS Rest Client """

    def __init__(self, vls_protocol: str, vls_hostname: str, vls_port: int, accounts_client: AccServRESTClient,
                 ncm_client: NCMv1RESTClient,  user: nca.NetCloudAccount):
        self.vls_protocol = vls_protocol
        self.vls_hostname = vls_hostname
        self.vls_port = vls_port
        self.accounts_client = accounts_client
        self.ncm_client = ncm_client
        self.user = user

    def __init__(self, config_fixture: ConfigFixture, accounts_client: AccServRESTClient, ncm_client: NCMv1RESTClient,
                 user: nca.NetCloudAccount):
        self.vls_protocol = config_fixture.target.services.bsf.viewLayerSolution.protocol
        self.vls_hostname = config_fixture.target.services.bsf.viewLayerSolution.hostname
        self.vls_port = config_fixture.target.services.bsf.viewLayerSolution.port
        self.accounts_client = accounts_client
        self.ncm_client = ncm_client
        self.user = user

    def get_client(self) -> AccServRESTClient:
        client = VlsUserLevelRestClient(self.accounts_client, self.ncm_client, self.user, self.vls_protocol,
                                        self.vls_hostname, self.vls_port)
        return client

    def teardown(self):
        pass

    def get_failure_report(self):
        pass
