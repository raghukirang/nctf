import random
from requests import Response
from requests import Session
from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
from nctf.test_lib.accounts.rest.client import AccServRESTClient
from nctf.test_lib.ncm.rest.client import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as nca

ACCOUNTS_SERV_JWT_COOKIE_NAME = 'cpAccountsJwt'
DEVICE_ENTITLEMENTS_ENDPOINT = '/api/v1/deviceEntitlements'
TASK_REGRADE_TASKS_ENDPOINT = '/api/v1/tasks/regrade'


REGRADE_ACTION_UPGRADE = 'UPGRADE'
REGRADE_ACTION_DOWNGRADE = 'DOWNGRADE'


class VlsUserLevelRestClient(RESTClient):
    """ View Layer Solution User Level Rest Client """

    DEFAULT_HEADERS = {'Accept': 'application/vnd.api+json', 'Content-Type': 'application/vnd.api+json'}

    def __init__(self, accounts_client: AccServRESTClient, ncm_client: NCMv1RESTClient,  user: nca.NetCloudAccount,
                 protocol: str, hostname: str, port=443, timeout: float = 30.0, session: Session = None):
        super().__init__(protocol, hostname, port, timeout, session)
        self.accounts_client = accounts_client
        self.ncm_client = ncm_client
        self.user = user
        self.jwt = None

        # attempt to auth to acc client and get jwt
        self.get_jwt()

        # set parent account id
        self.ncm_client.authenticate(self.user.username, self.user.password)
        self.ncm_parent_account_id = self.ncm_client._ncm.accounts.parent_account_id()

        self.device_entitlements = DeviceEntitlementsEndpoint(
            self, DEVICE_ENTITLEMENTS_ENDPOINT, self.ncm_parent_account_id, self.user.netcloud_account.tenant_id)
        self.task_regrade_tasks = TasksRegradeTaskEndpoint(
            self, TASK_REGRADE_TASKS_ENDPOINT, self.ncm_parent_account_id, self.user.netcloud_account.tenant_id)

    def get_jwt(self):
        """ Authenticates to Accounts Services and stores JTW """

        self.accounts_client.authenticate(self.user.username, self.user.password)
        self.jwt = self.accounts_client.session.cookies.get('cpAccountsJwt')


class VlsRESTEndpoint(RESTEndpoint):
    """ Default Endpoint for VLS Endpoints """

    def __init__(self, client: RESTClient, api_path: str, ncm_parent_account_id: str, tenant_id: str,
                 status_checking: bool = True, enforce_trailing_slash: bool = False):
        super().__init__(client, api_path, status_checking, enforce_trailing_slash)
        self.ncm_parent_account_id = ncm_parent_account_id
        self.tenant_id = tenant_id

    def include_parent_id(self, path=None, include_tenant_id=True):
        """ Helper method to automatically include parent/tenant ids """

        if not path:
            path = ''

        query_string_delimeter = '?'
        if '?' in path:
            query_string_delimeter = '&'
        path = path + query_string_delimeter + 'parentAccount=' + self.ncm_parent_account_id
        if include_tenant_id:
            path = path + '&accountId=' + self.tenant_id

        return path

    def _get(self, resource_id: str = None, *args, **kwargs):
        return super()._get(resource_id, headers=self.client.DEFAULT_HEADERS, cookies={ACCOUNTS_SERV_JWT_COOKIE_NAME:
                                                                                       self.client.jwt}, *args,
                            **kwargs)

    def _post(self, resource_id: str = None, *args, **kwargs):
        return super()._post(resource_id, headers=self.client.DEFAULT_HEADERS, cookies={ACCOUNTS_SERV_JWT_COOKIE_NAME:
                                                                                        self.client.jwt}, *args,
                             **kwargs)


class DeviceEntitlementsEndpoint(VlsRESTEndpoint):
    """ Handles Device Entitlement Operations """

    @checked_status(200)
    def check_available_downgrade(self, mac_address: str, entity_id=None, *args, **kwargs) -> Response:
        path = 'regrade/DOWNGRADE?macAddress=' + mac_address
        path = self.include_parent_id(path)

        if entity_id is not None:
            path = path + '&entityId=' + entity_id
        return self._get(path, *args, **kwargs)

    @checked_status(200)
    def check_available_upgrade(self, mac_address: str, entity_id=None,  *args, **kwargs) -> Response:
        path = 'regrade/UPGRADE?macAddress=' + mac_address
        path = self.include_parent_id(path)

        if entity_id is not None:
            path = path + '&entityId='+entity_id
        return self._get(path, *args, **kwargs)

    @checked_status(200)
    def tier_summary(self, mac_address: str, *args, **kwargs):
        path = 'tierSummary?macAddress=' + mac_address
        path = self.include_parent_id(path)
        return self._get(path, *args, **kwargs)


class TasksRegradeTaskEndpoint(VlsRESTEndpoint):
    """ Handles regrade task operations """

    @checked_status(202)
    def upgrade(self, device_mac: str, *args, **kwargs):
        return self._regrade(REGRADE_ACTION_UPGRADE, device_mac, *args, **kwargs)

    @checked_status(202)
    def downgrade(self, device_mac: str, *args, **kwargs):
        return self._regrade(REGRADE_ACTION_DOWNGRADE, device_mac, *args, **kwargs)

    @checked_status(200)
    def tasks(self, device_mac: str, *args, **kwargs):
        path = '?macAddress=' + device_mac
        path = self.include_parent_id(path)

        return self._get(path, *args, **kwargs)

    def _regrade(self, regrade_operation: str, device_mac: str, *args, **kwargs):
        path = self.include_parent_id()
        json_api_post_payload = self._build_task_json_api(regrade_operation, device_mac)
        return self._post(path, json=json_api_post_payload, *args, **kwargs)

    def _build_task_json_api(self, regrade_operation: str, device_mac: str):
        """ Builds json api compatible post """

        # note random id's for device and acc
        # ACC: BSF does not persist accs so no Id is availble
        # DEVICE: waiting for public BSF Ids

        acc_id = random.randint(1, 100000)
        dev_id = random.randint(1, 100000)

        return {
              "data": {
                "type": "regradeTasks",
                "attributes": {
                  "regradeOperation": regrade_operation
                },
                "relationships": {
                  "device": {
                    "data": {
                      "id": dev_id,
                      "type": "devices"
                    }
                  },
                  "account": {
                    "data": {
                      "id": acc_id,
                      "type": "accounts"
                    },
                  }
                }
              },
              "included": [{
                "id": acc_id,
                "type": "accounts",
                "attributes": {
                  "tenantId": self.tenant_id
                }
              }, {
                "id": dev_id,
                "type": "devices",
                "attributes": {
                  "macAddress": device_mac
                }
              }]
            }
