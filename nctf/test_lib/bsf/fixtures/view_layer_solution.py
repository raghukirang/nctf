import logging
from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


class ViewLayerSolutionSession(BSFObject):
    """Instantiates and provides an connection to a BSF View Layer Solution API.

    Note:
        Access should be limited to the 'bsf_vls_fixture' only.

        Requires the configuration values:
            - config_fixture.target.services.bsf.viewLayerSolution.protocol
            - config_fixture.target.services.bsf.viewLayerSolution.hostname
            - config_fixture.target.predefinedAuth.bsf.username
            - config_fixture.target.predefinedAuth.bsf.password
    """
    logger = logging.getLogger('bsf.vls.interface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        bsf_url = f"{config_fixture.target.services.bsf.viewLayerSolution.protocol}://" \
                  f"{config_fixture.target.services.bsf.viewLayerSolution.hostname}"
        super(ViewLayerSolutionSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username,
                                                       password=config_fixture.target.predefinedAuth.bsf.password,
                                                       url=bsf_url)
        


