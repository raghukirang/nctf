import json
import logging

from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


class ProductSubscriptionServiceSession(BSFObject):
    """Instantiates and provides an connection to a BSF Product Subscription Service API.

    Note:
        Access should be limited to the 'product_subscription_interface' fixture only.

        Requires the configuration values:
            - BSF.URLS.PRODUCT
            - BSF.USER
            - BSF.PASS
    """
    logger = logging.getLogger('bsf.pssinterface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        product_url = f"{config_fixture.target.services.bsf.productSubscriptionService.protocol}://{config_fixture.target.services.bsf.productSubscriptionService.hostname}"
        super(ProductSubscriptionServiceSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username, password=config_fixture.target.predefinedAuth.bsf.password, url=product_url)
        self.device_entitlement_endpoint = "/api/v1/deviceEntitlements"
        self.entitlement_summary_endpoint = "/api/v1/entitlementSummary"
        self.subscription_summary_endpoint = "/api/v1/subscriptionSummary"

    def get_device_entitlement_by_entity_id(self, entity_id=None):
        """Retrieve a Device Entitlement (via an Entity ID or All)

        Args:
            entity_id (str): : The Commerce ID of the entity that has been entitled (defaults to None [Grab all Device
            Entitlements])

        Returns:
            *Requests* response from request.
        """
        if entity_id is None:
            endpoint = '/api/v1/deviceEntitlements'

        else:
            endpoint = '/api/v1/deviceEntitlements?entityId={}'.format(entity_id)

        self.logger.info('[GET] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_device_entitlement_by_commerce_id(self, commerce_id=None):
        """Retrieve a Device Entitlement (via a Commerce ID or All)

        Args:
            commerce_id (str): : The ID of the Object in the External Service (defaults to None [Grab all Device
            Entitlements])

        Returns:
            *Requests* response from request.
        """
        if commerce_id is None:
            endpoint = '/api/v1/deviceEntitlements'

        else:
            endpoint = '/api/v1/deviceEntitlements?commerceId={}'.format(commerce_id)

        self.logger.info('[GET] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint)

    def get_device_entitlement(self, bsf_id=None, **kwargs):
        """Retrieve a Device Entitlement (via a BSF ID or All)

        Args:
            bsf_id (str): : The BSF ID of the Object (defaults to None [Grab all Device Entitlements])

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/deviceEntitlements/{}'.format(bsf_id) if bsf_id else '/api/v1/deviceEntitlements'
        self.logger.info('[GET] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('get', endpoint, **kwargs)

    def post_device_entitlement_allocation(self, **kwargs):
        """Creates an allocation for a feature, for an entity

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/deviceEntitlements/allocate'
        self.logger.info('[POST] DEVICE ENTITLEMENT ALLOCATION: {}{}'.format(self.base_url, endpoint))
        response = self._request('post', endpoint, **kwargs)
        return response

    def delete_device_entitlement(self, bsf_id):
        """Deletes a Device Entitlement (via a BSF ID)

        Args:
            bsf_id (str): : The BSF ID of the Object

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/deviceEntitlements/{}'.format(bsf_id)
        self.logger.info('[DELETE] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        response = self._request('delete', endpoint)
        return response

    def deallocate_device_entitlement(self, entity_id):
        """Deallocates an entity from all Device Entitlements and corresponding Entitlements

        Args:
            entity_id (str): : The Commerce ID of the entity that has been entitled

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/deviceEntitlements/allocate?entityId={}'.format(entity_id)
        self.logger.info('[DEALLOCATE] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        response = self._request('delete', endpoint)
        return response

    def put_device_entitlement(self, bsf_id, **kwargs):
        """Updates an entity

        Args:
            bsf_id (str): : The BSF ID of the Object

        Returns:
            *Requests* response from request.
        """
        endpoint = '/api/v1/deviceEntitlements/{}'.format(bsf_id)
        self.logger.info('[PUT] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        response = self._request('put', endpoint, **kwargs)
        return response

    def _validate_http_op(self, http_op, **kwargs):
        if not hasattr(self.session, http_op):
            raise ValueError('Not a valid HTTP Request Type: \'{}\''.format(http_op))

        if http_op.upper() in ['PUT', 'POST']:
            if 'data' not in kwargs:
                raise ValueError('HTTP {} operation specified with no \'data\' entry!'.format(http_op))

    def _http_op(self, operation, endpoint, **kwargs):
        self._validate_http_op(operation, **kwargs)

        # Retrieve 'data'
        data = None
        if 'data' in kwargs:
            data = kwargs['data']
            del kwargs['data']

        # Build query params with kwargs
        endpoint += '?'
        for key, value in kwargs.items():
            endpoint += '{}={}&'.format(key, value)
        endpoint = endpoint[:-1]

        if data is not None:
            self.logger.info('{}: {}{}, DATA: {}'.format(operation.upper(), self.base_url, endpoint, json.dumps(data)))
            return self._request(operation, endpoint, data=data)

        else:
            self.logger.info('{}: {}{}'.format(operation.upper(), self.base_url, endpoint))
            return self._request(operation, endpoint)
