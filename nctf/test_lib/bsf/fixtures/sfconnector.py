import logging

import requests

from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


class SFConnectorSession(BSFObject):
    """Instantiates and provides an connection to a BSF Salesforce Connector API.

    Note:
        Access should be limited to the 'bsf_sfconnector_fixture' fixture only.

        Requires the configuration values:
            - BSF.URLS.SFCONNECTOR
            - BSF.USER
            - BSF.PASS
    """
    logger = logging.getLogger('bsf.sfconn.interface')

    def __init__(self, config_fixture):
        """Initializes Interface with configuration values"""
        sf_url = f"{config_fixture.target.services.bsf.sfconnector.protocol}://{config_fixture.target.services.bsf.sfconnector.hostname}"
        super(SFConnectorSession, self).__init__(username=config_fixture.target.predefinedAuth.bsf.username, password=config_fixture.target.predefinedAuth.bsf.password, url=sf_url)

    def get_account_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve an Account from a specified Salesforce ID.

        Args:
            commerce_id: Account Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/accounts/{}'.format(commerce_id)
        self.logger.info('[GET] ACCOUNT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def put_account(self, commerce_id: str, **kwargs) -> requests.Response:
        """PUT HTTP request for Account in SFConnector.

        Args:
            commerce_id: Account Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/accounts/{}'.format(commerce_id)
        self.logger.info('[PUT] ACCOUNT: {}{}'.format(self.base_url, endpoint))
        return self._request('PUT', endpoint, **kwargs)

    def get_contact_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve a Contact from a specified Salesforce ID.

        Args:
            commerce_id: Contact Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/contacts/{}'.format(commerce_id)
        self.logger.info('[GET] CONTACT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_device_entitlement_by_entity_id(self, entity_id: str) -> requests.Response:
        """Retrieve a Device Entitlement from a specified Device Salesforce ID.

        Args:
            entity_id: Device Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/deviceEntitlements?entityId={}'.format(entity_id)
        self.logger.info('[GET] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_device_entitlement_by_entity_id_and_subscription_id(self, entity_id: str, subscription_id: str) \
            -> requests.Response:
        """Retrieve a Device Entitlement from a specified Device Salesforce ID and Entitlement Salesforce ID.

        Args:
            entity_id: Device Salesforce ID
            subscription_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/deviceEntitlements?entityId={}&subscriptionId={}'.format(entity_id, subscription_id)
        self.logger.info('[GET] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_device_entitlement_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve a Device Entitlement from a specified Device Entitlement Salesforce ID.

        Args:
            commerce_id: Device Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/deviceEntitlements/{}'.format(commerce_id)
        self.logger.info('[GET] DEVICE ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def post_device(self, **kwargs) -> requests.Response:
        """Create a Device in SFConnector

        Returns:
            Requests response
        """
        endpoint = '/api/v1/devices'
        self.logger.info('[POST] DEVICE: {}{}'.format(self.base_url, endpoint))
        return self._request('POST', endpoint, **kwargs)

    def get_device_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve a Device from a specified Salesforce ID.

        Args:
            commerce_id: Device Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/devices/{}'.format(commerce_id)
        self.logger.info('[GET] DEVICE: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_entitlement_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve an Entitlement from a specified Salesforce ID.

        Args:
            commerce_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/entitlements/{}'.format(commerce_id)
        self.logger.info('[GET] ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_license_by_subscription_id(self, subscription_id: str) -> requests.Response:
        """Retrieve a License from a specified Entitlement Salesforce ID.

        Args:
            subscription_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/licenses?subscriptionId={}'.format(subscription_id)
        self.logger.info('[GET] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_license_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve a License from a specified Salesforce ID.

        Args:
            commerce_id: License Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/licenses/{}'.format(commerce_id)
        self.logger.info('[GET] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_all_routers(self) -> requests.Response:
        """Retrieve all routers.

        Returns:
            Requests response
        """
        endpoint = '/api/v1/products/routers'
        self.logger.info('[GET] ROUTERS: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_product_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve Router from specified Salesforce ID.

        Args:
            commerce_id: Product Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/products/'.format(commerce_id)
        self.logger.info('[GET] PRODUCT: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_product_by_sku(self, sku: str) -> requests.Response:
        """Retrieve product from specified sku.

        Args:
            sku: Product sku

        Returns:
            Requests response
        """
        endpoint = '/api/v1/products?sku={}'.format(sku)
        self.logger.info('[GET] PRODUCT BY SKU: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_subscription_by_account_id(self, account_id: str) -> requests.Response:
        """Retrieve a Subscription from an Account Salesforce ID.

        Args:
            account_id: Account Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/subscriptions?accountId={}'.format(account_id)
        self.logger.info('[GET] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_subscription_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve a Subscription from a specified Salesforce ID.

        Args:
            commerce_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/subscriptions/{}'.format(commerce_id)
        self.logger.info('[GET] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_user_by_contact_id(self, contact_id: str) -> requests.Response:
        """Retrieve a User from a specified Contact Salesforce ID.

        Args:
            contact_id: Contact Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/users?contactId={}'.format(contact_id)
        self.logger.info('[GET] USER: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def get_user_by_commerce_id(self, commerce_id: str) -> requests.Response:
        """Retrieve a User from a specified Salesforce ID.

        Args:
            commerce_id: User Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/users/{}'.format(commerce_id)
        self.logger.info('[GET] USER: {}{}'.format(self.base_url, endpoint))
        return self._request('GET', endpoint)

    def put_user(self, commerce_id: str, **kwargs) -> requests.Response:
        """PUT HTTP request for  User in SFConnector.

        Args:
            commerce_id: User Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/users/{}'.format(commerce_id)
        self.logger.info('[PUT] USER: {}{}'.format(self.base_url, endpoint))
        return self._request('PUT', endpoint, **kwargs)

    """Below are methods used for negative testing with SFConnector and are not currently supported by SFConnector"""

    def post_account(self, **kwargs) -> requests.Response:
        """POST HTTP request for Account in SFConnector.

        Returns:
            Requests response
        """
        endpoint = '/api/v1/accounts'
        self.logger.info('[POST] ACCOUNT: {}{}'.format(self.base_url, endpoint))
        return self._request('POST', endpoint, expected_status_code=200, **kwargs)

    def patch_account(self, commerce_id: str, **kwargs) -> requests.Response:
        """PATCH HTTP request for Account in SFConnector.

        Args:
            commerce_id: Account Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/accounts/{}'.format(commerce_id)
        self.logger.info('[PATCH] ACCOUNT: {}{}'.format(self.base_url, endpoint))
        return self._request('PATCH', endpoint, **kwargs)

    def delete_account(self, commerce_id: str) -> requests.Response:
        """DELETE HTTP request for Account in SFConnector.

        Args:
            commerce_id: Account Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/accounts/{}'.format(commerce_id)
        self.logger.info('[DELETE] ACCOUNT: {}{}'.format(self.base_url, endpoint))
        return self._request('DELETE', endpoint)

    def patch_device(self, commerce_id: str, **kwargs) -> requests.Response:
        """PATCH HTTP request for Device in SFConnector.

        Args:
            commerce_id: Device Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/devices/{}'.format(commerce_id)
        self.logger.info('[PATCH] DEVICE: {}{}'.format(self.base_url, endpoint))
        return self._request('PATCH', endpoint, **kwargs)

    def put_device(self, commerce_id: str, **kwargs) -> requests.Response:
        """PUT HTTP request for Device in SFConnector.

        Args:
            commerce_id: Device Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/devices/{}'.format(commerce_id)
        self.logger.info('[PUT] DEVICE: {}{}'.format(self.base_url, endpoint))
        return self._request('PUT', endpoint, **kwargs)

    def delete_device(self, commerce_id: str) -> requests.Response:
        """Delete HTTP request for Device in SFConnector.

        Args:
            commerce_id: Device Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/devices/{}'.format(commerce_id)
        self.logger.info('[DELETE] DEVICE: {}{}'.format(self.base_url, endpoint))
        return self._request('DELETE', endpoint)

    def post_entitlement(self, **kwargs) -> requests.Response:
        """POST HTTP Request for Entitlement in SFConnector.

        Returns:
            Requests response
        """
        endpoint = '/api/v1/entitlements'
        self.logger.info('[POST] ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('POST', endpoint, **kwargs)

    def patch_entitlement(self, commerce_id: str, **kwargs) -> requests.Response:
        """PATCH HTTP Request for Entitlement in SFConnector.

        Args:
            commerce_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/entitlements/{}'.format(commerce_id)
        self.logger.info('[PATCH] ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('PATCH', endpoint, **kwargs)

    def put_entitlement(self, commerce_id: str, **kwargs) -> requests.Response:
        """PUT HTTP Request for Entitlement in SFConnector.

        Args:
            commerce_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/entitlements/{}'.format(commerce_id)
        self.logger.info('[PUT] ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('PUT', endpoint, **kwargs)

    def delete_entitlement(self, commerce_id: str) -> requests.Response:
        """DELETE HTTP Request for Entitlement in SFConnector.

        Args:
            commerce_id: Entitlement Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/entitlements/{}'.format(commerce_id)
        self.logger.info('[DELETE] ENTITLEMENT: {}{}'.format(self.base_url, endpoint))
        return self._request('DELETE', endpoint)

    def post_license(self, **kwargs) -> requests.Response:
        """POST HTTP Request for License in SFConnector.

        Returns:
            Requests response
        """
        endpoint = '/api/v1/licenses'
        self.logger.info('[POST] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('POST', endpoint, expected_status_code=201, **kwargs)

    def patch_license(self, commerce_id: str, **kwargs) -> requests.Response:
        """PATCH HTTP Request for License in SFConnector.

        Args:
            commerce_id: License Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/licenses/{}'.format(commerce_id)
        self.logger.info('[PATCH] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('PATCH', endpoint, **kwargs)

    def put_license(self, commerce_id: str, **kwargs) -> requests.Response:
        """PUT HTTP Request for License in SFConnector.

        Args:
            commerce_id: License Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/licenses/{}'.format(commerce_id)
        self.logger.info('[PUT] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('PUT', endpoint, **kwargs)

    def delete_license(self, commerce_id: str) -> requests.Response:
        """DELETE HTTP Request for License in SFConnector.

        Args:
            commerce_id: License Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/licenses/{}'.format(commerce_id)
        self.logger.info('[DELETE] LICENSE: {}{}'.format(self.base_url, endpoint))
        return self._request('DELETE', endpoint)

    def post_subscription(self, **kwargs) -> requests.Response:
        """POST HTTP Request for Subscription in SFConnector.

        Returns:
            Requests response
        """
        endpoint = '/api/v1/subscriptions'
        self.logger.info('[POST] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('POST', endpoint, expected_status_code=201, **kwargs)

    def patch_subscription(self, commerce_id: str, **kwargs) -> requests.Response:
        """PATCH HTTP Request for Subscription in SFConnector.

        Args:
            commerce_id: Subscription Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/subscriptions/{}'.format(commerce_id)
        self.logger.info('[PATCH] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('PATCH', endpoint, **kwargs)

    def put_subscription(self, commerce_id: str, **kwargs) -> requests.Response:
        """PUT HTTP Request for Subscription in SFConnector.

        Args:
            commerce_id: Subscription Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/subscriptions/{}'.format(commerce_id)
        self.logger.info('[PUT] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('PUT', endpoint, **kwargs)

    def delete_subscription(self, commerce_id: str) -> requests.Response:
        """DELETE HTTP Request for Subscription in SFConnector.

        Args:
            commerce_id: Subscription Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/subscriptions/{}'.format(commerce_id)
        self.logger.info('[DELETE] SUBSCRIPTION: {}{}'.format(self.base_url, endpoint))
        return self._request('DELETE', endpoint)

    def post_user(self, **kwargs) -> requests.Response:
        """POST HTTP request for User in SFConnector.

        Returns:
            Requests response
        """
        endpoint = '/api/v1/users'
        self.logger.info('[POST] USER: {}{}'.format(self.base_url, endpoint))
        return self._request('POST', endpoint, expected_status_code=200, **kwargs)

    def delete_user(self, commerce_id: str, **kwargs) -> requests.Response:
        """DELETE HTTP request for  User in SFConnector.

        Args:
            commerce_id: User Salesforce ID

        Returns:
            Requests response
        """
        endpoint = '/api/v1/users/{}'.format(commerce_id)
        self.logger.info('[DELETE] USER: {}{}'.format(self.base_url, endpoint))
        return self._request('DELETE', endpoint, **kwargs)
