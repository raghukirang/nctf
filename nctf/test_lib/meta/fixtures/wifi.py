import logging

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.core.arm2.interface import ARMInterface
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPIFixture
from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture

logger = logging.getLogger("test_lib.meta.fixtures.wifi")


class WifiMetaFixture(FixtureBase):
    def __init__(self, tlogger: logging.Logger, arm_fixture: ARMInterface, router_rest_fixture: RouterRESTAPIFixture,
                 wifi_client_fixture: WiFiClientFixture, fwloader_v2_fixture: FwLoaderFixtureV2):
        """Fixture strictly meant to reduce boilerplate in tests by aggregating useful fixtures

        Args:
            tlogger: The special logger specifically used for logging in tests.
            arm_fixture: Fixture for leasing hardware from the Automation Resource Manager
            router_rest_fixture: Fixture for communicating with coconut firmware routers
            wifi_client_fixture: Fixture for communication with arbitrary wifi clients e.g. Android phones
            fwloader_v2_fixture: Fixture that enables easy coconut firmware loading from guido/local locations.

        """
        self.tlogger = tlogger
        self.arm_fixture = arm_fixture
        self.router_rest_fixture = router_rest_fixture
        self.wifi_client_fixture = wifi_client_fixture
        self.firmware_loader_fixture = fwloader_v2_fixture

    def teardown(self):
        """Teardown all the things."""
        logger.debug(f"{self} tearing down {self.arm_fixture}, {self.router_rest_fixture}, "
                     f"{self.wifi_client_fixture}, {self.firmware_loader_fixture}...")

        self.arm_fixture.teardown()
        self.router_rest_fixture.teardown()
        self.wifi_client_fixture.teardown()
        self.firmware_loader_fixture.teardown()

    def get_failure_report(self):
        pass
