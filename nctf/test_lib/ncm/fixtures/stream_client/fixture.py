from json import dumps
import logging
import random
import re
import time

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.ncm.fixtures.stream_client import ecm
from nctf.test_lib.ncm.fixtures.stream_client.client import Client
from nctf.test_lib.ncm.fixtures.stream_client.exceptions import ClientNotInstantiatedException

logger = logging.getLogger('fixtures.stream_client')


class StreamClientFixture(FixtureBase):
    POLL_TIMEOUT = 60

    def __str__(self):
        return "<StreamClientFixture>"

    def __init__(self, url: str, port: int):
        self.url = url
        self.port = port
        self.stream_url = "{}:{}".format(self.url, self.port)
        self.client = None
        self.mac = None
        self.poll_results = []
        self.triggers = []
        self.poll_id = None
        self.kwargs = {}
        self.context = {}
        # Bools used to determine state during cleanup:
        self.connected = False
        self.authorized = False
        self.bound = False

    @staticmethod
    def generate_mac_address() -> str:
        """ Creates a randomly generated mac address which begins with "de:ad:"

        Returns:
            A MAC address that has been verified.
        """

        def random_hex_pair():
            value = "{:02x}".format(random.randint(0, 255))
            return value

        mac = "de:ad:{}:{}:{}:{}".format(*(random_hex_pair() for _ in range(4)))
        return mac

    @staticmethod
    def _is_valid_mac_address(mac: str) -> bool:
        """ Verifies that the mac address is a valid format.

        Args:
            mac:

        Returns:
            True if the regex matches, false if it does not.
        """
        return bool(re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac.lower()))

    def update_kwargs(self, kwargs: dict) -> None:
        """ Updates the kwargs property with the latest information provided by the test.

        Args:
            kwargs:

        """
        z = self.kwargs.copy()
        z.update(kwargs)
        self.kwargs = z

    def create_client(self, kwargs: dict) -> None:
        """ Instantiate the Stream Client.

        Args:
            kwargs: The environmental values that the test controls.

        """
        mac = kwargs.get('mac', None)
        self.client = Client(host=self.stream_url, port=self.port, mac=mac)
        self.client.initialize()
        self.mac = mac
        logger.info("Created the stream client.")
        self.update_kwargs(kwargs)

    def connect(self) -> None:
        """ Connects to the ECM Stream Server. """
        self.client.connect()
        if not self.client:
            raise ClientNotInstantiatedException("Failed to instantiate the stream client during initialization.")
        else:
            self.connected = True
            logger.info("Stream client connected.")

    def disconnect(self) -> None:
        """ Disconnects from the ECM Stream Server. """
        self.client.disconnect()
        self.connected = False

    def authorize(self, kwargs: dict) -> None:
        """ Authorizes the client

        Args:
            kwargs: The environmental values that the test controls, these could include either the
            username and password, or the token_secret and token_id retrieved from an activation.

        Note:
            If the token_secret has been set, will attempt to use it first, and then fall back to
            authenticating via username and password.

        """
        self.update_kwargs(kwargs)
        token_secret = kwargs.get('token_secret')
        token_id = kwargs.get('token_id')
        username = kwargs.get('username')
        password = kwargs.get('password')

        if token_secret and token_id == 0:
            credentials = {"token_secret": token_secret, "token_id": token_id}
        elif username and password:
            credentials = {"username": username, "password": password}
        else:
            logger.error("Must set token_secret and token_id or username and password")
            return
        try:
            self.client.authorize(**credentials)
            self.authorized = True
            logger.info("Stream client authorized.")
        except Exception as exc:
            logger.error(exc)
            raise exc

    def register(self, product: str = None) -> dict:
        """ Used to register the client whose information was passed in when create_client() was called.

        Note:
            Think of this like logging into your router and registering to ecm via the router ui.

        Arguments:
            product: must be a valid Cradlepoint product, i.e. AER3100

        Returns:
            A diction that contains; client_id, token_secret, and token_id

        """
        try:
            if product:
                token = self.client.register(product)
            else:
                token = self.client.register()
            if token:
                self.kwargs['client_id'] = token.get('client_id', None)
                logger.info("Stream client registered.")
                return token
            else:
                logger.warning("Failed to register the device: {}".format(self.mac))
        except Exception as exc:
            logger.error(exc)
            raise exc

    def unregister(self) -> None:
        """ Unregisters the client from the stream and updates the self.bound, and self.authorized
        properties to False."""
        try:
            self.client.unregister()
            self.bound = False
            self.authorized = False
            self.poll_id = None
            self.kwargs['client_id'] = None
        except Exception as exc:
            logger.error(exc)
            raise exc

    def bind(self, client_id: int = None) -> None:
        """ Binds the stream client to the server with the requested client_id.

        Args:
            client_id: The ecm ID value assigned to the client during activation or registration.

        Note:
            The client_id value is allowed to be None in the event the tester wants to attempt to bind to
            a null value.

        """
        if client_id is None:
            logger.warning("bind: requested to bind against a null client_id.")
        else:
            store_cid = {'client_id': client_id}
            self.update_kwargs(store_cid)
        try:
            self.client.bind(client_id)
            self.bound = True
            logger.info("Stream client bound.")
        except Exception as exc:
            logger.error(exc)

    def start_poll(self) -> str:
        """ Instruct the stream server to start a long poll.

        Returns:
            poll_id: The mailbox id that the stream server will use to send messages to.

        """
        logger.debug("start_poll: initiating poll")
        self.poll_id = self.client.start_poll()
        logger.debug("start_poll: poll created with id: {}".format(self.poll_id))
        return self.poll_id

    def poll(self, poll_id: int = None, timeout: int = None) -> None:
        """ Retrieves all responses from the stream server to the client.

        Args:
            poll_id: The index value of the mailbox which was provided when start_poll() is called.
            timeout: The amount of time we will loop to retrieve messages, defaults to StreamClientFixture.POLL_TIMEOUT.

        Note:
            Calls single_poll() for the duration of the timeout. The results are updated to the property:
            self.poll_results

        """
        logger.info("poll: called with timeout: {}".format(timeout))
        poll_id = poll_id or self.poll_id
        timeout = timeout or StreamClientFixture.POLL_TIMEOUT
        stop_time = time.time() + timeout
        while time.time() < stop_time:
            result = self.single_poll(poll_id=poll_id)
            if result == -1:
                time.sleep(1)

    def single_poll(self, poll_id: int = None) -> dict:
        """ Retrieves the most recent message from the requested poll_id. If a poll ID is not requested,
        it will default to the property self.poll_id, which gets set when start_poll() is called.

        Arguments:
            poll_id:

        Returns:
            A dictionary containing the results of the first response in the mailbox.
        """
        poll_id = poll_id or self.poll_id
        try:
            current_queue = self.client.stream.mailboxes[poll_id]
        except KeyError:
            try:
                self.client.stream.drain_events(timeout=1)
            except ecm.base.Timeout:
                logger.debug("single_poll: drain_events Encountered Stream Timeout")
            try:
                current_queue = self.client.stream.mailboxes[poll_id]
            except KeyError:
                logger.info("single_poll: requested mailbox contains no response.")
                return -1
        if current_queue:
            try:
                result = self.client.stream.get_response(poll_id)
                self.poll_results.append(result)
                # logger.debug("Poll Results Length: {}".format(len(self.poll_results)))
                logger.debug("single_poll: found result")
                return result
            except ecm.base.Timeout:
                logger.info("single_poll: requested mailbox is empty.")
                return -1

    def check_activation(self) -> dict:
        """ Checks for the presence of an activation.

        Note:
            If an activation has not been pre-defined, you will need to call register to connect the client.

        Returns:
            The activation values returned in dictionary form.
            # TODO: I haven't actually seen an activation, so redress this when that code is working.

        """
        try:
            activation = self.client.check_activation()
        except Exception as exc:
            logger.info(exc)
            return None
        else:
            logger.info("check_activation() located an activation: {}".format(activation))
            return activation

    def check_triggers(self) -> list:
        """ Retrieves any triggers that are currently stored within poll_responses.

        Iterates over the self.poll_results property list and copies any triggers into the
        self.triggers property list, and then returns that list.

         """
        if not self.poll_results:
            logger.error("retrieve_triggers: failed to locate any values in the poll_results list.")
        for request in self.poll_results:
            try:
                if request['request']['event_trigger']:
                    self.triggers.append(request)
            except KeyError:
                continue
        return self.triggers

    def retrieve_request(self, search_value: str) -> dict:
        """ Retrieves a stream server request from self.poll_results that contains the requested string.

        This method loops over the list of results in poll_results, and individually examines
        each request by first converting it to a json object that it can then attempt to find the
        search string within. It will remove and return the first match found.

        Args:
            search_value:

        Returns:
            The stream server request that contains the requested search string.
        """
        for request in self.poll_results:
            r = dumps(request)
            if search_value in r:
                found = request
                self.poll_results.remove(request)
                return found

    @staticmethod
    def format_message_response(msg: dict, success: bool, response_content: dict, exception=None, message=None) -> dict:
        """
        Formats the response to send back for the given message (msg).

        Args:
            msg: The message to send back
            success: Is the response a success or failure state
            response_content: The data to return. Must be JSON serializable
            exception: The string representation of an exception
            message: The original message object received from the server.

        Returns:
            A properly formatted response message that can be passed to post().
        """
        queue = msg.get('response_queue')
        response_id = msg.get('response_id')
        # Handle varying response ID location in messages vs triggers.
        if not response_id:
            response_id = msg['request']['event_trigger']['id']
            logger.info("Formatting Remote Task Message")
        else:
            logger.info("Formatting Event Trigger Message")
        if not response_id:
            logger.error("Failed to retrieve the response_id from the message.")

        data = {
            'queue': queue,
            'id': response_id,
            'value': {
                "success": success,
                "data": response_content,
                "exception": exception,
                "message": message
            }
        }
        return data

    def post(self, data: dict) -> None:
        """ Sends the data to the stream server.

        Note: Under construction, need to determine data structure.

        Arguments:
            data: To Be Determined.

        """
        self.client.post(data)

    def get_failure_report(self) -> None:
        raise NotImplemented

    def teardown(self) -> None:
        try:
            client_id = self.kwargs.get('client_id', None)
            if self.connected and self.authorized and not self.bound and client_id:
                self.bind(client_id)
                self.unregister()
                self.disconnect()
            elif self.connected and self.authorized and self.bound and client_id:
                self.unregister()
                self.disconnect()
            elif not self.connected:
                return
            else:
                self.disconnect()
        except Exception as e:
            logger.error("Teardown failed with exception: {}".format(e))
            raise e
