"""
Exceptions for the stream_client_fixture.
"""


class StreamClientException(Exception):
    """Base class for all stream_client_fixture exceptions."""


class ClientNotInstantiatedException(StreamClientException):
    """Raised when the client has not been instantiated."""
