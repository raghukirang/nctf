"""Stream Client

Derived from sgulp/tester.py, this simplifies the sgulp Client by eliminating
all the parallel execution used to implement multiple clients.

"""

from __future__ import division
from __future__ import print_function

import bisect
import fcntl
import itertools
import logging
import os
import random
import time

from nctf.test_lib.ncm.fixtures.stream_client.ecm import base
from nctf.test_lib.ncm.fixtures.stream_client.ecm.event import EventHub
from nctf.test_lib.ncm.fixtures.stream_client.ecm.stream import StreamConnectionV1

try:
    from time import monotonic as iotime
except ImportError:
    from time import time as iotime
logger = logging.getLogger('stream_client.instance')


class Client(object):

    system_id = itertools.count()

    def __str__(self):
        return "<StreamClientInstance>"

    def __init__(self, host=None, port=None, mac=None):
        self.bytes_counter = itertools.count()
        self.host = host
        self.port = port
        self.mac = mac
        self.product = None
        self.mac_base = random.randint(0, (1 << 48) - 1)
        self.stream = None
        self.event_refs = []
        self.callbacks = []
        self.callback_deadlines = []
        self.callback_autoid = 0
        self.eventhub = EventHub()
        self.wakee_fd, wfd = os.pipe()
        self.wakee = os.fdopen(self.wakee_fd, 'rb', 0)
        fops = fcntl.fcntl(self.wakee_fd, fcntl.F_GETFL)
        fcntl.fcntl(self.wakee_fd, fcntl.F_SETFL, fops | os.O_NONBLOCK)
        self.waker = os.fdopen(wfd, 'wb', 0)
        self.eventhub.register(self.wakee, self.eventhub.READABLE)
        self.poll_map = {}
        self._shutdown = False

    def shutdown(self):
        self._shutdown = True
        self.wakeup()

    def ioloop_cycle(self, deadline):
        if self.callbacks:
            deadline = max(0, min(self.callback_deadlines[0] - iotime(), deadline))
        events = self.eventhub.poll(timeout=deadline)
        if not events:
            cbs = []
            while self.callbacks and \
                  self.callback_deadlines[0] <= iotime():
                cbs.append(self.callbacks.pop(0))
                del self.callback_deadlines[0]
            for id_, cb, args, kwargs in cbs:
                try:
                    cb(*args, **kwargs)
                except Exception as e:
                    logger.error('IOLoop Callback Error:', e)
            return 0
        count = 0
        for fd, ev in events:
            if fd == self.wakee_fd:
                try:
                    self.wakee.read()
                except IOError:
                    pass
                continue
            stream = self.poll_map[fd]
            count += stream.drain_events()
            if stream._poll_id:
                while True:
                    try:
                        poll_msg = stream.get_response(stream._poll_id, timeout=0)
                    except base.Timeout:
                        break
                    else:
                        try:
                            self.handle_poll_response(stream, poll_msg)
                        except Exception as e:
                            logger.error('IOLoop Response Error:', e)
            # Perform garbage collection for remaining mailboxes.
            for cmd_id in list(stream.mailboxes):
                stream.get_response(cmd_id, timeout=0)
                count += 1
        return count

    def ioloop(self):
        while not self._shutdown:
            self.ioloop_cycle(1)

    def call_soon(self, callback, *args, **kwargs):
        return self.call_later(0, callback, *args, **kwargs)

    def call_repeatedly(self, interval, callback, *args, **kwargs):
        self.callback_autoid += 1
        id_ = self.callback_autoid

        def fn():
            try:
                callback(*args, **kwargs)
            finally:
                self._call_later(id_, interval, fn)

        return self._call_later(id_, interval, fn)

    def _call_later(self, id_, delay, callback, *args, **kwargs):
        deadline = iotime() + delay
        index = bisect.bisect_right(self.callback_deadlines, deadline)
        self.callbacks.insert(index, (id_, callback, args, kwargs))
        self.callback_deadlines.insert(index, deadline)
        self.wakeup()
        return id_

    def call_later(self, *args, **kwargs):
        self.callback_autoid += 1
        return self._call_later(self.callback_autoid, *args, **kwargs)

    def cancel_call(self, id_):
        for i, x in enumerate(self.callbacks):
            if x[0] == id_:
                index = i
                break
        else:
            raise ValueError('callback not found: %d' % id_)
        del self.callbacks[index]
        del self.callback_deadlines[index]

    def wakeup(self):
        """ Wakeup the IOLoop. """
        self.waker.write(b'x')

    def progress(self, statement, current, total):
        pass

    def initialize(self):
        host = self.host
        hostname, port = host.rsplit(':', 1)
        self.stream = StreamConnectionV1(host=hostname, port=int(port))
        self.stream._poll_id = None
        self.stream.connect_timeout = 3600

    def connect(self):
        logger.debug("client.connect called")
        self.stream.connect()
        self.eventhub.register(self.stream.sock, self.eventhub.READABLE)
        self.poll_map[self.stream.sock.fileno()] = self.stream

    def disconnect(self):
        logger.debug("client.disconnect called")
        self.eventhub.unregister(self.stream.sock)
        del self.poll_map[self.stream.sock.fileno()]
        self.stream.close()

    def get_response(self, cmd_id, timeout=60):
        deadline = time.time() + timeout
        while time.time() < deadline:
            try:
                self.stream.drain_events(timeout=1)
                response = self.stream.get_response(cmd_id=cmd_id, timeout=0)
                return response
            except base.Timeout:
                pass
        logger.error("Failed to retrieve a response within {} seconds.".format(timeout))
        raise base.Timeout()

    def check_activation(self):
        logger.debug("client.check_activation called")

        # NOTE: Copied from rome/core/apps/models.py and MUST match exactly!
        def hash_v2(value):
            import hashlib
            min_loops = 10000  # about 20ms on i7
            max_loops = 40000  # about 60ms on i7
            loops = sum(ord(x) + 1 for x in value)
            i = 3
            while loops < min_loops or loops > max_loops:
                i += 1
                loops = (loops + i)**3 if loops < min_loops else loops % max_loops
            output = None
            for x in range(loops):
                output = hashlib.sha1((output if output else value).encode()).hexdigest()
            return output

        secret = hash_v2(self.mac)
        args = dict(secrethash=secret)
        cmd_id = self.stream.execute_command('check_activation', args)
        return self.get_response(cmd_id)

    def authorize(self, **credentials):
        logger.debug("client.authorize called")
        cmd_id = self.stream.authorize(**credentials)
        return self.get_response(cmd_id)

    def register(self, product="AER3100"):
        logger.debug("client.register called")
        self.product = product
        cmd_id = self.stream.register({'product': product, 'mac': self.mac, 'name': 'Journey Interactive Stream Client'})
        return self.get_response(cmd_id)

    def unregister(self):
        logger.debug("client.unregister called")
        cmd_id = self.stream.unregister()
        return self.get_response(cmd_id)

    def bind(self, client_id):
        logger.debug("client.bind called")
        cmd_id = self.stream.bind(client_id)
        return self.get_response(cmd_id)

    def start_poll(self):
        return self.stream.start_poll()

    def post(self, msg):
        cmd_id = self.stream.post(msg)
        return self.get_response(cmd_id)
