'''
Client Stream API for CradlePoint ECM service.

This is a simple Python abstraction for the Stream interface used in the
CradlePoint ECM service.  This is for client devices wishing to interact
with ECM's long poll based streaming API.
'''

__copyright__ = '''
Copyright (c) 2013 CradlePoint, Inc. <www.cradlepoint.com>.
All rights reserved.

This file contains confidential information of CradlePoint, Inc. and your
use of this file is subject to the CradlePoint Software License Agreement
distributed with this file. Unauthorized reproduction or distribution of
this file is subject to civil and criminal penalties. '''

import errno
import json
import logging
import socket
import ssl
import struct
import threading
import time
import zlib

# from nctf.fixtures.s3simulator.device.simclient.ecm import base, event
from nctf.test_lib.ncm.fixtures.stream_client.ecm import base
from nctf.test_lib.ncm.fixtures.stream_client.ecm import event

try:  # python > 3.0.0
    import queue
except ImportError:
    import queue as queue

logger = logging.getLogger('Stream')

USE_SSL = True


class StreamTranscoder(object):
    """ Versioned data encoder and decoder interface. """

    def header_encode(self, *args):
        """ Return bytes type data intended for wire transmission. """
        raise NotImplementedError()

    def header_decode(self, *args):
        """ Return native data structure from the bytes input data.
        Also verify that the data is well formed. """
        raise NotImplementedError()

    def encode(self, obj):
        """ Return bytes type data intended for wire transmission. """
        raise NotImplementedError()

    def decode(self, data):
        """ Return native data structure from the bytes input data. """
        raise NotImplementedError()


class StreamTranscoderV1(StreamTranscoder):
    """ Version 1 data transcoder.  This encodes data in JSON and uses
    zlib compression when the data is large enough to warrent doing so. """

    header_sof_magic = 194
    header_struct = struct.Struct('!BII')
    header_size = header_struct.size
    encoder = json.JSONEncoder(separators=(',', ':'))
    decoder = json.JSONDecoder()
    compression_levels = [
        # (minimum size, compression level or 0 to disable)
        (0, 0),
        (100, 1),
        (1000, 3),
        (10000, 6),
        (100000, 9),
    ]

    def _calc_sof(self, *numbers):
        """ Calculate the start of frame byte from our seed numbers. """
        seed = (sum(numbers) & 0xff)
        return self.header_sof_magic ^ (seed ^ 0xff)

    def header_encode(self, size, cmd_id):
        """ Prefix a magic framing byte that we XOR with the one's complement
        of the last 8 bits of the size and cmd_id sum.  This gives us better
        integrity over using just using a fixed SOF byte. """
        sof = self._calc_sof(size, cmd_id)
        return self.header_struct.pack(sof, size, cmd_id)

    def header_decode(self, data):
        """ Ensure that our header is correct and return the size and
        cmd_id if so. """
        sof_claim, size, cmd_id = self.header_struct.unpack(data)
        sof_actual = self._calc_sof(size, cmd_id)
        if sof_claim != sof_actual:
            raise ValueError('Invalid start of frame byte')
        return size, cmd_id

    def decode(self, data):
        """ Inspect the first byte for a compression hint and return
        a native python object. """
        compressed = data[0:1] == b'\x01'
        data = data[1:]
        if compressed:
            data = self.decompress(data)
        return self.decoder.decode(data.decode())

    def encode(self, obj):
        """ Convert to JSON and optionally compress if large enough. """
        json_data = self.encoder.encode(obj).encode()
        size = len(json_data)
        level = max(level for min_size, level in self.compression_levels if size >= min_size)
        if level:
            return b'\x01' + self.compress(json_data, level)
        else:
            return b'\x00' + json_data

    def compress(self, data, level):
        return zlib.compress(data, level)

    def decompress(self, data):
        return zlib.decompress(data)


class StreamConnectionV1(object):
    """ This code implements the streaming protocol for full duplex
    message exchange over wide area links.  It's primary function is
    to handle request/response task processing.  If you are implementing
    an ECM client device you should use this to handle realtime task
    execution and event delivery. """

    version = 1
    version_struct = struct.Struct('!B')
    transcoder = StreamTranscoderV1()
    connect_timeout = 60
    mailbox_max = 100

    def __init__(self, host=None, port=None):
        self.shutdown = False
        self.host = host
        self.port = port
        self.client_id = None
        self.poll_callbacks = {}
        self.write_lock = threading.RLock()
        self.mailboxes_lock = threading.RLock()
        self.mailboxes = {}
        self._cmd_id_offt = 0
        self.readable = event.EventHub()
        self.writeable = event.EventHub()
        self._read_buf = bytearray()

    def connect(self):
        with self.write_lock:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock = ssl.wrap_socket(sock) if USE_SSL else sock
            self.sock.settimeout(self.connect_timeout)
            self.sock.connect((self.host, self.port))
            self.sock.send(self.version_struct.pack(self.version))
            self.sock.setblocking(0)
            self.readable.register(self.sock, self.readable.READABLE)
            self.writeable.register(self.sock, self.writeable.WRITABLE)

    def close(self):
        with self.write_lock:
            self.readable.unregister(self.sock)
            self.writeable.unregister(self.sock)
            self.sock.shutdown(socket.SHUT_RDWR)
            self.sock.close()

    def authorize(self, **creds):
        """ Issue an authorize command and return the command id used to see
        the result. """
        return self.execute_command('authorize', creds)

    def register(self, info):
        """ Retrieve new client credentials and register a client with ECM. """
        return self.execute_command('register', info)

    def unregister(self):
        """ Remove our client object from the stream server.  To renable
        access you must re-authorize and re-register. """
        return self.execute_command('unregister')

    def bind(self, client_id):
        """ Bind this client to the server.  This sets the client state to
        online in the database and hooks us up to the message task queue. """
        return self.execute_command('bind', {'client_id': client_id})

    def post(self, msg):
        """ Send a message to the stream server.  This is probably a response
        to a task request but could also be a dynamic event we generated. """
        return self.execute_command('post', msg)

    def start_poll(self):
        """ Instruct the server to start a long poll.  The messages from this
        request will be stored in the mailbox identified by our return value.
        """
        return self.execute_command('start_poll')

    def stop_poll(self):
        """ Instruct the server to stop a long poll.  This should be used when
        using the connection for multiple purposes. """
        return self.execute_command('stop_poll')

    def get_response(self, cmd_id, timeout=None):
        """ Wait for a message in our mailbox queue. """
        mailbox = self.get_mailbox(cmd_id)
        try:
            msg = mailbox.get(timeout=timeout)
        except queue.Empty:
            raise base.Timeout()
        else:
            mailbox.task_done()
        finally:
            self.gc_mailbox(cmd_id)
        return self.msg_filter(msg)

    def sock_read(self, size):
        """ SSL agnostic socket reader that won't block if the ssl buffer
        is holding our data. """
        try:
            recv = self.sock.read
        except AttributeError:
            recv = self.sock.recv
        try:
            data = recv(size)
        except ssl.SSLError as e:
            if e.args[0] == ssl.SSL_ERROR_WANT_READ:
                return None
            raise
        except socket.error as e:
            if e.errno == errno.EAGAIN:
                return None
            raise
        if data == b'':
            raise IOError("Connection closed by peer")
        return data

    def sock_read_complete(self, size, timeout=None):
        """ Block until we get the entire size of data. """
        buf = []
        _timeout = timeout
        if timeout is not None:
            start_ts = time.time()
        while True:
            while size:
                data = self.sock_read(size)
                if data is None:
                    break
                buf.append(data)
                size -= len(data)
            if size:
                if timeout is not None:
                    _timeout = max(0, timeout - (time.time() - start_ts))
                if not self.readable.poll(maxevents=1, timeout=_timeout):
                    raise base.Timeout()
            else:
                return b''.join(buf)

    def _recv_msg(self):
        """ Attempt to read a message off the 'wire'.  If we can't get
        a message header off the wire then we return. """
        try:
            header = self.sock_read_complete(self.transcoder.header_size, timeout=0)
        except base.Timeout:
            return None, None
        msg_size, incoming_cmd_id = self.transcoder.header_decode(header)
        msg_data = self.sock_read_complete(msg_size)
        return incoming_cmd_id, self.transcoder.decode(msg_data)

    def drain_events(self, timeout=None, maxevents=None):
        """ Read messages from the server and store them in threadsafe queues.
        If maxevents is set we limit the number of events we gather before
        returning control to the caller. Otherwise we drain all pending
        events available and block for timeout seconds if none are availabe.
        We return the number of messages handled. """
        count = 0
        if timeout is not None:
            start_ts = time.time()
        while True:
            while maxevents is None or count < maxevents:
                incoming_cmd_id, msg = self._recv_msg()
                if msg is None:
                    break
                #logger.info('READ: {}'.format(msg))
                count += 1
                with self.mailboxes_lock:
                    mailbox = self.get_mailbox(incoming_cmd_id)
                    mailbox.put(msg, block=False)
            if not count:
                if timeout is not None:
                    timeout = max(0, timeout - (time.time() - start_ts))
                if not self.readable.poll(timeout, maxevents=1):
                    raise base.Timeout()
            else:
                return count

    def get_mailbox(self, cmd_id):
        """ Get or create a mailbox queue for the provided command identifier.
        """
        with self.mailboxes_lock:
            try:
                return self.mailboxes[cmd_id]
            except KeyError:
                mb = self.mailboxes[cmd_id] = queue.Queue(self.mailbox_max)
                return mb

    def gc_mailbox(self, cmd_id):
        """ Do garbage collection on a potentially empty mailbox. """
        with self.mailboxes_lock:
            if self.mailboxes[cmd_id].empty():
                del self.mailboxes[cmd_id]

    def msg_filter(self, msg):
        """ Strip the protocol header off a message and possible raise an
        exception if things did not work correctly. """
        if not msg['success']:
            raise base.exc_map.get(msg['exception'], base.RootException)(msg)
        return msg.get('data')

    def write(self, obj):
        """ Atomic write of entire message object.  Note that we have to
        around an outstanding python bug http://bugs.python.org/issue8240
        for SSL based connections. """
        data = self.transcoder.encode(obj)
        #logger.info('WRITE: {}'.format(obj))
        with self.write_lock:
            self._cmd_id_offt += 1
            cmd_id = self._cmd_id_offt
            header = self.transcoder.header_encode(len(data), cmd_id)
            buf = header + data
            remain = len(buf)
            while remain:
                i = self.sock.send(buf)
                if i:
                    # Never happens with ssl which protects our buf addr.
                    buf = buf[i:]
                    remain -= i
                if remain:
                    self.writeable.poll()
        return cmd_id

    def execute_command(self, command, args=None):
        op = {'command': command}
        if args is not None:
            op['args'] = args
        return self.write(op)
