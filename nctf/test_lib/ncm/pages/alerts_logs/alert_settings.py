import logging

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class AlertSettingsPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#logs/alert_settings"

    @property
    def alert_settings_panel(self):
        return AlertSettingsPanelRegion(parent=self)

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")


class AlertSettingsPanelRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH,
                    "//div[starts-with(@id, 'ecm-core-view-monitoring-Notifications') and contains(@class, 'x-panel ')]")

    @property
    def add_button(self):
        return pages.UIRegionLinkElement(
            'Add button',
            self,
            region_cls=AddAlertNotificationDialog,
            strategy=pages.By.XPATH,
            locator="//span[starts-with(@id, 'alert-notification-add-button')]",
            region_parent=self.page
        )


class AlertSettingsTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@class, 'x-panel')]"
                    "//div[contains(@id, 'ecm-core-view-monitoring-Notifications')"
                    " and contains(@class, 'x-grid-with-row-lines')]")


class AddAlertNotificationDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-core-view-monitoring-AlertNotifierDlg')]")

    @property
    def accounts_groups_table(self):
        return AccountsGroupsTable(self)

    @property
    def save_button(self):
        return pages.UIPageLinkElement(
            'Save button',
            parent=self,
            page_cls=AlertSettingsPage,
            frame=self.parent.frame,
            strategy=pages.By.EXTJS_ID,
            locator="okButton"
        )

    @property
    def alerts_grid(self):
        return pages.UIRegionLinkElement(
            'Alerts Grid',
            self,
            region_cls=AlertsDialog,
            strategy=pages.By.EXTJS_ID,
            locator="alertsGrid",
            region_parent=self.page
        )

    @property
    def users_grid(self):
        return pages.UIRegionLinkElement(
            'Users Grid',
            self,
            region_cls=UsersDialog,
            strategy=pages.By.EXTJS_ID,
            locator="usersGrid",
            region_parent=self.page
        )

    @property
    def interval_grid(self):
        return pages.UIRegionLinkElement(
            'Interval Grid',
            self,
            region_cls=IntervalDialog,
            strategy=pages.By.EXTJS_ID,
            locator="intervalGrid",
            region_parent=self.page
        )


class AlertsDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "alertsGrid")

    def alert_category(self, category_name):
        return pages.UIButtonElement(
            'Alert category',
            self,
            pages.By.XPATH,
            "//div[contains(@id, '{}') and contains(@class,'x-grid-group-hd-collapsible')]".format(category_name)

        )

    def sub_category_alert(self, alert_name):
        return pages.UIButtonElement(
            'Alert name',
            self,
            pages.By.XPATH,
            "//div[(text()='{}')]".format(alert_name)
        )


class UsersDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "usersGrid")

    @property
    def all_users_checkbox(self):
        return pages.UICheckboxElement(
            'All Users Checkbox',
            self,
            strategy=pages.By.XPATH,
            locator="//span[contains(text(),'Users:')]/../ancestor::div[contains(@class,'x-panel')] "
                    "//span[@class='x-column-header-text' and starts-with(@id,'gridcolumn')]",
            region_parent=self.page
        )


class IntervalDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "intervalGrid")




class AccountsGroupsTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-widget-AccountTreePicker')]")

    def group_checkbox(self, group_name):
        return pages.UICheckboxElement(
            'Group checkbox',
            self,
            strategy=pages.By.XPATH,
            locator="//span[text()='{}']".format(group_name),
            region_parent=self.page
        )
