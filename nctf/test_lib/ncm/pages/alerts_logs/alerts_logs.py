import logging

import nctf.test_lib.als.pages.activity_log as als
from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.alerts_logs.alert_settings import AlertSettingsPage
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class AlertLogTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@class, 'x-panel')]"
                    "//div[contains(@id, 'ecm-core-view-monitoring-Alertsv2')"
                    " and contains(@class, 'x-grid-with-row-lines')]")
    PAGING_TOOLBAR = (pages.By.XPATH, ".//div[starts-with(@id, 'alerts2-paging-toolbar')]")

    # TODO:: This is incomplete

    def refresh(self) -> 'AlertLogTable':
        return self.parent.toolbar.refresh_button.click().table

    class PagingToolbar(pages.ExtJSTable.PagingToolbar):
        def last_page(self) -> 'AlertLogTable':
            """There is no last_page button for this table"""
            return self.parent

        def goto_page(self, page_number: int) -> 'AlertLogTable':
            """There is no goto_page page number entry for this table."""
            return self.parent

    @property
    def paging_toolbar(self):
        return AlertLogTable.PagingToolbar(self)

    @property
    def total_pages(self) -> int:
        return 1


class Toolbar(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "div.x-toolbar")

    @property
    def refresh_button(self):
        return pages.UIRegionLinkElement(
            name='refresh_button',
            parent=self,
            region_cls=AlertLogPanelRegion,
            strategy=pages.By.ID,
            locator='alerts2-refresh-button',
            region_parent=self.parent.parent)

    @property
    def setup_alerts_button(self):
        return pages.UIPageLinkElement(
            name='setup alerts button',
            parent=self,
            page_cls=AlertSettingsPage,
            frame=self.parent.frame,
            strategy=pages.By.ID,
            locator='alerts2-settings-button'
        )

class AlertLogPanelRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH,
                    "//div[starts-with(@id, 'ecm-core-view-monitoring-Alertsv2') and contains(@class, 'x-panel ')]")

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.spinner.wait_for_spinner()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def toolbar(self):
        return Toolbar(self)

    @property
    def table(self) -> AlertLogTable:
        return AlertLogTable(self)


class AlertsAndLogsBasePage(NCMBasePage):
    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def alert_log_tab(self):
        return pages.UIPageLinkElement(
            'Alert Log',
            self,
            page_cls=AlertsAndLogsPage,  # the Alert Log Page is the AlertsAndLogsPage
            strategy=pages.By.ID,
            locator='nav-button-alert-log')

    @property
    def activity_log_tab(self):
        return pages.UIPageLinkElement(
            'Activity Log', self, page_cls=ActivityLogPage, strategy=pages.By.ID, locator='nav-button-activity-log')


class AlertsAndLogsPage(AlertsAndLogsBasePage):
    URL_TEMPLATE = "/ecm.html#logs/alert_log"

    @property
    def alert_log_panel(self) -> AlertLogPanelRegion:
        return AlertLogPanelRegion(parent=self)

    def alert_present(self):
      return self.alert_log_panel.table.num_rows > 0


class ActivityLogPage(AlertsAndLogsBasePage):
    URL_TEMPLATE = "/ecm.html#logs/activity_log"

    @property
    def activity_log_panel(self) -> als.ActivitiesLogPage:
        """The landing page after clicking the level 100 NETWORKS tab."""
        return als.ActivitiesLogPage(parent=self, frame=(pages.By.XPATH, '//iframe'))

