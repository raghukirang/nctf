from nctf.test_lib.base import pages
from nctf.test_lib.base.pages.by import By
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage
from nctf.test_lib.ncm.pages.devices.routers.routers import RoutersPage
from nctf.test_lib.ncm.pages.networks.networks import NetworksPage


class ApplicationsPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#applications"

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def netcloud_manager_region(self):
        return NetCloudManagerRegion(self)

    @property
    def netcloud_perimeter_region(self):
        return NetCloudPerimeterRegion(self)

    @property
    def netcloud_api_region(self):
        return NetCloudAPIRegion(self)

    @property
    def cp_secure_threat_management_region(self):
        return CPSecureThreatManagementRegion(self)

    @property
    def cvr_region(self):
        return CVRRegion(self)

    @property
    def deploy_cvr_region(self):
        return _DeployCVRRegion(self)


class NetCloudManagerRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, ".//div[contains(@class, 'Feature_StandardTier')]")

    @property
    def try_enterprise_button(self):
        return pages.UIRegionLinkElement(name="Try Enterprise button",
                                         parent=self,
                                         region_cls=self.TryEnterpriseDialog,
                                         strategy=By.ID,
                                         locator="app-tile-try-primestandard-tier-button-btnEl")

    @property
    def manage_button(self):
        return pages.UIPageLinkElement(name="Manage button",
                                       parent=self,
                                       page_cls=RoutersPage,
                                       strategy=By.ID,
                                       locator="app-tile-manage-standard-tier-button-btnEl")

    class TryEnterpriseDialog(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".x-window.x-layer")

        @property
        def try_netcloud_manager_enterprise_button(self):
            return pages.UIPageLinkElement(name="Try NetCloud Manager Enterprise button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.XPATH,
                                           locator=".//a[contains(@class, 'ECMTryWindow_Start')]")

        @property
        def no_thanks_button(self):
            return pages.UIPageLinkElement(name="No Thanks button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.XPATH,
                                           locator=".//a[contains(@class, 'ECMTryWindow_Cancel')]")


class NetCloudPerimeterRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, ".//div[contains(@class, 'Feature_NetcloudEngine')]")

    @property
    def enable_button(self):
        return pages.UIPageLinkElement(name="Enable button",
                                       parent=self,
                                       page_cls=ApplicationsPage,
                                       strategy=By.ID,
                                       locator="app-tile-enable-ncpnetcloud-engine-button-btnEl")

    @property
    def manage_button(self):
        return pages.UIPageLinkElement(name="Manage button",
                                       parent=self,
                                       page_cls=NetworksPage,
                                       strategy=By.ID,
                                       locator="app-tile-manage-netcloud-engine-button-btnEl")


class NetCloudAPIRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, ".//div[contains(@class, 'Feature_ApiManagement')]")

    @property
    def sign_up_button(self):
        return pages.UIPageLinkElement(name="Sign Up button",
                                       parent=self,
                                       page_cls=ApplicationsPage,
                                       strategy=By.ID,
                                       locator="app-tile-signup-api-management-button-btnEl")

    @property
    def manage_button(self):
        return pages.UIRegionLinkElement(name="Manage button",
                                         parent=self,
                                         region_cls=self.NetCloudAPIPanel,
                                         strategy=By.ID,
                                         locator="app-tile-manage-api-management-button-btnEl")

    class NetCloudAPIPanel(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".x-panel.x-fit-item")

        @property
        def back_button(self):
            return pages.UIPageLinkElement(name="Back button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.CLASS_NAME,
                                           locator="x-btn-icon-el")


class CPSecureThreatManagementRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, ".//div[contains(@class, 'Feature_Ips')]")

    @property
    def try_button(self):
        return pages.UIRegionLinkElement(name="Try button",
                                         parent=self,
                                         region_cls=self.CPSecureThreatManagementTOS,
                                         strategy=By.ID,
                                         locator="app-tile-try-ips-button-btnEl")

    @property
    def manage_button(self):
        return pages.UIRegionLinkElement(name="Manage button",
                                         parent=self,
                                         region_cls=self.CPSecureThreatManagementPanel,
                                         strategy=By.ID,
                                         locator="app-tile-manage-ips-button-btnEl")

    class CPSecureThreatManagementTOS(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".x-window.x-layer")

        @property
        def i_agree_button(self):
            return pages.UIPageLinkElement(name="I Agree button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.XPATH,
                                           locator=".//a[contains(@class, 'SystemMessgeDlg_Agree')]")

        @property
        def cancel_button(self):
            return pages.UIPageLinkElement(name="Cancel button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.XPATH,
                                           locator=".//a[contains(@class, 'SystemMessageDlg_Cancel')]")

    class CPSecureThreatManagementPanel(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".x-panel.x-fit-item")

        @property
        def back_button(self):
            return pages.UIPageLinkElement(name="Back button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.CLASS_NAME,
                                           locator="x-btn-icon-el")


# todo: Verify these buttons work when CVR tile is implemented
class CVRRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, ".//div[contains(@class, 'Feature_Activation')]")

    @property
    def cvr_management_panel(self):
        return CVRManagementPanel(self)

    @property
    def manage_button(self):
        return pages.UIRegionLinkElement(name="Manage button",
                                         parent=self,
                                         region_cls=CVRManagementPanel,
                                         strategy=By.ID,
                                         locator="app-tile-manage-activation-button")

    @property
    def buy_button(self):
        return pages.UIPageLinkElement(name="Buy button",
                                       parent=self,
                                       page_cls=ApplicationsPage,
                                       strategy=By.ID,
                                       locator="app-tile-buy-activation-button")

    @property
    def try_button(self):
        return pages.UIRegionLinkElement(name="Try button",
                                         parent=self,
                                         region_cls=self.TryCVRToS,
                                         strategy=By.ID,
                                         locator="app-tile-try-activation-button")

    class TryCVRToS(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, ".x-window.x-layer")

        @property
        def i_agree_button(self):
            return pages.UIPageLinkElement(name="I Agree button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.XPATH,
                                           locator=".//a[contains(@class, 'SystemMessgeDlg_Agree')]")

        @property
        def cancel_button(self):
            return pages.UIPageLinkElement(name="Cancel button",
                                           parent=self,
                                           page_cls=ApplicationsPage,
                                           strategy=By.XPATH,
                                           locator=".//a[contains(@class, 'SystemMessageDlg_Cancel')]")


class CVRManagementPanel(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, ".//div[contains(@class, 'FeatureDetail_Activation')]")

    @property
    def cvr_router_region(self):
        return self._CVRRouterRegion(self)

    class _CVRRouterRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.ID, "container-1132-innerCt")

        # @property
        # def add_button(self):
        #     return pages.UIRegionLinkElement(name="Add button",
        #                                      parent=self,
        #                                      region_cls=DeployCVRRegion,
        #                                      strategy=By.EXTJS_ID,
        #                                      locator="addEntitledDevice")

        @property
        def remove_button(self):
            return pages.UIButtonElement("remove button", self, By.ID, 'deleteEntitledDevice')

        @property
        def cvr_router_table(self):
            return self.CVRRouterTable(self)

        class CVRRouterTable(pages.UIRegion):
            ROOT_LOCATOR = (pages.By.XPATH, ".//div[contains(@id, 'ecm-cvrentitlementgrid')]")

            # @property
            # def delete_cvr_dialog(self):
            #     return self.DeleteCVRDialog(self)

            @property
            def add_button(self):
                return pages.UIButtonElement(
                    name='Add CVR button',
                    parent=self,
                    strategy=By.EXTJS_ID,
                    locator="addEntitledDevice")



            # @property
            # def remove_button(self):
            #     return pages.UIRegionLinkElement(
            #         'Add CVR button',
            #         self,
            #         region_cls=DeleteCVRDialog,
            #         strategy=By.EXTJS_ID,
            #         locator="deleteEntitledDevice",
            #         region_parent=self.page)


class _DeployCVRRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "ecm-core-view-apps-CvrDeployDlg")

    # @property
    # def next_button(self):
    #     return pages.UIRegionLinkElement(
    #         name='Next button',
    #         parent=self.page,
    #         region_cls=self,
    #         strategy=By.TAG_NAME,
    #         locator="Next",
    #         region_parent=self.page)

    @property
    def cancel_button(self):
        return pages.UIRegionLinkElement(
            'Next button',
            self,
            region_cls=_DeployCVRRegion,
            strategy=By.TAG_NAME,
            locator="Cancel",
            region_parent=self.page)
