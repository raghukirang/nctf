import time

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.scheduler import SchedulerBasePage


class SchedulesPage(SchedulerBasePage):
    URL_TEMPLATE = "/ecm.html#scheduler/schedules"

    @property
    def schedules_panel(self):
        return SchedulesPanel(self)

    def add_default_schedule(self, schedule_name: str) -> 'SchedulesPage':
        """Add a default schedule

        Args:
          schedule_name (str): the name of the schedule
        """
        add_form = self.schedules_panel.add_button.click()
        add_form.name_field.send_keys(schedule_name)
        return add_form.add_button.click()

    def delete_schedule(self, schedule_name: str) -> 'SchedulesPage':
        """Delete a schedule

        Args:
         schedule_name (str): the name of the schedule to delete
        """
        self.schedules_panel.schedules_table.row_name_field(schedule_name).click()
        delete_dialogue = self.schedules_panel.delete_button.click()
        #time.sleep(1)  # QAS-1172 There seems to be a race condition happening with the next click in the nightly run
        return delete_dialogue.ok_button.click()
        # delete_dialogue.spinner.wait_for_spinner()
        # self.schedules_panel.spinner.wait_for_spinner()
        #


class SchedulesPanel(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.ID, "schedules")

    @property
    def schedules_table(self):
        return SchedulesTable(self)

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def add_button(self):
        return pages.UIRegionLinkElement("schedules add button", self, AddScheduleRegion, pages.By.XPATH,
                                         ".//span[text()='Add']")

    @property
    def delete_button(self):
        return pages.UIRegionLinkElement("schedules delete button", self, DeleteScheduleRegion, pages.By.XPATH,
                                         ".//span[text()='Delete']")


class SchedulesTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-core-view-scheduler-ScheduleWidget')]")

    def row_name_field(self, schedule_name: str):
        """ Gets a Button Element representing the name field of a given row via the schedule name

        Args:
         schedule_name (str): the name of the schedule
        """
        return pages.UIButtonElement(
            "Schedules row name field", self, strategy=pages.By.XPATH, locator="//div[text()='{}']".format(schedule_name))


class AddScheduleRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-core-view-scheduler-AddEditScheduleDlg')]")

    def wait_for_region_to_load(self):
        self.spinner.wait_for_spinner()
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def name_field(self):
        return pages.UIElement('Name field', self, pages.By.XPATH, './/input')

    @property
    def add_button(self):
        xpath = ".//span[text()='Add']/../../ancestor::a[not(contains(@class, 'disabled'))]"
        return pages.UIPageLinkElement('Add button', self, SchedulesPage, strategy=pages.By.XPATH, locator=xpath)

    @property
    def cancel_button(self):
        xpath = ".//span[text()='Cancel']/../../ancestor::a[not(contains(@class, 'disabled'))]"
        return pages.UIPageLinkElement('Cancel button', self, SchedulesPage, strategy=pages.By.XPATH, locator=xpath)


class DeleteScheduleRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'messagebox') and contains(@class, 'x-message-box')]")

    def wait_for_region_to_load(self):
        self.ok_button.wait_for_clickable()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            "ok button", self, SchedulesPage, strategy=pages.By.XPATH, locator="//span[text()='OK']/..")

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            "ok button", self, SchedulesPage, strategy=pages.By.XPATH, locator="//span[text()='Cancel']/..")
