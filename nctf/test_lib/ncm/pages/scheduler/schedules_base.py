from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By
from nctf.test_lib.ncm.pages import scheduler as scheduler_pages
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class SchedulerBasePage(NCMBasePage):
    @property
    def tasks_page_button(self):
        return pages.UIPageLinkElement(
            'Tasks', self, page_cls=scheduler_pages.TasksPage, strategy=By.ID, locator='nav-button-tasks')

    @property
    def schedules_page_button(self):
        return pages.UIPageLinkElement(
            'Schedules', self, page_cls=scheduler_pages.SchedulesPage, strategy=By.ID, locator='nav-button-schedules')
