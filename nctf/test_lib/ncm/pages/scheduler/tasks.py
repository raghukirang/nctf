from nctf.test_lib.ncm.pages.scheduler import SchedulerBasePage


class TasksPage(SchedulerBasePage):
    URL_TEMPLATE = "/ecm.html#scheduler/tasks"
