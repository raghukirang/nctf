import logging

from selenium.webdriver import ActionChains

from nctf.test_lib.base import pages
from nctf.test_lib.accounts.pages import login

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class AccountDropdownRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "#menu-1010-innerCt")

    @property
    def profile_button(self):
        xpath = ".//span[text()='Profile']/.."
        return pages.UIRegionLinkElement(
            name='Profile Button',
            parent=self.page,
            region_cls=NCMProfileDialog,
            region_parent=self.page,
            strategy=pages.By.XPATH,
            locator=xpath)

    @property
    def settings_button(self):
        xpath = ".//a/span[text()='Settings']"
        return pages.UIRegionLinkElement(
            name='Settings Button',
            parent=self.page,
            region_cls=UserSettingsDialog,
            region_parent=self.page,
            strategy=pages.By.XPATH,
            locator=xpath
        )

    @property
    def logout_button(self):
        xpath = ".//span[text()='Log Out']/.."
        return pages.UIPageLinkElement(
            name='Logout Button',
            parent=self,
            page_cls=login.LoginRedirectErrorPage,
            strategy=pages.By.XPATH,
            locator=xpath)


class ProfileDialog(pages.UIPage):
    """The prompt displayed after clicking the Profile button on NCMBasePage."""
    URL_TEMPLATE = '/static/dist/index.html#/profile'

    @property
    def firstname_field(self):
        return pages.UIElement('First Name field', self, pages.By.ID, 'firstName')

    @property
    def lastname_field(self):
        return pages.UIElement('Last Name field', self, pages.By.ID, 'lastName')

    @property
    def email_field(self):
        return pages.UIElement('Email field', self, pages.By.ID, 'email')

    @property
    def change_password_button(self):
        return pages.UIRegionLinkElement(name="Change Password Button",
                                         parent=self,
                                         region_cls=NCMChangePasswordDialog,
                                         region_parent=self.page,
                                         strategy=pages.By.ID,
                                         locator="changePasswordButton")

    @property
    def set_up_mfa_button(self):
        return pages.UIRegionLinkElement(name="Set Up MFA Device Button",
                                         parent=self,
                                         region_cls=NCMMFASetupDialog,
                                         region_parent=self.page,
                                         strategy=pages.By.ID,
                                         locator="mfaButton")

    @property
    def save_button(self):
        return pages.UIPageLinkElement(name="Save Button",
                                       parent=self,
                                       page_cls=type(self.page),
                                       strategy=pages.By.CLASS_NAME,
                                       locator="cp-save-btn")

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(name="Cancel Button",
                                       parent=self,
                                       page_cls=type(self.page),
                                       strategy=pages.By.CLASS_NAME,
                                       locator="cp-cancel-btn")


class NCMProfileDialog(pages.UIRegion, ProfileDialog):
    ROOT_LOCATOR = (pages.By.XPATH, "/html")  # (pages.By.XPATH, '//div[starts-with(@id, "ecm-core-view-Frame")]')
    FRAME_LOCATOR = (pages.By.XPATH, '//iframe[starts-with(@data-selector, "user_profile")]')

    def wait_for_region_to_load(self):
        self.parent.page.wait_for_page_to_load()
        return self


class ChangePasswordDialog(pages.UIPage):
    """The prompt displayed after clicking the Change Password button on ProfileDialog."""
    URL_TEMPLATE = '/static/dist/index.html#/changepassword'

    @property
    def current_password_field(self):
        return pages.UIElement('Change Password field', self, pages.By.ID, 'current-password')

    @property
    def new_password_field(self):
        return pages.UIElement('Password field', self, pages.By.ID, 'new-password')

    @property
    def confirm_password_field(self):
        return pages.UIElement('Password (confirm) field', self, pages.By.ID, 'confirm-password')

    @property
    def ok_button(self):
        return pages.UIRegionLinkElement(name="OK Button",
                                         parent=self,
                                         region_cls=type(self),
                                         region_parent=self.page,
                                         strategy=pages.By.ID,
                                         locator="submitButton")


class NCMChangePasswordDialog(pages.UIRegion, ChangePasswordDialog):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@class, 'cp-change-password-dialog')]")
    FRAME_LOCATOR = (pages.By.XPATH, '//iframe[starts-with(@data-selector, "undefined")]')

    def wait_for_region_to_load(self):
        self.parent.page.wait_for_page_to_load()
        return self


class MFASetupDialog(pages.UIPage):
    """The prompt displayed after clicking the Set Up MFA Device button on ProfileDialog."""
    URL_TEMPLATE = '/static/dist/index.html#/mfa'

    @property
    def mfa_manual_config_option(self) -> str:
        """'MFA configuration option which contains the configuration key."""
        element = pages.UIElement('MFA Configuration Key', self, pages.By.ID, 'mfaOption2')
        return element.get_text()[-32:]

    @property
    def authentication_code_field(self):
        return pages.UIElement('Authentication Code field', self, pages.By.ID, 'authCode')

    @property
    def finish_button(self):
        return pages.UIRegionLinkElement(name="Finish Button",
                                         parent=self,
                                         region_cls=type(self),
                                         region_parent=self.page,
                                         strategy=pages.By.ID,
                                         locator="submitButton")


class NCMMFASetupDialog(pages.UIRegion, MFASetupDialog):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@class, 'cp-mfa-dialog')]")
    FRAME_LOCATOR = (pages.By.XPATH, '//iframe[starts-with(@data-selector, "undefined")]')

    def wait_for_region_to_load(self):
        self.parent.page.wait_for_page_to_load()
        return self


class UserSettingsDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "/html")

    @property
    def user_settings(self):
        return self._UserSettingsRegion(self)

    class _UserSettingsRegion(pages.UIRegion):
        xpath = ".//div[contains(@id, 'UserSettingsWindow')]"
        ROOT_LOCATOR = (pages.By.XPATH, xpath)

        def ok_button(self):
            return self.driver.find_element_by_xpath(".//a[contains(@class, 'OkButton')]")

        def cancel_button(self):
            return self.driver.find_element_by_xpath(".//a[contains(@class, 'CancelButton')]")

        def _get_items_per_page(self) -> int:
            # cannot find a unique way to grab this value, it matches four others
            # so I'm just grabbing all of them and returning the one we need
            xpath = ".//div[starts-with(@id, 'displayfield')]"
            return int(self.driver.find_elements_by_xpath(xpath)[-1].text)

        def _move_slider(self, direction):
            element = self.driver.find_elements_by_xpath(f"{self.xpath}//div[contains(@id, 'thumb-0')]")
            move = ActionChains(self.driver)
            if direction.lower() == "left":
                move.click_and_hold(element[1]).move_by_offset(-15, 0).release().perform()
            elif direction.lower() == "right":
                move.click_and_hold(element[1]).move_by_offset(15, 0).release().perform()
            else:
                raise ValueError(f"'{direction}' is not a valid direction. Must be 'left' or 'right'.")

        def set_items_per_page(self, items_per_page: int):
            if 5 <= items_per_page <= 100:
                if items_per_page % 5 != 0:
                    raise ValueError("Must choose a number in increments of 5. (5, 10, 15, etc)")
            else:
                raise ValueError(f"'{items_per_page}' is out of the valid range of numbers. "
                                 f"Must be greater or equal to 5 and less than or equal to 100.")

            current_items_per_page = self._get_items_per_page()
            if current_items_per_page != items_per_page:
                while True:
                    current_items_per_page = self._get_items_per_page()
                    if current_items_per_page == items_per_page:
                        self.ok_button().click()
                        break
                    elif current_items_per_page < items_per_page:
                        self._move_slider("right")
                    else:
                        self._move_slider("left")
            else:
                self.cancel_button().click()
