import logging

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.common.account_dropdown import AccountDropdownRegion
from nctf.test_lib.ncm.pages.common.side_nav_bar import SideNavBar

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class NCMBasePage(pages.UIPage):

    WEB_SERVICE = 'ncm'

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def side_nav_bar(self):
        # self.wait_for_page_to_load()
        return SideNavBar(
            self, strategy=pages.By.XPATH, locator="//*[contains(@class, 'x-panel x-border-item x-box-item x-panel-navbar')]")

    def nav_bar_buttons(self) -> list:
        """All buttons contained in the level 200 navigation bar.

        Returns:
            List of `WebElement`. Each element is a level 200 nav button
        """
        selector = "div.app-nav-button"
        return self.find_elements(pages.By.CSS_SELECTOR, selector)

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def account_dropdown(self):
        """The account dropdown that accesses Profile, Settings, Switch Accounts, and Log Out"""

        return pages.UIRegionLinkElement(
            'Account dropdown',
            self,
            region_cls=AccountDropdownRegion,
            strategy=pages.By.CLASS_NAME,
            locator='header-menu-trigger')
