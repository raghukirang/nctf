import logging

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.ncm import pages as ncm_pages
from selenium.webdriver.common.by import By

logger = logging.getLogger('ecm.pages.common.side_nav')


class SideNavBarButton(base_pages.UIPageLinkElement):
    # TODO:: Implement to only return self when clicking the side nav bar button corresponding to the current page
    pass


class SideNavBar(base_pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//*[contains(@class, 'x-panel x-border-item x-box-item x-panel-navbar')]")

    @property
    def dashboard_button(self):
        return base_pages.UIPageLinkElement(
            'Dashboard Button', self, ncm_pages.DashboardPage, strategy=By.ID, locator='app-dashboard-button')

    @property
    def devices_button(self):
        return base_pages.UIPageLinkElement(
            'Devices Button', self, ncm_pages.RoutersPage, strategy=By.ID, locator='app-devices-button')

    @property
    def groups_button(self):
        return base_pages.UIPageLinkElement(
            'Groups Button', self, ncm_pages.GroupsPage, strategy=By.ID, locator='app-groups-button')

    @property
    def ncp_button(self):
        return base_pages.UIPageLinkElement(
            'Netcloud Engine Button', self, ncm_pages.NCPPage, strategy=By.ID, locator='app-nce-button')

    @property
    def networks_button(self):
        return base_pages.UIPageLinkElement(
            'Networks Button', self, ncm_pages.NetworksPage, strategy=By.ID, locator='app-nce-button', path='vpn')

    @property
    def alerts_and_logs_button(self):
        return base_pages.UIPageLinkElement(
            'Alerts & Logs Button', self, ncm_pages.AlertsAndLogsPage, strategy=By.ID, locator='app-logs-button')

    @property
    def reports_button(self):
        return base_pages.UIPageLinkElement(
            'Reports Button', self, ncm_pages.ReportsPage, strategy=By.ID, locator='app-reports-button')

    @property
    def scheduler_button(self):
        return base_pages.UIPageLinkElement(
            'Scheduler Button', self, ncm_pages.TasksPage, strategy=By.ID, locator='app-scheduler-button')

    @property
    def tools_button(self):
        return base_pages.UIPageLinkElement(
            'Tools Button', self, ncm_pages.ToolsPage, strategy=By.ID, locator='app-tools-button')

    @property
    def applications_button(self):
        return base_pages.UIPageLinkElement(
            'Applications Button', self, ncm_pages.ApplicationsPage, strategy=By.ID, locator='app-applications-button')

    @property
    def accounts_and_users_button(self):
        return base_pages.UIPageLinkElement(
            'Account and Users Button', self, ncm_pages.AccountsAndUsersPage, strategy=By.ID, locator='app-account-button')

    @property
    def marketplace_button(self):
        return base_pages.UIPageLinkElement(
            'Marketplace Button', self, ncm_pages.MarketplacePage, strategy=By.ID, locator='app-marketplace-button')

    def wait_for_region_to_load(self):
        self.dashboard_button.wait_for_clickable(timeout=30)
