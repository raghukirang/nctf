import logging

from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By
from nctf.test_lib.ncm import pages as ncm_pages
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage
from nctf.test_lib.ncm.pages.devices.routers.remote_ui import DevicesPage
from nctf.test_lib.ncm.pages.devices.routers.remote_ui.local_ip_networks import LocalIPNetworksPage

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


def switch_to_networks_frame(driver):  # TODO: Take this out when iframe stuff is better integrated
    driver.switch_to.default_content()
    frame = driver.find_element_by_xpath("//iframe[contains(@src, 'static/config_pages')]")
    driver.switch_to_frame(frame)


class EditConfigurationDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.CSS_SELECTOR, 'div.x-window[id^="ecm-core-view-S3ConfigPagesWindow"]')

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def wait_for_region_to_load(self):
        self.spinner.wait_for_spinner()
        self.view_pending_changes_button.wait_for_clickable()
        return self

    @property
    def view_pending_changes_button(self):
        """
        The View Pending Changes button on the edit configuration dialog
        """
        return pages.UIRegionLinkElement(
            'view pending changes button',
            self,
            region_cls=ViewPendingChangesDialog,
            strategy=By.EXTJS_ID,
            locator='viewPending')

    @property
    def commit_changes_button(self):
        return pages.UIRegionLinkElement(
            'view pending changes button',
            self,
            region_cls=CommitPendingChangesDialog,
            strategy=By.EXTJS_ID,
            locator='commitChanges')

    @property
    def discard_changes_button(self):
        return pages.UIRegionLinkElement(
            "Configuration Editor Discard button",
            self,
            region_cls=ConfirmDiscardChangesDialog,
            strategy=pages.By.EXTJS_ID,
            locator="discardChanges",
            region_parent=self.page)

    @property
    @property
    def disgard_changes_button(self):
        return pages.UIPageLinkElement(
            'view pending changes button', self, page_cls=ncm_pages.RoutersPage, strategy=By.EXTJS_ID, locator='commitChanges')

    @property
    def config_body(self) -> DevicesPage:
        return DevicesPage(parent=self.parent.page, frame=(pages.By.ID, 'bsd_device_iframe')).wait_for_page_to_load()


class ViewPendingChangesDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.CSS_SELECTOR, 'div.x-window[id^="ecm-core-view-ConfigSummaryWindow"]')

    @property
    def spinner(self):
        return pages.UISpinnerElement(self.page, strategy=By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def wait_for_region_to_load(self):
        self.spinner.wait_for_spinner()
        return self

    @property
    def pending_changes(self):
        return pages.UIElement('pending changes', self, strategy=By.XPATH, locator=".//div[contains(@id,'configpanel-')]/pre")

    @property
    def close_button(self):
        xpath = ".//span[text()='View Pending Changes']/../..//img[contains(@class,('x-tool-close'))]"
        return pages.UIRegionLinkElement(
            'close button', self, region_cls=EditConfigurationDialog, strategy=By.XPATH, locator=xpath)


class ConfirmDiscardChangesDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.CSS_SELECTOR, 'div[id^="messagebox-"')

    @property
    def ok_button(self):
        return pages.UIPageLinkElement('OK Button', self, page_cls=ncm_pages.GroupsPage, strategy=By.EXTJS_ID, locator="ok")


class CommitPendingChangesDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.CSS_SELECTOR, 'div[id^="messagebox-"')

    @property
    def ok_button(self):
        return pages.UIButtonElement("ok button", self, By.EXTJS_ID, 'ok')
