
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class ReportsPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#reports"
