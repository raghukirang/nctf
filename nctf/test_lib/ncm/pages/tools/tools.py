import time

from nctf.test_lib.base import pages
from nctf.test_lib.base.pages.by import By
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage
from nctf.test_lib.utils import waiter


class ToolsPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#tools/router_apps"

    @property
    def router_apps_region(self):
        return RouterAppsRegion(self)

    @property
    def opt_in_button(self):
        return OptInButtonRegion(self)

    @property
    def dialog_ok_button(self):
        return pages.UIButtonElement("ok button", self, By.EXTJS_ID, 'ok')

    @property
    def router_apps_refresh_button(self):
        return pages.UIButtonElement("refresh button", self, By.CLASS_NAME, 'RefreshButton')

    def are_all_apps_loaded(self) -> bool:
        self.router_apps_refresh_button.click()
        rows = self.router_apps_region.available_apps_table.get_rows()
        all_loaded = False
        for row in rows:
            all_loaded = row.cells['Status'].text == 'Ready'
            if not all_loaded:
                return False
        return all_loaded

    def wait_for_apps_to_load(self, timeout: float = 30, poll_interval: float = 0.1) -> 'AvailableAppsTable':
        waiter.wait(timeout, poll_interval).for_call(self.are_all_apps_loaded).to_be(True)
        return self.router_apps_region.available_apps_table

    def opt_in(self):
        if not self.opt_in_button.is_checked():
            self.opt_in_button.click()
            self.dialog_ok_button.click()
            self.spinner.wait_for_spinner()
            self.router_apps_region.dev_mode_routers_add_button.wait_for_loaded(timeout=30)
        return self

    def opt_out(self):
        if self.opt_in_button.is_checked():
            self.opt_in_button.click()
            self.dialog_ok_button.click()
        return self

    def purge_apps(self):
        if self.router_apps_region.available_apps_table.num_rows > 0:
            self.router_apps_region.available_apps_table.all_apps_checkbox.click()
            self.router_apps_region.available_apps_delete_button.click().ok_button.click()

    def purge_apps_with_retry(self):
        # If the app was deployed until very recently, then we must wait for the
        # removal request to propagate through the celery queue
        timeout = 60
        start_time = time.time()
        while self.router_apps_region.available_apps_table.num_rows > 0 and (time.time() - start_time) < timeout:
            if self.router_apps_region.available_apps_table.num_rows > 0:
                self.purge_apps()

        assert self.router_apps_region.available_apps_table.num_rows == 0, "Failed to remove all apps from ECM"
        return self


class OptInButtonRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.ID, "routerAppsCheckbox")

    def click(self):
        return pages.UIButtonElement("opt in button", self, By.ID, 'routerAppsCheckbox-inputEl').click()

    def is_checked(self):
        return pages.ExtCheckboxElement("opt in checkbox", self, By.ID, "routerAppsCheckbox").is_checked()


class AvailableAppsTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "routerAppGrid")
    # TABLE_CONTENT = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-core-view-tools-RouterAppsWidget')"
    #                  "and contains(@id, 'body')]")

    @property
    def all_apps_checkbox(self):
        return pages.UIButtonElement("All Apps checkbox", self, By.CLASS_NAME, "x-column-header-checkbox")

    @property
    def status_error_link(self):
        return pages.UIRegionLinkElement(
            'Error - Click for details link',
            self,
            region_cls=AppValidationErrorDialog,
            strategy=By.XPATH,
            locator="//span[@data-qtip='Error - Click for details']",
            region_parent=self.page)


class DevModeRoutersTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "entitledGrid")
    # TABLE_CONTENT = (pages.By.XPATH,
    #                  ".//table[@role='presentation' and contains(@class, 'x-grid-table') and contains(@id, 'table')]")

    @property
    def all_dev_mode_routers_checkbox(self):
        return pages.ExtCheckboxElement("All Dev Mode Routers checkbox", self, By.XPATH, "//div[starts-with(@id,"
                                        "'routersresourcelist')]//div[contains"
                                        "(@class,'x-column-header-checkbox')]//span")


class RouterAppsRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.EXTJS_ID, "routerApps")
    DEFAULT_TIMEOUT = 30

    @property
    def available_apps_table(self):
        return AvailableAppsTable(self)

    @property
    def dev_mode_router_table(self):
        return DevModeRoutersTable(self)

    @property
    def add_devices_popup(self):
        return AddDevicesConfirmationDialog(self)

    @property
    def available_apps_add_button(self):
        return pages.UIRegionLinkElement(
            'Available Apps add button',
            self,
            region_cls=AddAppDialog,
            strategy=By.EXTJS_ID,
            locator="addRouterAppButton",
            region_parent=self.page)

    @property
    def available_apps_delete_button(self):
        return pages.UIRegionLinkElement(
            'Available Apps delete button',
            self,
            region_cls=DeleteAppsDialog,
            strategy=By.EXTJS_ID,
            locator="deleteRouterAppButton",
            region_parent=self.page)

    @property
    def dev_mode_routers_add_button(self):
        return pages.UIRegionLinkElement(
            'Dev mode add button',
            self,
            region_cls=RouterPickerDialog,
            strategy=By.EXTJS_ID,
            locator="addEntitledDevice",
            region_parent=self.page)

    @property
    def dev_mode_routers_remove_button(self):
        return pages.UIButtonElement("remove button", self, By.EXTJS_ID, 'deleteEntitledDevice')


class AddAppDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.ID, "uploadAppWindow")


class AddDevicesConfirmationDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//div[starts-with(@id, 'messagebox') and contains(@id, 'toolbar')]")

    @property
    def yes_button(self):
        return pages.UIPageLinkElement(
            name='Yes button',
            parent=self,
            page_cls=ToolsPage,
            frame=self.frame,
            strategy=By.EXTJS_ID,
            locator="yes",
            tenant_id='{tenant_id}')

    @property
    def no_button(self):
        return pages.UIPageLinkElement(
            name='No button',
            parent=self,
            page_cls=ToolsPage,
            frame=self.frame,
            strategy=By.EXTJS_ID,
            locator="no",
            tenant_id='{tenant_id}')


class RouterPickerDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH,
                    "//div[starts-with(@id, 'routerpickerwindow') and contains(@class, 'x-window-default-resizable')]")

    class DevicesTable(pages.ExtJSTable):
        ROOT_LOCATOR = (pages.By.EXTJS_ID, 'routerPicker')
        TABLE_CONTENT = (pages.By.CLASS_NAME, 'resource-list-body')

        def device_selection_checkbox(self, device_id: str):
            """ Selection checkbox for a specified device id

            Args:
                device_id: The id of the device in ncm to get the checkbox for
            """
            css = '[data-recordid="{}"] > td > div > div.x-grid-row-checker'.format(device_id)
            return pages.UIButtonElement("device selection checkbox", self, strategy=pages.By.CSS_SELECTOR, locator=css)

        def is_device_selection_checkbox_checked(self, device_id) -> bool:
            # get the table row <tr>
            cb_element = self.device_selection_checkbox(device_id)
            table_row = cb_element._element.find_element_by_xpath("../../..")

            # the class attribute will reflect the state of the checkbox
            if 'x-grid-row-selected' in table_row.get_attribute('class'):
                return True
            else:
                return False

    @property
    def maximize_button(self):
        return pages.UIRegionLinkElement(
            'Maximize Button',
            self,
            region_cls=RouterPickerDialog,
            strategy=By.CLASS_NAME,
            locator="x-tool-maximize",
            region_parent=self.page)

    @property
    def restore_button(self):
        return pages.UIRegionLinkElement(
            'Restore Button',
            self,
            region_cls=RouterPickerDialog,
            strategy=By.CLASS_NAME,
            locator="x-tool-restore",
            region_parent=self.page)

    @property
    def close_button(self):
        return pages.UIPageLinkElement(
            name='Close Button',
            parent=self,
            page_cls=ToolsPage,
            frame=self.frame,
            strategy=By.CLASS_NAME,
            locator="x-tool-close",
            tenant_id='{tenant_id}')

    @property
    def devices_table(self) -> 'RouterPickerDialog.DevicesTable':
        return RouterPickerDialog.DevicesTable(self)

    @property
    def save_button(self):
        return pages.UIRegionLinkElement(
            'Save button',
            self,
            region_cls=AddDevicesConfirmationDialog,
            strategy=By.EXTJS_ID,
            locator="saveButton",
            region_parent=self.page)

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            name="Cancel button",
            parent=self,
            page_cls=ToolsPage,
            frame=self.frame,
            strategy=By.EXTJS_ID,
            locator="cancelButton",
            tenant_id='{tenant_id}')


class DeleteAppsDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//span[text() = 'Delete NCOS Application']/"
                    "ancestor::div[starts-with(@id, 'messagebox') and contains(@class, 'x-message-box')]")

    @property
    def ok_button(self):
        return pages.UIButtonElement("ok button", self, By.EXTJS_ID, 'ok')

    @property
    def cancel_button(self):
        return pages.UIButtonElement("cancel button", self, By.EXTJS_ID, 'cancel')


class AppValidationErrorDialog(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//span[text() = 'App Validation Error']/"
                    "ancestor::div[starts-with(@id, 'messagebox') and contains(@class, 'x-message-box')]")

    def error_message_present(self, expected_error):
        return pages.UIElement("error message", self, By.XPATH, "//div[contains(text(), '{}')]".format(expected_error))

    @property
    def ok_button(self):
        return pages.UIButtonElement("ok button", self, By.EXTJS_ID, 'ok')
