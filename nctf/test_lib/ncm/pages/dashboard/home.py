from .common import DashboardPage


class HomePage(DashboardPage):
    URL_TEMPLATE = 'ecm.html' 

    def wait_for_page_to_load(self):
        return super().wait_for_page_to_load()

    def do_stuff(self):
        pass


class NoPermissionsHomePage(DashboardPage):
    URL_TEMPLATE = 'ecm.html#noPermission'

