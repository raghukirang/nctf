from .common import DashboardPage


class ModemUsagePage(DashboardPage):
    URL_TEMPLATE = '/ecm.html#dashboard/carrier'

    def wait_for_page_to_load(self):
        return super().wait_for_page_to_load()

    def do_stuff(self):
        pass
