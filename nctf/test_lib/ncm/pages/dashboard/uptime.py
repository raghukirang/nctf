from .common import DashboardPage


class UptimePage(DashboardPage):
    URL_TEMPLATE = '/ecm.html#dashboard/wan'

    def wait_for_page_to_load(self):
        return super().wait_for_page_to_load()

    def do_stuff(self):
        pass
