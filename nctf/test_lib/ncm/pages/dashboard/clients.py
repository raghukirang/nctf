from .common import DashboardPage


class ClientsPage(DashboardPage):
    URL_TEMPLATE = '/ecm.html#dashboard/clientanalytics'

    def wait_for_page_to_load(self):
        return super().wait_for_page_to_load()

    def do_stuff(self):
        pass
