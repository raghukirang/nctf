from nctf.test_lib.base import pages
from .common import DashboardPage


class GeoViewPage(DashboardPage):
    URL_TEMPLATE = '/ecm.html#dashboard/geoview'
    DEFAULT_TIMEOUT = 30

    NETCLOUD_CLIENTS_TEXT = 'NetCloud Perimeter Clients'

    def wait_for_page_to_load(self):
        return super().wait_for_page_to_load()

    def do_stuff(self):
        pass

    @property
    def netcloud_perimeter_clients_checkbox(self):
        xpath = f'//label[contains(text(),' \
            f' \'{self.NETCLOUD_CLIENTS_TEXT}\')]/preceding-sibling::*'
        return pages.UIButtonElement(self.NETCLOUD_CLIENTS_TEXT, self,
                                     strategy=pages.By.XPATH,
                                     locator=xpath)