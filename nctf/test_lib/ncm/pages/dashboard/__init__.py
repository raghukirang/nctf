from .clients import ClientsPage
from .geoview import GeoViewPage
from .home import HomePage, NoPermissionsHomePage
from .modem_usage import ModemUsagePage
from .traffic import TrafficPage
from .uptime import UptimePage
