from typing import cast

from selenium.webdriver.common.by import By

# import the package that contains these common pieces and then call out the actual class dependencies in the code
from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages import dashboard
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class DashboardPage(NCMBasePage):
    URL_TEMPLATE = 'ecm.html#dashboard/account'
    @property
    def dashboard_nav_bar(self):
        return DashboardNavBar(
            self,
            strategy=By.XPATH,
            locator=
            "//*[contains(@class, 'x-container x-border-item x-box-item x-container-view-filter x-box-layout-ct')]")


class DashboardNavBar(pages.UIRegion):

    ROOT_LOCATOR = (
        By.XPATH,
        "//*[contains(@class, 'x-container x-border-item x-box-item x-container-view-filter x-box-layout-ct')]")

    @property
    def home_tab(self):
        return pages.UIPageLinkElement(
            'home_nav_tab', self, DashboardPage, strategy=By.ID, locator='nav-button-account')

    @property
    def geoview_tab(self):
        return pages.UIPageLinkElement(
            'geoview_tab', self, dashboard.GeoViewPage, strategy=By.ID, locator='nav-button-geoview')

    @property
    def uptime_tab(self):
        return pages.UIPageLinkElement(
            'uptime_tab', self, dashboard.UptimePage, strategy=By.ID, locator='nav-button-wan')

    @property
    def modem_usage_tab(self):
        return pages.UIPageLinkElement(
            'modem_usage_tab', self, dashboard.ModemUsagePage, strategy=By.ID, locator='nav-button-carrier')

    @property
    def clients_tab(self):
        return pages.UIPageLinkElement(
            'clients_tab', self, dashboard.ClientsPage, strategy=By.ID, locator='nav-button-clientanalytics')

    @property
    def traffic_tab(self):
        return pages.UIPageLinkElement(
            'traffic_tab', self, dashboard.TrafficPage, strategy=By.ID, locator='nav-button-appanalytics')
