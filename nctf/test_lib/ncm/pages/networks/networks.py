import logging
from typing import cast

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages import ncp
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage
import nctf.test_lib.sockeye.pages.networks.vpn as vpn

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class NetworksPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#networks/{path}"
    DEFAULT_TIMEOUT = 30

    @property
    def overlay_home(self) -> vpn.NetworksHomePage:
        """The landing page after clicking the level 100 NETWORKS tab."""
        return cast(vpn.NetworksHomePage,
                    vpn.NetworksHomePage(parent=self, frame=(pages.By.XPATH, '//iframe[starts-with(@data-selector, "vpn")]')).\
            wait_for_page_to_load())

    @property
    def overlay_add_vpn(self) -> vpn.NetworksAddNetworkPage:
        """The add VPN Network page when +Add is clicked on the overlay_home page."""
        return cast( vpn.NetworksAddNetworkPage,
                     vpn.NetworksAddNetworkPage(parent=self,
                                                frame=(pages.By.XPATH, '//iframe[starts-with(@data-selector, "vpn")]')).\
                     wait_for_page_to_load())

    @property
    def ncp_page_button(self):
        return pages.UIPageLinkElement('NetCloud Perimeter Button', self, page_cls=ncp.NCPPage,
                                       strategy=pages.By.ID, locator='nav-button-ncp')