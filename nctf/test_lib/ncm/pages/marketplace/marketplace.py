from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage
from nctf.test_lib.testshop.testshop_base import TestshopBasePage


class MarketplacePage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#marketplace"

    @property
    def go_to_marketplace_button(self):
        return pages.UIPageLinkElement(
            'Go To Marketplace button',
            parent=self,
            page_cls=TestshopBasePage,
            strategy=pages.By.ID,
            locator="marketPlaceCartButton",
            opens_new_window=True)
