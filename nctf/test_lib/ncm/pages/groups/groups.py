import logging
import typing

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.common.edit_configuration_dialog import EditConfigurationDialog
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage

logger = logging.getLogger('groups')


class GroupsPanelRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "div.x-grid-with-row-lines[id^=groupsgrid]")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner()
        self.add_button.wait_for_clickable()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def toggle_treeview_button(self):
        return pages.UIButtonElement('toggle_treeview_button', self.page, pages.By.XPATH,
                                     '//a[contains(@data-qtip, "Toggle Tree")]')

    @property
    def add_button(self):
        return pages.UIButtonElement('add_button', self.page, pages.By.ID, 'groups-add-button-btnInnerEl')

    @property
    def delete_button(self):
        return pages.UIRegionLinkElement(
            "delete group button",
            self,
            region_cls=DeleteGroupConfirmation,
            strategy=pages.By.EXTJS_ID,
            locator="deleteButton",
            region_parent=self.page)

    @property
    def config_dropdown(self):
        return pages.UIRegionLinkElement(
            "configuration dropdown",
            self,
            region_cls=self.ConfigDropDownRegion,
            strategy=pages.By.EXTJS_ID,
            locator='groups-configuration-button',
            region_parent=self.page)

    class ConfigDropDownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "body")

        @property
        def edit_button(self):
            return pages.UIRegionLinkElement(
                "Configuration Editor Sidebar",
                self,
                region_cls=EditConfigurationDialog,
                strategy=pages.By.EXTJS_ID,
                locator="configEdit",
                region_parent=self.page)

    @property
    def commands_dropdown(self):
        return pages.UIRegionLinkElement(
            "commands dropdown",
            self,
            region_cls=self.CommandsDropDownRegion,
            strategy=pages.By.EXTJS_ID,
            locator='groups-commands-button',
            region_parent=self.page)

    class CommandsDropDownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "body")

        @property
        def manage_ncos_apps_item(self):
            return pages.UIRegionLinkElement(
                'Manage NCOS Apps',
                self,
                region_cls=ManageNCOSAppsDialog,
                strategy=pages.By.EXTJS_ID,
                locator="commandRouterApps",
                region_parent=self.page)

    @property
    def refresh_button(self):
        return pages.UIButtonElement('refresh_button', self, pages.By.ID, 'groups-refresh-button-btnIconEl')

    @property
    def add_group_dialog(self):
        self.add_button.click()
        return AddGroupDialogRegion(self)

    @property
    def groups_table(self):
        return GroupsTable(self)

    @property
    def groups_pagination(self):
        return GroupsPaginationRegion(self)  # TODO: This should be a generic class.

    @property
    def groups_grid_refresh_button(self):
        return pages.UIButtonElement('groups refresh button', self, pages.By.EXTJS_ID, 'groupsResourceListRefresh')


class GroupsTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'groupsgrid') " "and contains(@class, 'x-grid-with-row-lines')]")

    # Following row redefinitions are for ease of use, the record_id attribute is defined to be the NCM group_id
    class Row(pages.ExtJSTable.Row):
        @property
        def group_id(self) -> str:
            return self.record_id

    def get_rows(self, key: str = None, value: str = None, regex: str = None) -> typing.List['GroupsTable.Row']:
        return super(GroupsTable, self).get_rows(key, value, regex)

    def refresh(self) -> 'GroupsTable':
        return self.parent.toolbar.refresh_button.click()

    def select_group(self, group_name: str) -> None:
        """ Select the group row by name

        Args:
            group_name: The name of the group whose row will be selected in the table.
        """
        rows = self.get_rows("Name", group_name)
        if len(rows) == 0:
            raise ValueError("Not found in table group name = {}".format(group_name))
        elif len(rows) > 1:
            logger.warning("{} Found more than one group named = {}".format(self, group_name))
        rows[0].select()

    def is_group_selected(self, group_name: str) -> bool:
        """ Return whether the row with the group name given is selected.

        Args:
            group_name: The name of the group whose row will be selected in the table.
        """
        rows = self.get_rows("Name", group_name)
        if len(rows) == 0:
            raise ValueError("Not found in table group name = {}".format(group_name))
        elif len(rows) > 1:
            logger.warning("{} Found more than one group named = {}".format(self, group_name))
        return rows[0].is_selected()


class GroupsPaginationRegion(pages.UIRegion):
    @property
    def first_page_button(self):
        raise NotImplementedError()

    @property
    def prev_page_button(self):
        raise NotImplementedError()

    @property
    def next_page_button(self):
        raise NotImplementedError()

    @property
    def last_page_button(self):
        raise NotImplementedError()


class AddGroupDialogRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id,'ecm-core-view-GroupEditWindow') and contains(@class, 'x-layer')]")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.firmware_selector.wait_for_present()
        self.ok_button.wait_for_present()

    @property
    def group_name_field(self):
        return pages.UIElement('group_name_field', self, pages.By.XPATH, "//input[contains(@name, 'name')]")

    @property
    def product_selector(self):
        return pages.UIElement('product_selector', self, pages.By.XPATH, "//input[contains(@name, 'product')]")

    @property
    def firmware_selector(self):
        return pages.UIElement('firmware_selector', self, pages.By.XPATH, "//input[contains(@name, 'firmware')]")

    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            'OK button',
            parent=self,
            page_cls=GroupsPage,
            frame=self.parent.frame,
            strategy=pages.By.EXTJS_ID,
            locator="okButton")

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            'Cancel button',
            parent=self,
            page_cls=GroupsPage,
            frame=self.parent.frame,
            strategy=pages.By.EXTJS_ID,
            locator="cancelButton")

    def get_product_list_item(self, product_name):
        return pages.UIElement(product_name, self, pages.By.XPATH, "//li[contains(text(), '{}')]".format(product_name))

    def get_firmware_list_item(self, firmware_name):
        return pages.UIElement(firmware_name, self, pages.By.XPATH, "//li[contains(text(), '{}')]".format(firmware_name))

    def set_group_name(self, name):
        self.group_name_field.send_keys(name)
        return self

    def set_product(self, product_name):
        self.product_selector.click()
        self.get_product_list_item(product_name=product_name).click()
        return self

    def set_firmware(self, firmware_name):
        self.firmware_selector.click()
        self.get_firmware_list_item(firmware_name=firmware_name).click()
        return self

    def add_group(self, group_name, product_name, firmware_name) -> 'GroupsPage':
        self.set_group_name(group_name)
        self.set_product(product_name)
        self.set_firmware(firmware_name)
        return self.ok_button.click()

    def cancel(self):
        return self.cancel_button.click()


class ManageNCOSAppsDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@id, 'ManageRouterAppsWindow')]")

    def wait_for_region_to_load(self):
        self.spinner.wait_for_spinner()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def add_button(self):
        return pages.UIRegionLinkElement(
            'Add button',
            self,
            region_cls=AddAppsDialog,
            strategy=pages.By.XPATH,
            locator="//span[starts-with(@id, 'button') and (text() = 'Add')]",
            region_parent=self.page)

    @property
    def done_button(self):
        return pages.UIPageLinkElement(
            'Done button',
            parent=self,
            page_cls=GroupsPage,
            frame=self.parent.frame,
            strategy=pages.By.EXTJS_ID,
            locator="doneButton")

    @property
    def remove_button(self):
        return pages.UIRegionLinkElement(
            'Remove button',
            self,
            region_cls=UninstallAppConfirmation,
            strategy=pages.By.XPATH,
            locator="//span[text()='Remove']",
            region_parent=self.page)

    @property
    def router_apps_table(self):
        return RouterAppsTable(self)

    def add_app(self):
        add_apps_dialog = self.add_button.click()
        add_apps_dialog.add_apps_table.get_rows()[0].cells['CheckBox'].ui_element.click()
        confirmation = add_apps_dialog.ok_button.click()
        add_apps = confirmation.yes_button.click()
        add_apps.done_button.click()


class AddAppsDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-core-view-tools') "
                    "and contains(@id, 'AddRouterAppsWindow')]")

    @property
    def add_apps_table(self):
        return AddAppsTable(self)

    @property
    def cancel_button(self):
        return pages.UIRegionLinkElement(
            'Cancel button',
            self,
            region_cls=ManageNCOSAppsDialog,
            strategy=pages.By.EXTJS_ID,
            locator="cancelButton",
            region_parent=self.page)

    @property
    def ok_button(self):
        return pages.UIRegionLinkElement(
            'Ok button',
            self,
            region_cls=InstallAppConfirmation,
            strategy=pages.By.EXTJS_ID,
            locator="finishButton",
            region_parent=self.page)


class RouterAppsTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@id, 'ManageRouterAppsWindow')]")


class AddAppsTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-core-view-tools') "
                    "and contains(@id, 'AddRouterAppsWindow') "
                    "and contains(@id, 'body') "
                    "and not(contains(@id, 'header'))]")


class InstallAppConfirmation(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'messagebox') "
                    "and contains(@class, 'closable') "
                    "and not(contains(@class, 'body'))]")

    @property
    def yes_button(self):
        return pages.UIButtonElement('yes_button', self, pages.By.EXTJS_ID, "yes")

    @property
    def no_button(self):
        return pages.UIButtonElement('no_button', self, pages.By.XPATH, "no")


class UninstallAppConfirmation(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'messagebox') "
                    "and contains(@class, 'closable') "
                    "and not(contains(@class, 'body'))]")

    @property
    def yes_button(self):
        return pages.UIButtonElement('yes_button', self, pages.By.EXTJS_ID, "yes")

    @property
    def no_button(self):
        return pages.UIButtonElement('no_button', self, pages.By.EXTJS_ID, "no")


class GroupsPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#groups/list"

    @property
    def groups_panel(self):
        return GroupsPanelRegion(self)

    def add_group(self, group_name, product_name, firmware_name):
        return self.groups_panel.add_group_dialog.add_group(group_name, product_name, firmware_name)

    def modify_config(self):
        pass

    def delete_group(self):
        pass


class DeleteGroupConfirmation(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'messagebox-')]")

    @property
    def yes_button(self):
        return pages.UIButtonElement('yes_button', self, pages.By.EXTJS_ID, "yes")

    @property
    def no_button(self):
        return pages.UIButtonElement('no_button', self, pages.By.XPATH, "no")
