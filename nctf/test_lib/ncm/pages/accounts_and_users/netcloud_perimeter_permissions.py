from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.accounts_and_users import AccountsBasePage
from nctf.test_lib.ncp import pages as ncp_pages


class NCMNCPPermissionsPage(AccountsBasePage):
    URL_TEMPLATE = "/ecm.html#account/nce_permissions"

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.ncp_permissions_panel.wait_for_page_to_load()
        return self

    @property
    def ncp_permissions_panel(self):
        return ncp_pages.NCPPermissionsPage(
            parent=self,
            frame=(pages.By.XPATH, '//iframe[starts-with(@data-selector, "nce_permissions")]'),
            tenant_id='{tenant_id}')
