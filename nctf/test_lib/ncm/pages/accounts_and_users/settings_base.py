from nctf.test_lib.accounts import pages as ncmau
from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.accounts_and_users.accounts_and_users_base import AccountsBasePage


class SettingsBasePage(AccountsBasePage):
    URL_TEMPLATE = "/ecm.html#account/settings"
    IFRAME_LOCATOR = "//iframe[@data-selector='settings']"

    @property
    def settings_frame(self):
        self.iframe_element.wait_for_present(30)
        return ncmau.AccountSettingsPage(
            parent=self, frame=(pages.By.XPATH, self.IFRAME_LOCATOR))

    @property
    def iframe_element(self) -> pages.UIElement:
        return pages.UIElement(
            name="iframe for settings page",
            parent=self,
            strategy=pages.By.XPATH,
            locator=self.IFRAME_LOCATOR)
