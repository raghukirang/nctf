import calendar
import datetime
import typing

from selenium.webdriver.common.keys import Keys

from nctf.test_lib.base import pages
from nctf.test_lib.base.pages.by import By
from nctf.test_lib.ncm.pages.accounts_and_users import AccountsBasePage


class SubscriptionsPage(AccountsBasePage):

    @property
    def subscriptions_summary_region(self):
        return _SubscriptionsSummaryRegion(self)

    @property
    def subscriptions_region(self):
        return _SubscriptionsRegion(self)

    @property
    def date_range_picker_region(self):
        return _DateRangePickerRegion(self)


class _SubscriptionsSummaryRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.CSS_SELECTOR, "[data-test-subscriptions-summary-panel]")

    @property
    def title_region(self):
        return self._LicenseSummaryTitleRegion(self)

    @property
    def values_region(self):
        return self._LicenseSummaryValuesRegion(self)

    class _LicenseSummaryTitleRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CLASS_NAME, "cp-subscriptions-summary-panel-title")

        def get_title(self) -> str:
            return pages.UIReadOnlyElement(
                name="License Summary Title",
                parent=self,
                strategy=By.XPATH,
                locator=".//span").get_text().replace("\n", " ")

    class _LicenseSummaryValuesRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CLASS_NAME, "cp-subscriptions-summary-panel-values")

        def get_total_license_quantity_info(self) -> typing.Tuple[str, str]:
            title = pages.UIReadOnlyElement(
                name="Total License Quantity Title",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-total-licenses]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-text']").get_text().replace("\n", " ")
            value = pages.UIReadOnlyElement(
                name="Total License Quantity Value",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-total-licenses]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-active']").get_text()
            return title, value

        def get_total_devices_assigned_info(self) -> typing.Tuple[str, str]:
            title = pages.UIReadOnlyElement(
                name="Total Devices Assigned Title",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-devices-assigned]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-text']").get_text().replace("\n", " ")
            value = pages.UIReadOnlyElement(
                name="Total Devices Assigned Value",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-devices-assigned]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-active']").get_text()
            return title, value

        def get_days_until_next_expiration_info(self) -> typing.Tuple[str, str]:
            title = pages.UIReadOnlyElement(
                name="Days Until Next Expiration Title",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-next-expiration]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-text']").get_text().replace("\n", " ")
            value = pages.UIReadOnlyElement(
                name="Days Until Next Expiration Value",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-next-expiration]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-warning']").get_text()
            return title, value

        def get_total_non_compliant_devices_info(self) -> typing.Tuple[str, str]:
            title = pages.UIReadOnlyElement(
                name="Non-Compliant Devices",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-non-compliant]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-text']").get_text().replace("\n", " ")
            value = pages.UIReadOnlyElement(
                name="Non-Compliant Devices",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-non-compliant]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-negative']").get_text()
            return title, value

        def get_pending_active_quantity_info(self) -> typing.Tuple[str, str]:
            title = pages.UIReadOnlyElement(
                name="Pending Active Quantity",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-pending]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-text']").get_text().replace("\n", " ")
            value = pages.UIReadOnlyElement(
                name="Pending Active Quantity",
                parent=self,
                strategy=By.XPATH,
                locator=".//div[@data-test-subscriptions-pending]/"
                        "span[@class='cp-subscriptions-summary-panel-stat-accent']").get_text()
            return title, value


class _SubscriptionsRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//main[starts-with(@class, 'cp-subscriptions-page')]")

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.spinner.wait_for_spinner(timeout=60)
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CLASS_NAME, locator="cp-loader-background")

    @property
    def subscriptions_grid_region(self):
        return self._SubscriptionGridRegion(self)

    @property
    def subscription_filter_region(self):
        return self._SubscriptionFilterRegion(self)

    class _SubscriptionGridRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-main-content')]")

        @property
        def description_region(self):
            return self._PageDescriptionRegion(self)

        @property
        def filter_bar_region(self):
            return self._FilterBarRegion(self)

        @property
        def subscriptions_region(self):
            self.wait_for_region_to_load()
            return self._SubscriptionsRegion(self)

        class _PageDescriptionRegion(pages.UIRegion):
            ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-page-description')]")

            def get_description(self) -> str:
                return self.get_text()

        class _FilterBarRegion(pages.UIRegion):
            ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-filter-bar')]")

            def filter_button(self):
                return pages.UIButtonElement(name="Filter Button",
                                             parent=self,
                                             strategy=By.XPATH,
                                             locator=".//button[contains(@class, 'cp-filter-button')]")

        class _SubscriptionsRegion(pages.UIRegion):
            ROOT_LOCATOR = (By.XPATH, ".//section[starts-with(@class, 'cp-section')]")

            @property
            def toolbar_utilities_region(self):
                return self._ToolbarUtilitiesRegion(self)

            @property
            def subscriptions_table_region(self):
                return self._SubscriptionsTableRegion(self)

            @property
            def pagination_region(self):
                return self._PaginationRegion(self)

            class _ToolbarUtilitiesRegion(pages.UIRegion):
                ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-toolbar')]")

                def export_button(self):
                    return pages.UIButtonElement(name="Export Button",
                                                 parent=self,
                                                 strategy=By.CSS_SELECTOR,
                                                 locator="[data-test-subscriptions-export-button]")

                @property
                def export_options(self):
                    return self._ExportOptionsContainer(self)

                class _ExportOptionsContainer(pages.UIRegion):
                    ROOT_LOCATOR = (By.CLASS_NAME, "cp-utility-menu__container")

                    def export_filtered_subscriptions_button(self):
                        return pages.UIButtonElement(name="Export Filtered Subscriptions Button",
                                                     parent=self,
                                                     strategy=By.CSS_SELECTOR,
                                                     locator="[data-test-subscriptions-export-filtered]")

                    def export_all_subscriptions_button(self):
                        return pages.UIButtonElement(name="Export All Subscriptions Button",
                                                     parent=self,
                                                     strategy=By.CSS_SELECTOR,
                                                     locator="[data-test-subscriptions-export-all]")

            class _SubscriptionsTableRegion(pages.UIRegion):
                ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-subscriptions-grid')]")

                @property
                def subscriptions_table(self):
                    return self._SubscriptionsTable(self)

                class _SubscriptionsTable(pages.EmberTable):
                    ROOT_LOCATOR = (By.XPATH, ".//table")

            class _PaginationRegion(pages.UIRegion):
                ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-pagination')]")

                @property
                def pagination_controls_region(self):
                    return self._PaginationControlsRegion(self)

                def get_pagination_range_text(self):
                    return pages.UIReadOnlyElement(name="Pagination Range Text",
                                                   parent=self,
                                                   strategy=By.CLASS_NAME,
                                                   locator="cp-pagination-range").get_text()

                class _PaginationControlsRegion(pages.UIRegion):
                    ROOT_LOCATOR = (By.CLASS_NAME, "cp-pagination-controls")

                    def first_page_button(self):
                        return pages.UIButtonElement(name="First Page Button",
                                                     parent=self,
                                                     strategy=By.XPATH,
                                                     locator=".//button[contains(@class, 'cp-pagination__first-page')]")

                    def previous_page_button(self):
                        return pages.UIButtonElement(name="Previous Page Button",
                                                     parent=self,
                                                     strategy=By.XPATH,
                                                     locator=".//button[contains(@class, 'cp-pagination__prev-page')]")

                    @property
                    def page_input_region(self):
                        return self._PageInputRegion(self)

                    def next_page_button(self):
                        return pages.UIButtonElement(name="Next Page Button",
                                                     parent=self,
                                                     strategy=By.XPATH,
                                                     locator=".//button[contains(@class, 'cp-pagination__next-page')]")

                    def last_page_button(self):
                        return pages.UIButtonElement(name="Last Page Button",
                                                     parent=self,
                                                     strategy=By.XPATH,
                                                     locator=".//button[contains(@class, 'cp-pagination__last-page')]")

                    class _PageInputRegion(pages.UIRegion):
                        ROOT_LOCATOR = (By.CLASS_NAME, "cp-pagination__page-input-container")

                        def page_number_input(self):
                            return pages.UIElement(name="Page Number Input",
                                                   parent=self,
                                                   strategy=By.XPATH,
                                                   locator=".//input[contains(@class, 'cp-numeric-input')]")

                        def go_to_subscription_page_number(self, page_number: int):
                            if not isinstance(page_number, int):
                                raise TypeError("Page number must be an integer.")
                            if page_number <= 0:
                                raise ValueError(f"'{page_number}' is not a valid page number. "
                                                 "Page number must be greater or equal to 1")

                            subscriptions_page_input = self.page_number_input()
                            subscriptions_page_input.wait_for_present()
                            subscriptions_page_input.click()
                            subscriptions_page_input.send_keys(str(page_number))
                            subscriptions_page_input.send_keys(Keys.ENTER)

                        def total_pages_label_text(self) -> str:
                            return pages.UIReadOnlyElement(name='Total Pages Label Text',
                                                           parent=self,
                                                           strategy=By.CLASS_NAME,
                                                           locator="cp-pagination__total-pages-label").get_text()

    class _SubscriptionFilterRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CSS_SELECTOR, "[data-test-subscriptions-filter]")

        @property
        def filter_header(self):
            return self._FilterHeader(self)

        @property
        def filter_contents(self):
            return self._FilterContents(self)

        class _FilterHeader(pages.UIRegion):
            ROOT_LOCATOR = (By.CLASS_NAME, "cp-filter-header")

            @property
            def title_block(self):
                return self._TitleBlock(self)

            @property
            def actions(self):
                return self._Actions(self)

            class _TitleBlock(pages.UIRegion):
                ROOT_LOCATOR = (By.CLASS_NAME, "cp-filter-header-titleblock")

                def get_header_text(self) -> str:
                    return pages.UIReadOnlyElement(
                        name="Filter Header Text",
                        parent=self,
                        strategy=By.TAG_NAME,
                        locator="h2").get_text()

                def reset_filter_link(self):
                    return pages.UIButtonElement(name="Reset Filter Button",
                                                 parent=self,
                                                 strategy=By.CLASS_NAME,
                                                 locator="cp-filter-clear")

            class _Actions(pages.UIRegion):
                ROOT_LOCATOR = (By.CLASS_NAME, "cp-filter-header-actions")

                def close_filter_button(self):
                    return pages.UIButtonElement(name="Close Filter Button",
                                                 parent=self,
                                                 strategy=By.XPATH,
                                                 locator=".//i[contains(@class, 'cp-filter-close')]")

        class _FilterContents(pages.UIRegion):
            ROOT_LOCATOR = (By.CLASS_NAME, "cp-filter-contents")

            @property
            def filter_date_picker_region(self):
                return self._FilterDatePickerRegion(self)

            @property
            def filter_status_region(self):
                return self._FilterStatusRegion(self)

            class _FilterDatePickerRegion(pages.UIRegion):
                ROOT_LOCATOR = (By.XPATH, ".//section[1]")

                def get_header_text(self) -> str:
                    return pages.UIReadOnlyElement(
                        name="Expiration Header Text",
                        parent=self,
                        strategy=By.CLASS_NAME,
                        locator="cp-filter-section-header-title").get_text()

                def date_picker_dropdown(self):
                    return pages.UIButtonElement(name="Date Picker Button",
                                                 parent=self,
                                                 strategy=By.CSS_SELECTOR,
                                                 locator="[data-test-subscriptions-date-picker]")

            class _FilterStatusRegion(pages.UIRegion):
                ROOT_LOCATOR = (By.XPATH, ".//section[2]")

                @property
                def status_checkbox_region(self):
                    return self._StatusCheckboxRegion(self)

                def get_header_text(self) -> str:
                    return pages.UIReadOnlyElement(name="Expiration Header Text",
                                                   parent=self,
                                                   strategy=By.CLASS_NAME,
                                                   locator="cp-filter-section-header-title").get_text()

                class _StatusCheckboxRegion(pages.UIRegion):
                    ROOT_LOCATOR = (By.XPATH, ".//div[starts-with(@class, 'cp-checkbox-group')]")

                    def active_checkbox(self):
                        return pages.UICheckboxElement(name="Date Picker Button",
                                                       parent=self,
                                                       strategy=By.CSS_SELECTOR,
                                                       locator="[data-test-subscriptions-filter-status-active]")

                    def pending_active_checkbox(self):
                        return pages.UICheckboxElement(name="Date Picker Button",
                                                       parent=self,
                                                       strategy=By.CSS_SELECTOR,
                                                       locator="[data-test-subscriptions-filter-status-pending-active]")

                    def expired_checkbox(self):
                        return pages.UICheckboxElement(name="Date Picker Button",
                                                       parent=self,
                                                       strategy=By.CSS_SELECTOR,
                                                       locator="[data-test-subscriptions-filter-status-expired]")

                    def non_compliant_checkbox(self):
                        return pages.UICheckboxElement(name='Date Picker Button',
                                                       parent=self,
                                                       strategy=By.CSS_SELECTOR,
                                                       locator="[data-test-subscriptions-filter-status-non-compliant]")


class _DateRangePickerRegion(pages.UIRegion):
    ROOT_LOCATOR = (By.XPATH, "//div[starts-with(@class, 'daterangepicker')]")

    @property
    def date_ranges_region(self):
        return self._DateRangesRegion(self)

    @property
    def left_calendar_region(self):
        return self._LeftCalendarRegion(self)

    @property
    def right_calendar_region(self):
        return self._RightCalendarRegion(self)

    @property
    def calendar_footer_region(self):
        return self._CalendarFooterRegion(self)

    def select_date_range(self, start_date: str, end_date: str):

        def validate_date(date_text):
            try:
                datetime.datetime.strptime(date_text, "%m/%d/%Y")
            except ValueError:
                raise ValueError("Incorrect data format, should be MM/DD/YYYY")

        validate_date(start_date)
        validate_date(end_date)

        start_date_object = datetime.datetime.strptime(start_date, "%m/%d/%Y")
        end_date_object = datetime.datetime.strptime(end_date, "%m/%d/%Y")

        if start_date_object > end_date_object:
            raise ValueError(f"The end date ({end_date}) must be greater than or equal to the start date ({start_date})")

        while True:
            current_left_calendar_date = self.left_calendar_region.calendar_header.calendar_date().get_text().split(" ")
            current_left_calendar_date_month = int(list(calendar.month_abbr).index(current_left_calendar_date[0]))
            current_left_calendar_date_year = int(current_left_calendar_date[1])
            if start_date_object.year == current_left_calendar_date_year:
                if start_date_object.month == current_left_calendar_date_month:
                    self.left_calendar_region.calendar_body.select_day(start_date_object.day)
                    if start_date_object.month == end_date_object.month and start_date_object.year == end_date_object.year:
                        self.left_calendar_region.calendar_body.select_day(end_date_object.day)
                        break
                    else:
                        while True:
                            current_right_calendar_date = self.right_calendar_region.calendar_header.calendar_date().get_text().split(" ")
                            current_right_calendar_date_month = int(list(calendar.month_abbr).index(current_right_calendar_date[0]))
                            current_right_calendar_date_year = int(current_right_calendar_date[1])
                            if end_date_object.year == current_right_calendar_date_year:
                                if end_date_object.month == current_right_calendar_date_month:
                                    self.right_calendar_region.calendar_body.select_day(end_date_object.day)
                                    break
                                else:
                                    self.right_calendar_region.calendar_header.next_month_button().click()
                            else:
                                self.right_calendar_region.calendar_header.next_month_button().click()
                        break
                elif start_date_object.month < current_left_calendar_date_month:
                    self.left_calendar_region.calendar_header.previous_month_button().click()
                else:
                    self.right_calendar_region.calendar_header.next_month_button().click()
            else:
                if start_date_object.year < current_left_calendar_date_year:
                    self.left_calendar_region.calendar_header.previous_month_button().click()
                else:
                    self.right_calendar_region.calendar_header.next_month_button().click()

    class _DateRangesRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.CLASS_NAME, "ranges")

        def next_30_days_button(self):
            return pages.UIButtonElement(name="Next 30 Days Button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="[data-range-key='Next 30 Days']")

        def next_60_days_button(self):
            return pages.UIButtonElement(name="Next 60 Days Button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="[data-range-key='Next 60 Days']")

        def next_90_days_button(self):
            return pages.UIButtonElement(name="Next 90 Days Button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="[data-range-key='Next 90 Days']")

        def previous_30_days_button(self):
            return pages.UIButtonElement(name="Previous 30 Days Button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="[data-range-key='Previous 30 Days']")

        def previous_60_days_button(self):
            return pages.UIButtonElement(name="Previous 60 Days Button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="[data-range-key='Previous 60 Days']")

        def previous_90_days_button(self):
            return pages.UIButtonElement(name="Previous 90 Days Button",
                                         parent=self,
                                         strategy=By.CSS_SELECTOR,
                                         locator="[data-range-key='Previous 90 Days']")

    class _LeftCalendarRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.XPATH, ".//div[@class='drp-calendar left']")

        @property
        def calendar_header(self):
            return self._CalendarHeader(self)

        @property
        def calendar_body(self):
            return self._CalendarBody(self)

        class _CalendarHeader(pages.UIRegion):
            ROOT_LOCATOR = (By.TAG_NAME, "thead")

            def calendar_date(self):
                return pages.UIReadOnlyElement(name="Left Calendar Date",
                                               parent=self,
                                               strategy=By.CLASS_NAME,
                                               locator="month")

            def previous_month_button(self):
                return pages.UIButtonElement(name="Previous Month",
                                             parent=self,
                                             strategy=By.XPATH,
                                             locator=".//th[@class='prev available']")

        class _CalendarBody(pages.UIRegion):
            ROOT_LOCATOR = (By.TAG_NAME, "tbody")

            def select_day(self, day_of_month):
                return pages.UIButtonElement(
                    name="Calendar Day",
                    parent=self,
                    strategy=By.XPATH,
                    locator=f".//td[not(contains(@class, 'off')) and text()='{day_of_month}']").click()

    class _RightCalendarRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.XPATH, ".//div[@class='drp-calendar right']")

        @property
        def calendar_header(self):
            return self._CalendarHeader(self)

        @property
        def calendar_body(self):
            return self._CalendarBody(self)

        class _CalendarHeader(pages.UIRegion):
            ROOT_LOCATOR = (By.TAG_NAME, "thead")

            def calendar_date(self):
                return pages.UIReadOnlyElement(name="Right Calendar Date",
                                               parent=self,
                                               strategy=By.CLASS_NAME,
                                               locator="month")

            def next_month_button(self):
                return pages.UIButtonElement(name="Next Month",
                                             parent=self,
                                             strategy=By.XPATH,
                                             locator=".//th[@class='next available']")

        class _CalendarBody(pages.UIRegion):
            ROOT_LOCATOR = (By.TAG_NAME, "tbody")

            def select_day(self, day_of_month):
                return pages.UIButtonElement(
                    name="Calendar Day",
                    parent=self,
                    strategy=By.XPATH,
                    locator=f".//td[not(contains(@class, 'off')) and text()='{day_of_month}']").click()

    class _CalendarFooterRegion(pages.UIRegion):
        ROOT_LOCATOR = (By.XPATH, ".//div[@class='drp-buttons']")

        def get_date_range_text(self):
            return pages.UIReadOnlyElement(name="Date Range Text",
                                           parent=self,
                                           strategy=By.CLASS_NAME,
                                           locator='drp-selected').get_text()

        def apply_button(self):
            return pages.UIButtonElement(name="Apply Button",
                                         parent=self,
                                         strategy=By.XPATH,
                                         locator=".//button[starts-with(@class, 'applyBtn')]",
                                         )

        def cancel_button(self):
            return pages.UIButtonElement(name="Cancel Button",
                                         parent=self,
                                         strategy=By.XPATH,
                                         locator=".//button[starts-with(@class, 'cancelBtn')]",
                                         )
