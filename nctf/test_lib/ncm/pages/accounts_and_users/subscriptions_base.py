from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages import accounts_and_users
from nctf.test_lib.ncm.pages.accounts_and_users.accounts_and_users_base import AccountsBasePage


class SubscriptionsBasePage(AccountsBasePage):
    URL_TEMPLATE = "/ecm.html#account/account_subscriptions"
    IFRAME_LOCATOR = "//iframe[@data-selector='account_subscriptions']"

    @property
    def subscriptions_frame(self):
        self.iframe_element.wait_for_present(30)
        return accounts_and_users.SubscriptionsPage(
            parent=self, frame=(pages.By.XPATH, self.IFRAME_LOCATOR))

    @property
    def iframe_element(self) -> pages.UIElement:
        return pages.UIElement(
            name="iframe for subscriptions page",
            parent=self,
            strategy=pages.By.XPATH,
            locator=self.IFRAME_LOCATOR)
