import time

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.accounts_and_users.accounts_and_users_base import AccountsBasePage


class NCMPermissionsPage(AccountsBasePage):
    URL_TEMPLATE = "ecm.html#account/ecm_permissions"
    DEFAULT_TIMEOUT = 40

    @property
    def permissions_panel(self):
        return NCMPermissionsPanel(parent=self)


class NCMPermissionsPanel(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, ".//div[starts-with(@id, 'ecm-core-view-Accounts')]")

    @property
    def toolbar(self):
        return Toolbar(self)

    @property
    def ncm_permissions_table(self):
        return PermissionsTable(self)


class Toolbar(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CLASS_NAME, "x-toolbar")

    @property
    def add_dropdown(self):
        return pages.UIRegionLinkElement(
            name="Add dropdown",
            parent=self,
            region_cls=Toolbar.AddDropdownRegion,
            strategy=pages.By.EXTJS_ID,
            locator="addButton",
            region_parent=self.page)

    @property
    def edit_subaccount_button(self):
        return pages.UIRegionLinkElement(
            name="Subaccount Edit button",
            parent=self,
            region_cls=EditSubaccountDialog,
            strategy=pages.By.EXTJS_ID,
            locator="editButton")

    @property
    def edit_user_button(self):
        return pages.UIRegionLinkElement(
            name="User Permissions Edit button",
            parent=self,
            region_cls=EditUserDialog,
            strategy=pages.By.EXTJS_ID,
            locator="editButton")

    @property
    def delete_button(self):
        return pages.UIRegionLinkElement(
            name="Delete button",
            parent=self,
            region_cls=DeleteAccountDialog,
            strategy=pages.By.EXTJS_ID,
            locator="deleteButton")

    @property
    def move_button(self):
        return pages.UIRegionLinkElement(
            name="Move button",
            parent=self,
            region_cls=pages.UIRegion,  # not sure about the region
            strategy=pages.By.EXTJS_ID,
            locator="moveButton")

    class AddDropdownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CLASS_NAME, "x-body")  # claim the region is the whole page since we don't have an id

        @property
        def user_button(self):
            return pages.UIRegionLinkElement(
                name='User Button',
                parent=self,
                region_cls=NCMPermissionsAddUserDialog,
                region_parent=self.page,
                strategy=pages.By.ID,
                locator="accounts-add-user-button")

        @property
        def subaccount_button(self):
            return pages.UIRegionLinkElement(
                name='Add Subaccount Button',
                parent=self,
                region_cls=AddSubaccountDialog,
                region_parent=self.page,
                strategy=pages.By.ID,
                locator="accounts-add-sub-account-button")


class PermissionsTable(pages.ExtJSTreePicker):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, '.x-tree-panel')

    def expand_account_icon(self, sub_account_name: str):
        xpath = "//span[text()='{sub_account_name}']/preceding-sibling::" \
                "img[contains(@class,'x-tree-expander')]".format(sub_account_name=sub_account_name)

        return pages.UIRegionLinkElement(
            name='Expand Subaccount Button',
            parent=self,
            region_cls=NCMPermissionsPanel,
            strategy=pages.By.XPATH,
            locator=xpath)


class EditUserDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, ".x-window.x-layer")

    def wait_for_page_to_load(self):
        super().wait_for_region_to_load()
        self.cancel_button.wait_for_clickable()
        return self

    @property
    def role_dropdown(self):
        return pages.UIRegionLinkElement(
            name='NCM Role dropdown',
            parent=self,
            region_cls=EditUserDialog.RoleSelectionRegion,
            strategy=pages.By.XPATH,
            locator="//input[contains(@id, 'ecm-widget-ObjectComboBox')]")

    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            name='OK button',
            parent=self,
            page_cls=NCMPermissionsPage,
            strategy=pages.By.CSS_SELECTOR,
            locator=".x-btn.OkButton")

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            name="Cancel button",
            parent=self,
            page_cls=NCMPermissionsPage,
            strategy=pages.By.CSS_SELECTOR,
            locator=".x-btn.CancelButton")

    class RoleSelectionRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.XPATH, "//ul[contains(@class, 'x-list-plain')]")

        def wait_for_region_to_load(self):
            super().wait_for_region_to_load()
            time.sleep(0.5)
            self.no_access_item.wait_for_clickable()
            return self

        @property
        def no_access_item(self):
            return pages.UIElement(name="No Access item",
                                   parent=self,
                                   strategy=pages.By.XPATH,
                                   locator=".//li[text()='No Access']")

        def role_selector(self, role: str):
            dropdown_options = ['Administrator', 'Full Access User', 'Read Only User', 'Diagnostic User', 'No Access']
            assert role in dropdown_options, "{} is not a valid NCM Role".format(role)
            xpath = ".//li[text()='{}']".format(role)
            return pages.UIRegionLinkElement(
                name="User Role button",
                parent=self,
                region_cls=EditUserDialog,
                region_parent=self.page,
                strategy=pages.By.XPATH,
                locator=xpath)


class EditSubaccountDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//span[text()='Edit Subaccount']/../../../../../..")

    @property
    def name_field(self):
        return pages.UIElement('Subaccount Name field', self, pages.By.XPATH, "//input[@name='name']")

    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            name='OK button',
            parent=self,
            page_cls=NCMPermissionsPage,
            strategy=pages.By.CSS_SELECTOR,
            locator=".x-btn.OkButton")

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            name="Cancel button",
            parent=self,
            page_cls=NCMPermissionsPage,
            strategy=pages.By.CSS_SELECTOR,
            locator=".x-btn.CancelButton")


class AddSubaccountDialog(EditSubaccountDialog):
    ROOT_LOCATOR = (pages.By.XPATH, "//span[text()='Add Subaccount']/../../../../../..")


class NCMPermissionsAddUserDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CLASS_NAME, "add-sso-users-window")

    @property
    def existing_users_radio_button(self):
        return pages.UIElement('Select from Existing Users radio button', self, pages.By.EXTJS_ID, 'selectExisting')

    @property
    def another_ncm_account_radio_button(self):
        return pages.UIElement('Select from Another NCM Account radio button', self, pages.By.EXTJS_ID, 'selectOther')

    @property
    def add_ncm_user_table(self):
        return NCMPermissionsAddUserDialog.UserTable(self)

    class UserTable(pages.ExtJSTable):
        ROOT_LOCATOR = (pages.By.EXTJS_ID, 'usersGrid')

    @property
    def role_dropdown(self):
        return pages.UIRegionLinkElement(
            name='NCM Role dropdown',
            parent=self,
            region_cls=NCMPermissionsAddUserDialog.RoleSelectionRegion,
            region_parent=self.page,
            strategy=pages.By.EXTJS_ID,
            locator='rolesCombo')

    @property
    def save_changes_button(self):
        return pages.UIPageLinkElement(
            name="Save Changes Button",
            parent=self,
            page_cls=NCMPermissionsPage,
            frame=self.frame,
            strategy=pages.By.ID,
            locator="add-sso-users-window-save-button")

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            name="Cancel Button",
            parent=self,
            page_cls=NCMPermissionsPage,
            frame=self.frame,
            strategy=pages.By.ID,
            locator="add-sso-users-window-cancel-button")

    class RoleSelectionRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CLASS_NAME, 'x-boundlist')

        def role_selector(self, role: str):
            dropdown_options = ['Administrator', 'Full Access User', 'Read Only User', 'Diagnostic User', 'No Access']
            assert role in dropdown_options, "{} is not a valid NCM Role".format(role)
            xpath = ".//li[text()='{}']".format(role)
            return pages.UIRegionLinkElement(
                name="User Role button",
                parent=self,
                region_cls=NCMPermissionsAddUserDialog,
                region_parent=self.page,
                strategy=pages.By.XPATH,
                locator=xpath)


class DeleteAccountDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//span[text()='Delete Account']/../../../../../..")

    def wait_for_region_to_load(self):
        self.yes_button.wait_for_clickable()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def yes_button(self):
        return pages.UIPageLinkElement(
            name="Yes Button",
            parent=self,
            page_cls=NCMPermissionsPage,
            frame=self.frame,
            strategy=pages.By.XPATH,
            locator="//span[text()='Yes']/..")

    @property
    def no_button(self):
        return pages.UIPageLinkElement(
            name="No Button",
            parent=self,
            page_cls=NCMPermissionsPage,
            frame=self.frame,
            strategy=pages.By.XPATH,
            locator="//span[text()='No']/..")


class DeleteErrorDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//span[text()='Request Error']/../../../../../..")
