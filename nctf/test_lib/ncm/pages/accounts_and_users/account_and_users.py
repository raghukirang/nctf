from nctf.test_lib.accounts import pages as acct_pages
from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.accounts_and_users import AccountsBasePage


class AccountsAndUsersPage(AccountsBasePage):
    URL_TEMPLATE = "/ecm.html#account/users"

    @property
    def account_users_panel(self):
        self.iframe_element.wait_for_present(20)
        return acct_pages.UsersPage(parent=self, frame=(pages.By.XPATH, '//iframe'))

    @property
    def iframe_element(self) -> pages.UIElement:
        return pages.UIElement(
            name="iframe for users page",
            parent=self,
            strategy=pages.By.XPATH,
            locator='//iframe[starts-with(@data-selector, "users")]')
