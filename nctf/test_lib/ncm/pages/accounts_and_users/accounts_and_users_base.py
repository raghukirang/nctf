from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By
from nctf.test_lib.ncm.pages import accounts_and_users as ncmau
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class AccountsBasePage(NCMBasePage):
    @property
    def account_users_page_button(self):
        return pages.UIPageLinkElement(
            'Users Button',
            self,
            page_cls=ncmau.AccountsAndUsersPage,
            strategy=By.ID,
            locator='nav-button-users')

    @property
    def ncm_permissions_page_button(self):
        return pages.UIPageLinkElement(
            'NetCloud Manager Permissions Button',
            self,
            page_cls=ncmau.NCMPermissionsPage,
            strategy=By.ID,
            locator='nav-button-ecm-permissions')

    @property
    def ncp_permissions_page_button(self):
        return pages.UIPageLinkElement(
            'NetCloud Perimeter Permissions Button',
            self,
            page_cls=ncmau.NCMNCPPermissionsPage,
            strategy=By.ID,
            locator='nav-button-nce-permissions')

    @property
    def settings_page_button(self):
        return pages.UIPageLinkElement(
            name='Settings Button',
            parent=self,
            page_cls=ncmau.SettingsBasePage,
            strategy=By.ID,
            locator='nav-button-settings')

    @property
    def subscriptions_page_button(self):
        return pages.UIPageLinkElement(
            name='Subscriptions Button',
            parent=self,
            page_cls=ncmau.SubscriptionsBasePage,
            strategy=By.ID,
            locator='nav-button-account-subscriptions')
