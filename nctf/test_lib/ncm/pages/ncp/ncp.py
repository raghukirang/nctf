import logging

from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage
from nctf.test_lib.ncp.pages.overlay import OverlayPage

logger = logging.getLogger('ncm.pages.ncp')


class OverlayInFrame(OverlayPage):
    FRAME_LOCATOR = (pages.By.XPATH, '//iframe')


class NCPPage(NCMBasePage):
    URL_TEMPLATE = "/ecm.html#networks/vpn"
    DEFAULT_TIMEOUT = 30

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def overlay(self):
        return OverlayInFrame(parent=self, base_url=self.base_url)
