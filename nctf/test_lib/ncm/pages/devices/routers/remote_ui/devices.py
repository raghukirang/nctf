from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.ncm.pages.devices.routers.remote_ui.common import RemoteUIBasePage


class DevicesPage(RemoteUIBasePage):
    URL_TEMPLATE = '{TBD}'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.add_button.wait_for_clickable(timeout=30)
        return self

    @property
    def add_button(self):
        return base_pages.UIRegionLinkElement(
            'Add Button', self, region_cls=NotImplemented, strategy=base_pages.By.EXTJS_ID, locator="addButton")

    @property
    def edit_button(self):
        return base_pages.UIRegionLinkElement(
            'Edit Button', self, region_cls=NotImplemented, strategy=base_pages.By.EXTJS_ID, locator="editButton")

    @property
    def delete_button(self):
        return base_pages.UIRegionLinkElement(
            'Delete Button', self, region_cls=NotImplemented, strategy=base_pages.By.EXTJS_ID, locator="deleteButton")

    @property
    def control_button(self):
        return base_pages.UIRegionLinkElement(
            'remove button', self, region_cls=NotImplemented, strategy=base_pages.By.EXTJS_ID, locator="controlButton")

    @property
    def door_open_warning_b(self):
        return base_pages.UIReadOnlyElement('Door Open Warning', self)
