import logging

from attrdict import AttrMap
from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.ncm.pages.devices.routers import remote_ui as remote_ui_pages


class SNB(base_pages.UIRegion):
    ROOT_LOCATOR = (base_pages.By.CSS_SELECTOR, 'div.x-panel-west-side[id^="PagesMenu"]')

    @property
    def menu(self):
        return AttrMap({
            'connection_manager': {
                'devices':
                base_pages.UIPageLinkElement(
                    'Devices Button',
                    parent=self,
                    page_cls=remote_ui_pages.devices.DevicesPage,
                    strategy=base_pages.By.ID,
                    locator='//*[@id="treeview-1049-record-13"]/tbody/tr/td/div/span')
            }
        })


class SideNavBar(base_pages.UIRegion):
    ROOT_LOCATOR = (base_pages.By.CSS_SELECTOR, 'div.x-panel-west-side[id^="PagesMenu"]')

    class ConnectionManagerMenu(base_pages.UIRegion):
        @property
        def devices(self):
            return base_pages.UIPageLinkElement(
                'Devices Button',
                self,
                page_cls=remote_ui_pages.devices.DevicesPage,
                strategy=base_pages.By.ID,
                locator='//*[@id="treeview-1049-record-13"]/tbody/tr/td/div/span')

        @property
        def vpn_networks(self):
            return base_pages.UIPageLinkElement(
                'Devices Button',
                self,
                remote_ui_pages.VPNNetworksPage,
                strategy=base_pages.By.ID,
                locator='app-dashboard-button')

    @property
    def connection_manager_button(self):
        return base_pages.UIRegionLinkElement(
            'Dashboard Button',
            self,
            SideNavBar.ConnectionManagerMenu,
            strategy=base_pages.By.ID,
            locator='app-dashboard-button')

    class NetworkingMenu(base_pages.UIRegion):
        ROOT_LOCATOR = (base_pages.By.EXTJS_ID, 'networking')

        class LocalNetworksDropdown(base_pages.UIRegion):
            # There is nothing in the DOM to differentiate the dropdown items from the rest of the Networking menu
            ROOT_LOCATOR = (base_pages.By.EXTJS_ID, 'networking')

            @property
            def local_ip_networks(self):
                return base_pages.UIPageLinkElement(
                    'local ip networks',
                    self,
                    page_cls=remote_ui_pages.local_ip_networks.LocalIPNetworksPage,
                    strategy=base_pages.By.XPATH,
                    locator="//span[text()='Local IP Networks']/..",
                    TBD='{TBD}',
                    frame=self.frame)

        @property
        def local_networks(self):
            return base_pages.UIRegionLinkElement(
                'local networks dropdown',
                self,
                region_cls=SideNavBar.NetworkingMenu.LocalNetworksDropdown,
                region_parent=self.parent,
                strategy=base_pages.By.XPATH,
                locator="//span[text()='Local Networks']/..")

    @property
    def networking_menu(self):
        return base_pages.UIRegionLinkElement(
            'networking menu',
            self,
            region_cls=SideNavBar.NetworkingMenu,
            strategy=base_pages.By.EXTJS_ID,
            locator="networking",
            region_parent=self.parent)


class SideHelp(base_pages.UIRegion):
    ROOT_LOCATOR = (base_pages.By.CSS_SELECTOR, 'div.x-panel-east-side[id^="MainHelp"]')

    @property
    def collapse_right_button(self):
        return base_pages.UIPageLinkElement(
            'Collapse Right Button',
            self,
            page_cls=type(self.parent.page),
            strategy=base_pages.By.CLASS_NAME,
            locator='x-tool-collapse-right',
            TBD='{TBD}')

    @property
    def main_help_knowledge_base_button(self):
        raise NotImplemented  #(strategy=base_pages.By.EXTJS_ID, locator='MainHelpKnowledgeBaseButton')


class RemoteUIBasePage(base_pages.UIPage):
    base_pages.UIPage.DEFAULT_TIMEOUT += 5.0  # Increasing remote timeout beyond the default page value.

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._side_help = None

    class SideHelpButton(base_pages.UIRegionLinkElement):
        def __init__(self, parent: 'RemoteUIBasePage', strategy: str, locator: str):
            super().__init__(
                name="Get Help Button",
                parent=parent,
                region_cls=SideHelp,
                strategy=strategy,
                locator=locator,
                region_parent=parent)
            self.parent = parent

        def click(self):
            self.parent._side_help = super().click()
            return self.parent._side_help

    @property
    def spinner(self):
        return base_pages.UISpinnerElement(self, strategy=base_pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def wait_for_page_to_load(self):
        self.spinner.wait_for_spinner()
        return self

    @property
    def side_nav_bar(self):
        return SideNavBar(self)

    @property
    def side_help(self) -> SideHelp:
        """Returns the expanded side help region on the page.

        If the side_help_button is visible, then the side help needs to be expanded, i.e. clicked; otherwise, it is just
        returned from the cached _side_help.
        """
        if self.side_help_button.is_displayed():
            self._side_help = self.side_help_button.click()
        if self._side_help:
            return self._side_help
        else:
            raise RuntimeError('{} Inconsistent State: _side_help is None'.format(self))

    @property
    def side_help_button(self):
        return RemoteUIBasePage.SideHelpButton(parent=self, strategy=base_pages.By.CLASS_NAME, locator='x-tool-expand-left')
