import logging
import time

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.ncm.pages.devices.routers.remote_ui.common import RemoteUIBasePage

logger = logging.getLogger(base_pages.make_short_qualified_name(__name__))


class LocalIPNetworksPage(RemoteUIBasePage):
    URL_TEMPLATE = '{TBD}'

    @property
    def add_button(self):
        return base_pages.UIRegionLinkElement(
            'add button',
            self,
            region_cls=LocalNetworkGeneralSettings,
            region_parent=self,
            strategy=base_pages.By.EXTJS_ID,
            locator="addButton")

    @property
    def edit_button(self):
        return base_pages.UIRegionLinkElement(
            'edit button', self, region_cls=NotImplemented, strategy=base_pages.By.EXTJS_ID, locator="editButton")

    @property
    def remove_button(self):
        return base_pages.UIRegionLinkElement(
            'remove button', self, region_cls=NotImplemented, strategy=base_pages.By.EXTJS_ID, locator="removeButton")

    def add_network(self, name: str, ipv4_addr: str):
        local_network_editor = self.add_button.click()
        local_network_editor.name_field.send_keys(name)
        local_network_editor.sidebar.ipv4_settings.click().ip_field.send_keys(ipv4_addr)
        time.sleep(0.5)  # Otherwise it is too quick and throws error: missing required field(s): ['ip_address']
        save_window = local_network_editor.save_button.click()
        assert save_window.message.get_text() == 'Settings were successfully saved'
        return save_window.ok_button.click()


class LocalNetworkSettings(base_pages.UIRegion):
    ROOT_LOCATOR = (base_pages.By.CSS_SELECTOR, 'div[id^="NetworkForm"]')

    @property
    def sidebar(self):
        return LocalNetworkSidebar(self)

    @property
    def save_button(self):
        return base_pages.UIRegionLinkElement(
            'save button',
            self,
            region_cls=SaveSuccessWindow,
            region_parent=self.parent,
            strategy=base_pages.By.CLASS_NAME,
            locator='x-btn-inner-accept-button-medium')


class SaveSuccessWindow(base_pages.UIRegion):
    ROOT_LOCATOR = (base_pages.By.CSS_SELECTOR, 'div[id^="messagebox-"')

    @property
    def spinner(self):
        return base_pages.UISpinnerElement(self.page, strategy=base_pages.By.CLASS_NAME, locator="x-progress-bar")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner()
        self.ok_button.wait_for_clickable()
        return self

    @property
    def message(self):
        return base_pages.UIElement(
            'save window message', self, strategy=base_pages.By.CSS_SELECTOR, locator='div[id^="messagebox-"][id$="-msg"]')

    @property
    def ok_button(self):
        return base_pages.UIPageLinkElement(
            'ok_button',
            self,
            page_cls=LocalIPNetworksPage,
            frame=self.parent.frame,
            strategy=base_pages.By.XPATH,
            locator='.//span[text()="OK"]')


class LocalNetworkSidebar(base_pages.UIRegion):
    ROOT_LOCATOR = (base_pages.By.CLASS_NAME, 'x-tab-bar-default-docked-left')

    @property
    def ipv4_settings(self):
        return base_pages.UIRegionLinkElement(
            'ipv4 settings tab',
            self,
            region_cls=LocalNetworkIPv4Settings,
            region_parent=self.parent.parent,  # important relationship
            strategy=base_pages.By.XPATH,
            locator=".//span[text()='IPv4 Settings']")


class LocalNetworkGeneralSettings(LocalNetworkSettings):
    #ROOT_LOCATOR = (base_pages.By.EXTJS_ID, 'general')

    @property
    def name_field(self):
        return base_pages.UIElement(
            'network name field', self, strategy=base_pages.By.XPATH, locator='.//span[text()="Name:"]/../..//input')


class LocalNetworkIPv4Settings(LocalNetworkSettings):
    #ROOT_LOCATOR = (base_pages.By.EXTJS_ID, 'ipv4')

    @property
    def ip_field(self):
        return base_pages.UIElement(
            'ip address field', self, strategy=base_pages.By.XPATH, locator='.//span[text()="IP Address:"]/../..//input')
