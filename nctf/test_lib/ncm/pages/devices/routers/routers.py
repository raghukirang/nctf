import logging
import typing

from nctf.test_lib.base import pages
from nctf.test_lib.base.pages.by import By
from nctf.test_lib.ncm.pages import devices as dev_pages
from nctf.test_lib.ncm.pages.common.edit_configuration_dialog import EditConfigurationDialog

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class RouterTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@class, 'x-panel')]"
                    "//div[contains(@id, 'ecm-core-view-devices-Routers')"
                    " and contains(@class, 'x-grid-with-row-lines')]")

    # Following row redefinitions are for ease of use, the record_id attribute is defined to be the NCM router_id
    class Row(pages.ExtJSTable.Row):
        @property
        def router_id(self) -> str:
            return self.record_id

    def get_rows(self, key: str = None, value: str = None, regex: str = None) -> typing.List['RouterTable.Row']:
        return super(RouterTable, self).get_rows(key=key, value=value, regex=regex)

    def refresh(self) -> 'RouterTable':
        return self.parent.toolbar.refresh_button.click().router_table

    def device_selection_checkbox(self, device_id: str):
        """ Selection checkbox for a specified device id

        Args:
            device_id: The id of the device in ncm to get the checkbox for
        """
        css = '[data-recordid="{}"] > td > div > div.x-grid-row-checker'.format(device_id)
        return pages.UIButtonElement("device selection checkbox", self, strategy=pages.By.CSS_SELECTOR, locator=css)

    def is_device_selection_checkbox_checked(self, device_id) -> bool:
        # get the table row <tr>
        cb_element = self.device_selection_checkbox(device_id)
        table_row = cb_element._element.find_element_by_xpath("../../..")

        # the class attribute will reflect the state of the checkbox
        if 'x-grid-row-selected' in table_row.get_attribute('class'):
            return True
        else:
            return False

    def device_name_hyperlink(self, device_name):
        """device name

        Note:
            will not work if multiple fields have the same name

        Args:
           device_name (str): the device name
        """
        xpath = "//a[text()='{}']".format(device_name)
        return pages.UIPageLinkElement(
            "device name link",
            self,
            page_cls=dev_pages.DevicesDashBoardPage,
            strategy=pages.By.XPATH,
            locator=xpath,
            id='{id}')


class Toolbar(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "div.x-toolbar")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.refresh_button.wait_for_clickable(timeout=30)  # although this button clickable seems to work too

    @property
    def refresh_button(self):
        return pages.UIRegionLinkElement(
            name='refresh_button',
            parent=self,
            region_cls=RoutersPanelRegion,
            strategy=pages.By.CSS_SELECTOR,
            locator='#router-refresh-button',
            region_parent=self.parent.parent)

    def refresh(self) -> 'RoutersPanelRegion':
        return self.refresh_button.click()

    @property
    def commands_dropdown(self):
        return pages.UIRegionLinkElement(
            "commands dropdown",
            self,
            region_cls=self.CmdDropDownRegion,
            strategy=pages.By.EXTJS_ID,
            locator='commandsMenu',
            region_parent=self.page)

    @property
    def configuration_dropdown(self):
        xpath = ".//span[text()='Configuration' and starts-with(@id, 'button')]"
        return pages.UIRegionLinkElement(
            "configuration dropdown",
            self,
            region_cls=self.ConfigDropDownRegion,
            strategy=pages.By.XPATH,
            locator=xpath,
            region_parent=self.page)

    @property
    def move_button(self):
        return pages.UIRegionLinkElement(
            name='Move Button',
            parent=self,
            region_cls=SelectAccountGroupDialog,
            strategy=pages.By.EXTJS_ID,
            locator='moveButton',
            region_parent=self.page)

    class ConfigDropDownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, 'div.x-menu-body[id^="menu-"][id$="-body"]')

        @property
        def edit_configuration_button(self):
            xpath = ".//span[text()='Edit']"
            return pages.UIRegionLinkElement(
                'Edit Configuration Button',
                self,
                region_cls=EditDeviceConfigurationDialog,
                strategy=pages.By.XPATH,
                locator=xpath)

        @property
        def summary_button(self):
            xpath = ".//span[text()='Summary']"
            return pages.UIRegionLinkElement(
                'Summary Button', self, region_cls=DeviceConfigSummary, strategy=pages.By.XPATH, locator=xpath)

    class CmdDropDownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "body")

        @property
        def wifi_site_survey_button(self):
            return pages.UIRegionLinkElement(
                'WiFi Site Survey Button',
                self,
                region_cls=WifiSiteSurveyRegion,
                strategy=pages.By.EXTJS_ID,
                locator="commandSiteSurvey",
                region_parent=self.page)

        @property
        def traceroute_button(self):
            return pages.UIButtonElement('Traceroute Button', self, strategy=pages.By.EXTJS_ID, locator="commandTraceroute")

        @property
        def create_netcloud_gateway_button(self):
            return pages.UIRegionLinkElement(
                'Create NetCloud Gateway Button',
                self,
                region_cls=AddNcpRegion,
                strategy=pages.By.EXTJS_ID,
                locator="commandAddNCE",
                region_parent=self.page)

        @property
        def remove_netcloud_gateway_button(self):
            return pages.UIRegionLinkElement(
                'Remove NetCloud Gateway Button',
                self,
                region_cls=RemoveNcpRegion,
                strategy=pages.By.EXTJS_ID,
                locator="commandRemoveNCE",
                region_parent=self.page)


class SelectAccountGroupDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, (".//div[starts-with(@id, 'ecm-core-view-AccountGroupTreeWindow')"
                                     "and contains(@class, 'window-default')]"))

    @property
    def account_group_tree(self):
        return AccountTreePicker(self.page)

    # Returns the user to the Routers Page
    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            name='Yes button',
            parent=self,
            page_cls=RoutersPage,
            frame=self.frame,
            strategy=pages.By.EXTJS_ID,
            locator='okButton')

    @property
    def cancel_button(self):
        return pages.UIPageLinkElement(
            name='Cancel button',
            parent=self,
            page_cls=RoutersPage,
            frame=self.frame,
            strategy=pages.By.EXTJS_ID,
            locator='cancelButton')

    # Redirects the user to the Request Error popup dialog
    @property
    def ok_button_to_error(self):
        return pages.UIRegionLinkElement(
            name='Ok button',
            parent=self,
            region_cls=RequestErrorDialog,
            strategy=pages.By.EXTJS_ID,
            locator='okButton',
            region_parent=self.parent)

    def request_error(self):
        self.ok_button_to_error.click()
        return RequestErrorDialog.wait_for_region_to_load()


class RequestErrorDialog(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'messagebox') and contains(@class, 'x-message-box')]")

    # Error button within the Request Error
    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            name='Ok button', parent=self, page_cls=RoutersPage, frame=self.frame, strategy=pages.By.EXTJS_ID, locator='ok')

    def error_message_present(self, expected_error):
        return pages.UIElement("error message", self, By.XPATH,
                               ".//div[contains(text(), '{}')]".format(expected_error)).is_present()


class AccountTreePicker(pages.ExtJSTreePicker):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, 'treePicker')

    @property
    def headers(self) -> pages.UITable.Headers:
        # There are no column headers for this picker, so define it as a one column header with the column being 'Name'
        headers = pages.UITable.Headers()
        headers['Name'] = None  # no UIElement
        return headers


class WifiSiteSurveyRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//body")

    @property
    def yes_button(self):
        return pages.UIPageLinkElement('Yes Button', self, page_cls=RoutersPage, strategy=pages.By.EXTJS_ID, locator='yes')


class AddNcpRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//body")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        super().wait_for_region_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def save_button(self):
        return pages.UIPageLinkElement('Save', self, page_cls=RoutersPage, strategy=pages.By.EXTJS_ID, locator='okButton')


class RemoveNcpRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//body")

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        super().wait_for_region_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def yes_button(self):
        return pages.UIPageLinkElement('Yes Button', self, page_cls=RoutersPage, strategy=pages.By.EXTJS_ID, locator='yes')


class RoutersPanelRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH,
                    "//div[starts-with(@id, 'ecm-core-view-devices-Routers') and contains(@class, 'x-panel ')]")

    @property
    def toolbar(self):
        return Toolbar(self)

    @property
    def router_table(self) -> RouterTable:
        return RouterTable(self)


class DeviceConfigSummary(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, 'div.x-window[id^="ecm-core-view-ConfigSummaryWindow-"]')

    def wait_for_region_to_load(self):
        logger.debug("{} waiting to load...".format(self))
        self.spinner.wait_for_spinner()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    @property
    def actual_tab(self):
        return pages.UIRegionLinkElement(
            'actual tab', self, region_cls=ActualDeviceConfig, strategy=pages.By.XPATH, locator=".//span[text()='Actual']")


class ActualDeviceConfig(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'configpanel')]")

    @property
    def actual_config(self):
        configpanel_name = self.parent.ext_finder.execute_js_query(
            'Ext.ComponentQuery.query("tabpanel[cls=\'summaryPanel\']")[0].query("configpanel['
            'itemId=\'actualConfig\']")[0].id ')
        config_element = self.parent.find_element(pages.By.XPATH, ".//div[@id = '{}-innerCt']/pre".format(configpanel_name))
        return config_element.text


class EditDeviceConfigurationDialog(EditConfigurationDialog):
    class CommitChangesDialog(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, 'div.x-message-box[id^="messagebox-"]')

        @property
        def ok_button(self):
            return pages.UIPageLinkElement(
                'ok button', self, RoutersPage, strategy=pages.By.XPATH, locator=".//span[text()='OK']/..")

    @property
    def commit_changes_button(self):
        self.driver.switch_to.default_content()  # TODO: Take this out when iframe stuff is better integrated
        return pages.UIRegionLinkElement(
            'commit changes button',
            self,
            region_cls=self.CommitChangesDialog,
            strategy=pages.By.XPATH,
            locator="//span[text()='Commit Changes']/..")


class ToggleTree(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[starts-with(@id, 'ecm-widget-AccountTreePicker') and contains(@class, 'x-panel ')]")

    def wait_for_region_to_load(self):
        self.spinner.wait_for_spinner()

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def account_or_group_button(self, name: str):

        xpath = "//div[starts-with(@id,'ecm-widget-AccountTreePicker') and " \
                "contains(@id,'header-body')]/../..//span[text()='{}']".format(name)
        return pages.UIButtonElement("select account or group", self, pages.By.XPATH, xpath)


class RoutersPage(dev_pages.DevicesBasePage):
    URL_TEMPLATE = "/ecm.html#devices/routers"
    DEFAULT_TIMEOUT = 30

    @property
    def toggle_tree_button(self):
        return pages.UIRegionLinkElement('toggle tree button', self, ToggleTree, pages.By.CSS_SELECTOR,
                                         "[data-qtip='Toggle Tree']")

    @property
    def toggle_tree(self) -> ToggleTree:
        return ToggleTree(self)

    @property
    def netcloud_os_button(self):
        # TODO: Mock locator for demo.
        return pages.UIButtonElement('netcloud_os_button', self, pages.By.ID, 'firmware')

    @property
    def routers_panel(self):
        return RoutersPanelRegion(self)

    def install_netcloud_engine(self, client_id, lan_id):
        raise NotImplementedError

    def perform_wifi_site_survey(self, device_name: str) -> None:
        """ Performs a wifi site survey on the given device

        Args:
            device_name: The name of the device in ncm to perform the survey on
        """
        self.routers_panel.router_table.device_selection_checkbox(device_name).click()
        self.routers_panel.toolbar.commands_dropdown.click().wifi_site_survey_button.click().yes_button.click()
        self.spinner.wait_for_spinner()

    def get_device_actual_config(self, device_id: str) -> str:
        """
        Get the actual config from the config summary window.

        Args:
            device_id

        Returns:
            String of the device configuration. Can be easily parsed with json.loads()

        """
        is_checkbox_enabled = self.routers_panel.router_table.is_device_selection_checkbox_checked(device_id)
        checkbox = self.routers_panel.router_table.device_selection_checkbox(device_id)
        if not is_checkbox_enabled:
            checkbox.click()

        summary_window = self.routers_panel.toolbar.configuration_dropdown.click().summary_button.click()
        actual_config_window = summary_window.actual_tab.click()
        config_text = actual_config_window.actual_config
        return config_text
