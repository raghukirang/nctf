from nctf.test_lib.ncm.pages.devices.devices_base import DevicesBasePage


class AccessPointsPage(DevicesBasePage):
    URL_TEMPLATE = "/ecm.html#devices/access_points"
