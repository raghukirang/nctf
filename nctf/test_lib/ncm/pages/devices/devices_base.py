from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By
from nctf.test_lib.ncm import pages as ncm_pages
from nctf.test_lib.ncm.pages import devices as dev_pages
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class DevicesBasePage(NCMBasePage):
    @property
    def device_name_breadcrumb(self):
        return pages.UIPageLinkElement(name='Devices level 200 tab',
                                       parent=self,
                                       page_cls=dev_pages.RoutersPage,
                                       strategy=By.ID,
                                       locator='breadcrumb-button-devices-btnEl',
                                       id='{id}')

    @property
    def routers_page_button(self):
        return pages.UIPageLinkElement(
            'Routers', self, page_cls=ncm_pages.RoutersPage, strategy=By.ID, locator='nav-button-routers')

    @property
    def access_points_button(self):
        return pages.UIPageLinkElement(
            'Access Points', self, page_cls=dev_pages.AccessPointsPage, strategy=By.ID, locator='nav-button-access-points')

    @property
    def network_interfaces_button(self):
        return pages.UIPageLinkElement(
            'Network Interfaces',
            self,
            page_cls=dev_pages.NetworkInterfacesPage,
            strategy=By.ID,
            locator='nav-button-network-interfaces')

    @property
    def rogue_ap_button(self):
        return pages.UIPageLinkElement(
            'Rogue AP', self, page_cls=dev_pages.RogueAPPage, strategy=By.ID, locator='nav-button-rogue-ap')
