from .devices_base import DevicesBasePage
from .device_dashboard import DevicesDashBoardPage
from .routers.routers import RoutersPage
from .access_points.access_points import AccessPointsPage
from .network_interfaces.network_interfaces import NetworkInterfacesPage
from .rogue_ap.rogue_ap import RogueAPPage

