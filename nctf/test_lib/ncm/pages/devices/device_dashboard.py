from nctf.test_lib.base import pages
from nctf.test_lib.base.pages import By
from nctf.test_lib.ncm.pages.devices.license import LicenseBasePage
from nctf.test_lib.ncm.pages.common.ncm_base import NCMBasePage


class DevicesDashBoardPage(NCMBasePage):

    URL_TEMPLATE = "/ecm.html#devices/dashboard?id={id}"

    @property
    def device_state(self):
        root_id = self.ext_finder.locate_simple_button_by_extjs_id("deviceState")
        return pages.UIElement('device state', self, By.CSS_SELECTOR, '#{} > .dash-card-desc'.format(root_id))

    @property
    def device_name(self):
        root_id = self.ext_finder.locate_simple_button_by_extjs_id("deviceName")
        return pages.UIElement('device name', self, By.CSS_SELECTOR, '#{} > .dash-card-black'.format(root_id))

    @property
    def device_name_breadcrumb(self):
        return pages.UIElement('device name breadcrumb', self, By.EXTJS_ID, 'viewFilterDashboardSuffix')

    @property
    def device_state_icon(self):
        xpath = '//img[starts-with(@class, "router-")]'
        return pages.UIElement('device state icon', self, By.XPATH, xpath)

    @property
    def license_button(self):
        return pages.UIPageLinkElement(
            'License Tab',
            self,
            page_cls=LicenseBasePage,
            strategy=By.ID,
            locator='nav-button-devicelicense',
            id='{id}')
