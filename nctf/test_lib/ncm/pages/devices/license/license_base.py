from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.devices import DevicesBasePage


class LicenseBasePage(DevicesBasePage):
    URL_TEMPLATE = "/ecm.html#devices/devicelicense?id={id}"

    @property
    def check_availability_button(self):
        return pages.UIRegionLinkElement(
            name='Check Availability',
            parent=self,
            region_cls=self.ChangeLicenseDialog,
            strategy=pages.By.EXTJS_ID,
            locator='checkAvailabilityButton',
            region_parent=self.page)

    @property
    def check_no_availability_button(self):
        return pages.UIRegionLinkElement(
            name='Check Availability (with no licenses available)',
            parent=self,
            region_cls=self.NoAvailableLicensesDialog,
            strategy=pages.By.EXTJS_ID,
            locator='checkAvailabilityButton',
            region_parent=self.page)

    @property
    def license(self):
        return pages.UIElement("Current License",
                               self,
                               pages.By.EXTJS_ID,
                               "licenseNameLabel")

    @property
    def expiration_date(self):
        return pages.UIElement("Expiration Date",
                               self,
                               pages.By.EXTJS_ID,
                               "licenseExpirationLabel")

    @property
    def days_remaining(self):
        return pages.UIElement("Days Remaining for Current License",
                               self,
                               pages.By.EXTJS_ID,
                               "licenseDaysLabel")

    class ChangeLicenseDialog(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.EXTJS_ID, "mainLicenseContainer")

        def wait_for_region_to_load(self):
            super().wait_for_region_to_load()
            self.spinner.wait_for_spinner()
            return self

        @property
        def spinner(self):
            return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

        @property
        def upgrade_button(self):
            return pages.UIPageLinkElement(
                'Upgrade',
                self,
                page_cls=LicenseBasePage,
                strategy=pages.By.EXTJS_ID,
                locator='upgradeButton',
                id='{id}')

        @property
        def downgrade_button(self):
            return pages.UIPageLinkElement(
                'Downgrade',
                self,
                page_cls=LicenseBasePage,
                strategy=pages.By.EXTJS_ID,
                locator='downgradeButton',
                id='{id}')

    class NoAvailableLicensesDialog(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.EXTJS_ID, "buyLicenseContainer")

        @property
        def cancel_button(self):
            return pages.UIPageLinkElement(
                'Cancel',
                self,
                page_cls=LicenseBasePage,
                strategy=pages.By.EXTJS_ID,
                locator='cancelButton')
