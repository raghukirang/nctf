from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.devices.devices_base import DevicesBasePage


class RogueAPTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.EXTJS_ID, "rogue_ap")

    @property
    def refresh_button(self):
        return pages.UIButtonElement(
            'refresh button', self, strategy=pages.By.CSS_SELECTOR, locator='a[data-qtip^="Auto-refresh is"]')


class RogueAPPage(DevicesBasePage):
    URL_TEMPLATE = "/ecm.html#devices/rogue_ap"

    @property
    def rogue_ap_table(self) -> RogueAPTable:
        return RogueAPTable(self)
