from nctf.test_lib.base import pages
from nctf.test_lib.ncm.pages.devices.devices_base import DevicesBasePage


class NetworkInterfacesTable(pages.ExtJSTable):
    ROOT_LOCATOR = (pages.By.XPATH, "//div[contains(@class, 'x-panel')]"
                    "//div[contains(@id, 'ecm-core-view-devices-NetDevices')"
                    " and contains(@class, 'x-grid-with-row-lines')]")

    def refresh(self) -> 'NetworkInterfacesTable':
        return self.parent.toolbar.refresh_button.click().network_interfaces_table


class Toolbar(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "div.x-toolbar")

    def wait_for_region_to_load(self):
        self.refresh_button.wait_for_clickable()

    @property
    def refresh_button(self):
        return pages.UIRegionLinkElement(
            name='refresh_button',
            parent=self,
            region_cls=NetworkInterfacesPanelRegion,
            strategy=pages.By.CSS_SELECTOR,
            locator='#network-interfaces-refresh-button',
            region_parent=self.parent.parent)

    def refresh(self) -> 'NetworkInterfacesPanelRegion':
        return self.refresh_button.click()

    @property
    def commands_dropdown(self):
        return pages.UIRegionLinkElement(
            "commands dropdown",
            self,
            region_cls=self.CmdDropDownRegion,
            strategy=pages.By.EXTJS_ID,
            locator='network-interfaces-command-button',
            region_parent=self.page)

    class CmdDropDownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CSS_SELECTOR, "body")

        #TODO: Implement

        @property
        def manage_modem_fw_button(self):
            return None

        @property
        def puk_unlock_button(self):
            return None


class NetworkInterfacesPanelRegion(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.XPATH,
                    "//div[starts-with(@id, 'ecm-core-view-devices-NetDevices') and contains(@class, 'x-panel ')]")

    @property
    def toolbar(self):
        return Toolbar(self)

    @property
    def network_interfaces_table(self) -> NetworkInterfacesTable:
        return NetworkInterfacesTable(self)


class NetworkInterfacesPage(DevicesBasePage):
    URL_TEMPLATE = "/ecm.html#devices/network_interfaces"
    DEFAULT_TIMEOUT = 15

    @property
    def network_interfaces_panel(self):
        return NetworkInterfacesPanelRegion(self)
