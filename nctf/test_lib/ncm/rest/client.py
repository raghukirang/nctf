import json
import logging

from requests import Response
from requests import Session

import nctf.test_lib.base.rest as rest

logger = logging.getLogger('test_lib.ncm.rest')

ACCOUNTS_ENDPOINT = '/api/v1/accounts'
FIRMWARES_ENDPOINT = '/api/v1/firmwares'
GROUPS_ENDPOINT = '/api/v1/groups'
PRODUCTS_ENDPOINT = '/api/v1/products'
USERS_ENDPOINT = '/api/v1/users'
ROUTERS_ENDPOINT = '/api/v1/routers'
ROLES_ENDPOINT = '/api/v1/roles'
COPY_CONFIG_ENDPOINT = '/api/v1/copy_config'
SECURITYTOKENS_ENDPOINT = '/api/v1/securitytokens'
NET_DEVICES_ENDPOINT = '/api/v1/net_devices/'
SDK_VERSIONS_ENDPOINT = '/api/v1/device_app_versions'
FEATURE_OPT_IN_ENDPOINT = '/api/v1/feature_opt_in'
FEATURE_BINDINGS_ENDPOINT = '/api/v1/featurebindings/?limit=999'
TRIAL_INITIATOR_ENDPOINT = '/api/v1/trial_initiator'
DEVICE_APP_BINDINGS = '/api/v1/device_app_bindings'
DEVICE_APP_VERSIONS = '/api/v1/device_app_versions'
CONFIGURATION_MANAGERS_ENDPOINT = '/api/v1/configuration_managers/'
AUTHORIZATIONS_ENDPOINT = '/api/v1/authorizations'


class AuthenticationError(rest.RESTAPIException):
    """Raised when session authorization attempts fail for any reason."""


class NCMv1RESTClient(rest.RESTClient):
    """NCM REST API client

    QA2 REST API documentation: https://qa2.cradlepointNCM.com/api/v1/?format=json

    """

    def __init__(self, protocol: str, hostname: str, port: int, timeout: float = 30.0, session: Session = None):
        super().__init__(protocol, hostname, port, timeout, session)

        self.accounts = AccountsEndpoint(self, ACCOUNTS_ENDPOINT, enforce_trailing_slash=True)
        self.users = UsersEndpoint(self, USERS_ENDPOINT, enforce_trailing_slash=False)
        self.groups = GroupsEndpoint(self, GROUPS_ENDPOINT, enforce_trailing_slash=True)
        self.routers = RoutersEndpoint(self, ROUTERS_ENDPOINT, enforce_trailing_slash=True)
        self.firmwares = FirmwaresEndpoint(self, FIRMWARES_ENDPOINT, enforce_trailing_slash=True)
        self.products = ProductsEndpoint(self, PRODUCTS_ENDPOINT, enforce_trailing_slash=True)
        self.roles = RolesEndpoint(self, ROLES_ENDPOINT, enforce_trailing_slash=True)
        self.copy_config = CopyConfigEndpoint(self, COPY_CONFIG_ENDPOINT, enforce_trailing_slash=True)
        self.securitytokens = SecurityTokensEndpoint(self, SECURITYTOKENS_ENDPOINT, enforce_trailing_slash=True)
        self.net_devices = NetDevicesEndpoint(self, NET_DEVICES_ENDPOINT, enforce_trailing_slash=True)
        self.sdk_versions = SDKVersionsEndpoint(self, SDK_VERSIONS_ENDPOINT, enforce_trailing_slash=True)
        self.feature_opt_in = FeatureOptInEndpoint(self, FEATURE_OPT_IN_ENDPOINT, enforce_trailing_slash=True)
        self.feature_bindings = FeatureBindingsEndpoint(self, FEATURE_BINDINGS_ENDPOINT, enforce_trailing_slash=False)
        self.trial_initiator = TrialInitiatorEndpoint(self, TRIAL_INITIATOR_ENDPOINT, enforce_trailing_slash=True)
        self.device_apps_bindings = DeviceAppBindingsEndpoint(self, DEVICE_APP_BINDINGS, enforce_trailing_slash=True)
        self.device_app_versions = DeviceAppVersionsEndpoint(self, DEVICE_APP_VERSIONS, enforce_trailing_slash=True)
        self.configuration_managers = ConfigurationManagersEndpoint(
            self, CONFIGURATION_MANAGERS_ENDPOINT, enforce_trailing_slash=True)
        self.authorizations = AuthorizationsEndpoint(self, AUTHORIZATIONS_ENDPOINT, enforce_trailing_slash=True)

    def authenticate(self, username: str, password: str) -> Response:
        """Authenticates this client's session via the NCM Service.

        Args:
            username: Username for the user to authenticate.
            password: Password for the user.

        Raises:
            AuthenticationError: If the username/password combo is not valid.
            UnexpectedStatusError: If a status other than those for successful/invalid login are received.
        """
        logger.info("Authenticating client session with credentials: {}/{}".format(username, password))

        try:
            r = self.post("/login", 302, data={"username": username, "password": password}, allow_redirects=False)
        except rest.RESTAPIException as e:
            raise AuthenticationError from e

        logger.info("Successful authentication for user '{}'".format(username))
        return r

    def authenticated(self) -> bool:
        """Check our cookies to see if we're authenticated."""
        accounts_session_cookie = self.session.cookies.get('accounts_sessionid', default=None)

        # Either a 'cpAccountsJwt' cookie will be present or a 'jwt' depending on the stack.
        cp_accounts_jwt_cookie = self.session.cookies.get('cpAccountsJwt', default=None)
        jwt_cookie = self.session.cookies.get('jwt', default=None)

        return accounts_session_cookie and (jwt_cookie or cp_accounts_jwt_cookie)


class NCMv1Endpoint(rest.RESTEndpoint):
    @rest.checked_status(200)
    def detail(self, resource_id: str, *args, **kwargs) -> Response:
        return self._get(resource_id, *args, **kwargs)

    @rest.checked_status(200)
    def list(self, *args, **kwargs) -> Response:
        return self._get(*args, **kwargs)

    @rest.checked_status(204)
    def delete(self, resource_id: str, *args, **kwargs) -> Response:
        return self._delete(resource_id, *args, **kwargs)


class FirmwaresEndpoint(NCMv1Endpoint):
    pass


class ProductsEndpoint(NCMv1Endpoint):
    pass


class UsersEndpoint(NCMv1Endpoint):
    def user_id(self) -> str:
        """Returns the authenticated client's user id."""
        return self.list().json()['data'][0]['id']


class AccountsEndpoint(NCMv1Endpoint):
    def create(self, name: str, parent_account_id: str, *args, **kwargs) -> Response:
        payload = {"name": name, "account": "{}/{}/".format(ACCOUNTS_ENDPOINT, parent_account_id), "disabled": False}
        return self._post(json=payload, *args, **kwargs)

    def parent_account_id(self) -> str:
        """Returns the authenticated client's parent account id."""
        return self.list().json()['data'][0]['id']


class GroupsEndpoint(NCMv1Endpoint):
    @rest.checked_status(202)
    def update(self, group_id: str, *args, name: str = None, firmware_id: str = None, **kwargs):
        payload = kwargs.pop('payload', dict())
        if name:
            payload["name"] = name
        if firmware_id:
            payload["target_firmware"] = "{}/{}/".format(FIRMWARES_ENDPOINT, firmware_id)
        return self._put(group_id, *args, json=payload, **kwargs)

    @rest.checked_status(201)
    def create(self, name: str, account_id: str, product_id: str, firmware_id: str, *args, **kwargs) -> Response:
        payload = {
            "name": name,
            "account": "{}/{}/".format(ACCOUNTS_ENDPOINT, account_id),
            "product": "{}/{}/".format(PRODUCTS_ENDPOINT, product_id),
            "target_firmware": "{}/{}/".format(FIRMWARES_ENDPOINT, firmware_id),
            "configuration": [{}, []],
            "has_individually_configured_routers": False,
            "commit_settings": False,
            "device_app_bindings": None
        }
        return self._post(*args, json=payload, **kwargs)

    @rest.checked_status(200)
    def configuration(self, group_id: int, *args, **kwargs) -> Response:
        """Return the current group configuration."""
        return self._get('{}/configuration/'.format(group_id), *args, **kwargs)

    @rest.checked_status(200)
    def statistics(self, group_id: int, *args, **kwargs) -> Response:
        return self._get('{}/statistics/'.format(group_id), *args, **kwargs)


class ConfigurationManagersEndpoint(NCMv1Endpoint):
    @rest.checked_status(200)
    def get_id_by_router(self, router_id: str, *args, **kwargs) -> str:
        """Get the configuration_managers id for the given router."""
        params = {"router": router_id, "expand": "configuration"}
        resource_uri = self._get(*args, params=params, **kwargs).json()["data"][0]["resource_uri"]
        manager_id = resource_uri.split('/')[-2]
        return manager_id

    @rest.checked_status(200)
    def patch_configuration(self, configuration_manager_id: str, configuration: dict, *args, **kwargs) -> Response:
        "Update the provided configuration managers config to configuration (json)"
        return self._patch(configuration_manager_id, *args, json=configuration, **kwargs)

    @rest.checked_status(202)
    def resume_updates(self, configuration_manager_id: str, parent_account_id: str, *args, **kwargs) -> Response:
        params = {'parent_account': parent_account_id}
        payload = {'id': configuration_manager_id, 'suspended': False}
        return self._put(configuration_manager_id, *args, params=params, json=payload, **kwargs)


class RoutersEndpoint(NCMv1Endpoint):
    @rest.checked_status(202)
    def move_to_group(self, router_id: str, group_id: str, *args, **kwargs) -> Response:
        """Move the specified router into the specified group."""

        payload = {"id": router_id, "group": "{}/{}/".format(GROUPS_ENDPOINT, group_id)}
        return self._put(router_id, *args, json=payload, **kwargs)

    @rest.checked_status(202)
    def remove_from_group(self, router_id: str, account_id: str, *args, **kwargs) -> Response:
        """Move the specified router out of a group (make it "un-grouped")."""
        logger.info('un-grouping router {}'.format(router_id))

        payload = {"account": "/api/v1/accounts/{}/".format(account_id), "group": None, "id": router_id}
        return self._put(router_id, *args, json=payload, **kwargs)

    @rest.checked_status(204)
    def unregister(self, router_id: str, *args, **kwargs) -> Response:
        """Unregister the specified device."""

        data = {"id": "{}".format(router_id)}

        return self._delete(resource_id=router_id, json=data, *args, **kwargs)

    @rest.checked_status(200)
    def reboot_required(self, router_id: str, *args, **kwargs) -> bool:
        """Determine if the device requires a reboot."""

        return self._get(router_id, *args, **kwargs).json()['data']['reboot_required']

    @rest.checked_status(200)
    def config_status(self, router_id: str, *args, **kwargs) -> str:
        """Return the current config_status for the requested device."""

        return self._get(router_id, *args, **kwargs).json()['data']['config_status']

    @rest.checked_status(200)
    def state(self, router_id: str, *args, **kwargs) -> str:
        """Return the current state for the requested device."""

        return self._get(router_id, *args, **kwargs).json()['data']['state']

    @rest.checked_status(200)
    def actual_firmware(self, router_id: str, *args, **kwargs) -> str:
        """Return the actual firmware uri for the requested device."""

        return self._get(router_id, *args, **kwargs).json()['data']['actual_firmware']

    @rest.checked_status(200)
    def actual_firmware_version(self, router_id: str, *args, **kwargs) -> str:
        """Return the actual firmware name for the requested device."""

        params = {'expand': 'actual_firmware'}
        return self._get(router_id, *args, params=params, **kwargs).json()['data']['actual_firmware']['version']

    @rest.checked_status(200)
    def router_id(self, *args, **kwargs) -> str:
        """Returns the router's id

        NOTE:
            THIS WILL ONLY WORK FOR A BRAND NEW ACCOUNT WITH ONE SINGLE ROUTER
        """
        logger.warning('THIS METHOD WILL ONLY WORK FOR A BRAND NEW ACCOUNT WITH A SINGLE ROUTER')
        return self.list(*args, **kwargs).json()['data'][0]['id']

    @rest.checked_status(200)
    def name(self, router_id: str, *args, **kwargs) -> str:
        """Return the NCM name of the requested device."""

        return self._get(router_id, *args, **kwargs).json()['data']['name']

    @rest.checked_status(200)
    def product(self, router_id: str, *args, **kwargs) -> str:
        """Return the product uri for the requested device"""

        return self._get(router_id, *args, **kwargs).json()['data']['product']

    @rest.checked_status(200)
    def product_name(self, router_id: str, *args, **kwargs) -> str:
        """Retrun the product name for the requested device"""

        params = {'expand': 'product'}
        return self._get(router_id, *args, params=params, **kwargs).json()['data']['product']['name']

    @rest.checked_status(200)
    def config_status(self, router_id: str, *args, **kwargs) -> str:
        """Return the config status for the requested device"""

        return self._get(router_id, *args, **kwargs).json()['data']['config_status']

    @rest.checked_status(202)
    def change_name_to_id(self, router_id: str, *args, **kwargs) -> Response:
        """Change the router to name to be the same as the router id"""

        data = {'id': router_id, 'name': router_id}

        return self._put(resource_id=router_id, json=data, *args, **kwargs)

    @rest.checked_status(200)
    def dhcp_leases(self, router_id: str, *args, **kwargs) -> list:
        """Return the dhcp_leases for the requested device"""

        params = {'expand': 'lans'}
        response = self._get(router_id, *args, params=params, **kwargs).json()
        dhcp_leases = []
        for lan in response['data']['lans']:
            response = self.client.get(lan['dhcp_leases'], expected_status_code=200).json()
            for lease in response['data']:
                dhcp_leases.append(lease)
        return dhcp_leases

    @rest.checked_status(200)
    def group(self, router_id: str, *args, **kwargs) -> str:
        """Return the group uri for the requested device"""

        return self._get(router_id, *args, **kwargs).json()['data']['group']

    @rest.checked_status(200)
    def group_name(self, router_id: str, *args, **kwargs) -> str:
        params = {'expand': 'group'}
        response = self._get(router_id, *args, params=params, **kwargs).json()
        if response['data']['group'] and 'name' in response['data']['group']:
            return response['data']['group']['name']
        else:
            return response['data']['group']

    @rest.checked_status(200)
    def account(self, router_id: str, *args, **kwargs) -> str:
        """Return the account uri for the requested device"""

        return self._get(router_id, *args, **kwargs).json()['data']['account']

    @rest.checked_status(200)
    def account_name(self, router_id: str, *args, **kwargs) -> str:
        """Return the account name for the requested device"""

        params = {'expand': 'account'}
        return self._get(router_id, *args, params=params, **kwargs).json()['data']['account']['name']

    @rest.checked_status(200)
    def mac(self, router_id: str, *args, **kwargs) -> str:
        """Returns the mac address for the requested device"""
        return self._get(router_id, *args, **kwargs).json()['data']['mac']

    @rest.checked_status(200)
    def configuration_manager(self, router_id: str, *args, **kwargs) -> str:
        """Return the configuration manager uri for the requested device"""
        return self._get(router_id, *args, **kwargs).json()['data']['configuration_manager']


class RolesEndpoint(NCMv1Endpoint):
    pass


class CopyConfigEndpoint(NCMv1Endpoint):
    @rest.checked_status(201)
    def copy_device_config_to_group(self, router_id: int, group_id: int, *args, **kwargs) -> Response:
        payload = {
            "source_router": "/api/v1/routers/{}/".format(router_id),
            "destination_groups": ["/api/v1/groups/{}/".format(group_id)]
        }
        return self._post(*args, json=payload, **kwargs)

    @rest.checked_status(201)
    def copy_group_config_to_group(self, source_group_id: int, destination_group_id: int, *args, **kwargs) -> Response:
        payload = {
            "source_group": "/api/v1/groups/{}/".format(source_group_id),
            "destination_groups": ["/api/v1/groups/{}/".format(destination_group_id)]
        }
        return self._post(*args, json=payload, **kwargs)


class SecurityTokensEndpoint(NCMv1Endpoint):
    @rest.checked_status(201)
    def create(self, account_id: str, label: str, *args, **kwargs) -> Response:
        payload = {'account': f'{ACCOUNTS_ENDPOINT}/{account_id}/', 'label': label}
        return self._post(json=payload, *args, **kwargs)


class NetDevicesEndpoint(NCMv1Endpoint):
    @rest.checked_status(201)
    def find_router_netdevice_info(self, router_id: int, netdevice_type: str = None, *args, **kwargs) -> Response:
        params = {"router": router_id}
        if netdevice_type:
            params["type"] = netdevice_type

        return self._get(*args, params=params, **kwargs)


class SDKVersionsEndpoint(NCMv1Endpoint):
    pass


class FeatureOptInEndpoint(NCMv1Endpoint):
    """Opts the user into the feature"""

    @rest.checked_status(202)
    def opt_in(self, opt_in_id: str, *args, **kwargs) -> Response:
        payload = {"opted_in": True, "id": "{}".format(opt_in_id)}
        return self._put(resource_id=opt_in_id, *args, json=payload, **kwargs)


class FeatureBindingsEndpoint(NCMv1Endpoint):
    @rest.checked_status(200)
    def feature_binding_expands(self, feature_name: str, *args, **kwargs) -> Response:
        """Returns the expanded feature binding"""
        return self._get(*args, params={"expand": "feature", "limit": 99, "feature.name": f"{feature_name}"}, **kwargs)


class TrialInitiatorEndpoint(NCMv1Endpoint):
    @rest.checked_status(201)
    def trial_initiate_sdk(self, parent_account_id: int, *args, **kwargs):
        """Initiates the SDK feature"""
        payload = {
            "feature": "/api/v1/features/36/",
            "product_code": "RTR-SDK-1YR",
            "account": f"/api/v1/accounts/{parent_account_id}/"
        }
        return self._post(*args, json=payload, **kwargs)

    @rest.checked_status(201)
    def trial_initiate_cvr(self, parent_account_id: int, *args, **kwargs):
        """Initiates the CVR Demo"""
        payload = {
            "feature": "/api/v1/features/105/",
            "product_code": "CVR-DEMO",
            "account": f"/api/v1/accounts/{parent_account_id}/"
        }
        return self._post(*args, json=payload, **kwargs)


class DeviceAppBindingsEndpoint(NCMv1Endpoint):
    @rest.checked_status(201)
    def add_app_to_group(self, account_id: int, app_id: int, group_id: int, *args, **kwargs) -> Response:
        """Add a router app to a router

        *Note* An app must be uploaded to NCM before this will work
        """
        payload = {
            "account": f"/api/v1/account/{account_id}",
            "app_version": f"/api/v1/device_app_versions/{app_id}",
            "group": f"/api/v1/groups/{group_id}",
            "resource_uri": ""
        }
        return self._post(*args, json=payload, **kwargs)


class DeviceAppVersionsEndpoint(NCMv1Endpoint):
    @rest.checked_status(204)
    def delete_app_from_group(self, account_id: int, app_id: int, group_id: int, app_bindings_id: int, *args,
                              **kwargs) -> Response:
        """Deletes an app from a group"""
        payload = {
            "account": f"/api/v1/account/{account_id}",
            "app_version": f"/api/v1/device_app_versions/{app_id}",
            "group": f"/api/v1/groups/{group_id},",
            "id": f"{app_bindings_id}",
            "resource_uri": f"/api/v1/device_app_bindings/{app_bindings_id}"
        }
        return self._delete(*args, json=payload, **kwargs)


class AuthorizationsEndpoint(NCMv1Endpoint):
    @rest.checked_status(201)
    def create(self,
               account_id: str,
               role_id: str,
               security_token_id: str,
               active: bool = True,
               cascade: bool = True,
               *args,
               **kwargs) -> Response:
        payload = {
            'account': f'{ACCOUNTS_ENDPOINT}/{account_id}/',
            'role': f'{ROLES_ENDPOINT}/{role_id}/',
            'securitytoken': f'{SECURITYTOKENS_ENDPOINT}/{security_token_id}/',
            'active': active,
            'cascade': cascade
        }
        return self._post(json=payload, *args, **kwargs)
