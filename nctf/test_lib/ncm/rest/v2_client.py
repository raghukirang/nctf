import logging

from requests import Response
from requests import Session

from faker import Faker
import nctf.test_lib.base.rest as rest
from nctf.test_lib.ncm.rest.client import NCMv1RESTClient

logger = logging.getLogger('test_lib.ncm.rest')

ACCOUNTS_ENDPOINT = '/api/v2/accounts'
ALERTS_ENDPOINT = '/api/v2/alerts'
ACTIVITY_LOGS_ENDPOINT = '/api/v2/activity_logs'
CONFIGURATION_MANAGERS_ENDPOINT = '/api/v2/configuration_managers'
DEVICE_APP_BINDINGS_ENDPOINT = '/api/v2/device_app_bindings'
DEVICE_APP_STATES_ENDPOINT = '/api/v2/device_app_states'
DEVICE_APP_VERSIONS_ENDPOINT = '/api/v2/device_app_versions'
DEVICE_APPS_ENDPOINT = '/api/v2/device_apps'
FIRMWARES_ENDPOINT = '/api/v2/firmwares'
GROUPS_ENDPOINT = '/api/v2/groups'
LOCATIONS_ENDPOINT = '/api/v2/locations'
NET_DEVICE_METRICS_ENDPOINT = '/api/v2/net_device_metrics'
NET_DEVICE_SIGNAL_SAMPLES_ENDPOINT = '/api/v2/net_device_signal_samples'
NET_DEVICE_USAGE_SAMPLES_ENDPOINT = '/api/v2/net_device_usage_samples'
NET_DEVICES_ENDPOINT = '/api/v2/net_devices'
PRODUCTS_ENDPOINT = '/api/v2/products'
REBOOT_ACTIVITY_ENDPOINT = '/api/v2/reboot_activity'
ROUTER_ALERTS_ENDPOINT = '/api/v2/router_alerts'
ROUTER_LOGS_ENDPOINT = '/api/v2/router_logs'
ROUTER_STATE_SAMPLES_ENDPOINT = '/api/v2/router_state_samples'
ROUTER_STREAM_USAGE_SAMPLES_ENDPOINT = '/api/v2/router_stream_usage_samples'
ROUTERS_ENDPOINT = '/api/v2/routers'
SPEED_TEST_ENDPOINT = '/api/v2/speed_test'


class NCMv2RESTClient(rest.RESTClient):
    """NCM V2 REST API client"""

    def __init__(self,
                 protocol: str,
                 hostname: str,
                 port: int,
                 x_cp_api_id: str,
                 x_cp_api_key: str,
                 timeout: float = 30.0,
                 session: Session = None):
        super().__init__(protocol, hostname, port, timeout, session)
        self.x_cp_api_id = x_cp_api_id
        self.x_cp_api_key = x_cp_api_key

        self.accounts = AccountsEndpoint(self, ACCOUNTS_ENDPOINT, enforce_trailing_slash=True)
        self.alerts = AlertsEndpoint(self, ALERTS_ENDPOINT, enforce_trailing_slash=True)
        self.activity_logs = ActivityLogsEndpoint(self, ACTIVITY_LOGS_ENDPOINT, enforce_trailing_slash=True)
        self.configuration_managers = ConfigurationManagersEndpoint(
            self, CONFIGURATION_MANAGERS_ENDPOINT, enforce_trailing_slash=True)
        self.device_app_bindings = DeviceAppBindingsEndpoint(self, DEVICE_APP_BINDINGS_ENDPOINT, enforce_trailing_slash=True)
        self.device_app_states = DeviceAppStatesEndpoint(self, DEVICE_APP_STATES_ENDPOINT, enforce_trailing_slash=True)
        self.device_app_versions = DeviceAppVersionsEndpoint(self, DEVICE_APP_VERSIONS_ENDPOINT, enforce_trailing_slash=True)
        self.device_apps = DeviceAppsEndpoint(self, DEVICE_APPS_ENDPOINT, enforce_trailing_slash=True)
        self.firmwares = FirmwaresEndpoint(self, FIRMWARES_ENDPOINT, enforce_trailing_slash=True)
        self.groups = GroupsEndpoint(self, GROUPS_ENDPOINT, enforce_trailing_slash=True)
        self.locations = LocationsEndpoint(self, LOCATIONS_ENDPOINT, enforce_trailing_slash=True)
        self.net_device_metrics = NetDeviceMetricsEndpoint(self, NET_DEVICE_METRICS_ENDPOINT, enforce_trailing_slash=True)
        self.net_device_signal_samples = NetDeviceSignalSamplesEndpoint(
            self, NET_DEVICE_SIGNAL_SAMPLES_ENDPOINT, enforce_trailing_slash=True)
        self.net_device_usage_samples = NetDeviceUsageSamplesEndpoint(
            self, NET_DEVICE_USAGE_SAMPLES_ENDPOINT, enforce_trailing_slash=True)
        self.net_devices = NetDevicesEndpoint(self, NET_DEVICES_ENDPOINT, enforce_trailing_slash=True)
        self.products = ProductsEndpoint(self, PRODUCTS_ENDPOINT, enforce_trailing_slash=True)
        self.reboot_activity = RebootActivityEndpoint(self, REBOOT_ACTIVITY_ENDPOINT, enforce_trailing_slash=True)
        self.router_alerts = RouterAlertsEndpoint(self, ROUTER_ALERTS_ENDPOINT, enforce_trailing_slash=True)
        self.router_logs = RouterLogsEndpoint(self, ROUTER_LOGS_ENDPOINT, enforce_trailing_slash=True)
        self.router_state_samples = RouterStateSamplesEndpoint(
            self, ROUTER_STATE_SAMPLES_ENDPOINT, enforce_trailing_slash=True)
        self.router_stream_usage_samples = RouterStreamUsageSamplesEndpoint(
            self, ROUTER_STREAM_USAGE_SAMPLES_ENDPOINT, enforce_trailing_slash=True)
        self.routers = RoutersEndpoint(self, ROUTERS_ENDPOINT, enforce_trailing_slash=True)
        self.speed_test = SpeedTestEndpoint(self, SPEED_TEST_ENDPOINT, enforce_trailing_slash=True)

    def authenticate(self, ncm_v1_rest_client: NCMv1RESTClient, role: str = 'admin') -> Response:
        """Role choices: read_only, full_access, admin"""
        account = ncm_v1_rest_client.accounts.list().json()['data'][0]
        label = f'NCTF_TEST_{Faker().pystr(min_chars=6, max_chars=8)}'
        logger.info(f'NCM v2 credential label: {label}')
        response = ncm_v1_rest_client.securitytokens.create(account_id=account['id'], label=label).json()
        ecm_api_id = response['data']['api_token_id']
        ecm_api_key = response['data']['api_token_key']
        token_id = response['data']['id']
        response = ncm_v1_rest_client.roles.list(params={'name': role}).json()
        role_id = response['data'][0]['id']
        response = ncm_v1_rest_client.authorizations.create(
            account_id=account['id'], role_id=role_id, security_token_id=token_id)
        self.session.headers.update({
            "X-CP-API-ID": self.x_cp_api_id,
            "X-CP-API-KEY": self.x_cp_api_key,
            "X-ECM-API-ID": ecm_api_id,
            "X-ECM-API-KEY": ecm_api_key
        })
        return response

    def authenticated(self) -> bool:
        cp_api_id = self.session.headers.get('X-CP-API-ID')
        cp_api_key = self.session.headers.get('X-CP-API-KEY')
        ecm_api_id = self.session.headers.get('X-ECM-API-ID')
        ecm_api_key = self.session.headers.get('X-ECM-API-KEY')
        if cp_api_id and cp_api_key and ecm_api_id and ecm_api_key:
            return True
        else:
            return False


class NCMv2Endpoint(rest.RESTEndpoint):
    @rest.checked_status(200)
    def list(self, *args, **kwargs) -> Response:
        return self._get(*args, **kwargs)


class AccountsEndpoint(NCMv2Endpoint):
    @rest.checked_status(201)
    def create(self, name: str, parent_account_id: str, *args, **kwargs) -> Response:
        payload = {
            "name":
            name,
            "account":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}/{ACCOUNTS_ENDPOINT}/{parent_account_id}/",
            "is_disabled":
            False
        }
        return self._post(json=payload, *args, **kwargs)

    @rest.checked_status(202)
    def update(self, account_id: str, *args, name: str = None, is_disabled: bool = None, **kwargs) -> Response:
        payload = dict()
        if name:
            payload["name"] = name
        if is_disabled:
            payload['is_disabled'] = is_disabled
        return self._put(account_id, *args, json=payload, **kwargs)

    @rest.checked_status(204)
    def delete(self, account_id: str, *args, **kwargs) -> Response:
        return self._delete(account_id, *args, **kwargs)


class AlertsEndpoint(NCMv2Endpoint):
    pass


class ActivityLogsEndpoint(NCMv2Endpoint):
    pass


class ConfigurationManagersEndpoint(NCMv2Endpoint):
    @rest.checked_status(202)
    def update(self, configuration_managers_id: str, *args, **kwargs) -> Response:
        return self._put(configuration_managers_id, *args, **kwargs)


class DeviceAppBindingsEndpoint(NCMv2Endpoint):
    @rest.checked_status(201)
    def create(self, account_id: int, app_id: int, group_id: int, *args, **kwargs) -> Response:
        """Add a router app to a router

        *Note* An app must be uploaded to NCM before this will work
        """
        payload = {
            "account":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{ACCOUNTS_ENDPOINT}/{account_id}/",
            "app_version":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{DEVICE_APP_VERSIONS_ENDPOINT}/{app_id}/",
            "group":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{GROUPS_ENDPOINT}/{group_id}/"
        }
        return self._post(*args, json=payload, **kwargs)

    @rest.checked_status(204)
    def delete(self, device_app_bindings_id: str, *args, **kwargs) -> Response:
        return self._delete(device_app_bindings_id, *args, **kwargs)


class DeviceAppStatesEndpoint(NCMv2Endpoint):
    pass


class DeviceAppVersionsEndpoint(NCMv2Endpoint):
    @rest.checked_status(204)
    def delete(self, device_app_versions_id: str, *args, **kwargs) -> Response:
        return self._delete(device_app_versions_id, *args, **kwargs)


class DeviceAppsEndpoint(NCMv2Endpoint):
    @rest.checked_status(204)
    def delete(self, device_apps_id: str, *args, **kwargs) -> Response:
        return self._delete(device_apps_id, *args, **kwargs)


class FirmwaresEndpoint(NCMv2Endpoint):
    pass


class GroupsEndpoint(NCMv2Endpoint):
    @rest.checked_status(201)
    def create(self, name: str, account_id: str, product_id: str, firmware_id: str, *args, **kwargs) -> Response:
        payload = {
            "name":
            name,
            "account":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{ACCOUNTS_ENDPOINT}/{account_id}/",
            "product":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{PRODUCTS_ENDPOINT}/{product_id}/",
            "target_firmware":
            f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{FIRMWARES_ENDPOINT}/{firmware_id}/",
            "configuration": [{}, []]
        }
        return self._post(*args, json=payload, **kwargs)

    @rest.checked_status(202)
    def update(self, group_id: str, *args, name: str = None, firmware_id: str = None, **kwargs) -> Response:
        payload = dict()
        if name:
            payload["name"] = name
        if firmware_id:
            payload[
                "target_firmware"] = f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{FIRMWARES_ENDPOINT}/{firmware_id}/"
        return self._put(group_id, *args, json=payload, **kwargs)

    @rest.checked_status(204)
    def delete(self, group_id: str, *args, **kwargs) -> Response:
        return self._delete(group_id, *args, **kwargs)


class LocationsEndpoint(NCMv2Endpoint):
    @rest.checked_status(201)
    def create(self, account_id: str, accuracy: int, latitude: int, longitude: int, method: str, router_id: str, *args,
               **kwargs) -> Response:
        payload = {
            "account": f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{ACCOUNTS_ENDPOINT}/{account_id}/",
            "accuracy": accuracy,
            "latitude": latitude,
            "longitude": longitude,
            "method": method,
            "router": f"{self.client.protocol}://{self.client.hostname}:{self.client.port}{ROUTERS_ENDPOINT}/{router_id}/",
        }
        return self._post(*args, json=payload, **kwargs)

    @rest.checked_status(202)
    def update(self,
               location_id: str,
               *args,
               accuracy: int = None,
               latitude: int = None,
               longitude: int = None,
               method: str = None,
               **kwargs) -> Response:
        payload = dict()
        if accuracy:
            payload['accuracy'] = accuracy
        if latitude:
            payload['latitude'] = latitude
        if longitude:
            payload['longitude'] = longitude
        if method:
            payload['method'] = method
        return self._put(location_id, *args, json=payload, **kwargs)

    @rest.checked_status(204)
    def delete(self, location_id: str, *args, **kwargs) -> Response:
        return self._delete(location_id, *args, **kwargs)


class NetDeviceMetricsEndpoint(NCMv2Endpoint):
    pass


class NetDeviceSignalSamplesEndpoint(NCMv2Endpoint):
    pass


class NetDeviceUsageSamplesEndpoint(NCMv2Endpoint):
    pass


class NetDevicesEndpoint(NCMv2Endpoint):
    pass


class ProductsEndpoint(NCMv2Endpoint):
    pass


class RebootActivityEndpoint(rest.RESTEndpoint):
    @rest.checked_status(202)
    def create(self, router_id: str = None, group_id: str = None, *args, **kwargs) -> Response:
        payload = dict()
        if group_id and router_id:
            raise ValueError('Cannot specify both a group and a router, please choose one')

        if router_id:
            payload['router'] = f'{ROUTERS_ENDPOINT}/{router_id}/'
        elif group_id:
            payload['group'] = f'{GROUPS_ENDPOINT}/{group_id}/'

        if not router_id and not group_id:
            raise ValueError('Must specify a group or a router to reboot')

        return self._post(*args, json=payload, **kwargs)


class RouterAlertsEndpoint(NCMv2Endpoint):
    pass


class RouterLogsEndpoint(NCMv2Endpoint):
    pass


class RouterStateSamplesEndpoint(NCMv2Endpoint):
    pass


class RouterStreamUsageSamplesEndpoint(NCMv2Endpoint):
    pass


class RoutersEndpoint(NCMv2Endpoint):
    @rest.checked_status(202)
    def update(self, router_id: str, *args, **kwargs) -> Response:
        return self._put(router_id, *args, **kwargs)
        # TODO probably should fill this out with actual values but I don't want to right now so

    @rest.checked_status(204)
    def delete(self, router_id: str, *args, **kwargs) -> Response:
        return self._delete(router_id, *args, **kwargs)


class SpeedTestEndpoint(rest.RESTEndpoint):
    # TODO POST/create later because I don't want to

    @rest.checked_status(200)
    def detail(self, speed_test_id: str, *args, **kwargs) -> Response:
        return self._get(speed_test_id, *args, **kwargs)

    @rest.checked_status(204)
    def delete(self, speed_test_id: str, *args, **kwargs) -> Response:
        return self._delete(speed_test_id, *args, **kwargs)
