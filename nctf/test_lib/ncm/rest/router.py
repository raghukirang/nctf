import logging
import typing

import requests

from nctf.libs.common_library import wait_for_with_interval
from nctf.test_lib.ncm.rest.client import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTClient

logger = logging.getLogger("ncm.router")


class NCMRouterException(Exception):
    pass


class NCMRouter(object):
    def __init__(self, ncm_rest_client: NCMv1RESTClient, ncm_router_id: str):
        self.ncm_rest_client = ncm_rest_client
        self.router_id = ncm_router_id
        logger.info("WIP: created NCMRouter object")

    def get_router_record(self) -> typing.Union[requests.Response, None]:
        return self.ncm_rest_client.routers.detail(resource_id=self.router_id)

    @property
    def name(self) -> str:
        return self.ncm_rest_client.routers.name(router_id=self.router_id)

    @property
    def group_name(self) -> typing.Union[str, None]:
        return self.ncm_rest_client.routers.group_name(router_id=self.router_id)

    @property
    def group_id(self) -> typing.Union[str, None]:
        group = self.ncm_rest_client.routers.group(router_id=self.router_id)
        if group:
            return group.split('/')[-2]
        else:
            return None

    @property
    def state(self) -> str:
        return self.ncm_rest_client.routers.state(router_id=self.router_id)

    @property
    def product_name(self) -> str:
        return self.ncm_rest_client.routers.product_name(router_id=self.router_id)

    @property
    def product_id(self) -> str:
        product = self.ncm_rest_client.routers.product(router_id=self.router_id)
        return product.split('/')[-2]

    @property
    def config_status(self) -> str:
        return self.ncm_rest_client.routers.config_status(router_id=self.router_id)

    @property
    def reboot_required(self) -> bool:
        return self.ncm_rest_client.routers.reboot_required(router_id=self.router_id)

    @property
    def firmware_version(self) -> str:
        return self.ncm_rest_client.routers.actual_firmware_version(router_id=self.router_id)

    @property
    def account_name(self) -> str:
        return self.ncm_rest_client.routers.account_name(router_id=self.router_id)

    @property
    def account_id(self) -> str:
        account = self.ncm_rest_client.routers.account(router_id=self.router_id)
        return account.split('/')[-2]

    @property
    def actual_firmware_id(self) -> str:
        actual_firmware = self.ncm_rest_client.routers.actual_firmware(router_id=self.router_id)
        return actual_firmware.split('/')[-2]

    @property
    def mac(self) -> str:
        mac = self.ncm_rest_client.routers.mac(router_id=self.router_id)
        return mac

    @property
    def dhcp_leases(self) -> list:
        leases = self.ncm_rest_client.routers.dhcp_leases(router_id=self.router_id)
        return leases

    @property
    def configuration_manager_id(self) -> str:
        configuration_manager = self.ncm_rest_client.routers.configuration_manager(router_id=self.router_id)
        return configuration_manager.split('/')[-2]

    def move_to_group(self, group_id: str):
        self.ncm_rest_client.routers.move_to_group(router_id=self.router_id, group_id=group_id)

    def remove_from_group(self):
        if self.group_id:
            self.ncm_rest_client.routers.remove_from_group(router_id=self.router_id, account_id=self.account_id)
        else:
            raise NCMRouterException(f"{self} is not in a group")

    def unregister(self):
        self.ncm_rest_client.routers.unregister(router_id=self.router_id)

    def change_name_to_id(self):
        self.ncm_rest_client.routers.change_name_to_id(router_id=self.router_id)

    def resume_config_updates(self):
        self.ncm_rest_client.configuration_managers.resume_updates()

    def wait_for_config_sync(self, timeout: int = 120) -> None:
        """Waits for the ECM router config status to become 'synched'

        Args:
            timeout: a timeout in seconds, default is 120 seconds.
        """
        wait_for_with_interval(timeout, 1, 'synched', lambda: self.config_status)


if __name__ == '__main__':
    client = NetCloudRESTClient('https', '', 443, 'https', '', 443)
    client.authenticate('', '')
    test_router = NCMRouter(client.ncm, '')
    test = test_router.account_id
    print(test_router)
