"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.base.pages import UIPageLinkElement as DynamicPageLink
from nctf.test_lib.portal import pages


class ResourcesDropdownMenu(base_pages.UIRegion):
    """POM for the Menubar -> Resources Dropdown."""
    ROOT_LOCATOR = (By.XPATH, '//*[@id="navigationMenu"]/li[3]/div/div[2]')

    @property
    def menu_item_sales_tools(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='Resources->SalesPage',
            parent=self,
            page_cls=pages.SalesPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Sales Tools"]')

    @property
    def menu_item_marketing_tools(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='Resources->MarketingPage',
            parent=self,
            page_cls=pages.MarketingPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Marketing Tools"]')

    @property
    def menu_item_partner_program(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='Resources->Partner Program',
            parent=self,
            page_cls=pages.PartnerPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Partner Program & Information"]')

    @property
    def menu_item_policy_documentation(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='Resources->Policy Documentation',
            parent=self,
            page_cls=pages.PolicyPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Policy Documentation & Notification"]')

    @property
    def menu_item_communication_archive(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='MyAccount->Communication Archive',
            parent=self,
            page_cls=pages.CummunicationPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Communication Archive"]')
