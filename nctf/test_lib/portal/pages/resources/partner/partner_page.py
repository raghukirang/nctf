"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class PartnerPage(PortalBasePage):
    """POM for Deal Reg Page"""
    URL_TEMPLATE = 'article/Partner-Program-Information'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.partner_label.wait_for_displayed()
        return self

    @property
    def partner_label(self) -> base_pages.UIElement:
        return base_pages.UIElement(
            name='MarketingTools',
            parent=self,
            strategy=By.CSS_SELECTOR,
            locator='.article-head.selfServiceArticleHeaderDetail')
