"""POM framework for the Partner Portal"""

from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal import pages

# class Drivers():
#     """Trmp class to control browser under test"""
#     if False:
#         driver = Firefox()
#     else:
#         driver = Chrome()


class LoginFormRegion(base_pages.UIRegion):
    """All login related elements."""
    ROOT_LOCATOR = (By.CLASS_NAME, 'salesforceIdentityLoginForm2')

    @property
    def username_field(self) -> base_pages.UIElement:
        return base_pages.UIElement('username_field', self, By.CSS_SELECTOR, '#sfdc_username_container .input')

    @property
    def password_field(self) -> base_pages.UIElement:
        return base_pages.UIElement('password_field', self, By.CSS_SELECTOR, '#sfdc_password_container .input')

    @property
    def login_button(self) -> base_pages.UIPageLinkElement:
        return base_pages.UIElement(name='login_button', parent=self, strategy=By.CLASS_NAME, locator="loginButton")

    def login(self, username: str, password: str) -> None:
        """
        Do the entry of text for username and password and click the login button.

        Args:
            username: a string representing the name of the user to login.
            password: a string representing the password fo the user to login.

        """
        self.username_field.wait_for_clickable(10)
        self.username_field.send_keys(username)
        self.password_field.wait_for_clickable(10)
        self.password_field.send_keys(password)
        self.login_button.click()


class PortalLoginPage(base_pages.UIPage):
    """Overall container for the Portal login page."""
    WEB_SERVICE = 'partnerPortal'
    URL_TEMPLATE = "login/?language=en_US"

    @property
    def login_form(self):
        return LoginFormRegion(self)
        # alternatively we could have done the following construction instead of using _root_locator in LoginFormRegion
        # return LoginFormRegion('LoginFormRegion', self, strategy=By.CLASS_NAME, locator='cp-netcloud-form' )

    def login(self, portal_user_with_valid_creds: pages.PortalActor) -> 'pages.PortalHomePage':
        """Login a valid Portal user.

        This method assumes the user has been successfully created by the test and has already completed the initial
        log-in email and reset their password

        Args:
            portal_user_with_valid_creds: a portal user with valid credentials.
        Returns:
            The Portal Home page.
        """

        self.login_form.login(portal_user_with_valid_creds.username, portal_user_with_valid_creds.password)
        page = pages.PortalHomePage(
            driver=self.driver, base_url='https://staging-cradlepointconnect.cs71.force.com/Connect/s/', timeout=30)
        page.wait_for_page_to_load()
        return page

    def login_invalid_creds(self, username: str, password: str) -> 'pages.PortalLoginPage':
        """
        Try to login an invalid Portal user whose username doesn't exist or whose password is incorrect.
        This method assumes that the user does not exist in Portal or exists but not with the same password. Since the
        caller need not have had to create a valid Portal user, this method only takes the username and password.
        Args:
            username: a string representing the name of the user to login
            password: a string representing the password fo the user to login
        Returns:
            The PortalLoginPage.
        """
        self.login_form.login(username, password)
        page = pages.PortalLoginPage(self.driver, base_url=self.base_url, timeout=self.timeout)
        return page


# class PortalActorLogin:
#     """Use this if you have a Portal Actor."""
#
#     def login(self, portal_user: pages.PortalActor) -> 'pages.PortalHomePage':
#         # Login to Portal
#         # login_page = PortalLoginPage(
#         #     driver=self.driver,
#         #     name='PortalLoginPage',
#         #     base_url='https://staging-cradlepointconnect.cs71.force.com/Connect/s/login/?language=en_US',
#         #     timeout=30)
#         login_page = PortalLoginPage(
#             driver=base_pages.UIWebDriver,
#             name='PortalLoginPage',
#             base_url='https://staging-cradlepointconnect.cs71.force.com/Connect/s/login/?language=en_US',
#             parent=None,
#             timeout=30,
#             frame=None)
#
#         login_page.open()
#         login_page.wait_for_page_to_load()
#         # Login method returns an object of type PortalPage.  PortalPage is the base class modeling the Portal Home Page
#         return login_page.login(portal_user)
