"""POM framework for the Partner Portal"""

from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class ContactSupportPage(PortalBasePage):
    """POM for Contact Support Page."""
    URL_TEMPLATE = 'contactsupport'
