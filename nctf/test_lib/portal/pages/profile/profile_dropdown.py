"""POM framework for the Partner Portal"""

from typing import cast
from typing import List

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.base.pages import UIPageLinkElement
from nctf.test_lib.base.pages.selenium_deps import By
from nctf.test_lib.base.pages.selenium_deps import WebElement
from nctf.test_lib.portal import pages


class ProfileDropDownMenu(base_pages.UIRegion):
    """
    This is the drop down menu that is exposed when you select the
    Profile drop down element defined in HeaderProfile
    """
    ROOT_LOCATOR = (By.CSS_SELECTOR, ".menuList")

    @property
    def menu_item_home(self):
        return UIPageLinkElement(
            name='ProfileMenuItemHome',
            parent=self,
            page_cls=pages.PortalHomePage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Home"]')

    @property
    def menu_item_my_profile(self):
        return UIPageLinkElement(
            name='ProfileMenuItemMyProfile',
            parent=self,
            page_cls=pages.ProfilePage,
            strategy=By.CSS_SELECTOR,
            locator='[title="My Profile"]')

    @property
    def menu_item_my_company(self):
        return UIPageLinkElement(
            name='ProfileMenuItemMyCompany',
            parent=self,
            page_cls=pages.CompanyPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="My Company"]')

    @property
    def menu_item_contact_support(self):
        return UIPageLinkElement(
            name='ProfileMenuItemContactSupport',
            parent=self,
            page_cls=pages.ContactSupportPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Contact Support"]')

    @property
    def menu_item_logout(self):
        return UIPageLinkElement(
            name='ProfileMenuItemLogout',
            parent=self,
            page_cls=pages.LogoutPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Logout"]')


class HeaderProfile(base_pages.UIRegion):
    """POM for Portal Profile Drop down list."""
    ROOT_LOCATOR = (By.XPATH, '//*[@id="253:0"]/div/div/a')

    @property
    def logged_in_user_name(self) -> str:
        return str(self.menu_item_profile.get_attribute('title'))

    @property
    def get_menu_items_text(self) -> List[str]:
        self.menu_item_profile.click()
        ret_val = []
        for element in self.parent.driver.find_elements(By.CSS_SELECTOR, ".menuList li"):
            ele = cast(WebElement, element)
            ret_val.append(ele.get_attribute('textContent'))
        self.menu_item_profile.click()
        return ret_val

    @property
    def menu_item_profile(self) -> base_pages.UIRegionLinkElement:
        return base_pages.UIRegionLinkElement(
            name='ProfileMenu',
            parent=self,
            region_cls=ProfileDropDownMenu,
            strategy=By.CLASS_NAME,
            locator='profileIcon',
            region_parent=self.parent)
