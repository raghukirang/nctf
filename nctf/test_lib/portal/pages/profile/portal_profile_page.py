"""POM framework for the Partner Portal"""

import pprint
from typing import cast
from typing import Dict

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class ProfileRegion(base_pages.UIRegion):
    """POM for the region that contains the Profile dropdown."""
    ROOT_LOCATOR = (By.CLASS_NAME, "cProfileViewBody")

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.profile_icon.wait_for_clickable(timeout=10)
        return self

    @property
    def profile_icon(self) -> base_pages.UIButtonElement:
        return base_pages.UIButtonElement(
            name='ProfileIconContainer', parent=self, strategy=By.CSS_SELECTOR, locator='.circularPhoto')

    def get_portal_profile_details(self) -> Dict[str, str]:
        """
        Expects that the My Company Details page is displayed
        Attempts to get the text property of the label and corresponding field on the My Company Details page
        The labels will be used as the key in a dictionary, the fields will be used as the values in the dictionary
        The keys are expected to match the keys used in the method get_sfdc_company_details.
        This relationship becomes the verification of those labels
        Ex:
        sfdc_details = details_page.get_sfdc_company_details(salesforce_fixture)
        portal_details = details_page.get_potal_company_details()
        assert 0 == len( set(sfdc_details) - set(portal_details)),
                             "\nExpected:" + pp.pformat(sfdc_details) + " \nFound:" + pp.pformat(portal_details).
        """
        p_print = pprint.PrettyPrinter(indent=4)
        field_data = []
        field_labels = []

        ele = cast(WebElement, self.driver.find_element(By.CSS_SELECTOR, ".cUserProfileCon .profileName"))
        field_data.append(ele.get_attribute('title'))

        ele = cast(WebElement,
                   self.driver.find_element(By.CSS_SELECTOR, ".cUserProfileCon .userDetailHeader .userBadge"))
        field_labels.append(
            str(
                self.driver.find_element(By.CSS_SELECTOR, "[data-aura-rendered-by='" +
                                         ele.get_attribute('data-aura-rendered-by') + "']").text))

        for element in self.driver.find_elements(By.CSS_SELECTOR, ".test-id__field-value"):
            ele = cast(WebElement, element)
            string_to_modify = str(
                self.driver.find_element(By.CSS_SELECTOR, "[data-aura-rendered-by='" +
                                         ele.get_attribute('data-aura-rendered-by') + "']").text)
            field_data.append(string_to_modify.replace('\n', " "))
        for element in self.driver.find_elements(By.CSS_SELECTOR, ".test-id__field-label-container"):
            ele = cast(WebElement, element)
            string_to_modify = str(
                self.driver.find_element(By.CSS_SELECTOR, ("[data-aura-rendered-by='" +
                                                           ele.get_attribute('data-aura-rendered-by') + "']")).text)
            field_labels.append(string_to_modify.replace('\n', " "))

        return dict(zip(field_labels, field_data))


class ProfilePage(PortalBasePage):
    """POM for Profile Page."""
    URL_TEMPLATE = 'profile/00550000003jy7T'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.edit_button.wait_for_clickable()
        return self

    @property
    def edit_button(self) -> base_pages.UIButtonElement:
        return base_pages.UIButtonElement(
            name='EditButton', parent=self, strategy=By.CLASS_NAME, locator='userDetailsEditButton')

    @property
    def profile_data(self) -> ProfileRegion:
        return ProfileRegion(self)
