"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal import pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class CompanyTabsRegion(base_pages.UIRegion):
    """POM for the Company Tab Region."""
    ROOT_LOCATOR = (By.CLASS_NAME, "tabs__nav")

    @property
    def details(self):
        return base_pages.UIPageLinkElement(
            name='MyCompany->Details',
            parent=self,
            page_cls=pages.CompanyDetailsPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Details"]')

    @property
    def additional_info(self):
        return base_pages.UIPageLinkElement(
            name='MyCompany->Add-Info',
            parent=self,
            page_cls=pages.CompanyAdditionalInfoPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Additional Info"]')

    @property
    def devices(self):
        return base_pages.UIPageLinkElement(
            name='MyCompany->Devices',
            parent=self,
            page_cls=pages.CompanyAdditionalInfoPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Devices"]')


class CompanyPage(PortalBasePage):
    """POM for the Company Page."""
    URL_TEMPLATE = 'account/0015000000MsQDdAAN/digital-highway-inc'

    @property
    def company_tabs(self) -> CompanyTabsRegion:
        return CompanyTabsRegion(self)
