"""POM framework for the Partner Portal"""

import pprint
from typing import cast
from typing import Dict

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class CompanyDetailsRegion(base_pages.UIRegion):
    """POM for Company Details Region"""
    ROOT_LOCATOR = (By.CLASS_NAME, "forceCommunityTabset")

    def wait_for_region_to_load(self):
        self.details_container.wait_for_displayed(timeout=10)
        return self

    @property
    def details_container(self):
        return base_pages.UIElement(
            name='MyCompany->Details', parent=self, strategy=By.CSS_SELECTOR, locator='[data-region-name="1"]')


class CompanyAdditionalInfoRegion(base_pages.UIRegion):
    """POM for Add info region"""
    ROOT_LOCATOR = (By.CLASS_NAME, "forceCommunityTabset")

    def wait_for_region_to_load(self):
        self.additional_info_container.wait_for_displayed(timeout=10)
        return self

    @property
    def additional_info_container(self):
        return base_pages.UIElement(
            name='MyCompany->AdditionalInfo', parent=self, strategy=By.CSS_SELECTOR, locator='[title="Contacts"]')


class CompanyDevicesRegion(base_pages.UIRegion):
    """POM for devices region"""
    ROOT_LOCATOR = (By.CLASS_NAME, "forceCommunityTabset")

    def wait_for_region_to_load(self):
        self.drives_container.wait_for_displayed(timeout=10)
        return self

    @property
    def drives_container(self):
        return base_pages.UIElement(
            name='MyCompany->Drives', parent=self, strategy=By.CSS_SELECTOR, locator='[data-region-name="0d4ce"]')


class CompanyDetailsPage(PortalBasePage):
    """POM for Details page."""
    URL_TEMPLATE = 'account/0015000000MsQDdAAN/digital-highway-inc'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.company_details.wait_for_region_to_load()
        return self

    @property
    def company_details(self) -> CompanyDetailsRegion:
        return CompanyDetailsRegion(self)

    def get_portal_company_details(self) -> Dict[str, str]:
        """
        Expects that the My Company Details page is displayed
        Attempts to get the text property of the label and corresponding data field on the My Company Details page
        The labels will be used as the key in a dictionary, the fields will be used as the values in the dictionary
        The keys are expected to match the keys used in the method get_sfdc_company_details.
        This relationship becomes the verification of those labels
        Ex:
        sfdc_details = details_page.get_sfdc_company_details(salesforce_fixture)
        portal_details = details_page.get_potal_company_details()
        assert 0 == len( set(sfdc_details) - set(portal_details)),
                       "\nExpected:" + pp.pformat(sfdc_details) + " \nFound:" + pp.pformat(portal_details).
        """
        p_print = pprint.PrettyPrinter(indent=4)
        ret_val = dict()
        field_data = []
        field_labels = []
        for element in self.driver.find_elements(By.CSS_SELECTOR, ".test-id__field-value"):
            ele = cast(WebElement, element)
            value = ele.get_attribute('data-aura-rendered-by')
            string_to_modify = str(
                self.driver.find_element(By.CSS_SELECTOR, "[data-aura-rendered-by='" + value + "']").text)
            field_data.append(string_to_modify.replace('\n', " "))
        for element in self.driver.find_elements(By.CSS_SELECTOR, ".test-id__field-label-container"):
            ele = cast(WebElement, element)
            value = ele.get_attribute('data-aura-rendered-by')
            string_to_modify = str(
                self.driver.find_element(By.CSS_SELECTOR, "[data-aura-rendered-by='" + value + "']").text)
            field_labels.append(string_to_modify.replace('\n', " "))

        ret_val = dict(zip(field_labels, field_data))
        # The way the Portal pages are implemented, it is common to have hidden blank labels and other fields.
        # These need to be removed before the dictionary can be returned.
        if "" in ret_val.keys():
            del ret_val[""]
        return ret_val


class CompanyAdditionalInfoPage(PortalBasePage):
    """POM for Add info region."""
    URL_TEMPLATE = 'account/0015000000MsQDdAAN/digital-highway-inc?tabset-96c0d=2'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.company_add_info.wait_for_region_to_load()
        return self

    @property
    def company_add_info(self) -> CompanyAdditionalInfoRegion:
        return CompanyAdditionalInfoRegion(self)
