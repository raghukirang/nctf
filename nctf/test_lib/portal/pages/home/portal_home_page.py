"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal import pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class PortalHomePage(PortalBasePage):
    """POM for Portal Home Page."""

    def __init__(self, driver: base_pages.UIWebDriver, base_url: str=None, **url_kwargs):
        if base_url is None:
            base_url = driver.stack_info.partner_portal.url
        super().__init__(driver=driver, base_url=base_url, **url_kwargs)

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.hero_image.wait_for_clickable(10)
        return self

    @property
    def hero_image(self) -> base_pages.UIPageLinkElement['pages.PortalHomePage']:
        """
        This method needs to be fixed
        I do not know the page that loads after pressing the hero image
        I mostly needed this for "wait_for_page_to_load".
        """
        x_path = '//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div[2]/div/div[1]/div/div[1]/p/a/img'
        return base_pages.UIPageLinkElement(
            name='HeroImage', parent=self, page_cls=pages.PortalHomePage, strategy=By.XPATH, locator=x_path)

    @property
    def view_all(self):
        return base_pages.UIElement(
            name='ConnectCommunity', parent=self, strategy=By.CLASS_NAME, locator='viewAllLink')
