"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.base.pages import UIPageLinkElement as DynamicPageLink
from nctf.test_lib.portal import pages


class TrainingDropdownMenu(base_pages.UIRegion):
    """POM for the Menubar -> Account Dropdown."""
    ROOT_LOCATOR = (By.XPATH, '//*[@id="navigationMenu"]/li[4]/div/div[2]')

    @property
    def menu_item_registration(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='Training->UniversityRegistration',
            parent=self,
            page_cls=pages.UniversityRegistrationPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Cradlepoint University Registration"]')

    @property
    def menu_item_logon(self):
        """A failure to find this item may mean the XPath changed"""
        return base_pages.UIPageLinkElement(
            name='Training->UniversityLogon',
            parent=self,
            page_cls=pages.UniversityLogonPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Cradlepoint University Login"]')
