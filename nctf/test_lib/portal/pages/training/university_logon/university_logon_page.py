"""POM framework for the Partner Portal"""

from nctf.test_lib.base import pages as base_pages


class UniversityLogonPage(base_pages.UIPage):
    """The base for all portal pages."""
    URL_TEMPLATE = 'https://cradlepoint.myabsorb.com/#/login'

    def __init__(self, driver: base_pages.UIWebDriver, base_url: str=None, **url_kwargs):
        base_url = None
        super().__init__(driver=driver, base_url=base_url, **url_kwargs)
