"""POM framework for the Partner Portal"""

from typing import cast
from typing import List

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.base.pages.selenium_deps import By
from nctf.test_lib.base.pages.selenium_deps import WebElement
from nctf.test_lib.portal import pages


class HeaderMenubar(base_pages.UIRegion):
    """
    This is used to model the menu bar area including the Home button/icon
    Each dropdown menu should modeled separately as a UIDynamicPage.
    """
    ROOT_LOCATOR = (By.ID, 'navigationMenu')

    def wait_for_region_to_load(self):
        self.home_button.wait_for_clickable(timeout=30)
        return self

    @property
    def get_my_account_menu_items_text(self) -> List[str]:
        self.my_account.click()
        return self.get_menu_items(1)

    @property
    def get_resources_menu_items_text(self) -> List[str]:
        self.resources.click()
        return self.get_menu_items(2)

    def get_menu_items(self, listNumber):
        ret_val = []
        for element in self.parent.driver.find_elements(By.CSS_SELECTOR,
                                                        '.navigationMenuNode')[listNumber].find_elements(
                                                            By.CSS_SELECTOR, '.subMenu li'):
            ele = cast(WebElement, element)
            ret_val.append(ele.get_attribute('textContent'))
        self.my_account.click()
        return ret_val

    @property
    def home_button(self):
        return base_pages.UIPageLinkElement(
            name='MenuBarHomeButton',
            parent=self,
            page_cls=pages.PortalHomePage,
            strategy=By.CLASS_NAME,
            locator='homeButton')

    @property
    def my_account(self):
        return base_pages.UIRegionLinkElement(
            name='My Account Dropdown',
            parent=self,
            region_cls=pages.MyAccountDropdownMenu,
            strategy=By.XPATH,
            locator='//*[@id="20:37;a"]/div/div/a',
            region_parent=self.parent)

    @property
    def resources(self):
        return base_pages.UIRegionLinkElement(
            name='Resources Dropdown',
            parent=self,
            region_cls=pages.ResourcesDropdownMenu,
            strategy=By.XPATH,
            locator='//*[@id="107:37;a"]/div/div/a',
            region_parent=self.parent)

    @property
    def training(self) -> base_pages.UIRegionLinkElement:
        return base_pages.UIRegionLinkElement(
            name='Training Dropdown',
            parent=self,
            region_cls=pages.TrainingDropdownMenu,
            strategy=By.XPATH,
            locator='//*[@id="194:37;a"]/div/div/a',
            region_parent=self.parent)

    @property
    def support(self) -> base_pages.UIRegionLinkElement:
        return base_pages.UIRegionLinkElement(
            name='Support Dropdown',
            parent=self,
            region_cls=pages.SupportDropdownMenu,
            strategy=By.XPATH,
            locator='//*[@id="257:37;a"]/div/div/a',
            region_parent=self.parent)


class HeaderRegion(base_pages.UIRegion):
    """POM for Portal Header area: Includes MenuBar, Logo, Search and Profile."""
    ROOT_LOCATOR = (By.ID, "header")

    def wait_for_region_to_load(self):
        self.search_button.wait_for_present(timeout=30)
        return self

    @property
    def cradlepoint_logo(self):
        return base_pages.UIElement('ConnectCommunity', self, By.CSS_SELECTOR, '[title="Connect Community"]')

    @property
    def search_button(self):
        return base_pages.UIElement('SearchButton', self, By.CLASS_NAME, 'search-button')

    @property
    def profile_dropdown(self):
        return pages.HeaderProfile(self)

    @property
    def menubar(self) -> HeaderMenubar:
        return pages.HeaderMenubar(self)
