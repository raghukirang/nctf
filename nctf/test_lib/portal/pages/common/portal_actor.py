"""POM framework for the Partner Portal"""

from nctf.test_lib.base.actor import NCTFActorWithCreds
from nctf.test_lib.salesforce.objects.account import SFAccount
from nctf.test_lib.salesforce.objects.user import SFUser


class PortalAccountData:
    """
        This covers the Partner Account data significant to the Portal
        The Keys represent the field labels used in the Portal
        By verifying that the labels scrapped from the Portal match the ones used here,
        we confirm that the expected labels are displayed.
    """

    def __init__(self,
                 disti=None,
                 account_name=None,
                 account_owner=None,
                 parent=None,
                 phone=None,
                 fax=None,
                 webpage=None,
                 billing_address=None,
                 shipping_address=None):
        """Allows creation when you already have the data"""
        self.data = dict()
        self.data['Primary Distributor The distributor that is the primary source for inventory fulfillment.'] = disti
        self.data['Account Name'] = account_name
        self.data['Account Owner Full Name'] = account_owner
        # self.data['Parent Account'] = parent
        self.data['Phone'] = phone
        self.data['Fax'] = fax
        self.data['Website'] = webpage
        self.data['Billing Address'] = billing_address
        self.data['Shipping Address'] = shipping_address

    def populate_from_sfdc(self, sf_account: SFAccount, disti_account: SFAccount, parent_account: SFAccount):
        """Populate from SFDC objects"""
        self.data = dict()
        self.data['Account Name'] = str(sf_account.sf_attributes['Name'])
        self.data['Account Owner Full Name'] = str(sf_account.sf_attributes['Account_Owner_Full_Name__c'])
        self.data['Phone'] = str(sf_account.sf_attributes['Phone'])
        self.data['Fax'] = str(sf_account.sf_attributes['Fax'])
        self.data['Website'] = str(sf_account.sf_attributes['Website'])
        self.data['Billing Address'] = str(
            sf_account.sf_attributes['BillingStreet'] + ", " + sf_account.sf_attributes['BillingCity'] + ", " +
            sf_account.sf_attributes['BillingState'] + " " + sf_account.sf_attributes['BillingPostalCode'] + " " +
            sf_account.sf_attributes['BillingCountry'])
        self.data['Shipping Address'] = str(
            sf_account.sf_attributes['ShippingStreet'] + ", " + sf_account.sf_attributes['ShippingCity'] + ", " +
            sf_account.sf_attributes['ShippingState'] + " " + sf_account.sf_attributes['ShippingPostalCode'] + " " +
            sf_account.sf_attributes['ShippingCountry'])

        self.data['Primary Distributor The distributor that is the primary source for inventory fulfillment.'] = ""
        if 'Name' in disti_account.sf_attributes:
            self.data['Primary Distributor The distributor that is the primary source for inventory fulfillment.'] = str(
                disti_account.sf_attributes['Name'])

        # self.data['Parent Account'] = ""
        # if 'Name' in parent_account.sf_attributes:
        #     self.data['Parent Account'] = str(parent_account.sf_attributes['Name'])

    @property
    def info(self) -> dict:
        """This is the payload"""
        return self.data


class PortalUserData:
    """
        This covers the Contact User data significant to the Portal
        The Keys represent the field labels used in the Portal
        By verifying that the labels scrapped from the Portal match the ones used here,
        we confirm that the expected labels are displayed.
    """

    def __init__(self, account=None, address=None, contact=None, partner=None, phone=None, title=None):
        """Allows creation when you already have the data"""
        self.data = dict()
        self.data['Account'] = account
        self.data['Address'] = address
        self.data['Contact'] = contact
        self.data['Partner'] = partner
        self.data['Phone'] = phone
        self.data['Title'] = title

    def populate_from_sfdc(self, sf_user: SFUser, sf_account: SFAccount):
        """Populate from SFDC data objects"""
        self.data['Account'] = str(sf_user.sf_attributes['Name'])
        self.data['Address'] = (str(sf_user.sf_attributes['Street']) + " " + str(sf_user.sf_attributes['City']) + " " +
                                str(sf_user.sf_attributes['State']) + " " + str(sf_user.sf_attributes['PostalCode']) + " " +
                                str(sf_user.sf_attributes['Country']) + " ")
        self.data['Contact'] = str(sf_user.sf_attributes['Phone'])
        self.data['Partner'] = str(sf_user.sf_attributes['Name'])
        self.data['Phone'] = str(sf_account.sf_attributes['Phone'])
        self.data['Title'] = str(sf_user.sf_attributes['Title'])

    @property
    def info(self):
        return self.data


class PortalActor(NCTFActorWithCreds):
    """
        This object represents all SFDC data relater to a Portal User
        It will be used to log-in a user and verify the correct values are displayed
        for the user data and partner account data related to a given portal actor
        It may develop into a Get/Set object that can create Portal Actors on the fly.
    """

    def __init__(self, username: str, password: str, displayname: str, account_identifier: str, contactId: str,
                 userdata: PortalUserData, accountdata: PortalAccountData):
        super().__init__(username, password)
        self.display_name = displayname
        self.account_id = account_identifier
        self.contact_id = contactId
        self.user_data = userdata
        self.account_data = accountdata
