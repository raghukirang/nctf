"""POM framework for the Partner Portal"""

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_regions import HeaderRegion


class PortalBasePage(base_pages.UIPage):
    """This is the base page for all other portal pages."""
    @property
    def portal_header(self) -> 'HeaderRegion':
        return HeaderRegion(self)
