"""POM framework for the Partner Portal"""

import time

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class UnmanagedDevicesRegion(base_pages.UIRegion):
    """POM for the Device Region."""
    ROOT_LOCATOR = (By.XPATH, '//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div[1]')

    def wait_for_region_to_load(self):
        time.sleep(3)

    @property
    def aer(self) -> base_pages.UIElement:
        x_path = '//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div[1]/div[1]/div[1]/div/div/span/label/span[1]'
        return base_pages.UIElement(name="AER Checkbox", parent=self, strategy=By.XPATH, locator=x_path)


class DevicePage(PortalBasePage):
    """POM for the Device Page."""
    URL_TEMPLATE = 'devices'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.unmanaged_devices.wait_for_region_to_load()
        return self

    @property
    def unmanaged_devices(self) -> UnmanagedDevicesRegion:
        return UnmanagedDevicesRegion(self)
