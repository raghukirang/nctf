"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class CustomerAccountsDropdown(base_pages.UIRegion):
    """POM for Accounts Dropdown"""
    ROOT_LOCATOR = (By.CSS_SELECTOR, '[title="Select List View"]')


class CustomerAccountsTableRegion(base_pages.UIRegion):
    """POM for Accounts Table."""
    ROOT_LOCATOR = (By.CLASS_NAME, 'test-listViewManager')

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.customer_accounts.wait_for_clickable()
        return self

    @property
    def customer_accounts(self) -> CustomerAccountsDropdown:
        return CustomerAccountsDropdown(self)


class MyCustomersPage(PortalBasePage):
    """POM for My Customers Page."""
    URL_TEMPLATE = 'account/Account/00B4D000000RdKkUAK'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.accounts_table.wait_for_region_to_load()
        return self

    @property
    def accounts_table(self) -> CustomerAccountsTableRegion:
        return CustomerAccountsTableRegion(self)
