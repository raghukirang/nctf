"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class SubscriptionDropdown(base_pages.UIRegion):
    """POM for Subscription dropdown."""
    ROOT_LOCATOR = (
        By.XPATH,
        '//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div[2]/div/div[1]/div[1]/div[1]/div/div[2]/h1/a')


class SubscriptionTableRegion(base_pages.UIRegion):
    """POM for the Sub table."""
    ROOT_LOCATOR = (By.CLASS_NAME, 'test-listViewManager')

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.subscription.wait_for_clickable()
        return self

    @property
    def subscription(self) -> SubscriptionDropdown:
        return SubscriptionDropdown(self)


class SubscriptionPage(PortalBasePage):
    """POM for the Subscription page."""
    URL_TEMPLATE = 'subscriptions'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.account_name.wait_for_clickable()
        return self

    @property
    def subscription_table(self) -> SubscriptionTableRegion:
        return SubscriptionTableRegion(self)

    @property
    def account_name(self) -> base_pages.UIElement:
        return base_pages.UIElement(
            name="AccountColumnTitle", parent=self, strategy=By.CSS_SELECTOR, locator='[title="Account Name"]')
