"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.base.pages import UIPageLinkElement as DynamicPageLink
from nctf.test_lib.portal import pages


class MyAccountDropdownMenu(base_pages.UIRegion):
    """POM for the Menubar -> Account Dropdown."""
    ROOT_LOCATOR = (By.XPATH, '//*[@id="navigationMenu"]/li[2]/div/div[2]')

    @property
    def menu_item_opportunities(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='MyAccount->Opportunity',
            parent=self,
            page_cls=pages.OpportunitiesPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Opportunities"]')

    @property
    def menu_item_subscriptions(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='MyAccount->Subscriptions',
            parent=self,
            page_cls=pages.SubscriptionPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Subscriptions"]')

    @property
    def menu_item_devices(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='MyAccount->Devices',
            parent=self,
            page_cls=pages.DevicePage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Devices"]')

    @property
    def menu_item_my_customers(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='MyAccount->MyCustomers',
            parent=self,
            page_cls=pages.MyCustomersPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="My Customers"]')

    @property
    def menu_item_deal_registration(self):
        """A failure to find this item may mean the XPath changed"""
        return DynamicPageLink(
            name='MyAccount->DealReg',
            parent=self,
            page_cls=pages.DealRegistrationPage,
            strategy=By.CSS_SELECTOR,
            locator='[title="Deal Registration"]')
