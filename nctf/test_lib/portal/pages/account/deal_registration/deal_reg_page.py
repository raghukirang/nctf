"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class DealRegistrationDropdown(base_pages.UIRegion):
    """POM for the Deal Reg Dropdown"""
    ROOT_LOCATOR = (By.XPATH, '//*[@id="1014:0"]/div/div/div/div[1]/div[1]/div[1]/div/div[2]/h1/a/div/span[1]')


class DealRegistrationTableRegion(base_pages.UIRegion):
    """POM for the Deal Reg Table region"""
    ROOT_LOCATOR = (By.CLASS_NAME, 'test-listViewManager')

    def wait_for_region_to_load(self):
        super().wait_for_region_to_load()
        self.deal_registration.wait_for_clickable()
        return self

    @property
    def deal_registration(self) -> DealRegistrationDropdown:
        return DealRegistrationDropdown(self)


class DealRegistrationPage(PortalBasePage):
    """POM for Deal Reg Page"""
    URL_TEMPLATE = 'deal-registration-2'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.dr_app_id.wait_for_clickable()
        return self

    @property
    def deal_reg_table(self) -> DealRegistrationTableRegion:
        return DealRegistrationTableRegion(self)

    @property
    def dr_app_id(self) -> base_pages.UIElement:
        return base_pages.UIElement(
            name="AccountColumnTitle", parent=self, strategy=By.CSS_SELECTOR, locator='[title="DR Application"]')
