"""POM framework for the Partner Portal"""

from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages as base_pages
from nctf.test_lib.portal.pages.common.portal_base import PortalBasePage


class OpportunitiesDropdown(base_pages.UIRegion):
    """POM for the Opp dropdown."""
    ROOT_LOCATOR = (
        By.XPATH,
        '//*[@id="NapiliCommunityTemplate"]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/div[1]/div[1]/div/div[2]')


class OpportunitiesTableRegion(base_pages.UIRegion):
    """POM for the Opp table."""
    ROOT_LOCATOR = (By.CLASS_NAME, 'test-listViewManager')

    def wait_for_region_to_load(self):
        self.opportunities.wait_for_region_to_load()
        return self

    @property
    def opportunities(self) -> OpportunitiesDropdown:
        return OpportunitiesDropdown(self)


class OpportunitiesPage(PortalBasePage):
    """POM for the Opp page."""
    URL_TEMPLATE = 'opportunity/Opportunity/00B4D000000sx78UAA'

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.opportunities_table.wait_for_region_to_load()
        return self

    @property
    def opportunities_table(self) -> OpportunitiesTableRegion:
        return OpportunitiesTableRegion(self)
