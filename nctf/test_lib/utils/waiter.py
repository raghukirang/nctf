import functools
import numbers
import operator
from typing import Any
from typing import Callable

from nctf.libs.common_library import wait_for_with_interval
# Included for convenience; do not remove
from nctf.libs.common_library import WaitTimeoutError


def wait(timeout: numbers.Number, interval: numbers.Number=0):
    """Convenience method for creating a `Waiter` object."""
    return Waiter(timeout, interval)


class Waiter(object):
    def __init__(self,
                 timeout_sec: numbers.Number,
                 interval_sec: numbers.Number=0,
                 func: Callable[..., Any]=None,
                 *args,
                 **kwargs):
        """A helper class for descriptively building polling waits.

        The object is initially built with a timeout (optional call interval) and
        a method to call. The function and its paramters may be specified at
        construction, or as a following call to the waiter object.

        All `to_*` methods (`to_equal`, `to_contain`, etc.) will raise a
        WaitTimeoutError if the conditions are not satisfied before the calls are complete.

        The passed-in function is guaranteed to be called at least once.

        Args:
            timeout_sec: Maximum time to wait, in seconds.
            interval_sec: Minimum interval between calls, in seconds.
                Default 0 (back-to-back calls).
            func: The function to continuously call and check the return value of.
            *args: Positional arguments passed along to the given function.
            **kwargs: Keyword arguments passed along to the given function.

        Notes:
            A Waiter, by itself, does not execute the contained function. One
            of the `to_*` methods must be used to call & check the function.

        Examples:
            Wait up to 20 seconds for calls to function bar() to return 'bazzer'.
                >>> Waiter(20).for_call(bar).to_equal('bazzer')
            Same, but with a minimum 0.5s wait between calls, and waits until
                the string merely contains 'baz'.
                >>> Waiter(20, 0.5).for_call(bar).to_contain('baz')
            Wait 5 seconds for synced_with_salesforce(sf_account) to return
                True. Note the parameter is passed in separately.
                >>> Waiter(5.0).for_call(synched_with_salesforce, sf_account).to_be(True)
        """
        self.timeout = timeout_sec
        self.interval = interval_sec
        self.func = func
        self.func_args = args
        self.func_kwargs = kwargs

    def for_call(self, func: Callable[..., Any], *args, **kwargs) -> 'Waiter':
        """Sets the function to be called for this Waiter.

        Optionally, the `args` and `kwargs` passed here will be used to call
        the given function with when waiting for return values.

        Args:
            func: The function to set for later polling.
            *args: Positional arguments passed along to the given function.
            **kwargs: Keyword arguments passed along to the given function.
        """
        self.func = func
        self.func_args = args
        self.func_kwargs = kwargs
        return self

    def to(self, oper: Callable[[Any, Any], bool], rhs: Any) -> Any:
        """Wait for the given operator to return True, given the RHS value.

        Args:
            oper: Any two-parameter operator from the `operator` module which
                returns a boolean value.
            rhs: The value to be used as the second parameter (right-hand side)
                for the operator. The LHS is the return from the stored function.

        Returns:
            The final return from the stored function, if the timeout is not
            reached (i.e. the operator returns True before the timeout).

        Raises:
            WaitTimeoutError: If the timeout is reached for the operator returns True.
        """
        return wait_for_with_interval(
            self.timeout, self.interval, rhs, self.func, *self.func_args, **self.func_kwargs, _operator=oper)

    to_equal = functools.partialmethod(to, operator.eq)
    """Wait for the result to equal (i.e. `==`) a certain value. See func:`Waiter.to`."""

    to_not_equal = functools.partialmethod(to, lambda a, b: operator.not_(operator.eq(a, b)))
    """Wait for the result to not equal (i.e. `!=`) a certain value. See func:`Waiter.to`."""

    to_be_lt = functools.partialmethod(to, operator.lt)
    """Wait for the result to be strictly less than (i.e. `<`) a certain value. See func:`Waiter.to`."""

    to_be_le = functools.partialmethod(to, operator.le)
    """Wait for the result to be less than or equal to (i.e. `<=`) a certain value. See func:`Waiter.to`."""

    to_be_gt = functools.partialmethod(to, operator.gt)
    """Wait for the result to be strictly greater than (i.e. `>`) a certain value. See func:`Waiter.to`."""

    to_be_ge = functools.partialmethod(to, operator.ge)
    """Wait for the result to be greater than or equal to (i.e. `>=`) a certain value. See func:`Waiter.to`."""

    to_be = functools.partialmethod(to, operator.is_)
    """Wait for the result to be (i.e. `is`) a certain value. See func:`Waiter.to`."""

    to_not_be = functools.partialmethod(to, operator.is_not)
    """Wait for the result to not be (i.e. `is not`) a certain value. See func:`Waiter.to`."""

    # TODO: In some cases, it may be desirable to allow non-Iterable values to not throw TypeErrors here.
    to_contain = functools.partialmethod(to, operator.contains)
    """Wait for the result to contain (i.e. `in`) a certain value. See func:`Waiter.to`."""

    to_not_contain = functools.partialmethod(to, lambda a, b: operator.not_(operator.contains(a, b)))
    """Wait for the result to not contain (i.e. `not in`) a certain value. See func:`Waiter.to`."""

    to_be_in = functools.partialmethod(to, lambda a, b: a in b)
    """Wait for the result to be 'in' the list. See func:`Waiter.to`."""
