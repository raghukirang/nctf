"""
S3 Stream functions

s3_firehose_reader
 - Optimize s3 file set by examining top level directory names
   We don't want to iterate through thousands of files on each stream
   Find common year, month, day, hour and feed into objects.filter()

 - Review s3 read charges, does having a large CHUNK_SIZE reduce cost?
   Does checking the object name cost the same as a read

 - S3 files are named with the timestamp of the first sample and
   written at the end of the buffering period

 - Each JSON objects must include a trailing new line

s3_firehose_writer
 - Write fact dictionary to s3 using firehose format.

s3_support_writer
 - Write a shareable JSON or CSV file to S3 for a specific tenant ID.

 - Can we find a way to do the JSON-to-CSV conversion in-memory?

 - Returns the S3 object name that we'll later use to create a presigned URL.
"""
import datetime
import logging
import json
import csv
import pytz
import uuid
import io
from pytz import UTC

logger = logging.getLogger('s3_streams')

S3_READ_CHUNK_SIZE_DEFAULT = 10000
S3_READ_CHUNK_SIZE = S3_READ_CHUNK_SIZE_DEFAULT

FIREHOSE_MAX_BUFFER_SEC = 900
SAMPLE_LATENCY_SEC = 300
UUID_LEN = 36


def _override_defaults(s3_read_chunk_size):
    global S3_READ_CHUNK_SIZE
    S3_READ_CHUNK_SIZE = s3_read_chunk_size


def _reset_defaults():
    global S3_READ_CHUNK_SIZE
    S3_READ_CHUNK_SIZE = S3_READ_CHUNK_SIZE_DEFAULT


def _get_common_prefix(start_time, end_time):
    """
    Determines the common s3 prefix based on start and end time.
    Must account for Firehose buffering and sample latency.
    """
    start_tuple = start_time.timetuple()
    end_tuple = end_time.timetuple()

    # Loop through year, month, day, hour
    prefix = ''
    for i in range(4):
        if start_tuple[i] == end_tuple[i]:
            prefix += str(start_tuple[i]).zfill(2) + '/'
        else:
            break

    return prefix


def _get_file_datetime(obj_name):
    """
    Extract the datetime write time based on the object name
    ecm.dev1.presence-5-2017-04-11-18-13-09-154149f9-425b-4dbc-8bc8-867779d8a07c
    """
    ts = None

    # Isolate bad object names
    try:
        # Trim trailing UUID
        name = obj_name[:-UUID_LEN - 1]

        # Split by -
        name_parts = name.split('-')
        ts = datetime.datetime(*map(lambda x: int(x), name_parts[-6:])) \
            .replace(tzinfo=pytz.UTC)
    except Exception as e:
        logger.error('Invalid S3 object name: {}, Exception: {}'
                     .format(obj_name, e))

    return ts


def _time_str_fields(time_str):
    """Return year, month, day, hour, minute, second"""
    return (time_str[0:4], time_str[5:7], time_str[8:10],
            time_str[11:13], time_str[14:16], time_str[17:19])


def _firehose_s3_name(stream_name, stream_qual, path, time_str):
    """
    Build firehose compatible name.
    path: yyyy/mm/dd/hh/<obj-name>
    obj-name: <stream-name>-<qual>-yyyy-mm-dd-hh-mm-ss-<guid>
    guid: 8-4-4-4-12, uuid.uuid4()
    """
    (year, month, day, hour, minute, second) = _time_str_fields(time_str)
    date_path = '{}/{}/{}/{}'.format(year, month, day, hour)
    obj_name = '{}-{}-{}-{}-{}-{}-{}-{}'. \
               format(stream_name, stream_qual, year, month,
                      day, hour, minute, second)
    guid = uuid.uuid4()

    full_name = '{}/{}/{}/{}-{}'. \
                format(stream_name, path, date_path, obj_name, guid)
    return full_name


def s3_firehose_reader(bucket, start_time, end_time, bucket_postfix=''):
    """
    Create iterator to return all the objects within the
    given time range stored in the s3 bucket. Optimize lookup
    using directory and filename structure. Look forward one
    file to catch any objects that overflowed into the next file.
    """

    # Validate start and end time
    if start_time > end_time:
        raise ValueError('Start time greater than end time')

    # s3 filenames don't contain microseconds
    # Must account for Firehose buffering and sample latency
    file_start_time = start_time.replace(microsecond=0) \
        - datetime.timedelta(seconds=FIREHOSE_MAX_BUFFER_SEC)
    file_end_time = end_time.replace(microsecond=0) \
        + datetime.timedelta(seconds=SAMPLE_LATENCY_SEC)

    # Objects are filtered to the microsecond
    obj_start_time = start_time.isoformat()
    obj_end_time = end_time.isoformat()

    # Perform preliminary filtering based on directory path. Fill out as much
    # of the common path to the file(s) as possible.
    common_date_prefix = _get_common_prefix(file_start_time, file_end_time)
    prefix = bucket_postfix + common_date_prefix
    for bucket_item in bucket.objects.filter(Prefix=prefix):
        # Next, filter by s3 filename
        file_datetime = _get_file_datetime(bucket_item.key)
        if file_datetime and file_start_time <= file_datetime < file_end_time:
            s3_file = bucket_item.get()
            remaining = b''
            for chunk in iter(lambda: s3_file['Body'].read(
                    S3_READ_CHUNK_SIZE), b''):
                # Extract JSON objects
                data = remaining + chunk
                json_objects = data.split(b'\n')
                for json_obj in json_objects[0:-1]:
                    # Isolate bad JSON objects
                    try:
                        obj = json.loads(json_obj.decode('utf-8'))
                        if obj_start_time <= obj['timestamp'] <= obj_end_time:
                            yield obj
                    except Exception as e:
                        logger.error('Invalid JSON object in stream: {}, '
                                     'Exception: {}'.format(json_obj, e))
                remaining = json_objects[-1]


def s3_firehose_writer(stream_name, stream_qual, facts, bucket):
    """Write fact dictionary to s3 using firehose format"""

    # Calculate object name, firehose uses first fact timestamp
    # to name file s3 object
    for key, value in facts.items():
        s3_obj_name = _firehose_s3_name(stream_name, stream_qual, key,
                                        value[0].get('timestamp'))
        body = '\n'.join(map(json.dumps, value))
        print('obj_name: {}\nbody: {}'.format(s3_obj_name, body))
        bucket.put_object(
            Key=s3_obj_name,
            Body=body,
            ServerSideEncryption='AES256')


def s3_support_writer(stream_name, stream_qual, key, data, bucket, format):
    """
    Write presence data to a location Support can share with a customer.

    :param stream_name:
    :param stream_qual:
    :param key: Tenant ID.
    :param data: List of dictionaries (presence records)
    :param bucket: Output S3 bucket name.
    :param format: JSON or CSV.
    :return: S3 object name.
    """
    timestamp = datetime.datetime.utcnow().replace(tzinfo=UTC)
    s3_obj_name = _firehose_s3_name(
        stream_name, stream_qual, key, str(timestamp)) + '.' + format

    body = None
    if format == 'json':
        body = '\n'.join(map(json.dumps, data))
    else:  # csv

        # Used for in-memory file operations.
        csv_output = io.StringIO()

        f = csv.DictWriter(csv_output, data[0].keys(), quoting=csv.QUOTE_ALL)

        f.writeheader()
        f.writerows(data)

        # Get converted CSV data.
        body = csv_output.getvalue()

        csv_output.close()

    print('obj_name: {}\nbody: {}'.format(s3_obj_name, body))
    bucket.put_object(Key=s3_obj_name, Body=body)

    return s3_obj_name
