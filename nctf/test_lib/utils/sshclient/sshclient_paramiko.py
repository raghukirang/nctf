import json
import re
import requests
import socket
import time
from typing import List
from typing import Union

import logging
import paramiko
from paramiko import SFTPAttributes
from paramiko import RSAKey
from semantic_version import Version

logger = logging.getLogger("utils.SSHShellClient")

TCPDUMP_TIMEOUT = 20
MYTIMEOUT = 10
LOOKBACK = 2
TCPDUMP_EMPTY_LEN = 285
SH_TCPDUMP_EMPTY_LEN = 0


class SSHShellClient(object):
    def __init__(self, host: str, username: str, password: str, port=22):
        """
        Create a new SSHClient
        Args:
            host: the host to connect to
            username: username to authenticate with
            password:  password to authenticate with
            port: port to send connect to 
        """

        self.host = host
        self.port = port
        self.username = username
        self.password = password

        logger.info('SSHShellClient settings: host[%s] port[%s] username[%s] pwd[%s]' % (self.host, self.port,
                                                                                         self.username,
                                                                                         self.password))
        self.ssh = paramiko.SSHClient()
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        self.timeout = MYTIMEOUT
        self.connect_timeout = None
        self.key_data = {}

    def change_timeout(self, changeto: int, which='timeout') -> None:
        """
        Update the ssh timeouts
        Args:
            changeto: value for timeout
            which: the name of the timeout to update

        Returns:

        """
        if which == 'connect_timeout':
            self.connect_timeout = changeto
        else:
            self.timeout = changeto
        logger.debug('SSHShellClient timeout[%s] connect_timeout[%s]' % (self.timeout, self.connect_timeout))

    @staticmethod
    def readline_sock(sock: paramiko.Channel, terminator: str) -> str:
        """
        read output from channel
        Args:
            sock: ssh channel
            terminator: regex to match to know we've hit the end of what we expect

        Returns: list of lines read from socket

        """
        chars = ""
        while True:
            try:
                a = sock.recv(1024)
            except socket.timeout:
                break
            if not a:
                logger.debug(f"recv returned empty: [{a.decode()}]")
                break  # empty string means channel closed

            logger.log(level=5, msg=f"recv: {a.decode()}")  # need/want timestamps on output here -- what is happening?
            chars += a.decode()
            if re.search(terminator, chars, re.MULTILINE):
                break
            if len(a) < 100:
                time.sleep(0.1)  # let the remote actually generate some output before we grab it

        return chars

    def read_sock_output(self, sock: paramiko.Channel, terminator: str) -> List[str]:
        """
        Read output from channel
        Args:
            sock: ssh channel
            terminator: regex to match

        Returns: list of lines read from socket

        """
        lines = []
        sock.settimeout(self.timeout)
        while True:
            line = self.readline_sock(sock=sock, terminator=terminator)
            if line:
                lines.append(line)
                try:
                    prompt_found = '$' in line[-(LOOKBACK + 2):] or '#' in line[-(LOOKBACK + 2):]
                except IndexError:
                    prompt_found = False
                if prompt_found:
                    break
            else:
                break
        return lines

    @staticmethod
    def no_escape_codes(lines: List[str]) -> List[str]:
        """
        Strip out escape codes from lines
        Args:
            lines: output from channel recv function

        Returns: list with escape codes removed

        """
        newlines = []
        for line in lines:
            try:
                i = line.index('\x1b')
                line = line[:i] + '^[' + line[i + 1:]
            except ValueError:
                pass
            newlines.append(line)
        return newlines

    def return_output(self, chan: paramiko.Channel, printout=False, terminator: str=r"[$#\n] ?$") -> str:
        """
        Return output from the channel
        Args:
            chan: ssh channel
            printout: print output to logger
            terminator: regex string to match for to know we're at the end of what we expect

        Returns: concatenated string representing output from channel 

        """
        output = ''.join(self.no_escape_codes(self.read_sock_output(sock=chan, terminator=terminator)))

        if printout:
            logger.info('{}'.format(output))

        return output

    def update_router(self, pub_key: str) -> None:
        # 1. Send generated Public Key to router
        url = 'http://%s/api/config/certmgmt/public_keys' % self.host
        data = {"data": '{"key": "%s"}' % (pub_key)}
        try:
            ret = requests.post(url, data=data, auth=(self.username, self.password)).json()
        except Exception as exc:
            logger.error(f'Error communicating with config store (posting) {type(exc)} {exc}')
            raise Exception('Error posting public key') from exc

        if not ret or ret.get('data') is None:
            logger.error('Error communicating with config store (post)')
            raise Exception('Error getting public key key_id')

        self.key_data['key_index'] = ret['data']

        # 2. Get key's index to put under self.username's key list
        url = 'http://%s/api/config/certmgmt/public_keys/%d/_id_' % (self.host, self.key_data['key_index'])
        try:
            ret = requests.get(url, auth=(self.username, self.password)).json()
        except Exception as exc:
            logger.error(f'Error communicating with config store (getting) {type(exc)} {exc}')
            raise Exception('Error getting public key key_id') from exc

        if not ret or ret.get('data') == None:
            logger.error('Error communicating with config store (get)')
            raise Exception('Error getting public key key_id')

        self.key_data['key_uid'] = ret.get('data')

        # 3. Grab self.username's user index
        url = 'http://%s/api/config/system/users' % (self.host)
        try:
            users = requests.get(url, auth=(self.username, self.password)).json()
        except Exception as exc:
            logger.error(f'Error communicating with config store {type(exc)} {exc}')
            raise Exception('Error getting user config store data') from exc

        if not users or users.get('data') is None:
            logger.error('Error communicating with config store (user get)')
            raise Exception('Error getting user config store data')

        self.key_data['user_index'] = 0
        for x in users['data']:
            if x['username'] == self.username:
                break
            self.key_data['user_index'] = self.key_data['user_index'] + 1

        # 4. Post key index into self.username's key list
        url = 'http://%s/api/config/system/users/%d/keys' % (self.host, self.key_data['user_index'])
        data = {"data": '{"key_id": "%s"}' % (ret.get('data'))}
        try:
            ret = requests.post(url, data=data, auth=(self.username, self.password)).json()
        except Exception as exc:
            logger.error(f'Error communicating with config store {type(exc)} {exc}')
            raise Exception('Error posting user key data') from exc

        if not ret or ret.get('data') is None:
            logger.error('Error communicating with config store (user post)')
            raise Exception('Error posting user key data')

        self.key_data['user_key_index'] = ret.get('data')

    def restore_router(self) -> None:
        url = 'http://%s/api/config/certmgmt/public_keys/%d' % (self.host, self.key_data['key_index'])
        ret = requests.delete(url, auth=(self.username, self.password)).json()

        url = 'http://%s/api/config/system/users/%d/keys/%d' % (self.host, self.key_data['user_index'], self.key_data['user_key_index'])
        ret = requests.delete(url, auth=(self.username, self.password)).json()

    # ############################
    #  INFO GATHERING FUNCTIONS  #
    # ############################

    def connect(self, pk=False) -> None:
        """
        Create a connection to the SSH host
        Returns: success of making the connection

        """
        connected = False
        if pk:
            pkey = RSAKey.generate(bits=2048)
            self.update_router(pkey.get_base64())

            try:
                if self.connect_timeout is not None:
                    logger.info('Connect timeout %s' % self.connect_timeout)
                    self.ssh.connect(
                        self.host,
                        port=self.port,
                        username=self.username,
                        pkey=pkey,
                        timeout=self.connect_timeout)
                else:
                    self.ssh.connect(self.host, port=self.port, username=self.username, pkey=pkey)
            except (paramiko.SSHException, Exception) as e:
                logger.info('Got ssh connect error [%s]' % e)
            else:
                connected = True
        else:
            for i in range(3):
                try:
                    if self.connect_timeout is not None:
                        logger.info('Connect timeout %s' % self.connect_timeout)
                        self.ssh.connect(
                            self.host,
                            port=self.port,
                            username=self.username,
                            password=self.password,
                            timeout=self.connect_timeout)
                    else:
                        self.ssh.connect(self.host, port=self.port, username=self.username, password=self.password)
                except (paramiko.SSHException, Exception) as e:
                    logger.info('Got ssh connect error [%s]' % e)
                else:
                    connected = True
                    break

        if not connected:
            logger.warning('Could not connect to ssh client')
            raise Exception('Could not connect to ssh client')

    def close(self, close=True) -> None:
        """
        Close the connection to SSH host
        Args:
            close: to close or not to close
        """
        if close:
            try:
                self.ssh.close()
            except (Exception, paramiko.SSHException):
                pass

        if self.key_data:
            self.restore_router()

    def do_complex_cmd(self, cmd: Union[str, List[str]], close=True, raiseexception=False) -> List[str]:
        """
        Send a command, or a list of commands, to the host
        This method of sending commands first invokes a shell
        session and issues commands using chan.send()
        Args:
            cmd: A string or list of commands to be sent to the host
            close: Close the transport channel after sending the command
            raiseexception: if True, will raise exceptions encountered sending commands rather than eating them.
                            but waits until the channel is closed before raising it.

        Returns: A list of responses from the issued commands

        """
        logger.debug('complex command: %s' % cmd)
        chan = None
        responses = []
        exceptionraised = False
        excepval = None

        try:
            self.connect()
            chan = self.ssh.invoke_shell()
            responses = []
            # do stuff
            if isinstance(cmd, str):
                cmd = [cmd]

            for command in cmd:
                logger.debug(f'reading before sending:')  # reads to initial prompt or to next prompt
                responses.append(self.return_output(chan, printout=True))
                if command == "Ctrl+C":
                    chan.send(chr(3))
                elif command == "Ctrl+D":
                    chan.send(chr(4))
                else:
                    logger.info(f'sending: {command}')
                    chan.send(f'{command}\n')

            logger.debug('All commands sent, waiting for final output')
            responses.append(self.return_output(chan, printout=True))
            # exit from sh
            chan.send('exit\n')
            responses.append(self.return_output(chan, printout=False))
            if not chan.closed:
                # exit from ssh
                chan.send('exit\n')
                # any final output?  Append it to the previous response
                responses[1] += self.return_output(chan, printout=False)
        except (paramiko.SSHException, Exception) as e:
            logger.info('Got exception in do_complex_cmd [%s]' % e)
            exceptionraised = True
            excepval = e
        try:
            if chan is not None:
                chan.close()
        except (Exception, paramiko.SSHException):
            pass

        # Now close the ssh session
        self.close(close)

        if raiseexception and exceptionraised:
            raise excepval
        logger.debug('RESPONSES:\n{}'.format(responses))
        return responses

    def do_cmd(self, cmd: str, close=True, readlines=True, wait_for_stdout=True, timeout: int = 300) -> str:
        """
        Send a command to the host. This method of sending commands
        uses exec_command() -> transport.open_session()
        This is the least shell method of issuing commands and
        does not invoke a shell session on the server.
        Args:
            cmd: str command to execute
            close: to close or not to close
            readlines: to readlines or to read entire output
            wait_for_stdout: True - run the command and wait for the stdout
                     False - run command and return
            timeout: int to set timeout for call

        Returns: output from stdout

        """
        logger.info('Returning {0} command from router at {1}'.format(cmd, self.host))
        result = None
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command(cmd, timeout=timeout)
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in do_cmd [%s]' % e)
        else:
            if wait_for_stdout:
                if readlines:
                    result = stdout.readlines()
                else:
                    result = stdout.read()
        self.close(close)
        return result

    def do_sftp_file_transfer(self, remote_file_path: str, local_file_path: str) -> None:
        """
        Copy a remote file (remotepath) from the SFTP server on the host to the local host as localpath
        The sftp channel will be closed after transfer is complete
        Args:
            remote_file_path: path to file on remote host
            local_file_path: path to store file on local host

        """
        logger.info('Executing remote file transfer from host: {}  files: {} to {}'.format(
            self.host, remote_file_path, local_file_path))

        try:
            self.connect()
            sftp = self.ssh.open_sftp()
            sftp.get(remote_file_path, local_file_path)
        except (Exception, paramiko.SSHException) as e:
            logger.info('{} occurred during do_sftp_file_transfer {} from {}'.format(e, remote_file_path, self.host))

        self.close()

    def do_sftp_remove_file(self, remote_file_path: str) -> None:
        """
        Delete a file on remote host
        The sftp channel will be closed after transfer is complete
        Args:
            remote_file_path: path to file on remote host

        """
        logger.info('Executing remote file deletion from host: {}  file: {}'.format(self.host, remote_file_path))

        try:
            self.connect()
            sftp = self.ssh.open_sftp()
            sftp.remove(remote_file_path)
        except (Exception, paramiko.SSHException) as e:
            logger.info('{} occurred during do_sftp_remove_file {} from {}'.format(e, remote_file_path, self.host))

        self.close()

    def do_sftp_stat(self, remote_file_path: str) -> SFTPAttributes:
        """
        Retrieve information about a file on the remote system
        Args:
            remote_file_path: path to file on remote host

        Returns: the os.stat values for the file, None if file does not exist

        """
        logger.info('Executing remote file stat collection from host: {}  file: {}'.format(self.host, remote_file_path))
        value = None
        try:
            self.connect()
            sftp = self.ssh.open_sftp()
            value = sftp.stat(remote_file_path)
        except (Exception, paramiko.SSHException) as e:
            logger.info('{} occurred during do_sftp_stat {} from {}'.format(e, remote_file_path, self.host))

        self.close()
        return value

    # ############################
    #  Router specific FUNCTIONS #
    # ############################

    def return_devices(self):
        logger.info('Returning \'devices\' command from router at %s' % self.host)
        foo = None
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command('devices')
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in return_devices [%s]' % e)
        else:
            foo = stdout.readlines()
        self.close()
        return foo

    def return_lan(self):
        logger.info('Returning \'lan\' command from router at %s' % self.host)

        self.ssh.connect(self.host, port=self.port, username=self.username, password=self.password)
        stdin, stdout, stderr = self.ssh.exec_command('lan')
        foo = stdout.readlines()
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command('lan')
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in return_lan [%s]' % e)
        else:
            foo = stdout.readlines()
        self.close()
        return foo

    def return_wan(self):
        logger.info('Returning \'wan\' command from router at %s' % self.host)

        self.ssh.connect(self.host, port=self.port, username=self.username, password=self.password)
        stdin, stdout, stderr = self.ssh.exec_command('wan')
        foo = stdout.readlines()
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command('wan')
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in return_wan [%s]' % e)
        else:
            foo = stdout.readlines()
        self.close()
        return foo

    def return_switch(self):
        logger.info('Returning \'switch\' command from router at %s' % self.host)

        self.ssh.connect(self.host, port=self.port, username=self.username, password=self.password)
        stdin, stdout, stderr = self.ssh.exec_command('switch')
        foo = stdout.readlines()
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command('switch')
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in return_switch [%s]' % e)
        else:
            foo = stdout.readlines()
        self.close()
        return foo

    def tcpdump(self, i, c=30, A=False, e=False, n=False, host=None, not_host=None, port=None, not_port=None, other_args=None):
        """
        -e Print link level header on each dump file
        -A Print each packet in ASCII
        -n Don't convert to names
        """
        logger.info('starting tcpdump capture')
        cmd = 'tcpdump -c {0} -i {1}'.format(c, i)
        if A:
            cmd += ' -A'
        elif n:
            cmd += ' -n'
        elif e:
            cmd += ' -e'
        elif host:
            cmd += ' host {0}'.format(host)
        elif not_host:
            cmd += ' not host {0}'.format(not_host)
        elif port:
            cmd += ' port {0}'.format(port)
        elif not_port:
            cmd += ' not port {0}'.format(not_port)
        elif other_args:
            cmd += ' ' + other_args

        pkts = self.do_complex_cmd(cmd)

        self.close()
        return pkts

    def tcpdump_simple(self, other_args=None):
        """
        -e Print link level header on each dump file
        -A Print each packet in ASCII
        -n Don't convert to names
        """
        logger.info('starting tcpdump capture')
        cmd = 'tcpdump ' + other_args
        logger.info('{}'.format(cmd))
        pkts = self.do_complex_cmd(cmd)
        self.close()
        return pkts

    # ##################
    #  TOOL FUNCTIONS  #
    # ##################

    def from_cp_ping(self, target, count='5', interval='.4', size='56', close=False):
        foo = None
        logger.info('Starting ping from CP router to %s' % target)
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command('ping {0} -c {1} -i {2} -s {3}'.format(
                target, count, interval, size))
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in from_cp_ping [%s]' % e)
        else:
            foo = stdout.readlines()

        self.close(close)
        return foo

    def from_cp_ping6(self, target, count='5', interval='.4', size='56', close=False):
        foo = None
        logger.info('Starting ping6 from CP router to %s' % target)
        try:
            self.connect()
            stdin, stdout, stderr = self.ssh.exec_command('ping6 {0} -c {1} -i {2} -s {3}'.format(
                target, count, interval, size))
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in from_cp_ping6 [%s]' % e)
        else:
            foo = stdout.readlines()

        self.close(close)
        return foo

    def cp_traceroute(self, target, max_ttl='30', F=False, I=False, q='3', w='3'):
        """
        -F  Set the don't fragment bit
        -m  Max time-to-live, default=30
        -I  Use ICMP ECHO instead of UDP datagrams
        -q  Number of probes per TTL (default 3)
        -w  Time in seconds to wait for a response(default 3)
        """
        logger.info('Starting traceroute from CP router to %s' % target)

        if F and I:
            cmd = 'traceroute -F -I -m {0} -q {1} -w {2} {3}'.format(max_ttl, q, w, target)
        elif F:
            cmd = 'traceroute -F -m {0} -q {1} -w {2} {3}'.format(max_ttl, q, w, target)
        elif I:
            cmd = 'traceroute -I -m {0} -q {1} -w {2} {3}'.format(max_ttl, q, w, target)
        else:
            cmd = 'traceroute -m {0} -q {1} -w {2} {3}'.format(max_ttl, q, w, target)

        rt = self.do_complex_cmd(cmd)

        self.close()
        return rt

    # ###########################
    #  CONFIGURATION FUNCTIONS  #
    # ###########################

    def enable_cproot(self, fw_version: Version = None) -> None:
        """Enables cproot login on a router.

        Args:
            fw_version: Firmware version of the router.

        Notes:
            Defaults to the method appropriate for @fw_version >= 6.1.
        """
        self.ssh.connect(self.host, port=self.port, username=self.username, password=self.password)
        try:
            self.connect()
            if fw_version and fw_version < Version('6.1.0'):
                self.ssh.exec_command('set /config/firewall/remote_admin/techsupport_access true')
            else:
                self.ssh.exec_command('set /control/system/techsupport_access true')
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in enable_cproot [%s]' % e)
        self.close()

    def disable_cproot(self):
        self.ssh.connect(self.host, port=self.port, username=self.username, password=self.password)
        try:
            self.connect()
            self.ssh.exec_command('set /control/system/techsupport_access false')
        except (Exception, paramiko.SSHException) as e:
            logger.info('Got exception in disable_cproot [%s]' % e)
        self.close()

    # ###########################################
    #  PARSING/ANALYSIS/VERIFICATION FUNCTIONS  #
    # ###########################################

    def get_router_licenses(self):

        # query the license key database
        result = self.do_cmd('fm get keydb -s')
        result = result[0]

        # format the response for json acceptance
        result = result.replace('\x00', '')
        result = result.replace("'", "\"")
        d = json.loads(result)

        license_list = []
        for key in d['Keys']:
            license_list.append(d['Keys'][key]['Name'])
        logger.info('The router has the following features:\n{}'.format(license_list))
        return license_list

    def parse_cp_ping(self, lines, target):
        response = False
        lines_ln = len(lines)
        while lines_ln >= 0:
            if '100% packet loss' in lines[lines_ln - 1]:
                logger.info('No response from %s' % target)
                return 0
            elif 'from {0}: icmp_req'.format(target) in lines[lines_ln - 1]:
                response = True
            lines_ln -= 1
        if response is True:
            logger.info('We have a response from %s' % target)
            return 1

    def parse_cp_ping6(self, lines, target):
        response = False
        lines_ln = len(lines)
        while lines_ln >= 0:
            if '100% packet loss' in lines[lines_ln - 1]:
                logger.info('No response from %s' % target)
                return 0
            elif 'from {0}: icmp_req'.format(target) in lines[lines_ln - 1]:
                response = True
            lines_ln -= 1
        if response is True:
            logger.info('We have a response from %s' % target)
            return 1

    def parse_tcpdump(self, lines):
        lines_ln = len(lines[1])
        logger.info('{}'.format(lines_ln))
        if lines_ln < TCPDUMP_EMPTY_LEN:
            logger.error('tcpdump appears to have captured nothing')
            return 0

    def verify_contains(self, lines, string):
        lines_ln = len(lines)
        while lines_ln >= 0:
            if string in lines[lines_ln - 1]:
                logger.info('Found \'%s\' in the returned responses' % string)
                return 1
            lines_ln -= 1


if __name__ == "__main__":
    """
    Sanity test of sshclient_paramiko module
    """
    r1 = SSHShellClient('172.11.0.150', username='cproot', password='14158362', port=22)

    tcp = r1.do_complex_cmd('tcpdump -i eth0 icmp -c 1')
    r1.parse_tcpdump(tcp)

    try:
        r1.close()
    except paramiko.SSHException:
        logger.info('Could not close ssh')
        pass
