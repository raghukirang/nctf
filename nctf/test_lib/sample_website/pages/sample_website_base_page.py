from selenium.webdriver.common.by import By

import nctf.test_lib.base.pages as JWP


class SampleWebsiteBasePage(JWP.UIPage):
    WEB_SERVICE = 'sample_website'

    class ContentRegion(JWP.UIRegion):
        ROOT_LOCATOR = (By.ID, 'content')

    class FooterRegion(JWP.UIRegion):
        class Center(JWP.UIRegion):
            ROOT_LOCATOR = (By.XPATH, '//*[@id="page-footer"]/div')

            @property
            def style_break(self) -> JWP.UIElement:
                return JWP.UIElement(
                    'Footer Separator', self, strategy=By.XPATH, locator='//*[@id="page-footer"]/div/hr')

            @property
            def title(self) -> JWP.UIElement:
                return JWP.UIElement('Footer Title', self, strategy=By.XPATH, locator='//*[@id="page-footer"]/div/div')

            @property
            def link(self) -> JWP.UIElement:
                return JWP.UIElement('Footer Link', self, strategy=By.LINK_TEXT, locator='Elemental Selenium')

            @property
            def link_by_full_link_text(self) -> JWP.UIElement:
                return JWP.UIElement(
                    'Footer Link By.LINK_TEXT', self, strategy=By.LINK_TEXT, locator='Elemental Selenium')

            @property
            def link_by_partial_link_text(self) -> JWP.UIElement:
                return JWP.UIElement(
                    'Footer Link By.PARTIAL_LINK_TEXT', self, strategy=By.PARTIAL_LINK_TEXT, locator='Elemental')

            @property
            def link_by_tag_name(self) -> JWP.UIElement:
                return JWP.UIElement('Footer Link By.TAG_NAME', self, strategy=By.TAG_NAME, locator='a')

            @property
            def link_by_xpath(self) -> JWP.UIElement:
                return JWP.UIElement('Footer Link By.XPATH', self, strategy=By.XPATH, locator='.//a')

            @property
            def link_by_page_rooted_xpath(self) -> JWP.UIElement:
                return JWP.UIElement('Footer Link By.XPATH', self, strategy=By.XPATH, locator='//a')

        @property
        def center(self) -> Center:
            return SampleWebsiteBasePage.FooterRegion.Center(self)

        class CenterByRelativeXPath(Center):
            ROOT_LOCATOR = (By.XPATH, './div')

        @property
        def center_by_relative_xpath(self) -> CenterByRelativeXPath:
            return SampleWebsiteBasePage.FooterRegion.CenterByRelativeXPath(self)

        class CenterByConstruction(Center):
            ROOT_LOCATOR = (By.XPATH, './div')

        @property
        def center_by_construction(self) -> CenterByConstruction:
            return SampleWebsiteBasePage.FooterRegion.CenterByConstruction(
                self, strategy=By.XPATH, locator='//*[@id="page-footer"]/div')

        class CenterByRelativeConstruction(Center):
            ROOT_LOCATOR = (By.XPATH, './div')

        @property
        def center_by_relative_xpath_construction(self) -> CenterByRelativeConstruction:
            return SampleWebsiteBasePage.FooterRegion.CenterByRelativeConstruction(
                self, strategy=By.XPATH, locator='./div')

    @property
    def flash_messages(self) -> JWP.UIElement:
        return JWP.UIElement(name="flash messages", parent=self, strategy=By.ID, locator="flash-messages")

    def flash_messages_by_class(self) -> JWP.UIElement:
        return JWP.UIElement(name="flash messages", parent=self, strategy=By.CLASS_NAME, locator="large-12")

    @property
    def content(self) -> ContentRegion:
        return SampleWebsiteBasePage.ContentRegion(self)

    @property
    def footer(self) -> FooterRegion:
        return SampleWebsiteBasePage.FooterRegion(self, strategy=By.ID, locator='page-footer')
