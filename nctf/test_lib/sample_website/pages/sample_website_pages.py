from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages

from .sample_dynamic_content import SampleDynamicLoadingIndexPage
from .sample_editor_page import SampleEditorPage
from .sample_frames import SampleFramesPage
from .sample_website_base_page import SampleWebsiteBasePage
from .sample_windows import SampleWindowLoadingPage


class ABTestPage(SampleWebsiteBasePage):

    # set up the URL_TEMPLATE to be an absolute URL for testing purposes
    URL_TEMPLATE = '/abtest'

    # nothing to model on this page.  using it for its fully formed URL in the URL_TEMPLATE with no base_url


class SampleLoginPage(SampleWebsiteBasePage):

    URL_TEMPLATE = "/login"

    class LoginPageContentRegion(SampleWebsiteBasePage.ContentRegion):
        @property
        def heading(self) -> pages.UIElement:
            return pages.UIElement(name="heading", parent=self, strategy=By.XPATH, locator='//*[@id="content"]/div/h2')

        @property
        def heading_by_relative_xpath(self) -> pages.UIElement:
            return pages.UIElement(name="heading", parent=self, strategy=By.XPATH, locator='./div/h2')

        @property
        def subheader(self) -> pages.UIElement:
            return pages.UIElement(name="Instructions", parent=self, strategy=By.XPATH, locator='./h4')

        @property
        def username(self) -> pages.UIElement:
            return pages.UIElement("Username Field", self, strategy=By.ID, locator="username")

        @property
        def username_by_id(self) -> pages.UIElement:
            return pages.UIElement("Username Field", self, strategy=By.ID, locator="username")

        @property
        def username_by_name(self) -> pages.UIElement:
            return pages.UIElement("Username Field", self, strategy=By.NAME, locator="username")

        @property
        def password(self) -> pages.UIElement:
            return pages.UIElement("Password Field", self, strategy=By.ID, locator="password")

    @property
    def content(self) -> LoginPageContentRegion:
        return SampleLoginPage.LoginPageContentRegion(self)

    # The login button is modeled as a button on the login page here instead of in the login form content
    @property
    def login_button(self) -> pages.UIButtonElement:
        return pages.UIButtonElement(
            name='Login Button', parent=self, strategy=By.XPATH, locator='//*[@id="login"]/button')


class SampleWebsiteIndexPage(SampleWebsiteBasePage):
    """This is the top-level page for the test sample website."""

    class SampleWebsiteIndexPageContentRegion(SampleWebsiteBasePage.ContentRegion):
        @property
        def heading(self) -> pages.UIElement:
            return pages.UIElement(name="heading", parent=self, strategy=By.CLASS_NAME, locator="heading")

        @property
        def title(self):
            return pages.UIElement(
                name="title of content", parent=self, strategy=By.XPATH, locator='//*[@id="content"]/h2')

        @property
        def ab_testing_link(self):
            return pages.UIPageLinkElement(
                name="ab_testing",
                parent=self,
                page_cls=ABTestPage,
                strategy=By.XPATH,
                locator='//*[@id="content"]/ul/li[1]/a')

        @property
        def dynamic_loading_link(self):
            return pages.UIPageLinkElement(
                'Dynamic Loading link',
                self,
                SampleDynamicLoadingIndexPage,
                strategy=By.XPATH,
                locator='//a[text()="Dynamic Loading"]')

        @property
        def login_link(self):
            return pages.UIPageLinkElement(
                name="typos",
                parent=self,
                page_cls=SampleLoginPage,
                strategy=By.XPATH,
                locator='//*[@id="content"]/ul/li[18]/a')

        @property
        def frames_link(self):
            return pages.UIPageLinkElement(
                name="Nested Frames",
                parent=self,
                page_cls=SampleFramesPage,
                strategy=By.XPATH,
                locator='//*[@id="content"]/ul/li[19]/a')

        @property
        def editor_link(self):
            return pages.UIPageLinkElement(
                name="Editor",
                parent=self,
                page_cls=SampleEditorPage,
                strategy=By.XPATH,
                locator='//*[@id="content"]/ul/li[39]/a')

        @property
        def multiple_windows_link(self):
            return pages.UIPageLinkElement(
                name="Tabs",
                parent=self,
                page_cls=SampleWindowLoadingPage,
                strategy=By.XPATH,
                locator='//*[@id="content"]/ul/li[29]/a')

    @property
    def content(self) -> SampleWebsiteIndexPageContentRegion:
        return SampleWebsiteIndexPage.SampleWebsiteIndexPageContentRegion(self)
