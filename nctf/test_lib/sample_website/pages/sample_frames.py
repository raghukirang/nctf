import nctf.test_lib.base.pages as jwp
from nctf.test_lib.sample_website.pages.sample_editor_page import SampleEditorPage

from .sample_website_base_page import SampleWebsiteBasePage


class LeftFramePage(jwp.UIPage):
    URL_TEMPLATE = "/frame_left"

    @property
    def body(self) -> jwp.UIElement:
        return jwp.UIElement(name="Left Body", parent=self, strategy=jwp.By.XPATH, locator="/html/body")


class RightFrame(jwp.UIRegion):
    FRAME_LOCATOR = (jwp.By.XPATH, "/html/frameset/frame[3]")
    ROOT_LOCATOR = (jwp.By.XPATH, "/html")

    @property
    def body(self) -> jwp.UIElement:
        return jwp.UIElement(name="Right Body", parent=self, strategy=jwp.By.XPATH, locator="/html/body")


class MiddleFrameRegion(jwp.UIRegion):
    ROOT_LOCATOR = (jwp.By.XPATH, "/html")

    @property
    def body(self) -> jwp.UIElement:
        return jwp.UIElement(name="Middle Body", parent=self, strategy=jwp.By.XPATH, locator="/html/body")


class TopFrame(jwp.UIRegion):
    FRAME_LOCATOR = (jwp.By.XPATH, "/html/frameset/frame[1]")
    ROOT_LOCATOR = (jwp.By.XPATH, "/html")

    class LeftFramePageInFrame(LeftFramePage):
        FRAME_LOCATOR = (jwp.By.XPATH, "/html/frameset/frame[1]")

    @property
    def left_frame(self) -> LeftFramePageInFrame:
        return TopFrame.LeftFramePageInFrame(parent=self)

    class MiddleFrameRegioninFrame(MiddleFrameRegion):
        FRAME_LOCATOR = (jwp.By.XPATH, "/html/frameset/frame[2]")

    @property
    def middle_frame(self) -> MiddleFrameRegion:
        return TopFrame.MiddleFrameRegioninFrame(self)

    @property
    def right_frame(self) -> RightFrame:
        return RightFrame(self)


class NestedFramesPage(SampleWebsiteBasePage):

    URL_TEMPLATE = "/nested_frames"

    @property
    def top_frame(self) -> TopFrame:
        return TopFrame(self)

    class BottomFrame(jwp.UIRegion):
        FRAME_LOCATOR = (jwp.By.XPATH, "/html/frameset/frame[2]")
        ROOT_LOCATOR = (jwp.By.XPATH, "/html")

        @property
        def body(self):
            return jwp.UIElement(name="Bottom Body", parent=self, strategy=jwp.By.XPATH, locator="/html/body")

    @property
    def bottom_frame(self) -> BottomFrame:
        return NestedFramesPage.BottomFrame(self)


class SampleFramesPage(SampleWebsiteBasePage):
    URL_TEMPLATE = "frames"

    @property
    def nested_frames(self) -> jwp.UIPageLinkElement[NestedFramesPage]:
        return jwp.UIPageLinkElement(
            name="Nested Frames",
            parent=self,
            page_cls=NestedFramesPage,
            strategy=jwp.By.XPATH,
            locator='//*[@id="content"]/div/ul/li[1]/a')

    @property
    def iframe(self) -> jwp.UIPageLinkElement[SampleEditorPage]:
        return jwp.UIPageLinkElement(
            name="Editor",
            parent=self,
            page_cls=SampleEditorPage,
            strategy=jwp.By.XPATH,
            locator='//*[@id="content"]/div/ul/li[2]/a')
