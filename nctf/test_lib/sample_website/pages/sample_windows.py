from nctf.test_lib.base import pages as jwp

from .sample_website_base_page import SampleWebsiteBasePage


class NewWindowPage(jwp.UIPage):
    """Test page with some only text on it, doesn't even have the common elements that the other pages have.
    """
    URL_TEMPLATE = '/windows/new'
    WEB_SERVICE = 'sample_website'

    @property
    def some_text(self):
        return jwp.UIElement('Text', self, strategy=jwp.By.XPATH, locator='/html/body/div/h3')


class SampleWindowLoadingPage(SampleWebsiteBasePage):
    """Landing page with links to windows link pages."""
    URL_TEMPLATE = '/windows'

    @property
    def click_here_link(self):
        return jwp.UIPageLinkElement(
            name='New Window Tab',
            parent=self.content,
            page_cls=NewWindowPage,
            strategy=jwp.By.XPATH,
            locator='//*[@id="content"]/div/a',
            opens_new_window=True)
