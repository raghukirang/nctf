from selenium.webdriver.common.by import By

from nctf.test_lib.base import pages

from .sample_website_base_page import SampleWebsiteBasePage


class DynamicContent(pages.UIRegion):
    # ROOT_ELEMENT was specified dynamically

    def wait_for_region_to_load(self):
        self.text_content.wait_for_displayed(30)

    @property
    def text_content(self):
        return pages.UIElement('Greeting text', self, By.CSS_SELECTOR, 'h4')


class SampleDynamicContentPage2(SampleWebsiteBasePage):
    """Test page with user-initiated dynamic content.

    Clicking the "Start" button will trigger a loading bar for a few seconds,
    which then reveals a dynamically-rendered "Hello World!" message.
    """
    URL_TEMPLATE = '/dynamic_loading/{id}'

    @property
    def start_button(self):  # -> pages.UIRegionLinkElement[DynamicContent]:
        """Click to get the dynamic content, once it appears."""
        return pages.UIRegionLinkElement(
            'Start btn',
            self,
            DynamicContent,
            strategy=By.XPATH,
            locator='//*[@id="start"]/button',
            region_parent=self,
            region_strategy=By.ID,
            region_locator='finish')

    @property
    def dynamic_content(self) -> DynamicContent:
        return DynamicContent(self.content, 'Loaded text', By.ID, 'finish')


class SampleDynamicContentPage1(SampleWebsiteBasePage):
    """Test page with user-initiated dynamic content.

    Clicking the "Start" button will trigger a loading bar for a few seconds,
    which then reveals a dynamically-rendered "Hello World!" message.
    """
    URL_TEMPLATE = '/dynamic_loading/{id}'

    @property
    def start_button(self):  # -> pagesUIRegionLinkElement[DynamicContent]:
        """Click to get the dynamic content, once it appears."""
        return pages.UIRegionLinkElement(
            'Start btn',
            self,
            DynamicContent,
            strategy=By.XPATH,
            locator='//*[@id="start"]/button',
            region_parent=self,
            region_strategy=By.ID,
            region_locator='finish')

    @property
    def dynamic_content(self) -> DynamicContent:
        return DynamicContent(self.content, 'Loaded text', By.ID, 'finish')


class SampleDynamicLoadingIndexPage(SampleWebsiteBasePage):
    """Landing page with links to dynamic testing pages."""
    URL_TEMPLATE = '/dynamic_loading'

    @property
    def example_1_link(self):
        return pages.UIPageLinkElement(
            'Example 1 link',
            self.content,
            SampleDynamicContentPage1,
            strategy=By.XPATH,
            locator='//a[starts-with(., "Example 1:")]',
            id='{id}')

    @property
    def example_2_link(self):
        return pages.UIPageLinkElement(
            'Example 2 link',
            self.content,
            SampleDynamicContentPage2,
            strategy=By.XPATH,
            locator='//a[starts-with(., "Example 2:")]',
            id='2')
