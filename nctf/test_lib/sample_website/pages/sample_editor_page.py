import nctf.test_lib.base.pages as JWP

from .sample_website_base_page import SampleWebsiteBasePage


class FileDropDownRegion(JWP.UIRegion):
    ROOT_LOCATOR = (JWP.By.XPATH, "//i[contains(@class,'mce-i-newdocument')]/parent::div/parent::div/parent::div")

    @property
    def new_document(self):
        return JWP.UIRegionLinkElement(
            name='New Document',
            parent=self,
            strategy=JWP.By.XPATH,
            locator="//i[contains(@class,'mce-i-newdocument')]/parent::div",
            region_cls=EditorContentRegion.Body)


class EditDropdownRegion(JWP.UIRegion):
    ROOT_LOCATOR = (JWP.By.XPATH, "//i[contains(@class,'mce-i-undo')]/parent::div/parent::div/parent::div")

    @property
    def undo(self):
        return JWP.UIRegionLinkElement(
            'Undo',
            parent=self,
            strategy=JWP.By.XPATH,
            locator="//i[contains(@class,'mce-i-undo')]/parent::div",
            region_cls=EditorContentRegion.Body)

    @property
    def redo(self):
        return JWP.UIButtonElement(
            'Redo', parent=self, strategy=JWP.By.XPATH, locator="//i[contains(@class,'mce-i-redo')]/parent::div")

    @property
    def cut(self):
        return JWP.UIButtonElement(
            'Cut', parent=self, strategy=JWP.By.XPATH, locator="//i[contains(@class,'mce-i-cut')]/parent::div")

    @property
    def copy(self):
        return JWP.UIButtonElement(
            'Copy', parent=self, strategy=JWP.By.XPATH, locator="//i[contains(@class,'mce-i-copy')]/parent::div")

    @property
    def paste(self):
        return JWP.UIButtonElement(
            'Paste', parent=self, strategy=JWP.By.XPATH, locator="//i[contains(@class,'mce-i-paste')]/parent::div")


class EditorContentRegion(SampleWebsiteBasePage.ContentRegion):
    """Incomplete decomposition of the this page to show drop down menus
    """

    @property
    def heading(self) -> JWP.UIElement:
        return JWP.UIElement(name="heading", parent=self, strategy=JWP.By.XPATH, locator='//*[@id="content"]/div/h3')

    class MenuBar(JWP.UIRegion):
        ROOT_LOCATOR = (JWP.By.XPATH, "//div[contains(@class,'mce-menubar')]")

        @property
        def file(self):
            return JWP.UIRegionLinkElement(
                "File",
                parent=self,
                region_cls=FileDropDownRegion,
                strategy=JWP.By.XPATH,
                locator='//span[contains(.,"File")]/parent::button/parent::div')

        @property
        def edit(self):
            return JWP.UIRegionLinkElement(
                "Edit",
                parent=self,
                region_cls=EditDropdownRegion,
                strategy=JWP.By.XPATH,
                locator='//span[contains(.,"Edit")]/parent::button/parent::div')

    @property
    def menu_bar(self) -> MenuBar:
        return EditorContentRegion.MenuBar(self)

    class Body(JWP.UIRegion):
        FRAME_LOCATOR = (JWP.By.ID, "mce_0_ifr")
        ROOT_LOCATOR = (JWP.By.ID, "tinymce")

        @property
        def text_area(self) -> JWP.UIElement:
            return JWP.UIElement("Text Area", parent=self, strategy=JWP.By.XPATH, locator="//body")

        def write(self, some_text: str):
            self.text_area.send_keys(some_text)

    @property
    def body(self) -> Body:
        return EditorContentRegion.Body(self)


class SampleEditorPage(SampleWebsiteBasePage):

    URL_TEMPLATE = "/tinymce"
    # purposely set this page default timeout different than the UIPage
    DEFAULT_TIMEOUT = JWP.UIPage.DEFAULT_TIMEOUT + 5

    @property
    def content(self) -> EditorContentRegion:
        return EditorContentRegion(self)
