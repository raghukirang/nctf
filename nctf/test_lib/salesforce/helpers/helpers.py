import time
from typing import List

from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects.account import SFAccount
from nctf.test_lib.salesforce.objects.device import SFDevice
from nctf.test_lib.salesforce.objects.deviceentitlement import SFDeviceEntitlement
from nctf.test_lib.salesforce.objects.devicelicenseallocation import SFDeviceLicenseAllocation
from nctf.test_lib.salesforce.objects.entitlement import SFEntitlement
from nctf.libs.common_library import random_imei
from nctf.libs.common_library import random_serial
from nctf.libs.common_library import WaitTimeoutError
from nctf.libs.salesforce.common_lib import ProductCodes


def create_router_activation(sf_account: SFAccount,
                             primary_account_entitlement: str,
                             mac_address: str,
                             serial_number: str = None,
                             imei: str = None,
                             ) -> SFDeviceEntitlement:
    """ Creates a SFDevice entitlement that is a device activation, for the requested mac address.

    Args:
        sf_account:
        primary_account_entitlement: Used to look up the product code for the primary ECM entitlement.
        mac_address: The router's specific mac address of the device.
        serial_number: The device's specific serial number, which is generated if not passed in.
        imei: Industry Mobile Equipment Identifier, which is generated if not passed in.

    Returns:
        The newly created Salesforce Device Entitlement for the specified mac address.

    """
    sf_interface = sf_account.sf_interface
    try:
        primary_product = ProductCodes[primary_account_entitlement].value
    except KeyError:
        raise KeyError("Requested product does not match a valid product code.")
    mac = mac_address.replace(':', '')
    serial_number = random_serial() if serial_number is None else serial_number
    imei = random_imei(salesforce_fix=True) if imei is None else imei

    sf_device = None
    sf_device_id = SFDevice.get_device_sf_id_by_mac_address(sf_interface, mac)
    if sf_device_id:
        sf_device = SFDevice.get_by_sf_id(sf_interface, sf_device_id)
    if sf_device:
        SFDeviceEntitlement.delete_device_entitlements(sf_interface, mac)
    else:
        sf_device = SFDevice.create(sf_interface=sf_interface,
                                    serial_number=serial_number,
                                    mac_address=mac,
                                    imei=imei)

    sf_entitlements = SFEntitlement.entitlements_for_account(sf_interface=sf_interface,
                                                             account_id=sf_account.sf_id,
                                                             entitlement_name=primary_product)
    if len(sf_entitlements) != 1:
        raise ValueError("Failed to find the expected SalesForce entitlement, found: {}".format(len(sf_entitlements)))
    else:
        sf_entitlement = sf_entitlements[0]

    sf_device_entitlement = SFDeviceEntitlement.create(sf_interface=sf_interface,
                                                       sf_device=sf_device,
                                                       sf_entitlement=sf_entitlement)

    if not sf_device_entitlement.wait_for_bsfid(60):
        raise WaitTimeoutError("Failed to verify the expected device entitlement.")

    wait_for_dla_by_device_sf_id(sf_account.sf_interface, sf_device, poll=10)

    return sf_device_entitlement


def wait_for_dla_by_device_sf_id(sf_interface: _SalesforceConnection,
                                 sf_device: SFDevice,
                                 poll: int = 60,
                                 poll_interval: int = 1) -> List[str]:
        """
        Retrieves a list of Device License Allocation Salesforce IDs assigned to a given SalesForce Device.

        Args:
            sf_interface:
            sf_device: Salesforce ID of specified Device
            poll: Number to set for the query to run
            poll_interval: Interval to run the query

        Returns:
            Salesforce ID of Device License Allocations assigned to the Device.
        """
        sf_id = sf_device.sf_id
        dla_id = None
        try:
            dla_id = SFDeviceLicenseAllocation.get_device_license_allocation_by_device_sf_id(sf_interface, sf_id)
        except ValueError:
            time.sleep(poll_interval)
        last_possible_call = time.monotonic() + poll - poll_interval
        while not dla_id and time.monotonic() < last_possible_call:
            try:
                dla_id = SFDeviceLicenseAllocation.get_device_license_allocation_by_device_sf_id(sf_interface, sf_id)
            except ValueError:
                time.sleep(poll_interval)
        return dla_id
