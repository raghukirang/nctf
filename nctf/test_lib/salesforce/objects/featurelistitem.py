"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
from nctf.test_lib.salesforce.objects.feature import SFFeature
from nctf.test_lib.salesforce.objects.featurelist import SFFeatureList
from nctf.test_lib.salesforce.objects.object import SFObject


class SFFeatureListItem(SFObject):
    sf_license = None
    sf_feature = None
    sf_feature_list = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFFeatureListItem, self).__init__('Feature_List_Item__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface):
        '''
        cls.sf_feature = SFFeature.create_random(sf_interface)
        cls.sf_feature_list = SFFeatureList.create_random(sf_interface)

        required_params = {'Feature__c': cls.sf_feature.sf_id, 'Feature_List__c': cls.sf_feature_list.sf_id}
        return cls(sf_interface, required_params=required_params)
        '''

        # TODO: Implement
        raise NotImplementedError('Work in Progress...')

    @classmethod
    def create(cls, sf_interface, sf_feature, sf_feature_list):
        cls.sf_feature = sf_feature if isinstance(sf_feature, SFFeature) else None
        cls.sf_feature_list = sf_feature_list if isinstance(sf_feature_list, SFFeatureList) else None

        if cls.sf_feature is None:
            raise ValueError('Feature: {} is not of type SFFeature!'.format(sf_feature))

        if cls.sf_feature_list is None:
            raise ValueError('FeatureList: {} is not of type SFFeatureList!'.format(sf_feature_list))

        required_params = {'Feature__c': cls.sf_feature.sf_id, 'Feature_List__c': cls.sf_feature_list.sf_id}
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    def set_feature(self, sf_feature):
        self.sf_feature = sf_feature

    def set_feature_list(self, sf_feature_list):
        self.sf_feature_list = sf_feature_list

    def set_license(self, sf_license):
        self.sf_license = sf_license
