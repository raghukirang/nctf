from nctf.test_lib.salesforce.objects.object import SFObject


class SFOrderServiceLine(SFObject):
    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFOrderServiceLine, self).__init__('kugo2p__SalesOrderServiceLine__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create(cls, sf_interface, required_params=None):
        return cls(sf_interface, required_params)

    def sf_delete(self):
        # Note: Unable to delete this object. Do Nothing!
        pass
