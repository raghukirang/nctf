import datetime
from typing import List

from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects.object import SFObject


class SFOrder(SFObject):
    ops_order = False

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFOrder, self).__init__('kugo2p__SalesOrder__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create(cls, sf_interface):
        return cls(sf_interface)

    @classmethod
    def create_order(cls, sf_interface, product_code, sf_account, sf_contact, origin='ECM', validate_product=True):
        """ Submit a new order to salesforce. """
        from nctf.test_lib.salesforce.objects.orderserviceline import SFOrderServiceLine
        from nctf.libs.salesforce.common_lib import BundleProducts
        from nctf.libs.salesforce.common_lib import ProductCodes


        if not isinstance(product_code, ProductCodes):
            product_name = product_code
            product_value = product_code
            if validate_product:
                raise ValueError('Product Code: \'{}\' is not apart of the Enum \'ProductCodes\''.format(product_code))
        else:
            product_name = product_code.name
            product_value = product_code.value
            if product_code.name in BundleProducts.__members__:
                cls.logger.warning("Attempting order of a bundled product (generally unsupported)")

        # Retrieve Product from Product Code
        product = sf_interface.query(
            "SELECT Id FROM kugo2p__AdditionalProductDetail__c WHERE kugo2p__ProductCode__c = '{}'".format(product_value))

        if not product['records']:
            raise KeyError('No such product: {} -> {}'.format(product_name, product_value))

        product = product['records'][0]

        # Create Order
        cls.logger.debug('Creating Order: {} for Account: {}'.format(product_name, sf_account.sf_id))
        order_params = {
            "Kugo2p__Account__c": sf_account.sf_id,
            "Origin__c": origin,
            "Apex_Context__c": True,
            "kugo2p__PaymentMethod__c": "Invoice",
            "kugo2p__PriceBookName__c": "CP Master Price Book",
            "kugo2p__ContactBilling__c": sf_contact.sf_id,
            "kugo2p__ContactBuying__c": sf_contact.sf_id,
            "kugo2p__ContactShipping__c": sf_contact.sf_id
        }
        sf_order = cls(sf_interface, required_params=order_params)

        # Create Order Service Line
        service_params = {
            "kugo2p__SalesOrder__c": sf_order.sf_id,
            "kugo2p__Service__c": product['Id'],
            "kugo2p__Quantity__c": 10,
            "kugo2p__DateServiceStart__c": str(datetime.datetime.utcnow().date()),
        }
        sf_orderserviceline = SFOrderServiceLine.create(sf_interface, service_params)

        sf_order.ops_order = True
        return sf_order, sf_orderserviceline

    def release_ops_order(self):
        self.sf_update({"kugo2p__RecordStatus__c": 'Released'})
        self.logger.debug("Released Order: {}".format(self.sf_id))

    @classmethod
    def orders_for_account(self, sf_interface: _SalesforceConnection, account_id: str) -> List[str]:
        """ Retrieve Salesforce Orders on a given Salesforce account.

        Args:
            sf_interface: Salesforce Fixture
            account_id: Salesforce ID of the account to get a list of entitlements from.

        Returns:
            A list of strings that are the salesforce order IDs for the given account ID.

        """
        order_query = "SELECT Id FROM kugo2p__SalesOrder__c WHERE kugo2p__Account__c = '{}'".format(account_id)

        order_data = sf_interface.query(order_query)

        if not order_data['records']:
            raise KeyError('No order data for account: {}'.format(account_id))

        orders = []
        for order in order_data['records']:
            orders.append(order['Id'])

        return orders

    def sf_delete(self):
        if self.ops_order is True:
            from nctf.test_lib.salesforce.objects.entitlement import SFEntitlement

            # Delete Entitlement
            entitlement_query = "SELECT Id FROM Entitlement WHERE Order__c = '{}'".format(self.sf_id)
            query = self.sf_interface.query(entitlement_query)
            for entry in query['records']:
                entitlement = SFEntitlement.get_by_sf_id(self.sf_interface, entry['Id'])
                entitlement.sf_delete()

            # Delete Service Contract
            contract_query = "SELECT ServiceContract__c FROM kugo2p__SalesOrder__c WHERE Id = '{}'".format(self.sf_id)
            query = self.sf_interface.query(contract_query)
            for entry in query['records']:
                if entry['ServiceContract__c'] is not None:
                    contract = SFObject('ServiceContract', self.sf_interface, sf_id=entry['ServiceContract__c'])
                    contract.sf_delete()

            # Delete Order
            super(SFOrder, self).sf_delete()
