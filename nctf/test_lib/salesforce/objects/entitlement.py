"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
import datetime
import time
from typing import List

from nctf.libs.common_library import random_string
from nctf.libs.common_library import require_type
from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects import account
from nctf.test_lib.salesforce.objects import product
from nctf.test_lib.salesforce.objects.featurelist import SFFeatureList
from nctf.test_lib.salesforce.objects.object import SFObject
import pytz


class SFEntitlement(SFObject):
    sf_account = None
    sf_license = None
    sf_feature_list = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFEntitlement, self).__init__('Entitlement', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface: _SalesforceConnection) -> None:
        raise NotImplementedError('Work in Progress...')

    @classmethod
    def create(cls,
               sf_interface: _SalesforceConnection,
               sf_account: 'SFAccount',
               sf_feature_list: SFFeatureList,
               additional_params: dict = None) -> 'SFEntitlement':
        """Creates an entitlement on a given account for a specified feature list.

        Args:
            sf_interface: Authenticated interface to Salesforce.
            sf_account: The account to apply the entitlement on.
            sf_feature_list: The feature list to entitle the account for.
            additional_params: Additional keyword parameters to pass to the
                SFEntitlement constructor.

        Returns:
            A new entitlement representative of the Salesforce entitlement data.
        """
        require_type(sf_account, account.SFAccount)
        require_type(sf_feature_list, SFFeatureList)

        required_params = {
            'Name': random_string(20, 'Entitlement_', False),
            'AccountId': sf_account.sf_id,
            'Feature_List__c': sf_feature_list.sf_id,
            'Apex_Context__c': True,
            'Devices_under_Contract__c': 10
        }
        required_params.update(additional_params or {})

        new_entitlement = cls(sf_interface, required_params=required_params)
        new_entitlement.sf_account = sf_account
        new_entitlement.sf_feature_list = sf_feature_list
        return new_entitlement

    @classmethod
    def create_for_product(cls, sf_interface: _SalesforceConnection, sf_account: 'SFAccount', sf_feature_list: SFFeatureList,
                           sf_product: 'SFProduct') -> 'SFEntitlement':
        """Creates an entitlement on a given account, based on a given product.

        This is intended to mimic the Entitlements created via the Order process.
        The product name will be used (best-effort) to set a valid entitlement
        term, e.g. 365 days if the product code contains "1YR". If there is no
        known match, it will default to a 30-day term.

        Additionally, the usual user-creation process normally triggered by
        the Order process will be triggered if the provided `sf_account` has a
        `contact` member value that is valid.

        Args:
            sf_interface: Authenticated interface to Salesforce.
            sf_account: The account to apply the entitlement on.
            sf_feature_list: The feature list to entitle the account for.
            sf_product: The product, used to determine the term and set the
                Name and Product_Code__c fields (to mimic Orders).

        Returns:
            A new entitlement representative of the Salesforce entitlement data.
        """
        term_map = {'DAY': 1, 'DEMO': 30, 'QTR': 365 // 4, '1YR': 365, '3YR': 365 * 3, '5YR': 365 * 5}

        require_type(sf_product, product.SFProduct)
        product_code = sf_product.sf_attributes['ProductCode']

        for term in term_map:
            if term in product_code:
                entitled_days = term_map[term]
                break
        else:
            entitled_days = 30

        start_date = datetime.datetime.utcnow().date()
        end_date = start_date + datetime.timedelta(days=entitled_days)
        product_params = {
            'Name': product_code,
            'Product_Code__c': sf_product.sf_id,
            'StartDate': start_date.isoformat(),
            'EndDate': end_date.isoformat()
        }
        if sf_account.contact is not None:
            product_params.update({'End_User__c': sf_account.contact.sf_id})

        return cls.create(sf_interface, sf_account, sf_feature_list, product_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    @classmethod
    def get_by_order_id(cls, sf_interface: _SalesforceConnection, order_id: 'SFOrder') -> List['SFEntitlement']:
        ret = []
        entitlement_query = "SELECT Id FROM Entitlement WHERE Order__c = '{}'".format(order_id)
        matching_records = sf_interface.query(entitlement_query)['records']
        for entry in matching_records:
            ret.append(cls.get_by_sf_id(sf_interface, entry['Id']))
        return ret

    def get_license_ids(self):
        self.logger.debug('\tGetting License Ids...')
        license_objs = self.sf_interface.query("SELECT Id FROM License__c WHERE Entitlement__c = '{}'".format(
            self.sf_id))['records']

        license_ids = []
        for license_obj in license_objs:
            self.logger.debug('\tFound License: {}'.format(license_obj['Id']))
            license_ids.append(license_obj['Id'])

        return license_ids

    @classmethod
    def entitlements_for_account(cls, sf_interface: _SalesforceConnection, account_id: str,
                                 entitlement_name: str = None) -> List['SFEntitlement']:
        """Gets a list of entitlements on a given Salesforce account.

        Args:
            sf_interface: Salesforce fixture/interface.
            account_id: Salesforce ID of the account to get a list of entitlements from.
            entitlement_name: If specified, get only entitlements that match this name, eg. 'ECM-PRM-MT1YR'

        Returns:
            A list of entitlement objects for the specified account. May be an empty list.
        """
        if entitlement_name:
            entitlement_query = "SELECT Id FROM Entitlement WHERE AccountId = '{}' AND Name = '{}'".format(
                account_id, entitlement_name)
        else:
            entitlement_query = "SELECT Id FROM Entitlement WHERE AccountId = '{}'".format(account_id)

        entitlement_data = sf_interface.query(entitlement_query)

        entitlements = []
        for entitlement in entitlement_data['records']:
            entitlements.append(SFEntitlement.get_by_sf_id(sf_interface, entitlement['Id']))

        return entitlements

    def set_expiry(self, days_in_future: int) -> None:
        """Sets the expiry date to a number of days in the future.

        If the specified number of days is <= 0, the start date will also be
        updated to be before the expiry date.

        Args:
            days_in_future: The number of days ahead, relative to current
                system date, to set the entitlement end date to.
        """

        today = datetime.datetime.now(pytz.UTC)
        update_expiry = {'EndDate': (today + datetime.timedelta(days=days_in_future)).date().isoformat()}
        if days_in_future <= 0:
            update_expiry['StartDate'] = (today + datetime.timedelta(days=days_in_future - 1)).date().isoformat()

        self.sf_update(update_expiry)

    @classmethod
    def expire_all_on_account(cls, sf_interface: _SalesforceConnection, account_id: str) -> None:
        """
        Queries a Salesforce Account for Entitlements and expires them by changing the StartDate to 2 days before the
        current date and EndDate 1 day before the current date. Once expired in Salesforce this will require waiting for
        the 60 second taxman sync to for the entitlement features to be disabled in ECM.

        Args:
            sf_interface: Salesforce Fixture
            account_id: Salesforce ID of specified Salesforce Account
        """
        entitlements = cls.entitlements_for_account(sf_interface, account_id)
        if not entitlements:
            raise ValueError("No entitlements found for Account '{}'".format(account_id))

        for entitlement in entitlements:
            entitlement.set_expiry(-1)

    @classmethod
    def expire_by_name_on_account(cls, sf_interface: _SalesforceConnection, account_id: str, entitlement_name: str) -> None:
        """
        Queries a Salesforce Account for Entitlements with a specific name and expires them by changing the
        StartDate to 2 days before the current date and EndDate 1 day before the current date. Once expired in
        Salesforce this will require waiting for the 6  0 second taxman sync to for the entitlement features to be
        disabled in ECM.

        Args:
            sf_interface: Salesforce Fixture
            account_id: Salesforce ID of specified Salesforce Account
            entitlement_name: Entitlement Name i.e. 'ECM-PRM-MT1YR' or 'NCE-GWPRM-1YR'
        """
        entitlements = cls.entitlements_for_account(sf_interface, account_id, entitlement_name)
        if not entitlements:
            raise ValueError("Entitlement '{}' not found in Account '{}'".format(entitlement_name, account_id))

        for entitlement in entitlements:
            entitlement.set_expiry(-1)

    def set_account(self, sf_account):
        self.sf_account = sf_account

    def set_license(self, sf_license):
        self.sf_license = sf_license

    def set_feature_list(self, sf_feature_list):
        self.sf_feature_list = sf_feature_list

    def activate(self):
        update = {
            'StartDate': (datetime.datetime.now(pytz.UTC) - datetime.timedelta(days=1)).date().isoformat(),
            'EndDate': (datetime.datetime.now(pytz.UTC) + datetime.timedelta(days=29)).date().isoformat()
        }

        return self.sf_update(update)

    def wait_for_bsfid(self, wait_time=5, poll_interval=0.5):
        self.logger.info('\tWaiting for BSF ID...')
        attempts = int(wait_time / poll_interval)

        for attempt in range(attempts):
            self.sf_get()

            if self.sf_attributes['BSF_ID__c'] is not None:
                return self.sf_attributes['BSF_ID__c']

            time.sleep(poll_interval)

        return False
