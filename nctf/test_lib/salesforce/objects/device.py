"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
import time

from nctf.libs.common_library import random_imei
from nctf.libs.common_library import random_mac
from nctf.libs.common_library import random_serial
from nctf.test_lib.salesforce.objects.object import SFObject
from nctf.test_lib.salesforce.objects.product import SFProduct


class SFDevice(SFObject):
    sf_account = None
    sf_device_license_allocation = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFDevice, self).__init__('Device__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface, optional_params=None):
        required_params = {
            'SerialNumber__c': random_serial(),
            'MAC_Address__c': random_mac(),
            'IMEI__c': random_imei(salesforce_fix=True),
            'Product__c': SFProduct.get_random_router(sf_interface).id()
        }

        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def create(cls, sf_interface, serial_number, mac_address, imei, optional_params=None):
        required_params = {'SerialNumber__c': serial_number, 'MAC_Address__c': mac_address, 'IMEI__c': imei}

        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    @classmethod
    def get_device_sf_id_by_mac_address(cls, sf_interface, mac_address):
        """
        Retrieve a device's Salesforce Id by Mac Address.

        Parameters:
            sf_interface: Salesforce Fixture
            mac_address (str): mac address of a device

        Return:
            device_sf_id (str): Device's Salesforce Id
        """
        # Query Salesforce to get the Device Object record associated with the MAC Address
        device_query = "SELECT Id FROM Device__c WHERE MAC_Address__c = '{}'".format(mac_address)
        query = sf_interface.query(device_query)

        # From the query result get the Device__c Salesforce Id
        sf_device = []
        for entry in query['records']:
            sf_device.append(entry['Id'])

        # Convert to string to be value can be used in a query to Salesforce
        device_sf_id = ''.join(sf_device)

        return device_sf_id

    def set_device_license_allocation(self, sf_device_license_allocation):
        self.sf_device_license_allocation = sf_device_license_allocation

    def set_account(self, sf_account):
        self.sf_account = sf_account

    def wait_for_bsfid(self, wait_time=5, poll_interval=1):
        self.logger.info('\tWaiting for BSF ID...')
        attempts = int(wait_time / poll_interval)

        for attempt in range(attempts):
            self.sf_get()

            if self.sf_attributes['BSF_ID__c'] is not None:
                return self.sf_attributes['BSF_ID__c']

            time.sleep(poll_interval)

        return False

    def wait_for_account_id(self, account_id=None, wait_time=5, poll_interval=1):
        self.logger.info('\tWaiting for Account Id...')
        attempts = int(wait_time / poll_interval)

        for attempt in range(attempts):
            self.sf_get()

            if (account_id is None and self.sf_attributes['Account__c'] is not None) or \
                    (account_id is not None and account_id == self.sf_attributes['Account__c']):
                return self.sf_attributes['Account__c']

            time.sleep(poll_interval)

        return False
