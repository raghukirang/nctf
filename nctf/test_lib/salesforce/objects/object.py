"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
from functools import reduce
import logging
import time
from typing import Dict

import requests
import simple_salesforce


class SFObject(object):
    __is_created = False

    sf_id = None
    metatype = str()

    logger = logging.getLogger('salesforce.object')

    sf_attributes = dict()
    sf_create_params = dict()

    def __init__(self, metatype, sf_interface, required_params=None, sf_id=None):
        self.metatype = metatype
        self.sf_interface = sf_interface

        if sf_id:
            self.sf_id = sf_id
            self.sf_get()

    def __sf_object_operation(self, obj_oper: str, obj_params: str, retries: int = 24) -> dict:
        """Generically chains and calls a 'simple_salesforce' call depending on object type and object operation.

        Example:
            (This may seem complicated, but I swear, it's not!)

            obj_type = 'Device__c'    obj_oper = 'get'    obj_params = '0013B000004k8Di'

            reduce(getattr, '{}.{}'.format(obj_type, obj_oper).split('.'), sf)(obj_id)
            reduce(getattr('Device__c get'), sf)('0013B000004k8Di')
            reduce(Device__c.get, sf)('0013B000004k8Di')
            sf.Device__c.get('0013B000004k8Di')

        See Also:
         - 'getattr': https://docs.python.org/3/library/functions.html#getattr
         - 'reduce': http://stackoverflow.com/questions/3279082/python-chain-getattr-as-a-string

        Args:
            obj_oper: Object Operation ('get', 'delete').
            obj_params: Parameters of Object.
            retries: Number of retry attempts to make for common request blockers (e.g. a row was locked).

        Returns:
            SF Response Object/JSON
        """
        self.logger.debug('({}) Object [{}] with ID: {}'.format(obj_oper.upper(), self.metatype, obj_params))

        try:
            sf_object = self.do_operation(obj_oper, obj_params)

        except simple_salesforce.SalesforceExpiredSession as e:
            self.logger.warning('Failed to perform the ({})'.format(obj_oper.upper()))
            self.logger.warning('Attempting to reconnect to salesforce and retry the call.')
            try:
                # Reconnect _SalesforceConnection
                self.sf_interface.establish_connection()
                # Retry the call
                sf_object = self.do_operation(obj_oper, obj_params)
            except simple_salesforce.SalesforceExpiredSession as e:
                self.logger.error('Expired Salesforce Session:\n\n{}'.format(e))
                raise e

        except simple_salesforce.SalesforceMalformedRequest as e:
            if "UNABLE_TO_LOCK_ROW" not in str(e) or retries <= 0:
                self.logger.error('Malformed Salesforce Request:\n\n{}'.format(e))
                raise e
            self.logger.warning("Couldn't perform query due to row lock. Retrying query in 1 second")
            time.sleep(1)
            return self.__sf_object_operation(obj_oper, obj_params, retries - 1)

        except simple_salesforce.SalesforceResourceNotFound as e:
            # If we try to delete an object, and it's already deleted, ignore the error.
            if not (obj_oper.lower() == 'delete' and e.content[0]['errorCode'] == 'ENTITY_IS_DELETED'):
                self.logger.error('Salesforce Resource Not Found:\n\n{}'.format(e))
                raise e
            return None

        except simple_salesforce.SalesforceGeneralError as e:
            if "UNABLE_TO_LOCK_ROW" not in str(e) or retries <= 0:
                self.logger.error('Salesforce General Error:\n\n{}'.format(e))
                raise e
            self.logger.warning("Couldn't perform query due to row lock. Retrying query in 1 second")
            time.sleep(1)
            return self.__sf_object_operation(obj_oper, obj_params, retries - 1)

        except requests.exceptions.RequestException as e:
            self.logger.error('Requests Exception:\n\n{}'.format(e))
            raise e

        self.logger.debug('\tObject: [{}] ({}): {}'.format(self.metatype, obj_oper.upper(), sf_object))
        return sf_object

    def sf_get(self) -> Dict:
        """Retrieves the values from a Salesforce Object (and saves it into sf_attributes)"""
        if self.sf_id is not None:
            # We always reset the sf_id, as if we create an object from a 15 character ID, we cannot
            #   compare the 15 character ID to an 18 character ID from another relational object!
            self.sf_attributes = self.__sf_object_operation('get', self.sf_id)
            self.sf_id = self.sf_attributes['Id']
            del self.sf_attributes['attributes']
            return self.sf_attributes

        self.logger.error('Current Object has No SF ID!')
        return None

    def sf_delete(self):
        """Deletes an Object from Salesforce"""
        self.sf_interface._remove_object(self)
        return self.__sf_object_operation('delete', self.sf_id)

    def sf_create(self, sf_create_params):
        """Creates an Object in Salesforce with given parameters"""
        if self.__is_created is False:
            self.sf_create_params = sf_create_params
            result = self.__sf_object_operation('create', self.sf_create_params)
            assert result['success']

            self.sf_id = result['id']
            self.logger.info(f"Created {self.__class__.__name__} record with Id:\t{self.sf_id}")
            self.__is_created = True
            self.sf_interface._add_object(self)
            return self.sf_id

        self.logger.error('Object Already Created with ID: {}'.format(self.sf_id))
        return None

    def sf_update(self, sf_update_params):
        """Update a Salesforce Object with specified Salesforce API Keys

        Note:
            You'll have to find the Salesforce API keys for a given object to update the
            values correctly!
        """
        if isinstance(sf_update_params, dict):
            return self.__sf_object_operation('update', (self.sf_id, sf_update_params))
        else:
            self.logger.error('\'sf_update_params\' is not of type dict: {}'.format(sf_update_params))
            return None

    def sf_validate(self):
        """Validate a Salesforce Object was created correctly (compared to it's creation parameters)"""
        sf_obj_json = self.sf_get()

        for key in self.sf_create_params:
            if key != 'Apex_Context__c':
                assert self.sf_create_params[key] == sf_obj_json[key]

    def id(self, trimmed=False):
        """Returns a 15 character/18 character Salesforce ID

        Params:
            trimmed (bool): True = 15 character ID, False = 18 character ID

        Note:
            BSF Services utilize the 15 character Salesforce ID and reject 18 character IDs.
        """
        if trimmed is True and len(self.sf_id) == 18:
            return self.sf_id[:-3]
        else:
            return self.sf_id

    def do_operation(self, obj_oper, obj_params):
        """Performs the requested operation upon the passed in object."""
        # TODO: Reduce further ((*obj_params) required for tuples) [functional design?]
        # Note: Tuple is used for 'sf_update', where you pass an ID, and a dict into the simple-salesforce object.
        # Note: - i.e (sf.Object.update(id, {'Apex_Context__c': True}))
        if isinstance(obj_params, tuple):
            # '*' converts a tuple from (foo, bar) to 'foo, bar'.
            sf_object = reduce(getattr, '{}.{}'.format(self.metatype, obj_oper).split('.'),
                               self.sf_interface.connection)(*obj_params)
        else:
            sf_object = reduce(getattr, '{}.{}'.format(self.metatype, obj_oper).split('.'),
                               self.sf_interface.connection)(obj_params)
        return sf_object

    def __str__(self) -> str:
        return "<{} (id={})>".format(self.__class__.__name__, self.sf_id)


    def clone(self, update_fields:dict):
        """
        Clones an object and creates in sfdc

        :param update_fields:
        :return:
        """
        self.sf_get()
        for field, value in update_fields.items():
            self.sf_attributes[field] = value
        self._remove_system_immutable_fields()
        self._remove_custom_fields()
        cloned_object = self.__class__(self.sf_interface, required_params=self.sf_attributes)
        cloned_object.sf_get()
        return cloned_object

    def _remove_system_immutable_fields(self):
        """
        Used to remove global unwrittable system fields for objects in sfdc
        :return:
        """
        unwritable_fields = ['Id', 'LastModifiedDate', 'IsDeleted', 'LastViewedDate',
                             'LastReferencedDate', 'SystemModstamp', 'CreatedById',
                             'CreatedDate', 'LastActivityDate', 'LastModifiedById']
        if self.sf_attributes:
            for field in unwritable_fields:
                self.sf_attributes.pop(field, None)

    def _remove_custom_fields(self):
        """
        Should be overridden to remove customer fields on a per
        object basis. See SFProduct for an example

        :return:
        """
        pass

