import random

from nctf.libs.common_library import random_hex_string
from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.test_lib.salesforce.objects.featurelist import SFFeatureList
from nctf.test_lib.salesforce.objects.object import SFObject
from nctf.test_lib.salesforce.objects.product import SFProduct



class SFAdditionalProductDetail(SFObject):


    def __init__(self, sf_interface, product_code: str):
        """
        These records are created automatically, so no creates needed
        :param sf_interface:
        :param product_code:
        """
        super(SFAdditionalProductDetail, self).__init__('kugo2p__AdditionalProductDetail__c', sf_interface)

        product = sf_interface.query(
            "SELECT Id FROM kugo2p__AdditionalProductDetail__c WHERE kugo2p__ProductCode__c = '{}'".format(product_code))

        if not product['records']:
            raise KeyError('No such product: {}'.format(product_code))

        self.sf_id = product['records'][0]['Id']
        self.sf_get()

    def update_to_valid_package(self):
        valid_service = {'kugo2p__UnitofTerm__c': 'Year',
                             'kugo2p__Service__c': True,
                         'kugo2p__DefaultServiceTerm__c': '1'}
        self.sf_update(valid_service)