"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
from nctf.libs.common_library import random_string
from nctf.test_lib.salesforce.objects.object import SFObject


class SFFeatureList(SFObject):
    sf_entitlement = None
    sf_feature_list_item = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFFeatureList, self).__init__('Feature_List__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface):
        required_params = {'Name': random_string(20, 'FeatureList_', False)}
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def create(cls, sf_interface, name):
        return cls(sf_interface, required_params={'Name': name})

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    @classmethod
    def get_by_name(cls, sf_interface: '_SalesforceConnection', name: str) -> 'SFFeatureList':
        """Instantiates a new FeatureList representation, given the name of an existing Feature List.

        Args:
            sf_interface: Salesforce interface.
            name: Feature List name, i.e. 'Enterprise Cloud Manager Prime + CradleCare'

        Returns:
            An SFFeatureList, if a matching name is found.

        Raises:
            ValueError: If a matching Feature List was not found in Salesforce, or there are multiple matches.
        """

        feature_list_query = "SELECT Id FROM Feature_List__c WHERE Name = '{}'".format(name)
        feature_list_recs = sf_interface.query(feature_list_query)['records']

        if len(feature_list_recs) == 0:
            raise ValueError("Feature List not found with Name '{}'".format(name))
        elif len(feature_list_recs) > 1:
            raise ValueError("More than one Feature List found with Name '{}'".format(name))

        return cls(sf_interface, sf_id=feature_list_recs[0]['Id'])

    def set_feature_list_item(self, feature_list_item):
        self.sf_feature_list_item = feature_list_item

    def set_entitlement(self, entitlement):
        self.sf_entitlement = entitlement
