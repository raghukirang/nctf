import time

from nctf.test_lib.salesforce.objects.device import SFDevice
from nctf.test_lib.salesforce.objects.license import SFLicense
from nctf.test_lib.salesforce.objects.object import SFObject


class SFDeviceLicenseAllocation(SFObject):
    sf_device = None
    sf_license = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFDeviceLicenseAllocation, self).__init__('DeviceLicenseAllocation__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface):
        raise NotImplementedError('Work in Progress...')

    @classmethod
    def create(cls, sf_interface, sf_device, sf_license):
        cls.sf_device = sf_device if isinstance(sf_device, SFDevice) else None
        cls.sf_license = sf_license if isinstance(sf_license, SFLicense) else None

        if cls.sf_device is None:
            raise ValueError('Device: {} is not of type SFDevice!'.format(sf_device))

        if cls.sf_license is None:
            raise ValueError('FeatureList: {} is not of type SFLicense!'.format(sf_license))

        required_params = {'Device__c': cls.sf_device.sf_id, 'License__c': cls.sf_license.sf_id}
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    def set_device(self, sf_device):
        self.sf_device = sf_device

    def set_license(self, sf_license):
        self.sf_license = sf_license

    def wait_for_bsfid(self, wait_time=5, poll_interval=0.5):
        self.logger.info('\tWaiting for BSF ID...')
        attempts = int(wait_time / poll_interval)

        for attempt in range(attempts):
            self.sf_get()

            if self.sf_attributes['BSF_ID__c'] is not None:
                return self.sf_attributes['BSF_ID__c']

            time.sleep(poll_interval)

        return False

    @classmethod
    def delete_device_license_allocations(cls, sf_interface, mac_address):
        """
        Deletes Device License Allocations assigned to the Device of the specified MAC Address.
        """

        # Query Salesforce to get the Device Object record associated with the MAC Address
        device_query = "SELECT Id FROM Device__c WHERE MAC_Address__c = '{}'".format(mac_address)
        device_obj = sf_interface.query(device_query)

        if not device_obj['records']:
            raise ValueError('Device with MAC Address {} not found.'.format(mac_address))

        # From the query result get the Device__c Salesforce Id
        device_sf_id = []
        for entry in device_obj['records']:
            device_sf_id.append(entry['Id'])

        # Convert device_id to a string to use in the device_license_allocation_query to Salesforce
        device = ''.join(device_sf_id)

        # Query Salesforce to get the Device License Allcacations that are assigned to the passed in MAC Address
        device_license_allocation_query = "SELECT Id FROM DeviceLicenseAllocation__c WHERE Device__c = '{}'".format(device)
        device_license_allocation_obj = sf_interface.query(device_license_allocation_query)

        if not device_license_allocation_obj['records']:
            raise ValueError('Device License Allocation not found for Device with Salesforce ID {}.'.format(device))

        # Delete Device License Allocation Records assigned to passed in MAC Address
        for entry in device_license_allocation_obj['records']:
            device_license_allocation = SFDeviceLicenseAllocation.get_by_sf_id(sf_interface, entry['Id'])
            device_license_allocation.sf_delete()

    @classmethod
    def verify_device_is_assigned_to_device_license_allocation(cls, sf_interface, mac_address):
        """
        Retrieves Salesforce ID of the Device License Allocation assigned to the Device of the specified MAC Address.

        Args:
            sf_interface: Salesforce Fixture
            mac_address (str): MAC Address of the Device

        Return:
            Salesforce ID of Device License Allocation assigned to the Device.
        """

        # Query Salesforce to get the Device Object record associated with the MAC Address
        device_query = "SELECT Id FROM Device__c WHERE MAC_Address__c = '{}'".format(mac_address)
        device_obj = sf_interface.query(device_query)

        if not device_obj['records']:
            raise ValueError('Device with MAC Address {} not found.'.format(mac_address))

        # From the query result get the Device__c Salesforce Id
        device_sf_id = []
        for entry in device_obj['records']:
            device_sf_id.append(entry['Id'])

        # Convert device_id to a string to use in the device_license_allocation_query to Salesforce
        device = ''.join(device_sf_id)

        # Query Salesforce to get the Device License Allocations that are assigned to the passed in MAC Address
        device_license_allocation_query = "SELECT Id FROM DeviceLicenseAllocation__c WHERE Device__c = '{}'".format(device)
        device_license_allocation_obj = sf_interface.query(device_license_allocation_query)

        if not device_license_allocation_obj['records']:
            raise ValueError('Device License Allocation not found for Device with Salesforce ID {}.'.format(device))

        # Return Device License Allocation Salesforce ID assigned to Device
        device_license_allocation_sf_id = []
        for entry in device_license_allocation_obj['records']:
            device_license_allocation_sf_id.append(entry['Id'])

        return device_license_allocation_sf_id

    @classmethod
    def get_device_license_allocation_by_device_sf_id(cls, sf_interface, sf_id):
        """
        Retrieves Salesforce ID of the Device License Allocation assigned to specified Device's Salesforce ID.

        Args:
            sf_interface: Salesforce Fixture
            sf_id (str): Salesforce ID of specified Device

        Return:
            Salesforce ID of Device License Allocation assigned to the Device.
        """

        # Query Salesforce to get the Device License Alocations that are assigned to the passed in MAC Address
        device_license_allocation_query = "SELECT Id FROM DeviceLicenseAllocation__c WHERE Device__c = '{}'".format(sf_id)
        device_license_allocation_obj = sf_interface.query(device_license_allocation_query)

        if not device_license_allocation_obj['records']:
            raise ValueError('Device License Allocation not found for Device with Salesforce ID {}.'.format(sf_id))

        # Return Device License Allocation Salesforce ID assigned to Device
        device_license_allocation_sf_id = []
        for entry in device_license_allocation_obj['records']:
            device_license_allocation_sf_id.append(entry['Id'])

        return device_license_allocation_sf_id
