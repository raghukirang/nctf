from typing import Any
from typing import Dict
from typing import List

from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects.object import SFObject


class SFRMA(SFObject):
    def __init__(self, sf_interface: _SalesforceConnection, required_params: Dict[str, Any] = None, sf_id: str = None):
        super(SFRMA, self).__init__('RMA__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create(cls,
               sf_interface: _SalesforceConnection,
               sf_contact_id: str,
               optional_params: Dict[str, Any] = None) -> 'SFRMA':
        """
        Create an RMA with Minimum Required Values
        Args:
            sf_interface: Salesforce Fixture
            sf_contact_id: Salesforce ID of specified Contact
            optional_params: Salesforce Key/Value pairs that will be set on the Salesforce RMA.

        Returns:
            Instance of SFRMA
        """
        required_params = {
            "Status__c": "NEW",
            "RMA_Reason__c": "Firmware Failure",
            "Contact__c": sf_contact_id
        }

        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_contact_id(cls, sf_interface: _SalesforceConnection, sf_contact_id: str) -> List['SFRMA']:
        """
        Retrieve a List of Salesforce RMAs with a specified Salesforce Contact ID.
        Args:
            sf_interface: Salesforce Fixture
            sf_contact_id: Salesforce ID of specified Account

        Returns:
            The List of Salesforce RMAs associated with specified Contact
        """
        rma_objs = sf_interface.query(f"SELECT Id FROM RMA__c WHERE Contact__c = '{sf_contact_id}'")['records']

        return [cls(sf_interface, sf_id=obj['Id']) for obj in rma_objs]
