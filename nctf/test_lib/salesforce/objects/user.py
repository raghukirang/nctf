"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
import logging

from nctf.libs.common_library import random_string
from nctf.test_lib.salesforce.objects.object import SFObject

logger = logging.getLogger('salesforce.user')


class SFUser(SFObject):
    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFUser, self).__init__('User', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface, sf_contact, sf_profile_name, optional_params=None):
        profile_id = cls._get_profile_id(sf_interface, sf_profile_name)[0]

        required_params = {
            'LastName': sf_contact.sf_attributes['LastName'],
            'Alias': random_string(6, 't_', False),
            'CommunityNickname': random_string(10, 'test_', False),
            'Username': sf_contact.sf_attributes['Email'],
            'Email': sf_contact.sf_attributes['Email'],
            'ProfileId': profile_id,
            'ContactId': sf_contact.sf_id,
            'TimeZoneSidKey': 'America/Denver',
            'LocaleSidKey': 'en_US',
            'EmailEncodingKey': 'ISO-8859-1',
            'LanguageLocaleKey': 'en_US',
            'IsActive': True
        }

        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params)

    @classmethod
    def get_user_by_contact_id(cls, sf_interface, contact_id):
        user_objs = sf_interface.query("SELECT Id FROM User WHERE ContactId = '{}'".format(contact_id))['records']

        user_ids = []
        for user_obj in user_objs:
            user_ids.append(user_obj['Id'])

        return cls(sf_interface, None, user_ids[0])

    @classmethod
    def get_user_by_contact_email(cls, sf_interface, contact_email: str) -> SFObject:
        """

        Args:
            sf_interface: Salesforce Fixture
            contact_email: Email address
        Returns:
            SalesForce user object
        """

        user_objs = sf_interface.query("SELECT Id FROM User WHERE Email = '{}'".format(contact_email))['records']

        user_ids = []
        for user_obj in user_objs:
            user_ids.append(user_obj['Id'])

        if len(user_ids) == 1:
            return cls(sf_interface, None, user_ids[0])
        elif len(user_ids) == 0:
            raise Exception('No users exist that contain the contact email: {}'.format(contact_email))
        elif len(user_ids) > 1:
            raise Exception('Multiple users exist that contain the contact email: {}'.format(contact_email))

    @staticmethod
    def _get_profile_id(sf_interface, profile_name):
        profile_objs = sf_interface.query("SELECT Id FROM Profile WHERE Name = '{}'".format(profile_name))['records']

        profile_ids = []
        for profile_obj in profile_objs:
            profile_ids.append(profile_obj['Id'])

        return profile_ids

    def sf_delete(self):
        self.sf_update({'IsPortalEnabled': False})  # also sets IsActive to false
