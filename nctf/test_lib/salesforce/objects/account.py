"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""

import time
from typing import List
from typing import Tuple
from typing import Union

# from nctf.libs.salesforce.common_lib import ExceptionalFeatures
# from nctf.libs.common_library import require_type
from nctf.libs.common_library import random_string
from nctf.libs.salesforce.common_lib import BundleProducts
from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.libs.salesforce.common_lib import Products
from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects import product
from nctf.test_lib.salesforce.objects.contact import SFContact
from nctf.test_lib.salesforce.objects.entitlement import SFEntitlement
from nctf.test_lib.salesforce.objects.featurelist import SFFeatureList
from nctf.test_lib.salesforce.objects.object import SFObject
from nctf.test_lib.salesforce.objects.order import SFOrder


class SFAccount(SFObject):
    ecm_user = None
    ecm_account = None

    device = None
    contact = None
    orders = list()
    entitlements = list()
    opportunities = list()
    order_service_lines = list()

    has_been_entitled = False
    connected_with_ecm = False

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFAccount, self).__init__('Account', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface, optional_params=None, create_contact=True, name_prefix='TEST_'):
        """
        Create a Random Account (Recommended)

        Parameters:
            sf_interface: Salesforce Fixture
            optional_params (dict): Salesforce Key/Value pairs that will be set on the Salesforce Account.
                **The key/value pairs are specific to the Salesforce Account Database Object, not SFAccount**

        Return:
            SFAccount: Instance of SFAccount with randomly instantiated values.
        """
        required_params = {
            'Name': random_string(20, name_prefix, False),
            'Apex_Context__c': True,
            'BillingStreet': '123 St',
            'BillingState': 'Idaho',
            'BillingPostalCode': '83701',
            'BillingCountry': 'United States',
            'BillingCity': 'Boise',
            'ShippingStreet': '123 St',
            'ShippingState': 'Idaho',
            'ShippingPostalCode': '83701',
            'ShippingCountry': 'United States',
            'ShippingCity': 'Boise'
        }
        if optional_params is not None:
            required_params.update(optional_params)

        instance = cls(sf_interface, required_params=required_params)
        if create_contact:
            instance.contact = SFContact.create_random(sf_interface, {'AccountId': instance.sf_id})

        return instance

    @classmethod
    def create(cls, sf_interface, account_name, optional_params=None):
        """
        Create an Account with *Bare Minimum* Values (Not Recommended)

        Note:
            Account operations may require more than the minimum values needed for creation.
            i.e: Entitling an Account requires a Contact associated with the Account.

        Parameters:
            sf_interface: Salesforce Fixture
            account_name (str): Name of Account
            optional_params (dict): Salesforce Key/Value pairs that will be set on the Salesforce Account.
                **The key/value pairs are specific to the Salesforce Account Database Object, not SFAccount**

        Return:
            SFAccount: Instance of SFAccount
        """
        required_params = {'Name': account_name, 'Apex_Context__c': True}

        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        """
        Retrieve an Salesforce Account with a specified Salesforce ID.

        Args:
            sf_interface: Salesforce Fixture
            sf_id (str): Salesforce ID of specified Salesforce Account

        Return:
            SFAccount: Instance of SFAccount with an existing Salesforce Account
        """
        return cls(sf_interface, sf_id=sf_id)

    @classmethod
    def get_by_pertino_integration_id(cls, sf_interface: _SalesforceConnection, pertino_sf_id: str) -> 'SFAccount':
        """ Retrieve a Salesforce Account with a specified Pertino_Integration_AccountId

        Args:
            sf_interface: Salesforce Fixture
            pertino_sf_id: ID linking CP SF accounts to Pertino SF accounts linked by BlueWolf

        Returns:
            Instance of SFAccount with an existing Salesforce Account
        """
        account_query = "SELECT Id FROM Account WHERE Pertino_Integration_AccountId__c = '{}'".format(pertino_sf_id)
        sf_id = sf_interface.query(account_query)['records']
        return cls(sf_interface, sf_id=sf_id[0]['Id'])

    @classmethod
    def get_by_account_name(cls, sf_interface: _SalesforceConnection, account_name: str) -> 'SFAccount':
        """ Retrieve a Salesforce Account with a specified Account Name

        Args:
            sf_interface: Salesforce Fixture
            account_name: Name of an existing account
        Returns:
            Instance of SFAccount with an existing Salesforce Account

        """

        account_query = "SELECT Id FROM Account WHERE Name = '{}'".format(account_name)
        sf_id = sf_interface.query(account_query)['records']

        return cls(sf_interface, sf_id=sf_id[0]['Id'])

    @classmethod
    def search_for_account_name_wildcard(cls, sf_interface: _SalesforceConnection, account_name: str) -> 'SFAccount':
        """ Retrieve a Salesforce Account with a specified Account Name

        Args:
            sf_interface: Salesforce Fixture
            account_name: Name of an existing account
        Returns:
            Instance of SFAccount with an existing Salesforce Account

        """

        account_query = "SELECT Id FROM Account WHERE Name LIKE '{}%'".format(account_name)
        sf_id = sf_interface.query(account_query)['records'][0]
        return cls(sf_interface, sf_id=sf_id[0]['Id'])

    def get_entitlement_ids(self):
        self.logger.debug('\tGetting Entitlement Ids...')
        entitlement_objs = self.sf_interface.query("SELECT Id FROM Entitlement WHERE AccountId = '{}'".format(
            self.sf_id))['records']

        entitlement_ids = []
        for entitlement_obj in entitlement_objs:
            self.logger.debug('\tFound Entitlement: {}'.format(entitlement_obj['Id']))
            entitlement_ids.append(entitlement_obj['Id'])

        return entitlement_ids

    def get_entitlement_info(self):
        self.logger.debug('\tGetting Entitlement Info...')
        entitlement_objs = self.sf_interface.query("SELECT Id, Name, StartDate, EndDate FROM Entitlement "
                                                   "WHERE AccountId = '{}'".format(self.sf_id))['records']

        return entitlement_objs

    def connect_with_ecm(self, ecm_account, ecm_user) -> None:
        raise NotImplementedError("ECMClassicAccount and ECMClassicUser removed in NCTF.")
        # if self.connected_with_ecm is True:
        #     self.logger.warn("Account is already connected to ECM with User: '{}', Account: '{}'".format(
        #         self.ecm_user.username, self.ecm_account.name))
        #     return
        #
        # require_type(ecm_account, ECMClassicAccount)
        # require_type(ecm_user, ECMClassicUser)
        #
        # self.ecm_user = ecm_user
        # self.ecm_account = ecm_account
        #
        # # Create Contact
        # if self.contact is None:
        #     self.logger.debug('\tCreating Contact...')
        #     self.contact = SFContact(self.sf_interface)
        #     self.contact.sf_create({
        #         'AccountId': self.sf_id,
        #         'FirstName': ecm_user.firstname,
        #         'LastName': ecm_user.lastname,
        #         'Customer_Type__c': 'Enterprise',
        #     })
        #     self.logger.debug('\tContact Created Successfully!')
        #
        # self.logger.debug('Linking with ECM Account...')
        # ecm_api = ECMAPIV1Interface(ecm_account.root_api.base_url)
        # ecm_api.authenticate_with_keys('299c2da5-36af-4139-95b1-f29c5c1c1943', '878071307dbccebee492780630aab7248e1f38c1')
        # customer_data = {
        #     "account": '/api/v1/accounts/{}/'.format(ecm_account.ecm_id),
        #     "address2": "",
        #     "address1": "",
        #     "city": "",
        #     "company_name": "",
        #     "contact_name": "",
        #     "customer_id": str(self.id(trimmed=True)),
        #     "postal": "",
        #     "phone": "",
        #     "state": ""
        # }
        #
        # ecm_customer = ecm_account.root_api.post('/api/v1/customers/', 201, json=customer_data).json()
        #
        # if ecm_customer['success'] is True:
        #     customer_uri = ecm_customer['data']['resource_uri']
        #     self.logger.debug("ECM Customer '{}' successfully created.".format(customer_uri))
        # else:
        #     self.logger.error('ECM Customer creation failed. Message: {}'.format(ecm_customer))
        #     raise Exception(ecm_customer)
        #
        # ecm_account.ecm_customer_id = ecm_customer['data']['id']
        # self.logger.debug('ECM and SF Accounts Linked!')

    def entitle(self, product_code: ProductCodes, origin: str = 'ECM', wait_for_taxman: bool = False,
                validate_product: bool = True) -> SFOrder:
        """Submit a new order to Salesforce.

        Args:
            product_code: Product code mapping for the product.
                See nctf.libs.salesforce.common_lib::ProductCodes for valid objects.
            origin:
            wait_for_taxman: If true, waits a pre-defined, reasonable amount of time
                in the hopes that Taxman will have finished the sync.

        Returns:
            The order that was created and released to entitle the account.
        """

        # Sanity Check
        if self.contact is None:
            error = 'Contact is required to entitle an Account!'
            self.logger.error(error)
            raise ValueError(error)

        new_order, new_order_lines = SFOrder.create_order(self.sf_interface, product_code, self, self.contact, origin,
                                                          validate_product=validate_product)
        self.orders.append(new_order)
        self.order_service_lines.append(new_order_lines)
        new_order.release_ops_order()

        if wait_for_taxman:
            time.sleep(240)

        return new_order

    def entitle_bundle(self, bundle_product: Union[BundleProducts, Products]) -> SFEntitlement:
        """Directly entitle the account for a given product.

        This is intended for bypassing the regular Order process, which no
        longer supports bundled products.

        Args:
            bundle_product: Bundled product mapping.
                See nctf.libs.salesforce.common_lib::BundleProducts for valid objects.

        Returns:
            The new entitlement that was directly activated for this account.
        """
        if self.contact is None:
            error = 'Contact is required to entitle an Account!'
            self.logger.error(error)
            raise ValueError(error)

        product_code = bundle_product.value.product_code
        self.logger.info("Using alternate method to entitle the SF account '{}' for bundled product '{}'".format(
            self.id(), product_code.value))

        sf_feature_list = SFFeatureList.get_by_name(self.sf_interface, bundle_product.value.feature_list.value.list_name)
        sf_product = product.SFProduct.get_product_by_product_code(self.sf_interface, product_code)
        entitlement = SFEntitlement.create_for_product(self.sf_interface, self, sf_feature_list, sf_product)
        entitlement.activate()

        return entitlement

    def track_ordered_product(self, product_code: ProductCodes) -> None:
        """Searches for and tracks an order for a specific project, on this account.

        Useful for when a system generates orders itself (eg. ECM trials).
        This order is tracked via SFOrders in self.orders.

        Args:
            product_code: The product to search for and track on this account.
        """
        # Retrieve Product from Product Code
        product = self.sf_interface.query(
            "SELECT Id FROM kugo2p__AdditionalProductDetail__c WHERE kugo2p__ProductCode__c = '{}'".format(product_code.value))

        if not product['records']:
            raise KeyError('No such product in Salesforce: {} -> {}'.format(product_code.name, product_code.value))

        product_id = product['records'][0]['Id']

        order_query = "SELECT Id FROM kugo2p__SalesOrder__c WHERE Kugo2p__Account__c = '{}'".format(self.sf_id)
        orders = [SFOrder(self.sf_interface, sf_id=o['Id']) for o in self.sf_interface.query(order_query)['records']]
        for order in orders:
            orderline_query = "SELECT Id FROM kugo2p__SalesOrderServiceLine__c " \
                              "WHERE kugo2p__Service__c = '{}' " \
                              "AND kugo2p__SalesOrder__c = '{}'".format(product_id, order.sf_id)
            if self.sf_interface.query(orderline_query)['records']:
                self.orders.append(order)

    def verify_feature_bindings(self) -> Tuple[List[str], List[str]]:
        raise NotImplementedError("ECMClassicAccount and ECMClassicUser removed in NCTF.")
        #
        # """Verifies feature bindings are in sync between this account and the connected ECM account.
        #
        # This method does not wait for a sync - it is meant to check if the accounts are in sync.
        # The comparison method is based on Salesforce feature binding UUIDs - NOT feature UUIDs.
        #
        # Notes:
        #     If the ECM account has known ECM-only features (eg. CLIENT_ANALYTICS), this method will
        #     attempt to verify if the binding should be present or not.
        #
        # Raises:
        #     ValueError: If special handling for ECM-only feature bindings fails.
        #
        # Returns:
        #     Tuple of two lists of feature binding UUIDs, the first found only in Salesforce,
        #     the second found only in Netcloud (ECM).
        #
        # """
        # self.logger.info("Checking feature binding sync between SFAccount {} and ECMAccount {}".format(
        #     self.id(), self.ecm_account.ecm_id))
        # if self.ecm_account is None:
        #     raise Exception('Feature Binding Verification requires a connected ECM Account and ECM User!')
        #
        # def _collect_ecm_feature_bindings() -> List[str]:
        #     # TODO: Once the new NetCloud fixture is more complete, use this object's ECM User to call ECM API
        #     bindings_uri = '/api/v1/accounts/{}/features/'.format(self.ecm_account.ecm_id)
        #     ecm_api = ECMAPIV1Interface(self.ecm_account._ecm_api_root, 30)
        #     ecm_api.authenticate_with_keys('299c2da5-36af-4139-95b1-f29c5c1c1943', '878071307dbccebee492780630aab7248e1f38c1')
        #
        #     json_resp = ecm_api.get(bindings_uri, 200, params={'limit': 1000, 'expand': 'feature'}).json()
        #     assert json_resp['success']
        #
        #     uuids = []
        #     for ecm_binding in json_resp['data']:
        #         if not ecm_binding['settings']:
        #             # Non-Salesforce-originated binding. Verify special cases
        #             feature_uuid = ecm_binding['feature']['uuid']
        #             feature_name = ecm_binding['feature']['name']
        #             if feature_uuid in [feat.value for feat in ExceptionalFeatures.ECM_PRIME_CREATED.value]:
        #                 active_prime_params = {'feature.name': 'ADVANCED_TIER', 'enabled': True}
        #                 count = ecm_api.get(bindings_uri, 200, params=active_prime_params).json()['meta']['total_count']
        #                 if count != 1:
        #                     raise ValueError("Expected an active feature binding for ADVANCED_TIER because {} is "
        #                                      "enabled, but found {} active bindings for it.".format(feature_name, count))
        #         else:
        #             binding_uuid = ecm_binding['settings']['entitlement']['uuid']
        #             uuids.append(binding_uuid)
        #             self.logger.debug('ECM feature binding UUID: {}'.format(binding_uuid))
        #
        #     return uuids
        #
        # def _collect_sf_feature_bindings() -> List[str]:
        #     uuids = []
        #     for order in self.orders:
        #         entitlement_query = "SELECT Id FROM Entitlement WHERE Order__c = '{}'".format(order.sf_id)
        #         query = self.sf_interface.query(entitlement_query)
        #
        #         self.logger.info('SF Order: {}'.format(order.sf_id))
        #
        #         for entry in query['records']:
        #             entitlement = SFEntitlement.get_by_sf_id(self.sf_interface, entry['Id'])
        #             self.entitlements.append(entitlement)
        #
        #             self.logger.debug('\tSF Entitlement: {}'.format(entitlement.sf_id))
        #
        #             q1 = "SELECT Feature__c FROM Feature_List_Item__c " \
        #                  "WHERE Feature_List__c = '{}'".format(entitlement.sf_attributes['Feature_List__c'])
        #             q1_resp = self.sf_interface.query(q1)
        #
        #             for feature in q1_resp['records']:
        #                 q2_resp = self.sf_interface.query("SELECT UUID__c FROM Feature__c WHERE Id = '{}'".format(
        #                     feature['Feature__c']))
        #                 uuids.append(q2_resp['records'][0]['UUID__c'])
        #                 self.logger.debug('SF feature binding UUID: {}'.format(q2_resp['records'][0]['UUID__c']))
        #
        #     return uuids
        #
        # sf_uuids = set(_collect_sf_feature_bindings())
        # ecm_uuids = set(_collect_ecm_feature_bindings())
        #
        # in_ecm_only = ecm_uuids - sf_uuids
        # in_sf_only = sf_uuids - ecm_uuids
        #
        # if len(in_ecm_only) + len(in_sf_only) != 0:
        #     self.logger.info("Feature binding mismatch between Salesforce and ECM.\n"
        #                      "Only in Salesforce: {}\n"
        #                      "Only in ECM: {}".format(', '.join(in_sf_only), ', '.join(in_ecm_only)))
        #
        # return (list(in_sf_only), list(in_ecm_only))

    def sf_delete(self):
        self.logger.info('Deleting SF Object!')

        for entitlement in self.entitlements:
            entitlement.sf_delete()
            self.entitlements.remove(entitlement)

        for order in self.orders:
            order.sf_delete()
            self.orders.remove(order)

        for osl in self.order_service_lines:
            osl.sf_delete()
            self.order_service_lines.remove(osl)

        if self.contact is not None:
            self.contact.sf_delete()

        # Delete Account
        super(SFAccount, self).sf_delete()
