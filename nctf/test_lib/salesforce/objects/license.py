import time

from nctf.test_lib.salesforce.objects.object import SFObject


# Note: New Data Model
class SFLicense(SFObject):
    sf_entitlement = None
    sf_feature_list_item = None
    sf_device_license_allocation = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFLicense, self).__init__('License__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface):
        '''
        cls.sf_entitlement = SFEntitlement.create_random(sf_interface)
        cls.sf_feature_list_item = SFFeatureListItem.create_random(sf_interface)

        required_params = {'Feature_List_Item__c': cls.sf_feature_list_item.sf_id, 'Entitlement__c': cls.sf_entitlement.sf_id}
        return cls(sf_interface, required_params=required_params)
        '''

        # TODO: Implement
        raise NotImplementedError('Work in Progress...')

    @classmethod
    def create(cls, sf_interface, sf_entitlement, sf_feature_list_item):
        from nctf.test_lib.salesforce.objects.entitlement import SFEntitlement
        from nctf.test_lib.salesforce.objects.featurelistitem import SFFeatureListItem

        cls.sf_entitlement = sf_entitlement if isinstance(sf_entitlement, SFEntitlement) else None
        cls.sf_feature_list_item = sf_feature_list_item if isinstance(sf_feature_list_item, SFFeatureListItem) else None

        if cls.sf_entitlement is None:
            raise ValueError('Entitlement: {} is not of type SFEntitlement!'.format(sf_entitlement))

        if cls.sf_feature_list_item is None:
            raise ValueError('FeatureListItem: {} is not of type SFFeatureListItem!'.format(sf_feature_list_item))

        required_params = {'Feature_List_Item__c': cls.sf_feature_list_item.sf_id, 'Entitlement__c': cls.sf_entitlement.sf_id}
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    def set_entitlement(self, sf_entitlement):
        self.sf_entitlement = sf_entitlement

    def set_feature_list_item(self, sf_feature_list_item):
        self.sf_feature_list_item = sf_feature_list_item

    def set_device_license_allocation(self, sf_device_license_allocation):
        self.sf_device_license_allocation = sf_device_license_allocation

    def wait_for_bsfid(self, wait_time=5, poll_interval=0.5):
        self.logger.info('\tWaiting for BSF ID...')
        attempts = int(wait_time / poll_interval)

        for attempt in range(attempts):
            self.sf_get()

            if self.sf_attributes['BSF_ID__c'] is not None:
                return self.sf_attributes['BSF_ID__c']

            time.sleep(poll_interval)

        return False
