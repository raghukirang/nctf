import time

from nctf.test_lib.salesforce.objects.device import SFDevice
from nctf.test_lib.salesforce.objects.entitlement import SFEntitlement
from nctf.test_lib.salesforce.objects.object import SFObject


class SFDeviceEntitlement(SFObject):
    sf_device = None
    sf_entitlement = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFDeviceEntitlement, self).__init__('DeviceEntitlement__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create(cls, sf_interface, sf_device, sf_entitlement):
        cls.sf_device = sf_device if isinstance(sf_device, SFDevice) else None
        cls.sf_entitlement = sf_entitlement if isinstance(sf_entitlement, SFEntitlement) else None

        if cls.sf_device is None:
            raise ValueError('Device: {} is not of type SFDevice!'.format(sf_device))

        if cls.sf_entitlement is None:
            raise ValueError('Entitlement: {} is not of type SFEntitlement!'.format(sf_entitlement))

        required_params = {'Device__c': cls.sf_device.sf_id, 'Entitlement__c': cls.sf_entitlement.sf_id}
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    def set_device(self, sf_device):
        self.sf_device = sf_device

    def set_entitlement(self, sf_entitlement):
        self.sf_entitlement = sf_entitlement

    def wait_for_bsfid(self, wait_time=5, poll_interval=0.5):
        self.logger.info('\tWaiting for BSF ID...')
        attempts = int(wait_time / poll_interval)

        for attempt in range(attempts):
            self.sf_get()

            if self.sf_attributes['BSF_ID__c'] is not None:
                return self.sf_attributes['BSF_ID__c']

            time.sleep(poll_interval)

        return False

    # Deletes Device Entitlements assigned to the passed in MAC Address
    @classmethod
    def delete_device_entitlements(cls, sf_interface, mac_address):
        # Remove colons from MAC
        mac = mac_address.replace(":", "")

        # Query Salesforce to get the Device Object record associated with the MAC Address
        device_query = "SELECT Id FROM Device__c WHERE MAC_Address__c = '{}'".format(mac)
        query = sf_interface.query(device_query)

        # From the query result get the Device__c Salesforce Id
        device_sf_id = []
        for entry in query['records']:
            device_sf_id.append(entry['Id'])

        # Convert device_id to a string to use in the device_entitlement_query to Salesforce
        device = ''.join(device_sf_id)

        # Query Salesforce to get the Device Entitlements that are assigned to the passed in MAC Address
        device_entitlement_query = "SELECT Id FROM DeviceEntitlement__c WHERE Device__c = '{}'".format(device)
        query = sf_interface.query(device_entitlement_query)

        # Delete Device Entitlement Records assigned to passed in MAC Address
        for entry in query['records']:
            device_entitlement = SFDeviceEntitlement.get_by_sf_id(sf_interface, entry['Id'])
            device_entitlement.sf_delete()

    # Verifies Device Entitlements assigned to the passed in MAC Address
    @classmethod
    def verify_device_is_assigned_to_device_entitlement(cls, sf_interface, mac_address):
        # Remove colons from MAC
        mac = mac_address.replace(":", "")

        # Query Salesforce to get the Device Object record associated with the MAC Address
        device_query = "SELECT Id FROM Device__c WHERE MAC_Address__c = '{}'".format(mac)
        query = sf_interface.query(device_query)

        # From the query result get the Device__c Salesforce Id
        device_sf_id = []
        for entry in query['records']:
            device_sf_id.append(entry['Id'])

        assert device_sf_id != []

        # Convert device_id to a string to use in the device_entitlement_query to Salesforce
        device = ''.join(device_sf_id)

        # Query Salesforce to get the Device Entitlements that are assigned to the passed in MAC Address
        device_entitlement_query = "SELECT Id FROM DeviceEntitlement__c WHERE Device__c = '{}'".format(device)
        query = sf_interface.query(device_entitlement_query)

        # Return Device Entitlement Salesforce ID assigned to Device
        device_entitlement_sf_id = []
        for entry in query['records']:
            device_entitlement_sf_id.append(entry['Id'])

        assert device_entitlement_sf_id != []
