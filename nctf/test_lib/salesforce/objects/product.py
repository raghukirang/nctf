import random

from nctf.libs.common_library import random_hex_string
from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.test_lib.salesforce.objects.featurelist import SFFeatureList
from nctf.test_lib.salesforce.objects.object import SFObject


class SFProduct(SFObject):
    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFProduct, self).__init__('Product2', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface, feature_list: SFFeatureList, optional_params=None):
        name = f"Test-Product-{random_hex_string(6)}"
        required_params = {
            'RecordTypeId': '012500000009j1b',
            'Name': name,
            'ProductCode': name,
            'Feature_List__c': feature_list.id(),
            'Family': 'Services',
            'isActive': True,
            'Default_Service_Term__c': 1,
            'Service__c': True
        }

        if optional_params is not None:
            required_params.update(optional_params)
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_random_router(cls, sf_interface):
        """
        Query Salesforce to get the SF ID of a random router from the Salesforce Product object. The SF ID then can
        be used passed to other methods to create a device in Salesforce or BSF.

        Parameters:
            sf_interface: Salesforce Fixture

        Return:
            model: SF ID of random router in the Salesforce Products object.
        """
        router_objs = sf_interface.query("SELECT Id FROM Product2 WHERE Family = \'Routers\'")['records']

        router_ids = []
        for router_obj in router_objs:
            router_ids.append(router_obj['Id'])

        model = random.choice(router_ids)

        return cls(sf_interface, None, model)

    @classmethod
    def get_product_by_product_code(cls, sf_interface: '_SalesforceConnection', product_code: ProductCodes) -> 'SFProduct':
        """Gets a Salesforce Product representation, given its product code.

        Args:
            sf_interface: Authenticated interface to Salesforce.
            product_code: Product code mapping for the product.
                See nctf.libs.salesforce.common_lib::ProductCodes for valid objects.

        Returns:
            A Product representative of a matching Salesforce product entry.
        """
        product_recs = sf_interface.query("SELECT Id FROM Product2 WHERE ProductCode = '{}'".format(
            product_code.value))['records']

        if len(product_recs) == 0:
            raise ValueError("Product not found with product code '{}'".format(product_code))
        if len(product_recs) > 1:
            raise ValueError("More than one Product found with product code '{}'".format(product_code))

        return cls(sf_interface, None, product_recs[0]['Id'])

    def _remove_custom_fields(self):
        custom_fields = ['Base_Disti_Discount__c','Discount_Category__c', 'ID__c', 'Unique_Product__c']
        if self.sf_attributes:
            for field in custom_fields:
                self.sf_attributes.pop(field, None)

    def sf_delete(self):
        self.sf_update({'IsActive': False})
