"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
from enum import Enum
import random

from nctf.libs.common_library import random_digit_string
from nctf.libs.common_library import random_string
from nctf.libs.common_library import random_uuid
from nctf.test_lib.salesforce.objects.object import SFObject


class ConsumerTypes(Enum):
    """Feature 'Consumer' Types Enum"""
    DEVICE = 'Device'
    USER = 'User'
    ACCOUNT = 'Account'
    NETWORK = 'Network'


class SFFeature(SFObject):
    feature_list_item = None

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFFeature, self).__init__('Feature__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface):
        required_params = {
            'Name': random_string(20, 'Feature_', False),
            'UUID__c': str(random_uuid()),
            'Units__c': int(random_digit_string(3)) + 3,
            # https://docs.python.org/3/library/enum.html#iteration
            'Consumer__c': random.choice(list(ConsumerTypes)).value
        }
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def create(cls, sf_interface, name, uuid, units, consumer):
        if not isinstance(consumer, ConsumerTypes):
            raise ValueError('Consumer: \'{}\' is not apart of ConsumerTypes!'.format(consumer))

        required_params = {'Name': name, 'UUID__c': uuid, 'Units__c': units, 'Consumer__c': consumer}
        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    def set_feature_list_item(self, feature_list_item):
        self.feature_list_item = feature_list_item

    @classmethod
    def get_by_feature_list_id(cls, sf_interface, feature_list_id):
        features = []
        fli_recs = sf_interface.query("SELECT Id, Feature__c FROM Feature_List_Item__c WHERE Feature_List__c = '{}'".format(
            feature_list_id))['records']
        for fli in fli_recs:
            features.append(cls(sf_interface, sf_id=fli['Feature__c']))
        return features
