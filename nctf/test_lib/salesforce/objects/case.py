from typing import Any
from typing import Dict
from typing import List

from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects.object import SFObject


class SFCase(SFObject):
    def __init__(self, sf_interface: _SalesforceConnection, required_params: Dict[str, Any] = None, sf_id: str = None):
        super(SFCase, self).__init__('Case', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create(cls,
               sf_interface: _SalesforceConnection,
               sf_account_id: str,
               sf_contact_id: str,
               optional_params: Dict[str, Any] = None) -> 'SFCase':
        """Create a Case with Minimum Required Values


        Parameters:
           sf_interface: Salesforce Fixture
           sf_account_id: Salesforce ID of specified Account
           sf_contact_id: Salesforce ID of specified Contact
           optional_params: Salesforce Key/Value pairs that will be set on the Salesforce Case.

        Return:
           Instance of SFCase

        """
        required_params = {
            "Origin": "Email",
            "AccountId": sf_account_id,
            "ContactId": sf_contact_id,
            "Priority": "Medium",
            "Reason_Codes__c": "Services",
            "Subject": "subject",
            "Notes__c": "notes"
        }

        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_account_id(cls, sf_interface: _SalesforceConnection, sf_account_id: str) -> List['SFCase']:
        """Retrieve a List of Salesforce Cases with a specified Salesforce Account ID.

        Parameters:
            sf_interface: Salesforce Fixture
            sf_account_id: Salesforce ID of specified Account

        Return:
            the List of Salesforce Cases associated with specified Account
        """

        case_objs = sf_interface.query("SELECT Id FROM Case WHERE AccountId = '{}'".format(sf_account_id))['records']

        sf_cases = [cls(sf_interface, sf_id=obj['Id']) for obj in case_objs]

        return sf_cases
