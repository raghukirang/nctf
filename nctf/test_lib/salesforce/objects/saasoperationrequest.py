from nctf.libs.common_library import wait_for_with_interval
from nctf.pytest_lib.salesforce.fixture import salesforce_fixture
from nctf.test_lib.salesforce.objects.account import SFAccount
from nctf.test_lib.salesforce.objects.device import SFDevice
from nctf.test_lib.salesforce.objects.object import SFObject


class SaasOperationRequest(SFObject):

    REPARENT_DEVICES = 'REPARENT_DEVICES'
    REPARENT_SUBSCRIPTIONS = 'REPARENT_SUBSCRIPTIONS'
    PROVISON_DEVICE = 'PROVISION_DEVICE'

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SaasOperationRequest, self).__init__('SaaS_Operation_Request__c', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_account_merge_devices(cls, sf_interface, sf_source_account, sf_dest_account):

        # create request for devices
        return cls._create_account_merge(sf_interface, sf_source_account.id(True), sf_dest_account.id(), cls.REPARENT_DEVICES)

    @classmethod
    def create_account_merge_subscriptions(cls, sf_interface, sf_source_account, sf_dest_account):

        # create request for subscriptions
        return cls._create_account_merge(sf_interface, sf_source_account.id(True), sf_dest_account.id(),
                                         cls.REPARENT_SUBSCRIPTIONS)

    @classmethod
    def _create_account_merge(cls, sf_interface, sf_source_account_id, sf_dest_account_id, operation):

        # create saas request
        required_params = {
            'OperationObjectId__c': sf_source_account_id,
            'Account__c': sf_dest_account_id,
            'Operation__c': operation
        }

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def create_provision_device(cls, sf_interface: salesforce_fixture, sf_feature_key: str, sf_account: SFAccount,
                                sf_device: SFDevice) -> 'SaasOperationRequest':
        """Setup a new PROVISION_DEVICE SaasOperationRequest.

        Args:
            sf_interface: Salesforce fixture.
            sf_feature_key: Feature to be used for newly provisioned device.
            sf_account: Salesforce account.
            sf_device: New device to provision.

        Returns:
            SaasOperationRequest

        """
        required_params = {
            'Feature_Key__c': sf_feature_key,
            'Account__c': sf_account.id(True),
            'Device__c': sf_device.id(True),
            'Operation__c': cls.PROVISON_DEVICE
        }

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        return cls(sf_interface, sf_id=sf_id)

    def wait_for_completed(self, timeout_sec=10, interval_sec=2):
        wait_for_with_interval(timeout_sec, interval_sec, True, self.get_completed)

    def get_completed(self):
        self.sf_get()
        return self.sf_attributes['Completed__c']
