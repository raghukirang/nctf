import random

from nctf.libs.common_library import random_hex_string
from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.test_lib.salesforce.objects.featurelist import SFFeatureList
from nctf.test_lib.salesforce.objects.object import SFObject
from nctf.test_lib.salesforce.objects.product import SFProduct



class SFPricebookEntry(SFObject):

    master_pricebook = 'CP Master Price Book'

    def __init__(self, sf_interface, required_params=None, pricebook_name='', sf_id=None):
        super(SFPricebookEntry, self).__init__('PricebookEntry', sf_interface)

        if required_params is not None:
            required_params['UnitPrice'] = 1
            required_params['IsActive'] = True
            self.sf_create(required_params)
        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()


    @classmethod
    def create(cls, sf_interface, required_params={}):

        # create a standard pricebook entry
        standard_pricebook = cls.create_standard(sf_interface, required_params)

        # get pricebook id
        pricebooks = sf_interface.query(
            "SELECT Id FROM Pricebook2 WHERE Name = '{}'".format(SFPricebookEntry.master_pricebook))

        if not pricebooks['records']:
            raise KeyError('No such pricebook: {} '.format(SFPricebookEntry.pricebook_name))

        pricebook_id = pricebooks['records'][0]['Id']

        required_params['Pricebook2Id'] = pricebook_id

        return cls(sf_interface, required_params=required_params)

    @classmethod
    def create_standard(cls, sf_interface, required_params={}):
        # have to add the product to the standard pricebook before custom ones
        std_pricebooks = sf_interface.query(
            "SELECT Id FROM Pricebook2 WHERE isStandard = true")

        if not std_pricebooks['records']:
            raise KeyError('Std pricebook not found!')

        pricebook_id = std_pricebooks['records'][0]['Id']

        required_params['Pricebook2Id'] = pricebook_id

        return cls(sf_interface, required_params=required_params)



