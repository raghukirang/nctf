"""Salesforce Object Models

These Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""
from faker import Faker
from nctf.libs.common_library import random_string
from nctf.test_lib.salesforce.fixture import _SalesforceConnection
from nctf.test_lib.salesforce.objects.object import SFObject


class SFContact(SFObject):
    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(SFContact, self).__init__('Contact', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def create_random(cls, sf_interface, optional_params=None):
        first_name = Faker().first_name()
        last_name = Faker().last_name()
        username = f"{first_name}_{last_name}_{random_string(10)}"
        required_params = {
            'FirstName': first_name,
            'LastName': last_name,
            'Email': f'{username}@cradlepoint.mailinator.com'
        }
        if optional_params is not None:
            required_params.update(optional_params)

        return cls(sf_interface, required_params)

    @classmethod
    def get_contact_from_account(cls, sf_interface: _SalesforceConnection, sf_id: str) -> 'SFContact':
        """Retrieves SFContact from the specified SFAccount.

        Args:
            sf_interface: Salesforce Fixture
            sf_id: Salesforce ID of specified Account

        Returns:
            SFContact assigned to the SFAccount
        """

        contact_objs = sf_interface.query("SELECT Id FROM Contact WHERE AccountId = '{}'".format(sf_id))['records']

        if not contact_objs:
            raise ValueError('Contact not found for Account {}'.format(sf_id))

        return cls(sf_interface, None, contact_objs[0]['Id'])
