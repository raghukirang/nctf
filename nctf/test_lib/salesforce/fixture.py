"""Fixture and Interface to interact with Salesforce via it's API

This module provides an interface to connect and interact with Salesforce via the
sandbox's API. The interface utilizes a private singleton salesforce connection object
to ensure that there is only one connection at a time.

This module also utilizes the 'stack_option' fixture that is created within *conftest.py*.
In order to provide a connection to Salesforce, this module requires the following
configuration values within the specified stack:
    - target.predefinedAuth.salesforce.username
    - target.services.salesforce.hostname
    - target.predefinedAuth.salesforce.password
    - target.predefinedAuth.salesforce.token
    - run.fixtures.salesforce.queryTimeout

Example:
    ::

        from journey.fixtures.salesforce.interface import salesforce_interface
        from journey.libs.salesforce.objects import SFAccount

        def test_connect_salesforce(salesforce_interface):
            sf_account = SFAccount(salesforce_interface, create_random=True)

"""
import logging
import time

from nctf.test_lib.base.fixtures.fixture import FixtureBase
import requests
import simple_salesforce


class _SalesforceConnection(FixtureBase):
    """Singleton class to instantiate a *simple_salesforce* connection to a specified stack.

    Notes:
        This is a private singleton class. Access should be limited to the 'salesforce_fixture' fixture only.

        Requires the configuration values:
            - target.predefinedAuth.salesforce.username
            - target.services.salesforce.hostname
            - target.predefinedAuth.salesforce.password
            - target.predefinedAuth.salesforce.token
            - run.fixtures.salesforce.queryTimeout

    Attributes:
        username (str): Salesforce API Username.
        password (str): Salesforce API Password.
        instance_url (str): Salesforce API Instance URL.
        security_token (str): Salesforce API Account Security Token.
        query_timeout (int): Salesforce SOQL query default timeout.
        connection (simple_salesforce.Salesforce): API Connection to specified Salesforce stack.
    """

    # Config Definitions
    username = None
    password = None
    instance_url = None
    security_token = None
    query_timeout = None

    __instance = None
    __object_stack = list()
    connection = None

    logger = logging.getLogger('salesforce.interface')

    def __new__(cls, config_fixture):
        """Singleton Initializer.

        If an instance of this class exists, it returns the existing instance. Otherwise, it creates a new instance.
        This allows this object to only exist once, so only one connection to SF will exist at a time.

        Return:
            _SalesforceConnection: An active class instance with a instantiated connection to the Salesforce API.
        """
        if not _SalesforceConnection.__instance:
            _SalesforceConnection.__instance = object.__new__(cls)
            _SalesforceConnection.reset_singleton(config_fixture)

        return _SalesforceConnection.__instance

    @staticmethod
    def reset_singleton(config_fixture):
        """Resets the state and connection to Salesforce

        Retrieves and re-instantiates the stack_option configuration values, then attempts to re-create and establish a connection to the
        Salesforce API via the simple_salesforce package.
        """
        _SalesforceConnection.__instance.instance_url = config_fixture.target.services.salesforce.hostname
        _SalesforceConnection.__instance.username = config_fixture.target.predefinedAuth.salesforce.username
        _SalesforceConnection.__instance.password = config_fixture.target.predefinedAuth.salesforce.password
        _SalesforceConnection.__instance.security_token = config_fixture.target.predefinedAuth.salesforce.token
        _SalesforceConnection.__instance.query_timeout = config_fixture.run.fixtures.salesforce.queryTimeout
        _SalesforceConnection.__instance.establish_connection()

    def establish_connection(self):
        """Establishes a connection to the Salesforce API via the simple_salesforce package.

        raises:
            Requests Timeout: Connection to Salesforce fails.
            Requests TooManyRedirects: Too many redirects to Salesforce [Possibly bad instance url].
            SalesforceAuthenticationFailed: Incorrect Salesforce User Authentication [Bad config values, or API user doesn't exist in stack].
            SalesforceExpiredSession: Connection to Salesforce expired [Too long of a wait/timeout?].
            SalesforceGeneralError: Generic Salesforce Exception Handler.
            RequestException: Generic Requests Exception Handler.
        """
        try:
            self.logger.debug('Establishing SF Connection: {}'.format(self.instance_url))
            self.connection = simple_salesforce.Salesforce(
                instance_url=self.instance_url,
                username=self.username,
                password=self.password,
                security_token=self.security_token,
                session=requests.Session(),
                sandbox=True)

        except requests.exceptions.Timeout as e:
            self.logger.error('Connection to \'{}\' timed out!'.format(self.instance_url))
            raise e
        except requests.exceptions.TooManyRedirects as e:
            self.logger.error('Too Many Redirects for \'{}\'. Bad URL?'.format(self.instance_url))
            raise e
        except simple_salesforce.SalesforceAuthenticationFailed as e:
            self.logger.error('Authentication failed!:\n\nException Message:\n{}'.format(e))
            raise e
        except simple_salesforce.SalesforceExpiredSession as e:
            self.logger.error('Expired Session!:\n\nException Message:\n{}'.format(e))
            raise e
        except simple_salesforce.SalesforceGeneralError as e:
            self.logger.error('Caught Salesforce Exception!: {}'.format(e))
            raise e
        except requests.exceptions.RequestException as e:
            self.logger.error('Caught Requests Exception!: {}'.format(e))
            raise e

    def query(self, query: str, retries: int = 24, **kwargs) -> dict:
        """Wrapper for SOQL queries to enforce timeout and retry behaviour.

        Enforces default timeout, but can be overridden via keyword argument.
        `retries` specifies the number of attempts that will be made to retry
        the query if either a timeout or "UNABLE_TO_LOCK_ROW" error occurs.

        The following is copied from SimpleSalesforce's query():

        Returns the result of a Salesforce SOQL query as a dict decoded from
        the Salesforce response JSON payload.

        Args:
            query: The SOQL query to send to Salesforce, e.g.
                `SELECT Id FROM Lead WHERE Email = "waldo@somewhere.com"
            retries: Number of retry attempts.

        Returns:
            The result of a Salesforce SOQL query as a dict decoded from
                the Salesforce response JSON payload.
        """
        args = {'timeout': self.query_timeout}  # Default timeout
        args.update(kwargs)

        for _ in range(retries):
            try:
                return self.connection.query(query, **args)
            except requests.exceptions.Timeout:
                self.logger.warning("Query timed out with timeout={}. Retrying query.".format(args['timeout']))
            except simple_salesforce.api.SalesforceMalformedRequest as e:
                if "UNABLE_TO_LOCK_ROW" not in str(e):
                    raise e
                self.logger.warning("Couldn't perform query due to row lock. Retrying query in 2 seconds.")
                time.sleep(1)
            except simple_salesforce.api.SalesforceGeneralError as e:
                if e.status == 503:
                    self.logger.warning('503 error in salesforce. Retrying query in 1 second.')
                    time.sleep(1)
                else:
                    raise e

        self.logger.warning("On final attempt for query {}".format(query))
        return self.connection.query(query, **args)

    def _add_object(self, sf_object):
        """Appends an SFObject to the *__object_stack* for future cleanup."""
        self.__object_stack.append({'type': sf_object.metatype, 'id': sf_object.sf_id, 'object': sf_object})

    def _remove_object(self, sf_object):
        """Removes an SFObject from the *__object_stack*, for cleanup."""
        # This is not how you should use a Stack! But... Python!
        for entry in self.__object_stack:
            if entry['id'] == sf_object.sf_id:
                self.__object_stack.remove(entry)
                break

    def _cleanup(self):
        """Deletes all SFObjects recorded in *__object_stack*.

        The purpose of this function is to delete all recorded SFObjects that were created in Salesforce.
        It a test passes/fails/halts due to any reason, this function should run after the tests to attempt
        to clean up any objects that may not have been deleted already.

        Note:
            **This should only be used within salesforce_fixture!**... Seriously!

        Note:
            This will only delete objects that have been recorded via *__object_stack*. This will not delete
            objects that were created via Salesforce workflow or validation rules, and deletions may fail due
            to untracked object dependencies. I.e: An account has an untracked Entitlement, the account deletion
            will fail, as the entitlement wasn't deleted first.
        """

        # NTI-88 workaround
        for curr_obj in self.__object_stack:
            if curr_obj['type'] == 'Account':
                try:
                    curr_obj['object'].sf_update({'IsCustomerPortal': False, 'IsPartner': False})
                except Exception as e:
                    pass  # This workaround isn't always needed, so I don't want teardown to fail here for any reason

        non_compliant = None

        while len(self.__object_stack) > 0:
            curr_obj = self.__object_stack.pop()

            # NTI-178 Workaround
            skip = False
            if curr_obj['type'] == 'Account':
                # Causes a nasty circular import that is non-trivial to fix if I import it at the top
                from nctf.test_lib.salesforce.objects.entitlement import SFEntitlement
                sf_entitlements = SFEntitlement.entitlements_for_account(sf_interface=self,
                                                                         account_id=curr_obj['object'].sf_id)
                for entitlement in sf_entitlements:
                    if entitlement.sf_attributes['Name'] == 'NON-COMPLIANT':
                        non_compliant = entitlement
                        skip = True
                        break

            if not skip:
                try:
                    curr_obj['object'].sf_delete()
                except simple_salesforce.SalesforceResourceNotFound:
                    self.logger.info(
                        '\t\tObject \'{}\' has already been deleted/doesn\'t exist. Skipping...'.format(curr_obj['id']))
                except simple_salesforce.SalesforceMalformedRequest as e:
                    self.logger.error('Unable to delete Object: {}'.format(curr_obj['id']))

        if non_compliant:
            non_compliant.sf_delete()

    def teardown(self) -> None:
        self._cleanup()

    def get_failure_report(self) -> str:
        msg = "Salesforce Fixture Failure Report\n"
        msg += "Known objects:\n"
        for idx, s_obj in enumerate(self.__object_stack):
            msg += "  Obj #{}: {}\n".format(idx, s_obj)
        return msg
