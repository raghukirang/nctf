"""This module is a Activity Log page"""

import logging

from nctf.test_lib.als.pages.common.als_base import ALSBasePage
from nctf.test_lib.base import pages

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class Toolbar(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.CLASS_NAME, "cp-toolbar")

    def wait_for_region_to_load(self):
        self.refresh_button.wait_for_clickable(timeout=30)  # although this button clickable seems to work too

    @property
    def details_button(self):
        return pages.UIRegionLinkElement(
            name='Details Button', parent=self, region_cls=ActivitiesDetails, strategy=pages.By.ID, locator='detailsButton')

    @property
    def refresh_button(self):
        return pages.UIRegionLinkElement(
            name='refresh_button',
            parent=self.parent,
            region_cls=ActivitiesLogTable,
            strategy=pages.By.ID,
            locator='refreshButton',
            region_parent=self.parent)

    class ExportDropDownRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CLASS_NAME, 'dropdown-menu')

    @property
    def export_dropdown(self):
        return pages.UIRegionLinkElement(
            name="Export Dropdown",
            parent=self,
            region_cls=Toolbar.ExportDropDownRegion,
            strategy=pages.By.ID,
            locator='exportButton',
            region_parent=self)


class ActivitiesDetails(pages.UIRegion):
    ROOT_LOCATOR = (pages.By.ID, 'cp-activity-details')

    @property
    def close_button(self):
        return pages.UIPageLinkElement(
            name='Close Button',
            parent=self,
            page_cls=ActivitiesLogPage,
            frame=self.parent.frame,
            strategy=pages.By.CLASS_NAME,
            locator='close')

    @property
    def maximize_button(self):
        return pages.UIRegionLinkElement(
            name='Maximize Button',
            parent=self,
            region_cls=ActivitiesDetails,
            strategy=pages.By.CLASS_NAME,
            locator='maximize-icon')

    @property
    def ok_button(self):
        return pages.UIPageLinkElement(
            name='OK Button',
            parent=self,
            page_cls=ActivitiesLogPage,
            frame=self.parent.frame,
            strategy=pages.By.CLASS_NAME,
            locator='btn-default')

    class BodyRegion(pages.UIRegion):
        ROOT_LOCATOR = (pages.By.CLASS_NAME, 'modal-body')
        # TODO: probably should be modeled with a list of rows

    @property
    def body(self):
        return ActivitiesDetails.BodyRegion(parent=self)


class ActivitiesLogTable(pages.EmberTable):
    ROOT_LOCATOR = (pages.By.CLASS_NAME, "ember-light-table")

    def refresh(self):
        return self.parent.toolbar.refresh_button.click()


class ActivitiesLogPage(ALSBasePage):
    URL_TEMPLATE = '{path_tbd}'

    @property
    def toolbar(self):
        return Toolbar(parent=self)

    @property
    def activities_log(self):
        return ActivitiesLogTable(parent=self)
