import logging

from nctf.test_lib.base import pages

logger = logging.getLogger(pages.make_short_qualified_name(__name__))


class ALSBasePage(pages.UIPage):

    WEB_SERVICE = 'als'

    @property
    def spinner(self):
        return pages.UISpinnerElement(self, strategy=pages.By.CSS_SELECTOR, locator="div.x-mask-canvasloader")

    def wait_for_page_to_load(self):
        super().wait_for_page_to_load()
        self.spinner.wait_for_spinner()
        return self
