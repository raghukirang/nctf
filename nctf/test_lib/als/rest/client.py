"""ALS REST API Client."""
import datetime
import logging

import pytz
from requests import Response
from requests import Session
from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint


logger = logging.getLogger('test_lib.als.rest')

ACTIVITIES_ENDPOINT = '/api/v1/activities'


class ALSv1RESTClient(RESTClient):
    """Activity Log Service REST API client."""

    DEFAULT_HEADERS = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/vnd.api+json'
    }

    def __init__(self, protocol: str, hostname: str, port: int,
                 timeout: float = 30.0, session: Session = None):
        """Initialize Endpoint and REST client."""
        super().__init__(protocol, hostname, port, timeout, session)

        self.activities = ActivitiesEndpoint(self, ACTIVITIES_ENDPOINT,
                                             enforce_trailing_slash=False)

    def authenticate(self, *args, **kwargs):
        pass


class ALSv1Endpoint(RESTEndpoint):
    """V1 of ALS Endpoint.

    Acivity, Actor, and Object types updated from:
    https://gitlab.cradlepoint.com/ecm/activity_log_service/
    blob/master/src/data/models/activity_log.py
    """

    @staticmethod
    def action_type(activity_type: str) -> int:
        """
        Convert activity type to numeric value.

        Args:
            activity_type
        """
        return {
            'create': 1,
            'delete': 2,
            'update': 3,
            'request': 4,
            'report': 5,
            'login': 6,
            'logout': 7,
            'register': 8,
            'unregister': 9,
            'activate': 10,
        }.get(activity_type)

    @staticmethod
    def actor_type(actor_type: str) -> int:
        """
        Convert actor type to numeric value.

        Args:
             actor_type
        """
        return {'system': 1,
                'user': 2,
                'api_key': 3,
                'router': 4,
                'sso_user': 5,
                'access_point': 6
               }.get(actor_type)

    @staticmethod
    def object_type(object_type: str) -> int:
        """
        Convert object type to numeric value.

        Args:
            object_type
        """
        return {
            'account': 1,
            'user': 2,
            'group': 3,
            'router': 4,
            'schedule': 5,
            'sso_user': 6,
            'sso_account': 7,
            'task': 8,
            'api_key': 9,
            'net_device': 10,
            'notifier': 11,
            'feature_binding': 12,
            'authorization': 13
        }.get(object_type)

    @staticmethod
    def timestamp(time_span_days: int = 0, timezone: str =
                  'America/Boise') -> str:
        """Return formatted timestamp the requested number of days prior.

        Args:
            time_span_days: When specifying a previous timestamp, the amount
            of days back you wish to cover.
            timezone: Region/City for your location, contained within
            pytz.all_timezones

        Returns:
            A properly formated timezone string that can be passed to the
            Activity Log Service.

        Notes:
            You must always pass in a GTE timestamp when making calls to ALS.
            Failure to do so can result in excessively large reads as an entire
             DB table would need to be read, rather than a subset beginning
            with the "greater than or equal to" (GTE).

        """
        if timezone not in pytz.all_timezones:
            raise ValueError("Invalid timezone argument.")
        tz = pytz.timezone(timezone)
        dt = tz.localize(datetime.datetime.utcnow().replace(microsecond=0) -
                         datetime.timedelta(days=time_span_days))
        timestamp = dt.isoformat()
        return timestamp


class ActivitiesEndpoint(ALSv1Endpoint):
    """ALS Endpoint."""

    @checked_status(200)
    def log_activity(self, limit: str = 25, gte_days: str = 14,
                     lte_days: str = 0, *args, **kwargs) -> Response:
        """Return the most recent log activities."""
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "ordering": "-action.id",
            "limit": limit,
            "action.timestamp_gte": self.timestamp(time_span_days=gte_days),
            "action.timestamp_lte": self.timestamp(time_span_days=lte_days)
        }
        return self._get(params=params, *args, **kwargs)

    @checked_status(200)
    def router_registration_log_activity(self, limit: str = 25,
                                         gte_days: str = 14, lte_days: str = 0,
                                         *args, **kwargs
                                        ) -> list:
        """Return the most recent router registration log activities."""
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "ordering": "-action.id",
            "limit": limit,
            "actor.type": self.actor_type('router'),
            "action.type": self.action_type('register'),
            "object.type": self.object_type('router'),
            "action.timestamp_gte": self.timestamp(time_span_days=gte_days),
            "action.timestamp_lte": self.timestamp(time_span_days=lte_days)
        }
        return self._get(params=params, *args, **kwargs).json()['data']

    @checked_status(200)
    def user_login_log_activity(self, limit: str = 25, gte_days: str = 14,
                                lte_days: str = 0, *args, **kwargs) -> list:
        """Return the most recent user login history."""
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "ordering": "-action.id",
            "limit": limit,
            "actor.type": self.actor_type('sso_user'),
            "action.type": self.action_type('login'),
            "object.type": self.object_type('sso_user'),
            "action.timestamp_gte": self.timestamp(time_span_days=gte_days),
            "action.timestamp_lte": self.timestamp(time_span_days=lte_days)
        }
        return self._get(params=params, *args, **kwargs).json()['data']

    @checked_status(200)
    def group_creation_log_activity(self, limit: str = 25,
                                    gte_days: str = 14, lte_days: str = 0,
                                    *args, **kwargs
                                   ) -> list:
        """Return the most recent Group Creation log activities."""
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "ordering": "-action.id",
            "limit": limit,
            "actor.type": self.actor_type('sso_user'),
            "action.type": self.action_type('create'),
            "object.type": self.object_type('group'),
            "action.timestamp_gte": self.timestamp(time_span_days=gte_days),
            "action.timestamp_lte": self.timestamp(time_span_days=lte_days)
        }
        return self._get(params=params, *args, **kwargs).json()['data']

    @checked_status(200)
    def sso_user_creation_log_activity(self, limit: str = 25,
                                       gte_days: str = 14, lte_days: str = 0,
                                       *args, **kwargs
                                      ) -> list:
        """Return the most recent SSO User Creation log activities."""
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "ordering": "-action.id",
            "limit": limit,
            "actor.type": self.actor_type('sso_user'),
            "action.type": self.action_type('create'),
            "object.type": self.object_type('sso_user'),
            "action.timestamp_gte": self.timestamp(time_span_days=gte_days),
            "action.timestamp_lte": self.timestamp(time_span_days=lte_days)
        }
        return self._get(params=params, *args, **kwargs).json()['data']

    @checked_status(200)
    def remote_connect_activity(self, limit: str = 25,
                                gte_days: str = 14, lte_days: str = 0,
                                *args, **kwargs
                               ) -> list:
        """Return the most recent remote connect log activities."""
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        params = {
            "ordering": "-action.id",
            "limit": limit,
            "actor.type": self.actor_type('sso_user'),
            "action.type": self.action_type('request'),
            "object.type": self.object_type('router'),
            "action.timestamp_gte": self.timestamp(time_span_days=gte_days),
            "action.timestamp_lte": self.timestamp(time_span_days=lte_days)
        }
        return self._get(params=params, *args, **kwargs).json()['data']
