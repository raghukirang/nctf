import logging
from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
from requests import Response
from requests import Session

logger = logging.getLogger('test_lib.als.rest')

CONNECTIONS_ENDPOINT = '/api/v1/connections'
PROTOCOLS_ENDPOINT = '/api/v1/protocols'


class Overmindv1RESTClient(RESTClient):
    """Overmind REST API client

    """

    DEFAULT_HEADERS = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/vnd.api+json'
    }

    def __init__(self, protocol: str, hostname: str, port: int, timeout: float = 30.0, session: Session = None):
        super().__init__(protocol, hostname, port, timeout, session)

        self.connections = ConnectionsEndpoint(self, CONNECTIONS_ENDPOINT, enforce_trailing_slash=False)
        self.protocols = ProtocolsEndpoint(self, PROTOCOLS_ENDPOINT, enforce_trailing_slash=False)

    def authenticate(self, *args, **kwargs):
        pass


class Overmindv1Endpoint(RESTEndpoint):
    pass


class ProtocolsEndpoint(Overmindv1Endpoint):
    @checked_status(200)
    def get(self, *args, **kwargs) -> Response:
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        return self._get(*args, **kwargs)


class ConnectionsEndpoint(Overmindv1Endpoint):
    @checked_status(201)
    def create(self,
               router_id: int,
               authorization_id: str,
               protocol_id: int,
               is_cradlepoint_device: bool = True,
               **kwargs) -> Response:
        payload = {
            "data": {
                "type": "connections",
                "attributes": {
                    "routerId": router_id,
                    "authorizationId": authorization_id,
                    "cradlepointDevice": is_cradlepoint_device
                },
                "relationships": {
                    "protocol": {
                        "data": {
                            "type": "protocols",
                            "id": protocol_id
                        }
                    }
                }
            }
        }
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS
        return self._post(json=payload, **kwargs)

    @checked_status(200)
    def get_connection(self,
                       connection_id: str,
                       *args,
                       **kwargs) -> Response:
        if 'headers' not in kwargs:
            kwargs['headers'] = self.client.DEFAULT_HEADERS

        return self._get(resource_id=connection_id, *args, **kwargs)
