from attrdict import AttrMap


class ServicesInfo(AttrMap):
    def get_url(self, service: str) -> str:
        """Returns a URL of the given named service.

        Args:
            service: string name of the service to look up in the AttrMap to build a URL.

        Returns:
            A URL string built from the given service port, protocol, hostname and path.
        """
        port = self[service].get('port', None)
        protocol = self[service]['protocol']
        host = self[service]['hostname']
        path = self[service].get('path', '')
        if port:
            return "{}://{}:{}{}".format(protocol, host, port, path)
        else:
            return "{}://{}{}".format(protocol, host, path)
