"""A helper to assist with ECM Table validation"""
import typing

from .. import selenium_deps
from ..by import By
from ..elements import UISpinnerElement
from ..exceptions import ElementNotViewableError
from ..table import UITable
from .paging import UIPagingTable


class ExtJSTable(UIPagingTable):
    """A suite of tools to assist with Table validation for tables written in ExtJS.

    Notes:
        - Since checkboxes have no unique text associated with them we're using a hard-coded key of "Checkbox"
        to represent it.

        - The representation of text is a best effort to use either the text associated with a cell, or
        the text from the data-qtip. If neither of these are present the cell will have no text associated with it.

    Examples:
        container_xpath = "//div[contains(@class, 'x-panel')]" \
                          "//div[contains(@id, 'ecm-core-view-devices-Routers')" \
                          " and contains(@class, 'x-grid-with-row-lines')]"
        table = ECMTable(session, container_xpath, By.XPATH)

        # Get all rows where the product is a 3200v
        3200v_rows = table.get_rows('Product', '3200v')

        # Get all rows
        all_rows = table.get_rows()

        # Get all rows with a specific IP Address
        ip_rows = table.get_rows('IP Address', '10.1.1.1')
    """
    PAGING_TOOLBAR = (By.XPATH, ".//div[starts-with(@id, 'pagingtoolbar')]")
    IS_PAGEABLE = True
    """By default all ExtJSTables support paging.
    
    Override the toolbar location and whether paging is supported in inheriting table types.  If paging is not supported by
    the table, make PAGING_TOOLBAR = None and IS_PAGEABLE = False.
    """
    TABLE_HEADER = (By.XPATH, ".//div[contains(@class, 'x-grid-header-ct')]//div[contains(@class, 'x-box-target')]"
                    "/div[contains(@class, 'x-column-header') and not(contains(@style, 'display: none'))]")
    """The strategy, locator for where the header is located."""

    TABLE_CONTENT = (By.XPATH,
                     ".//table[@role='presentation' and contains(@class, 'x-grid-table') and contains(@id, 'table')]")

    @property
    def spinner(self):
        return UISpinnerElement(self.page, strategy=By.ID, locator="canvasLoader")

    @property
    def headers(self) -> UITable.Headers:
        hdr_elems = self.wait_for_region_to_load().find_elements(*self.TABLE_HEADER)

        headers = UITable.Headers()

        column_count = 1
        for hdr in hdr_elems:
            hdr_text = 'Unknown Column {}'.format(column_count)
            # make sure all columns visible otherwise getting the text won't work
            if not hdr.is_displayed():
                raise ElementNotViewableError('Column {} header not viewable. Resize the browser or scroll into view.'
                                              .format(column_count), self)
            if hdr.text.strip():
                hdr_text = hdr.text
            else:
                if column_count == 1:
                    hdr_text = 'CheckBox'
                else:
                    try:
                        qt_element = hdr.find_element_by_xpath(".//*[@data-qtip]")
                        qtip_text = qt_element.get_attribute('data-qtip')
                        if qtip_text:
                            hdr_text = qtip_text
                    except selenium_deps.NoSuchElementException:
                        pass
            headers[hdr_text] = UITable.Header(hdr_text, self, element=hdr)
            column_count += 1
        return headers

    class Cell(UITable.Cell):
        @property
        def text(self) -> str:
            text = self.cached_html.text
            if len(text) == 0:
                text = self.cached_html.find('span').attrs['data-qtip']
            elif len(text) == 1 and (ord(text) == 32 or ord(text) == 160):
                if 'x-grid-cell-row-checker' in self.cached_html.attrs['class']:
                    text = 'Checkbox'
            return text

    class Row(UITable.Row):
        @property
        def record_id(self) -> str:
            """ Returns the data-recordid of the row which typically maps to an useful id to the service e.g. NCM group_id."""
            return self.cached_html.attrs.get('data-recordid')

    def get_rows(self, key: str = None, value: str = None, regex: str = None) -> typing.List['ExtJSTable.Row']:
        """Gets the rows which match the key, value pair, if supplied.

        Args:
            key: The table header name for a column.
            value: The value present in the cell.
            regex: A regular expression that can be used to locate text in a cell.

        Raises:
            ValueError: If the caller uses both @value and @regex

        Returns:
            A list of all matching rows.
        """
        return super(ExtJSTable, self).get_rows(key=key, value=value, regex=regex)
