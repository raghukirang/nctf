from ..by import By
from ..elements import UIElement
from ..elements import UIReadOnlyElement
from ..region import UIRegion
from .table import UITable


class UIPagingTable(UITable):
    PAGING_TOOLBAR = (By.XPATH, ".//div[starts-with(@id, 'pagingtoolbar')]")
    IS_PAGEABLE = True
    FIRST_PAGE = (By.CLASS_NAME, 'x-tbar-page-first')
    PREVIOUS_PAGE = (By.CLASS_NAME, 'x-tbar-page-prev')
    GOTO_PAGE = (By.TAG_NAME, 'input')
    TOTAL_PAGE = (By.XPATH, ".//div[contains(text(),'of ')]")
    NEXT_PAGE = (By.CLASS_NAME, 'x-tbar-page-next')
    LAST_PAGE = (By.CLASS_NAME, 'x-tbar-page-last')

    class PagingToolbar(UIRegion):
        class Button(UIElement):
            def click(self) -> 'UIPagingTable':
                """Performs a click on the element and returns the table so that calls can be chained."""
                super().click()
                return self.parent.parent.wait_for_region_to_load()

        @property
        def first_page(self):
            return UIPagingTable.PagingToolbar.Button("First Page Button", self, self.parent.FIRST_PAGE[0],
                                                      self.parent.FIRST_PAGE[1])

        @property
        def previous_page(self):
            return UIPagingTable.PagingToolbar.Button("Previous Page Button", self, self.parent.PREVIOUS_PAGE[0],
                                                      self.parent.PREVIOUS_PAGE[1])

        @property
        def page_to_goto(self) -> UIElement:
            return UIElement("Page to Goto", self, self.parent.GOTO_PAGE[0], self.parent.GOTO_PAGE[1])

        @property
        def total_pages_of(self) -> UIReadOnlyElement:
            return UIReadOnlyElement(
                "Total Pages Label",
                self,
                self.parent.TOTAL_PAGE[0],
                self.parent.TOTAL_PAGE[1],
                regex_sub=("(.*of\D*)(\d*)(.*)", "\\2"))

        @property
        def next_page(self):
            return UIPagingTable.PagingToolbar.Button("Next Page Button", self, self.parent.NEXT_PAGE[0],
                                                      self.parent.NEXT_PAGE[1])

        @property
        def last_page(self):
            return UIPagingTable.PagingToolbar.Button("Last Page Button", self, self.parent.LAST_PAGE[0],
                                                      self.parent.LAST_PAGE[1])

    @property
    def paging_toolbar(self):
        return UIPagingTable.PagingToolbar(self, strategy=self.PAGING_TOOLBAR[0], locator=self.PAGING_TOOLBAR[1])

    def first_page(self) -> 'UIPagingTable':
        """Click the first page button '<<', returning the table to the caller."""
        return self.paging_toolbar.first_page.click()

    def previous_page(self) -> 'UIPagingTable':
        """Click the previous page button '<', returning the table to the caller."""
        return self.paging_toolbar.previous_page.click()

    def goto_page(self, page_number: int) -> 'UIPagingTable':
        """Enter the given number and an 'enter', then wait for the table to be loaded,
        returning the table to the caller.
        """
        self.paging_toolbar.page_to_goto.set_text(str(page_number) + '\n')
        return self.parent.wait_for_region_to_load()

    def next_page(self) -> 'UIPagingTable':
        """Click the next page button '>', returning the table to the caller."""
        return self.paging_toolbar.next_page.click()

    def last_page(self) -> 'UIPagingTable':
        """Click the last page button '>>', returning the table to the caller."""
        return self.paging_toolbar.last_page.click()

    @property
    def total_pages(self) -> int:
        """Return the total number of pages in the table."""
        total = self.paging_toolbar.total_pages_of.get_text()
        return 0 if total == '' else int(total)
