"""A base class implementation for UI Tables."""

import abc
import collections
import re
import typing

from bs4 import BeautifulSoup

from .. import base
from .. import by
from .. import elements
from .. import frame
from .. import region
from .. import selenium_deps


class UITable(region.UIRegion, abc.ABC):
    """A suite of tools to assist with table validation in Journey.

    Args:
        selector: The selector for a web element which contains both the ECM table and the headers for said table.
        method: The method for the corresponding selector
    """
    IS_SCROLLABLE = False
    IS_PAGEABLE = False
    """UITable does not implement paging or scrolling. Inheriting types should implement and override these as appropriate."""

    TABLE_CONTENT = (by.By.CSS_SELECTOR, 'tbody')
    """The strategy, locator of where the table html element is that contains the content."""

    def __init__(self,
                 parent: base.UIViewBase,
                 name: str = None,
                 strategy: str = None,
                 locator: str = None,
                 root_element=None,
                 timeout: float = None,
                 frame: frame.UIFrame = None):
        self.container = None
        self._scrollbar = None
        super().__init__(
            parent=parent,
            name=name,
            strategy=strategy,
            locator=locator,
            root_element=root_element,
            timeout=timeout,
            frame=frame)

    def wait_for_region_to_load(self) -> 'UITable':
        super().wait_for_region_to_load()
        self.spinner.wait_for_spinner()
        return self

    @property
    @abc.abstractmethod
    def spinner(self) -> elements.UISpinnerElement:
        pass

    class Header(base.UIElementBase):
        def __init__(self, name: str, parent: 'UITable', element: selenium_deps.WebElement):
            super().__init__(name=name, parent=parent, element=element)
            self.header_text = name

    Headers = collections.OrderedDict

    @property
    @abc.abstractmethod
    def headers(self) -> Headers:
        """Get the headers of the table.

        Returns:  really should return a collections.OrderedDict[str, UIElement].
            Python won't let me type hint the key value.

        """
        pass

    class Cell(abc.ABC):
        """The definition of what is meant by a cell.

        Minimally this is a UIElement of the <td> and the text that the cell represents.  It is intended that
        specific versions of Cell for a given table will inherit and add other interesting properties that are
        parsed from the self.cached_html.

        Args:
            row: row number this cell is in.
            header: the header string of the column.
            table: the UITable that this cell is in.
            html_tree: a serialized version of the <td> in the DOM.
        """

        def __init__(self, row: int, header: str, table: 'UITable', html_tree: BeautifulSoup):
            self.ui_element = base.UIElementBase(
                name='[{}]{}'.format(row, header), parent=table, strategy=by.By.ID, locator=html_tree.attrs["id"])
            super().__init__()
            self.header = header
            self.row = row
            self.cached_html = html_tree

        def __str__(self) -> str:
            return self.text

        @property
        @abc.abstractmethod
        def text(self) -> str:
            """Parse out from self.cached_html the string that represents the text of the cell.

            In most cases this is just the self.cached_html.text.

            Returns: a string representing the text of the cell.
            """
            pass

    class Row(object):
        """The definition of a cached row in the table.

            Args:
                table: the parent of the row.
                headers: a ordered dictionary of the columns indexed by the column header name.  This is not derived from the
                    table passed in since it will cause the headers to be rebuilt for each row instantiated.
                row_count: the 1 based row number to associate with the row for debug naming purposes.
                html_tree: the <tr> of the DOM as expressed as a BeautifulSoup class
        """

        def __init__(self, table: 'UITable', headers: 'UITable.Headers', row_count: int, html_tree: BeautifulSoup):
            super().__init__()
            self._cells = collections.OrderedDict()
            self.ui_element = base.UIElementBase(
                name='[{}]'.format(row_count), parent=table, strategy=by.By.ID, locator=html_tree.attrs["id"])
            self.cached_html = html_tree
            hdr_itr = iter(headers)
            for cell in html_tree.find_all('td'):
                col_header = next(hdr_itr)
                self._cells[col_header] = table.Cell(row=row_count, header=col_header, table=table, html_tree=cell)
            pass

        def __str__(self) -> str:
            return "{{{}}}".format(", ".join("'{}':'{}'".format(header, cell) for header, cell in self.cells.items()))

        def __repr__(self):
            return "\n{}".format(self.__str__())

        @property
        def cells(self) -> typing.Dict[str, 'UITable.Cell']:
            """Return a dictionary of cells in the order of their appearance.

            Although created as a OrderedDict, return it as a Dict so that we can type hint the key, value."""
            return self._cells

        def is_selected(self) -> bool:
            return 'x-grid-row-selected' in self.cached_html.attrs['class'] or \
                           'is-selected' in self.cached_html.attrs['class']

        def select(self) -> None:
            return self.ui_element.click()

    def refresh(self) -> 'UITable':
        """Refresh the table on the page.

        Override this method for tables that can be refreshed.
        """
        return self

    def is_scrollable(self) -> bool:
        return self.IS_SCROLLABLE

    def is_pageable(self) -> bool:
        return self.IS_PAGEABLE

    @property
    def cached_table(self) -> typing.List[Row]:
        """Get a snapshot of the table in the DOM then parse it out into rows of cells.

        Returns: A list of rows where each row is a dictionary of cells indexed by the column heading in the order
            as they appear in the table. Python type: collections.OrderedDict[col_header, Cell], but Python won't let
            type hinting of the key and value of an OrderedDict.
        """
        cached_table = []  # start as an empty list

        self.wait_for_region_to_load()
        with self.frame:
            html_string = self.frameless_find_element(*self.TABLE_CONTENT).get_attribute('outerHTML')

        tree = BeautifulSoup(html_string, 'html.parser')
        headers = self.headers
        row_count = 1

        for row in tree.find_all('tr'):
            new_row = self.Row(table=self, headers=headers, row_count=row_count, html_tree=row)
            cached_table.append(new_row)
            row_count += 1
        return cached_table

    @property
    def num_rows(self) -> int:
        """Number of rows present in the table.

        Returns:
            The total number of rows in the table.
        """
        with self.frame:
            table_content = self.frameless_find_element(*self.TABLE_CONTENT)
            if not table_content:
                return 0
            row_elems = table_content.find_elements(by.By.CSS_SELECTOR, 'tr')
        if not row_elems:
            return 0
        return len(row_elems)

    def get_rows(self, key: str = None, value: str = None, regex: str = None) -> typing.List[Row]:
        """Gets the rows which match the key, value pair, if supplied.

        Args:
            key: The table header name for a column.
            value: The value present in the cell.
            regex: A regular expression that can be used to locate text in a cell.

        Raises:
            ValueError: If the caller uses both @value and @regex

        Returns:
            A list of all matching rows.
        """
        if value and regex:
            raise ValueError('Please use either an exact value or a regex. Not both.')

        rows = self.cached_table

        if key:
            try:
                if value:
                    rows = [row for row in rows if row.cells[key].text == value]
                elif regex:
                    pattern = re.compile(regex)
                    rows = [row for row in rows if pattern.match(row.cells[key].text)]
            except KeyError:
                rows = []
        return rows

    def get_rows_not_containing(self, key: str, value: str = None, regex: str = None) -> typing.List[Row]:
        """Gets the rows which do not match the key, value pair, if supplied.

        Args:
            key: The table header name for a column.
            value: The value present in the cell.
            regex: A regular expression that can be used to locate text in a cell.

        Raises:
            ValueError: If the caller uses both @value and @regex

        Returns:
            A list of all matching rows.
        """
        if value and regex:
            raise ValueError('Please use either an exact value or a regex. Not both.')

        rows = self.cached_table

        if key:
            try:
                if value:
                    rows = [row for row in rows if row.cells[key].text != value]
                elif regex:
                    pattern = re.compile(regex)
                    rows = [row for row in rows if not pattern.match(row.cells[key].text)]
            except KeyError:
                rows = []
        return rows
