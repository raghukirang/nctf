"""A helper to assist with Ember Table validation"""

import typing

from .. import selenium_deps
from ..by import By
from ..elements import UIScrollbar
from ..elements import UISpinnerElement
from ..exceptions import ElementNotViewableError
from ..table import UITable


class EmberTable(UITable):
    """A suite of tools to assist with Table validation for tables written in Ember.

    Notes:
        - Since checkboxes and the GeoView headers have no unique text
        associated with them we're using hard-coded keys of "Checkbox"
        and "GeoView" respectively.

    Examples:
        # This xpath works if you're on the NETCLOUD ENGINE page :)
        container_xpath = "//table"
        table = ArcherTable(session, container_xpath, By.XPATH)

        # Get all rows
        all_rows = table.get_rows()

        # Get all rows with a specific IPv4 Address
        ip_rows = table.get_rows('IPv4 Address', '10.1.1.1')

        # Get all rows with a Host Name regex
        all_aer_rows = table.get_rows('Host Name', regex='AER.*')
    """
    SCROLL_CONTENT = (By.CLASS_NAME, 'tse-scroll-content')
    SCROLL_HANDLE = (By.CSS_SELECTOR, 'div.drag-handle')
    IS_SCROLLABLE = True
    """The SCROLL_CONTENT and SCROLL_HANDLE define the DOM elements necessary to control scrolling. 
    
    The EmberTable, by default implements its own scrolling. If the inheriting table does not implement scrolling, None out 
    these two properties and set IS_SCROLLABLE to False.
    """
    TABLE_HEADER = (By.CSS_SELECTOR, "thead>tr>th")
    """The strategy, locator for where the header is located."""

    @property
    def scroll_bar(self) -> typing.Union[UIScrollbar, None]:
        if self.SCROLL_CONTENT and self.SCROLL_HANDLE:
            self._scrollbar = UIScrollbar(
                parent=self, strategy=self.SCROLL_HANDLE[0], locator=self.SCROLL_HANDLE[1], scroll_content=self.SCROLL_CONTENT)
        return self._scrollbar

    def scroll_to_top(self):
        """Scroll the first row into view."""
        if self.scroll_bar:
            ct = self.cached_table
            ct[0].ui_element.scroll_to()

    def scroll_to_end_of_data(self):
        """Causes the scrolling region to scroll until there is no more pages to show.

        Warning: Use with caution since this could end up being a long running command.
        """
        if self.scroll_bar:
            self.scroll_bar.scroll_to_end_of_data()

    def page_down(self):
        """Causes the scrolling region to scroll down one page, the scrollHeight of the scroll content."""
        if self.scroll_bar:
            self.scroll_bar.scroll_to_top()
            self.scroll_bar.page_down()

    @property
    def spinner(self):
        return UISpinnerElement(self, strategy=By.CLASS_NAME, locator="cp-loading")

    @property
    def headers(self) -> UITable.Headers:
        """Updates the headers associated with the table.

        Unfortunately, there are a few unidentifiable column headers, making this code complicated and brittle.
        """
        hdr_elems = self.wait_for_region_to_load().find_elements(*self.TABLE_HEADER)
        headers = UITable.Headers()
        column_count = 1
        with self.frame:
            for hdr in hdr_elems:
                # make sure all columns visible otherwise getting the text won't work
                if not hdr.is_displayed():
                    raise ElementNotViewableError('Column {} header not viewable. Resize the browser or scroll into view.'
                                                  .format(column_count), self)
                if hdr.text:
                    headers[hdr.text] = UITable.Header(hdr.text, self, element=hdr)
                elif column_count == 1:
                    headers['Checkbox'] = UITable.Header('Checkbox', self, element=hdr)
                elif column_count == 2:
                    try:
                        if hdr.find_element_by_class_name('fa-cp-heartbeat'):
                            headers['Network Status'] = UITable.Header('Network Status', self, element=hdr)
                        else:
                            hn = 'Unknown Col{}'.format(column_count)
                            headers[hn] = UITable.Header(hn, self, element=hdr)
                    except selenium_deps.NoSuchElementException:
                        # if this table is on NCP Networks page, completely unmarked col 2 must be the Default column
                        if 'OverlayPage' in self.page.__str__():
                            headers['Default'] = UITable.Header('Default', self, element=hdr)
                        else:
                            hn = 'Unknown Col{}'.format(column_count)
                            headers[hn] = UITable.Header(hn, self, element=hdr)
                else:
                    hn = 'Unknown Col{}'.format(column_count)
                    headers[hn] = UITable.Header(hn, self, element=hdr)
                column_count += 1
        self.scroll_to()
        return headers

    class Cell(UITable.Cell):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        @property
        def text(self) -> str:
            text = self.cached_html.text.strip()
            if len(text) == 0:
                input_tag = self.cached_html.find('input')
                if input_tag:
                    text = input_tag.attrs['type']
                    if len(text) == 0:
                        text = self.header
                else:
                    if self.cached_html.find(attrs={'class', 'user-admin-icon'}):
                        text = 'User Administrator'
                    elif self.cached_html.find(attrs={'class', 'collaborator-icon'}):
                        text = 'Collaborator'
                    elif self.cached_html.find(attrs={'class', 'root-admin-icon'}):
                        text = 'System Administrator'
                    elif self.cached_html.find(attrs={'class', 'user-icon'}):
                        text = 'User'
                    elif self.cached_html.find('i', {'class', 'table-icon', 'title', 'User'}):
                        text = 'User'
                    else:
                        text = self.header
            return text
