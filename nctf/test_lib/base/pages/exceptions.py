import typing

from .selenium_deps import NoSuchElementException
from .selenium_deps import StaleElementReferenceException

StaleElementReferenceException = StaleElementReferenceException
NoSuchElementException = NoSuchElementException


class UIException(Exception):
    """Base class for exceptions for POM."""

    def __init__(self, message: str, element: typing.Any):
        self.message = message
        self.el = element

    def __str__(self) -> str:
        return "{}: {}".format(self.message, self.el)


class ActionNotPossibleError(UIException):
    """I can't do that."""


class FrameElementError(UIException):
    """There was an exception thrown while trying to find the frame element."""


class ElementNotViewableError(UIException):
    """The element was expected to be in view but wasn't."""


class TableHeaderElementError(UIException):
    """There was an exception thrown while trying to find the frame element."""


class CannotOpenBrowserToUndefinedURLError(UIException):
    """Raised when trying to open the browser to a empty URL string."""


class InfiniteSpinnersError(UIException):
    """Raised when an unending series of spinners is detected"""
