from nctf.test_lib.base.pages import selenium_deps


class By(object):
    """Define POM version of By to get other search strategies beyond selenium.
    """
    ID = selenium_deps.By.ID
    NAME = selenium_deps.By.NAME
    CLASS_NAME = selenium_deps.By.CLASS_NAME
    CSS_SELECTOR = selenium_deps.By.CSS_SELECTOR
    XPATH = selenium_deps.By.XPATH
    TAG_NAME = selenium_deps.By.TAG_NAME
    LINK_TEXT = selenium_deps.By.LINK_TEXT
    PARTIAL_LINK_TEXT = selenium_deps.By.PARTIAL_LINK_TEXT
    EXTJS_ID = 'extjs id'