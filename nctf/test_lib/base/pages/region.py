# This implementation leverages concepts from PyPOM like the notion of regions and pages and how the root element of a
# region is specified with a ROOT_LOCATOR.
#
# See PyPOM for more details: https://github.com/mozilla/PyPOM

import logging
import typing

from . import base
from . import exceptions
from . import frame
from . import selenium_deps
from . import utils

logger = logging.getLogger(utils.make_short_qualified_name(__name__))


class UIRegion(base.UIViewBase, base.UIElementBase):
    """A representation of a chunk of the UI
    
    This class represents a region of a page anchored by a web element.  It provides a nice name for logging purposes
    that includes parentage hierarchy, ensures the existence of the WebElement that defines this region and allows the
    region to be in a frame.  It allows for regions and elements to be described relative to this region when using 
    XPATH locators.

    Do:
        - Subclass this object when a chunk of the UI is async.
        - Subclass this object when a chunk of the UI is logically separable.
        - Subclass this object when a chunk of the UI is common and sharable.
        - Subclass this object when a chunk of the UI is a frame/iframe but not a page
        - Subclass this object when JourneyWebElement just isn't enough and you need to express more complexity.
            e.g. A selector with a clickable field with a series of list items.
        - Use as a container to contain other JourneyRegions and JourneyWebElements
        - wait_for_region_to_load should wait on JourneyWebElements that are visible to all users, and not used in
            negative tests.
            
    Details: 
        Search for "NetCloud UI / Base UI - The Journey Page Object Model" in Confluence for more details.

    """

    # The ROOT_LOCATOR identifies the interesting dom element that serves as an anchor for the region.  When a region
    # is defining a frame/iframe, the ROOT_LOCATOR identifies the element that represents the interesting outer-most
    # region within the frame/iframe and cannot be specified By.XPATH relative to its parent since its parent would
    # be outside the frame.  Note that the parent must always be passed as a parameter on construction to maintain
    # the page hierarchy.
    ROOT_LOCATOR = None

    def __init__(self,
                 parent: base.UIViewBase,
                 name: str = None,
                 strategy: str = None,
                 locator: str = None,
                 root_element=None,
                 timeout: float = None,
                 frame: frame.UIFrameParam = None):
        """The Journey Page Object Model notion of a region on a web page.
         
        This adds a nice name and makes sure the root element of the region exists before anyone interacts with it.  By default
        the region will support scrolling actions.


        Args:
            parent: the JourneyPage or JourneyRegion that this region is contained on
            name: optional nice name to give the region - helpful if the region is dynamic
            strategy: if the region ROOT_LOCATOR is not statically defined, caller can pass in a strategy and locator 
                or the actual root WebElement of the region
            locator: if the region ROOT_LOCATOR is not statically defined, caller can pass in a locator and strategy
                or the actual root WebElement of the region
            root_element: create the region with an already existing WebElement using this passed in element
            timeout: timeout override for this region
            frame: if this region is contained in a frame this parameter allows the caller to provide a frame
                locator,strategy used to find the frame WebElement or the caller can provide the actual UIFrame.
                Optionally FRAME_LOCATOR can be provided statically in the region class definition.
        Note:
            By default when the region is constructed with a strategy, locator or via ROOT_LOCATOR, we will wait for 
            the region's root element specified to be present.  If your region needs other conditions to be met, 
            override this method and implement an explicit wait for a condition that evaluates to True when the region
            has finished loading. Any overridden wait_for_region_to_load() will be called by the Region initializer.

        Raises:
            RuntimeError: Indicates either a parent was not provided on construction or enough information to establish
                the root element of the region.
        """

        if parent is None:
            raise RuntimeError('Parent is None. A parent must always be providing when constructing a region.')
        driver = parent.driver

        if root_element is None:
            if strategy is None or locator is None:
                if self.ROOT_LOCATOR is None:
                    raise RuntimeError('either root element or (strategy and location) must be non-None or the '
                                       'region must have a static locator ROOT_LOCATOR defined.')
                else:
                    strategy = self.ROOT_LOCATOR[0]
                    locator = self.ROOT_LOCATOR[1]

        super().__init__(
            driver=driver,
            name=name,
            parent=parent,
            strategy=strategy,
            locator=locator,
            element=root_element,
            timeout=timeout,
            frame=frame)

        # if this region defines a frame then all searching for elements contained within it needs to be expressed as
        # elements located on a page because there is no element higher in the DOM - i.e. it IS /html in the DOM.
        if not self.defines_frame:
            self.frameless_find_element = self._frameless_find_element_within
            self.frameless_find_elements = self._frameless_find_elements_within

        # get the anchoring element for the region by waiting for it to be present and wait for the region to load
        element = self.wait_for_present(timeout=self.timeout)
        self.wait_for_region_to_load()

        logger.debug("{} created region. Element={}".format(self, element))

    #######################
    #
    # Define methods the fundamental methods to do the finding for elements relative to the the ROOT_LOCATOR anchoring
    # element of this region.  The base find_element(s) calls these frameless versions.
    #
    def _frameless_find_element_within(self, strategy: str, locator: str) -> selenium_deps.WebElement:
        """Finds an element on the region relative to the region root element without using frames."""
        return self._element_.find_element(strategy, locator)

    def _frameless_find_elements_within(self, strategy: str, locator: str) -> typing.List[selenium_deps.WebElement]:
        """Finds elements on the region relative to the region root element without using frames."""
        return self._element_.find_elements(strategy, locator)

    @property
    def _element(self) -> typing.Optional[selenium_deps.WebElement]:
        """Returns: the WebElement that anchors the region.

        We override the UIElement get_element() for the case where the region defines a frame, in which case use the
        base class find_element() which is a driver.find_element() since the parent of this region is in another frame.
        We call to the base class to keep the implementation of find_element in one spot.

        Returns: the WebElement that anchors the region.
        """
        if self._element_ is not None:
            return self._element_
        if self.defines_frame:
            # if the region defines a frame
            logger.debug('{} retrieving WebElement for region that defines a frame.'.format(self))
            self._element_ = super().find_element(self.strategy, self.locator)
            return self._element_
        else:
            return super()._element

    def wait_for_region_to_load(self) -> 'UIRegion':
        """Wait for the page region to load.

        Override :py:func:`wait_for_region_to_load` and implement an explicit wait for a condition that evaluates to 
        True when the region has finished loading.

        Return: 
            The current page region object to allow for chaining of calls.
        """
        return self

    #######################
    #
    # Define methods for scrolling the region
    #
    def scroll_by_pixel(self, number_of_pixels: int) -> 'UIRegion':
        with self.frame:
            js_scroll = "window.scrollBy(0,{})".format(number_of_pixels)
            self.parent.driver.execute_script(js_scroll)
        return self

    def page_down(self) -> None:
        """Causes the scrolling region to scroll down one page, the scrollHeight of the scroll content."""
        with self.frame:
            self.parent.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")

    def scroll_to_top(self) -> 'UIRegion':
        self.scroll_to()
        return self

    def scroll_to_bottom(self) -> 'UIRegion':
        with self.frame:
            self.parent.driver.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        return self
