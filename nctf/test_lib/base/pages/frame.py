import logging
import typing

from . import base
from . import exceptions
from . import selenium_deps
from . import utils

logger = logging.getLogger(utils.make_short_qualified_name(__name__))


class UIFrameBase(object):
    """A class to contain the implementation of "frameness" of a UIViewBase.

    Every ui thing has a UIFrame property.  This class defines a default content frame.
    """

    def __init__(self, name: str):
        """Initialize a frame object that determines what if any frame that the page or region is on.

        Args:
            ui_view: the page or region that this frame is being created for.
            frame_locator: optional locator of the HTML element that defines the frame.
        """
        self.name = name
        self.frame_stack = []
        self.is_default_content = True
        self.ui_view = None
        self.frame_locator = None

    def __str__(self):
        return "<{}{}>".format(self.__class__.__name__, self.name)

    def __enter__(self):
        pass

    def __exit__(self, type, value, traceback):
        pass


THE_DEFAULT_CONTENT_FRAME = UIFrameBase(name='Default Content')


class UIFrame(UIFrameBase):
    """A class to contain the implementation of "frameness" of a UIViewBase.

    Every ui thing has a UIFrame property.  This class defines a frame which holds the stack of frames that needs to be 
    switched into prior to doing anything meaningful with the ui thing.  A UIFrame is only constructed via UIViewBase 
    (page or region) when it defines a frame.  UIFrame provides __enter__ and __exit__ methods to allow it to be used 
    in a with statement to automatically switch into the correct frame and upon exit, switch back to the default frame.

    Args:
        ui_view: the page or region that this frame is being created for.
        frame_locator: optional Tuple(strategy:str, locator:str) of the HTML element that defines the frame.

    Raises:
        FrameElementError: indicates difficulty in finding the WebElement that defines this <frame> or <iframe>.
    """

    def __init__(self, ui_view: 'base.UIViewBase', frame_locator: (str, str)):
        """Initialize a frame object that determines what if any frame that the page or region is on.

        Args:
            ui_view: the page or region that this frame is being created for.
            frame_locator: optional locator of the HTML element that defines the frame.
        """
        super().__init__(ui_view.__str__())
        self.is_default_content = False
        self.ui_view = ui_view
        self.frame_locator = frame_locator
        parent = ui_view.parent
        if parent and parent.frame and not parent.frame.is_default_content:
            self.frame_stack = list(parent.frame.frame_stack)
        self.frame_stack.append(self)

    def __enter__(self):
        for frame in self.frame_stack:
            logger.debug("{} switch to frame.".format(frame))
            self.ui_view.driver.switch_to.frame(frame._frame_element)

    def __exit__(self, type, value, traceback):
        logger.debug("{} switch to default content.".format(self))
        self.ui_view.driver.switch_to.default_content()

    def _frame_element_is_present(self) -> bool:
        """Returns True if the web element is in the DOM i.e. does not raise StaleElementReference or ElementNotFound
        when being accessed, otherwise it returns False."""
        try:
            self._frame_element_ = self.ui_view.parent.frameless_find_element(*self.frame_locator)
            self._frame_element_.is_enabled()  # call anything, just interested in the exceptions
            return True
        except selenium_deps.NoSuchElementException:
            return False

    @property
    def _frame_element(self) -> selenium_deps.WebElement:
        """Returns the element on the page or region that defines the frame.  

        Assumes that the caller has already gotten into the parent frame before causing this construction
        """
        self._frame_element_ = typing.cast(selenium_deps.WebElement, None)
        try:
            self.ui_view.wait.for_call(self._frame_element_is_present).to_be(True)
        except Exception as e:
            raise exceptions.FrameElementError("Could not find element locating this frame", self) from e
        return self._frame_element_


UIFrameParam = typing.Union[UIFrame, typing.Tuple[str, str]]
