import logging
import re
import time
import typing

from nctf.test_lib.utils import waiter

from . import base
from . import by
from . import exceptions
from . import frame as fr
from . import page
from . import region
from . import selenium_deps
from . import utils

logger = logging.getLogger(utils.make_short_qualified_name(__name__))
T_P = typing.TypeVar('UIPageType', bound=page.UIPage)
T_R = typing.TypeVar('UIRegionType', bound=region.UIRegion)


class UIElement(base.UIElementBase):
    """The basic web element type in the Journey Page Object Model.
    
    Use this type when no other Journey UIElements suffice.
    """
    pass


class UICheckboxElement(base.UIElementBase):
    def is_checked(self):
        raise NotImplementedError()


class ExtCheckboxElement(base.UIElementBase):
    """ A type for an element that behaves like a checkbox whose state can be checked via an ext query

    Notes:
        There's no click implementation as clicking this element may not do anything. May be most useful to implement
        as part of a button region
    """

    def is_checked(self) -> bool:
        """ Checks if the element is checked

        Returns: The result of the JavaScript query
        """
        return self.parent.ext_finder.execute_js_query("Ext.ComponentQuery.query('#{0}')[0].checked".format(self.locator))


class UIPageLinkElement(base.UIElementBase, typing.Generic[T_P]):
    """Convenience element for links which trigger dynamic page changes.

    Most useful for navigation which controls a primary content region versus
    reloading an entirely new page, e.g. 200-level tab navigation.

    Overrides the :func:`~pages.UIElementBase.click` method to return
    a UIPage representation, the class of which is passed on construction of
    this link. Clicking on this link will call the page's `wait_for_loaded`
    method.

    Args:
         name: A human readable name for the element.
         parent: The page or region the element resides in. Only the properties and methods from the UIViewBase are \
         not used.
         page_cls: The class that defines the page to be constructed upon click being called.
         frame: The frame of page that the constructed page is in. None means the default frame.
         strategy: Strategy to use when using the locator. e.g. By.ID, By.XPATH. Either an element or both strategy \
         and locator must be provided.
         locator: The WebElement locator used in conjunction with the strategy. Either an element or both strategy and \
         locator must be provided.
         element: The WebElement in cases where it already has been realized. Either an element or both strategy and \
         locator must be provided.
    Notes:
        This should only be used for "internal" links (within the same base
        URL/service). The returned page instance is created with base_url of
        the base this link resides on.

    Examples:
        When defining a dynamic link, you should either fully-qualify
        the type-hint or not specify one so that PyCharm can correctly
        determine the correct page.::

            @property
            def some_link(self) -> base_ui.UIPageLinkElement[SomeDynamicUIPage]:
                return pages.UIPageLinkElement("Some link", ... , SomeDynamicUIPage)

        Alternatively, PyCharm and mypy will still be aware of the type
        without any explicit type hints.::

            @property
            def some_link(self):
                return pages.UIPageLinkElement("Some link", ... , SomeDynamicUIPage)
    """

    def __init__(self,
                 name: str,
                 parent: base.UIViewBase,
                 page_cls: typing.Type[T_P],
                 frame: fr.UIFrame = None,
                 strategy: str = None,
                 locator: str = None,
                 element: selenium_deps.WebElement = None,
                 opens_new_window=False,
                 **page_url_kwargs):
        super().__init__(name, parent, strategy, locator, element)
        self._page_cls = page_cls
        self._frame = frame
        self._opens_new_window = opens_new_window
        self._page_url_kwargs = page_url_kwargs

    def click(self) -> T_P:
        """Clicks the link and waits for the next page to load.

        Returns:
            The page that is dynamically loaded upon clicking this link (specified at construction).

        Raises:
            waiter.WaitTimeoutError: If there is a failure to load the next page.
        """
        logger.debug("Clicking %s to load dynamic page %s.%s", self, '.'.join(self._page_cls.__module__.split('.')[-2:]),
                     self._page_cls.__name__)
        super().click()
        if self._opens_new_window:
            self.parent.page.save_window()
            self.parent.driver.switch_to_window(self.parent.driver.window_handles[len(self.parent.driver.window_handles) - 1])

        parent = None if self._frame is None \
            else None if self._frame.is_default_content \
            else None if self._page_cls == type(self._frame.ui_view.parent) \
            else self._frame.ui_view.parent

        return self._page_cls(
            driver=self.parent.driver, parent=parent, frame=self._frame, **self._page_url_kwargs).wait_for_page_to_load()


class UIRegionLinkElement(base.UIElementBase, typing.Generic[T_R]):
    """Convenience element for links which trigger loading of dynamic regions.

    Most useful for buttons which trigger modal dialogues, dropdown menus, etc.

    Overrides the :func:`~elements.UIDynamicRegionLink.click` method to
    return a UIRegion representation, the type of which is passed on
    construction of this link. Clicking on this link will call the region's
    :func:`~pages.UIRegion.wait_for_region_loaded` method.

    Args:
         friendly_name: A human readable name for the element.
         parent: The page or region the element resides in. Only the properties and methods from the UIViewBase are \
         used.
         region_cls: The class that defines the region to be constructed upon click being called.
         strategy: Strategy to use when using the locator. e.g. By.ID, By.XPATH. Either an element or both strategy \
         and locator must be provided.
         locator: The WebElement locator used in conjunction with the strategy. Either an element or both strategy and \
         locator must be provided.
         element: The WebElement in cases where it already has been realized. Either an element or both strategy and \
         locator must be provided.
         region_name: An optional name to be used for the region that is created upon click of this element.  If not \
         provided, the class name of the region will be used.
         region_parent: An optional parent to be used for the region that is created upon click of this element. If \
         not provided, the page that this element belongs on will be used as the parent of the created region.
         region_strategy: Optional strategy to use to locate the region root element. e.g. By.ID, By.XPATH.
         region_locator: Optional locator to use to locate the region root element. Both the region_locator and \
         region_strategy must be provided if the ROOT_LOCATOR is not defined by the region class.

    Examples:
        When defining a dynamic region trigger, you should either fully-qualify
        the type-hint or not specify one so that PyCharm can correctly
        determine the correct page.::

            @property
            def some_link(self) -> base_ui.UIDynamicRegionLink[SomePopupRegion]:
                return base_ui.UIDynamicPageLink("Some link", ... , SomePopupRegion)

        Alternatively, PyCharm and mypy will still be aware of the type
        without any explicit type hints.::

            @property
            def some_link(self):
                return base_ui.UIDynamicRegionLink("Some link", ... , SomePopupRegion)
    """

    def __init__(self,
                 name: str,
                 parent: base.UIViewBase,
                 region_cls: typing.Type[T_R],
                 strategy: str = None,
                 locator: str = None,
                 element: selenium_deps.WebElement = None,
                 region_name: str = None,
                 region_parent: base.UIViewBase = None,
                 region_strategy: str = None,
                 region_locator: str = None):

        super().__init__(name, parent, strategy, locator, element)
        self._region_cls = region_cls
        self._region_name = region_name
        self._region_parent = region_parent
        self._region_strategy = region_strategy
        self._region_locator = region_locator

    def click(self) -> T_R:
        """Clicks the link, returns the linked UIRegion object.

        Returns:
            The page that is loaded upon clicking this link (specified at construction).

        Raises:
            waiter.WaitTimeoutError: If there is a failure to load the next page.
        """
        logger.debug("Clicking %s to load region %s", self, self._region_cls)
        super().click()
        if self._region_parent is not None:
            parent = self._region_parent
        else:
            parent = self.parent.page
        return self._region_cls(
            name=self._region_name, parent=parent, strategy=self._region_strategy, locator=self._region_locator)


class UIButtonElement(base.UIElementBase):
    def click(self) -> 'UIButtonElement':
        """Performs a click on the element and returns self so that calls can be chained."""
        super().click()
        return self

    def send_keys(self, value: str) -> None:
        raise exceptions.ActionNotPossibleError("Sending key clicks to a button is not recommended since its behavior "
                                                "is not consistent among browsers. If you must, use "
                                                "button._element.sendkeys()", self)


class UIReadOnlyElement(base.UIElementBase):
    def __init__(self,
                 name: str,
                 parent: base.UIViewBase,
                 strategy: str = None,
                 locator: str = None,
                 element: selenium_deps.WebElement = None,
                 regex_sub: (str, str) = None,
                 **kwargs):
        """Selenium WebElement Wrapper.

        Wraps a single Selenium WebElement to provide more robust methods, logging, and exception handling.

        Args:
            name: A human readable name for the element.
            parent: The page or region the element resides in. Only the properties and methods from the UIViewBase are
            used.
            strategy: Strategy to use when using the locator. e.g. By.ID, By.XPATH
            locator: The WebElement locator used in conjunction with the strategy.
            element: The WebElement in cases where it already has been realized.
            regex_sub: The (pattern, replace) tuple of strings that will be provided to get_text() to process the text.
            **kwargs: pass through parameters
        """
        self.regex_sub = regex_sub
        super().__init__(name=name, parent=parent, strategy=strategy, locator=locator, element=element, **kwargs)

    def get_text(self) -> str:
        """Get the text contained in the DOM under the element and applies the regex_sub given on instantiation.

        Eats ActionNotPossibleError exceptions and returns ''.
        """
        try:
            if self.regex_sub:
                pattern = self.regex_sub[0]
                replace = self.regex_sub[1]
                return re.sub(pattern, replace, super().get_text())
            else:
                return super().get_text()
        except exceptions.ActionNotPossibleError:
            return ''


#TODO: fix this to meet the requirements
class UIElementCollection(list):
    """This class is just an experiment. Unsure if it adds value."""

    def __init__(self, elements: typing.List[UIElement] = None):
        super().__init__()
        self.elements = elements if elements else []

    def __iter__(self):
        for element in self.elements:
            yield element

    def all_displayed(self):
        if len(self.elements) == 0:
            raise Exception("No elements")  # TODO: More informative exception.
        for element in self.elements:
            if element.is_displayed():
                pass
            else:
                return False
        return True

    def all_clickable(self):
        if len(self.elements) == 0:
            raise Exception("No elements")  # TODO: More informative exception.
        for element in self.elements:
            if element.is_clickable():
                pass
            else:
                return False
        return True


class UISpinnerElement(base.UIElementBase):
    def __init__(self, parent: base.UIViewBase, strategy: str = None, locator: str = None):
        """The Spinner on a page with methods to check for existence and wait for them to disappear.
        
        Args:
            parent: The page or region the element resides in.
            strategy: Strategy to use when using the locator. e.g. By.ID, By.XPATH.
            locator: The WebElement locator used in conjunction with the strategy.
        """
        super().__init__(name='Spinner', parent=parent, strategy=strategy, locator=locator)

    def wait_for_spinner(self, timeout: typing.Optional[float] = None) -> None:
        """Waits until the supplied amount of time for the spinner web element to be visible.
        
        Notes:
            This method relies on checking that the spinner is visible versus using DOM presence since some spinners
            are purposely in the DOM, but hidden and reused if needed.
        
        Args:
            timeout: A timeout to wait in seconds. If not specified the default page timeout will be used.
        """
        timeout = timeout if timeout else self.parent.timeout
        self._wait_for_spinner_helper(20, timeout)

    def _wait_for_spinner_helper(self, calls_left: int, timeout: typing.Optional[float] = None) -> None:
        """Recursive helper for wait_for_spinner.

        Calls itself recursively until no spinners are found, or calls_left reaches 0.
        Will always run at least once.

        Args:
            calls_left: How many times to attempt this function, if spinners are found.
            timeout: The time to wait for each spinner to disappear.

        Raises:
            waiter.WaitTimeoutError: If a spinner was present for longer than the timeout.
            exceptions.InfiniteSpinnersError: If oscillating spinners are detected (ie. visible <=> not visible loop).
        """

        # Have to clear the saved element in order to re-find potentially new spinners (instead of using a stale one)
        self._element_ = None
        try:
            self.wait_for_displayed(timeout * 0.05)
        except (waiter.WaitTimeoutError, selenium_deps.StaleElementReferenceException):
            try:
                self.wait_for_displayed(timeout * 0.005)
            except (waiter.WaitTimeoutError, selenium_deps.StaleElementReferenceException):
                # logger.info("Never saw spinner for:{} strategy={} locator={}".format(self, self.strategy, self.locator))
                return
            # logger.info("SAW SECOND attempt to see spinner for:{} strategy={} locator={}".format(
            #     self, self.strategy, self.locator))
            return
        # logger.info("SAW FIRST attempt to see spinner for:{} strategy={} locator={}".format(self, self.strategy, self.locator))

        try:
            self.wait_until_not_displayed(timeout)
        except waiter.WaitTimeoutError:
            logger.error("Reached unlikely timeout while waiting for spinners to disappear.")
            raise

        if calls_left <= 0:
            raise exceptions.InfiniteSpinnersError("Oscillating spinners detected! (reached recursion limit)", self)

        if calls_left == 1:
            logger.warning("wait_for_spinner on last attempt!")
        self._wait_for_spinner_helper(calls_left - 1, timeout)


class UIScrollbar(base.UIElementBase):

    js_get_elements_by = {
        by.By.CLASS_NAME: 'getElementsByClassName',
        by.By.ID: 'getElementsById',
        by.By.TAG_NAME: 'getElementsByTagName'
    }

    def __init__(self,
                 parent: base.UIViewBase,
                 strategy: str = by.By.CSS_SELECTOR,
                 locator: str = "div.drag-handle",
                 scroll_content: typing.Tuple[str, str] = (by.By.CLASS_NAME, 'tse-scroll-content')):
        """The Scrollbar on a page that implements scrollbars not relying on the browser.

        This UIElement supports scroll_to_top(), page_down(), and scroll_to_end_of_data()  So far, only ember implemenations
        make use of this, but if it is needed for other implementations, provide the overriding strategy, locator, and
        scroll_content.

        Args:
            parent: The page or region the element resides in.
            strategy: Strategy to use for the locator. e.g. By.ID, By.XPATH. Default is ember, CSS_SELECTOR.
            locator: The WebElement locator used in conjunction with the strategy. Default is ember, 'div.drag-handle'.
            scroll_content: The DOM location of the region that will scroll.  Default is ember strategy and locator,
                (by.By.CLASS_NAME, 'tse-scroll-content').
        """
        if scroll_content:
            if scroll_content[0] not in UIScrollbar.js_get_elements_by:
                raise ValueError('parameter error: scroll_content strategy="{}" is not supported. '
                                 'Only {} strategies are allowed.'.format(scroll_content[0], ', '.join(
                                     '"{}"'.format(key) for (key, val) in UIScrollbar.js_get_elements_by.items())))

            self.sc_js_strategy = UIScrollbar.js_get_elements_by[scroll_content[0]]
            self.sc_locator = scroll_content[1]
        else:
            raise ValueError('parameter error: scroll_content needs to be provided')

        super().__init__(name='Scrollbar', parent=parent, strategy=strategy, locator=locator)

    def is_scrollable(self) -> bool:
        """If there is no scrollbar drag handle or its height is 0, then the scrollbar is not scrollable.

        This method will get into the frame that the UIScrollbar element belongs to."""
        try:
            # If there is no scrollbar drag handle or its height is 0, then return False. get_attribute() gets into frame.
            if "height: 0px" in self.get_attribute("style"):
                return False
        except (selenium_deps.NoSuchElementException, exceptions.ActionNotPossibleError):
            return False
        return True

    def scroll_to_top(self) -> None:
        """Scroll the element locating the scroll content into view."""
        if not self.is_scrollable():
            return
        with self.frame:
            js_top = "var elem = document.{}('{}'); " \
                           "elem[0].scrollTop=0; ".format(self.sc_js_strategy, self.sc_locator)
            self.parent.driver.execute_script(js_top)

    def page_down(self) -> None:
        """Causes the scrolling region to scroll down one page, the scrollHeight of the scroll content."""
        if not self.is_scrollable():
            return
        with self.frame:
            js_page_down = "var elem = document.{}('{}'); " \
                           "elem[0].scrollTop=elem[0].scrollHeight; ".format(self.sc_js_strategy, self.sc_locator)
            self.parent.driver.execute_script(js_page_down)

    def scroll_to_end_of_data(self) -> None:
        """Causes the scrolling region to scroll until there is no more pages to show.

        Warning: Use with caution since this could end up being a long running command.
        """
        if not self.is_scrollable():
            return
        with self.frame:
            js_get_scroll_height = "return document.{}('{}')[0].scrollHeight;".\
                format(self.sc_js_strategy, self.sc_locator)
            prev_ht = self.parent.driver.execute_script(js_get_scroll_height)
            cur_ht = 0
            while cur_ht != prev_ht:
                prev_ht = cur_ht
                self.page_down()
                time.sleep(0.5)
                cur_ht = self.parent.driver.execute_script(js_get_scroll_height)
                pass
            pass
