"""A base class implementation for UI Tables."""

import abc
import collections
import re
import typing

from attrdict import AttrDict
from bs4 import BeautifulSoup

from . import base
from . import by
from . import elements
from . import frame
from . import selenium_deps
from .table import extjs_table
from .table import table


def tabbing(n: int) -> str:
    return "".join("{}".format("\t") for i in range(n))


class ExtJSTreePicker(extjs_table.ExtJSTable):
    """A suite of tools to assist with table validation in Journey.

    Args:
        selector: The selector for a web element which contains both the ECM table and the headers for said table.
        method: The method for the corresponding selector
    """
    IS_SCROLLABLE = False
    SCROLL_CONTENT = None
    SCROLL_HANDLE = None
    IS_PAGEABLE = False
    PAGING_TOOLBAR = None
    """ExtJSTreePicker does not implement its own scrolling or paging, so IS_SCROLLABLE and IS_PAGEABLE are both False and
    the associated elements are set to None.  It relies on the browsers implementation of scrolling.  Inheriting types should 
    implement and override these as appropriate."""

    class Row(extjs_table.UITable.Row):
        def __str__(self):
            return "{}{}".format(tabbing(self.level), super().__str__())

        @property
        def level(self) -> int:
            return len(next(iter(self.cells.values())).cached_html.find_all('img')) - 2

        def is_expanded(self) -> bool:
            if self.is_tree():
                with self.ui_element.parent.frame:
                    return 'x-grid-tree-node-expanded' in self.ui_element.get_attribute('class')
            else:
                return False

        def expand(self) -> 'ExtJSTreePicker':
            if not self.is_expanded():
                with self.ui_element.parent.frame:
                    self.ui_element._element.find_element_by_class_name('x-tree-expander').click()
            return self.ui_element.parent

        def collapse(self) -> 'ExtJSTreePicker':
            if self.is_expanded():
                with self.ui_element.parent.frame:
                    self.ui_element._element.find_element_by_class_name('x-tree-expander').click()
            return self.ui_element.parent

        def is_leaf(self) -> bool:
            return 'x-grid-tree-node-leaf' in self.cached_html.attrs['class']

        def is_tree(self) -> bool:
            return not self.is_leaf()

    class Tree(typing.Dict[str, 'ExtJSTreePicker.Tree']):
        def __init__(self, name: str, row: 'ExtJSTreePicker.Row', parent: 'ExtJSTreePicker.Tree' = None):
            super().__init__()
            self.leaves: typing.List[ExtJSTreePicker.Row] = []
            self.row = row
            self.parent = parent
            self.name = name

        def __str__(self):
            tabs = tabbing(self.row.level)
            return "{}Tree:{}\n{}leaves:{}\n{}trees:\n{}".\
                format(tabs, self.name, tabs, self.leaves, tabs,
                       "{}".format("\n".join("{}".format(tree) for tree_name, tree in self.items())))
            # return "{}Tree:{}\n\t{}leaves:{}\n\t{}trees:\n{}".\
            #     format(tabs, self.name, tabs, self.leaves, tabs,
            #            "{}".format("\n".join("{}".format(tree) for tree_name, tree in self.items())))

        def __repr__(self):
            return self.__str__()

    @property
    def cached_tree(self) -> Tree:
        """Get a snapshot of the table in the DOM then parse it out into a tree.

        Returns: A tree where each leaf is a row, a dictionary of cells indexed by the column heading in the order
            as they appear in the table and where the row is a tree, a tree class is made to hold the row and its children.
        """
        cached_tree = {}  # start as an empty dictionary
        current_tree = cached_tree
        self.wait_for_region_to_load()
        with self.frame:
            html_string = self.frameless_find_element(*self.TABLE_CONTENT).get_attribute('outerHTML')

        tree = BeautifulSoup(html_string, 'html.parser')
        headers = self.headers
        row_count = 1

        for tr in tree.find_all('tr'):
            new_row = self.Row(table=self, headers=headers, row_count=row_count, html_tree=tr)
            if new_row.is_leaf():
                current_tree.leaves.append(new_row)
            else:
                # The new row is a tree
                tree_name = new_row.cells[next(iter(headers))].text
                if row_count == 1:
                    current_tree = ExtJSTreePicker.Tree(tree_name, new_row)
                    cached_tree = current_tree
                else:
                    # The new row is not the first row and hence not the head of the cached_tree
                    if new_row.level > current_tree.row.level:
                        # The new tree is contained in the current tree
                        current_tree[tree_name] = ExtJSTreePicker.Tree(tree_name, new_row, current_tree)
                    else:
                        # The new tree is a sibling to (the current tree or one of its parents)
                        while new_row.level <= current_tree.row.level:
                            current_tree = current_tree.parent
                        current_tree[tree_name] = ExtJSTreePicker.Tree(tree_name, new_row, current_tree)
                    # After setting up the new tree, increment to point to it
                    current_tree = current_tree[tree_name]
            row_count += 1
        return cached_tree

    def find_first_collasped_row(self) -> typing.Union[Row, None]:
        for row in self.cached_table:
            if row.is_tree():
                if not row.is_expanded():
                    return row
        return None

    def are_all_rows_expanded(self) -> bool:
        expanded = True
        for row in self.cached_table:
            expanded &= row.is_expanded()
            if not expanded:
                return False
        return expanded

    def expand_all(self) -> 'ExtJSTreePicker':
        unexpanded_row = self.find_first_collasped_row()
        while (unexpanded_row):
            unexpanded_row.expand()
            unexpanded_row = self.find_first_collasped_row()
        return self
