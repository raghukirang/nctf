import logging
import typing
from urllib.parse import urljoin

import parse

from nctf.libs.common_library import WaitTimeoutError
from . import base
from . import exceptions
from . import frame
from . import utils
from . import web_driver

logger = logging.getLogger(utils.make_short_qualified_name(__name__))


class UIPage(base.UIViewBase):
    """A representation of a UI Page that is reachable via URL - use this as a base class for those pages.

    Do:

        * Subclass when this page is reachable via a URL.
        * Use as a container to contain JourneyRegions and some stray JourneyWebElements
        * Any JourneyWebElements defined in the Page should be available on DOM ready.  If not, consider wrapping them
          in a JourneyRegion.

    Example Usage::

        from nctf.test_lib.base.pages.journey_page import JourneyPage
        from selenium.webdriver import Firefox

        class LoginPage(UIPage):
            URL_TEMPLATE = 'ecm.html'
            ...

        driver = Firefox()
        page = LoginPage(driver, base_url="https://qa2.cradlepointecm.com").open()

    Args:
        driver: a JourneyWebDriver (which is a WebDriver at heart with helpers
            bundled. Either driver or parent *MUST* be specified.
        name: An optional nice name to describe this page in logging.
        base_url: Base URL. Either base_url or URL_TEMPLATE must be specified.
        parent: if this page is in a frame, parent should be the region or page containing the frame/iframe and the
            FRAME_LOCATOR must be defined by the inheriting class or a frame provided on construction.
        timeout: Timeout used for explicit waits. Defaults to DEFAULT_TIMEOUT
        frame: if this page is contained in a frame this parameter allows the caller to provide a frame
            locator,strategy used to find the frame WebElement or the caller can provide the actual UIFrame.
            Optionally FRAME_LOCATOR can be provided statically in the page class definition.
        **url_kwargs: Keyword arguments used when formatting the `page_url`
            using the URL_TEMPLATE as a template calling for an argument.

    Details:
        Search for "NetCloud UI / Base UI - The Journey Page Object Model" in Confluence for more details.
    """

    URL_TEMPLATE = None
    """The URL_TEMPLATE is a string that when combined with the base_url and initializer values becomes the page_url.

    The URL_TEMPLATE is appended to the base_url and can contain names of keyword arguments passed on construction of 
    the page.

    Example Usage::

        # absolute URL
        URL_TEMPLATE = 'https://ui-overlay-qa2.cradlepointecm.com/#/home'  
        
        # relative to base URL that might be https://qa2.cradlepointecm.com
        URL_TEMPLATE = '/ecm.html#devices/routers'
        
        # keyword argument expansion where the construction parameter list contains tenant_id=id which is used to
        # construct the full URL
        URL_TEMPLATE = '/#/home?tenantId={tenant_id}'
        
        # with an initializer where you know the tenant_id:
        page = PageType(driver=driver, base_url='https://ui-overlay-qa2.cradlepointecm.com', 
                        tenant_id=current_tenant_id)
                                
        # with an initializer where you do NOT know the tenant_id:
        page = PageType(driver=driver, base_url='https://ui-overlay-qa2.cradlepointecm.com', 
                        tenant_id='{tenant_id}')
                        
        # you can then get the tenant_id by doing:
        tenant_id = parse.parse(normalize_url(page.page_url), normalize_url(page.driver.current_url))['tenant_id']
    """

    WEB_SERVICE = None
    """The WEB_SERVICE is a string name of the service that this page should be served up from.
    
    The URL generated from the WEB_SERVICE is used in place of the base_url if it is not provided.  The intended use
    of this is for each service to make a base page class that all pages served up by that service inherits from.  
    That base page class will define the name of the service by setting WEB_SERVICE to the appropriate service that 
    corresponds to the same name used in the test run config yaml file, which is the ultimate source for determining
    where the pages are served up from.
    """

    DEFAULT_TIMEOUT = base.THE_DEFAULT_TIMEOUT
    """The DEFAULT_TIMEOUT for all pages is defined in the base as THE_DEFAULT_TIMEOUT.
    
    Page authors can define a different timeout by redefining DEFAULT_TIMEOUT in their page definition or by providing
    a different timeout on construction of the page.  All regions contained on this page will get this timeout unless
    they themselves have overridden the DEAFULT_TIMEOUT or defined a timeout upon construction.  All elements use the
    timeout defined by their parent by default.
    """

    DEFAULT_POLL_INTERVAL = base.THE_DEFAULT_POLL_INTERVAL
    """The default POLL_INTERVAL for all pages is defined in the base as THE_POLL_INTERVAL
    
    Page authors can define a different polling interval by redefining DEFAULT_POLL_INTERVAL in their page definition 
    or by providing.  All regions contained on this page will get this poll interval unless they themselves have
    overridden the DEAFULT_POLL_INTERVAL.  All elements use the poll interval defined by their parent.
    """

    def __init__(self,
                 driver: web_driver.UIWebDriver = None,
                 name: str = None,
                 base_url: str = None,
                 parent: base.UIViewBase = None,
                 timeout: float = None,
                 frame: frame.UIFrameParam = None,
                 **url_kwargs):
        if driver is None:
            if parent is None:
                raise ValueError('parameter error: either a driver or parent must be provided upon construction')
            else:
                driver = parent.driver
        if parent and self.FRAME_LOCATOR is None and frame is None:
            raise ValueError('parameter error: by specifying a parent, the implication is that this page is being '
                             'contained in a frame/iframe and therefore FRAME_LOCATOR needs to be specified in the '
                             'derived page class or frame needs to be passed on construction.')

        super().__init__(driver=driver, name=name, parent=parent, timeout=timeout, frame=frame, **url_kwargs)

        if self.WEB_SERVICE:
            web_service_url = driver.services_info.get_url(self.WEB_SERVICE)
        else:
            web_service_url = None
        self.base_url = base_url or web_service_url
        self.url_kwargs = url_kwargs
        self.window_handle = None
        self.save_window()
        logger.debug("{} created page.".format(self))

    @property
    def page_url(self) -> str:
        """Returns the fully qualified URL suitable for a browser to use to navigate to the page that this
        page object represents.

        The URL will be fully expressed if you need a normalized URL, i.e. to match what a modern web browser
        would show. For example, default ports (eg. 443 for HTTPS) stripped from the result, use utils.normalize:

        url = util.normalize(page.page_url)

        Raises:
            KeyError: If there are missing parameters for the URL_TEMPLATE.
        """
        try:
            if self.URL_TEMPLATE is not None:
                return urljoin(self.base_url, self.URL_TEMPLATE.format(**self.url_kwargs))
            return self.base_url
        except KeyError as e:
            raise KeyError("Insufficient parameters to form a complete URL {}: {}".format(self, self.url_kwargs)) from e

    def open(self, **url_kwargs) -> 'UIPage':
        """Open the page and wait for it to be loaded by calling wait_for_page_to_load().

        If the URL_TEMPLATE for this class is parametrized, `url_kwargs`
        *must* be specified completely by a combination of those specified on
        construction and those specified here. The `url_kwargs` specified here
        override the constructor ones.

        Raises:
            KeyError: If there are missing parameters for the URL_TEMPLATE.
            CannotOpenBrowserToUndefinedURL: If no base_url or URL_TEMPLATE is set.

        Returns:
            'self' to allow for chaining of methods.
        """
        self.url_kwargs.update(url_kwargs)
        if self.page_url:
            self.driver.get(self.page_url)
            self.wait_for_page_to_load()
            return self
        else:
            raise exceptions.CannotOpenBrowserToUndefinedURLError('Set a base URL or URL_TEMPLATE to open page.', self)

    def wait_for_page_to_load(self) -> 'UIPage':
        """Waits for this page to load.

        By default, this calls :func:`~page.UIPage.wait_for_on_this`. POM
        writers are encouraged to override this method to provide more accurate
        behaviour for dynamic content.

        Note that this is *NOT* called during the initialization of the page.
        Only calls to :func:`~page.UIPage.open` will directly trigger this method.

        Raises:
            KeyError: If there are missing parameters for the URL_TEMPLATE.

        Returns:
            'self' to allow for chaining of methods.
        """
        self.wait_for_on_this()
        return self

    def is_on_this(self) -> bool:
        """Whether or not the normalized browser URL matches this page's normalized URL template.

        Returns:
            returns True if the normalized browser URL parse.parse matches the normalized page URL template, otherwise
            False is returned.
        """
        # if this page is contained in a frame at least ask if the parent.page is on this
        if self.defines_frame:
            return self.parent.page.is_on_this()
        template = self.base_url if self.URL_TEMPLATE is None else urljoin(self.base_url, self.URL_TEMPLATE)
        return parse.parse(utils.normalize_url(template), utils.normalize_url(self.driver.current_url)) is not None

    def wait_for_on_this(self) -> 'UIPage':
        """This method will wait for the browser to achieve the URL specified by the page.

        It waits the default page timeout amount of seconds.

        Returns:
            'self' to allow for chaining of methods.
        """
        logger.debug('{} waiting up to {} seconds for the browsers current url to match {}'.format(
            self, self.timeout, utils.normalize_url(self.page_url)))
        try:
            self.wait.for_call(self.is_on_this).to_be(True)
        except WaitTimeoutError as e:
            raise WaitTimeoutError("Wait for on this timeout {}={} browser page={}".format(
                self, utils.normalize_url(self.page_url), self.driver.current_url)) from e
        return self

    def save_window(self) -> 'UIPage':
        """ Associates the current driver window with this page.

         Associates the driver/browser current window with the page to make it easier for a test to jump between
         windows.  The driver window is associated/saved to the page object upon the page creation.  Use this method
         if you need finer granularity or to make sure that a page is associated with the current window.

         Returns:
             Returns the current page for chaining
        """
        self.window_handle = self.driver.current_window_handle
        return self

    def restore_window(self) -> 'UIPage':
        """ Resets the current driver window to be the one associated with this page.

         This method allows a test to force the driver to jump to the window that is associated with the page.  This
         makes it easier for a test to jump between windows.

         Returns:
             Returns the current page for chaining
        """
        if self.window_handle:
            self.driver.switch_to_window(self.window_handle)
        return self.wait_for_page_to_load()
