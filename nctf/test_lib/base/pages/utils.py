from urllib.parse import urlsplit
from urllib.parse import urlunsplit


def make_short_qualified_name(module_name: str) -> str:
    """

    Args:
        module_name: string representing a fully qualified name of a class 

    Returns:
        the name with the first few package levels stripped off

    """
    # strip off the first two levels of the hierarchy
    return module_name.split('.', 2)[-1]


def normalize_url(url: str) -> str:
    """Returns the given URL in a normalized form.

    The URL will be normalized to match what a modern web browser would show. For example,
    default ports (eg. 443 for HTTPS) will be stripped from the result.

    Args:
        url: The URL to normalize.

    Returns:
        The provided URL, normalized.
    """
    default_ports = [('https', 443), ('http', 80)]
    parsed = urlsplit(url)
    if (parsed.scheme, parsed.port) in default_ports:
        return urlunsplit((parsed.scheme, parsed.hostname, parsed.path or '/', parsed.query, parsed.fragment))
    else:
        return urlunsplit((parsed.scheme, parsed.netloc, parsed.path or '/', parsed.query, parsed.fragment))
