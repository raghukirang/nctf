# Local
import logging
import sys
import time
from typing import Any

# Pip
from selenium.common.exceptions import WebDriverException

logger = logging.getLogger('selenium.ext')


class ExtFinder(object):

    def __init__(self, driver, timeout):
        self.driver = driver
        self.TIMEOUT = timeout

    def execute_js_query(self, js_query: str, timeout: int=None) -> Any:
        """Executes a given JavaScript query via Selenium WebDriver.

        Note:
            Polls for a valid response until the timeout is reached.

        Args:
            js_query: The JavaScript query to be executed.
            timeout: The time in seconds to wait for the query to succeed.

        Returns:
            The result of the JavaScript query.

        """
        wait_time = timeout if timeout is not None and isinstance(timeout, int) else self.TIMEOUT
        query = "{} {}".format('return', js_query)
        result = None
        start_time = time.time()
        while result is None and (time.time() - start_time) < wait_time:
            try:
                result = self.driver.execute_script(query)
            except WebDriverException:
                time.sleep(1)
                pass
            except:
                logger.error("Unexpected error: {}".format(sys.exc_info()[0]))
                raise

        assert result is not None, "The JavaScript query failed after {} seconds:\n\t{}".format(wait_time, js_query)

        return result

    def locate_element_by_extjs(self, extjs_query, timeout=None) -> str:
        """Locates an element on the page by the given Ext JS query. Polls for the
        element until the timeout is reached.
        Assumption: the result of the Ext JS query is an element id

        Args:
            extjs_query (str): the full Ext JS query
            timeout (int): the amount of time in seconds to wait for the element to appear on the page

        Returns:
            The found id

        """
        element_id = self.execute_js_query(extjs_query, timeout)
        return element_id

    def locate_simple_button_by_extjs_id(self, button_id, timeout=None) -> str:
        """Packages the simple string into a JS ComponentQuery, executes the query and
        waits for the located element to become clickable.

        Args:
            button_id (str): the Ext JS itemId of the element to find
            timeout (int): the amount of time in seconds to wait for the element to appear on the page

        Returns:
            The found id

        """
        query = "Ext.ComponentQuery.query('#{}')[0].id".format(button_id)
        return self.locate_element_by_extjs(query, timeout)
