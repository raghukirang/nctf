import functools

from . import exceptions


def raise_anpe_on_error(action: str):
    """If a WebDriver Exception is raised, re-raises as an ActionNotPossibleError.

    Args:
        action: For the log message, the action which was being performed.
    """

    def decorator(method):
        @functools.wraps(method)
        def wrapper(self, *args, **kwargs):
            try:
                return method(self, *args, **kwargs)
            except Exception as e:
                raise exceptions.ActionNotPossibleError('Encountered exception "{}" while trying to {}'.format(
                    type(e).__name__, action), self) from e

        return wrapper

    return decorator
