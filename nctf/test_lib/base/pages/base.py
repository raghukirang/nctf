import contextlib
import logging
import typing

from nctf.test_lib.base.pages.ext import ExtFinder
import nctf.test_lib.utils.waiter as waiter

from . import exceptions
from . import frame as fr
from . import selenium_deps
from . import utils
from . import web_driver
from .by import By
from .decorators import raise_anpe_on_error

logger = logging.getLogger(utils.make_short_qualified_name(__name__))

THE_DEFAULT_TIMEOUT = 10.0
"""The default timeout for everything is 10 seconds."""
THE_DEFAULT_POLL_INTERVAL = 0.1
"""The default polling interval for everything is 0.1 seconds."""


class UIBase(object):
    def __init__(self, name: str = None, parent: 'UIViewBase' = None, **kwargs):
        self.name = name
        self.parent = parent

        # By default assume this ui thing is in its parent's frame. If the ui thing ends up defining a frame, then the
        # UIViewBase construction will override this definition since it is concerned with frame construction.
        self.frame = fr.THE_DEFAULT_CONTENT_FRAME if parent is None else parent.frame

    def __str__(self) -> str:
        """Return the whole parentage with optional nice names"""
        if self.name is None:
            if self.parent is None:
                result = '<{}>'.format(self.__class__.__name__)
            else:
                result = '{}<{}>'.format(self.parent, self.__class__.__name__)
        else:
            if self.parent is None:
                result = "<{}(name='{}')>".format(self.__class__.__name__, self.name)
            else:
                result = "{}<{}(name='{}')>".format(self.parent, self.__class__.__name__, self.name)
        return result

    def __repr__(self):
        return self.__str__()


class UIViewBase(UIBase):
    """A base class for regions and pages.
    
    This relies on UIBase and sets up the driver, the page this UIViewBase is on, timeout, construct a default wait and 
    a UIFrame.  It provides the find methods. A UIViewBase can have another UIViewBase as a parent giving the ability of 
    pages and regions to have children.
    """

    FRAME_LOCATOR = None
    """The FRAME_LOCATOR is a tuple of strings (strategy, locator) used to locate the frame/iframe Web Element.
    
    When a region is defining a frame/iframe, the FRAME_LOCATOR locates the frame and the ROOT_LOCATOR identifies the 
    element that represents the interesting outer-most region within the frame/iframe. In this way the region can serve
    as the root element for locating containing elements but cannot itself be expressed relative to its parent since 
    its parent would be outside the frame.  This does not apply to pages since they do not have an expressed DOM 
    element anchoring them (by implication it is /html).
    """

    DEFAULT_TIMEOUT = None
    DEFAULT_POLL_INTERVAL = None
    """ The default timeout and default polling interval to use when waiting on a page or region wait() call."""

    def __init__(self,
                 driver: web_driver.UIWebDriver,
                 name: str = None,
                 parent: 'UIViewBase' = None,
                 timeout: float = None,
                 frame: 'fr.UIFrameParam' = None,
                 **kwargs):
        """
        
        Args:
            driver: a UIWebDriver needed to get to driver functionality
            name: A human readable name for the UIView, if you name your region and page classes descriptively, this is
            unneeded.
            parent: The page or region the element resides in. Only the properties and methods from the UIViewBase are 
            used.  This may be None for pages.
            timeout: An optional override of the DEFAULT_TIMEOUT
            frame: The locator,strategy used to find a frame WebElement or and the actual UIFrame.
            **kwargs: pass through parameters
        """
        super().__init__(name=name, parent=parent, **kwargs)

        self.driver = driver
        if parent is None:
            self.page = self
            parent_timeout = THE_DEFAULT_TIMEOUT
            parent_poll_interval = THE_DEFAULT_POLL_INTERVAL
        else:
            self.page = parent.page
            parent_timeout = parent.timeout
            parent_poll_interval = parent.poll_interval

        # use the passed in timeout, or a DEFAULT_TIMEOUT defined by the page or region, or the timeout of the parent
        # which could ultimately be THE_DEFAULT_TIMEOUT if there is no parent
        self.timeout = (timeout or self.DEFAULT_TIMEOUT) or parent_timeout

        # use the DEFAULT_POLL_INTERVAL defined by the page or region or use the parent poll interval
        # which could ultimately be the THE_DEFAULT_POLL_INTERVAL
        self.poll_interval = self.DEFAULT_POLL_INTERVAL or parent_poll_interval

        self.wait = waiter.Waiter(self.timeout, self.poll_interval)

        self.ext_finder = ExtFinder(self.driver, self.timeout)

        # Set up frame property - do this only after setting up driver, timeout, and wait!
        # If a frame as a UIFrame was provided then we use what is given and mark this UIView as being the defining
        # point for a frame if the given frame was NOT the default content frame.  If this UIView has FRAME_LOCATOR
        # defined or frame was passed in as locator tuple, construct a new UIFrame using this locator.
        self.defines_frame = False
        if frame:
            if isinstance(frame, fr.UIFrameBase):
                self.frame = frame
                if not frame.is_default_content:
                    self.defines_frame = True
            else:
                self.frame = fr.UIFrame(self, frame)
                self.defines_frame = True
        else:
            if self.FRAME_LOCATOR:
                self.frame = fr.UIFrame(self, self.FRAME_LOCATOR)
                self.defines_frame = True

    #############################
    #
    # Define find methods with the frameless versions being the fundamental definitions. Regions will override the
    # frameless finds to be relative finds.
    #

    def frameless_find_element(self, strategy: str, locator: str) -> selenium_deps.WebElement:
        """Finds an element without getting into a frame."""
        return self.driver.find_element(strategy, locator)

    def find_element(self, strategy: str, locator: str) -> selenium_deps.WebElement:
        """Finds an element on the view, i.e. page or region.

        Args:
            strategy: Location strategy to use.
            locator: Location of target element.
            
        Returns:
            WebElement matching the strategy, locator.
        """
        with self.frame:
            if strategy == By.EXTJS_ID:
                strategy = By.ID
                locator = self.ext_finder.locate_simple_button_by_extjs_id(locator)
            element = self.frameless_find_element(strategy, locator)
        return element

    def frameless_find_elements(self, strategy: str, locator: str) -> typing.List[selenium_deps.WebElement]:
        """Find elements without getting into a frame."""
        return self.driver.find_elements(strategy, locator)

    def find_elements(self, strategy: str, locator: str) -> typing.List[selenium_deps.WebElement]:
        """Finds elements on the view, i.e. page or region.

        Args:
            strategy: Location strategy to use.
            locator: Location of target elements.
        
        Returns:
            A List of WebElements matching the strategy, locator.
        """
        with self.frame:
            if strategy == By.EXTJS_ID:
                strategy = By.ID
                locator = self.ext_finder.locate_simple_button_by_extjs_id(locator)
            elements = self.frameless_find_elements(strategy, locator)
        return elements


class UIElementBase(UIBase):
    """The base type for all web element types in the Journey Page Object Model.

    Please use a derived type in the elements.py file instead of this base type.

    All NCTF web element types, UIElement derive from this class.  It provides an overlay of Selenium WebElement
    providing some ease of use and debugging/triage standardization.  It handles getting into and out of frames where
    the element is defined on a page or region that defines or is contained within a frame.  Derivations provide
    semantic naming for ease of debug and triage as well as any helper methods to perform the specialized function of
    the element.
    """

    def __init__(self,
                 name: str,
                 parent: UIViewBase,
                 strategy: str = None,
                 locator: str = None,
                 element: selenium_deps.WebElement = None,
                 **kwargs):
        """Selenium WebElement Wrapper.

        Wraps a single Selenium WebElement to provide more robust methods, logging, and exception handling.

        Args:
            name: A human readable name for the element.
            parent: The page or region the element resides in. Only the properties and methods from the UIViewBase are
            used.
            strategy: Strategy to use when using the locator. e.g. By.ID, By.XPATH
            locator: The WebElement locator used in conjunction with the strategy.
            element: The WebElement in cases where it already has been realized.
            **kwargs: pass through parameters

        Note:
            If creating this UIElement via a passed in (strategy, locator), there will be no actual construction or waiting
            on the WebElement to be created or present by default. This lazy initialization allows us to define UIElements
            but not go through initialization until the WebElement is actually needed by a test.

        """
        # only the name and parent must be provided and they need to be passed onto the UIBase constructor
        if parent is None:
            raise RuntimeError('Parent is None. A parent must always be providing when constructing a UIElement.')
        super().__init__(name=name, parent=parent, **kwargs)
        self.strategy = strategy
        self.locator = locator
        self._element_ = element

        if element is None:
            if strategy is None or locator is None:
                raise RuntimeError(
                    'parameter error: both strategy and location must be defined or an WebElement be passed in.')

    @property
    def _element(self) -> typing.Optional[selenium_deps.WebElement]:
        """ Returns: a cached WebElement or finds the element from its locator.  Gets into the correct frame first."""
        if self._element_ is not None:
            return self._element_
        # always find via the parent.find_element because it will be set up for relative or absolute find
        self._element_ = self.parent.find_element(self.strategy, self.locator)
        return self._element_

    def _debug_get_raw_web_element(self) -> selenium_deps.WebElement:
        """Test method to get at the raw web element without any processing happening"""
        return self._element_

    def _debug_reset_raw_web_element(self) -> None:
        """Test method to reset the raw web element to None to force the re-finding of the web element."""
        self._element_ = None

    @property
    def _selector(self) -> typing.Tuple[str, str]:
        """Helper for returning a Selenium-API-friendly locator tuple."""
        return self.strategy, self.locator

    ##############################
    # is methods
    ##############################
    def is_enabled(self) -> bool:
        """Returns True or False depending on if the element is enabled. """
        try:
            ref_element = self._element
            with self.frame:
                return ref_element.is_enabled()
        except selenium_deps.NoSuchElementException:
            return False

    def is_displayed(self) -> bool:
        """Returns True or False depending on if the element is being displayed. """
        try:
            ref_element = self._element
            with self.frame:
                return ref_element.is_displayed()
        except selenium_deps.NoSuchElementException:
            return False

    def is_present(self) -> bool:
        """Returns True if the web element is in the DOM i.e. does not raise StaleElementReference or ElementNotFound
        when being accessed, otherwise it returns False."""
        try:
            ref_element = self._element
            with self.frame:
                ref_element.is_enabled()  # call anything, just interested in the exceptions
            return True
        except selenium_deps.NoSuchElementException:
            return False

    def is_clickable(self) -> bool:
        """Returns True or False depending on if the element is clickable. """
        try:
            ref_element = self._element
            with self.frame:
                return ref_element.is_displayed() and ref_element.is_enabled()
        except selenium_deps.NoSuchElementException:
            return False

    def is_selected(self) -> bool:
        """Returns: True or False depending on if the element is clickable."""
        try:
            ref_element = self._element
            with self.frame:
                return ref_element.is_selected()
        except selenium_deps.NoSuchElementException:
            return False

    ##############################
    # wait methods
    ##############################
    def wait_specific_timeout(self, timeout: typing.Optional[float] = None) -> waiter.Waiter:
        """Provides a way to wait on a timeout other than the default page timeout

        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.
        """
        if timeout and (timeout != self.parent.timeout):
            # Factory up a wait with the given timeout since we want more fine grained control of timeout.
            logger.debug('{} waiting for up to {} seconds'.format(self, timeout))
            return waiter.wait(timeout, self.parent.poll_interval)
        else:
            logger.debug('{} waiting for up to {} seconds'.format(self, self.parent.timeout))
            return self.parent.wait

    def wait_for_clickable(self, timeout: typing.Optional[float] = None) -> 'UIElementBase':
        """Waits until the supplied amount of time for the underlying web element to be clickable

        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.

        Returns:
            Itself is returned so that this method may be used in chaining.
        """
        logger.debug('{} waiting until clickable'.format(self))
        self.wait_specific_timeout(timeout).for_call(self.is_clickable).to_be(True)
        return self

    def wait_for_present(self, timeout: typing.Optional[float] = None) -> 'UIElementBase':
        """Waits until the supplied amount of time for the underlying web element to be present

        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.

        Returns:
            Itself is returned so that this method may be used in chaining.
        """
        logger.debug('{} waiting until present'.format(self))
        self.wait_specific_timeout(timeout).for_call(self.is_present).to_be(True)
        return self

    def wait_for_displayed(self, timeout: typing.Optional[float] = None) -> 'UIElementBase':
        """Waits until the supplied amount of time for the underlying web element to be visible

        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.

        Returns:
            Itself is returned so that this method may be used in chaining.
        """
        logger.debug('{} waiting until displayed'.format(self))
        self.wait_specific_timeout(timeout).for_call(self.is_displayed).to_be(True)
        return self

    def wait_until_not_displayed(self, timeout: typing.Optional[float] = None) -> None:
        """Waits until the element is not visible on the page

        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.

        Returns:
            Itself is returned so that this method may be used in chaining.
        """
        logger.debug('{} waiting until NOT displayed'.format(self))
        try:
            self.wait_specific_timeout(timeout).for_call(self.is_displayed).to_be(False)
        except selenium_deps.StaleElementReferenceException:
            # swallow stale element exception since it implies that the element doesn't exist
            pass

    def wait_for_loaded(self, timeout: typing.Optional[float] = None) -> 'UIElementBase':
        """Checks if this reference is present on the page.
        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.

        Returns:
            Itself is returned so that this method may be used in chaining.
        """
        return self.wait_for_present(timeout)

    def wait_for_unloaded(self, timeout: typing.Optional[float] = None) -> None:
        """Checks if this reference is present on the page.
        Args:
            timeout: Maximum wait time for the element. If not provided, the timeout uses the page default timeout.

        Returns:
            Itself is returned so that this method may be used in chaining.
        """
        try:
            self.wait_specific_timeout(timeout).for_call(self.is_present).to_be(False)
        except selenium_deps.StaleElementReferenceException:
            # swallow stale element exception since it implies that the element has unloaded,
            # which is what we are looking for
            pass

    ##############################
    # action methods
    ##############################
    @raise_anpe_on_error('click')
    def click(self) -> None:
        """Performs a click on the element. """
        element_ref = self.wait_for_clickable()._element
        with self.frame:
            logger.info('{} clicking WebElement.'.format(self))
            element_ref.click()
            #self.parent.page.driver.execute_script("arguments[0].click();", element_ref)

    @raise_anpe_on_error('clear')
    def clear(self) -> None:
        """Clears the text entry box."""
        logger.info('{} clearing WebElement.'.format(self))
        element_ref = self.wait_for_present()._element
        with self.frame:
            element_ref.clear()

    @raise_anpe_on_error('send keys')
    def send_keys(self, value: str) -> None:
        """Sends the text in value to the web element."""
        logger.info('{} sending keys "{}" to WebElement.'.format(self, value))
        element_ref = self.wait_for_displayed()._element
        with self.frame:
            element_ref.send_keys(value)

    @raise_anpe_on_error('set text')
    def set_text(self, text: str) -> None:
        """Clears the field and sends the specified text to it."""
        self.clear()
        self.send_keys(text)

    @raise_anpe_on_error('get text')
    def get_text(self) -> str:
        """Get the text contained in the DOM under the element. """
        logger.info('{} getting the text from WebElement.'.format(self))
        element_ref = self.wait_for_present()._element
        with self.frame:
            text = element_ref.text
            logger.info('{} get_text text="{}".'.format(self, text))
            return text

    @raise_anpe_on_error('get value')
    def get_value(self) -> str:
        """Get the current value of this element."""
        return self.get_attribute('value')

    @raise_anpe_on_error('get attribute')
    def get_attribute(self, attribute_name: str) -> str:
        """Get the content of a given attribute of a given element. """
        logger.debug('Getting attribute {} for element {}.'.format(attribute_name, self))
        element_ref = self.wait_for_present()._element
        with self.frame:
            return element_ref.get_attribute(name=attribute_name)

    @raise_anpe_on_error('scroll to')
    def scroll_to(self) -> None:
        """Scrolls the given element into the visible portion of the browser window and
         makes it visible to the driver. This is required for Screenshots and Needle."""
        # TODO: test this... not sure need to be in frame to execute a script
        element_ref = self._element
        with self.frame:
            self.parent.driver.execute_script("return arguments[0].scrollIntoView();", element_ref)


@contextlib.contextmanager
def wait_for_unload(elem: UIElementBase):
    """Context manager for taking in an element to ensure transitions to an unloaded state."""
    # TODO: Untested in this form
    yield
    elem.wait_for_unloaded()
