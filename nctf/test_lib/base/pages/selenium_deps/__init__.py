"""Selenium Dependencies.

These imports represent the dependencies that POM has on Selenium

Imports copied from below for documentation purposes::

    from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException, TimeoutException
    from selenium import webdriver
    from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
    from selenium.webdriver.common.by import By
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.remote.webdriver import WebDriver
    from selenium.webdriver.remote.webelement import WebElement
    from selenium.webdriver.support import expected_conditions
    from selenium.webdriver.remote.remote_connection import LOGGER as selenium_internal_logger

"""
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException, TimeoutException
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.remote.remote_connection import LOGGER as selenium_internal_logger