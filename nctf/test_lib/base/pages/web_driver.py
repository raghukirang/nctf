# This implementation leverages Selenium WebDriver implementation but supplements it with things that CP needs.
# The UIWebDriverConfig implementation is extended from Selenium DesiredCapabilities to add capabilities like 'remote'
# and configuration like default display size, etc.

import logging
import os
import random
import string
from time import gmtime
from time import strftime
import typing

import pytest

from selenium.webdriver import ActionChains

from . import selenium_deps
from .services_info import ServicesInfo

logger = logging.getLogger('base.pages.journey_web_driver')


class UIWebDriverConfig(selenium_deps.DesiredCapabilities):
    """
    Set of default supported configurations.  This inherited from selenium with a few config things that we
    need added - remote, display_width, display_height, artifacts_dir, diable_cookies,
    suppress_download_dialog, artifacts_dir and timeout.

    Use this as a starting point for creating a UIWebDriverConfig object for requesting local webdrivers or
    remote webdrivers for connecting to selenium server or selenium grid.

    Usage Example::

        from nctf/test_lib/base/pages import UIWebDriver

        # Create a desired capabilities object as a starting point.
        config = UIWebDriverConfig.FIREFOX.copy()
        config['remote'] = True
        config['selenium_grid_url'] = "http://198.0.0.1:4444/wd/hub"
        config['platform'] = "WINDOWS"
        config['version'] = "10"

        # Instantiate an instance of Remote WebDriver with the desired capabilities.
        driver = JourneyWebDriver.factory(config=config, proxy=None)

        # Instantiate an instance of a totally default web driver
        driver = JourneyWebDriver.factory(JourneyWebDriver.DesiredCapabilities.DEFAULT.copy(), proxy=None)

    Note: ALWAYS use '.copy()' on the DesiredCapabilities object to avoid the side
    effects of altering the Global class instance.

    """
    FIREFOX = selenium_deps.DesiredCapabilities.FIREFOX.copy()
    FIREFOX.update({
        "remote": False,
        "display_width": 1920,
        "display_height": 1080,
        "artifacts_dir": None,
        "disable_cookies": False,
        "suppress_download_dialog": False,
        "display_visible": True,
        "timeout": 60
    })
    CHROME = selenium_deps.DesiredCapabilities.CHROME.copy()
    CHROME.update({
        "remote": False,
        "version": "",
        "platform": "ANY",
        "display_width": 1920,
        "display_height": 1080,
        "artifacts_dir": None,
        "disable_cookies": False,
        "suppress_download_dialog": False,
        "display_visible": True,
        "timeout": 60
    })
    DEFAULT = CHROME


class UIWebDriver(selenium_deps.WebDriver):
    """An overlay of the WebDriver to add Journey extra functionality."""

    @staticmethod
    def factory(config: typing.Dict, services_info: ServicesInfo,
                proxy: selenium_deps.webdriver.Proxy = None) -> 'UIWebDriver':

        # expect to have browserName in config if "chrome" is not good enough
        browser_kind = config.get('browser_name', "chrome").lower()

        if browser_kind == 'firefox':
            logger.warning("Firefox currently has limited framework support. Use it at your own peril.")
            return UIFireFoxWebDriver(config=config, services_info=services_info, proxy=proxy)
        elif browser_kind == 'chrome':
            return UIChromeWebDriver(config=config, services_info=services_info, proxy=proxy)
        else:
            raise ValueError("Invalid local browserName value '{}'.".format(browser_kind))

    # Ignore the IDE's warning here. We rely on the specific Journey{Browser}WebDriver to initialize the underlying
    # WebDriver through its inherited webdriver.Firefox, webdriver.Chrome, etc. initialization before this init is
    # called.
    def __init__(self, config: typing.Dict, services_info: ServicesInfo):

        self.services_info = services_info
        self.last_screenshot = None
        self.artifacts_dir = config.get('artifacts_dir', None)
        # expect to have these in config if defaults are not good enough
        self.timeout = config.get('timeout', 60)
        display_width = config.get('display_width', 1920)
        display_height = config.get('display_height', 1080)

        # set up the display size
        current_window = self.get_window_size()  # https://github.com/mozilla/geckodriver/issues/643
        current_width = int(current_window['width'])
        current_height = int(current_window['height'])
        if current_width != int(display_width) or current_height != int(display_height):
            self.set_window_size(display_width, display_height)

    def screenshot(self) -> str:
        """Take a screenshot of the current state of the browser."""
        if self.artifacts_dir:

            test_name = None
            if hasattr(pytest, '_journey_current_test_func'):
                test_name = pytest._journey_current_test_func
            if test_name is None:
                test_name = 'UnknownTest'

            self.last_screenshot = strftime('%Y-%m-%d_%Hh%Mm%Ss', gmtime())
            file_ext = '.png'
            image_name = ''.join((test_name, '_', self.last_screenshot, file_ext))
            screenshot_path = os.path.join(self.artifacts_dir, image_name)
            self.save_screenshot(screenshot_path)
            return screenshot_path
        else:
            logger.critical('Unable to save screenshot of browser. artifacts_dir not defined')

    def new_tab(self, url: str = '') -> 'UIWebDriver':
        """Opens a new tab in the current session and moves the focus to that tab.

        It is recommended that this method be used in conjunction with creating a new page, e.g.

        home_page2 = sample_pages.SampleWebsiteIndexPage(driver= tabs_page.driver.new_tab(), name="2nd home page").open()

        """
        logger.debug('Opening new tab')
        try:
            script = "window.open('{}')".format(url)
            self.execute_script(script)
            self.switch_to_window(self.window_handles[len(self.window_handles) - 1])
        except Exception as e:
            screenshot_filename = self.screenshot()
            msg = str(e).format(" screenshot saved as {}", screenshot_filename)
            raise type(e)(msg) from e
        return self

    def get_window_handle(self):
        try:
            return self.current_window_handle
        except Exception as e:
            screenshot_filename = self.screenshot()
            msg = str(e).format(" screenshot saved as {}", screenshot_filename)
            raise type(e)(msg) from e

    def next_window(self) -> None:
        """Moves the focus to the next tab.  Wraps to first tab if called from last tab."""
        logger.debug('Moving focus to next tab over')
        next_windows_index = (self.window_handles.index(self.current_window_handle) + 1) % len(self.window_handles)
        self.switch_to.window(self.window_handles[next_windows_index])

    def switch_to_window(self, window_name: str) -> str:
        """Switches focus to the specified window.

        Args:
            window_name: The name or window handle of the window to switch to.
        """
        try:
            return self.switch_to.window(window_name)
        except Exception as e:
            screenshot_filename = self.screenshot()
            msg = str(e).format(" screenshot saved as {}", screenshot_filename)
            raise type(e)(msg) from e

    def switch_window(self, default_handle=None):
        """Switch to another browser window.

        If there is more than 1 window open, pass the handle of the parent
        (current) window so that it can exclude the current window and switch
        to the new window. If handle is not passed, it'll assume that there's
        only one window open and it'll switch to that one window.

        Todo:
            Maybe error handling could be done.
        """
        try:
            lst_handles = list(self.window_handles)
            if default_handle is not None:
                assert len(lst_handles) > 1
                lst_handles.remove(default_handle)
                assert len(lst_handles) > 0

            self.switch_to_window(lst_handles[0])
        except Exception as e:
            screenshot_filename = self.screenshot()
            msg = str(e).format(" screenshot saved as {}", screenshot_filename)
            raise type(e)(msg) from e

    def close_window(self) -> None:
        """Close the current browser window.

        If there is more than 1 window open, it'll close the current window.
        We'll have to switch to the required window after closing the current
        window. Just be careful while calling this when there's only one
        browser instance open.
        """
        try:
            self.close()
        except Exception as e:
            screenshot_filename = self.screenshot()
            msg = str(e).format(" screenshot saved as {}", screenshot_filename)
            raise type(e)(msg) from e

    def refresh_page(self) -> None:
        """Reloads the content in the current browser tab."""
        try:
            self.refresh()
        except Exception as e:
            screenshot_filename = self.screenshot()
            msg = str(e).format(" screenshot saved as {}", screenshot_filename)
            raise type(e)(msg) from e

    def on_page(self, element, by_type, wait):
        try:
            wait(element, by_type, screenshot_on_fail=False)
        except selenium_deps.TimeoutException:
            logger.debug("TimeoutException caught! {} element never found!".format(element))
            return False
        return True


class UIFireFoxWebDriver(UIWebDriver, selenium_deps.webdriver.Firefox):
    def __init__(self, config: typing.Dict, services_info: ServicesInfo, proxy: selenium_deps.webdriver.Proxy = None):

        # expect to have these in config if defaults are not good enough
        suppress_download_dialog = config.get('suppress_download_dialog', False)
        disable_cookies = config.get('disable_cookies', False)

        # Set up Firefox profile.
        profile = selenium_deps.webdriver.FirefoxProfile()
        profile.set_preference("xpinstall.signatures.required", False)
        profile.set_preference("browser.privatebrowsing.autostart", True)

        if "display_visible" in config and config["display_visible"] is False:
            raise NotImplementedError("Firefox does not support a headless mode.")

        if proxy:
            # Sets values that are still essential for cooperating with FF54 + Gecko.
            profile.set_proxy(proxy)

        if suppress_download_dialog:
            # Don't use default download directory
            profile.set_preference("browser.download.folderList", 2)
            # Don't show download progress
            profile.set_preference("browser.download.manager.showWhenStarting", False)
            # Don't always ask what to do with file
            profile.set_preference("browser.helperApps.alwaysAsk.force", False)

            artifacts_dir = config['artifacts_dir']
            # Set download directory
            if artifacts_dir:
                profile.set_preference("browser.download.dir", artifacts_dir)
            else:
                logger.critical('Unable to run test using downloads, artifacts directory was not specified')

            # Don't ask to download these types (the various cert files types that can
            # be downloaded from firmware and plain text reports)
            profile.set_preference("browser.helperApps.neverAsk.saveToDisk",
                                   "application/binary, application/x-pem-file, application/x-pkcs12, text/plain, text/csv")

        if disable_cookies:
            profile.set_preference("network.cookie.cookieBehavior", 2)

        try:
            selenium_deps.webdriver.Firefox.__init__(self, firefox_profile=profile, proxy=proxy)
            UIWebDriver.__init__(self, config=config, services_info=services_info)

        except Exception as e:
            logger.error('Exception {} occurred when creating the Firefox webdriver.'.format(e))
            raise e


class UIChromeWebDriver(UIWebDriver, selenium_deps.webdriver.Chrome):
    def __init__(self, config: typing.Dict, services_info: ServicesInfo, proxy: selenium_deps.webdriver.Proxy = None):

        selenium_deps.selenium_internal_logger.setLevel(logging.WARNING)
        options = selenium_deps.webdriver.ChromeOptions()

        # Modify our desired capabilities so we can capture all logs from the console.
        capabilities = config.copy()
        capabilities["loggingPrefs"] = {'browser': 'ALL'}

        profile_path = capabilities.get("browser_profile", None)
        if profile_path:
            os.makedirs(profile_path, exist_ok=True)
            options.add_argument("user-data-dir=" + profile_path)
        debug_log_filename: str = None
        service_args: list = None
        if capabilities.get("browser_debug"):
            debug_path = capabilities.get("browser_debug_path")
            if not debug_path:
                raise ValueError('Turning on debug requires browser-debug-path in config.')
            os.makedirs(debug_path, exist_ok=True)
            debug_log_filename = debug_path + "/ChromeDriver.log"
            service_args = ['--verbose']

        if "display_visible" in config and config["display_visible"] is False:
            logger.debug("Setting display to be hidden.")
            options.add_argument("--headless")
            options.add_argument("--disable-gpu")
            options.add_argument("--no-sandbox")

        if proxy:
            options.add_argument('--proxy-server={}'.format(proxy.http_proxy))

        # expect to have these in config if defaults are not good enough
        suppress_download_dialog = config.get('suppress_download_dialog', False)
        disable_cookies = config.get('disable_cookies', False)
        if disable_cookies or suppress_download_dialog:
            raise NotImplementedError("disable_cookies and suppress_download_dialog options have not been implemented.")

        try:
            selenium_deps.webdriver.Chrome.__init__(self, chrome_options=options, desired_capabilities=capabilities,
                                                    service_args=service_args, service_log_path=debug_log_filename)
            UIWebDriver.__init__(self, config=config, services_info=services_info)

        except Exception as e:
            logger.error('Exception {} occurred when creating the Chrome webdriver.'.format(e))
            raise e
        chromedriver_version = self.capabilities['chrome']['chromedriverVersion']
        chrome_version = self.capabilities['version']
        logger.info('\n***********************************************************************************************'
                    '\nChrome Version = {}\nChromedriver Vesion = {} \nChrome Profile = {}'
                    '\n***********************************************************************************************'.format(
                        chrome_version, chromedriver_version, profile_path))
