import typing

from nctf.test_lib.base import pages as JWP
from nctf.test_lib.base.fixtures.fixture import FixtureBase
from selenium import webdriver


class BaseUIFixture(FixtureBase):
    """This fixture is a web driver factory.

    This fixture hands out new web driver instances.  It holds onto an each instance that it creates so it can get
    failure reports upon test failure and cleanup after a test has completed.  This allows tests to hold onto two
    distinct web drivers (browsers) for multi-user access to services.

        Args:
            config: A selenium-like dictionary that contains a request for capabilities of and about the
            driver.  The
            services_info: An object that defines all the URLs to access the UIs for services that Journey knows about.
            proxy: an optional proxy to do things like track web traffic like browsermob-proxy etc.
    """

    def __init__(self, config: dict, services_info: JWP.ServicesInfo, proxy: typing.Union[webdriver.Proxy, None]):

        self.config = config
        self.services_info = services_info
        self._proxy_list = []
        if proxy:
            self._proxy_list.append(proxy)
        self._driver_list = []

    def get_driver(self,
                   config: dict = None,
                   services_info: JWP.ServicesInfo = None,
                   proxy: typing.Union[webdriver.Proxy, None] = None) -> typing.Union[None, JWP.UIWebDriver]:
        """This method returns a new webdriver instance.
        
        Args:
            config: optional dictionary of desired capabilities otherwise the one passed in on fixture instantiation \
            is used.
            services_info: optional services_info otherwise the one passed in on fixture instantiation is used.
            proxy: optional proxy, if None and this is the first driver instance, then the proxy passed in on \
            fixture instantiation is used.

        Returns: a new webdriver instance.

        """
        config = config or self.config
        services_info = services_info or self.services_info

        if proxy:
            self._proxy_list.append(proxy)
        else:
            if len(self._driver_list) == 0:
                proxy = None if len(self._proxy_list) == 0 else self._proxy_list[0]

        driver = JWP.UIWebDriver.factory(config, services_info, proxy)
        self._driver_list.append(driver)
        return driver

    def get_failure_report(self) -> str:
        """This method builds a failure report string from screenshots and console logs from all of the drivers."""
        report = str(self)
        report += '\n'

        if not self._driver_list:
            report += "No webdrivers initialized."
            return report

        for driver in self._driver_list:
            driver.screenshot()

            report += "Current URL: {}\n".format(driver.current_url)
            if driver.capabilities['browserName'] == 'chrome':
                report += "Browser console log:\n"
                console_logs = driver.get_log('browser')
                for log in console_logs:
                    report += "{:12s} {:13d} {:10s} {}\n".format(log["source"], log["timestamp"], log["level"], log["message"])
            report += '\n'

        return report

    def teardown(self) -> None:
        """This method quits all drivers and clears the list of drivers and proxies."""
        for driver in self._driver_list:
            driver.quit()
        self._driver_list.clear()
        self._proxy_list.clear()
