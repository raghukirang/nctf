import abc

from nctf.test_lib.base.objects import NCTFObject


class FixtureBase(NCTFObject, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def teardown(self):
        """All fixtures should be able to be torn down, removing any unnecessary state/artifacts

        Example: The ARM fixture should release all devices that were leased for tests back to ARM.

        """
        pass

    @abc.abstractmethod
    def get_failure_report(self) -> str:
        """All fixtures should be able to generate some report describing their state to simplify triage"""
        pass
