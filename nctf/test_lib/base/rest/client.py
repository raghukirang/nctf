import logging
import os
from typing import Tuple
from typing import Union

import requests
from requests import Response
from requests import Session

from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError

logger = logging.getLogger('test_lib.base.rest')


class RESTClient(object):
    """Base class for REST API clients. Glorified wrapper of the requests library.

    Args:
        protocol: The REST API protocol. 'http' or 'https'
        hostname: The REST APIs hostname. e.g. qa2.cradlepointecm.com
        port : The REST APIs port. e.g. 80 or 443
        session: requests.Session to be used for REST API.
        timeout: Number of seconds to be used as the Session's timeout.

    """

    def __init__(self, protocol: str, hostname: str, port: int, timeout: float = 30.0, session: Session = None):

        self.protocol = protocol
        self.hostname = hostname
        self.port = port
        self.session = session if session is not None else Session()
        self.timeout = timeout

        try:
            self.opid = os.environ['NCTF_CURRENT_OPID']
        except KeyError:
            self.opid = None

    def __str__(self):
        return "{}://{}:{}".format(self.protocol, self.hostname, self.port)

    @property
    def api_root(self):
        return "{}://{}:{}".format(self.protocol, self.hostname, self.port)

    """ Requests Helpers """

    def request(self, method: str, path: str, expected_status_code: Union[int, Tuple[int, ...]] = None,
                **requests_kwargs) -> Response:
        """Common request method that all requests should flow through for uniformity of exceptions and logging.

        Optionally checks the response's status code. If the status code is unexpected,
        an error is raised and the request/response details are logged.

        Other requests exceptions are caught and re-raised with additional logging.

        Args:
            method: HTTP method ('GET', 'PUT', 'POST', etc.).
            path: The resource path (eg. '/api/v1/users/' or '/api/v1/accounts/123/').
            expected_status_code: The expected HTTP status code. May be a single int or a tuple/list of ints.
            **requests_kwargs: Additional requests arguments.

        Returns:
            The response, if no exceptions were raised.

        Raises:
            UnexpectedStatusError: If the response status code != expected_status_code.

        """
        # Default timeout
        args_to_pass = {'timeout': self.timeout}
        args_to_pass.update(requests_kwargs)

        logger.debug("{} {}".format(method, path))
        if 'data' in requests_kwargs:
            logger.debug("%s data: %s", method, requests_kwargs['data'])
        if 'json' in requests_kwargs:
            logger.debug("%s data: %s", method, requests_kwargs['json'])

        try:
            r = self.session.request(method, self.api_root + path, **args_to_pass)
        except requests.exceptions.RequestException as e:
            msg = " Exception raised on '{} {}'.\n".format(method, path)
            msg += "---Request params (next line):\n\t{}\n".format(requests_kwargs if requests_kwargs else "")
            msg += "---Exception (next line):\n\t{}".format(str(e))
            msg = str(e) + msg
            exception_type = type(e)
            raise exception_type(msg) from e

        exp_status_codes = (expected_status_code,) if isinstance(expected_status_code, int) else expected_status_code
        if expected_status_code is not None and r.status_code not in exp_status_codes:
            msg = "Unexpected response from '{} {}'. ".format(method, path)
            msg += "Status code: {} ({}).\n".format(r.status_code, r.reason)
            msg += "---Request params (next line):\n\t{}\n".format(requests_kwargs)
            msg += "---Response content (next line):\n\t{}\n".format(r.content)
            raise UnexpectedStatusError(msg, r)

        return r

    def get(self, path: str, expected_status_code: int = None, **kwargs) -> Response:
        """Perform GET as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        if self.opid:
            if 'headers' not in kwargs:
                kwargs['headers'] = {}
            kwargs['headers']['X-OPID'] = self.opid
        return self.request("GET", path, expected_status_code, **kwargs)

    def put(self, path: str, expected_status_code: int = None, **kwargs) -> Response:
        """Perform PUT as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        if self.opid:
            if 'headers' not in kwargs:
                kwargs['headers'] = {}
            kwargs['headers']['X-OPID'] = self.opid
        return self.request("PUT", path, expected_status_code, **kwargs)

    def post(self, path: str, expected_status_code: int = None, **kwargs) -> Response:
        """Perform POST as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        if self.opid:
            if 'headers' not in kwargs:
                kwargs['headers'] = {}
            kwargs['headers']['X-OPID'] = self.opid
        return self.request("POST", path, expected_status_code, **kwargs)

    def delete(self, path: str, expected_status_code: int = None, **kwargs) -> Response:
        """Perform DELETE as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        if self.opid:
            if 'headers' not in kwargs:
                kwargs['headers'] = {}
            kwargs['headers']['X-OPID'] = self.opid
        return self.request("DELETE", path, expected_status_code, **kwargs)

    def patch(self, path: str, expected_status_code: int = None, **kwargs) -> Response:
        """Perform PATCH as the `method` for :func:`RESTAPIInterface.request`, using the assigned session."""
        if self.opid:
            if 'headers' not in kwargs:
                kwargs['headers'] = {}
            kwargs['headers']['X-OPID'] = self.opid
        return self.request("PATCH", path, expected_status_code, **kwargs)

    def authenticate(self, *args, **kwargs):
        """Authenticate with the REST API

        Should be implemented if a REST API requires authentication.

        """
        raise NotImplementedError()
