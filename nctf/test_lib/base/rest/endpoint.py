import functools
import logging
from typing import Callable
from typing import Tuple
from typing import Union

from requests import Response

from .client import RESTClient

logger = logging.getLogger('test_lib.base.rest')


def checked_status(expected_status_codes: Union[int, Tuple[int, ...]]):
    """Inserts a status check into the request iff the interface's  ._checked member is truthy.

    If the caller provides an expected status, the caller-specified status will override this one.
    """

    def decorator(responder: Callable[..., Union[Response]]):
        @functools.wraps(responder)
        def func_wrapper(self, *args, **kwargs):
            if self.status_checking and 'expected_status_code' not in kwargs:
                kwargs['expected_status_code'] = expected_status_codes
            return responder(self, *args, **kwargs)

        return func_wrapper

    return decorator


class RESTEndpoint(object):
    def __init__(self, client: RESTClient, api_path: str, status_checking: bool = True,
                 enforce_trailing_slash: bool = False):
        """Base endpoint class. Establishes python mappings to the basic functionality of a REST API endpoint.

        Args:
            client: The REST API Client we'll be using to perform requests.
            api_path: The URL path segment. Will be concatenated by the RESTAPIClient to create a full URL.
            status_checking: Enables/disables the default status code checks via the checked_status decorator.
            enforce_trailing_slash: If the endpoint of the REST API always requires a trailing slash then set this to True.

        """
        self.client = client

        if not api_path.startswith('/'):
            api_path = '/' + api_path

        if not api_path.endswith('/') and enforce_trailing_slash:
            api_path = api_path + '/'

        self.api_path = api_path
        self.status_checking = status_checking
        self.trailing_slash = enforce_trailing_slash

    def _format_path(self, resource_id: str = None) -> str:
        if resource_id:
            api_path = self.api_path
            if not api_path.endswith('/'):
                api_path = api_path + '/'
            api_path = api_path + resource_id
            if self.trailing_slash:
                api_path = api_path + '/'
            return api_path
        else:
            api_path = self.api_path
            return api_path

    def _request(self, method: str, resource_id: str = None, *args, **kwargs) -> Response:
        path = self._format_path(resource_id)
        return self.client.request(method, path, *args, **kwargs)

    def _get(self, resource_id: str = None, *args, **kwargs) -> Response:
        path = self._format_path(resource_id)
        logger.debug("Performing GET on path: {}".format(path))
        return self.client.get(path, *args, **kwargs)

    def _post(self, resource_id: str = None, *args, **kwargs) -> Response:
        path = self._format_path(resource_id)
        logger.debug("Performing POST on path: {}".format(path))
        return self.client.post(path, *args, **kwargs)

    def _put(self, resource_id: str = None, *args, **kwargs) -> Response:
        path = self._format_path(resource_id)
        logger.debug("Performing PUT on path: {}".format(path))
        return self.client.put(path, *args, **kwargs)

    def _patch(self, resource_id: str = None, *args, **kwargs) -> Response:
        path = self._format_path(resource_id)
        logger.debug("Performing PATCH on path: {}".format(path))
        return self.client.patch(path, *args, **kwargs)

    def _delete(self, resource_id: str = None, *args, **kwargs) -> Response:
        path = self._format_path(resource_id)
        logger.debug("Performing DELETE on path: {}".format(path))
        return self.client.delete(path, *args, **kwargs)
