from nctf.test_lib.base.fixtures.fixture import FixtureBase

from .client import RESTClient


class RESTFixture(FixtureBase):
    """Factory class to handle simplified construction of REST Clients"""

    def get_client(self, *args, **kwargs) -> RESTClient:
        raise NotImplemented()

    def teardown(self):
        raise NotImplemented()

    def get_failure_report(self):
        raise NotImplemented()
