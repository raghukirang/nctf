class RESTAPIException(Exception):
    """Base exception for REST API errors"""


class UnexpectedStatusError(RESTAPIException):
    """Raised when REST API calls return with an unexpected status code."""

    def __init__(self, msg="", response=None):
        self.msg = msg
        self.status_code = response.status_code if response is not None else -1
        self.response = response

    def __str__(self):
        return self.msg
