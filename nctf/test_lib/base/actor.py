import abc

import requests


class NCTFActor(metaclass=abc.ABCMeta):
    def __init__(self):
        self.session = requests.Session()

    @property
    @abc.abstractmethod
    def auth(self) -> tuple:
        pass


class NCTFActorWithCreds(NCTFActor):
    def __init__(self, username: str, password: str):
        super().__init__()
        self.username = username
        self.password = password
        self.mfa_token = ''

    @property
    def auth(self) -> tuple:
        return self.username, self.password

    def __str__(self):
        return "<{}(username='{}', password='{}')>".format(self.__class__.__name__, self.username, self.password)
