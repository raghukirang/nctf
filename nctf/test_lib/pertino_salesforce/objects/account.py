"""Pertino Salesforce Object Models

These Pertino Salesforce Object Models provide quick and easy management of different objects in Salesforce.

Warning:
    Everything in this file is a Work in Progress! Objects may only work on specific Salesforce Stacks, and some specific BSF Versions.
    This is currently out-of-date with the newest Salesforce Data Model. We are working on completely implementing these objects once
    the data model changes are tested and verified.

"""

from nctf.test_lib.salesforce.objects.object import SFObject
from nctf.test_lib.salesforce.fixture import _SalesforceConnection

import time


class PertinoSFAccount(SFObject):
    ecm_user = None
    ecm_account = None

    device = None
    contact = None
    orders = list()
    entitlements = list()
    opportunities = list()
    order_service_lines = list()

    has_been_entitled = False
    connected_with_ecm = False

    def __init__(self, sf_interface, required_params=None, sf_id=None):
        super(PertinoSFAccount, self).__init__('Account', sf_interface)

        if required_params is not None:
            self.sf_create(required_params)

        elif sf_id is not None and isinstance(sf_id, str):
            self.sf_id = sf_id
            self.sf_get()

    @classmethod
    def get_by_sf_id(cls, sf_interface, sf_id):
        """
        Retrieve an Salesforce Account with a specified Salesforce ID.

        Parameters:
            sf_interface: Salesforce Fixture
            sf_id (str): Salesforce ID of specified Salesforce Account

        Return:
            SFAccount: Instance of SFAccount with an existing Salesforce Account
        """
        return cls(sf_interface, sf_id=sf_id)

    @classmethod
    def get_by_account_name(cls, sf_interface, account_name: 'SalesForce Account Name'):
        """
        Retrieve a Salesforce Account with a specified Account Name

        Args:
            sf_interface:
            account_name:
        Returns:
            SFAccount:

        """

        account_query = "SELECT Id FROM Account WHERE Name = '{}'".format(account_name)
        sf_id = sf_interface.query(account_query)['records']

        return cls(sf_interface, sf_id=sf_id[0]['Id'])

    @classmethod
    def search_for_account_name_wildcard(cls, sf_interface: _SalesforceConnection, account_name: str) -> 'SFAccount':
        """ Retrieve a Salesforce Account with a partial Account Name

        Args:
            sf_interface: Salesforce Fixture
            account_name: Partial string to match
        Returns:
            Instance of SFAccount with an existing Salesforce Account

        """

        account_query = "SELECT Id FROM Account WHERE Name LIKE '{}%'".format(account_name)
        sf_id = sf_interface.query(account_query)['records'][0]
        return cls(sf_interface, sf_id=sf_id['Id'])

    @classmethod
    def get_zuora_account_number(cls, sf_interface, account_name: 'SalesForce Account Name'):

        billing_account_number = sf_interface.query(
            "SELECT Cradlepoint_Integration_AccountId__c FROM Account WHERE Name = '{}'".format(
                account_name))['records'][0]['Cradlepoint_Integration_AccountId__c'].replace(' ', '')[:-3]

        minutes_to_wait = 5
        elapsed_time = 0
        error = None

        while elapsed_time < 60 * minutes_to_wait:
            try:
                return sf_interface.query("SELECT Zuora__AccountNumber__c FROM Zuora__CustomerAccount__c WHERE Name ="
                                          " '{}'".format(billing_account_number)
                                          )['records'][0]['Zuora__AccountNumber__c']
            except IndexError as e:
                elapsed_time += 10
                error = e
                time.sleep(10)
        else:
            raise error

    @classmethod
    def get_by_cradlepoint_integration_id(cls, sf_interface, cp_sf_id: 'Cradlepoint SalesForce Account ID'):
        account_query = "SELECT Id FROM Account WHERE Cradlepoint_Integration_AccountId__c = '{}'".format(cp_sf_id)
        sf_id = sf_interface.query(account_query)['records']
        return cls(sf_interface, sf_id=sf_id[0]['Id'][:-3])

    def sf_delete(self):
        self.logger.info('Deleting Pertino SF Object!')

        for entitlement in self.entitlements:
            entitlement.sf_delete()
            self.entitlements.remove(entitlement)

        for order in self.orders:
            order.sf_delete()
            self.orders.remove(order)

        for osl in self.order_service_lines:
            osl.sf_delete()
            self.order_service_lines.remove(osl)

        if self.contact is not None:
            self.contact.sf_delete()

        # Delete Account
        super(PertinoSFAccount, self).sf_delete()
