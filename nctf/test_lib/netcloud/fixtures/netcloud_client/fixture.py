import logging

from nctf.test_lib.base.fixtures.fixture import FixtureBase

logger = logging.getLogger("fixtures.netcloud_client")


class NetCloudClientFixture(FixtureBase):
    def __init__(self):
        logger.debug('WIP: Created a NetCloudClientFixture')

    def get_failure_report(self):
        report = str(self)
        report += 'This is the failure report for NetCloudClientFixture'
        return report

    def teardown(self):
        pass
