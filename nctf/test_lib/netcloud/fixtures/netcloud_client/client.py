import logging
import platform
from typing import List
from typing import Union

from nctf.test_lib.base.objects import NCTFObject
from nctf.test_lib.ncos.base.client import NCOSLANClient
from nctf.test_lib.netcloud.fixtures.netcloud_router import CreationMethod
from nctf.test_lib.netcloud.fixtures.netcloud_router import NetCloudRouter

# virtnetwork is only compatible with Linux (due to KVM+QEMU). Windows and macOS should not import this code.
if platform == "linux":
    from nctf.test_lib.ncos.fixtures.virtnetwork.fixture import VirtRouterConsole
else:
    VirtRouterConsole = None

logger = logging.getLogger("fixtures.netcloud_client")


class NetCloudClient(NCTFObject):
    def __init__(self, client: Union[NCOSLANClient, VirtRouterConsole], creation_method: CreationMethod):
        logger.debug('WIP: Created new NetCloudClient')
        self.creation_method = creation_method
        self.ncos = client

    def router_exec_command(self, command: Union[List[str], str], router: NetCloudRouter = None, timeout: int = 30) -> str:
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            if not router:
                raise ValueError(f"router cannot be None with self.creation_method={self.creation_method}")
            return self.ncos.router_exec_command(command=command, router=router.ncos, timeout=timeout)
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()

    def exec_command(self, command: Union[List[str], str], timeout: int = None) -> str:
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            return self.ncos.exec_command(command=command, timeout=timeout)
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()

    def ping(self, target: str, interface: str = "eth0", loss_threshold: int = 49, count: int = 3) -> bool:
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            return self.ncos.ping(target=target, interface=interface, loss_threshold=loss_threshold, count=count)
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()
