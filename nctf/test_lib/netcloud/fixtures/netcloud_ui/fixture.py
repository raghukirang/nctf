import typing

import nctf.test_lib.accounts.pages.login as login
from nctf.test_lib.base import pages
from nctf.test_lib.base.fixtures.fixture import FixtureBase


class NetCloudUIFixture(FixtureBase):
    """This fixture will create a webdriver and initialize a LoginPage with it when start is called.

    Other methods support starting on a page other than the LoginPage or allow a caller to instantiate a new webdriver
    that they can use on page initialization of their choosing.

    """

    def __init__(self, get_driver: typing.Callable[[], typing.Optional[pages.UIWebDriver]]):
        self.get_driver = get_driver

    @property
    def current_page(self) -> pages.UIPage:
        raise NotImplemented

    def start(self) -> login.LoginPage:
        return login.LoginPage(self.get_driver())

    def start_on(self, base_url: str, page: typing.Type[pages.UIPage]):
        return page(driver=self.get_driver(), base_url=base_url)

    def get_failure_report(self):
        raise NotImplementedError()

    def teardown(self):
        raise NotImplementedError()
