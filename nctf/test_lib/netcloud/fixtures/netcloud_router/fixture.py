import logging
from typing import List

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.core.arm4.fixture import ARMv4Exception
from nctf.test_lib.core.arm4.fixture import ARMv4Fixture
from nctf.test_lib.core.arm4.fixture import ARMv4Router
from nctf.test_lib.core.config.fixture import ConfigFixture
from nctf.test_lib.netcloud.fixtures.netcloud_router.router import CreationMethod
from nctf.test_lib.netcloud.fixtures.netcloud_router.router import NetCloudRouter

logger = logging.getLogger("fixtures.netcloud_router")


class NetCloudRouterFixtureException(Exception):
    pass


class NetCloudRouterFixture(FixtureBase):
    def __init__(self,
                 config_fixture: ConfigFixture,
                 creation_method: CreationMethod,
                 request,
                 arm4_fixture: ARMv4Fixture = None):
        logger.debug('WIP: Created a NetCloudRouterFixture')
        self.config_fixture = config_fixture
        self.creation_method = creation_method
        self.request = request
        if self.creation_method == CreationMethod.ARMV4:
            if not arm4_fixture:
                raise ValueError('armv4_fixture cannot be None with self.creation_method=CreationMethod.ARMv4')
            self.arm4_fixture = arm4_fixture

    def create(self, product_name: str = None, tags: List[str] = None, capabilities: dict = None) -> NetCloudRouter:
        if self.creation_method == CreationMethod.ARMV4:
            # The self.request.node.name is the name of the current test
            arm_router = self.get_armv4_router(
                product_name=product_name, tags=tags, capabilities=capabilities, reason=self.request.node.name)
            return NetCloudRouter(arm_router, self.creation_method)
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            if product_name or tags or capabilities:
                raise NetCloudRouterFixtureException(
                    'product_name, tags, and/or capabilities are not supported with self.creation_method=CreationMethod.VIRTNETWORK'
                )
            raise NotImplementedError()
        elif self.creation_method == CreationMethod.LOCAL:
            if product_name or tags or capabilities:
                raise NetCloudRouterFixtureException(
                    'product_name, tags, and/or capabilities are not supported with self.creation_method=CreationMethod.VIRTNETWORK'
                )
            raise NotImplementedError()

    def get_armv4_router(self, product_name: str = None, tags: List[str] = None, capabilities: dict = None,
                         reason: str = "") -> ARMv4Router:
        try:
            return self.arm4_fixture.lease_router(
                product_name=product_name, tags=tags, capabilities=capabilities, reason=reason)
        except ARMv4Exception as e:
            raise NetCloudRouterFixtureException(e)

    def get_failure_report(self):
        report = str(self)
        report += 'This is the failure report for NetCloudRouterFixture'
        return report

    def teardown(self):
        pass
