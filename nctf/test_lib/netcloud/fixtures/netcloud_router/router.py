import enum
import logging
import platform
import time
from typing import Union

from semantic_version import Version

from nctf.test_lib.base.objects import NCTFObject
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError
from nctf.test_lib.ncm.rest.client import NCMv1RESTClient
from nctf.test_lib.ncm.rest.router import NCMRouter
from nctf.test_lib.ncos.base.router import NCOSRouter
from nctf.test_lib.netcloud.fixtures.netcloud_account import NetCloudAccount
from nctf.test_lib.netcloud.fixtures.netcloud_account import NetCloudActor

# virtnetwork is only compatible with Linux (due to KVM+QEMU). Windows and macOS should not import this code.
if platform == "linux":
    from nctf.test_lib.ncos.fixtures.virtnetwork.fixture import VirtRouterConsole
else:
    VirtRouterConsole = None

logger = logging.getLogger("fixtures.netcloud_router")


class CreationMethod(enum.Enum):
    ARMV4 = 'armv4'
    VIRTNETWORK = 'virtnetwork'
    LOCAL = 'local'


class NetCloudRouterException(Exception):
    pass


class NetCloudRouter(NCTFObject):
    def __init__(self, device: Union[NCOSRouter, VirtRouterConsole], creation_method: CreationMethod):
        logger.debug('WIP: Created new NetCloudRouter')
        self.creation_method = creation_method
        self.ncos = device
        self.ncm: NCMRouter = None

    @property
    def fw_version(self) -> Version:
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            return self.ncos.fw_version
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()

    @property
    def product(self) -> str:
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            return self.ncos.product
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()

    def register_with_account(self, account: NetCloudAccount) -> 'NetCloudRouter':
        """Registers this router with the given account using the admin credentials.

        If the test desires to register this router to an account using an account login other than the admin,
        call register(netcloud_actor=user) with the NetcloudActor that represents the user to perform the registration.

        Args:
            account: The NetCloudAccount (NCM account) to register this router with.

        Returns:
            an NetCloudRouter with the assumption that the caller will want may chain calls.

        """
        self.register(netcloud_actor=account.admin)
        return self

    def register(self,
                 stream_host: str = None,
                 ncm_username: str = None,
                 ncm_password: str = None,
                 netcloud_actor: NetCloudActor = None,
                 ncm_rest_client: NCMv1RESTClient = None) -> NCMRouter:
        """A backward compatible method to register this router with an account associated with the given user.

        If this method is used, it is best called by only specifying the NetcloudActor from whom all other info will be
        derived.  The other optional parameters will override the NetcloudActor info.  The extensive parameter list is only
        specified to support existing tests.  Probably makes most sense for a test to call register_with_account() and
        providing the NetcloudAccount to register this router with.

        Args:
            stream_host: Endpoint of the stream host most often specified in configuration.
            ncm_username: Username of the user whose account this router will be registered with.
            ncm_password: Password of the user whose account this router will be registered with.
            netcloud_actor: If netcloud_actor is provided, all other parameters are optional but if provided will override the
             information that can be derived from the given NetcloudActor
            ncm_rest_client: Rest client endpoint for NCM

        Returns:
            an NCMRouter for backwards compatibility

        """

        if netcloud_actor:
            ncm_username = ncm_username or netcloud_actor.username
            ncm_password = ncm_password or netcloud_actor.password
            stream_host = stream_host or \
                netcloud_actor.netcloud_account.netcloud_account_fixture.config_fixture.target.services.stream.hostname
            ncm_rest_client = ncm_rest_client or netcloud_actor.ncm_rest_client

        if not ncm_username or not ncm_password:
            raise ValueError("Either a ncm_username and ncm_password or a valid netcloud_actor must be provided")
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            if not ncm_rest_client:
                raise ValueError("ncm_rest_client cannot be None with self.creation_method=CreationMethod.ARMv4")
            self.ncos.set_stream_server(stream_host=stream_host)
            logger.info("Sleeping 10 seconds between setting the server host and registering")
            time.sleep(10)
            stream = self.ncos.get_value('/config/ecm/server_host')
            if stream != stream_host:
                raise NetCloudRouterException(f'{self} stream host was {stream} but should have been {stream_host}')
            self.ncos.register(ncm_username=ncm_username, ncm_password=ncm_password)
            client_id = self.ncos.get_value('/status/ecm/client_id')
            self.ncm = NCMRouter(ncm_rest_client=ncm_rest_client, ncm_router_id=str(client_id))
            logger.info(f'{self} registered with NCM with id: {self.ncm.router_id}')
            return self.ncm
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()

    def unregister(self):
        logger.info(f'{self} attempting to unregister from NCM')
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            self.ncos.unregister()
            try:
                self.ncm.unregister()
            except UnexpectedStatusError as e:
                logger.info(
                    f'{self} was unable to unregister through NCM, likely because unregister through the router API was successful'
                )
            self.ncm = None
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()

    def reboot(self):
        if self.creation_method == CreationMethod.ARMV4 or self.creation_method == CreationMethod.LOCAL:
            self.ncos.reboot()
        elif self.creation_method == CreationMethod.VIRTNETWORK:
            raise NotImplementedError()
