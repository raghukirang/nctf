import logging
import random
import re
import string
import time
import typing
from urllib.parse import urlparse

from nctf.libs.common_library import wait_for_with_interval
from nctf.libs.common_library import WaitTimeoutError
from nctf.libs.salesforce.common_lib import BundleProducts
from nctf.libs.salesforce.common_lib import CoreFeatures
from nctf.libs.salesforce.common_lib import FeatureConsumers
from nctf.libs.salesforce.common_lib import FeatureUUIDs
from nctf.libs.salesforce.common_lib import FeatureListIds
from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.libs.salesforce.common_lib import Products
from nctf.test_lib.accounts.rest import AccServActor
from nctf.test_lib.accounts.rest import AccServRESTClient
from nctf.test_lib.accounts.rest import AccSrvAccount
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.core.email.base import EmailInbox
from nctf.test_lib.salesforce.objects.account import SFAccount
from nctf.test_lib.salesforce.objects.contact import SFContact
import requests

from .actor import AcceptTOSMethod
from .actor import ActivationMethod
from .actor import CreationMethod
from .actor import NetCloudActor
from .fixture import NetCloudAccountFixture

logger = logging.getLogger("fixtures.netcloud_account")

NEW_ACCOUNT_INVITATION_EMAIL_SUBJECT = "Activate Your Cradlepoint NetCloud User Account"
OLD_ACCOUNT_INVITATION_EMAIL_SUBJECT = "Setup Your Cradlepoint NetCloud Account"


class NetCloudAccount(object):
    """Create a NetCloud Account that represents an account that is existing in NetCloud or is being created by the fixture.

    Args:
        netcloud_account_fixture: The netcloud accounts fixture to have access to the necessary endpoints and config to create
        creation_method: the method to use to create accounts, the default is whatever the fixture is configured for.
        name: Create the account with the given name.  Otherwise the account name will be made from admin first and last names.
        activate: Activate the account via the activation email.
        accept_tos: Accept the terms of service. (assumes activate is truthy)
        admin_username: The username of the pre-existing admin or one to create with this username
        admin_first_name: The first name of the admin to create.
        admin_last_name: The last name of the admin to create.
        admin_password: The password for the NetCloud Account Admin.
        entitlements: AKA subscriptions. Only used if creation method is Salesforce or Salesforce_Mock. Defaults to
        ["ECM_PRIME_1YR","NCE_GW_STANDARD_1YR"] if not specified.
        sf_optional_params: Any other fields to be populated in Salesforce during creation
    """

    def __str__(self):
        return "<{}(tenant id='{}', admin='{}')>".format(self.__class__.__name__, self.tenant_id, self.admin)

    def __init__(self,
                 netcloud_account_fixture: NetCloudAccountFixture,
                 creation_method: CreationMethod = None,
                 name: str = None,
                 activate: typing.Union[ActivationMethod, str, bool] = ActivationMethod.API,
                 accept_tos: typing.Union[AcceptTOSMethod, str, bool] = AcceptTOSMethod.API,
                 admin_username: str = None,
                 admin_first_name: str = None,
                 admin_last_name: str = None,
                 admin_password: str = "Password1!",
                 entitlements: typing.List[str] = None,
                 sf_optional_params: dict() = None,
                 validate_product: bool = True):

        self.netcloud_account_fixture = netcloud_account_fixture
        self._tenant_id = None
        self.sf_account = None
        self.admin: NetCloudActor = None
        inbox = None
        password_token = None

        creation_method = netcloud_account_fixture.creation_method if creation_method is None else creation_method
        activate = ActivationMethod(activate.lower()) if isinstance(activate, str) else activate
        accept_tos = AcceptTOSMethod(accept_tos.lower()) if isinstance(accept_tos, str) else accept_tos

        if accept_tos and not activate:
            raise ValueError("A new NetCloud Account Admin cannot accept the TOS if he does not activate.")
        if entitlements is not None and netcloud_account_fixture.creation_method is CreationMethod.ACCOUNTS:
            logger.warning("Passing in entitlements to create() will do nothing if creation method is ACCOUNTS.")

        # Default entitlement that will work for most tests
        entitlements = entitlements or ["ECM_PRIME_1YR", "NETCLOUD_BRANCH_ROUTERS_1YR", "NCP_ENABLE"]

        if not admin_username:
            admin_first_name, admin_last_name, inbox = self.generate_new_user_info(
                first_name=admin_first_name, last_name=admin_last_name)
            admin_username = inbox.address

        logger.info("Creating new NetCloud Account via {}".format(netcloud_account_fixture.creation_method))
        if creation_method == CreationMethod.ACCOUNTS:
            self._tenant_id, password_token = self.create_via_accounts_service(
                email=admin_username, first_name=admin_first_name, last_name=admin_last_name, name=name)
        elif creation_method == CreationMethod.SALESFORCE:
            self._tenant_id = self.create_via_salesforce(
                email=admin_username,
                first_name=admin_first_name,
                last_name=admin_last_name,
                entitlements=entitlements,
                name=name,
                sf_optional_params=sf_optional_params,
                validate_product=validate_product)
        elif creation_method == CreationMethod.SALESFORCE_MOCK:
            self._tenant_id = self.create_via_salesforce_mock(
                email=admin_username,
                first_name=admin_first_name,
                last_name=admin_last_name,
                entitlements=entitlements,
                name=name,
                sf_optional_params=sf_optional_params)
        elif creation_method == CreationMethod.PREDEFINED:
            # nothing to be done for creation purposes, but force no activation, create the admin and get their tenant id
            activate = False
            self.admin = NetCloudActor(
                username=admin_username,
                password=admin_password,
                password_token=password_token,
                user_state=NetCloudActor.UserState.ACTIVATED,
                netcloud_account=self)
            self._tenant_id = self.admin.authenticate().accserv.tenant_id
        else:
            raise ValueError("Invalid CreationMethod set.")

        if creation_method != CreationMethod.PREDEFINED:
            if self.netcloud_account_fixture.use_activation_email:
                inbox = inbox or self.generate_inbox_from_username(admin_username)
                if inbox:
                    password_token = self._get_password_token_from_email(inbox)
            self.admin = NetCloudActor(
                username=admin_username,
                password=admin_password,
                password_token=password_token,
                user_state=NetCloudActor.UserState.CREATED,
                netcloud_account=self)

        if activate:
            self.admin.activate(activation_method=activate)
            if accept_tos:
                self.admin.accept_tos(accept_tos)

        self.accserv = AccSrvAccount(self.admin.accserv.accounts_service_rest_client, self._tenant_id)

    def create_via_accounts_service(self, email: str, first_name: str, last_name: str, name: str = None) -> (str, str):
        """ Create a new NetCloud Account and NetCloud Account Admin via the Account Service using privileged credentials

        Args:
            email: The email address of the admin to create.
            first_name: The first name of the admin to create.
            last_name: The last name of the admin to create.
            name: The optional name of the account, if not provided it will be constructed from the admin first and last names.

        Returns:
            The tenant id of the new account and the password token of the admin.
        """
        tenant_id = str(int(time.time()))

        auth = (self.netcloud_account_fixture.accounts_privileged_username,
                self.netcloud_account_fixture.accounts_privileged_password)
        name = name or "{} {}'s Account".format(first_name, last_name)

        rest = AccServRESTClient(
            hostname=self.netcloud_account_fixture.accounts_api_hostname,
            protocol=self.netcloud_account_fixture.accounts_api_protocol,
            port=self.netcloud_account_fixture.accounts_api_port,
            timeout=self.netcloud_account_fixture.timeout)
        rest.authenticate(*auth)

        rest.accounts.create(tenant_id, name)
        r = rest.users.create(email, 'rootAdmin', tenant_id, first_name, last_name)
        password_token = r.headers.get("password_token", None)

        return tenant_id, password_token

    def create_via_salesforce(self,
                              email: str,
                              first_name: str,
                              last_name: str,
                              entitlements: typing.List[str],
                              name: str = None,
                              sf_optional_params: dict() = None,
                              validate_product=True) -> str:
        """Create a new NetCloud Account and NetCloud Account Admin vi Salesforce

        Args:
            email: The email address of the admin to create.
            first_name: The first name of the admin to create.
            last_name: The last name of the admin to create.
            entitlements: the list of entitlements for this new account.
            name: The optional name of the account, if not provided it will be constructed from the admin first and last names.
            sf_optional_params: any optional parameters to pass along to the creation of the salesforce interface.
            validate_product: validate the product exists within the nctf framework

        Returns:
            The tenant id of the new account.
        """
        optional_params = sf_optional_params if not name else \
            {"Name": name} if not sf_optional_params else sf_optional_params.update({"Name": name})

        sf_account = SFAccount.create_random(
            self.netcloud_account_fixture.salesforce_fixture, optional_params=optional_params, create_contact=False)
        self.sf_account = sf_account
        logger.info("Creating SF Account: {} with ID: {}".format(sf_account.sf_create_params['Name'], sf_account.sf_id))

        sf_contact = SFContact.create_random(self.netcloud_account_fixture.salesforce_fixture, {
            'FirstName': first_name,
            'LastName': last_name,
            'AccountId': sf_account.sf_id,
            'Email': email
        })
        sf_contact.sf_get()
        assert sf_contact.sf_attributes['AccountId'] == sf_account.sf_id
        sf_account.contact = sf_contact
        # Add entitlements
        for entitlement_name in entitlements:
            if entitlement_name in Products.__members__ and entitlement_name in ProductCodes.__members__:
                product = getattr(ProductCodes, entitlement_name)
                logger.info("Entitling the SF account '{}' for product '{}'".format(sf_account.id(), product.value))
                sf_account.entitle(product)
            elif entitlement_name in BundleProducts.__members__:
                product = getattr(BundleProducts, entitlement_name)
                sf_account.entitle_bundle(product)
                sf_account.entitle_bundle(product)
            else:
                if validate_product:
                    raise ValueError("Could not entitle SF account '{}': No known product '{}'".format(
                        sf_account.id(), entitlement_name))
                else:
                    logger.info("Attempting to entitle with a potentially invalid product!")
                    sf_account.entitle(entitlement_name, validate_product=False)

        # Wait for Tenant ID
        try:
            wait_for_with_interval(120, 2, True, lambda: sf_account.sf_get()['SSO_Tenant_Id__c'] is not None)
            tenant_id = sf_account.sf_get()['SSO_Tenant_Id__c']
            logger.info('Got Tenant ID: {}'.format(tenant_id))
        except WaitTimeoutError as e:
            raise WaitTimeoutError("Salesforce Account did not get a Tenant ID within two minutes for Account ID={}".format(
                sf_account.id())) from e
        return tenant_id

    def create_via_salesforce_mock(self,
                                   email: str,
                                   first_name: str,
                                   last_name: str,
                                   entitlements: typing.List[str],
                                   name: str = None,
                                   sf_optional_params: dict() = None) -> str:
        """Create a new NetCloud Account and NetCloud Account Admin vi Salesforce

        Args:
            email: The email address of the admin to create.
            first_name: The first name of the admin to create.
            last_name: The last name of the admin to create.
            entitlements: the list of entitlements for this new account.
            name: The optional name of the account, if not provided it will be constructed from the admin first and last names.
            sf_optional_params: any optional parameters to pass along to the creation of the salesforce interface.

        Returns:
            The tenant id of the new account.
        """

        def generate_id() -> str:
            return ''.join(random.SystemRandom().choice(string.digits + string.ascii_letters) for _ in range(15))

        bsf_auth = (self.netcloud_account_fixture.config_fixture.target.predefinedAuth.bsf.username,
                    self.netcloud_account_fixture.config_fixture.target.predefinedAuth.bsf.password)
        name = name or "{} {}'s Account".format(first_name, last_name)

        # Create account
        account_id = generate_id()
        payload = {
            "commerceId": account_id,
            "commerceIdSource": "SALESFORCE",
            "name": "Account for {} {} ({})".format(first_name, last_name, account_id)
        }
        self.netcloud_account_fixture.bsf_sfconnector_fixture.post_account(json=payload)

        # Create user
        user_id = generate_id()
        payload = {
            "commerceId": user_id,
            "commerceIdSource": "SALESFORCE",
            "email": email,
            "firstName": first_name,
            "name": name,
            "lastName": last_name,
            "tenantId": account_id
        }
        self.netcloud_account_fixture.bsf_sfconnector_fixture.post_user(json=payload)

        license_service_client = RESTClient(
            hostname=self.netcloud_account_fixture.config_fixture.target.services.bsf.licenseService.hostname,
            protocol=self.netcloud_account_fixture.config_fixture.target.services.bsf.licenseService.protocol,
            port=self.netcloud_account_fixture.config_fixture.target.services.bsf.licenseService.port)

        for entitlement in entitlements:
            subscription_commerce_id = generate_id()

            # Post subscription
            feature_list_id = FeatureListIds[entitlement].value
            payload = {
                "commerceId": subscription_commerce_id,
                "commerceIdSource": "SALESFORCE",
                "accountId": account_id,
                "accountIdSource": "SALESFORCE",
                "endUserId": user_id,
                "endUserIdSource": "SALESFORCE",
                "entitlementPolicy": "NONE",
                "featureListId": feature_list_id,
                "featureListIdSource": "SALESFORCE",
                "name": ProductCodes[entitlement].value,
                "quantity": 100,
                "trial": "false",
                "startDate": "2018-03-14T00:00:00Z",
                "endDate": "2020-03-14T00:00:00Z",
                "gracePeriodDate": "2020-03-14T00:00:00Z"
            }

            self.netcloud_account_fixture.bsf_sfconnector_fixture.post_subscription(json=payload)
            license_service_client.get(
                f"/api/v1/subscriptions?commerceId={subscription_commerce_id}&commerceIdSource=SALESFORCE",
                expected_status_code=200,
                auth=bsf_auth)
        return account_id

    @property
    def tenant_id(self) -> str:
        """Gets the tenant id of the account setup during init, otherwise call on the accounts service endpoint."""
        if not self._tenant_id:
            self._tenant_id = self.accserv.tenant_id
        return self._tenant_id

    @staticmethod
    def _get_password_token(email_body: str) -> typing.Union[str, None]:
        """Retrieve the password token from a NetCloud invitation email. """
        p = re.compile('<a href=".+?passwordTokens/(.+?)"><span class="teal-text">HERE</span></a>')
        matches = p.findall(email_body)
        if len(matches) == 0:
            return None
        return matches[0]

    def _get_password_token_from_email(self, inbox: EmailInbox) -> str:
        """Retrieve the password token from an email in the actor's mailbox."""
        if not inbox:
            raise RuntimeError("{} does not have an inbox to retrieve their password token.".format(self))
        try_count = 0
        attempts = 18
        password_token = None
        while try_count < attempts:
            try:
                messages = inbox.get_messages_by_subject(subject=NEW_ACCOUNT_INVITATION_EMAIL_SUBJECT)
                password_token = self._get_password_token(messages[0].body)
                break
            except IndexError:
                try_count += 1
                try:
                    messages = inbox.get_messages_by_subject(subject=OLD_ACCOUNT_INVITATION_EMAIL_SUBJECT)
                    password_token = self._get_password_token(messages[0].body)
                    break
                except IndexError:
                    if try_count >= attempts:
                        raise RuntimeError("{} failed to receive the account invitation email.".format(self))
                    time.sleep(10)
        return password_token

    def generate_inbox_from_username(self, username: str) -> typing.Union[EmailInbox, None]:
        """Generate an inbox from the given email address as a username.

        Will warn and return None if the domain of the given email address is different than the configured email fixture.

        Args:
            username: The email address of the user to create an inbox for.

        Returns:
            The inbox to the configured email service for the given user.
        """
        user_email_domain = username.split("@")[1]
        fixture_email_domain = urlparse(self.netcloud_account_fixture.email_fixture.api_root).netloc
        if user_email_domain in fixture_email_domain:
            email_root = username.split("@")[0]
        else:
            logger.warning("{} cannot generate an inbox from a domain different than the email fixture.".format(self))
            return None
        return self.netcloud_account_fixture.email_fixture.get_inbox(email_root)

    def generate_new_user_info(self, first_name: str = None, last_name: str = None):
        first_name = first_name or self.netcloud_account_fixture.email_fixture.get_random_firstname()
        last_name = last_name or self.netcloud_account_fixture.email_fixture.get_random_lastname()
        postfix = self.netcloud_account_fixture.email_fixture.get_random_postfix()
        email_root = "{}_{}_{}".format(first_name.lower(), last_name.lower(), postfix.lower())
        inbox = self.netcloud_account_fixture.email_fixture.get_inbox(email_root)
        return first_name, last_name, inbox

    default_applications = ["ecm"]

    def create_user(self,
                    username: str = None,
                    password: str = "Password1!",
                    first_name: str = None,
                    last_name: str = None,
                    role: AccServActor.Roles = AccServActor.Roles.USER,
                    applications: typing.List[str] = default_applications,
                    is_active: bool = True,
                    mfa_enabled=False,
                    is_locked: bool = False,
                    is_federated: bool = False,
                    activation_method: typing.Union[ActivationMethod, str, bool] = ActivationMethod.API,
                    authenticate: bool = True) -> NetCloudActor:
        """Create a NetCloudActor that is a user in this Account. Do NOT use this method to create the initial account admin.

        Args:
            username: The email address of the user to create.
            password: The password for the user, default is "Password1!"
            first_name: The first name of the user to create.
            last_name: The last name of the user to create.
            role: The role of the user to create as a AccServActor.Roles enum, default is AccServActor.Roles.USER
            applications: A list of strings defining the applications that the user can use.
            is_active: An indication that the user to create is to be active.
            mfa_enabled: Turn on multi factor authentication for the user to create.
            is_locked: Create the user locked.
            is_federated: The user is created as a federated account.
            activation_method: The method by which to active the user. Can be via ActivationMethod.UI,'ui' or
            ActivationMethod.API, 'api', or False to not activate the user.
            authenticate: Whether the new user should be authenticated against the accounts services.

        Returns:
            a NetCloudActor that has been at least CREATED but could be ACTIVATED or AUTHENTICATED.
        """
        activation_method = activation_method if not isinstance(activation_method, str) \
            else ActivationMethod(activation_method.lower())
        inbox = None
        if username:
            if not (first_name and last_name):
                raise ValueError("Cannot create a user with a username when there is no first name and last name."
                                 "If trying to create a NetCloudActor for an existing user, use account.find_user_by_login().")
        else:
            first_name, last_name, inbox = self.generate_new_user_info()
            username = inbox.address

        self.admin.authenticate()
        accserv_actor = self.accserv.create_user(
            username=username,
            role=role,
            first_name=first_name,
            last_name=last_name,
            applications=applications,
            is_active=is_active,
            is_locked=is_locked,
            mfa_enabled=mfa_enabled,
            is_federated=is_federated)

        password_token = accserv_actor.password_token
        if is_active and self.netcloud_account_fixture.use_activation_email:
            inbox = inbox or self.generate_inbox_from_username(username)
            if inbox:
                password_token = self._get_password_token_from_email(inbox)

        user = NetCloudActor(
            username=username,
            password=password,
            password_token=password_token,
            user_state=NetCloudActor.UserState.CREATED,
            netcloud_account=self)

        if activation_method:
            if not password:
                raise ValueError("{}: Could not activate user {} since password is None.".format(self, user))
            else:
                user.activate(activation_method=activation_method, authenticate=authenticate)

        logger.info('{}:Created user={}, role={}, is_active={}, is_locked={}, mfa_enabled={}, is_federated={}'.format(
            self, user, role, is_active, is_locked, mfa_enabled, is_federated))
        return user

    def delete_user(self, user: NetCloudActor) -> None:
        """Delete this user from the NetCloud account and neuters the NetCloudActor passed in."""
        self.delete_user_by_id(user_id=user.accserv.id)
        user.user_state = NetCloudActor.UserState.NONEXISTENT
        user._accounts_service_rest_client = None
        user._accounts_user_id = None

    def delete_user_by_id(self, user_id: str) -> None:
        """Delete the user specified by user_id from this NetCloud account."""
        self.admin.authenticate()
        self.accserv.delete_user_by_id(user_id=user_id)

    def instantiate_netcloud_actor_from_accserv_actor(self, actor: AccServActor) -> NetCloudActor:
        """Instantiate a NetCloudActor given the accounts service actor."""
        username = actor.username
        if actor.is_activated:
            user_state = NetCloudActor.UserState.ACTIVATED
        else:
            user_state = NetCloudActor.UserState.CREATED
        return NetCloudActor(username=username, netcloud_account=self, user_state=user_state)

    def get_users(self,
                  status: AccServActor.UserStatus = None,
                  email: str = None,
                  ordering: str = "firstName,lastName",
                  search: str = None,
                  batch_size: int = 25) -> typing.List[NetCloudActor]:
        """Get a list of netcloud actors in this account.

        Args:
            status: only return actors that match the given status
            email:  only return actors that match the given email
            ordering: order the list by this comma separated string. Default is "firstName,lastName"
            search: only return actors that match the given search string.
            batch_size: only return the given number of actors at one time.

        Returns:
            a list of batch_size NetcloudActors that match the given criteria in the specified order.
        """

        self.admin.authenticate()
        accserv_actors = self.accserv.get_users(status, email, ordering, search, batch_size)

        users = []
        for accserv_actor in accserv_actors:
            users.append(self.instantiate_netcloud_actor_from_accserv_actor(accserv_actor))
        return users

    def get_user_by_login(self, email: str) -> NetCloudActor:
        """Return a NetCloudActor that is a member of this account that has the given email address as their login. """
        user_list = self.get_users(email=email)
        return None if len(user_list) == 0 else user_list[0]
