import enum
import logging
import time
import typing

from nctf.test_lib.accounts.rest import AccServActor
from nctf.test_lib.base.actor import NCTFActorWithCreds
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as NCA
from nctf.test_lib.utils.waiter import wait

logger = logging.getLogger("fixtures.netcloud_account")


class CreationMethod(enum.Enum):
    ACCOUNTS = "accounts"
    PREDEFINED = "predefined"
    SALESFORCE = "salesforce"
    SALESFORCE_MOCK = "salesforce_mock"


class ActivationMethod(enum.Enum):
    UI = "ui"
    API = "api"


class AcceptTOSMethod(enum.Enum):
    UI = "ui"
    API = "api"


Roles = AccServActor.Roles


class NetCloudActor(NCTFActorWithCreds):
    """A customer using any NetCloud service.

    The NetCloudActor is a collection of all the user-ish pieces necessary to talk with any service in the NetCloud set of
    products whether that be NetCloudManager, NetCloudPerimeter or NetCloudOS.  It allows tests to hold in one object the
    notion of a user whose identity spans all products as long as the username and password are the same. NetCloudActor
    aggregates the notion of a Accounts Service actor (netcloud_actor.accserv) and NCM Service actor (netcloud_actor.ncmserv).
    To see the accounts service id reference netcloud_actor.accserv.id which is different than the NCM service notion of id
    which can be had by using netcloud_actor.ncmserv.id.

    The NetCloudActor can represent a pre-defined account in which case all that is needed is a username and password. If
    the actor is being created as a user in NetCloud, then the account it is being created in needs to be provided.

    The NetCloudActor has state which goes from NONEXISTENT to CREATED to ACTIVATED then to AUTHENTICATED.

    Args:
        username: The username of the actor in the form of an email address.  The ONLY use case to directly instantiate a
        NetCloudActor is to provide a username with a password in which case it is assumed to be a predefined account.
        password: The password to log into NetCloud products. If password is not given now, it must be provided on activation.
        password_token: The password token used to activate an actor.  If not provided at construction, must be provided on
        activation.
        netcloud_account: The creating account in the event that this actor is being created by an account.
        user_state: The state to set the instantiated actor to.

        # Pre-defined user use case - provide a username and password.  This is the ONLY marginally acceptable use of
        # instantiating a NetCloudActor directly; all instantiations should take place through the NetCloudAccount object.
        user = NetCloudActor('cp.jrny@gmail.com', 'Quality805!')

        # Users should be created via one of the following
        user = account.create_user(...)
        user = account.get_user_by_login("someone@login.com")   # a better way to get a pre-defined user
        users = account.get_users(...)

    """

    class UserState(enum.Enum):
        NONEXISTENT = "nonexistent"
        CREATED = "created"
        ACTIVATED = "activated"
        AUTHENTICATED = "authenticated"

    def __str__(self):
        return "<{} user_state={})>".format(super().__str__(), self.user_state)

    def __init__(self,
                 username: str,
                 password: str = None,
                 password_token: str = None,
                 netcloud_account: 'NCA.NetCloudAccount' = None,
                 user_state: 'NetCloudActor.UserState' = UserState.NONEXISTENT):

        self.user_state = user_state
        self.accserv: AccServActor = None
        self.ncmserv: NCMv1RESTClient = None
        self._netcloud_account = netcloud_account
        self._password_token = password_token
        accounts_api_protocol = None
        accounts_api_hostname = None
        accounts_api_port = None
        self.ncm_api_protocol = None
        self.ncm_api_hostname = None
        self.ncm_api_port = None
        self.timeout = 30.0

        if username and password and not netcloud_account:
            # Assume if the actor is instantiated with a username, password and not netcloud_account, that it is a pre-defined
            # user and has already been activated...this covers backward compatible use case expectation.
            self.user_state = NetCloudActor.UserState.ACTIVATED

        if netcloud_account:
            accounts_api_protocol = netcloud_account.netcloud_account_fixture.accounts_api_protocol
            accounts_api_hostname = netcloud_account.netcloud_account_fixture.accounts_api_hostname
            accounts_api_port = netcloud_account.netcloud_account_fixture.accounts_api_port
            self.ncm_api_protocol = netcloud_account.netcloud_account_fixture.ncm_api_protocol
            self.ncm_api_hostname = netcloud_account.netcloud_account_fixture.ncm_api_hostname
            self.ncm_api_port = netcloud_account.netcloud_account_fixture.ncm_api_port
            self.timeout = netcloud_account.netcloud_account_fixture.timeout or self.timeout
        else:
            if username:
                # Done this way for documentation:
                # Do not need an account if there is a username, but accounts service endpoint info must be
                # provided on authentication if this actor is to be used for REST api interactions.
                pass
            else:
                raise ValueError("NetCloudActor: need at least a username or netcloud_account to create a NetCloudActor.")
        super().__init__(username, password)
        self.accserv = AccServActor(
            username,
            password=password,
            password_token=password_token,
            accounts_api_protocol=accounts_api_protocol,
            accounts_api_hostname=accounts_api_hostname,
            accounts_api_port=accounts_api_port,
            timeout=self.timeout)

    @property
    def netcloud_account(self):
        if not self._netcloud_account:
            raise RuntimeError("{} needs to have been instantiated with a netcloud account or if a pre-defined user, "
                               "call authenticate() with accounts service api protocol, hostname and port..".format(self))
        return self._netcloud_account

    def activate(self,
                 password: str = None,
                 password_token: str = None,
                 activation_method: typing.Union[ActivationMethod, str, bool] = ActivationMethod.API,
                 authenticate: bool = True,
                 accounts_api_protocol: str = None,
                 accounts_api_hostname: str = None,
                 accounts_api_port: int = None,
                 timeout: float = None) -> 'NetCloudActor':
        """Activate this user using the given activation method.

        Args:
            password: Activate the user and change the password to the given password. Password here is optional since
            it could have been given on instantiation of the NetCloudActor.
            password_token: The token used to activate and set the initial password of the actor.
            activation_method: The method by which to active the user. Can be via ActivationMethod.UI,'ui' or
            ActivationMethod.API, 'api'.
            authenticate: By default, activation will cause authentication.  To avoid, set authenticate to False.
            accounts_api_protocol: optional protocol for the accounts service
            accounts_api_hostname: optional hostname for the accounts service
            accounts_api_port: optional port for the accounts service
            timeout: optional timeout for communicating with the accounts service, default is 30 secs (determined by accserv).

        Returns:
            The activated and possible authenticated actor for call chaining.
        """
        self.password = password or self.password

        if self.user_state is not NetCloudActor.UserState.CREATED:
            raise RuntimeError("{} Needs to be 'CREATED' from account.create_user()".format(self))

        if self.password is None:
            raise ValueError("{} Cannot activate without a password".format(self))

        self._password_token = password_token or self._password_token
        if self._password_token is None:
            raise ValueError("{} Cannot activate without a password token".format(self))

        activate = activation_method if not isinstance(activation_method, str) else ActivationMethod(activation_method.lower())
        if activate == ActivationMethod.API:
            self.accserv.activate(
                self.password,
                password_token=self._password_token,
                accounts_api_protocol=accounts_api_protocol,
                accounts_api_hostname=accounts_api_hostname,
                accounts_api_port=accounts_api_port,
                timeout=timeout)
        elif activate == ActivationMethod.UI:
            self.activate_via_ui(self.password)
        else:
            raise ValueError("{} Invalid ActivationMethod: {}".format(self, activation_method))

        self.user_state = NetCloudActor.UserState.ACTIVATED

        if authenticate:
            # Authenticate to get a session for subsequent self.accounts_service_rest_client accesses
            return self.authenticate()
        return self

    def activate_via_ui(self, password: str, *args, **kwargs):
        """ Activate the new NetCloud user using the netcloud UI.

        It uses the password token mined from the invitation email or gotten from the return of creating the user through
        the Accounts Service REST API.

        """
        raise NotImplementedError()

    def authenticate(self,
                     password: str = None,
                     accounts_api_protocol: str = None,
                     accounts_api_hostname: str = None,
                     accounts_api_port: int = None,
                     timeout: float = None) -> 'NetCloudActor':
        """Authenticates the users credentials with the accounts service.

        Authenticates with the accounts service such that subsequent actions that talk to the accounts service can do so as
        this actor. If the actor was created through an account then no accounts service endpoint information needs to be
        passed in.

        Args:
            password: optional password to authenticate with.  Provide the password if it was not passed in on NetCloudActor
            instantiation or not passed in on a call to activate().
            accounts_api_protocol: optional protocol for the accounts service if not previously provided.
            accounts_api_hostname: optional hostname for the accounts service if not previously provided.
            accounts_api_port: optional port for the accounts service if not previously provided.
            timeout: optional timeout for communicating with the accounts service, default is 30 secs (determined by accserv).

        Returns:
            The authenticated actor for call chaining.
        """
        if self.user_state != NetCloudActor.UserState.ACTIVATED and self.user_state != NetCloudActor.UserState.AUTHENTICATED:
            raise RuntimeError("{} needs to be activated before it can be authenticated".format(self))

        self.password = password or self.password
        self.accserv.authenticate(
            password=self.password,
            accounts_api_protocol=accounts_api_protocol,
            accounts_api_hostname=accounts_api_hostname,
            accounts_api_port=accounts_api_port,
            timeout=timeout)

        self.user_state = NetCloudActor.UserState.AUTHENTICATED
        return self

    def accept_tos(self, accept_method: typing.Union[AcceptTOSMethod, str, bool] = AcceptTOSMethod.API) -> 'NetCloudActor':
        """Activate this user using the given activation method.

        Args:
            accept_method: The method by which to active the user. Can be via AcceptTOSMethod.UI 'ui' or
            AcceptTOSMethod.API 'api'.

        Returns:
            The actor that has accepted the TOS for call chaining.
        """
        accept_tos = accept_method if not isinstance(accept_method, str) else AcceptTOSMethod(accept_method.lower())

        if accept_tos == AcceptTOSMethod.API:
            return self.accept_nc_user_tos_via_api()
        elif accept_tos == AcceptTOSMethod.UI:
            return self.accept_nc_user_tos_via_ui()
        else:
            raise ValueError("Invalid AcceptTOSMethod: {}".format(accept_method))

    @property
    def ncm_rest_client(self) -> NCMv1RESTClient:
        if not self.ncmserv:
            # The actor should have already been authenticated to have a valid session, but do it to be sure.
            self.authenticate()
            self.ncm_api_protocol = self.ncm_api_protocol or self.netcloud_account.netcloud_account_fixture.ncm_api_protocol
            self.ncm_api_hostname = self.ncm_api_hostname or self.netcloud_account.netcloud_account_fixture.ncm_api_hostname
            self.ncm_api_port = self.ncm_api_port or self.netcloud_account.netcloud_account_fixture.ncm_api_port
            if self.ncm_api_protocol and self.ncm_api_hostname and self.ncm_api_port:
                self.ncmserv = NCMv1RESTClient(self.ncm_api_protocol, self.ncm_api_hostname, self.ncm_api_port, self.timeout,
                                               self.accserv.accounts_service_rest_client.session)
            else:
                raise ValueError("{} needs to have an NCMv1RESTClient endpoints set up. "
                                 "Set up actor.ncm_api_protocol, actor.ncm_api_hostname and actor.ncm_api_port.".format(self))
        return self.ncmserv

    def accept_nc_user_tos_via_api(self) -> 'NetCloudActor':
        """Accept the Terms of Service agreement via the NCM REST API.

        If the actor was not created through a NetCloudAccount, then the endpoint information for NCM needs to be provided.

        Args:
            ncm_api_protocol: optional protocol for the ncm service
            ncm_api_hostname: optional hostname for the ncm service
            ncm_api_port: optional port for the ncm service
            timeout: optional timeout used to talk with the NCM service. The default is 30 secs.

        Returns:
            The actor that has accepted the TOS.
        """
        logger.info("Accepting TOS for {}".format(self))

        # wait until the user has been really created before trying to talk to NCM about them
        try:
            wait(30, 0.5).for_call(self.netcloud_account.netcloud_account_fixture.is_user(self.username))
        except UnexpectedStatusError as e:
            # something went wrong with the is_user call, if the fixture did not have the correct creds then just sleep
            if e.status_code == 401:
                logger.warning("{}: activate(): unexpected status raised when seeing if the user exists=e{}".format(self, e))
                time.sleep(10)
            else:
                # otherwise re-throw the exception
                raise e

        r = self.ncm_rest_client.get("/api/v1/system_message/?type=tos&order_by=-modified&fields=resource_uri", verify=False)
        resource_uri = r.json()['data'][0]['resource_uri']
        self.ncm_rest_client.post('/api/v1/system_message_confirm/', 201, json={'message': resource_uri})
        return self

    def accept_nc_user_tos_via_ui(self, *args, **kwargs) -> 'NetCloudActor':
        """Accept the Terms of Service agreement via the NCM UI."""
        raise NotImplementedError()

    def change_password(self, current_password: str, new_password: str) -> 'NetCloudActor':
        self.authenticate(password=current_password)
        self.accserv.change_password(current_password, new_password)
        # Set up the new password and force a new rest client to be built with new credentials
        self.password = new_password
        self.user_state = NetCloudActor.UserState.ACTIVATED
        return self
