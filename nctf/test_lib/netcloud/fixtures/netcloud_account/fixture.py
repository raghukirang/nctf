import logging
from typing import List
from typing import Union

from nctf.test_lib.accounts.rest import AccServRESTClient
from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError
from nctf.test_lib.bsf.fixtures.sfconnector import SFConnectorSession
from nctf.test_lib.core.config.fixture import ConfigFixture
from nctf.test_lib.core.email import EmailFixture
from nctf.test_lib.core.email import EmailService
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as NCA
from nctf.test_lib.salesforce.fixture import _SalesforceConnection

from .actor import AcceptTOSMethod
from .actor import ActivationMethod
from .actor import CreationMethod

logger = logging.getLogger("fixtures.netcloud_account")


class NetCloudAccountFixture(FixtureBase):
    def __init__(self,
                 creation_method: CreationMethod,
                 email_fixture: EmailFixture,
                 config_fixture: ConfigFixture = ConfigFixture(),
                 use_activation_email: bool = False,
                 ncm_api_protocol: str = None,
                 ncm_api_hostname: str = None,
                 ncm_api_port: int = None,
                 ncm_api_id: str = None,
                 ncm_api_token: str = None,
                 accounts_api_protocol: str = None,
                 accounts_api_hostname: str = None,
                 accounts_api_port: int = None,
                 accounts_privileged_username: str = None,
                 accounts_privileged_password: str = None,
                 timeout: float = 30.0):
        """NetCloud Account Fixture - the factory for NetCloud accounts.

        Args:
            creation_method: The way to provision new NetCloud Accounts.
            email_fixture: An EmailFixture object for generating email addresses and activation via invitation email.
            use_activation_email: Use the EmailFixture to find and open the activation email and retrieve the password token.
            ncm_api_protocol: The protocol for the NCM service
            ncm_api_hostname: The hostname for the NCM service
            ncm_api_port: The port for the NCM service
            accounts_api_protocol: The protocol for the accounts service
            accounts_api_hostname: The hostname for the accounts service
            accounts_api_port: The port for the accounts service
            accounts_privileged_username: Typically the Service Account username.
                Required for provisioning accounts via CreationMethod.ACCOUNTS.
            accounts_privileged_password: Typically the Service Account password.
                Required for provisioning accounts via CreationMethod.ACCOUNTS.
            timeout: REST API Client request timeout in seconds.
        """
        self.creation_method = creation_method
        self.use_activation_email = use_activation_email
        self.email_fixture = email_fixture
        self.config_fixture = config_fixture

        self.ncm_api_protocol = ncm_api_protocol
        self.ncm_api_hostname = ncm_api_hostname
        self.ncm_api_port = ncm_api_port
        self.ncm_api_id = ncm_api_id
        self.ncm_api_token = ncm_api_token

        self.sf_account = None
        if creation_method == CreationMethod.SALESFORCE:
            self.salesforce_fixture = _SalesforceConnection(config_fixture)
            self.salesforce_fixture.establish_connection()
            self.sf_account = None
        elif creation_method == CreationMethod.SALESFORCE_MOCK:
            self.bsf_sfconnector_fixture = SFConnectorSession(config_fixture)
            self.salesforce_fixture = None  # So we can know not to tear it down at the end
            self.sf_account = None
        else:
            self.salesforce_fixture = None  # So we can know not to tear it down at the end

        if creation_method == CreationMethod.ACCOUNTS:
            if not accounts_api_protocol:
                raise ValueError("accounts_api_protocol cannot be None with creation_method=CreationMethod.ACCOUNTS")
            if not accounts_api_hostname:
                raise ValueError("accounts_api_hostname cannot be None with creation_method=CreationMethod.ACCOUNTS")
            if not accounts_api_port:
                raise ValueError("accounts_api_port cannot be None with creation_method=CreationMethod.ACCOUNTS")
            if not accounts_privileged_username:
                raise ValueError("accounts_privileged_username cannot be None with creation_method=CreationMethod.ACCOUNTS")
            if not accounts_privileged_password:
                raise ValueError("accounts_privileged_password cannot be None with creation_method=CreationMethod.ACCOUNTS")

        self.accounts_api_protocol = accounts_api_protocol
        self.accounts_api_hostname = accounts_api_hostname
        self.accounts_api_port = accounts_api_port
        self.accounts_privileged_username = accounts_privileged_username
        self.accounts_privileged_password = accounts_privileged_password

        self.timeout = timeout

    def create(self,
               activate: Union[ActivationMethod, str, bool] = ActivationMethod.API,
               accept_tos: Union[AcceptTOSMethod, str, bool] = AcceptTOSMethod.API,
               name: str = None,
               password: str = "Password1!",
               entitlements: List[str] = None,
               sf_optional_params: dict() = None,
               validate_product: bool = True) -> 'NCA.NetCloudActor':
        """Create a new NetCloud Account and return the new NetCloud Account Admin

        Args:
            activate: Activate the account via the activation email.
            accept_tos: Accept the terms of service. (assumes activate is truthy)
            name: The name to call the newly created account.
            password: If we activate, this will be set as the password for the NetCloud Account Admin.
            entitlements: AKA subscriptions. Only used if creation method is Salesforce or Salesforce_Mock.
                Defaults to ["ECM_PRIME_1YR","NCE_GW_STANDARD_1YR"] if not specified.
            sf_optional_params: Any other fields to be populated in Salesforce during creation

        Returns:
            The admin of the account as a NetCloudActor.
        """
        return self.create_account(
            activate=activate,
            accept_tos=accept_tos,
            admin_password=password,
            name=name,
            entitlements=entitlements,
            sf_optional_params=sf_optional_params,
            validate_product=validate_product).admin

    def create_random_account(self,
                              activate: Union[ActivationMethod, str, bool] = ActivationMethod.API,
                              accept_tos: Union[AcceptTOSMethod, str, bool] = AcceptTOSMethod.API,
                              admin_password: str = "Password1!",
                              entitlements: List[str] = None) -> 'NCA.NetCloudAccount':
        """Create a random account.  Mostly for Ease of Use.

        Args:
            activate: Activate the account via the activation email via API by default.
            accept_tos: Accept the terms of service using the provided method. The default is via API.
            activate must evaluate to True, which by default, it is .
            admin_password: Password to use to active the admin.
            entitlements: AKA subscriptions. Only used if creation method is Salesforce or Salesforce_Mock.
                          Defaults to ["ECM_PRIME_1YR","NCE_GW_STANDARD_1YR"] if not specified.

        Returns:
            The created account with a random admin and random account name.
        """
        return self.create_account(
            activate=activate, accept_tos=accept_tos, admin_password=admin_password, entitlements=entitlements)

    def create_account(self,
                       name: str = None,
                       activate: Union[ActivationMethod, str, bool] = ActivationMethod.API,
                       accept_tos: Union[AcceptTOSMethod, str, bool] = AcceptTOSMethod.API,
                       admin_username: str = None,
                       admin_first_name: str = None,
                       admin_last_name: str = None,
                       admin_password: str = "Password1!",
                       entitlements: List[str] = None,
                       sf_optional_params: dict() = None,
                       validate_product: bool = True) -> 'NCA.NetCloudAccount':
        """Create a new NetCloud Account and return the new NetCloud Account

        Args:
            name: Create the account with the given name.  Otherwise the account name will consist of the admin's first and
            last names.
            activate: Activate the account via the activation email.
            accept_tos: Accept the terms of service. (assumes activate is truthy)
            admin_username: The username of the pre-existing admin or one to create with this username
            admin_first_name: The first name of the admin to create.
            admin_last_name: The last name of the admin to create.  admin_username must be provided.
            admin_password: If activate, this will be set as the password for the NetCloud Account Admin.
            entitlements: AKA subscriptions. Only used if creation method is Salesforce or Salesforce_Mock.
                Defaults to ["ECM_PRIME_1YR","NCE_GW_STANDARD_1YR"] if not specified.
            sf_optional_params: Any other fields to be populated in Salesforce during creation

        """
        activate = ActivationMethod(activate.lower()) if isinstance(activate, str) else activate
        accept_tos = AcceptTOSMethod(accept_tos.lower()) if isinstance(accept_tos, str) else accept_tos

        if accept_tos and not self.ncm_api_hostname:
            raise ValueError("ncm_api_hostname required on init if accept_tos=True")
        if accept_tos and not self.ncm_api_protocol:
            raise ValueError("ncm_api_protocol required on init if accept_tos=True")
        if accept_tos and not self.ncm_api_port:
            raise ValueError("ncm_api_port required on init if accept_tos=True")
        if accept_tos and not activate:
            raise ValueError("A new NetCloud Account Admin cannot accept the TOS if he does not activate.")
        if entitlements is not None and self.creation_method is CreationMethod.ACCOUNTS:
            logger.warning("Passing in entitlements to create() will do nothing if creation method is ACCOUNTS.")
        # Default entitlement that will work for most tests
        entitlements = entitlements or ["ECM_PRIME_1YR", "NETCLOUD_BRANCH_ROUTERS_1YR", "NCP_ENABLE"]

        return NCA.NetCloudAccount(
            netcloud_account_fixture=self,
            name=name,
            activate=activate,
            accept_tos=accept_tos,
            admin_username=admin_username,
            admin_first_name=admin_first_name,
            admin_last_name=admin_last_name,
            admin_password=admin_password,
            entitlements=entitlements,
            sf_optional_params=sf_optional_params,
            validate_product=validate_product)

    def get_account_by_admin_creds(self, username: str, password: str) -> 'NCA.NetCloudAccount':
        """Get an existing account using the admins username and password.

        Args:
            username: Username of a pre-existing admin
            password: Password of a pre-existing admin

        Returns:
            The account the given username and password is an admin of.
        """
        return NCA.NetCloudAccount(
            netcloud_account_fixture=self,
            creation_method=CreationMethod.PREDEFINED,
            admin_username=username,
            admin_password=password)

    def get_account_by_actor(self, netcloud_actor: 'NCA.NetCloudActor') -> 'NCA.NetCloudAccount':
        """Get an existing account using the admins username and password.

        Args:
            netcloud_actor: The NetCloudActor that has at least a username and password of a pre-existing admin.

        Returns:
            The account the actor is an admin of.
        """
        return NCA.NetCloudAccount(
            netcloud_account_fixture=self,
            creation_method=CreationMethod.PREDEFINED,
            admin_username=netcloud_actor.username,
            admin_password=netcloud_actor.password)

    def get_predefined_account(self, name: str = 'sso') -> 'NCA.NetCloudAccount':
        """Get the account associated with the credentials under the target{predefinedAuth{name.. in the target conf file.

        Args:
            name: The name under the predefinedAuth section of the target configuration file.

        Returns:
            The NetCloudAccount associated with that predefined admin.
        """
        username = self.config_fixture['target']['predefinedAuth'][name]['username']
        password = self.config_fixture['target']['predefinedAuth'][name]['password']
        return self.get_account_by_admin_creds(username, password)

    def is_user(self, username: str) -> bool:
        """Return whether a user exists in Rome.

        Args:
            username: The username (email address) of the user to check.

        Returns:
            True if they exist
        """
        ncm_rest = NCMv1RESTClient(
            hostname=self.ncm_api_hostname, protocol=self.ncm_api_protocol, port=self.ncm_api_port, timeout=self.timeout)
        r = ncm_rest.get(
            "/api/v1/users?email={}".format(username),
            headers={'Authorization': "SecurityToken {}:{}".format(self.ncm_api_id, self.ncm_api_token)})
        if r.status_code != 200:
            raise UnexpectedStatusError(msg=r.text, response=r)
        return r.json()['data'][0]['email'] == username

    def get_failure_report(self):
        report = str(self)
        report += '\nCreation Method: {}\n'.format(self.creation_method.value)
        report += 'NCM API Root: {}://{}:{}\n'.format(self.ncm_api_protocol, self.ncm_api_hostname, self.ncm_api_port)
        report += 'Use Activation Email: {}\n'.format(str(self.use_activation_email))
        if self.sf_account:
            report += 'SF Account ID: {}\n'.format(self.sf_account.sf_id)
        elif self.creation_method == CreationMethod.ACCOUNTS:
            report += 'Accounts API: {}://{}:{}\n'.format(self.accounts_api_protocol, self.accounts_api_hostname,
                                                          self.accounts_api_port)
            report += 'Accounts privileged credentials:\nUsername: {}\nPassword: {}\n'.format(
                self.accounts_privileged_username, self.accounts_privileged_password)

        return report

    def teardown(self):
        if self.salesforce_fixture:
            self.salesforce_fixture.teardown()


if __name__ == "__main__":
    logFormatter = logging.Formatter("%(asctime)s [%(name)-24.24s] [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)
    rootLogger.setLevel(logging.DEBUG)
    # BEGIN PYTEST FIXTURE CODE
    # @pytest.yield_fixture
    # def netcloud_account_fixture(config_fixture, email_fixture)
    email = EmailFixture(
        service=EmailService.MAILHOG, api_root="https://mailhog-mnorman-77.ncm.public.aws.cradlepointecm.com/")

    netcloud_account = NetCloudAccountFixture(
        creation_method=CreationMethod.ACCOUNTS,
        email_fixture=email,
        ncm_api_protocol="https",
        ncm_api_hostname="ncm-mnorman-77.ncm.public.aws.cradlepointecm.com",
        ncm_api_port=443,
        ncm_api_id="api_id",
        ncm_api_token="api_token",
        accounts_api_protocol="https",
        accounts_api_hostname="accounts-mnorman-77.ncm.public.aws.cradlepointecm.com",
        accounts_api_port=443,
        accounts_privileged_username="localServiceAccount@cradlepoint.com",
        accounts_privileged_password="Password1!",
        timeout=60.0)
    # END PYTEST FIXTURE CODE

    account_admin = netcloud_account.create()
    print(account_admin)
