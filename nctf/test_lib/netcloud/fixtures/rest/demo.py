from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture

if __name__ == "__main__":
    import random
    from pprint import pprint

    arg_sets = [('https', 'accounts-qa2.cradlepointecm.com', 443,
                 'https', 'qa2.cradlepointecm.com', 443,
                 'https', 'api-overlay-qa2.cradlepointecm.com', 443)]

    cred_sets = [('cp.jrny@gmail.com', 'Quality805!')]

    for args, creds in zip(arg_sets, cred_sets):
        factory = NetCloudRESTFixture(*args)
        netcloud = factory.get_client()
        netcloud.authenticate(*creds)
        assert netcloud.authenticated()

        # Accounts Service
        accounts_users = netcloud.accounts.users.list().json()
        accounts_accounts = netcloud.accounts.accounts.list().json()
        accounts_roles = netcloud.accounts.roles.list().json()

        # NetCloud Manager
        ncm_accounts = netcloud.ncm.accounts.list().json()
        ncm_users = netcloud.ncm.users.list().json()
        ncm_products = netcloud.ncm.products.list().json()
        ncm_firmwares = netcloud.ncm.firmwares.list().json()
        firmware = random.choice(ncm_firmwares['data'])

        account_id = ncm_accounts['data'][0]['id']
        product_id = firmware['product'].split('/')[-2]
        firmware_id = firmware['id']
        group = netcloud.ncm.groups.create('herbert', account_id, product_id, firmware_id).json()
        group_id = group['data']['id']
        pprint(group_id)

        r = netcloud.ncm.groups.update(group_id, name="island")
        pprint(r)
        r = netcloud.ncm.groups.delete(group_id)
        pprint(r)

        # NetCloud Perimeter
        r = netcloud.papi.devices.list()
        pprint(r)

        r = netcloud.papi.networks.list()
        pprint(r)

        r = netcloud.papi.users.list()
        pprint(r)


def test_kube_l3_foo(netcloud_rest_fixture, netcloud_account_fixture):
    new_admin = netcloud_account_fixture.create(accept_tos=True, activate=True)
    netcloud_api = netcloud_rest_fixture.get_client()
    netcloud_api.authenticate(new_admin.username, new_admin.password)

    assert netcloud_api.ncm.groups.list().json()