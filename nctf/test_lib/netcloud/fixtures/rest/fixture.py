import logging

from requests import Response
from requests import Session

from nctf.test_lib.accounts.rest import AccServRESTClient
from nctf.test_lib.als.rest import ALSv1RESTClient
from nctf.test_lib.aoobm.rest import Overmindv1RESTClient
from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.ncm.rest.v2_client import NCMv2RESTClient
from nctf.test_lib.ncp.rest import PAPIv1RESTClient
from nctf.test_lib.netcloud.fixtures.netcloud_account.actor import NetCloudActor
from nctf.test_lib.pdp.rest import PDPv1RESTClient

logger = logging.getLogger('test_lib.netcloud.rest')


class NetCloudRESTFixtureError(Exception):
    pass


class NetCloudRESTClient(object):
    """Composite class to simplify interaction with all NetCloud REST APIs."""

    def __init__(self,
                 accounts_protocol: str = None,
                 accounts_hostname: str = None,
                 accounts_port: int = None,
                 ncm_protocol: str = None,
                 ncm_hostname: str = None,
                 ncm_port: int = None,
                 papi_protocol: str = None,
                 papi_hostname: str = None,
                 papi_port: int = None,
                 als_protocol: str = None,
                 als_hostname: str = None,
                 als_port: int = None,
                 overmind_protocol: str = None,
                 overmind_hostname: str = None,
                 overmind_port: int = None,
                 pdp_protocol: str = None,
                 pdp_hostname: str = None,
                 pdp_port: str = None,
                 x_cp_api_id: str = None,
                 x_cp_api_key: str = None):

        self.session = Session()
        self._x_cp_api_id = x_cp_api_id
        self._x_cp_api_key = x_cp_api_key

        self._accounts = None
        self._ncm = None
        self._ncmv2 = None
        self._papi = None
        self._als = None
        self._overmind = None
        self._pdp = None

        if accounts_protocol and accounts_hostname and accounts_port:
            self._accounts = AccServRESTClient(accounts_protocol, accounts_hostname, accounts_port, session=self.session)
        else:
            logger.warning("AccServRESTClient will not be initialized.")

        if ncm_protocol and ncm_hostname and ncm_port:
            self._ncm = NCMv1RESTClient(ncm_protocol, ncm_hostname, ncm_port, session=self.session)
            if self._x_cp_api_id and self._x_cp_api_key:
                self._ncmv2 = NCMv2RESTClient(
                    protocol=self.ncm.protocol,
                    hostname=self.ncm.hostname,
                    port=self.ncm.port,
                    x_cp_api_id=self._x_cp_api_id,
                    x_cp_api_key=self._x_cp_api_key,
                    session=self.session)
            else:
                logger.warning("NCMv2RESTClient will not be initialized.")
        else:
            logger.warning("NCMv1RESTClient will not be initialized.")

        if papi_protocol and papi_hostname and papi_port:
            self._papi = PAPIv1RESTClient(papi_protocol, papi_hostname, papi_port, session=self.session)
        else:
            logger.warning("PAPIv1RESTClient will not be initialized.")

        if als_protocol and als_hostname and als_port:
            self._als = ALSv1RESTClient(als_protocol, als_hostname, als_port, session=self.session)
        else:
            logger.warning("ALSv1RESTClient will not be initialized.")

        if overmind_protocol and overmind_hostname and overmind_port:
            self._overmind = Overmindv1RESTClient(overmind_protocol, overmind_hostname, overmind_port, session=self.session)
        else:
            logger.warning("Overmindv1RESTClient will not be initialized.")

        if pdp_protocol and pdp_hostname and pdp_port:
            self._pdp = PDPv1RESTClient(pdp_protocol, pdp_hostname, pdp_port, session=self.session)
        else:
            logger.warning("PDPv1RESTClient will not be initialized.")

    @property
    def als(self) -> ALSv1RESTClient:
        if self._als:
            return self._als
        else:
            raise NetCloudRESTFixtureError("ALSv1RESTClient has not been initialized.")

    @property
    def accounts(self) -> AccServRESTClient:
        if self._accounts:
            return self._accounts
        else:
            raise NetCloudRESTFixtureError("AccServRESTClient has not been initialized.")

    @property
    def ncm(self) -> NCMv1RESTClient:
        if self._ncm:
            return self._ncm
        else:
            raise NetCloudRESTFixtureError("NCMv1RESTClient has not been initialized.")

    @property
    def ncmv2(self) -> NCMv2RESTClient:
        if self._ncmv2:
            return self._ncmv2
        else:
            raise NetCloudRESTFixtureError("NCMv2RESTClient has not been initialized.")

    @property
    def overmind(self) -> Overmindv1RESTClient:
        if self._overmind:
            return self._overmind
        else:
            raise NetCloudRESTFixtureError("Overmindv1RESTClient has not been initialized.")

    @property
    def papi(self) -> PAPIv1RESTClient:
        if self._papi:
            return self._papi
        else:
            raise NetCloudRESTFixtureError("PAPIv1RESTClient has not been initialized.")

    @property
    def pdp(self) -> PDPv1RESTClient:
        if self._pdp:
            return self._pdp
        else:
            raise NetCloudRESTFixtureError("PDPv1RESTClient has not been initialized.")

    def authenticate(self, username, password) -> Response:
        """Authenticate with the NetCloud Solution via Accounts Service"""
        return self.accounts.authenticate(username, password)

    def authenticate_actor(self, actor: NetCloudActor) -> Response:
        """Authenticate an actor with the NetCloud Solution via Accounts Service"""
        return self.accounts.authenticate(actor.username, actor.password)

    def authenticated(self) -> bool:
        """Verify authentication with the NetCloud Solution via Accounts Service"""
        return self.accounts.authenticated()

    def authenticate_ncmv2(self, role: str = 'admin') -> Response:
        """Authenticate to ncmv2. Role choices: read_only, full_access, admin"""
        if self.authenticated():
            return self.ncmv2.authenticate(ncm_v1_rest_client=self.ncm, role=role)
        else:
            raise NetCloudRESTFixtureError('Must be authenticated through NCM v1 before authenticating to NCM v2')

    def authenticated_ncmv2(self) -> bool:
        return self.ncmv2.authenticated()


class NetCloudRESTFixture(FixtureBase):
    """Factory class to handle simplified construction of NetCloud REST Fixtures"""

    def __init__(self,
                 accounts_protocol: str = None,
                 accounts_hostname: str = None,
                 accounts_port: int = None,
                 ncm_protocol: str = None,
                 ncm_hostname: str = None,
                 ncm_port: int = None,
                 papi_protocol: str = None,
                 papi_hostname: str = None,
                 papi_port: int = None,
                 als_protocol: str = None,
                 als_hostname: str = None,
                 als_port: int = None,
                 overmind_protocol: str = None,
                 overmind_hostname: str = None,
                 overmind_port: int = None,
                 pdp_protocol: str = None,
                 pdp_hostname: str = None,
                 pdp_port: int = None,
                 x_cp_api_id: str = None,
                 x_cp_api_key: str = None):

        self.accounts_protocol = accounts_protocol
        self.accounts_hostname = accounts_hostname
        self.accounts_port = accounts_port

        self.ncm_protocol = ncm_protocol
        self.ncm_hostname = ncm_hostname
        self.ncm_port = ncm_port

        self.papi_protocol = papi_protocol
        self.papi_hostname = papi_hostname
        self.papi_port = papi_port

        self.als_protocol = als_protocol
        self.als_hostname = als_hostname
        self.als_port = als_port

        self.overmind_protocol = overmind_protocol
        self.overmind_hostname = overmind_hostname
        self.overmind_port = overmind_port

        self.pdp_protocol = pdp_protocol
        self.pdp_hostname = pdp_hostname
        self.pdp_port = pdp_port

        self.x_cp_api_id = x_cp_api_id
        self.x_cp_api_key = x_cp_api_key

    def get_client(self, actor: NetCloudActor = None) -> NetCloudRESTClient:
        """ Returns a rest client

        args:
            actor: An optional valid NetCloud actor to authentic with before returning the client

        returns: NetCloudRESTClient

        """
        client = NetCloudRESTClient(self.accounts_protocol, self.accounts_hostname, self.accounts_port, self.ncm_protocol,
                                    self.ncm_hostname, self.ncm_port, self.papi_protocol, self.papi_hostname, self.papi_port,
                                    self.als_protocol, self.als_hostname, self.als_port, self.overmind_protocol,
                                    self.overmind_hostname, self.overmind_port, self.pdp_protocol, self.pdp_hostname,
                                    self.pdp_port, self.x_cp_api_id, self.x_cp_api_key)
        if actor is not None:
            client.authenticate_actor(actor)
        return client

    def teardown(self):
        pass

    def get_failure_report(self):
        pass
