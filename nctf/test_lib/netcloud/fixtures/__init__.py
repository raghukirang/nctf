from .netcloud_account import fixture
from .netcloud_client import fixture
from .netcloud_network import fixture
from .netcloud_router import fixture
