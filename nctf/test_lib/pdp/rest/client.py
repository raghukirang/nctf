"""PDP REST API Client."""
import datetime
import logging

import pytz
from requests import Response
from requests import Session
from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
import nctf.test_lib.base.rest as rest


logger = logging.getLogger('test_lib.pdp.rest')

DATA_PLANS_ENDPOINT = '/api/v1/data_plans'
NET_DEVICE_METRICS_ENDPOINT = '/api/v1/net_device_metrics'
ALLOWED_SITES_ENDPOINT = '/api/v1/allowed_sites'
BLOCKED_SITES_ENDPOINT = '/api/v1/blocked_sites'

# Example:
# https://api-pdp-qa1.cradlepointecm.com/api/v1/net_device_metrics/
#   ?fields=usage_total
#   &group_by=carrier_name%2Crouter_id%2Crouter_name
#   &sort=-usage_total
#   &parentAccount=34655
#   &page%5Blimit%5D=25
#   &page%5Boffset%5D=0
#   &end_time=2019-01-28T00%3A00%3A00%2B00%3A00
#   &start_time=2019-01-01T00%3A00%3A00%2B00%3A00
#   &filter%5Bcarrier_name%5D=VERIZON


class PDPv1RESTClient(RESTClient):
    """Activity Log Service REST API client."""

    DEFAULT_HEADERS = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/vnd.api+json'
    }

    def __init__(self, protocol: str, hostname: str, port: int,
                 timeout: float = 30.0, session: Session = None):
        """Initialize Endpoint and REST client."""
        super().__init__(protocol, hostname, port, timeout, session)

        self.data_plans = DataPlansEndpoint(self, DATA_PLANS_ENDPOINT, enforce_trailing_slash=True)
        self.net_device_metrics = NetDeviceMetricsEndpoint(self, NET_DEVICE_METRICS_ENDPOINT, enforce_trailing_slash=True)
        self.allowed_sites = AllowedSitesEndpoint(self, ALLOWED_SITES_ENDPOINT, enforce_trailing_slash=True)
        self.blocked_sites = BlockedSitesEndpoint(self, BLOCKED_SITES_ENDPOINT, enforce_trailing_slash=True)

    def authenticate(self, *args, **kwargs):
        pass


class PDPv1Endpoint(RESTEndpoint):
    """V1 of the PDP Endpoint."""

    @rest.checked_status(200)
    def detail(self, resource_id: str, *args, **kwargs) -> Response:
        return self._get(resource_id, *args, **kwargs)

    @rest.checked_status(200)
    def list(self, *args, **kwargs) -> Response:
        return self._get(*args, **kwargs)

    @rest.checked_status(204)
    def delete(self, resource_id: str, *args, **kwargs) -> Response:
        return self._delete(resource_id, *args, **kwargs)

    @rest.checked_status(201)
    def create(self, *args, **kwargs) -> Response:
        return self._post(*args, **kwargs)

    @staticmethod
    def timestamp(time_span_days: int = 0, include_tz: bool = True, timezone: str = None) -> str:
        """Return formatted timestamp from the requested number of days prior.

        Args:
            time_span_days: When specifying a previous timestamp, the amount of days back
            you wish to cover. 0 if you want the current time.
            timezone: Region/City for your location, contained within pytz.all_timezones
            include_tz: Adjusts the timestamp to the requested timezone, defaults to Boise time.

        Returns:
            A properly formatted timezone string that can be passed to the PDP Service.

        Notes:
            You must always pass in a start_time and end_time when making calls to PDP.
            The timestamp method will return a value based on the requested span of days
            from the current time.
            You can pass in a negative integer to represent tomorrow, as the PDP end_time
            defaults to tomorrow.

        """
        timezone = timezone if timezone else 'America/Boise'
        if timezone not in pytz.all_timezones:
            raise ValueError("Invalid timezone argument.")

        if include_tz:
            tz = pytz.timezone(timezone)
            dt = tz.localize(datetime.datetime.utcnow().replace(microsecond=0) - datetime.timedelta(days=time_span_days))
        else:
            dt = datetime.datetime.utcnow().replace(microsecond=0) - datetime.timedelta(days=time_span_days)

        timestamp = dt.isoformat()
        return timestamp


class DataPlansEndpoint(PDPv1Endpoint):
    pass


class NetDeviceMetricsEndpoint(PDPv1Endpoint):

    @checked_status(200)
    def usage_total(self, time_span_days: int = 30, *args, **kwargs) -> Response:
        end_time = self.timestamp(time_span_days=-1, include_tz=False)
        start_time = self.timestamp(time_span_days=time_span_days, include_tz=False)
        params = {
            "start_time": start_time,
            "end_time": end_time,
            "fields": "usage_total"
        }
        return self._get(params=params, *args, **kwargs)

    @checked_status(200)
    def usage_total_group_by_router(self, time_span_days: int = 30, *args, **kwargs) -> Response:
        end_time = self.timestamp(time_span_days=-1, include_tz=False)
        start_time = self.timestamp(time_span_days=time_span_days, include_tz=False)
        params = {
            "start_time": start_time,
            "end_time": end_time,
            "fields": "usage_total",
            "group_by": "router_id"
        }
        return self._get(params=params, *args, **kwargs)

    @checked_status(200)
    def usage_total_filter_by_router_id(self, router_id: str, time_span_days: int = 30, *args, **kwargs) -> Response:
        end_time = self.timestamp(time_span_days=-1, include_tz=False)
        start_time = self.timestamp(time_span_days=time_span_days, include_tz=False)
        params = {
            "start_time": start_time,
            "end_time": end_time,
            "fields": "usage_total",
            "filter[router_id]": router_id
        }
        return self._get(params=params, *args, **kwargs)


class AllowedSitesEndpoint(PDPv1Endpoint):
    pass


class BlockedSitesEndpoint(PDPv1Endpoint):
    pass
