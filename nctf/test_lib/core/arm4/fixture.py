import logging
import os
import random
from typing import List
from typing import Tuple
from typing import Union

from arm4.client import ARMv4Client
from arm4.serialization import CloudRouter as CloudRouterDataObj
from arm4.serialization import Router as RouterDataObj
from arm4.serialization import RouterControl as RouterCtrlDataObj
import requests

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.ncos.base import NCOSLANClient
from nctf.test_lib.ncos.base import NCOSNetworkProvider
from nctf.test_lib.ncos.base import NCOSRouter
from nctf.test_lib.ncos.base import NCOSWiFiClient
from nctf.test_lib.ncos.base.capability import NCOSRouterCapabilities
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwGetter
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2

logger = logging.getLogger('test_lib.core.arm4')


class ARMv4Exception(Exception):
    pass


class ARMv4LanClient(NCOSLANClient):
    def __init__(self, network_client_data: RouterCtrlDataObj):
        super().__init__()
        self.data = network_client_data
        self.linux_namespace = network_client_data.linux_namespace
        self.id = network_client_data.id
        # for better future compatibility with virtnet/phynet, this does not have a framework provided router
        # self.router_id = None
        self.ssh_port = network_client_data.ssh_port
        self.ssh_host = network_client_data.ssh_host
        self.ssh_user = network_client_data.ssh_user
        self.ssh_pass = network_client_data.ssh_pass
        # don't publish this until we know what is going to be needed for physnet
        # self.default_vlan_id = network_client_data.default_vlan_id
        # self.default_vlan_id_tagged = network_client_data.default_vlan_id_tagged
        # self.router_control_ports = network_client_data.router_control_ports


class ARMv4WiFiClient(NCOSWiFiClient):
    def __init__(self, network_client_data: RouterCtrlDataObj):
        self.data = network_client_data
        # things that make a wifi client unique or interesting go here.  (what are those? Is this YAGNI?)


class ARMv4Router(NCOSRouter):
    def __init__(self, router_data: Union[RouterDataObj, CloudRouterDataObj], fw_getter: FwGetter = None):
        self.data = router_data
        self.fw_getter = fw_getter
        self.router_id = self.data.id
        self.capability_id = self.data.capability_id
        if self.capability_id == '3200v':
            super().__init__(
                remote_admin_host=self.data.remote_admin_host,
                remote_admin_port=self.data.remote_admin_port,
                remote_admin_ssh_port=self.data.ssh_port,
                admin_user=self.data.admin_user,
                admin_password=self.data.admin_pass,
                cproot_password=self.data.cp_root_pass,
                default_lan_ip=self.data.capability.default_lan_ip,
                product=self.capability_id.lower())
        else:
            super().__init__(
                remote_admin_host=self.data.remote_admin_host,
                remote_admin_port=self.data.remote_admin_port,
                remote_admin_ssh_port=self.data.remote_admin_ssh_port,
                admin_user=self.data.admin_user,
                admin_password=self.data.admin_pass,
                cproot_password=self.data.cp_root_pass,
                default_lan_ip=self.data.capability.default_lan_ip,
                product=self.capability_id.lower())

    def get_router_capabilities(self, *args, **kwargs) -> NCOSRouterCapabilities:
        """Get the device capabilities. e.g. {'lan_ports': 4}"""
        capabilities = NCOSRouterCapabilities()
        capabilities.load_from_capabilities(self.data.capability)
        return capabilities

    def load_fw(self, protocol: str = 'https'):
        """Backwards compatibility.  Users should use loader.load_fw instead."""
        if self.get_router_capabilities().default_lan_only:
            # there is a chance this might work in some corner cases, so we're logging an error, but not raising one.
            # How might it work? If a test router has USB ethernet adapter connected and configured as WAN.
            # Could also work if router has a LAN port reconfigured as WAN (can happen with CBA850).
            logger.warning("router.load_fw() doesn't work with lan-only routers. use loader.load_fw() instead.")

        if self.fw_getter is None:
            raise ARMv4Exception(f"Unable to load FW to {self}. fw_getter was never set!")
        loader = FwLoaderFixtureV2(fw_getter=self.fw_getter)
        loader.load_fw(router=self, protocol=protocol)


class ARMv4Fixture(NCOSNetworkProvider, FixtureBase):
    def __init__(self,
                 default_reason: str,
                 host: str,
                 api_key: str,
                 port: int = 443,
                 default_pool_id: str = None,
                 default_fw_getter: FwGetter = None):
        """

        Args:
            host: Hostname including protocol. e.g. "https://testinfra-arm.private.aws.cradlepointecm.com/api/v4/"
            api_key: Your ARMv4 API key that allows you to lease devices.
            port: The port. Typically 443 or 80.
            default_pool_id: The default pool to lease resources from.

        """
        self.arm_client = ARMv4Client(hostname=host, api_key=api_key, port=port, default_pool_id=default_pool_id)
        self.default_fw_getter = default_fw_getter
        self.routers = dict()  # using a dict for these prevents us from trying to clean more than once
        self.cloud_routers = dict()
        self.lanclients = dict()
        self.default_reason = default_reason

    def lease_router_by_product_name(self, product_name: str, reason: str = "") -> ARMv4Router:
        """Lease a router by its product name.

        Args:
            product_name: e.g. AER2100, IBR1700, AP22, CVR
            reason: Reason for leasing the device. Typically the name of the test.

        """
        return self.lease_router(product_name=product_name, reason=reason or self.default_reason)

    def lease_router_by_tag(self, tags: List[str], reason: str = "") -> ARMv4Router:
        """Lease a router by its tags

        Note: A list of more than 1 tag uses AND logic.
            e.g. tags=['foo', 'bar'] => routers with tags 'foo' AND 'bar'

        """
        return self.lease_router(tags=tags, reason=reason or self.default_reason)

    def lease_router(self, product_name: str = None, tags: List[str] = None, capabilities: dict = None,
                     reason: str = "") -> ARMv4Router:
        logger.info(f"ARM4 lease reason: {reason}")

        if product_name in ['3200v', 'CVR']:
            # tags and capabilities are not supported for cloud_routers (yet?)
            routers_data = self.arm_client.cloud_routers.list(clean=True, healthy=True)
            if len(routers_data) == 0:
                raise ARMv4Exception(f"No Routers available with product name: {product_name}")

            router_data = random.choice(routers_data)
            return self.lease_cloud_router_by_id(router_data.id, reason=reason or self.default_reason)
        else:
            routers_data = self.arm_client.routers.list(
                clean=True, healthy=True, router_capability_id=product_name, router_capabilities=capabilities, tags=tags)
            if len(routers_data) == 0:
                raise ARMv4Exception(f"No Routers available with product name: {product_name},"
                                     f" tags: {tags}, capabilities: {capabilities}")

            router_data = random.choice(routers_data)
            return self.lease_router_by_id(router_data.id, reason=reason or self.default_reason)

    def lease_router_by_id(self, router_id: str, reason: str = "") -> ARMv4Router:
        """Lease a router by its unique identifier

        Args:
            router_id: The ID of the router. e.g. abr1, abr2
            reason: Reason for leasing the device. Typically the name of the test.

        """
        logger.info(f"Leasing router with id={router_id}")
        router_data = self.arm_client.routers.create_lease(id_=router_id, reason=reason or self.default_reason)
        router = ARMv4Router(router_data=router_data, fw_getter=self.default_fw_getter)
        self.routers[router.data.id] = router
        return router

    def lease_cloud_router_by_id(self, cloud_router_id: str, reason: str = "") -> ARMv4Router:
        """Lease a cloud router by its unique identifier

                Args:
                    cloud_router_id: The ID of the cloud_router. e.g. i-0041c29f0de72c5b9, i-01fa7ad418c7607d3
                    reason: Reason for leasing the device. Typically the name of the test.

                """
        logger.info(f"Leasing cloud router with id={cloud_router_id}")
        router_data = self.arm_client.cloud_routers.create_lease(id_=cloud_router_id, reason=reason or self.default_reason)
        cloud_router = ARMv4Router(router_data=router_data, fw_getter=self.default_fw_getter)
        try:
            api = cloud_router.get_rest_client(protocol="https")
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as exc:
            raise ARMv4Exception(f"It appears we are unable to reach the router with error {exc}")
        response = api.get(api_uri="/api/control/system/localtime").json()
        logger.info(f"Time on cloud router = {response}")
        self.cloud_routers[cloud_router.data.id] = cloud_router
        return cloud_router

    def lease_simple_network(self,
                             product_name: str = None,
                             capabilities: dict = None,
                             tags: List[str] = None,
                             reason: str = "") -> Tuple[ARMv4Router, ARMv4LanClient]:
        """Lease a router and a single LANClient

        Args:
            product_name: e.g. AER2100, IBR1700, AP22, aka the 'capability_id' in ARMv4
            capabilities: The ability of the product, e.g. lan_ports=2 or qos=false
            tags: Arbitrary tags added to individual routers
            reason: Reason for leasing the device. Typically the name of the test.

        Note: This function currently uses the RouterControl as the Network Client.
        In the future we'll use VLAN tagging to create a Network Client that isn't the Router Control.

        """
        if product_name in ['3200v', 'CVR']:
            raise ARMv4Exception(f"lease_simple_network not able to handle {product_name} at this time")
        else:
            logger.info(f"Leasing simple network with router with product name {product_name}")

            router = self.lease_router(
                product_name=product_name, capabilities=capabilities, tags=tags, reason=reason or self.default_reason)
            router_control_data = router.data.router_control
            network_client = ARMv4LanClient(router_control_data)
            router.default_client = network_client

            return router, network_client

    def lease_simple_network_by_id(self, router_id: str, reason: str = "") -> Tuple[ARMv4Router, ARMv4LanClient]:
        """Lease a router and a single LANClient

        Args:
            router_id: The ID of the router. e.g. abr1, abr2
            reason: Reason for leasing the device. Typically the name of the test.

        Note: This function currently uses the RouterControl as the Network Client.
        In the future we'll use VLAN tagging to create a Network Client that isn't the Router Control.

        """
        logger.warning("Method currently only works for physical device, will not work for cloud routers")
        logger.info(f"Leasing simple network with router id {router_id}")

        router = self.lease_router_by_id(router_id=router_id)
        router_control_data = router.data.router_control
        network_client = ARMv4LanClient(router_control_data)

        return router, network_client

    def get_simple_network(self, *args, **kwargs):
        """Deliver a simple network with a single NCOSRouter with a single NCOSLanClient

        Note: This fulfills NCOSNetworkProvider interface requirement.

        """
        product_name = kwargs.get('product_name', None)
        reason = kwargs.get('reason', None)

        self.lease_simple_network(product_name=product_name, reason=reason or self.default_reason)

    def renew_lease(self, router: ARMv4Router, reason: str = None):
        if router.capability_id == '3200v':
            if not reason:
                lease_data = self.arm_client.cloud_routers.detail(id_=router.router_id).lease
                reason = f'Renewing lease: {lease_data["reason"]}'
            self.arm_client.cloud_routers.create_lease(id_=router.router_id, reason=reason)
            if router.data.id not in self.cloud_routers:
                self.cloud_routers[router.data.id] = router
        else:
            if not reason:
                lease_data = self.arm_client.routers.detail(id_=router.router_id).lease
                reason = f'Renewing lease: {lease_data["reason"]}'
            self.arm_client.routers.create_lease(id_=router.router_id, reason=reason)
            if router.data.id not in self.routers:
                self.routers[router.data.id] = router

    def teardown(self):
        for _, router in self.routers.items():
            logger.debug(f"Deleting lease from {router}")
            try:
                self.arm_client.routers.delete_lease(router.data.id)
            except Exception as exc:
                logger.warning(exc)
        for _, cloud_router in self.cloud_routers.items():
            logger.debug(f"Deleting lease from {cloud_router}")
            try:
                self.arm_client.cloud_routers.delete_lease(cloud_router.data.id)
            except Exception as exc:
                logger.warning(exc)

    def get_failure_report(self):
        pass

    def collect_triage_logs(self, filename_prefix: str, directory: str):
        logger.info("Making a best effort to capture the logs of all routers leased from ARMv4...")
        for _, router in self.routers.items():
            try:
                router.write_logs_to_file(
                    path_to_file=os.path.join(directory, f'{filename_prefix}_{router.router_id}.log'))
            except Exception as e:
                logger.warning(f"Exception {e} occurred when attempting to dump logs from {router}!")

        for _, cloud_router in self.cloud_routers.items():
            try:
                cloud_router.write_logs_to_file(
                    path_to_file=os.path.join(directory, f'{filename_prefix}_{cloud_router.router_id}.log'))
            except Exception as e:
                logger.warning(f"Exception {e} occurred when attempting to dump logs from {cloud_router}!")


if __name__ == "__main__":
    """Example code."""

    API_KEY = 'YourApiKeyHere'

    # Lease simple network.
    arm = ARMv4Fixture(
        default_reason="example",
        host="https://testinfra-arm.private.aws.cradlepointecm.com/",
        api_key=API_KEY,
        default_pool_id="platformcore")
    try:
        leased_router, lan_client = arm.lease_simple_network(product_name="IBR900")
        output = lan_client.exec_command("ping -c 1 google.com")
        print(output)
    except Exception as e:
        print(e)
    finally:
        arm.teardown()

    # Lease by tag.
    arm = ARMv4Fixture(
        default_reason="example",
        host="https://testinfra-arm.private.aws.cradlepointecm.com/",
        api_key=API_KEY,
        default_pool_id="platformcore")
    try:
        leased_router, lan_client = arm.lease_simple_network(tags=['test'])
        output = lan_client.exec_command("ping -c 1 google.com")
        print(output)
    except Exception as e:
        print(e)
    finally:
        arm.teardown()

    # Lease by capabilities.
    arm = ARMv4Fixture(
        default_reason="example",
        host="https://testinfra-arm.private.aws.cradlepointecm.com/",
        api_key=API_KEY,
        default_pool_id="platformcore")
    try:
        leased_router, lan_client = arm.lease_simple_network(capabilities={'ieee_802_1q': 'true'})
        output = lan_client.exec_command("ping -c 1 google.com")
        print(output)
    except Exception as e:
        print(e)
    finally:
        arm.teardown()

    # Lease cloud router.
    arm = ARMv4Fixture(
        default_reason="example",
        host="https://testinfra-arm.private.aws.cradlepointecm.com/",
        api_key=API_KEY,
        default_pool_id="testinfra")
    try:
        leased_router = arm.lease_router(product_name='3200v')
    except Exception as e:
        print(e)
    finally:
        arm.teardown()
