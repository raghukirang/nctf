import logging
from abc import ABC
from abc import abstractmethod
from typing import List

logger = logging.getLogger('test_lib.core.email')


class EmailMessage(object):
    """Common Email Message object that all inboxes will return"""

    def __init__(self, id_, sender, subject, body):
        self.id_ = id_
        self.sender = sender
        self.subject = subject
        self.body = body

    def __repr__(self):
        return "<{}(id={}, subject={})>".format(self.__class__.__name__, self.id_, self.subject)


class EmailInbox(ABC):
    """Base class for all EmailInbox types"""

    @property
    @abstractmethod
    def name(self) -> str:
        pass

    @property
    @abstractmethod
    def domain(self) -> str:
        pass

    @property
    def address(self) -> str:
        return "{}@{}".format(self.name, self.domain)

    @abstractmethod
    def get_messages(self) -> List[EmailMessage]:
        pass

    def get_messages_by_subject(self, subject: str) -> List[EmailMessage]:
        return [message for message in self.get_messages() if message.subject == subject]

    def get_messages_by_sender(self, sender: str) -> List[EmailMessage]:
        return [message for message in self.get_messages() if message.sender == sender]

    def get_messages_by_sender_and_subject(self, sender: str, subject: str) -> List[EmailMessage]:
        return [message for message in self.get_messages() if message.sender == sender and message.subject == subject]
