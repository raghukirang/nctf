import logging
from typing import List
from typing import Optional
from urllib.parse import urlparse

from marshmallow import fields
from marshmallow import post_load
from marshmallow import Schema
import requests
from requests import Response

from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
from nctf.test_lib.core.email.base import EmailInbox
from nctf.test_lib.core.email.base import EmailMessage

logger = logging.getLogger('test_lib.core.email')


class MailhogEndpoint(RESTEndpoint):
    pass


class SearchEndpoint(MailhogEndpoint):
    @checked_status(200)
    def get(self, **kwargs) -> Response:
        return self._get('', **kwargs)


class MailhogRESTClient(RESTClient):
    """Client for the Mailhog REST API"""

    def __init__(self, api_root: str, timeout: float = 30.0, session: Optional[requests.Session] = None):

        parse_result = urlparse(api_root)
        protocol = parse_result.scheme
        hostname = parse_result.hostname
        port = parse_result.port
        if port is None:
            if protocol == 'https':
                port = 443
            if protocol == 'http':
                port = 80

        super().__init__(protocol=protocol, hostname=hostname, port=port, timeout=timeout, session=session)
        self.search = SearchEndpoint(self, '/api/v2/search')

    def authenticate(self, *args, **kwargs):
        pass


class MailhogMessageSchema(Schema):
    ID = fields.String()
    From = fields.Dict()
    Content = fields.Dict()

    @post_load
    def make_object(self, data):
        id_ = data.get('ID')

        from_ = data.get('From')
        mailbox = from_.get('Mailbox')
        domain = from_.get('Domain')
        sender = "{}@{}".format(mailbox, domain)

        content = data.get('Content')
        headers = content.get('Headers')
        subject_parts = headers.get('Subject')  # This is a list for some reason?
        subject = "".join(subject_parts) if subject_parts else ""
        body = content.get('Body')

        return EmailMessage(id_, sender, subject, body)


class MailhogInbox(EmailInbox):
    def __init__(self, name: str, api_root: str, timeout: float = 30.0):
        """
        Args:
            name: This name of the inbox. i.e. name=foo => foo@mailhog.com
            api_root: api_root: Root of the REST API to be used.
            timeout: Maximum number of seconds to wait for an API call to complete.
        """
        super().__init__()
        self._name = name
        self._domain = "mailhog.com"
        self.client = MailhogRESTClient(api_root, timeout)

    @property
    def name(self) -> str:
        return self._name

    @property
    def domain(self) -> str:
        return self._domain

    def get_messages(self) -> List[EmailMessage]:
        email_json = self.client.search.get(params={"kind": "to", "query": self.address}).json()
        items_json = email_json['items']

        # The query kind "to" acts as a contains. Filter the rest.
        items_json_filtered = []
        for item in items_json:
            for to in item['To']:
                if "{}@{}".format(to["Mailbox"], to["Domain"]) == self.address:
                    items_json_filtered.append(item)
                    break

        messages = []
        for message_json in items_json_filtered:
            message = MailhogMessageSchema(strict=False).load(message_json).data
            messages.append(message)

        return messages
