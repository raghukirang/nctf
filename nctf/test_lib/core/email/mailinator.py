import logging
from typing import List
from typing import Optional
from urllib.parse import urlparse

from marshmallow import fields
from marshmallow import post_load
from marshmallow import Schema
import requests
from requests import Response

from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
from nctf.test_lib.core.email.base import EmailInbox
from nctf.test_lib.core.email.base import EmailMessage

logger = logging.getLogger('test_lib.core.email')


class MailinatorEndpoint(RESTEndpoint):
    pass


class EmailEndpoint(MailinatorEndpoint):
    @checked_status(200)
    def get(self, **kwargs) -> Response:
        return self._get('', **kwargs)


class InboxEndpoint(MailinatorEndpoint):
    @checked_status(200)
    def get(self, **kwargs) -> Response:
        return self._get('', **kwargs)


class MailinatorRESTClient(RESTClient):
    """Client for the Mailinator REST API"""

    def __init__(self, api_root: str, timeout: float = 30.0, session: Optional[requests.Session] = None):

        parse_result = urlparse(api_root)
        protocol = parse_result.scheme
        hostname = parse_result.hostname
        port = parse_result.port
        if port is None:
            if protocol == 'https':
                port = 443
            if protocol == 'http':
                port = 80

        super().__init__(protocol=protocol, hostname=hostname, port=port, timeout=timeout, session=session)
        self.inbox = InboxEndpoint(self, '/api/inbox')
        self.email = EmailEndpoint(self, '/api/email')

    def authenticate(self, *args, **kwargs):
        pass


class MailinatorMessageSchema(Schema):
    id = fields.String()
    fromfull = fields.String()
    subject = fields.String()
    parts = fields.List(fields.Dict())

    @post_load
    def make_object(self, data):
        id_ = data.get('id')
        sender = data.get('fromfull')
        subject = data.get('subject')
        body = "".join(p['body'] for p in data.get('parts'))
        return EmailMessage(id_, sender, subject, body)


class MailinatorInbox(EmailInbox):
    def __init__(self, name: str, api_root: str, token: str, timeout: float = 30.0):
        """
        Args:
            name: This name of the inbox. i.e. name=foo => foo@mailinator.com
            api_root: api_root: Root of the REST API to be used.
            token: The mailinator provided token that authorizes REST API calls.
            timeout: Maximum number of seconds to wait for an API call to complete.
        """
        super().__init__()
        self._name = name
        self._domain = "cradlepoint.mailinator.com"

        self.token = token
        self.client = MailinatorRESTClient(api_root, timeout)

    @property
    def name(self) -> str:
        return self._name

    @property
    def domain(self) -> str:
        return self._domain

    def get_messages(self) -> List[EmailMessage]:
        email_json = self.client.inbox.get(params={"to": self.name, "token": self.token, "private_domain": 'true'}).json()
        messages_json = email_json["messages"]
        ids = [m["id"] for m in messages_json]

        messages = []
        for id_ in ids:
            message_json = self.client.email.get(params={"token": self.token, "id": id_, "private_domain": 'true'}).json()
            message = MailinatorMessageSchema(strict=False).load(message_json["data"]).data
            messages.append(message)
        return messages
