from enum import Enum
import logging
import random
import string

from faker import Faker
from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.core.email.base import EmailInbox
from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.core.email.mailinator import MailinatorInbox

logger = logging.getLogger('test_lib.core.email')


class EmailService(Enum):
    MAILINATOR = "mailinator"
    MAILHOG = "mailhog"


class EmailFixture(FixtureBase):
    def __init__(self, service: EmailService, api_root: str, mailinator_token: str = None, timeout: float = 30.0):
        """
        Args:
            service: The server type we'll be talking to in order to retrieve inboxes/messages.
            api_root: e.g. https://mailinator.com
            mailinator_token: Authorization token to use mailinator's REST API.
                Required if server==EmailService.Mailinator
            timeout: The maximum amount of time an API request can take to complete.

        """
        if service == EmailService.MAILHOG:
            pass
        elif service == EmailService.MAILINATOR:
            if not mailinator_token:
                raise ValueError("mailinator_token must be provided when using EmailService.MAILINATOR")
        else:
            raise ValueError("Invalid service parameter. Expected EmailService.MAILHOG or EmailService.MAILINATOR.")

        self.service = service
        self.api_root = api_root
        self.mailinator_token = mailinator_token
        self.timeout = timeout

    def get_inbox(self, inbox_name: str = None) -> EmailInbox:
        inbox_name = inbox_name or self.get_inbox_name()

        if self.service == EmailService.MAILHOG:
            return MailhogInbox(inbox_name, self.api_root, self.timeout)
        elif self.service == EmailService.MAILINATOR:
            return MailinatorInbox(inbox_name, self.api_root, self.mailinator_token, self.timeout)

    @staticmethod
    def get_inbox_name(firstname: str = None, lastname: str = None, postfix: str = None):
        firstname = firstname if firstname else EmailFixture.get_random_firstname()
        lastname = lastname if lastname else EmailFixture.get_random_lastname()
        postfix = postfix if postfix else EmailFixture.get_random_postfix()
        return "{}_{}_{}".format(firstname.lower(), lastname.lower(), postfix.lower())

    @staticmethod
    def get_random_firstname() -> str:
        return Faker().first_name()

    @staticmethod
    def get_random_lastname() -> str:
        return Faker().last_name()

    @staticmethod
    def get_random_postfix() -> str:
        return ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(8))

    def get_failure_report(self):
        report = str(self)
        report += '\nEmail service: {}\n'.format(self.service.value)
        report += 'API Root: {}\n'.format(self.api_root)
        if self.mailinator_token:
            report += 'Mailinator Token: {}\n'.format(self.mailinator_token)

        return report

    def teardown(self):
        pass
