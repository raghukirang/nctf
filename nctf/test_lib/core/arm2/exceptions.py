class ARMException(Exception):
    pass


class ARMAPIError(ARMException):
    pass


class ARMResourceUnavailable(ARMException):
    pass


class ARMResourceUnhealthy(ARMException):
    pass
