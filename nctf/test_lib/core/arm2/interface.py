from enum import Enum
import json
import logging
import os
import random
import time
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from urllib.parse import urlparse
from urllib.parse import urlunparse

from arm2.client.client import ARMClient
from arm2.client.objects import ARMLeasedWifiClient
from arm2.client.objects import ARMWiFiClient
import requests

from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.core.arm2.exceptions import ARMAPIError
from nctf.test_lib.core.arm2.exceptions import ARMException
from nctf.test_lib.core.arm2.exceptions import ARMResourceUnavailable
from nctf.test_lib.core.arm2.exceptions import ARMResourceUnhealthy
from nctf.test_lib.core.arm2.router import ARMRouter
from nctf.test_lib.core.arm2.wifi_client import ARMWifiClient
from nctf.test_lib.core.arm2.wifi_router import ARMWifiRouter
from nctf.test_lib.libs.device_capabilities import DeviceCapabilities
from nctf.test_lib.libs.device_capabilities import DeviceCapability
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI

ROUTER_CAPABILITIES = os.path.join(os.path.dirname(__file__), 'router_capabilities.json')

logger = logging.getLogger('test_lib.core.arm')


class DevicePool(Enum):
    """Possible device pools for use.

    """
    ROUTERS = "routers"
    WIFI_ROUTERS = "wifi_routers"
    WIFI_CLIENTS = "wifi_clients"


class ARMInterface(FixtureBase):
    def __init__(self, host, port, api_path, api_key, lease_wait=30, arm_api_timeout=60, router_log_path = None):
        """

        Args:
            host (str): Host of the ARM REST API.
            port (int): Port of the ARM REST API
            api_key (str): ARM REST API auth key.
            api_path (str): path to ARM REST API base endpoint
            lease_wait (int): Maximum amount of time to wait to lease a router in seconds.
                A value <=0 will attempt only 1 time.
            arm_api_timeout (int): Maximum amount of time in seconds to wait for the ARM API to return.


        """
        if host is None:
            raise ValueError('The Automation Resource Manager (ARM) requires a valid host parameter.')
        if api_key is None:
            raise ValueError('The Automation Resource Manager (ARM) requires a valid API key to be used.')
        if api_path is None:
            raise ValueError('The Automation Resource Manager (ARM) Fixture requires a valid API path')
        if port is None:
            raise ValueError('The Automation Resource Manager (ARM) Fixture requires a valid API port')
        if not isinstance(lease_wait, int):
            raise TypeError('lease_wait must be an integer value.')
        if not isinstance(arm_api_timeout, int):
            raise TypeError('arm_api_timeout must be a non-zero non-negative integer value.')
        if arm_api_timeout <= 0:
            raise ValueError('arm_api_timeout must be a non-zero non-negative integer value.')

        self.server = f'{host}:{port}{api_path}'
        self.api_key = api_key
        self.routers = []
        self.wifi_routers = []
        self.wifi_clients = []
        self.session = requests.Session()
        self.lease_wait = lease_wait
        self.arm_api_timeout = arm_api_timeout
        self.hostname = host
        self.client = ARMClient(hostname=self.hostname, key=self.api_key, port=port)
        self.router_log_path = router_log_path

    def __str__(self):
        return "<ARMInterface(server='{}', routers='{}', wifi_clients='{}'>".format(self.server, self.routers,
                                                                                    self.wifi_clients)

    def get_router(self,
                   desired_product: str = None,
                   exclude_products: List[str] = None,
                   physical_only: bool = None,
                   reason: str = None,
                   requested_router: str = None,
                   pool: DevicePool = DevicePool.ROUTERS) -> ARMRouter:
        """

        Args:
            desired_product: The desired product type. e.g. AER3100
            exclude_products: A list of product models you would like to exclude from router selection.
                eg. ["AER3100", "AER2100"]
            physical_only: If true get any physical device. If false get a 3200v.
            reason: Reason for leasing out the router. Should typically be the name of the automated test.
            requested_router: The router_id of a specific ARM router.
            pool: The router pool used to retrieve the ARM router.

        Returns:
             ARMRouter: An interface to a leased router.

        """
        leased_router = self.lease_router(desired_product, exclude_products, physical_only, reason, requested_router, pool)
        # Create our object representation of a router leased from ARM.
        arm_router = ARMRouter(
            manager=self,
            product=leased_router['type'],
            router_id=leased_router['id'],
            mac=leased_router['mac'],
            gateway=leased_router['gateway'],
            authkey=leased_router['authkey'],
            root_authkey=leased_router['root_authkey'],
            remote_admin_port=leased_router['remote_admin_port'],
            lan_client=leased_router['lan_client'],
            ssh_port=leased_router['ssh_port'])

        # Health check the router before we give it to the user.
        self.health_check_router(arm_router)

        self.routers.append(arm_router)
        return arm_router

    def get_wifi_router(self,
                        desired_product: str = None,
                        exclude_products: List[str] = None,
                        physical_only: bool = None,
                        reason: str = None,
                        requested_router: str = None,
                        pool: DevicePool = DevicePool.WIFI_ROUTERS) -> ARMWifiRouter:
        """

        Args:
            desired_product: The desired product type. e.g. AER3100
            exclude_products: A list of product models you would like to exclude from router selection.
                eg. ["AER3100", "AER2100"]
            physical_only: If true get any physical device. If false get a 3200v.
            reason: Reason for leasing out the router. Should typically be the name of the automated test.
            requested_router: The router_id of a specific ARM router.
            pool: The router pool used to retrieve the ARM router.

        Returns:
             ARMWifiRouter: An interface to a leased router.

        """
        leased_router = self.lease_router(desired_product, exclude_products, physical_only, reason, requested_router, pool)

        # Create our object representation of a router leased from ARM.
        arm_router = ARMWifiRouter(
            manager=self,
            model=leased_router['model'],
            router_id=leased_router['id'],
            root_pass=leased_router['root_pass'],
            remote_admin_host=leased_router['remote_admin_host'],
            remote_admin_port=leased_router['remote_admin_port'],
            remote_admin_user=leased_router['remote_admin_user'],
            remote_admin_pass=leased_router['remote_admin_pass'],
            ssh_host=leased_router['ssh_host'],
            ssh_port=leased_router['ssh_port'],
            router_log_path=self.router_log_path)

        # Health check the router before we give it to the user.
        self.health_check_wifi_router(arm_router)

        self.wifi_routers.append(arm_router)
        return arm_router

    def get_capability_routers(self, capabilities: List[DeviceCapability], pool: DevicePool) -> List[str]:
        """Gets a list of available routers that support the desired list of capabilities.

        Args:
            capabilities: The list of capabilities the router must support.
            pool: The router pool used to retrieve the ARM router.

        Returns:
            List: List of the desired routers from the desired pool
        """
        logger.debug("Get the devices from {} that support the capabilities {}".format(pool, capabilities))
        routers = self.get_arm_router_list(url_params={'available': True}, pool=pool)
        rtype = 'type' if pool == DevicePool.ROUTERS else 'model'
        router_list = map(lambda x: x[rtype], routers)

        device_capabilities = DeviceCapabilities()
        capability_list = device_capabilities.get_devices_by_capabilities_supported(capabilities=capabilities)
        final_list = list(set(router_list) & set(capability_list))
        logger.debug("The device models in pool {} that support {} are {}.".format(pool, capabilities, final_list))
        return final_list

    def is_valid_type(self, router_type: str, pool: DevicePool = DevicePool.ROUTERS):
        """Determines whether or not the requested router type
        exists in the list of routers being managed by ARM.

        Args:
            router_type (str): Cradlepoint router model. eg. 'AER1600'
            pool: The router pool used to retrieve the ARM router.

        Returns:
            bool: True if the router model is one managed by ARM, False otherwise.
        """
        routers = self.get_arm_router_list(pool=pool)
        rtype = 'type' if pool == DevicePool.ROUTERS else 'model'
        for r in routers:
            if r[rtype] == router_type:
                return True
        return False

    def is_valid_id(self, router_id: str, pool: DevicePool = DevicePool.ROUTERS) -> bool:
        """Determines whether or not the requested router id exists in the list of routers being managed by ARM.

        Args:
            router_id: The id of a device to locate in the list of ARM routers e.g. "jr1".
            pool: The router pool used to retrieve the ARM router.

        Returns:
            True if the given id belongs to a router managed by ARM, False otherwise.

        """
        routers = self.get_arm_router_list(pool=pool)
        for r in routers:
            if r['id'] == router_id:
                return True
        return False

    def get_arm_router_list(self, url_params: Optional[Dict] = None, pool: DevicePool = DevicePool.ROUTERS) -> Dict[str, Any]:
        """Retrieves the list of routers from ARM that match
        the given parameters.

        Args:
            url_params: Dictionary of parameters to pass through to the ARM API.
            pool: The router pool used to retrieve the ARM router.

        Returns:
            Information about the routers that have been returned by ARM.

        Examples:
            To get a list of available routers that are available, match the
            desired product type (model), and are physical only.

            url_params = {
                'available': True,
                'type': desired_product,
                'physical': physical_only
            }

            :obj:`ARMInterface`.get_arm_router_list(url_params)

            To get a list of all routers being managed by ARM.

            :obj:`ARMInterface`.get_arm_router_list()
        """
        try:
            logger.debug('Getting list of routers from ARM server.')
            server_url = "{}{}/".format(self.server, pool.value)
            r = self.session.get(url=server_url, json=self._build_args(), params=url_params, timeout=self.arm_api_timeout)

            if r.status_code != 200:
                pretty_json = json.dumps(r.json(), indent=4)
                err_msg = 'Unable to retrieve list of available routers.\nResponse JSON:\n\n{}'.format(pretty_json)
                raise ARMAPIError(err_msg)

            routers = r.json()['data']
            return routers

        except requests.exceptions.ConnectionError as ce:
            logger.warning("ARM API appears to be unavailable at the moment. {}".format(ce))
            raise
        except:
            raise

    def health_check_router(self, router):
        try:
            url = 'http://{}:{}'.format(router.gateway, router.remote_admin_port)
            api = RouterRESTAPI(api_root=url, timeout=self.arm_api_timeout)
            api.authenticate(user_name='admin', password=router.authkey)
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as requests_exception:
            raise ARMResourceUnhealthy('{} appears to be in an unhealthy state! {}'.format(router, requests_exception))
        except Exception:
            raise
        logger.info("{} appears to be healthy.".format(router))

    def health_check_wifi_router(self, router):
        try:
            url = 'https://{}:{}'.format(router.remote_admin_host, router.remote_admin_port)
            api = RouterRESTAPI(api_root=url, timeout=self.arm_api_timeout)
            api.authenticate(user_name=router.remote_admin_user, password=router.remote_admin_pass)
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as requests_exception:
            raise ARMResourceUnhealthy('{} appears to be in an unhealthy state! {}'.format(router, requests_exception))
        except Exception:
            raise
        logger.info("{} appears to be healthy.".format(router))

    def release_router(self, router: ARMRouter):
        """Release the router back into the pool of ARM resources."""
        r = self.session.delete(
            url=self.server + 'routers/{}/lease/'.format(router.router_id),
            json=self._build_args(),
            timeout=self.arm_api_timeout)
        if r.status_code != 204:
            logger.warning('Unable to release router {}.'.format(router))

    def release_wifi_router(self, router: ARMWifiRouter):
        """Release the router back into the pool of ARM resources."""
        r = self.session.delete(
            url=self.server + 'wifi_routers/{}/lease/'.format(router.router_id),
            json=self._build_args(),
            timeout=self.arm_api_timeout)
        if r.status_code != 204:
            logger.warning('Unable to release router {}.'.format(router))

    def release_all_routers(self):
        for router in self.routers:
            self.release_router(router)

    def release_all_wifi_routers(self):
        for router in self.wifi_routers:
            self.release_wifi_router(router)

    def unregister_all_routers_from_ecm(self):
        for router in self.routers:
            router.unregister_from_ncm()

    def remove_all_routers_from_nce(self):
        """Removes routers from NCE that have had NCE loaded onto them. This will remove them from the list
        of NCE devices and therefore will remove it from the UI.
        """
        for router in self.routers:
            router.remove_from_nce()

    def lease_router(self,
                     desired_product: str = None,
                     exclude_products: List[str] = None,
                     physical_only: bool = None,
                     reason: str = None,
                     requested_router: str = None,
                     pool: DevicePool = DevicePool.ROUTERS) -> Dict:
        """

                Args:
                    desired_product: The desired product type. e.g. AER3100
                    exclude_products: A list of product models you would like to exclude from router selection.
                        eg. ["AER3100", "AER2100"]
                    physical_only: If true get any physical device. If false get a 3200v.
                    reason: Reason for leasing out the router. Should typically be the name of the automated test.
                    requested_router: The router_id of a specific ARM router.
                    pool: The router pool used to retrieve the ARM router.

                Returns:
                     Dict: A dictionary representation of the leased router.

                """
        if requested_router and not self.is_valid_id(requested_router, pool):
            raise ARMResourceUnavailable('The requested router ({}) is not available in ARM.'.format(requested_router))

        if desired_product and not self.is_valid_type(desired_product, pool):
            raise ARMResourceUnavailable('The requested router model ({}) is not available in ARM.'.format(desired_product))

        if desired_product == '3200v' and physical_only is True:
            raise ValueError('The desired_product cannot be "3200v" when requesting only physical devices.')

        if not exclude_products:
            exclude_products = list()

        if desired_product in exclude_products:
            raise ValueError('The desired product cannot match the excluded product.')

        router_type = 'type' if pool == DevicePool.ROUTERS else 'model'

        url_params = {'available': True, router_type: desired_product, 'physical': physical_only}
        logger.debug("Looking for router matching params {}.".format(url_params))

        # Find some routers matching our params. If none are found try again until we time out.
        time_out = time.time() + self.lease_wait

        while True:
            # Query ARM for the type of router we're looking for.
            routers = self.get_arm_router_list(url_params, pool)
            # Remove routers we want to exclude.
            if requested_router:
                routers = [r for r in routers if r['id'] == requested_router]
            elif exclude_products:
                routers = [r for r in routers if r[router_type] not in exclude_products]

            if len(routers) > 0:
                break  # Routers found.
            elif time.time() > time_out:
                break  # We've ran out of time.

            logger.warning("No available routers found. Querying again in 1 second...")
            time.sleep(1)

        if len(routers) == 0:
            raise ARMResourceUnavailable('No available routers found within {} seconds.'.format(self.lease_wait))

        chosen_router = random.choice(routers)

        if chosen_router[router_type] in exclude_products:
            raise ValueError("Chosen router model should have been excluded.")

        logger.info('Attempting to lease {}.'.format(chosen_router))
        r = self.session.put(
            url=self.server + '{}/{}/lease/'.format(pool.value, chosen_router['id']),
            json=self._build_args(reason=reason),
            timeout=self.arm_api_timeout)
        if r.status_code != 200:
            message = r.json()['message']
            raise ARMAPIError('Unable to lease {}. Message from the server="{}"'.format(chosen_router, message))

        # Use the return value of the lease call to discover our auth for the router.
        try:
            leased_router = r.json()['data']
        except Exception as e:
            raise ARMException("{} occurred while obtaining leased router data. The response had status code '{}' and "
                               "text '{}'".format(type(e), r.status_code, r.text)) from e
        return leased_router

    def get_wifi_client(self, client_id: str = None, client_type: str = None, reason: str = None) -> ARMWifiClient:
        """Gets an available wifi client.

        Args:
            client_id: The id of the client.  e.g. 'Galaxy_S7_1'
            client_type: The type of the client.  e.g. 'android'
            reason: The reason for checking out the client.

        Returns:
            ARMWifiClient: The object representing the client.

        """
        time_out = time.time() + self.lease_wait
        wifi_clients = []

        while time.time() < time_out:
            wifi_clients = self.client.wifi_clients.get_many(type_=client_type, is_available=True)
            if client_id is not None:
                wifi_clients = [wifi_client for wifi_client in wifi_clients if wifi_client.id == client_id]
            if not wifi_clients:
                logger.warning("No available client found. Querying again in 1 second...")
                time.sleep(1)
            else:
                break  # Client found
        if not wifi_clients:
            raise ARMResourceUnavailable(
                'No clean client available matching client_id={} and type={} found in {} seconds.'.format(
                    client_id, client_type, self.lease_wait))

        chosen_client = random.choice(wifi_clients)
        lease = self.lease_wifi_client(chosen_client, reason)
        client = ARMWifiClient(
            manager=self, connection_config=lease.connection_config, client_id=lease.id, client_type=lease.type)
        self.wifi_clients.append(client)
        return client

    def lease_wifi_client(self, wifi_client: ARMWiFiClient, reason: str) -> ARMLeasedWifiClient:
        """Lease an ARM wifi client.

        Args:
            wifi_client: The client to be leased.
            reason: The reason for checking out the client.

        """
        logger.info('Leasing {} from the ARM pool.'.format(wifi_client))
        return self.client.wifi_clients.create_lease(wifi_client.id, str(reason))

    def get_available_wifi_clients_list(self) -> List[ARMWiFiClient]:
        """Gets the list of available wifi clients.

        Returns:
            List[ARMWiFiClient]: The list of available clients.

        """
        logger.debug('Getting list of available clients from the ARM server.')
        return self.client.wifi_clients.get_many(is_available=True)

    def release_wifi_client(self, wifi_client: ARMWifiClient) -> None:
        """Releases a client back to the ARM pool.

        Args:
            wifi_client: The id of the client.

        """
        logger.info('Release {} to the ARM pool.'.format(wifi_client))
        self.client.wifi_clients.delete_lease(client_id=wifi_client.client_id)

    def release_all_wifi_clients(self) -> None:
        """Releases all known checked out clients to the ARM pool.

        """
        for client in self.wifi_clients:
            self.release_wifi_client(client)

    def _build_args(self, test_args: Dict[str, Any] = None, reason: str = None) -> Dict[str, Any]:

        args = dict()
        args['api_key'] = self.api_key
        if reason:
            args['reason'] = reason
        if test_args:
            args.update(test_args)

        return args

    def teardown(self):
        for router in self.routers:
            router.teardown()
        for wifi_router in self.wifi_routers:
            wifi_router.teardown()
        for wifi_client in self.wifi_clients:
            wifi_client.teardown()

    def get_failure_report(self):
        report = str(self)
        report += '\n'

        for router in self.routers:
            report += '\n'
            report += router.get_failure_report()
            report += '\n'

        return report
