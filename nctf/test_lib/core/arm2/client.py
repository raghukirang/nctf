"""ARMClient"""
from typing import Callable
from typing import Dict

from nctf.test_lib.core.arm2.mission import MissionResponse


class ARMClient(object):
    """A client that is associated with an ARMRouter.

    Args:
        router_id: ID of the router this client is associated with.
        client_id: ID of this client.
        mission_executor: The method which will execute missions. In this case, The _do_mission method from ARMRouter.
    """

    def __init__(self, router_id: str, client_id: str, mission_executor: Callable[[str, Dict], MissionResponse]):
        self.router_id = router_id
        self.client_id = client_id
        self.mission_executor = mission_executor

    def curl(self, url: str) -> MissionResponse:
        """Runs curl on a router's client and returns the response.

        Args:
            url: The URL that will be curl'd.

        Returns:
            The Inception MissionResponse.
        """
        mission = 'curl_from_lan_client.test'
        args = {'args_router1': self.router_id, 'args_client1': self.client_id, 'args_url': url}

        return self.mission_executor(mission, args)
