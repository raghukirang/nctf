import logging

logger = logging.getLogger('test_lib.core.arm')


class MissionResponse(object):
    def __init__(self, arm_response):
        self.log = arm_response['stderr']
        if arm_response['exit_status'] == 0:
            self.success = True
        else:
            self.success = False

        logger.info('{}'.format(self.log))
        logger.info('*************************************************************************************************')
        logger.info('*************************************************************************************************')

    def __str__(self):
        return '<Success={}>'.format(self.success)


# Mapping of Inception test names to non-default timeout values.
# This is to allow for mission timeout extensions on long running missions
# such as firmware installation.
MISSION_TIMEOUTS = {'default': 300, 'modify_firmware.test': 600, 'appropriate_dpd_messages_logged.test': 420}
