import logging
import sys
from nctf.test_lib.ncos.fixtures.router_api.fixture import *

logger = logging.getLogger('test_lib.core.arm')


class NotRegisteredWithECM(Exception):
    pass


class DeviceNotAvailableError(Exception):
    pass


class ARMWifiRouter:
    """A wifi router that has been leased from ARM."""

    def __init__(self, manager: "ARMInterface", model: str, router_id: str, root_pass: str, remote_admin_host: str,
                 remote_admin_port: str, remote_admin_user: str, remote_admin_pass: str, ssh_host: str,
                 ssh_port: str, router_log_path: str=None) -> None:
        self._manager = manager
        self.arm_api_timeout = manager.arm_api_timeout

        self.model = model
        self.router_id = router_id
        self.root_pass = root_pass
        self.remote_admin_host = remote_admin_host
        self.remote_admin_port = remote_admin_port
        self.remote_admin_user = remote_admin_user
        self.remote_admin_pass = remote_admin_pass
        self.ssh_host = ssh_host
        self.remote_ssh_port = ssh_port
        self.router_log_path = router_log_path
        self.rest_client = self.get_rest_client()
        self.clear_router_log()
        self.enable_debug_logs()

    def unregister_from_ecm(self):
        pass

    def remove_from_nce(self):
        pass

    def get_failure_report(self):
        pass

    def teardown(self):
        self.get_router_log()

    def get_rest_client(self) -> RouterRESTAPI:
        rest_fixture = RouterRESTAPIFixture()
        rest_api = rest_fixture.connect(scheme='https', hostname=self.remote_admin_host, port=self.remote_admin_port)
        rest_api.authenticate(user_name=self.remote_admin_user, password=self.remote_admin_pass)
        return rest_api

    def clear_router_log(self):
        self.rest_client.authenticate(user_name=self.remote_admin_user, password=self.remote_admin_pass)
        self.rest_client.root.update(name='api/control/log', update={'clear': True})

    def enable_debug_logs(self):
        self.rest_client.authenticate(user_name=self.remote_admin_user, password=self.remote_admin_pass)
        self.rest_client.config_system.update(name='logging', update={'level': 'debug'})

    def get_router_log(self):
        logger.debug("Get status log.")
        if self.router_log_path:
            self.rest_client.authenticate(user_name=self.remote_admin_user, password=self.remote_admin_pass)
            cp_log = self.rest_client.status_log.get()["data"]
            log = json.dumps(cp_log).replace("],", "],\n")

            with open(self.router_log_path, 'w') as f:
                f.write(log)
        else:
            logger.debug("No path specified to place the status log file.")

    def __str__(self):
        return "<ARMWifiRouter(router_id='{}', model='{}')>".format(self.router_id, self.model)
