import logging
from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture

logger = logging.getLogger('test_lib.core.arm')


class NotRegisteredWithECM(Exception):
    pass


class DeviceNotAvailableError(Exception):
    pass


class ARMWifiClient:
    """A wifi client that has been leased from ARM."""

    def __init__(self, manager: "ARMInterface", connection_config: dict, client_id: str, client_type: str) -> None:
        self._manager = manager
        self.arm_api_timeout = manager.arm_api_timeout

        self.connection_config = connection_config
        self.client_id = client_id
        self.client_type = client_type

    def get_client(self):
        client_fixture = WiFiClientFixture()
        wifi_client = client_fixture.get_wifi_client(client_type=self.client_type, connection_config=self.connection_config)
        return wifi_client

    def unregister_from_ecm(self):
        pass

    def remove_from_nce(self):
        pass

    def get_failure_report(self):
        pass

    def teardown(self):
        pass

    def __str__(self):
        return "<ARMWifiClient(client_id='{}', type='{}')>".format(self.client_id, self.client_type)
