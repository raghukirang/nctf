import json
import logging
import os

from nctf.libs.common_library import is_valid_directory
from nctf.test_lib.base.fixtures.fixture import FixtureBase

logger = logging.getLogger('test_lib.core.asset_handler')


class AssetFinderError(Exception):
    pass


class AssetFinderNoSuchDirectoryError(AssetFinderError):
    pass


class AssetFinderDuplicateFileError(AssetFinderError):
    pass


class AssetFinderFileNotFoundError(AssetFinderError):
    pass


class AssetHandlerFixture(FixtureBase):
    """An interface to find test assets recursively in the given directories.

    Args:
        test_dir: The directory the test file is contained in.
        project_assets_dir: The directory where common assets are located.

    """

    def __str__(self):
        return "<AssetFinder(open_files='{}')>".format(self.open_files)

    def __init__(self, test_dir: str, project_assets_dir: str = None):

        self.test_dir = None
        self.project_assets_dir = None
        self.framework_assets_dir = None

        if is_valid_directory(test_dir):
            self.test_dir = test_dir
        else:
            raise AssetFinderNoSuchDirectoryError("Invalid test directory {}".format(test_dir))

        if project_assets_dir:
            if is_valid_directory(project_assets_dir):
                self.project_assets_dir = project_assets_dir
            else:
                raise AssetFinderNoSuchDirectoryError("Invalid project assets dir {}".format(project_assets_dir))
        else:
            logger.warning("No project assets directory specified by --assets. "
                           "Only searching for assets in the current test's directory.")

        self.open_files = []

    def get_asset_file_path(self, file_name: str):
        """Search recursively through our test's directory, project assets directory, and the framework assets directory
        in order to find a file with the given file_name. The first file we're able to find we'll return the path to.

        Note:
            The search priority is test directory > project assets directory > framework assets directory.

        Args:
            file_name: The file to be found.
            mode: The mode to open the file in. e.g. 'rw'

        Returns:
            File: The path to a single file that we found that matches the file_name.

        """
        if self.test_dir:
            logger.debug("Checking {} for asset {} first...".format(self.test_dir, file_name))
            p = self.__find_file_path(search_dir=self.test_dir, search_name=file_name)
            if p:
                logger.debug("Found asset {} in {}!".format(file_name, self.test_dir))
                return p

        if self.project_assets_dir:
            logger.debug("Checking {} for asset {} second...".format(self.project_assets_dir, file_name))
            p = self.__find_file_path(search_dir=self.project_assets_dir, search_name=file_name)
            if p:
                logger.debug("Found asset {} in {}!".format(file_name, self.project_assets_dir))
                return p

        raise AssetFinderFileNotFoundError("Asset {} could not be found.".format(file_name))

    def get_asset_file(self, file_name: str, mode: str = 'r'):
        """Search recursively through our test's directory, project assets directory, and the framework assets directory
        in order to find a file with the given file_name. The first file we're able to find we'll return. This allows
        duplication of asset names amongst the 3 directories.

        Note:
            The search priority is test directory > project assets directory > framework assets directory.

        Args:
            file_name: The file to be found.
            mode: The mode to open the file in. e.g. 'rw'

        Returns:
            File: A single opened file that we found that matches the file_name.

        """
        p = self.get_asset_file_path(file_name)
        f = open(p, mode)
        self.open_files.append(f)
        return f

    def get_asset_json(self, file_name: str):
        """

        Args:
            file_name: The file to be found.

        Returns:
            dict: The JSON retrieved from the file we found.

        """
        f = self.get_asset_file(file_name)
        j = json.load(f)
        return j

    def __find_file_path(self, search_dir: str, search_name: str):
        """Search through the given dir and find all files with the matching name. We expect
        that we should only find 1 such file. If we find more than 1 we'll raise an exception.

        Args:
            search_dir (str): The root of the directory tree we'll be recursively searching through.
            search_name (str): The name of the file we're searching for.

        Returns:
            File: A path to the file found that matches the search_name.

        Raises:
            AssetFinderDuplicateFileError:

        """
        matches = []

        for root, directories, filenames in os.walk(search_dir):
            for filename in filenames:
                if filename == search_name:
                    matches.append(os.path.join(root, filename))

        if len(matches) > 1:
            raise AssetFinderDuplicateFileError

        if len(matches) == 0:
            return None

        else:
            return matches[0]

    def teardown(self):
        logger.debug("Closing files...")
        for f in self.open_files:
            f.close()

    def get_failure_report(self):
        raise NotImplementedError()
