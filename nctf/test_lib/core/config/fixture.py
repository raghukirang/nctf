import logging
import os
from typing import List

from attrdict import AttrMap
import yaml

from cerberus import Validator

from .exceptions import ConfigFixtureError
from .exceptions import InvalidConfigError
from .exceptions import InvalidConfigOverrideError
from .schema.v1 import run_config_schema as run_schema_v1
from .schema.v1 import run_metadata_schema as run_metadata_schema_v1
from .schema.v1 import target_config_schema as target_schema_v1
from .schema.v1 import target_metadata_schema as target_metadata_schema_v1

logger = logging.getLogger("test_lib.core.config")


class ConfigFixture(AttrMap):
    pass


class ConfigFixtureFactory:
    def create(self,
               run_config: str,
               target_config: str,
               version: int = 1,
               use_env_vars: bool = False,
               overrides: List[str] = None) -> ConfigFixture:
        """Create the ConfigFixture using the specified schema version

        Args:
            run_config: Path to the run configuration file.
            target_config: Path to the target configuration file.
            version: The schema version to be validated against.
            use_env_vars: If true, after consuming the initial file override with any env vars. e.g. JFW_foo_bar_baz=1
            overrides: A comma delimited list of values to override. e.g. "foo.bar=baz,quick.brown=fox"

        """
        if version == 1:
            return self.create_v1(run_config, target_config, use_env_vars, overrides)
        else:
            raise ConfigFixtureError("Unsupported schema version {}".format(version))

    def create_v1(self, run_config: str, target_config: str, use_env_vars: bool = False,
                  overrides: List[str] = None) -> ConfigFixture:
        """Create the ConfigFixture using schema version 1

        Args:
            run_config: Path to the run configuration file.
            target_config: Path to the target configuration file.
            use_env_vars: If true, after consuming the initial file override with any env vars. e.g. JFW_foo_bar_baz=1
            overrides: A comma delimited list of values to override. e.g. "foo.bar=baz,quick.brown=fox"

        """
        # Open, load and validate run configuration.
        run_items = {}
        with open(run_config, mode='r') as f:
            if run_config.endswith('.yaml'):
                d = yaml.safe_load(f)
                self.validate_config(d, run_metadata_schema_v1, allow_unknown=True)
            else:
                raise TypeError("invalid file: {}".format(run_config))
            run_items = {"run": d}

        # Open, load and validate target configuration.
        target_items = {}
        with open(target_config, mode='r') as f:
            if target_config.endswith('.yaml'):
                d = yaml.safe_load(f)
                self.validate_config(d, target_metadata_schema_v1, allow_unknown=True)
            else:
                raise TypeError("invalid file: {}".format(target_config))
            target_items = {"target": d}

        if run_items["run"]["version"] != 1:
            raise ConfigFixtureError("Excepted schema version 1 for {}.".format(run_config))

        if target_items["target"]["version"] != 1:
            raise ConfigFixtureError("Excepted schema version 1 for {}.".format(target_config))

        items = {**target_items, **run_items}

        if use_env_vars:
            self._update_with_env_vars(items)

        if overrides:
            self._update_with_overrides(overrides, items)

        self.validate_config(items['run'], run_schema_v1)
        self.validate_config(items['target'], target_schema_v1)

        config_fixture = ConfigFixture(items)
        self._handle_mutations(config_fixture)
        return config_fixture

    @staticmethod
    def validate_config(config: dict, schema: dict, allow_unknown: bool = False):
        v = Validator(allow_unknown=allow_unknown)
        valid = v.validate(config, schema)
        if not valid:
            raise InvalidConfigError(v.errors)

    @staticmethod
    def _handle_mutations(config_fixture: ConfigFixture):
        """Mutate values from the configuration that have been changed, but do not break the existing schema

        These mutations should maintain consistent behavior of previous NCTF versions and provide a way for us to
        prevent cutting a new configuration schema for every nominal change.

        """
        try:
            if hasattr(config_fixture.run.fixtures.selenium, "useVirtualDisplay"):
                logger.warning("Python virtual display has been replaced by Chrome's --headless option.")
                logger.warning("run.fixtures.selenium.useVirtualDisplay will no longer do anything.")
                logger.warning("Please remove it from your run config YAML.")
        except AttributeError:
            pass

        try:
            if hasattr(config_fixture.run.fixtures.selenium, "virtualDisplayVisible"):
                logger.warning("Python virtual display has been replaced by Chrome's --headless option.")
                logger.warning("run.fixtures.selenium.virtualDisplayVisible's value will be repurposed "
                               "for run.fixtures.selenium.headlessChrome.")
                logger.warning("Please replace useVirtualDisplay and virtualDisplayVisible with "
                               "headlessChrome in your run config YAML.")
                virtual_display_visible = config_fixture.run.fixtures.selenium.virtualDisplayVisible
                config_fixture.run.fixtures.selenium.headlessChrome = virtual_display_visible
        except AttributeError:
            pass

    @staticmethod
    def _update_with_env_vars(items: dict):
        env_vars = os.environ.keys()
        # env_vars = filter(lambda v: v.startswith("JFW_"), env_vars)
        env_vars = [v for v in env_vars if (v.startswith("JFW_") or v.startswith("NCTF_"))]

        for var in env_vars:
            val = os.environ.get(var)
            var_parts = var.split("_")
            var_parts.pop(0)  # Remove "JFW_" or "NCTF_" prefix.

            sub_items = items
            for var_part in var_parts:
                # If this part of the env var is a key in the current dict.
                if var_part in sub_items.keys():
                    # If the value if that key is a dict then go deeper.
                    if isinstance(sub_items[var_part], dict):
                        sub_items = sub_items[var_part]
                        continue
                    # If this is the last var part then assign the val.
                    elif var_parts[-1] is var_part:
                        sub_items[var_part] = yaml.safe_load(val)
                    # The value of the current key is not a dictionary and this is not the last var part.
                    else:
                        raise InvalidConfigOverrideError(
                            "Could not find matching config key for environment variable: {}".format(var))

    @staticmethod
    def _update_with_overrides(overrides: List[str], items: dict):
        for override in overrides:
            val = override.split("=", 1)[-1]
            var = override.split("=", 1)[0]
            var_parts = var.split(".")

            sub_items = items
            for var_part in var_parts:
                # If this part of the override is a key in the current dict.
                if var_part in sub_items.keys():
                    # If the value if that key is a dict then go deeper.
                    if isinstance(sub_items[var_part], dict):
                        sub_items = sub_items[var_part]
                        continue
                    # If this is the last override part then assign the val.
                    elif var_parts[-1] == var_part:
                        sub_items[var_part] = yaml.safe_load(val)
                    # The value of the current key is not a dictionary and this is not the last override part.
                    else:
                        raise InvalidConfigOverrideError(
                            "Could not find matching config key for override: {}".format(override))
