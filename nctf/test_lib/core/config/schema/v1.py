allowed_run_meta_types = ["run"]
allowed_target_meta_types = ["target"]

run_metadata_schema = {
    "version": {
        "type": "integer",
        "required": True,
    },
    "meta": {
        "type": "dict",
        "required": True,
        "schema": {
            "type": {
                "type": "string",
                "required": True,
                "allowed": allowed_run_meta_types
            }
        }
    }
}

run_data_schema = {
    "version": {
        "type": "integer",
        "required": True,
    },
    "meta": {
        "type": "dict",
        "required": True,
        "schema": {
            "type": {
                "type": "string",
                "required": True,
                "allowed": allowed_run_meta_types
            }
        }
    },
    "fixtures": {
        "type": "dict",
        "schema": {
            "arm": {
                "type": "dict",
                "schema": {
                    "protocol": {
                        "type": "string",
                        "required": True
                    },
                    "hostname": {
                        "type": "string",
                        "required": True
                    },
                    "port": {
                        "type": "integer",
                        "required": True
                    },
                    "path": {
                        "type": "string",
                        "required": True
                    },
                    "apiKey": {
                        "type": "string",
                        "required": True,
                        "nullable": True
                    },
                    "leaseWait": {
                        "type": "integer",
                        "required": True
                    }
                }
            },
            "arm4": {
                "type": "dict",
                "schema": {
                    "protocol": {
                        "type": "string",
                        "required": True
                    },
                    "hostname": {
                        "type": "string",
                        "required": True
                    },
                    "port": {
                        "type": "integer",
                        "required": True
                    },
                    "apiKey": {
                        "type": "string",
                        "required": True,
                        "nullable": True
                    },
                    "defaultPoolId": {
                        "type": "string",
                        "required": False,
                        "nullable": True,
                        "default": None
                    }
                }
            },
            "selenium": {
                "type": "dict",
                "schema": {
                    "useVirtualDisplay": {},  # Deprecated in favor of browserVisible
                    "virtualDisplayVisible": {  # Deprecated in favor of browserVisible
                        "type": "boolean",
                        "required": True,
                        "excludes": "browserVisible"
                    },
                    "browserVisible": {
                        "type": "boolean",
                        "required": True,
                        "excludes": "virtualDisplayVisible"
                    },
                    "browserName": {  # "Chrome" | "Firefox"
                        "type": "string",
                        "required": False,
                        "default": "Chrome"
                    },
                    "browserVersion": {
                        "type": "string",
                        "required": False,
                        "nullable": True,
                        "dependencies": "browserName"
                    },
                    "browserPlatform": {
                        "type": "string",
                        "required": False,
                        "nullable": True
                    },
                    "browserDebug": {  # if True will make a directory in artifacts/<test name>/debug_<date-time>
                        "type": "boolean",
                        "required": False,
                        "default": False
                    },
                    "browserProfile":
                    {  # if left blank, will use the default profile, "$PYTEST_CURRENT_TEST" yields a unique artifacts/<test name>/debug_<date-time> for each test, otherwise define as directory where the profile is
                        "type": ["string"],
                        "required": False,
                        "nullable": True
                    },
                    "needleSaveBaseline": {
                        "type": "boolean",
                        "required": True
                    },
                    "displaySizeW": {
                        "type": "integer",
                        "required": True
                    },
                    "displaySizeH": {
                        "type": "integer",
                        "required": True
                    },
                    "timeout": {
                        "type": "integer",
                        "required": True
                    },
                    "useProxy": {
                        "type": "boolean",
                        "required": True
                    },
                    "recordTraffic": {
                        "type": "boolean",
                        "required": True
                    }
                }
            },
            "requests": {
                "type": "dict",
                "schema": {
                    "timeout": {
                        "type": "integer",
                        "required": True
                    }
                }
            },
            "virtNetwork": {
                "type": "dict",
                "schema": {
                    "guidoPath": {
                        "type": "string",
                        "required": True
                    },
                    "hardwareMap": {
                        "type": "string",
                        "nullable": True
                    }
                }
            },
            "salesforce": {
                "type": "dict",
                "schema": {
                    "queryTimeout": {
                        "type": "integer",
                        "required": True
                    }
                }
            },
            "s3sim": {
                "type": "dict",
                "schema": {
                    "ecmProtocol": {
                        "type": "string",
                        "required": True
                    },
                    "ecmUsername": {
                        "type": "string",
                        "required": True
                    },
                    "ecmPassword": {
                        "type": "string",
                        "required": True
                    },
                    "productName": {
                        "type": "string",
                        "required": True
                    },
                    "fwVersion": {
                        "type": "string",
                        "required": True
                    },
                    "fwBuildDate": {
                        "type": "string",
                        "required": True
                    },
                    "personality": {
                        "type": "string",
                        "required": True
                    },
                    "wanDevices": {
                        "type": "list",
                        "required": True,
                        "schema": {
                            "type": "string"
                        }
                    },
                    "ecmPrivate": {
                        "type": "string",
                        "required": True
                    }  # For whatever reason this needs to be a string.
                }
            },
            "email": {
                "type": "dict",
                "schema": {
                    "api": {
                        "type": "dict",
                        "required": True,
                        "schema": {
                            "service": {
                                "type": "string",
                                "required": True,
                                "allowed": ["mailinator", "mailhog"]
                            },
                            "protocol": {
                                "type": "string",
                                "required": True,
                                "allowed": ["http", "https"]
                            },
                            "hostname": {
                                "type": "string",
                                "required": True
                            },
                            "port": {
                                "type": "integer",
                                "required": True
                            },
                            "token": {
                                "type": "string",
                                "dependencies": {
                                    "service": ["mailinator"]
                                }
                            },
                            "timeout": {
                                "type": "float",
                                "required": True
                            }
                        }
                    }
                }
            },
            "netCloudAccount": {
                "type": "dict",
                "schema": {
                    "creationMethod": {
                        "type": "string",
                        "required": True,
                        "allowed": ["accounts", "salesforce", "salesforce_mock"]
                    },
                    "accountsPrivilegedUsername": {
                        "type": "string",
                        "dependencies": {
                            "creationMethod": "accounts"
                        }
                    },
                    "accountsPrivilegedPassword": {
                        "type": "string",
                        "dependencies": {
                            "creationMethod": "accounts"
                        }
                    }
                }
            },
            "fwLoader": {
                "type": "dict",
                "schema": {
                    "imgLocation": {
                        "type": "string",
                        "required": True,
                        "allowed": ["guido", "local"]
                    },
                    "guidoBranch": {
                        "type": "string",
                        "nullable": True,
                    },
                    "guidoPath": {
                        "type": "string",
                        "nullable": True,
                    },
                    "guidoDate": {
                        "type": "string",
                        "nullable": True,
                    },
                    "localFullPath": {
                        "type": "string",
                        "nullable": True,
                    },
                    "loadMethod": {
                        "type": "string",
                        "required": True,
                        "allowed": ["rest"]
                    }
                }
            },
            "fwLoader2": {
                "type": "dict",
                "schema": {
                    "imgLocation": {
                        "type": "string",
                        "required": True,
                        "allowed": ["artifactory", "local"]
                    },
                    "apiKey": {
                        "type": "string",
                        "required": True,
                        "nullable": True,
                    },
                    "repository": {
                        "type": "string",
                        "required": True,
                        "nullable": True,
                    },
                    "imgDir": {
                        "type": "string",
                        "required": True,
                        "nullable": True,
                    }
                }
            },
            "netCloudRouter": {
                "type": "dict",
                "schema": {
                    "creationMethod": {
                        "type": "string",
                        "required": True,
                        "allowed": ["armv4", "virtnetwork", "local"]
                    }
                }
            },
        }
    }
}

run_config_schema = dict()
run_config_schema.update(run_metadata_schema)
run_config_schema.update(run_data_schema)

webservice_schema = {
    "protocol": {
        "type": "string",
        "required": True,
        "allowed": ["http", "https"]
    },
    "hostname": {
        "type": "string",
        "required": True,
        "forbidden": ["cradlepointecm.com", "www.cradlepointecm.com"]
    },
    "port": {
        "type": "integer",
        "required": True
    },
    "path": {
        "type": "string"
    }
}

target_metadata_schema = {
    "version": {
        "type": "integer",
        "required": True
    },
    "meta": {
        "type": "dict",
        "required": True,
        "schema": {
            "type": {
                "type": "string",
                "required": True,
                "allowed": allowed_target_meta_types
            }
        }
    }
}

target_data_schema = {
    "services": {
        "type": "dict",
        "schema": {
            "accounts": {
                "type": "dict",
                "schema": webservice_schema
            },
            "ncm": {
                "type": "dict",
                "schema": webservice_schema
            },
            "stream": {
                "type": "dict",
                "schema": webservice_schema
            },
            "s2": {
                "type": "dict",
                "schema": webservice_schema
            },
            "als": {
                "type": "dict",
                "schema": webservice_schema
            },
            "archer": {
                "type": "dict",
                "schema": webservice_schema
            },
            "papi": {
                "type": "dict",
                "schema": webservice_schema
            },
            "mcPublic": {
                "type": "dict",
                "schema": webservice_schema
            },
            "mcInternal": {
                "type": "dict",
                "schema": webservice_schema
            },
            "manajs": {
                "type": "dict",
                "schema": webservice_schema
            },
            "nydus": {
                "type": "dict",
                "schema": webservice_schema
            },
            "sockeyeUI": {
                "type": "dict",
                "schema": webservice_schema
            },
            "salesforce": {
                "type": "dict",
                "schema": webservice_schema
            },
            "testshop": {
                "type": "dict",
                "schema": webservice_schema
            },
            "partnerPortal": {
                "type": "dict",
                "schema": webservice_schema
            },
            "pertinoSalesforce": {
                "type": "dict",
                "schema": webservice_schema
            },
            "pertinoPortal": {
                "type": "dict",
                "schema": webservice_schema
            },
            "zuoraApi": {
                "type": "dict",
                "schema": webservice_schema
            },
            "dsg": {
                "type": "dict",
                "schema": {
                    "presenceS3Bucket": {
                        "type": "string",
                        "required": True
                    },
                    "appAnalyticsS3Bucket": {
                        "type": "string",
                        "required": True
                    },
                    "appAnalyticsS3BucketTest": {
                        "type": "string",
                        "required": True
                    }
                }
            },
            "bsf": {
                "type": "dict",
                "schema": {
                    "sfconnector": {
                        "type": "dict",
                        "schema": webservice_schema
                    },
                    "entitlementService": {
                        "type": "dict",
                        "schema": webservice_schema
                    },
                    "licenseService": {
                        "type": "dict",
                        "schema": webservice_schema
                    },
                    "productSubscriptionService": {
                        "type": "dict",
                        "schema": webservice_schema
                    },
                    "provisioningService": {
                        "type": "dict",
                        "schema": webservice_schema
                    },
                    "orderService": {
                        "type": "dict",
                        "schema": webservice_schema
                    },
                    "viewLayerSolution": {
                        "type": "dict",
                        "schema": webservice_schema
                    }
                }
            },
            "ncos": {
                "type": "dict",
                "schema": {
                    "buildDate": {
                        "type": "string",
                        "required": True,
                        "nullable": True
                    },
                    "buildType": {
                        "type": "string",
                        "required": True,
                        "nullable": True
                    },
                    "gitBranch": {
                        "type": "string",
                        "required": True,
                        "nullable": True
                    },
                    "gitSha": {
                        "type": "string",
                        "required": True,
                        "nullable": True
                    },
                    "releaseVer": {
                        "type": "string",
                        "required": False,
                        "nullable": True,
                        "default": None
                    }
                }
            },
            "pdp": {
                "type": "dict",
                "schema": webservice_schema
            }
        }
    },
    "predefinedAuth": {
        "type": "dict",
    }
}

target_config_schema = dict()
target_config_schema.update(target_metadata_schema)
target_config_schema.update(target_data_schema)

config_schema = {
    "run": {
        "type": "dict",
        "required": True,
        "schema": run_config_schema,
    },
    "target": {
        "type": "dict",
        "required": True,
        "schema": target_config_schema,
    }
}
