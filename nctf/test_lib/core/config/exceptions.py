class ConfigFixtureError(Exception):
    pass


class InvalidConfigError(ConfigFixtureError):
    pass


class InvalidConfigOverrideError(ConfigFixtureError):
    pass
