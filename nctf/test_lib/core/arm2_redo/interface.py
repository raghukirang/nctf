from enum import Enum
import json
import logging
import os
import random
import time
from typing import Any
from typing import Dict
from typing import List
from typing import Optional

import requests

# from nctf.test_lib.core.arm2_redo.exceptions import ARMAPIError
# from nctf.test_lib.core.arm2.exceptions import ARMResourceUnavailable
# from nctf.test_lib.core.arm2_redo.wifi_client import ARMWifiClient
# from nctf.test_lib.core.arm2_redo.wifi_router import ARMWifiRouter
# from nctf.test_lib.libs.device_capabilities import DeviceCapabilities
# from nctf.test_lib.libs.device_capabilities import DeviceCapability
from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.core.arm2_redo.exceptions import ARMException
from nctf.test_lib.core.arm2_redo.exceptions import ARMResourceUnhealthy
from nctf.test_lib.core.arm2_redo.router import ARMRouter
from nctf.test_lib.core.arm4.fixture import ARMv4Exception
from nctf.test_lib.core.arm4.fixture import ARMv4Fixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwGetter
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI
from nctf.test_lib.netcloud.fixtures.netcloud_client.client import NetCloudClient
from nctf.test_lib.netcloud.fixtures.netcloud_router.router import CreationMethod
from nctf.test_lib.netcloud.fixtures.netcloud_router.router import NetCloudRouter
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture
from nctf.test_lib.utils.waiter import wait

logger = logging.getLogger('test_lib.core.arm')


class DevicePool(Enum):
    """Possible device pools for use.

    """
    ROUTERS = "routers"
    WIFI_ROUTERS = "wifi_routers"
    WIFI_CLIENTS = "wifi_clients"


class ARMInterfaceFake(FixtureBase):
    def __init__(self,
                 host,
                 port,
                 api_path,
                 api_key,
                 lease_wait=30,
                 arm_api_timeout=60,
                 router_log_path=None,
                 default_reason: str = None,
                 default_pool_id: str = None,
                 default_fw_getter: FwGetter = None,
                 netcloud_rest_fixture: NetCloudRESTFixture = None,
                 artifactory_api_key: str = None,
                 artifactory_repo: str = None):
        """

        Args:
            host (str): Host of the ARM REST API.
            port (int): Port of the ARM REST API
            api_key (str): ARM REST API auth key.
            api_path (str): path to ARM REST API base endpoint
            lease_wait (int): Maximum amount of time to wait to lease a router in seconds.
                A value <=0 will attempt only 1 time.
            arm_api_timeout (int): Maximum amount of time in seconds to wait for the ARM API to return.


        """
        if host is None:
            raise ValueError('The Automation Resource Manager (ARM) requires a valid host parameter.')
        if api_key is None:
            raise ValueError('The Automation Resource Manager (ARM) requires a valid API key to be used.')
        if api_path is None:
            raise ValueError('The Automation Resource Manager (ARM) Fixture requires a valid API path')
        if port is None:
            raise ValueError('The Automation Resource Manager (ARM) Fixture requires a valid API port')
        if not isinstance(lease_wait, int):
            raise TypeError('lease_wait must be an integer value.')
        if not isinstance(arm_api_timeout, int):
            raise TypeError('arm_api_timeout must be a non-zero non-negative integer value.')
        if arm_api_timeout <= 0:
            raise ValueError('arm_api_timeout must be a non-zero non-negative integer value.')

        self.server = f'{host}:{port}{api_path}'
        self.api_key = api_key
        self.routers = []
        self.wifi_routers = []
        self.wifi_clients = []
        self.session = requests.Session()
        self.lease_wait = lease_wait
        self.arm_api_timeout = arm_api_timeout
        self.hostname = host
        self.router_log_path = router_log_path
        self._armv4_fixture = ARMv4Fixture(
            default_reason=default_reason,
            host=host,
            api_key=api_key,
            port=port,
            default_pool_id=default_pool_id,
            default_fw_getter=default_fw_getter)
        self.client = self._armv4_fixture
        self._netcloud_rest_fixture = netcloud_rest_fixture
        self._artifactory_api_key = artifactory_api_key
        self._artifactory_repo = artifactory_repo

    def __str__(self):
        return "<ARMInterfaceFake(server='{}', routers='{}', wifi_clients='{}'>".format(self.server, self.routers,
                                                                                        self.wifi_clients)

    def get_router(self,
                   desired_product: str = None,
                   exclude_products: List[str] = None,
                   physical_only: bool = None,
                   reason: str = None,
                   requested_router: str = None,
                   pool: DevicePool = DevicePool.ROUTERS) -> ARMRouter:
        """

        Args:
            desired_product: The desired product type. e.g. AER3100
            exclude_products: A list of product models you would like to exclude from router selection.
                eg. ["AER3100", "AER2100"]
            physical_only: If true get any physical device. If false get a 3200v.
            reason: Reason for leasing out the router. Should typically be the name of the automated test.
            requested_router: The router_id of a specific ARM router.
            pool: The router pool used to retrieve the ARM router.

        Returns:
             ARMRouter: An interface to a leased router.

        """
        [leased_router, leased_client] = self.lease_router(desired_product, exclude_products, physical_only, reason,
                                                           requested_router, pool)
        router = NetCloudRouter(device=leased_router, creation_method=CreationMethod.ARMV4)
        client = NetCloudClient(client=leased_client, creation_method=CreationMethod.ARMV4)
        # Create our object representation of a router leased from ARM.
        arm_router = ARMRouter(
            manager=self,
            product=router.product,
            router_id=router.ncos.router_id,
            mac=router.ncos.mac,
            gateway=router.ncos.remote_admin_host,
            authkey=router.ncos.admin_password,
            root_authkey=router.ncos.cproot_password,
            remote_admin_port=router.ncos.remote_admin_port,
            lan_client="",
            ssh_port=router.ncos.remote_admin_ssh_port,
            netcloud_router=router,
            netcloud_client=client,
            netcloud_rest_fixture=self._netcloud_rest_fixture,
            artifactory_api_key=self._artifactory_api_key,
            artifactory_repo=self._artifactory_repo)

        self.routers.append(arm_router)
        return arm_router

    # def get_wifi_router(self,
    #                     desired_product: str = None,
    #                     exclude_products: List[str] = None,
    #                     physical_only: bool = None,
    #                     reason: str = None,
    #                     requested_router: str = None,
    #                     pool: DevicePool = DevicePool.WIFI_ROUTERS) -> ARMWifiRouter:
    #     """
    #
    #     Args:
    #         desired_product: The desired product type. e.g. AER3100
    #         exclude_products: A list of product models you would like to exclude from router selection.
    #             eg. ["AER3100", "AER2100"]
    #         physical_only: If true get any physical device. If false get a 3200v.
    #         reason: Reason for leasing out the router. Should typically be the name of the automated test.
    #         requested_router: The router_id of a specific ARM router.
    #         pool: The router pool used to retrieve the ARM router.
    #
    #     Returns:
    #          ARMWifiRouter: An interface to a leased router.
    #
    #     """
    #     leased_router = self.lease_router(desired_product, exclude_products, physical_only, reason, requested_router, pool)
    #
    #     # Create our object representation of a router leased from ARM.
    #     arm_router = ARMWifiRouter(
    #         manager=self,
    #         model=leased_router['model'],
    #         router_id=leased_router['id'],
    #         root_pass=leased_router['root_pass'],
    #         remote_admin_host=leased_router['remote_admin_host'],
    #         remote_admin_port=leased_router['remote_admin_port'],
    #         remote_admin_user=leased_router['remote_admin_user'],
    #         remote_admin_pass=leased_router['remote_admin_pass'],
    #         ssh_host=leased_router['ssh_host'],
    #         ssh_port=leased_router['ssh_port'],
    #         router_log_path=self.router_log_path)
    #
    #     # Health check the router before we give it to the user.
    #     self.health_check_wifi_router(arm_router)
    #
    #     self.wifi_routers.append(arm_router)
    #     return arm_router

    # def get_capability_routers(self, capabilities: List[DeviceCapability], pool: DevicePool) -> List[str]:
    #     """Gets a list of available routers that support the desired list of capabilities.
    #
    #     Args:
    #         capabilities: The list of capabilities the router must support.
    #         pool: The router pool used to retrieve the ARM router.
    #
    #     Returns:
    #         List: List of the desired routers from the desired pool
    #     """
    #     logger.debug("Get the devices from {} that support the capabilities {}".format(pool, capabilities))
    #     routers = self.get_arm_router_list(url_params={'available': True}, pool=pool)
    #     rtype = 'type' if pool == DevicePool.ROUTERS else 'model'
    #     router_list = map(lambda x: x[rtype], routers)
    #
    #     device_capabilities = DeviceCapabilities()
    #     capability_list = device_capabilities.get_devices_by_capabilities_supported(capabilities=capabilities)
    #     final_list = list(set(router_list) & set(capability_list))
    #     logger.debug("The device models in pool {} that support {} are {}.".format(pool, capabilities, final_list))
    #     return final_list

    # def is_valid_type(self, router_type: str, pool: DevicePool = DevicePool.ROUTERS):
    #     """Determines whether or not the requested router type
    #     exists in the list of routers being managed by ARM.
    #
    #     Args:
    #         router_type (str): Cradlepoint router model. eg. 'AER1600'
    #         pool: The router pool used to retrieve the ARM router.
    #
    #     Returns:
    #         bool: True if the router model is one managed by ARM, False otherwise.
    #     """
    #     routers = self.get_arm_router_list(pool=pool)
    #     rtype = 'type' if pool == DevicePool.ROUTERS else 'model'
    #     for r in routers:
    #         if r[rtype] == router_type:
    #             return True
    #     return False

    # def is_valid_id(self, router_id: str, pool: DevicePool = DevicePool.ROUTERS) -> bool:
    #     """Determines whether or not the requested router id exists in the list of routers being managed by ARM.
    #
    #     Args:
    #         router_id: The id of a device to locate in the list of ARM routers e.g. "jr1".
    #         pool: The router pool used to retrieve the ARM router.
    #
    #     Returns:
    #         True if the given id belongs to a router managed by ARM, False otherwise.
    #
    #     """
    #     routers = self.get_arm_router_list(pool=pool)
    #     for r in routers:
    #         if r['id'] == router_id:
    #             return True
    #     return False

    # def get_arm_router_list(self, url_params: Optional[Dict] = None, pool: DevicePool = DevicePool.ROUTERS) -> Dict[str, Any]:
    #     """Retrieves the list of routers from ARM that match
    #     the given parameters.
    #
    #     Args:
    #         url_params: Dictionary of parameters to pass through to the ARM API.
    #         pool: The router pool used to retrieve the ARM router.
    #
    #     Returns:
    #         Information about the routers that have been returned by ARM.
    #
    #     Examples:
    #         To get a list of available routers that are available, match the
    #         desired product type (model), and are physical only.
    #
    #         url_params = {
    #             'available': True,
    #             'type': desired_product,
    #             'physical': physical_only
    #         }
    #
    #         :obj:`ARMInterface`.get_arm_router_list(url_params)
    #
    #         To get a list of all routers being managed by ARM.
    #
    #         :obj:`ARMInterface`.get_arm_router_list()
    #     """
    #     try:
    #         logger.debug('Getting list of routers from ARM server.')
    #         server_url = "{}{}/".format(self.server, pool.value)
    #         r = self.session.get(url=server_url, json=self._build_args(), params=url_params, timeout=self.arm_api_timeout)
    #
    #         if r.status_code != 200:
    #             pretty_json = json.dumps(r.json(), indent=4)
    #             err_msg = 'Unable to retrieve list of available routers.\nResponse JSON:\n\n{}'.format(pretty_json)
    #             raise ARMAPIError(err_msg)
    #
    #         routers = r.json()['data']
    #         return routers
    #
    #     except requests.exceptions.ConnectionError as ce:
    #         logger.warning("ARM API appears to be unavailable at the moment. {}".format(ce))
    #         raise
    #     except:
    #         raise

    def health_check_router(self, router):
        try:
            url = 'http://{}:{}'.format(router.gateway, router.remote_admin_port)
            api = RouterRESTAPI(api_root=url, timeout=self.arm_api_timeout)
            api.authenticate(user_name='admin', password=router.authkey)
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as requests_exception:
            raise ARMResourceUnhealthy('{} appears to be in an unhealthy state! {}'.format(router, requests_exception))
        except Exception:
            raise
        logger.info("{} appears to be healthy.".format(router))

    # def health_check_wifi_router(self, router):
    #     try:
    #         url = 'https://{}:{}'.format(router.remote_admin_host, router.remote_admin_port)
    #         api = RouterRESTAPI(api_root=url, timeout=self.arm_api_timeout)
    #         api.authenticate(user_name=router.remote_admin_user, password=router.remote_admin_pass)
    #     except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as requests_exception:
    #         raise ARMResourceUnhealthy('{} appears to be in an unhealthy state! {}'.format(router, requests_exception))
    #     except Exception:
    #         raise
    #     logger.info("{} appears to be healthy.".format(router))

    # def release_router(self, router: ARMRouter):
    #     """Release the router back into the pool of ARM resources."""
    #     r = self.session.delete(
    #         url=self.server + 'routers/{}/lease/'.format(router.router_id),
    #         json=self._build_args(),
    #         timeout=self.arm_api_timeout)
    #     if r.status_code != 204:
    #         logger.warning('Unable to release router {}.'.format(router))

    # def release_wifi_router(self, router: ARMWifiRouter):
    #     """Release the router back into the pool of ARM resources."""
    #     r = self.session.delete(
    #         url=self.server + 'wifi_routers/{}/lease/'.format(router.router_id),
    #         json=self._build_args(),
    #         timeout=self.arm_api_timeout)
    #     if r.status_code != 204:
    #         logger.warning('Unable to release router {}.'.format(router))

    # def release_all_routers(self):
    #     for router in self.routers:
    #         self.release_router(router)
    #
    # def release_all_wifi_routers(self):
    #     for router in self.wifi_routers:
    #         self.release_wifi_router(router)

    def unregister_all_routers_from_ecm(self):
        for router in self.routers:
            router.unregister_from_ncm()

    def remove_all_routers_from_nce(self):
        """Removes routers from NCE that have had NCE loaded onto them. This will remove them from the list
        of NCE devices and therefore will remove it from the UI.
        """
        for router in self.routers:
            router.remove_from_nce()

    def lease_router(self,
                     desired_product: str = None,
                     exclude_products: List[str] = None,
                     physical_only: bool = None,
                     reason: str = None,
                     requested_router: str = None,
                     pool: DevicePool = DevicePool.ROUTERS):
        """

                Args:
                    desired_product: The desired product type. e.g. AER3100
                    exclude_products: A list of product models you would like to exclude from router selection.
                        eg. ["AER3100", "AER2100"]
                    physical_only: If true get any physical device. If false get a 3200v.
                    reason: Reason for leasing out the router. Should typically be the name of the automated test.
                    requested_router: The router_id of a specific ARM router.
                    pool: The router pool used to retrieve the ARM router.

                """

        if desired_product == '3200v' and physical_only is True:
            raise ValueError('The desired_product cannot be "3200v" when requesting only physical devices.')

        if not exclude_products:
            exclude_products = list()

        if desired_product in exclude_products:
            raise ValueError('The desired product cannot match the excluded product.')

        if physical_only is True:
            exclude_products.append('3200v')
        router = None
        client = None

        def arm4_lease():
            if requested_router:
                router, client = self._armv4_fixture.lease_simple_network_by_id(router_id=requested_router, reason=reason)
            else:
                if desired_product == '3200v':
                    router = self._armv4_fixture.lease_router(product_name='3200v')
                    client = None
                else:
                    router, client = self._armv4_fixture.lease_simple_network(product_name=desired_product, reason=reason)
            if router.capability_id in exclude_products:
                # Keep trying - we don't want to get excluded products
                router = None
                client = None
            return router, client

        try:
            router, client = wait(self.lease_wait, 5).for_call(arm4_lease).to_not_be(None)
        except ARMv4Exception as e:
            ARMException(e)
        return router, client

    # def get_wifi_client(self, client_id: str = None, client_type: str = None, reason: str = None) -> ARMWifiClient:
    #     """Gets an available wifi client.
    #
    #     Args:
    #         client_id: The id of the client.  e.g. 'Galaxy_S7_1'
    #         client_type: The type of the client.  e.g. 'android'
    #         reason: The reason for checking out the client.
    #
    #     Returns:
    #         ARMWifiClient: The object representing the client.
    #
    #     """
    #     time_out = time.time() + self.lease_wait
    #     wifi_clients = []
    #
    #     while time.time() < time_out:
    #         wifi_clients = self.client.wifi_clients.get_many(type_=client_type, is_available=True)
    #         if client_id is not None:
    #             wifi_clients = [wifi_client for wifi_client in wifi_clients if wifi_client.id == client_id]
    #         if not wifi_clients:
    #             logger.warning("No available client found. Querying again in 1 second...")
    #             time.sleep(1)
    #         else:
    #             break  # Client found
    #     if not wifi_clients:
    #         raise ARMResourceUnavailable(
    #             'No clean client available matching client_id={} and type={} found in {} seconds.'.format(
    #                 client_id, client_type, self.lease_wait))
    #
    #     chosen_client = random.choice(wifi_clients)
    #     lease = self.lease_wifi_client(chosen_client, reason)
    #     client = ARMWifiClient(
    #         manager=self, connection_config=lease.connection_config, client_id=lease.id, client_type=lease.type)
    #     self.wifi_clients.append(client)
    #     return client

    # def lease_wifi_client(self, wifi_client: ARMWiFiClient, reason: str) -> ARMLeasedWifiClient:
    #     """Lease an ARM wifi client.
    #
    #     Args:
    #         wifi_client: The client to be leased.
    #         reason: The reason for checking out the client.
    #
    #     """
    #     logger.info('Leasing {} from the ARM pool.'.format(wifi_client))
    #     return self.client.wifi_clients.create_lease(wifi_client.id, str(reason))
    #
    # def get_available_wifi_clients_list(self) -> List[ARMWiFiClient]:
    #     """Gets the list of available wifi clients.
    #
    #     Returns:
    #         List[ARMWiFiClient]: The list of available clients.
    #
    #     """
    #     logger.debug('Getting list of available clients from the ARM server.')
    #     return self.client.wifi_clients.get_many(is_available=True)

    # def release_wifi_client(self, wifi_client: ARMWifiClient) -> None:
    #     """Releases a client back to the ARM pool.
    #
    #     Args:
    #         wifi_client: The id of the client.
    #
    #     """
    #     logger.info('Release {} to the ARM pool.'.format(wifi_client))
    #     self.client.wifi_clients.delete_lease(client_id=wifi_client.client_id)

    # def release_all_wifi_clients(self) -> None:
    #     """Releases all known checked out clients to the ARM pool.
    #
    #     """
    #     for client in self.wifi_clients:
    #         self.release_wifi_client(client)

    # def _build_args(self, test_args: Dict[str, Any] = None, reason: str = None) -> Dict[str, Any]:
    #
    #     args = dict()
    #     args['api_key'] = self.api_key
    #     if reason:
    #         args['reason'] = reason
    #     if test_args:
    #         args.update(test_args)
    #
    #     return args

    def teardown(self):
        for router in self.routers:
            router.teardown()
        for wifi_router in self.wifi_routers:
            wifi_router.teardown()
        for wifi_client in self.wifi_clients:
            wifi_client.teardown()
        if self._armv4_fixture:
            self._armv4_fixture.teardown()

    def get_failure_report(self):
        report = str(self)
        report += '\n'

        for router in self.routers:
            report += '\n'
            report += router.get_failure_report()
            report += '\n'

        return report
