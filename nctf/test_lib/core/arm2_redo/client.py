"""ARMClient"""
from typing import Callable
from typing import Dict
from typing import Union

from nctf.test_lib.core.arm2_redo.mission import MissionResponse
from nctf.test_lib.netcloud.fixtures.netcloud_client.client import NetCloudClient


class ARMClient(object):
    """A client that is associated with an ARMRouter.

    Args:
        router_id: ID of the router this client is associated with.
        client_id: ID of this client.
        mission_executor: The method which will execute missions. In this case, The _do_mission method from ARMRouter.
    """

    def __init__(self,
                 router_id: str,
                 client_id: str,
                 mission_executor: Union[Callable[[str, Dict], MissionResponse], None],
                 netcloud_client: NetCloudClient = None):
        self.router_id = router_id
        self.client_id = client_id
        self.mission_executor = mission_executor
        self._netcloud_client = netcloud_client

    def curl(self, url: str) -> MissionResponse:
        """Runs curl on a router's client and returns the response.

        Args:
            url: The URL that will be curl'd.

        Returns:
            The Inception MissionResponse.
        """
        success = True
        try:
            log = self._netcloud_client.exec_command(f'curl --write-out -sL {url} -connect-timeout 40 -m 300')
        except Exception as e:
            success = False
            log = e

        return MissionResponse(success=success, log=log)
