import ast
import datetime
import json
import logging
import re
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple

import pytz
import requests
from requests.exceptions import ConnectionError as ReqConnectionError
from requests.exceptions import ConnectTimeout
from requests.exceptions import Timeout

from nctf.libs.common_library import WaitTimeoutError
from nctf.test_lib.accounts.rest.client import AccServRESTClient
from nctf.test_lib.base.fixtures.fixture import FixtureBase
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import ArtifactoryFwGetter
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2
from nctf.test_lib.ncp.rest.legacy.exceptions import UnexpectedStatusError
from nctf.test_lib.ncp.rest.legacy.overlay import OverlayAPIInterface
from nctf.test_lib.netcloud.fixtures.netcloud_account import NetCloudActor
from nctf.test_lib.netcloud.fixtures.netcloud_client.client import NetCloudClient
from nctf.test_lib.netcloud.fixtures.netcloud_router.router import NetCloudRouter
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture
from nctf.test_lib.utils.sshclient import SSHShellClient
from nctf.test_lib.utils.waiter import wait
from semantic_version import Version

from .client import ARMClient
from .mission import MissionResponse

logger = logging.getLogger('test_lib.core.arm')


class NotRegisteredWithECM(Exception):
    pass


class DeviceNotAvailableError(Exception):
    pass


class ARMRouter(FixtureBase):
    """A router (physical or virtual) that has been leased from ARM."""

    LOG_TIMESTAMP_DATE_RE = r'\d{2}/\d{2}/\d{2}\s'
    LOG_TIMESTAMP_TIME_RE = r'\d{2}:\d{2}:\d{2}\s[AP]M'
    LOG_TIMESTAMP_RE = '(?:{})?{}'.format(LOG_TIMESTAMP_DATE_RE, LOG_TIMESTAMP_TIME_RE)

    def __init__(self,
                 manager: "ARMInterfaceFake",
                 product: str,
                 router_id: str,
                 mac: str,
                 gateway: str,
                 authkey: str,
                 root_authkey: str,
                 remote_admin_port: str,
                 lan_client: str,
                 ssh_port: str,
                 netcloud_router: NetCloudRouter = None,
                 netcloud_client: NetCloudClient = None,
                 netcloud_rest_fixture: NetCloudRESTFixture = None,
                 artifactory_api_key: str = None,
                 artifactory_repo: str = None):
        self._manager = manager
        self.arm_api_timeout = manager.arm_api_timeout

        self.product = product
        self.router_id = router_id
        self.mac = mac
        self.gateway = gateway
        self.authkey = authkey
        self.root_authkey = root_authkey
        self.remote_admin_port = remote_admin_port
        self.remote_ssh_port = ssh_port
        self.session = None
        self.api_digest_auth = None
        self.api = None
        self.cproot_api = None
        self.ecm_api = None
        self._admin_shell_client = None
        self._cproot_shell_client = None

        # Client information
        self.lan_client = lan_client
        self.client = ARMClient(self.router_id, self.lan_client, mission_executor=None, netcloud_client=netcloud_client)

        # Attributes a user may want access to.
        self.ecm_client_id = None
        self.ecm_host = None
        self.ecm_stream_host = None
        self.ecm_accounts_host = None
        self.ecm_username = None
        self.ecm_password = None
        self.ecm_protocol = None
        self.ecm_use_sso = None
        self.ecm_api_session = None
        self._netcloud_router = netcloud_router
        self._netcloud_client = netcloud_client
        self._netcloud_rest_fixture = netcloud_rest_fixture
        self._artifactory_api_key = artifactory_api_key
        self._artifactory_repo = artifactory_repo
        self.post_reboot_cleanup()

    @property
    def name(self) -> str:
        """Dynamically fetches the name of the router in ECM.

        Returns:
            The name of the router.

        Notes:
            This needs to be dynamic in case the router's name is changed.
        """
        return self._netcloud_router.ncm.name

    @property
    def url(self):
        """The externally-accessible HTTP URL for this router."""
        return "http://{}:{}".format(self.gateway, self.remote_admin_port)

    @property
    def fw_version(self) -> Version:
        """Gets the firmware version of the router

        Returns:
            The firmware version of the router in the format: Version("@major.@minor.@patch")
        """
        return self._netcloud_router.ncos.fw_version

    @property
    def group_name(self) -> str:
        """Gets the group name that the router is in, if it's in one.

        Returns:
            The name of the router the router is in, if it's in one, and None otherwise.
        """
        return self._netcloud_router.ncm.group_name

    @property
    def cproot_ssh_client(self) -> SSHShellClient:
        """Provides a router SSH client, authenticated as cproot.

        Returns:
            An SSH client with cproot privileges.
        """
        if self._cproot_shell_client is None:
            host = self.gateway
            username = 'cproot'
            port = self.remote_ssh_port
            authkey = self.root_authkey
            self._cproot_shell_client = SSHShellClient(host=host, username=username, password=authkey, port=port)
            self.enable_cproot()

        return self._cproot_shell_client

    @property
    def ssh_client(self) -> SSHShellClient:
        """Provides a router SSH client.

        Returns:
            An SSH client with regular admin privileges.
        """
        return self._netcloud_router.ncos.get_ssh_client()

    # def _do_mission(self, mission: str, test_args: dict = None) -> MissionResponse:
    #     """Execute a QADEV Inception mission via the ARM REST API.
    #
    #     Args:
    #         mission: The mission (test) as defined by the QADEV Inception project.
    #         test_args: Data required to execute the Inception mission.
    #
    #     Returns:
    #         The result of the Inception mission executed over REST.
    #
    #     """
    #     args = dict()
    #     args['api_key'] = self._manager.api_key
    #     args['mission'] = mission
    #     args['test_args'] = dict()
    #     if test_args:
    #         args['test_args'].update(test_args)
    #
    #     retries = 3
    #     retry_time = 60
    #
    #     # Retry the mission in attempt to get a success.
    #     for i in range(retries):
    #         # FIXME: This new session is created to avoid a ReadTimeout error from ARM.
    #         # Once TES-1269 is resolved this should be revisited.
    #         self._manager.session = requests.Session()
    #         r = self._manager.session.put(
    #             url=self._manager.server + 'routers/{}/mission/'.format(self.router_id),
    #             json=args,
    #             timeout=self.arm_api_timeout)
    #         if r.status_code != 200:
    #             raise ARMException('Unable to execute mission={} on router={}. Server message={}'.format(
    #                 mission, self.router_id,
    #                 r.json()['message']))
    #         task_data = r.json()['data']
    #         task_result = self._get_result_from_task(task_data['task_id'], mission)
    #
    #         # Once we get our result attempt to convert it to a MissionResponse.
    #         try:
    #             result = MissionResponse(task_result)
    #         except Exception as e:
    #             raise ARMException(
    #                 "{} occurred while obtaining mission response for '{}'. The mission task result was '{}'".format(
    #                     type(e), mission, task_result)) from e
    #
    #         if result.success is True:
    #             return result
    #         elif result.success is False and i < (retries - 1):
    #             logger.warning("Mission failed attempt {}/{}. Retrying in {} seconds...".format(i + 1, retries, retry_time))
    #             time.sleep(retry_time)
    #         else:  # If we get here we've failed 3 times and need to return the result anyways.
    #             return result

    # def _get_result_from_task(self, task_id: str, mission: str = None) -> Dict:
    #     """Wait on task given by the ARM REST API to be completed and return the result.
    #
    #     Args:
    #         task_id (str): The ID of the task we're waiting for.
    #
    #     Returns:
    #         The result of the task.
    #
    #     """
    #     try:
    #         timeout_duration = MISSION_TIMEOUTS[mission]
    #     except KeyError:
    #         if mission:
    #             logger.debug('Mission "{}" does not exist. Using default timeout.'.format(mission))
    #         else:
    #             logger.debug('No mission was specified. Using default timeout.')
    #         timeout_duration = MISSION_TIMEOUTS['default']
    #
    #     timeout_time = time.time() + timeout_duration
    #     while timeout_time > time.time():
    #         response = self._manager.session.get(
    #             url=self._manager.server + 'tasks/{}/'.format(task_id), timeout=self.arm_api_timeout)
    #         if response.status_code == 200:
    #             json = response.json()
    #             if json['data']['status'] == "PENDING":
    #                 time.sleep(1)
    #             elif json['data']['status'] == "COMPLETED":
    #                 return response.json()['data']["result"]
    #         else:
    #             raise ARMException('Mission with task_id {} had status_code {}!'.format(task_id, response.status_code))
    #
    #     raise ARMException('MISSION FAILURE - Task {} timed out after {} seconds.'.format(task_id, timeout_duration))

    def get_log(self, grep_str: str = None) -> str:
        """Returns text output from the `log` command on the router CLI.

        Args:
            grep_str: If provided, filter the log with this grep string.
        """
        cmd = 'log' if grep_str is None else 'log | grep {}'.format(grep_str)
        return str(''.join(self.ssh_client.do_cmd(cmd, readlines=True)))

    def dump_log(self):
        logs = self._netcloud_router.ncos.get_logs()
        return MissionResponse(success=True, log=logs)

    @property
    def ecm_firmware_id(self) -> Optional[str]:
        """Returns the firmware ID for this router, according to ECM (may be None)."""
        return self._netcloud_router.ncm.actual_firmware_id

    @property
    def ecm_product_id(self) -> Optional[str]:
        """Returns the product ID for this router, according to ECM (may be None)."""
        return self._netcloud_router.ncm.product_id

    @property
    def ecm_service_state(self) -> str:
        """Returns the state (connection, authorizing, error, etc.) of the ECM service."""
        return self._netcloud_router.ncos.ncm_state

    @property
    def ecm_service_client_id(self) -> Optional[None]:
        """Returns the current ECM Client ID known by the ECM service. May be None."""
        return self._netcloud_router.ncos.ncm_client_id

    @property
    def pgateway_substate(self) -> str:
        """Returns the current pGateway "sub_state", e.g. `'ACTIVE'`, `'REPAIRING'`."""
        return self._netcloud_router.ncos.pgateway_substate

    def register_with_ecm(self,
                          ecm_host: str,
                          ecm_username: str,
                          ecm_password: str,
                          ecm_stream_host: str = None,
                          ecm_accounts_host: str = None,
                          ecm_protocol: str = 'https',
                          ecm_use_sso: bool = True) -> None:
        """Registers this router with ECM.

        Args:
            ecm_host: Hostname of  ECM
            ecm_username: Username given to ECM
            ecm_password: Password given to ECM
            ecm_protocol: Protocol given to ECM (default 'https')
            ecm_stream_host: Hostname of stream server. Defaults to ecm_host if not set
            ecm_accounts_host: Hostname of accounts service. Defaults to accounts-ecm_host if not set
            ecm_use_sso: Boolean for using SSO for registration (default True)

        """
        netcloud_actor = NetCloudActor(username=ecm_username, password=ecm_password)
        netcloud_rest_client = self._netcloud_rest_fixture.get_client(actor=netcloud_actor)
        if not ecm_stream_host:
            ecm_stream_host = ecm_host
        self._netcloud_router.register(
            stream_host=ecm_stream_host,
            ncm_username=ecm_username,
            ncm_password=ecm_password,
            ncm_rest_client=netcloud_rest_client.ncm)

        self.ecm_api_session = self._netcloud_router.ncm.ncm_rest_client.session
        self.ecm_client_id = self._netcloud_router.ncm.router_id
        self.ecm_protocol = ecm_protocol
        self.ecm_host = ecm_host
        self.ecm_accounts_host = ecm_accounts_host
        self.ecm_stream_host = ecm_stream_host
        self.ecm_username = ecm_username
        self.ecm_password = ecm_password
        self.ecm_use_sso = ecm_use_sso
        self.ecm_api = self._netcloud_router.ncm.ncm_rest_client
        self.wait_for_config_sync_in_ecm()

    def associate_client(self) -> MissionResponse:
        """Adds a client behind an ARM router.

        Returns:
            The Inception mission response. This will evaluate to True on mission success, else False.

        """
        args = dict()
        args['args_router1'] = self.router_id
        args['args_client1'] = self.lan_client
        mission = 'associate_client.test'
        return self._do_mission(mission, test_args=args)

    def disassociate_client(self) -> MissionResponse:
        """By default, ARM routers have one client behind them. This method removes the client.

        Returns:
            The Inception mission response. This will evaluate to True on mission success, else False.

        """
        args = dict()
        args['args_router1'] = self.router_id
        args['args_client1'] = self.lan_client
        mission = 'disassociate_client.test'
        return self._do_mission(mission, test_args=args)

    def ping_from_client_to_any_ip(self, destination_ip: str, count: str = '5'):
        """Ping from the router client to the given destination IP address.

        Args:
            destination_ip: The ping target.
            count: The ping count.

        Returns:
            The Inception mission response.

        """
        success = True
        log = None
        try:
            self._netcloud_client.ping(target=destination_ip, count=int(count))
        except Exception as e:
            success = False
            log = e

        return MissionResponse(success=success, log=log)

    def ping_from_client_to_client(self, destination_router, destination_client, count: str = '5'):
        """Ping from the router client to another router client.

        Args:
            destination_router: The ping target router.
            destination_client: The ping target client.
            count: The ping count.

        Returns:
            The Inception mission response.

        """
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        # args['args_router2'] = destination_router
        # args['args_client2'] = destination_client
        # args['args_ping_count1'] = count
        # mission = 'ping_from_client1_to_client2.test'
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError("This is not feasible in the current arm4 setup. Will be available again in physnet")

    def unregister_from_ncm(self):
        """ Sends the call to unregister a device from ncm """
        self._netcloud_router.unregister()

    def unregister_from_ecm(self):
        self._netcloud_router.unregister()

    def modify_firmware(self, fw_version: str) -> None:
        """Installs a new firmware image on the given router.

        `fw_version` must follow this format: coco_6_2_0_Release_2016_09_15
        and must be a subdirectory of guido/nightlies/.
        A firmware binary file for the router's platform must be present in the `fw_version` directory.

        Args:
            fw_version (str): Directory on guido which contains the firmware image that will be installed.

        Raises:
            ARMException: If the Inception mission is not successful.
        """
        getter = ArtifactoryFwGetter(
            api_key=self._artifactory_api_key, repository=self._artifactory_repo, release_ver=fw_version, build_type='release')
        loader = FwLoaderFixtureV2(fw_getter=getter)
        loader.load_fw(router=self._netcloud_router.ncos)
        self.post_reboot_cleanup()

    def enable_cproot(self) -> None:
        """Enables cproot SSH access to a router."""
        self._netcloud_router.ncos.enable_cproot()

    def app_analytics_license(self) -> bool:
        """Checks for the existance of the Application Analytics remote code on a router.

        Returns:
            The status of Application Analytics on a router: True if enabled, False otherwise.
        """
        licenses = self._netcloud_router.ncos.features()['db']
        return any(('Application Analytics' in lic for lic in licenses))

    def sdk_dev_mode_status(self):
        """Checks to see if 'mode' is set to 'devmode' on a router

        Returns:
            The status of the Router SDK mode on a router: True if set to 'devmode', False otherwise
        """
        mode = self._netcloud_router.ncos.sdk_mode
        if mode == 'devmode':
            return True
        else:
            return False

    def sdk_status(self):
        """ Returns the data for status/system/sdk

        """
        return self._netcloud_router.ncos.sdk_data()

    def client_analytics_status(self) -> bool:
        """Checks for the existance of the Client Analytics remote code on a router.

        Returns:
            The status of Client Analytics on a router: True if enabled, False otherwise.
        """
        return self._netcloud_router.ncos.client_analytics_enabled

    def trend_status(self) -> bool:
        """Checks the status of the Trend engine on a router.

        Notes:
            If the ips_info node is present and non-null then Trend is active.

        Returns:
            The status of the Trend engine on a router.
        """
        trend_data = self._netcloud_router.ncos.trend_data()
        if 'ip_info' in trend_data:
            return True
        else:
            return False

    def app_analytics_status(self) -> bool:
        """Checks the status of App Analytics on a router.

        Returns:
            The status of the App Analytics on a router.
        """
        return self._netcloud_router.ncos.app_analytics_data() is not None

    def last_app_push(self):
        """Calculate the amount of time since the last App RC Push to ECM.

        Returns:
            The number of seconds since the last App RC Push, or None if no
            push has occured.
        """
        # TODO: Convert this to use self.get_log() and ARMRouter.timestamp_from_log_line() once this can be tested
        root_ssh = SSHShellClient(host=self.gateway, username='admin', password=self.authkey, port=self.remote_ssh_port)
        res = root_ssh.do_cmd("log | grep Sending\spackets\sto\sserver\.\.\.", readlines=False)
        date_regex = r"\d{2}/\d{2}/\d{2}\s\d{2}:\d{2}:\d{2}\s[AP]M"
        time_regex = r"\d{2}:\d{2}:\d{2}\s[AP]M"
        # Without this tests will fail if they're run in another timezone. Yup...
        tz = pytz.timezone("US/Mountain")

        matches = re.findall(time_regex, str(res))

        if not matches:
            matches = re.findall(date_regex, str(res))
            if not matches:
                return None

        # Get the latest one
        match = matches[-1]
        # Replace the tzinfo. Can't diff them with it present.
        now = datetime.datetime.now(tz).replace(tzinfo=None)
        try:
            # We weren't given year, month, day, so use today's
            last_push = datetime.datetime.strptime(match, '%I:%M:%S %p').replace(year=now.year, month=now.month, day=now.day)
        except ValueError:
            last_push = datetime.datetime.strptime(match, '%m/%d/%y %I:%M:%S %p')

        diff = now - last_push
        return diff.total_seconds()

    def remote_code_present(self, remote_code_name: str) -> bool:
        """Returns whether or not the specified remote code is present on the router.

        This searches via `netcloud trig -v.`"

        Args:
            remote_code_name: String to search for.

        Returns:
            True if the string is present in `netcloud trig -v`, False otherwise.
        """
        # This cmd returns a string that is either just a null-byte, or something like
        # b'\x00    def router_lan_status_trigger(deltas=None, gc_delay=None,\n'
        match_text = self.cproot_ssh_client.do_cmd("netcloud trig -v | grep {}".format(remote_code_name), readlines=False)
        return len(match_text) > 1

    def lan_remote_code_present(self) -> bool:
        return self.remote_code_present('router_lan_status_trigger')

    @classmethod
    def timestamp_from_log_line(cls, log_line: str) -> datetime.datetime:
        """Given a line from a router `log` command, returns a datetime object for it.

        Args:
            log_line: A line from the `log` command to parse the date time.

        Returns:
            A Boise-local datetime object, representing the first timestamp found.
        """
        date_regex = cls.LOG_TIMESTAMP_RE
        time_regex = cls.LOG_TIMESTAMP_TIME_RE

        matches = re.findall(time_regex, log_line)
        if not matches:
            matches = re.findall(date_regex, log_line)
            if not matches:
                raise ValueError("Could not find a timestamp in string: {}".format(log_line))

        match = matches[0]
        try:
            log_time_only = datetime.datetime.strptime(match, '%I:%M:%S %p')
            now = datetime.datetime.now(tz=pytz.timezone("US/Mountain"))
            ts_boise = now.replace(hour=log_time_only.hour, minute=log_time_only.minute, second=log_time_only.second)
        except ValueError:
            ts_boise = pytz.timezone('US/Mountain').localize(datetime.datetime.strptime(match, '%m/%d/%y %I:%M:%S %p'))

        return ts_boise

    def lan_pushes(self) -> List[Tuple[Dict, float]]:
        """Calculate the amount of time since the last LAN data push to ECM.

        Returns:
            A list of tuple pairs of:
                0: The content of the push.
                1: Time since the last push in seconds.
        """
        # We need to extract a dict that was logged over multiple lines. I'm sorry.
        # Group 1 of the match would be the dict, group 0 is the full log line + timestamp of the next line.
        re_date = self.LOG_TIMESTAMP_RE
        re_netcloud_push = r'  \[[ ]{3}DEBUG\] \[[ ]{6}netcloud\] response: ({.*?})\n'
        re_netcloud_whole = re_date + re_netcloud_push + re_date

        log = self.get_log()

        netcloud_push_matches = re.finditer(re_netcloud_whole, log, re.DOTALL)

        lan_pushes = []
        for match in netcloud_push_matches:
            response_json = ast.literal_eval(match.group(1))
            if 'lans' in response_json:
                push_ts = ARMRouter.timestamp_from_log_line(match.group(0))
                now_ts = datetime.datetime.now(tz=pytz.UTC)
                lan_pushes.append((response_json, (now_ts - push_ts).total_seconds()))

        return lan_pushes

    def lan_push_info(self) -> str:
        msg = ""
        for push in self.lan_pushes():
            msg += "Push at {1:.1f} seconds ago with content: {0}\n".format(*push)
        return msg or "No LAN pushes were made"

    def ecm_data(self, uri: str = '', params: Dict = None) -> Dict[Any, Any]:
        """Generic protected method for getting router data from ECM.

        Args:
            uri: Additional path to include in the request. For example,
                uri='lans' would make a request on the /api/v1/routers/<router_id>/lans/
                endpoint.
            params: Additional parameters for the request, e.g. {'expand': 'account'}.

        Returns:
            JSON data about this router from ECM.

        Raises:
            NotRegisteredWithECM: If the router is not registered with ECM.
        """
        if 'expand' in params and uri:
            params['expand'] = f"{params['expand']},{uri}"
        elif uri and params:
            params['expand'] = uri
        elif uri and not params:
            params = dict()
            params['expand'] = uri
        return self._netcloud_router.ncm.ncm_rest_client.routers.detail(
            resource_id=self._netcloud_router.ncm.router_id, params=params)

    def get_router_data_from_ecm(self) -> Dict[Any, Any]:
        """Generic protected method for getting router data from ECM.

        Returns:
            JSON data about this router from ECM.

        Raises:
            NotRegisteredWithECM: If the router is not registered with ECM.
        """
        return self.ecm_data()

    def get_router_dhcp_leases_data_from_ecm(self):
        return self._netcloud_router.ncm.dhcp_leases

    @property
    def ecm_config_status(self) -> str:
        """Fetches the config state of this router from ECM."""
        return self._netcloud_router.ncm.config_status
        # return self.ecm_data()['config_status']

    def wait_for_config_sync_in_ecm(self) -> None:
        """Waits for the ECM router config status to become 'synched'"""
        wait(120, 1).for_call(lambda: self._netcloud_router.ncm.config_status).to_equal('synched')

    def resume_updates_in_ecm(self):
        self._netcloud_router.ncm.resume_config_updates()

    def wait_for_lans_in_ecm(self):
        logger.info('Polling ECM at {} to ensure {} has available LANs.'.format(self.ecm_host, self))

        # We've seen issues with ECM not getting any LANs from the router (ECM-15315). Info added for triage.
        try:
            wait(60, 1).for_call(self.ecm_data, 'lans').to_not_equal([])
        except WaitTimeoutError as e:
            router_lans = self._netcloud_router.ncos.lans
            ecm_lans = self.ecm_data('lans')
            remote_msg = "The LAN remote code was {}present".format('' if self.lan_remote_code_present() else 'NOT ')
            ecm_msg = "ECM reported {} lans: {}".format(len(ecm_lans), ecm_lans)
            router_msg = "Router reported {} lans: {}".format(len(router_lans), router_lans)
            lan_pushes = self.lan_push_info()
            logger.error("TIMEOUT waiting for lans: %s", remote_msg)
            logger.error("TIMEOUT waiting for lans: %s", ecm_msg)
            logger.error("TIMEOUT waiting for lans: %s", router_msg)
            logger.error("TIMEOUT waiting for lans: %s", lan_pushes)
            e_msg = 'Did not have available NCE LANs within 1 minute. Details:\n{}\n{}\n{}\n{}\nLAN Pushes:\n{}' \
                .format(self, remote_msg, ecm_msg, router_msg, lan_pushes)
            raise WaitTimeoutError(e_msg) from e

    def pgateway_ping(self, ping_target):
        # logger.info('{} attempting to ping {} through the NCE Network.'.format(self, ping_target))
        # # args = dict()
        # success = True
        # log = None
        #
        # try:
        #     if isinstance(ping_target, ARMRouter):
        #         # mission = ' ping_between_pGateway_clients.test'
        #         # args['args_router1'] = self.router_id
        #         # args['args_router2'] = ping_target.router_id
        #         self._netcloud_router.ncos.ping(ping_target.gateway, count=3)
        #     else:
        #         # mission = 'ping_from_pGateway_to_any_device_in_network.test'
        #         # args['args_router1'] = self.router_id
        #         # args['args_client_ip1'] = ping_target
        #         self._netcloud_router.ncos.ping(ping_target, count=3)
        # except Exception as e:
        #     success = False
        #     log = e
        #
        # return MissionResponse(success=success, log=log)
        raise NotImplementedError()

    def ping_from_pgateway_external(self, ping_target):
        # logger.info('LAN client (external) of {} attempting to ping {} through the NCE Network.'.format(self, ping_target))
        # args = dict()
        #
        # mission = 'ping_from_external.test'
        # args['args_router1'] = self.router_id
        # args['args_dest_ip1'] = ping_target
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def pgateway_config_entries_received_from_ecm(self):
        # logger.info('Verifying that {} config overlay entries have been received from ECM.'.format(self))
        #
        # mission = 'config_overlay_entries_received_from_ecm.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def pgateway_firewall_rules_added(self):
        # logger.info(
        #     'Verifying that {} appropriate routed and input firewall rules are added for configured LANs.'.format(self))
        #
        # mission = 'appropriate_firewall_rules_added.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def keep_alive_sent(self):
        # logger.info('Verifying that {} keep alive messages are sent every 30 sec'.format(self))
        #
        # mission = 'keep_alive_every_30_sec.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def disable_ecm_service(self):
        logger.info("Disabing the ECM service on {}".format(self))
        self._netcloud_router.ncos.disable_ncm()

    def set_mac_filter(self, enable: bool) -> None:
        """Enable or disable MAC Filtering on a router.

        Args:
            enable: A boolean to enable or disable MAC filtering on a router.

        """
        logger.info(f"{'Enabling' if enable else 'Disabling'} MAC Filtering on {self}")
        if enable:
            self._netcloud_router.ncos.enable_mac_filtering()
        else:
            self._netcloud_router.ncos.disable_mac_filtering()

    def _wait_for_wwan_device(self, device):
        """Wait up to 5 minutes for the wwan-{bssid} interface to become available.

        Args:
            device (str): BSSID for the device to wait for.

        Raises:
            DeviceNotAvailableError: If the device doesn't become available in the given timeframe

        """
        device = f'wwan-{device}'
        logger.info(f"Waiting for {device} to come up on {self}.")
        try:
            wait(300, 1).for_call(self.api.status_wan.get(name="devices")['data']).to_contain(device)
        except WaitTimeoutError as e:
            raise DeviceNotAvailableError(f'The device {device} never became available on {self}.') from e

    def restart_ecm_service(self):
        """Restarts the ECM service on a router."""
        self._netcloud_router.ncos.restart_ncm()

    def enable_wifi_as_wan(self):
        """Enables WiFi as WAN on a router to the point where it appears as a net device in ECM.
        This is required to inject data for that interface, which doesn't appear until it has a profile
        that has been at least partially enabled. This also assumes that there's an available AP in Boise
        that uses `wpa2psk` auth, which I think is a pretty safe assumption, given the number of devices
        broadcasting down there...

        """
        logger.info("Enabling WiFi as WAN on {}".format(self))
        get_url = 'api/status/wlan/radio/0/survey/'
        url = 'api/config/wwan/radio/0'

        res = self.api.get(get_url, expected_status_code=200, timeout=15)

        data = res.json()['data']
        if not len(data):
            raise ValueError('Not enough WiFi Profiles. We will be unable to enable WiFi as WAN from here.')

        profile = None
        # We need to use the 'inception_lb' SSID because of the crazy amount of WiFi noise we get in the Boise office.
        # This is the only one that seems to work consistently.
        for elem in data:
            if elem['ssid'] == 'inception_lb':
                profile = elem

        if not profile:
            raise ValueError("No valid profile available for use.")

        router_data = {
            "mode":
            "wwan",
            "ap_inhibit":
            True,
            "profiles": [{
                "uid": profile["bssid"],
                "bssid": profile["bssid"],
                "enabled": True,
                "ssid": profile["ssid"],
                "authmode": "wpa1wpa2psk"
            }]
        }

        payload = {'data': json.dumps(router_data)}

        self.api.put(url, expected_status_code=200, timeout=15, data=payload)
        self._wait_for_wwan_device(profile['bssid'])

    def reboot(self) -> None:
        """Reboot this router via the API."""
        self._netcloud_router.reboot()

    def wait_for_reachable(self, timeout=300):
        logger.info("Waiting until router: {} is reachable again".format(self))

        def check_api():
            try:
                return self._netcloud_router.ncos.get_fw_info()
            except (ReqConnectionError, ConnectTimeout, Timeout):
                return None

        try:
            wait(timeout, 1).for_call(check_api).to_not_be(None)
        except WaitTimeoutError as e:
            raise WaitTimeoutError("Was not able to reach the {self}") from e

    def wait_for_api_reachable(self, timeout: int = 300) -> None:
        """Waits for the router's API to be come reachable.

        Args:
            timeout: Time to wait before giving up on the API.

        Note:
            This isn't always reachable when the web UI is after a reboot
            so API calls may fail until this is reachable.
        """
        self.wait_for_reachable(timeout=timeout)

    def get_state_history(self):
        """Get the wan_state_history from for this router.

        Returns:
            Response: Response from the router.
        """
        return self._netcloud_router.ncos.wan_state_history

    def pdiagnostics(self):
        # logger.info('Verifying that {} pDiagnostics can be triggered from control tree'.format(self))
        #
        # mission = 'pDiagnostics.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def pdiagnostics_client_stopped(self):
        # logger.info(
        #     'Verifying that {} pDiagnostics doesn\'t generate any data when overlay client is not running'.format(self))
        #
        # mission = 'pDiagnostics_client_installed_but_stopped.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def half_tunnel_suppression(self):
        # logger.info('Verifying that {} overlay client suppresses the destination network in half tunnel mode'.format(self))
        #
        # mission = 'half_tunnel_suppression.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def client_logs_to_syslog_only(self):
        # logger.info('Validating that {} NCE Client logs to syslog only.'.format(self))
        #
        # mission = 'validate_pGateway_logs_syslog_only.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def dns_entries_cleared_router_reboot(self):
        # logger.info('Validating that {} DNS entries are cleared from the router on reboot.'.format(self))
        # self.pre_reboot_setup()
        #
        # mission = 'dns_entries_cleared_router_reboot.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # result = self._do_mission(mission, test_args=args)
        # self.post_reboot_cleanup()
        # return result
        raise NotImplementedError()

    def overlay_suppression(self):
        # logger.info('Validating that {} overlay client suppresses the overlay networks when it is uninstalled.'.format(self))
        #
        # mission = 'overlaySuppression.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    """Clientless 2 Router Missions"""

    def status_tree_count_increases(self, router2):
        # logger.info(
        #     'Validating that {} status tree count increases when traffic is passed through overlay client.'.format(self))
        #
        # mission = 'pass_traffic_status_tree_count_increases.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_router2'] = router2.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def overlay_client_with_ipsec_tunnel(self, router2):
        # logger.info('Validating that {} NCE Client is running along with IPSec tunnels.'.format(self))
        #
        # mission = 'pGateway_client_running_along_with_IPSec_tunnel.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_router2'] = router2.router_id

        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def overlay_client_new_lan_added(self, router2):
        # logger.info('Validating that {} appropriate firewall rules are added when a new LAN is added.'.format(self))
        #
        # mission = 'pGateway_client_running_new_lan_added.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_router2'] = router2.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def overlay_client_modify_lan(self, router2):
        # logger.info('Validating that {} firewall rules are modified accordingly on LAN changes.'.format(self))
        #
        # mission = 'pGateway_client_running_modify_lan_associated.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_router2'] = router2.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    """Client Associated Missions"""

    def dpd_msg_logged(self):
        # logger.info('Validating that {} has appropriate DPD messages logged.'.format(self))
        #
        # mission = 'appropriate_dpd_messages_logged.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def status_wan_disconnect(self, disconnect_duration_in_seconds: int = 0) -> MissionResponse:
        """Performs a WAN disconnect mission and validates the overlay client status changes

        args:
            disconnect_duration_in_seconds: The amount of time in seconds the WAN will be disconnected for

        Returns:
            The result of the Inception mission
        """
        # logger.info('Validating that {} overlay client status changes on wan disconnect.'.format(self))
        #
        # mission = 'client_status_wan_disconnect.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        # if disconnect_duration_in_seconds > 0:
        #     args['args_wait'] = disconnect_duration_in_seconds
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def router_services_lan_gateway(self):
        # logger.info('Validating that {} overlay client respects router services lan gateway feature.'.format(self))
        #
        # mission = 'router_services_lan_gateway_respected.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def overlay_client_factory_reset(self):
        # logger.info('Validating that {} client purged on factory reset.'.format(self))
        #
        # mission = 'factory_reset_client_purged.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def overlay_client_failover_failback(self):
        # logger.info(
        #     'Validating that {} overlay client fails over and fails back with WAN fail over and fail back.'.format(self))
        #
        # mission = 'client_failover_failback.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    """3 Router Missions"""

    def control_dns_entries(self, router2, router3):
        # logger.info('Validating that {} DNS queries are routed through overlay tunnel.'.format(self))
        #
        # mission = 'dns_queries_routed_through_overlay_tunnel.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # args['args_router2'] = router2.router_id
        # args['args_client2'] = router2.lan_client
        #
        # args['args_router3'] = router3.router_id
        # args['args_client3'] = router3.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def reverse_control_dns_entries(self, router2, router3):
        # logger.info('Validating that {} reverse DNS works for control DNS entries.'.format(self))
        #
        # mission = 'reverse_dns_for_control_dns_entries.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # args['args_router2'] = router2.router_id
        # args['args_client2'] = router2.lan_client
        #
        # args['args_router3'] = router3.router_id
        # args['args_client3'] = router3.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def suppression(self):
        # logger.info('Validating that {} suppression works.'.format(self))
        #
        # mission = 'suppression.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def firewall_rules_for_control_firewall_entries(self):
        # logger.info('Validating that {} firewall rules are added for control firewall entries.'.format(self))
        #
        # mission = 'firewall_rules_for_control_firewall_entries.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def pdiagnostics_client_not_installed(self):
        # logger.info(
        #     'Validating that {} pDiagnostics doesn\'t generate any data when overlay client is not installed.'.format(self))
        #
        # mission = 'pDiagnositics_client_not_installed.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def config_overlay_entries(self):
        # logger.info('Validate {} config/overlay values.'.format(self))
        #
        # mission = 'validate_config_overlay_entries.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def dns_servers_different_domains(self):
        # logger.info('Validating that {} appropriate DNS servers are used for DNS query resolution.'.format(self))
        #
        # mission = 'dns_servers_for_different_domains_pGateway.test'
        #
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def client_access_url(self, url, expected_output):
        # args = dict()
        # args['args_router1'] = self.router_id
        # args['args_client1'] = self.lan_client
        # args['args_url'] = url
        # args['args_output'] = expected_output
        #
        # logger.info('Validating that content of URL {} accessed from client {} contains {}'.format(
        #     url, self.lan_client, expected_output))
        #
        # mission = 'client_access_url.test'
        #
        # return self._do_mission(mission, test_args=args)
        raise NotImplementedError()

    def remove_from_nce(self) -> None:
        """Removes router from NCP if it has the NCP client loaded onto it.

        This will remove it from the list of NCP devices and therefore will
        remove it from the UI.

        Notes:
            We can only load the NCP client onto routers that have been
            registered with NCM and then only if they are on an account that
            is not legacy ECM (SSO account).
        """
        if self.ecm_client_id is not None and self.ecm_use_sso is True:
            logger.info("Removing router %s from NCP Devices list", self)
            router_data = self.ecm_data()

            overlay_api_url = '{}://api-overlay-{}'.format(self.ecm_protocol, self.ecm_host)
            overlay_api = OverlayAPIInterface(overlay_api_url, session=self.ecm_api_session)

            try:
                ncp_devices = overlay_api.devices.list()['data']
            except UnexpectedStatusError as e:
                if e.status_code == 403:
                    logger.info("Skipping NCP client cleanup; no permissions to see NCP devices.")
                    return
                raise e

            found_match = False
            for ncp_device in ncp_devices:
                if 'hostName' in ncp_device and ncp_device['hostName'] == router_data['name']:
                    if found_match:
                        logger.warning("Found more than one NCP device with hostname %s", router_data['name'])
                    found_match = True
                    logger.info("Found NCP device with matching hostname -- removing")
                    device_delete_uri = '/api/1/computer/{}'.format(ncp_device['clientId'])
                    overlay_api.delete(device_delete_uri, 200)

            if not found_match:
                logging.warning("Could not find matching NCP Client for %s", self)

    def rename_router_to_ecm_client_id(self) -> None:
        """Renames the router by the ECM client id through the ECM API."""
        self._netcloud_router.ncm.change_name_to_id()

    @staticmethod
    def get_sso_session(accounts_protocol, accounts_root, ecm_username, ecm_password):
        sso_session = requests.Session()
        AccServRESTClient(accounts_protocol, accounts_root, 443, session=sso_session).authenticate(ecm_username, ecm_password)
        return sso_session

    def __str__(self):
        return "<ARMRouter(router_id='{}', product='{}', ecm_client_id='{}')>".format(self.router_id, self.product,
                                                                                      self.ecm_client_id)

    def get_failure_report(self):
        report = str(self)
        report += '\n'

        report += '***ATTRIBUTES**'
        report += '\n'

        for attr in vars(self):
            report += '{} = {}'.format(attr, getattr(self, attr))
            report += '\n'

        report += 'ROUTER LOG DUMP'
        report += '\n'

        report += self.dump_log().log
        return report

    def teardown(self):
        pass

    def set_session_auth(self) -> None:
        """Sets the session and Router API member variables"""
        self.api = self._netcloud_router.ncos.get_rest_client()
        self.session = self.api.session
        self.session.verify = False

    def pre_reboot_setup(self) -> None:
        """Setting readonlyconfig to false so our router saves the config to flash -- see ECM-16524"""
        log_level = self.api.get('/api/config/system/logging/', 200).json()['data']['level']
        self._netcloud_router.ncos.exec_command('set /config/system/logging/level "critical"')
        self.cproot_ssh_client.do_cmd("set control/system/readonlyconfig false")
        self._netcloud_router.ncos.exec_command(command=f'set /config/system/logging/level "{log_level}"')

    def post_reboot_cleanup(self) -> None:
        """Reset internal connections to avoid errors with API & SSH on reboot."""
        self.set_session_auth()
        self._admin_shell_client = None
        self._cproot_shell_client = None
