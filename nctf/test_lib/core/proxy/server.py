"""Provides a proxy server for HTTP and HTTPS requests.

Primary purpose is to be able to record HAR files on request.
"""
import logging
import os
import platform
import signal
import subprocess
import time

import browsermobproxy
from browsermobproxy.exceptions import ProxyServerError
from nctf.test_lib.base.fixtures.fixture import FixtureBase

from . import client

PATH = (os.path.join(os.path.dirname(os.path.abspath(__file__)), 'browsermob-proxy', 'bin'))
os.environ['PATH'] += ':{}'.format(PATH)
logger = logging.getLogger('proxy.server')


# TODO: There's an existing PR for this, https://github.com/AutomatedTester/browsermob-proxy-py/pull/71/files
# TODO: Update and remove monkey patch if/once this PR is merged
# Monkey-patch browsermob's server connector to close the process group, not just the bash launcher script
def monkeypatch_browsermobproxy_start(self, options=None):
    if options is None:
        options = {}
    log_path = options.get('log_path', os.getcwd())
    log_file = options.get('log_file', 'server.log')
    retry_sleep = options.get('retry_sleep', 0.5)
    retry_count = options.get('retry_count', 60)
    log_path_name = os.path.join(log_path, log_file)
    self.log_file = open(log_path_name, 'w')

    self.process = subprocess.Popen(self.command, preexec_fn=os.setsid, stdout=self.log_file, stderr=subprocess.STDOUT)
    count = 0
    while not self._is_listening():
        if self.process.poll():
            message = ("The Browsermob-Proxy server process failed to start. "
                       "Check {0}"
                       "for a helpful error message.".format(self.log_file))

            raise ProxyServerError(message)
        time.sleep(retry_sleep)
        count += 1
        if count == retry_count:
            self.stop()
            raise ProxyServerError("Can't connect to Browsermob-Proxy")


def monkeypatch_browsermobproxy_stop(self):
    if self.process.poll() is not None:
        return
    os.killpg(os.getpgid(self.process.pid), signal.SIGKILL)
    self.process.wait(timeout=30)
    self.log_file.close()


# macOS does not require the binary invocation to be prepended with 'sh'.
def monkeypatch_browsermobproxy__init__(self, path='browsermob-proxy', options=None):
    """
    Initialises a Server object

    :param str path: Path to the browsermob proxy batch file
    :param dict options: Dictionary that can hold the port.
        More items will be added in the future.
        This defaults to an empty dictionary
    """
    options = options if options is not None else {}

    path_var_sep = ':'
    if platform.system() == 'Windows':
        path_var_sep = ';'
        if not path.endswith('.bat'):
            path += '.bat'

    exec_not_on_path = True
    for directory in os.environ['PATH'].split(path_var_sep):
        if os.path.isfile(os.path.join(directory, path)):
            exec_not_on_path = False
            break

    if not os.path.isfile(path) and exec_not_on_path:
        logger.warning(f"Browsermob-Proxy binary couldn't be found in path provided: {path}.")

    self.path = path
    self.host = 'localhost'
    self.port = options.get('port', 8080)
    self.process = None

    if platform.system() == 'Darwin':
        # self.command = ['sh']  # macOS doesn't work with 'sh' prepended to the command.
        self.command = []
    else:
        self.command = []
    self.command += [path, '--port=%s' % self.port]


browsermobproxy.Server.start = monkeypatch_browsermobproxy_start
browsermobproxy.Server.stop = monkeypatch_browsermobproxy_stop
browsermobproxy.Server.__init__ = monkeypatch_browsermobproxy__init__


class BrowserMobProxyFixture(FixtureBase):
    """An interface to manage browsermob-proxy and open clients to it.

    Most utility will be accomplished via `get_client()`, as a client can be
    used to direct selenium, requests, etc. to the proxy for recording traffic.

    Args:
        path: The path to the browsermob-proxy executable. Defaults to just the
            executable name (assumes it's in the PATH).
    """

    def __init__(self, path: str = 'browsermob-proxy'):
        self.server = browsermobproxy.Server(path)
        self.artifacts_dir = None
        self.clients = []

    def start(self, log_file: str = 'browsermob-proxy.log', artifacts_dir=None) -> None:
        """Starts the proxy service as specified.

        Args:
            log_file: The log file to be used for the proxy server.
            artifacts_dir: Folder to use for the log file and exported HAR
                files. Defaults to CWD.
        """
        if self.is_running:
            logger.warning("Proxy server already running. Ignoring start command.")
            return

        if artifacts_dir is None:
            artifacts_dir = os.getcwd()
        self.artifacts_dir = artifacts_dir

        proxy_options = {'log_file': log_file, 'log_path': self.artifacts_dir}

        logger.info("Starting BrowserMob proxy server (logfile: %s/%s)", self.artifacts_dir, log_file)
        self.server.start(options=proxy_options)
        logger.info("Proxy server started")

    def get_client(self, tag: str = "", capture_traffic: bool = True) -> 'client.BrowserMobProxyClient':
        """Fetches a new client to this proxy, with an optional tag for user tracking.

        The returned proxy client can be used for selenium, requests, etc.
        where a proxy specification is desired.
        The client will also record a HAR file by default.

        Args:
            tag: Optional name/reference for the HAR. Used for naming the
                exported .har file.
            capture_traffic: Whether or not to record a HAR file for traffic
                directed through the proxy. Defaults to True.

        Returns:
            An initialized proxy client.
        """
        new_client = client.BrowserMobProxyClient(self, tag)
        new_client.start(capture_traffic)
        self.clients.append(new_client)
        logger.info("Created proxy client %s", new_client)
        return new_client

    def stop(self) -> None:
        logger.info("Attempting to cleanly stop proxy clients before shutting down proxy server")
        for pc in self.clients:
            try:
                pc.stop()
            except Exception as e:
                # If we can't shut down cleanly, no big deal. The process will be killed anyways.
                logger.warning("Ignoring error from client %s delete: %s", pc, e)
        del self.clients[:]

        logger.info("Stopping proxy server %s", self)
        self.server.stop()

    @property
    def is_running(self) -> bool:
        if not self.server.process:
            return False
        return self.server.process.poll() is None

    def get_failure_report(self) -> str:
        msg = ""
        if not self.clients:
            return "No HAR files to write (no proxy clients)"
        for c in self.clients:
            logger.info("Writing out HAR file for %s", c)
            msg += "Wrote out HAR file: {}\n".format(self.write_har(c))
        return msg

    def teardown(self) -> None:
        if self.is_running:
            self.stop()

    def write_har(self, proxy_client: 'client.BrowserMobProxyClient', tag_override: str = None) -> str:
        """Writes a HAR file for a proxy client to disk.

        The naming will be as such,
            `<client_tag>_<timestamp>.har`
        Or, if no tag is provided,
            `proxy_export_<timestamp>.har`

        Args:
            proxy_client: The proxy client, used to get a JSON representation
                of the HAR file to write out.
            tag_override: A substring to use in the file name, overrides the client's tag.

        Returns:
            The path of the HAR file that was saved.
        """
        if tag_override:
            tag = tag_override
        elif proxy_client.tag:
            tag = proxy_client.tag
        else:
            tag = 'proxy_export'
        cur_time = time.strftime('%Y-%m-%d_%Hh%Mm%Ss', time.gmtime())
        file_name = "{}_{}.har".format(tag, cur_time)
        file_path = os.path.join(self.artifacts_dir, file_name)
        with open(file_path, 'w') as f:
            f.write(proxy_client.har_content())
        return file_path

    def __str__(self) -> str:
        return "<BrowserMobProxyServer(running={}, url='{}')>".format(self.is_running, self.server.url)
