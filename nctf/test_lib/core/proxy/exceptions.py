class ProxyNotStartedError(Exception):
    """Raised when the proxy server must be started to perform an action, but isn't."""
