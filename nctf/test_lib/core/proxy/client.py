"""Provides a client (active host and port) for directing HTTP and HTTPS requests to a proxy server.

Primary purpose is to be able to provide HAR files on request.
"""
import functools
import json
import logging
from typing import Any
from typing import Callable

from . import exceptions

logger = logging.getLogger('test_lib.proxy')


def require_server(proxy_method: Callable[..., Any]):
    """Requires that the parent proxy server is running before making the method call.

    Raises:
        ProxyNotStartedError: If the proxy is not started before calling the decorated method.
    """

    @functools.wraps(proxy_method)
    def func_wrapper(self, *args, **kwargs):
        if not self.proxy.is_running:
            raise exceptions.ProxyNotStartedError("The parent Proxy server has not been initialized!")
        return proxy_method(self, *args, **kwargs)

    return func_wrapper


class BrowserMobProxyClient(object):

    PROXY_OPTION_CAPTURE_ALL = {'captureHeaders': True, 'captureContent': True, 'captureBinaryContent': True}

    def __init__(self, parent_proxy: 'BrowserMobProxyServer', tag: str):
        self.tag = tag
        self.proxy = parent_proxy
        self.bm_client = None
        self.recording = False

    @property
    def selenium_proxy(self) -> 'selenium.webdriver.common.proxy.Proxy':
        return self.bm_client.selenium_proxy()

    @require_server
    def start(self, record_traffic: bool = True) -> None:
        logger.info("Creating proxy client %s", self)
        self.bm_client = self.proxy.server.create_proxy()
        if record_traffic:
            logger.info("Recording traffic via proxy (tag='%s')", self.tag)
            self.bm_client.new_har(self.tag, BrowserMobProxyClient.PROXY_OPTION_CAPTURE_ALL)
        self.recording = record_traffic

    @require_server
    def stop(self) -> None:
        logger.info("Shutting down proxy client %s", self)
        status = self.bm_client.close()
        if status != 200:
            logger.warning("Proxy server responded with %s when closing port for %s", status, self)

    @require_server
    def har_content(self) -> str:
        """Retrieves, as a string, a JSON representation of the recorded traffic so far."""
        if not self.recording:
            return ""
        return json.dumps(self.bm_client.har, ensure_ascii=False)

    def __str__(self) -> str:
        port = self.bm_client.port if self.bm_client else "Not initialized"
        return "<BrowserMobProxyClient('{}', port='{}', recording='{}')>".format(self.tag, port, self.recording)
