import decimal
import functools
import numbers
import operator
import sys
import time

import pytest

from nctf.libs.common_library import require_type
from nctf.libs.common_library import wait_for
from nctf.libs.common_library import wait_for_with_interval
from nctf.libs.common_library import WaitTimeoutError

from nctf.test_lib.utils.waiter import wait


def track_calls(func):
    """Keeps track of the number of calls on a function.

    The number of calls can be accessed via <original_func>.calls
    """

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        if not hasattr(func_wrapper, 'calls'):
            func_wrapper.calls = 0
        func_wrapper.calls += 1
        return func(*args, **kwargs)

    return func_wrapper


class FuncInfo(object):
    """Wraps a function with some stats.

    Attributes:
        func (Callable): A method, function, etc to keep track of.
        func.calls (int): The number of calls to the function.
    """

    def __init__(self, func):
        if not callable(func):
            raise TypeError("FuncInfo requires a callable object!")
        self.func = track_calls(func)
        self.func.calls = 0


def do_nothing(*args, **kwargs):
    pass


@pytest.mark.common
class TestRequireType(object):
    class _A(object):
        pass

    class _B(_A):
        pass

    def expected_message_builder(self, expected_type, expected_module, actual_type, actual_module):
        """Helper for matching the expected assertion messages from require_type

        Args:
            expected_type (str): Name of type expected by require_type.
            expected_module (str): Module name of type expected by require_type.
            actual_type (str): Name of type given by object to require_type.
            actual_module (str): Module name    of type given by object expected by require_type.

        Returns:
            str: The assert message to expect if require_type raises a TypeError
        """
        # This is deliberately done a bit different from how require_type is implemented.
        test_name = sys._getframe().f_back.f_code.co_name
        return "Function {} in {} expected {} ({}), not {} ({})".format(
            test_name, self.__class__.__name__, expected_type, expected_module, actual_type, actual_module)

    def test_pass_on_simple_types(self):
        """Tests basic isinstance-like usage in cases expected to pass."""
        a_obj = TestRequireType._A()
        require_type(1, int)
        require_type(1.0, float)
        require_type(1, numbers.Number)
        require_type(1.0, numbers.Number)
        require_type(decimal.Decimal(1), numbers.Number)
        require_type('str', str)
        require_type({'data': True}, dict)
        require_type([None], list)
        require_type(a_obj, TestRequireType._A)

    def test_pass_on_tuples_of_types(self):
        """Tests tuple isinstance-like support for specifying multiple possible types in cases expected to pass."""
        require_type(1, (int, float))
        require_type(1.0, (int, float))
        require_type('str', (str, int))
        require_type('str', (int, str))

    def test_pass_on_extended_classes(self):
        """Tests that if B extends A, require_type(b_instance, A) passes."""
        a_obj = TestRequireType._A()
        b_obj = TestRequireType._B()

        require_type(a_obj, TestRequireType._A)
        require_type(a_obj, TestRequireType._A)
        require_type(b_obj, TestRequireType._B)

    def test_pass_on_nonetype_as_type(self):
        """Specific passing case for proper handling of None/NoneType (ie. like isinstance(None, type(None)))."""
        obj = None
        require_type(obj, type(None))

    def test_correct_function_in_exception_message(self):
        """Specifically test that this function name (and this test class) is in the TypeError's message."""
        with pytest.raises(TypeError) as te:
            require_type(True, str)

        assert 'test_correct_function_in_exception_message' in str(te)
        assert 'TestRequireType' in str(te)

    def test_exception_on_wrong_type_builtins(self):
        """Tests that if an object is not of the expected type, an appropriate exception and message is raised."""
        with pytest.raises(TypeError) as te:
            require_type(1, bool)

        expected_msg = self.expected_message_builder('bool', 'builtins', 'int', 'builtins')
        assert expected_msg in str(te), 'Expected message with: "{}"\nGot message: "{}"'.format(expected_msg, str(te))

    def test_exception_on_wrong_type_custom_class(self):
        """Tests that if an object is not of the expected type, an appropriate exception and message is raised."""
        a_obj = TestRequireType._A()
        with pytest.raises(TypeError) as te:
            require_type(a_obj, TestRequireType._B)

        expected_msg = self.expected_message_builder('_B', 'common_library_test', '_A', 'common_library_test')
        assert expected_msg in str(te), 'Expected message with: "{}"\nGot message: "{}"'.format(expected_msg, str(te))

    def test_exception_on_wrong_type_with_tuple(self):
        """Tests that if an object is none of the types specified, an appropriate exception and message is raised."""
        a_obj = TestRequireType._A()
        with pytest.raises(TypeError) as te:
            require_type(a_obj, (numbers.Number, type(None)))

        expected_msg = "Function test_exception_on_wrong_type_with_tuple in TestRequireType expected " \
                       "Number (numbers) or NoneType (builtins), not _A (common_library_test)"
        assert expected_msg in str(te), 'Expected message with: "{}"\nGot message: "{}"'.format(expected_msg, str(te))

    def test_reject_non_type_or_tuple_of_types(self):
        """Test rejection of the type argument not being a type (or tuple of types)."""
        obj = None
        with pytest.raises(TypeError) as te:
            require_type(obj, None)

        expected_msg = "TypeError: expected_types parameter must be a type or tuple of types, not NoneType (builtins)"
        assert expected_msg in str(te), 'Expected message with: "{}"\nGot message: "{}"'.format(expected_msg, str(te))

    def test_item_in_tuple_is_not_a_type(self):
        """Tests that if an object is none of the types specified, an appropriate exception and message is raised."""
        a_obj = TestRequireType._A()
        with pytest.raises(TypeError) as te:
            require_type(a_obj, (1, True, TestRequireType._A))

        expected_msg = "TypeError: expected_types parameter must be a type or tuple of types, but got tuple " \
                       "containing int (builtins)"
        assert expected_msg in str(te), 'Expected message with: "{}"\nGot message: "{}"'.format(expected_msg, str(te))


@pytest.mark.common
class TestWaitFor(object):
    """ Helpers """

    @pytest.fixture
    def incrementer(self):
        return FuncInfo(do_nothing)

    """ Parameter verification """

    def test_reject_non_callable(self):
        with pytest.raises(TypeError) as te:
            wait_for(1, True, True)

        assert "Cannot wait for non-callable object. Got 'bool' type instead." in str(te)

    def test_reject_non_numeric_timeout(self):
        with pytest.raises(TypeError) as te:
            wait_for(dict(), None, do_nothing)

        assert "Function wait_for_with_interval expected Number (numbers), not dict (builtins)" in str(te)

    def test_reject_non_numeric_interval(self):
        with pytest.raises(TypeError) as te:
            wait_for_with_interval(1, dict(), None, do_nothing)

        assert "Function wait_for_with_interval expected Number (numbers), not dict (builtins)" in str(te)

    """ Basic tests """

    def test_simple_return(self):
        assert wait_for(1.0, 100, lambda x: x + x, 50) == 100
        assert wait(1.0).for_call(lambda x: x + x, 50).to_equal(100) == 100

    def test_simple_timeout_exception_message(self):
        exp_exc_msg = 'common_library.WaitTimeoutError: Function call "<lambda>(50)" timed out (timeout=1.0 sec):' \
                      ' Got final result of "100", did not get expected result of "50"'
        with pytest.raises(WaitTimeoutError) as wte1:
            wait_for(1.0, 50, lambda x: x + x, 50)
        assert exp_exc_msg in str(wte1), "Got unexpected message: {}".format(str(wte1))

        with pytest.raises(WaitTimeoutError) as wte2:
            wait(1.0).for_call(lambda x: x + x, 50).to_equal(50)
        assert exp_exc_msg in str(wte2), "Got unexpected message: {}".format(str(wte2))

    def test_uses_custom_compare(self):
        wait_for(1.0, 50, lambda x: x + x, 50, _operator=operator.gt)
        wait(1).for_call(lambda x: x + x, 50).to_be_gt(50)

    """ Interval functionality"""

    def test_instant_pass(self, incrementer):
        """Ensure that if the first call to a function is successful, the return is immediate."""
        start_time = time.process_time()  # Use CPU time to avoid scheduling time diffs
        wait_for_with_interval(2.0, 1.0, None, incrementer.func)
        end_time = time.process_time()
        time_diff = end_time - start_time

        assert incrementer.func.calls == 1, \
            "Expected single call to function, but it was called {} times".format(incrementer.func.calls)
        assert 0.0 < time_diff < 0.01, \
            "Expected tautological test to succeed immediately, but took {}s".format(time_diff)

    def test_call_at_least_once(self, incrementer):
        """Ensures at least one call to the function is made, regardless of timeout and interval."""
        wait_for_with_interval(0.0, 0.0, None, incrementer.func)
        assert incrementer.func.calls == 1, \
            "Expected a single call to function, but it was called {} times".format(incrementer.func.calls)

    def test_call_at_least_once_waiter(self, incrementer):
        """Ensures at least one call to the function is made, regardless of timeout and interval."""
        wait(0).for_call(incrementer.func).to_be(None)
        assert incrementer.func.calls == 1, \
            "Expected a single call to function, but it was called {} times".format(incrementer.func.calls)

    def test_multiple_interval_calls(self, incrementer):
        """Ensure that calls are separated by an interval, and eventually fails."""
        timeout = 2.0
        interval = 0.8
        expected_timeout = 1.6  # Shouldn't go over 2.0s
        start_time = time.monotonic()
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for_with_interval(timeout, interval, "SomeString", incrementer.func)
        end_time = time.monotonic()
        time_diff = end_time - start_time

        assert incrementer.func.calls == 3, \
            "Expected 3 calls to function, but it was called {} times".format(incrementer.func.calls)
        assert (expected_timeout - 0.1) < time_diff < (expected_timeout + 0.1), \
            "Expected wait to be around {}s, but took {}s".format(expected_timeout, time_diff)
        assert 'common_library.WaitTimeoutError: Function call "do_nothing()" timed out (timeout=2.0 sec): Got final' \
               ' result of "None", did not get expected result of "\'SomeString\'"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_multiple_interval_calls_waiter(self, incrementer):
        """Ensure that calls are separated by an interval, and eventually fails."""
        timeout = 2.0
        interval = 0.8
        expected_timeout = 1.6  # Shouldn't go over 2.0s
        start_time = time.monotonic()
        with pytest.raises(WaitTimeoutError) as wte:
            wait(timeout, interval).for_call(incrementer.func).to_equal("SomeString")
        end_time = time.monotonic()
        time_diff = end_time - start_time

        assert incrementer.func.calls == 3, \
            "Expected 3 calls to function, but it was called {} times".format(incrementer.func.calls)
        assert (expected_timeout - 0.1) < time_diff < (expected_timeout + 0.1), \
            "Expected wait to be around {}s, but took {}s".format(expected_timeout, time_diff)
        assert 'common_library.WaitTimeoutError: Function call "do_nothing()" timed out (timeout=2.0 sec): Got ' \
               'final' \
               ' result of "None", did not get expected result of "\'SomeString\'"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_rapid_calls(self, incrementer):
        """Ensure that calls are rapid when no interval is used, and eventually fails."""
        timeout = 1
        start_time = time.monotonic()
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(timeout, "SomeString", incrementer.func)
        end_time = time.monotonic()
        time_diff = end_time - start_time

        assert incrementer.func.calls > 10000, \
            "Expected many calls to function, but it was only called {} times".format(incrementer.func.calls)
        assert (timeout - 0.1) < time_diff < (timeout + 0.1), \
            "Expected wait to be around {}s, but took {}s".format(timeout, time_diff)
        assert 'common_library.WaitTimeoutError: Function call "do_nothing()" timed out (timeout=1 sec): Got final' \
               ' result of "None", did not get expected result of "\'SomeString\'"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    """ Error message verification (func params included) """

    def test_no_args_exception(self):
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(0.5, True, do_nothing)

        assert 'common_library.WaitTimeoutError: Function call "do_nothing()" timed out (timeout=0.5 sec): ' \
               'Got final result of "None", did not get expected result of "True"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_single_positional_int_exception(self):
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(0.5, True, do_nothing, 1)

        assert 'common_library.WaitTimeoutError: Function call "do_nothing(1)" timed out (timeout=0.5 sec): ' \
               'Got final result of "None", did not get expected result of "True"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_single_positional_str_exception(self):
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(0.5, 'expected string', do_nothing, 'string')

        assert 'common_library.WaitTimeoutError: Function call "do_nothing(\'string\')" timed out (timeout=0.5 sec):' \
               ' Got final result of "None", did not get expected result of "\'expected string\'"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_single_keyword_int_exception(self):
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(0.5, [], do_nothing, var1=1)

        assert 'common_library.WaitTimeoutError: Function call "do_nothing(var1=1)" timed out (timeout=0.5 sec): ' \
               'Got final result of "None", did not get expected result of "[]"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_single_keyword_str_exception(self):
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(0.5, False, do_nothing, 'string')

        assert 'common_library.WaitTimeoutError: Function call "do_nothing(\'string\')" timed out (timeout=0.5 sec):' \
               ' Got final result of "None", did not get expected result of "False"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))

    def test_positional_and_keyword_dict_exception(self):
        with pytest.raises(WaitTimeoutError) as wte:
            wait_for(0.5, {'status': 'OK'}, do_nothing, '/api/v1/users', data={'data': True})

        assert 'common_library.WaitTimeoutError: Function call "do_nothing(\'/api/v1/users\', data={\'data\': True})' \
               '" timed out (timeout=0.5 sec): Got final result of "None", did not get expected result of ' \
               '"{\'status\': \'OK\'}"' in str(wte), \
            "Got unexpected message: {}".format(str(wte))
