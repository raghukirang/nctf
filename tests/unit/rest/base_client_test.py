import requests_mock
import unittest

from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError


class RESTClientUnitTestCase(unittest.TestCase):
    pass


class TestRESTClient(RESTClientUnitTestCase):
    def test_request_expected_status_code(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443")
            client.request('GET', '', 200)

    def test_request_expected_status_code_raises(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443")
            with self.assertRaises(UnexpectedStatusError):
                client.request('GET', '', 400)

    def test_request_expected_status_codes(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443")
            client.request('GET', '', (200, 202))

    def test_request_expected_status_codes_raises(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443")
            with self.assertRaises(UnexpectedStatusError):
                client.request('GET', '', (400, 404))

    def test_request_get(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443")
            client.request('GET', '')

    def test_request_post(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.post("https://foo.bar.baz:443")
            client.request('POST', '')

    def test_request_put(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.put("https://foo.bar.baz:443")
            client.request('PUT', '')

    def test_request_delete(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.delete("https://foo.bar.baz:443")
            client.request('DELETE', '')

    def test_request_patch(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        with requests_mock.mock() as m:
            m.patch("https://foo.bar.baz:443")
            client.request('PATCH', '')
