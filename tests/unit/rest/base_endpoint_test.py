import requests_mock
import unittest

from nctf.test_lib.base.rest import RESTClient
from nctf.test_lib.base.rest import RESTEndpoint
from nctf.test_lib.base.rest import checked_status
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError


class RESTEndpointUnitTestCase(unittest.TestCase):
    pass


class TestRESTEndpoint(RESTEndpointUnitTestCase):
    def test_api_root(self):
        client = RESTClient("https", "foo.bar.baz", 443)
        self.assertEquals(client.api_root, "https://foo.bar.baz:443")

    def test_checked_status(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        class TstEndpoint(RESTEndpoint):
            @checked_status(200)
            def foo(self, **kwargs):
                return self._get(**kwargs)

        endpoint = TstEndpoint(client, "/foo/bar/baz")

        with requests_mock.mock() as m:
            m.get('https://foo.bar.baz:443/foo/bar/baz')
            endpoint.foo()

    def test_checked_status_raises(self):
        client = RESTClient("https", "foo.bar.baz", 443)

        class TstEndpoint(RESTEndpoint):
            @checked_status(204)
            def foo(self, **kwargs):
                return self._get(**kwargs)

        endpoint = TstEndpoint(client, "/foo/bar/baz")

        with requests_mock.mock() as m:
            m.get('https://foo.bar.baz:443/foo/bar/baz')
            with self.assertRaises(UnexpectedStatusError):
                endpoint.foo()

    def test_format_path_enforce_trailing_slash_true(self):
        client = RESTClient("https", "foo.bar.baz", 443)
        endpoint = RESTEndpoint(client, "/foo/bar/baz", enforce_trailing_slash=True)

        with requests_mock.mock() as m:
            m.get('https://foo.bar.baz:443/foo/bar/baz/')
            endpoint._get()

    def test_format_path_enforce_trailing_slash_false(self):
        client = RESTClient("https", "foo.bar.baz", 443)
        endpoint = RESTEndpoint(client, "/foo/bar/baz", enforce_trailing_slash=False)

        with requests_mock.mock() as m:
            m.get('https://foo.bar.baz:443/foo/bar/baz')
            endpoint._get()


class TestRESTEndpointVerbs(RESTEndpointUnitTestCase):

    def setUp(self):
        self.client = RESTClient("https", "foo.bar.baz", 443)

        class TstEndpoint(RESTEndpoint):
            @checked_status(200)
            def foo(self, **kwargs):
                return self._get(**kwargs)

        self.endpoint = TstEndpoint(self.client, "/foo/bar/baz")

    def test_request(self):
        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443/foo/bar/baz")
            self.endpoint._request('GET')

    def test_request_get(self):
        with requests_mock.mock() as m:
            m.get("https://foo.bar.baz:443/foo/bar/baz")
            self.endpoint._get()

    def test_request_post(self):
        with requests_mock.mock() as m:
            m.post("https://foo.bar.baz:443/foo/bar/baz")
            self.endpoint._post()

    def test_request_put(self):
        with requests_mock.mock() as m:
            m.put("https://foo.bar.baz:443/foo/bar/baz")
            self.endpoint._put()

    def test_request_delete(self):
        with requests_mock.mock() as m:
            m.delete("https://foo.bar.baz:443/foo/bar/baz")
            self.endpoint._delete()

    def test_request_patch(self):
        with requests_mock.mock() as m:
            m.patch("https://foo.bar.baz:443/foo/bar/baz")
            self.endpoint._patch()

