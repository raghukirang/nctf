import logging
import sys
import unittest
from unittest.mock import MagicMock

import mockfs
import requests_mock

from nctf.test_lib.libs.device_capabilities import DeviceModel
from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import BranchType
from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import FirmwareLoaderFixture
from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import FirmwareLoadError

logger = logging.getLogger()
logger.level = logging.INFO


class FirmwareLoaderInterfaceTestCase(unittest.TestCase):
    def setUp(self):
        self.firmware_loader = FirmwareLoaderFixture()
        self.firmware_loader.guido_branch = BranchType.FIELD
        self.firmware_loader.local_full_path = "mount/files/field/test_aer3100_test.bin"
        self.firmware_loader.guido_path = "mount/files"
        self.firmware_loader.guido_date = ""
        self.firmware_loader.img_location = "guido"
        self.firmware_loader.load_method = "rest"
        self.mounted_drive = mockfs.replace_builtins()
        self.api_root = 'http://host.name:80'


class TestFirmwareLoaderInterface(FirmwareLoaderInterfaceTestCase):
    def setUp(self):
        super().setUp()
        self.firmware_loader.mount_firmware = MagicMock()

    def tearDown(self):
        mockfs.restore_builtins()

    def test_get_latest_firmware_file_exists(self):
        self.mounted_drive.add_entries({'/mount/files/field/test_aer3100_test.bin': 'content'})
        path = self.firmware_loader.get_latest_firmware(DeviceModel.AER3100, BranchType.FIELD, "bin")
        self.assertEqual(path, '/mount/files/field/test_aer3100_test.bin')

    def test_get_latest_firmware_file_does_not_exist(self):
        self.mounted_drive.add_entries({'/mount/files/field/test_aer2100_test.bin': 'content'})
        with self.assertRaises(FileNotFoundError):
            self.firmware_loader.get_latest_firmware(DeviceModel.AER3100, BranchType.FIELD, "bin")

    def test_load_firmware_local_path(self):
        self.firmware_loader.img_location = "local"
        self.mounted_drive.add_entries({'/mount/files/field/test_aer3100_test.bin': 'content'})
        with requests_mock.mock() as m:
            get_fw_info = m.register_uri('GET', '{}/api/status/fw_info'.format(self.api_root), json={'success': True})
            get_read_only_config = m.register_uri(
                'GET',
                '{}/api/control/system/readonlyconfig'.format(self.api_root),
                json={
                    'data': {
                        'enabled': True
                    },
                    'success': True
                })
            get_boot_id = m.register_uri('GET', '{}/api/status/system/bootid'.format(self.api_root), [{
                'json': {
                    'data': 12345,
                    'success': True
                }
            }, {
                'json': {
                    'data': 67890,
                    'success': True
                }
            }])
            post_fw_upgrade = m.register_uri(
                'POST', '{}/fw_upgrade?factory_reset=False'.format(self.api_root), json={
                    'data': 'valid',
                    'success': True
                })
            get_connection_state = m.register_uri(
                'GET', '{}/api/status/wan/connection_state'.format(self.api_root), json={
                    'data': 'connected',
                    'success': True
                })
            put_control_system = m.register_uri(
                'PUT', '{}/api/control/system'.format(self.api_root), json={
                    'data': {
                        'enabled': True
                    },
                    'success': True
                })
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, 'user', 'pass')
            self.assertEqual(6, get_fw_info.call_count, 'GET on fw_info was not called the correct number of times.')
            self.assertEqual(1, get_read_only_config.call_count,
                             'GET on readonlyconfig was not called the correct number of times.')
            self.assertEqual(2, get_boot_id.call_count, 'GET on bootid was not called the correct number of times.')
            self.assertEqual(1, post_fw_upgrade.call_count, 'POST on fw_upgrade was not called the correct number of times.')
            self.assertEqual(1, get_connection_state.call_count,
                             'GET on connection_state was not called the correct number of times.')
            self.assertEqual(1, put_control_system.call_count,
                             'PUT on control/system was not called the correct number of times.')

    def test_load_firmware_from_mount_same_revision(self):
        self.firmware_loader.local_full_path = ""
        self.firmware_loader._get_expected_sha = MagicMock(return_value=12345)
        self.mounted_drive.add_entries({'/mount/files/field/test_aer3100_test.bin': 'content'})
        with requests_mock.mock() as m:
            get_fw_info = m.register_uri('GET', '{}/api/status/fw_info'.format(self.api_root), json={'success': True})
            get_build_version = m.register_uri(
                'GET', '{}/api/status/fw_info/build_version'.format(self.api_root), json={
                    'data': 12345,
                    'success': True
                })
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, 'user', 'pass')
            self.assertEqual(2, get_fw_info.call_count, 'GET on fw_info was not called the correct number of times.')
            self.assertEqual(1, get_build_version.call_count,
                             'GET on build_version was not called the correct number of times.')

    def test_load_firmware_from_mount_different_revision_fails(self):
        self.firmware_loader.local_full_path = ""
        self.firmware_loader._get_expected_sha = MagicMock(return_value=12345)
        self.mounted_drive.add_entries({'/mount/files/field/test_aer3100_test.bin': 'content'})
        with requests_mock.mock() as m:
            get_fw_info = m.register_uri('GET', '{}/api/status/fw_info'.format(self.api_root), json={'success': True})
            get_build_version = m.register_uri(
                'GET', '{}/api/status/fw_info/build_version'.format(self.api_root), json={
                    'data': 67890,
                    'success': True
                })
            get_read_only_config = m.register_uri(
                'GET',
                '{}/api/control/system/readonlyconfig'.format(self.api_root),
                json={
                    'data': {
                        'enabled': True
                    },
                    'success': True
                })
            get_boot_id = m.register_uri('GET', '{}/api/status/system/bootid'.format(self.api_root), [{
                'json': {
                    'data': 12345,
                    'success': True
                }
            }, {
                'json': {
                    'data': 67890,
                    'success': True
                }
            }])
            post_fw_upgrade = m.register_uri(
                'POST', '{}/fw_upgrade?factory_reset=False'.format(self.api_root), json={
                    'data': 'valid',
                    'success': True
                })
            get_connection_state = m.register_uri(
                'GET', '{}/api/status/wan/connection_state'.format(self.api_root), json={
                    'data': 'connected',
                    'success': True
                })
            put_control_system = m.register_uri(
                'PUT', '{}/api/control/system'.format(self.api_root), json={
                    'data': {
                        'enabled': True
                    },
                    'success': True
                })
            with self.assertRaises(FirmwareLoadError) as context:
                self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
                print(str(context.exception))
                self.assertTrue("Current router revision sha1 67890 does not match expected 12345 after firmware was loaded" in
                                str(context.exception))
            self.assertEqual(10, get_fw_info.call_count, 'GET on fw_info was not called the correct number of times.')
            self.assertEqual(2, get_build_version.call_count,
                             'GET on build_version was not called the correct number of times.')
            self.assertEqual(1, get_read_only_config.call_count,
                             'GET on readonlyconfig was not called the correct number of times.')
            self.assertEqual(2, get_boot_id.call_count, 'GET on bootid was not called the correct number of times.')
            self.assertEqual(1, post_fw_upgrade.call_count, 'POST on fw_upgrade was not called the correct number of times.')
            self.assertEqual(1, get_connection_state.call_count,
                             'GET on connection_state was not called the correct number of times.')
            self.assertEqual(1, put_control_system.call_count,
                             'PUT on control/system was not called the correct number of times.')

    def test_no_attributes_specified(self):
        stream_handler = logging.StreamHandler(sys.stdout)
        logger.addHandler(stream_handler)
        self.firmware_loader.guido_branch = BranchType.NONE
        self.firmware_loader.local_full_path = ""
        self.firmware_loader.guido_path = ""
        try:
            with self.assertLogs() as cm:
                self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
                output = ("INFO:firmware_loader.interface:No guido_branch or local_full_path specified.  "
                          "Firmware will not be loaded.")
                self.assertEqual(cm.output[0], output)
        finally:
            logger.removeHandler(stream_handler)

    def test_load_firmware_by_date(self):
        self.firmware_loader.guido_date = "01/01/2018"
        with self.assertRaises(NotImplementedError) as context:
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
        self.assertTrue("Loading firmware by date is not yet implemented.  Leave this field blank." in str(context.exception))

    def test_branch_specified_but_not_mount(self):
        self.firmware_loader.guido_path = ""
        with self.assertRaises(ValueError) as context:
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
        self.assertTrue(
            "Attributes guido_branch and guido_path must both have a value or neither have a value." in str(context.exception))

    def test_mount_specified_but_not_branch(self):
        self.firmware_loader.guido_branch = BranchType.NONE
        with self.assertRaises(ValueError) as context:
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
        self.assertTrue(
            "Attributes guido_branch and guido_path must both have a value or neither have a value." in str(context.exception))

    def test_unsupported_img_location(self):
        self.firmware_loader.img_location = "s3"
        with self.assertRaises(NotImplementedError) as context:
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
        self.assertTrue("Deriving firmware file from s3 is not implemented." in str(context.exception))

    def test_unsupported_load_method(self):
        self.firmware_loader.load_method = "ssh"
        self.firmware_loader._get_loaded_firmware_revision = MagicMock(return_value="67890")
        self.firmware_loader.get_latest_firmware = MagicMock(return_value="mount/path/to/files/test_AER3100_file.bin")
        with self.assertRaises(NotImplementedError) as context:
            self.firmware_loader.load_firmware(DeviceModel.AER3100, self.api_root, "user", "pass")
        self.assertTrue("Loading firmware with ssh is not implemented." in str(context.exception))
