import copy
import logging
import os
import unittest
from unittest.mock import MagicMock

from unittest.mock import patch

from artifactory import ArtifactoryPath

from nctf.test_lib.ncos.base.client import NCOSLANClient
from nctf.test_lib.ncos.base.router import NCOSRouter
from nctf.test_lib.ncos.base.router import NCOSFirmwareInfo
from nctf.test_lib.ncos.base.capability import NCOSRouterCapabilities

from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import ArtifactoryFwGetter
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwBinData
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwGetterError
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import FwLoaderFixtureV2
from nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture import LocalFwGetter

logger = logging.getLogger("fwloaderv2tests")

class FwLoaderV2TestCase(unittest.TestCase):
    def setUp(self):
        pass


class TestArtifactoryFwGetter(FwLoaderV2TestCase):
    def setUp(self):
        super().setUp()

    def test_create_query(self):
        ArtifactoryPath.aql = MagicMock(return_value=[{}])

        getter = ArtifactoryFwGetter('foo-api-key', 'foo-repo')
        getter._query_artifactory('stn666', '*.bin', release_ver='8.8.8')

        ArtifactoryPath.aql.assert_called_with('items.find', {
            'repo': 'foo-repo',
            'name': {
                '$match': '*.bin'
            },
            '@PRODUCT': {
                '$eq': 'stn666'
            },
            '@RELEASE_VERSION': {
                '$eq': '8.8.8'
            }
        }, '.include("property.*")')

    def test_choose_best_artifact(self):
        # choose_best_artifact(artifacts: list) -> dict:
        getter = ArtifactoryFwGetter('foo-api-key', 'foo-repo')

        # condition: no matching artifacts
        with self.assertRaises(FwGetterError):
            getter._choose_best_artifact([])

        # condition: one matching artifact
        chosen = getter._choose_best_artifact([1])
        self.assertEqual(1, chosen)

        # condition: more than one matching artifact,
        # theory is that artifactory will return them in date order
        # so we can simply choose the last in the list and it will be the best
        chosen = getter._choose_best_artifact([1, 2, 3])
        self.assertEqual(3, chosen)

    def test_add_artifact_to_bin_data(self):
        # _add_artifact_to_bin_data(bin_data: FwBinData, artifact: dict):

        bd = FwBinData(local_path="lp", artifactory_path="ap", product="prod", build_type="bt", git_sha="gs",
                       release_ver="rv")

        # an actual artifact dict from artifactory on 2018/11/16:
        artifact = \
            {'repo': 'platform-firmware-private',
             'path': 'nightlies/master/field/coco_nightly_field_2018_11_15/cr4250_field_2018_11_15',
             'name': 'cr4250_field_2018_11_15_core-image-cr4250-upgrade-1.0-20181115032718.vmp', 'type': 'file',
             'size': 145858576, 'created': '2018-11-15T03:28:40.580Z', 'created_by': 'engops-prod-jenkins-v1',
             'modified': '2018-11-15T03:28:35.589Z', 'modified_by': 'engops-prod-jenkins-v1',
             'updated': '2018-11-15T03:28:40.581Z',
             'properties': [{'key': 'GIT_SHA', 'value': 'ab25cb75c5ace410824cda88d7d6f259e931177a'},
                            {'key': 'build.name', 'value': 'vmp-builder'}, {'key': 'DEBUG', 'value': 'false'},
                            {'key': 'build.parentNumber', 'value': '533'}, {'key': 'BUILD_DATE', 'value': '2018_11_15'},
                            {'key': 'BUILD_URL',
                             'value': 'https:/engops-nonprod-jenkins-kube.private.aws.cradlepointecm.com/job/vmp-builder/134/'},
                            {'key': 'RELEASE_VERSION', 'value': '7.0.10'}, {'key': 'build.number', 'value': '134'},
                            {'key': 'GIT_BRANCH', 'value': 'master'}, {'key': 'BUILD_TYPE', 'value': 'field'},
                            {'key': 'RELEASE', 'value': 'false'}, {'key': 'PRODUCT', 'value': 'cr4250'},
                            {'key': 'build.timestamp', 'value': '1542248218706'},
                            {'key': 'build.parentName', 'value': 'firmware_build_nightly_child'}]}

        getter = ArtifactoryFwGetter('foo-api-key', 'foo-repo')

        getter._add_artifact_to_bin_data(bin_data=bd, artifact=artifact)
        self.assertEqual(bd.release_ver, "7.0.10")
        self.assertEqual(bd.product, "cr4250")
        self.assertEqual(bd.git_sha, "ab25cb75c5ace410824cda88d7d6f259e931177a")
        self.assertEqual(bd.build_type, "field")


class TestLocalFwGetter(FwLoaderV2TestCase):

    PATH_TO_THIS_FILES_DIR = os.path.dirname(os.path.abspath(__file__))
    PATH_TO_TEST_FW_IMAGE_DIR = os.path.join(PATH_TO_THIS_FILES_DIR, 'test_fw_images')

    def test_get_fw_from_local_folder(self):
        getter = LocalFwGetter(fw_image_dir=self.PATH_TO_TEST_FW_IMAGE_DIR)

        fw_img_path = getter._get_fw_from_local_folder(product='aer1600')
        self.assertTrue(fw_img_path.endswith('aer1600_foo.bin'))

    def test_get_fw_from_local_folder_raises_when_no_folder(self):
        getter = LocalFwGetter()

        with self.assertRaises(FwGetterError):
            getter._get_fw_from_local_folder(product='aer1600')

    def test_get_fw_from_local_folder_raises_when_multiple_matches(self):
        getter = LocalFwGetter(fw_image_dir=self.PATH_TO_TEST_FW_IMAGE_DIR)

        with self.assertRaises(FwGetterError):
            getter._get_fw_from_local_folder(product='aer3100')

    def test_get_fw_from_local_folder_raises_when_no_matches(self):
        getter = LocalFwGetter()

        with self.assertRaises(FwGetterError):
            getter._get_fw_from_local_folder(product='aer1650')


class TestFirmwareLoaderV2API(FwLoaderV2TestCase):
    boot_ids = 0
    client_execs = 0

    def setUp(self):
        super().setUp()

    def fake_fw_info(self):
        fw_info = NCOSFirmwareInfo()
        fw_info.build_version = "0123456789abcdef"
        fw_info.build_type = "head"
        fw_info.major_version = 6
        fw_info.minor_version = 5
        fw_info.patch_version = 4
        return fw_info

    def client_fake_fw_info(self, router, protocol):
        return self.fake_fw_info()

    def client_exec_command(self, command: str):
        self.client_execs = self.client_execs+1
        if "bootid" in command:
            self.boot_ids = self.boot_ids + 1
            return (f'{{"success": true, "data": "{str(self.boot_ids)}"}}', "ok")
        else:
            return ('{"build_type":"head", "version":"0123456789abcdef"}}', "ok")

    def get_test_client(self) -> NCOSLANClient:
        test_client = MagicMock(NCOSLANClient)

        return test_client

    def get_test_router(self) -> NCOSRouter:
        test_router = NCOSRouter()
        test_router.admin_user = "admin"
        test_router.admin_password = "pw"
        test_router.remote_admin_host = "127.0.0.1"
        test_router.remote_admin_port = 1234
        test_router.default_lan_ip = "127.0.0.2"
        test_router.capability_id = "AER9876"

        caps = NCOSRouterCapabilities()
        caps.default_lan_only = False
        caps.wan_ports = 3
        caps.lan_ports = 7
        caps.upgrade_method = "NORMAL"
        test_router.get_router_capabilities = MagicMock(return_value=caps)

        test_router.get_value = MagicMock(return_value=False)  # for readonly_config
        #test_router.set_value = MagicMock()
        #test_router.get_ssh_client = MagicMock(return_value=None)
        #test_router.get_rest_client = MagicMock(return_value=None)

        return test_router

    def get_test_getter(self):
        fw_getter = MagicMock(LocalFwGetter, autospec=True)
        bin_data = FwBinData()
        bin_data.build_type = "head"
        bin_data.git_sha = "0123456789abcdef"
        bin_data.release_ver = "6.5.4"
        bin_data.local_path = "somefile.bin"

        fw_getter.get_fw = MagicMock(return_value=bin_data)
        return fw_getter

    @patch(target='nctf.test_lib.ncos.base.client.NCOSLANClient.exec_command')
    @patch(target='nctf.test_lib.ncos.base.client.NCOSLANClient.router_get_fw_info')
    @patch(target='nctf.test_lib.ncos.fixtures.firmware_loader.v2.fixture.FwLoaderFixtureV2._indirect_get_boot_id')
    def test_load_fw(self, id_mock, rgfi_mock, ec_mock):
        id_mock.side_effect = ["1", "1", "2", "2", "3", "3", "4", "4"]
        router = self.get_test_router()
        router.get_fw_info = MagicMock(return_value=self.fake_fw_info())
        getter1 = self.get_test_getter()
        fixture = FwLoaderFixtureV2(fw_getter=getter1)

        logger.info("CASE: load_fw(router only)")
        fixture.load_fw(router)
        assert getter1.get_fw.call_count == 1

        logger.info("CASE: load_fw(router, client)")
        ec_mock.side_effect = [('{"success": true, "data": "123"}', "ok"),
                               ('{"build_type":"head", "version":"9876543210fedcba"}', "ok")]
        rgfi_mock.return_value = self.fake_fw_info()
        client = NCOSLANClient()
        fixture.load_fw(router=router, client=client)
        rgfi_mock.assert_called_once()

        logger.info("CASE: load_fw(router, protocol)")
        with patch(target='nctf.test_lib.ncos.base.router.NCOSRouter.get_fw_info') as gfi_mock:
            router2 = self.get_test_router()
            gfi_mock.side_effect = [self.fake_fw_info(), self.fake_fw_info()]
            fixture.load_fw(router=router2, protocol="secret")
            gfi_mock.assert_called_with(protocol="secret")

        logger.info("CASE: load_fw(router, getter)")
        getter = self.get_test_getter()
        fixture.load_fw(router=router, getter=getter)
        assert getter.get_fw.call_count == 1

    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI')
    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    def test_load_over_rest(self, auth_mock, get_mock, rest_mock):
        # CASE: works with router validated by test_load_fw() above
        # CASE: protocol validated by test_load_fw() above
        # CASE: getter validated by test_load_fw() above
        logger.info("CASE: load_over_rest(credentials)")
        getter1 = self.get_test_getter()
        fixture = FwLoaderFixtureV2(fw_getter=getter1)
        auth_mock.return_value = None
        rest_mock.status_fw_info.get = get_mock
        rest_mock.control_system.get = get_mock
        fwi_dict = {"success": True, "data": {"build_type": "head",
                                              "build_version": "0123456789abcdef",
                                              "major_version": 6,
                                              "minor_version": 5,
                                              "patch_version": 4,
                                              "build_date":"2018_10_15",
                                              "upgrade_major_version": 4,
                                              "upgrade_minor_version": 5,
                                              "upgrade_patch_version": 6}}
        get_mock.side_effect = [{"success": True, "data": False},
                                fwi_dict,
                                fwi_dict]

        fixture.load_over_rest(username="me", password="myself", hostname="http://127.0.0.3", product="Router2000")
        assert getter1.get_fw.call_count == 1
        assert auth_mock.call_count == 3
        auth_mock.assert_called_with("me", "myself")

    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI')
    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.update')
    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    @patch(target='nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch(target='nctf.test_lib.libs.bin_ops.BinOperations.load_firmware_bin_file')
    def test_load_over_rest_with_upgrade(self, binops_mock, auth_mock, get_mock, update_mock, rest_mock):
        logger.info("CASE: load_over_rest_with_upgrade (readonly=True)")
        binops_mock.return_value = None
        getter1 = self.get_test_getter()
        fixture = FwLoaderFixtureV2(fw_getter=getter1)
        FwLoaderFixtureV2._stabilize_delay = 1
        auth_mock.return_value = None
        rest_mock.status_fw_info.get = get_mock
        rest_mock.control_system.get = get_mock
        rest_mock.control_system.update = update_mock
        fwi_dict = {"success": True, "data": {"build_type": "head",
                                              "build_version": "5123456789abcdef",
                                              "major_version": 6,
                                              "minor_version": 5,
                                              "patch_version": 3,
                                              "build_date": "2018_10_15",
                                              "upgrade_major_version": 4,
                                              "upgrade_minor_version": 5,
                                              "upgrade_patch_version": 6}}
        fwi_dict2 = copy.deepcopy(fwi_dict)
        fwi_dict2["data"]["patch_version"] = 4  # after the upgrade we should now be the right version
        fwi_dict2["data"]["build_version"] = "0123456789abcdef"
        get_mock.side_effect = [{"success": True, "data": True},
                                fwi_dict,
                                fwi_dict2]

        fixture.load_over_rest(username="me", password="myself", hostname="http://127.0.0.3", product="Router2000")
        assert getter1.get_fw.call_count == 1
        assert auth_mock.call_count == 5
        auth_mock.assert_called_with("me", "myself")

