import os
import unittest
from unittest.mock import MagicMock

from nctf.pytest_lib.core.config.fixture import handle_run_conf
from nctf.pytest_lib.core.config.fixture import handle_target_conf


class PyTestConfigureTestCase(unittest.TestCase):
    def setUp(self):
        self.our_dir = os.path.dirname(os.path.realpath(__file__))
        self.mock_config = MagicMock()


class TestHandleRunConf(PyTestConfigureTestCase):
    def setUp(self):
        super().setUp()

    def test_valid_run_conf_alias(self):
        self.mock_config.option.run_conf = 'pytest_configure_test'
        self.mock_config.invocation_dir.strpath = self.our_dir

        handle_run_conf(self.mock_config)  # No exception is a test pass.

    def test_valid_run_conf_path(self):
        self.mock_config.option.run_conf = os.path.join(self.our_dir, 'pytest_configure_test.yaml')
        self.mock_config.invocation_dir.strpath = self.our_dir

        handle_run_conf(self.mock_config)  # No exception is a test pass.

    def test_invalid_run_conf_alias(self):
        self.mock_config.option.run_conf = 'does_not_exist'
        self.mock_config.invocation_dir.strpath = self.our_dir

        with self.assertRaises(FileNotFoundError):
            handle_run_conf(self.mock_config)

    def test_invalid_run_conf_path(self):
        self.mock_config.option.run_conf = os.path.join('/does/not/exists/', 'does_not_exist.yaml')
        self.mock_config.invocation_dir.strpath = self.our_dir

        with self.assertRaises(FileNotFoundError):
            handle_run_conf(self.mock_config)


class TestHandleTargetConf(PyTestConfigureTestCase):
    def setUp(self):
        super().setUp()

    def test_valid_target_conf_alias(self):
        self.mock_config.option.target_conf = 'pytest_configure_test'
        self.mock_config.invocation_dir.strpath = self.our_dir

        handle_target_conf(self.mock_config)  # No exception is a test pass.

    def test_valid_target_conf_path(self):
        self.mock_config.option.target_conf = os.path.join(self.our_dir, 'pytest_configure_test.yaml')
        self.mock_config.invocation_dir.strpath = self.our_dir

        handle_target_conf(self.mock_config)  # No exception is a test pass.

    def test_invalid_target_conf_alias(self):
        self.mock_config.option.target_conf = 'does_not_exist'
        self.mock_config.invocation_dir.strpath = self.our_dir

        with self.assertRaises(FileNotFoundError):
            handle_target_conf(self.mock_config)

    def test_invalid_target_conf_path(self):
        self.mock_config.option.target_conf = os.path.join('/does/not/exists/', 'does_not_exist.yaml')
        self.mock_config.invocation_dir.strpath = self.our_dir

        with self.assertRaises(FileNotFoundError):
            handle_target_conf(self.mock_config)
