import os
import unittest

from attrdict import AttrMap
from nctf.test_lib.core.config import ConfigFixtureFactory
from nctf.test_lib.core.config.exceptions import InvalidConfigError
from nctf.test_lib.core.config.exceptions import InvalidConfigOverrideError

config_test_folder = os.path.dirname(os.path.abspath(__file__))
run_config_yaml = os.path.abspath(os.path.join(config_test_folder, "test_run_config.yaml"))
target_config_yaml = os.path.abspath(os.path.join(config_test_folder, "test_target_config.yaml"))


class ConfigFixtureTestCase(unittest.TestCase):
    pass


class TestConfigFixtureReadsFiles(ConfigFixtureTestCase):
    def test_reads_yaml(self):
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml)
        self.assertEqual(config.run.fixtures.selenium.useProxy, True)
        self.assertEqual(config.target.services.ncm.protocol, "https")


class TestConfigFixtureJfwEnvVarOverrides(ConfigFixtureTestCase):
    def setUp(self):
        self.old_environ = dict(os.environ)

    def test_overrides_with_str_env_var(self):
        os.environ['JFW_run_fixtures_arm_apiKey'] = "foo"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertEqual(config.run.fixtures.arm.apiKey, "foo")

    def test_overrides_with_int_env_var(self):
        os.environ['JFW_run_fixtures_selenium_timeout'] = "123"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertEqual(config.run.fixtures.selenium.timeout, 123)

    def test_overrides_with_bool_env_var(self):
        os.environ['JFW_run_fixtures_selenium_useProxy'] = "false"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertEqual(config.run.fixtures.selenium.useProxy, False)

    def test_does_not_override_dict_val(self):
        os.environ['JFW_run_fixtures_arm'] = "foo"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertNotEqual(config.run.fixtures.arm.apiKey, "foo")

    def test_does_not_override_with_long_env_var(self):
        os.environ['JFW_run_fixtures_arm_apiKey_whoops'] = "foo"
        with self.assertRaises(InvalidConfigOverrideError):
            ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)

    def tearDown(self):
        os.environ.clear()
        os.environ.update(self.old_environ)


class TestConfigFixtureNctfEnvVarOverrides(ConfigFixtureTestCase):
    def setUp(self):
        self.old_environ = dict(os.environ)

    def test_overrides_with_str_env_var(self):
        os.environ['NCTF_run_fixtures_arm_apiKey'] = "foo"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertEqual(config.run.fixtures.arm.apiKey, "foo")

    def test_overrides_with_int_env_var(self):
        os.environ['NCTF_run_fixtures_selenium_timeout'] = "123"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertEqual(config.run.fixtures.selenium.timeout, 123)

    def test_overrides_with_bool_env_var(self):
        os.environ['NCTF_run_fixtures_selenium_useProxy'] = "false"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertEqual(config.run.fixtures.selenium.useProxy, False)

    def test_does_not_override_dict_val(self):
        os.environ['NCTF_run_fixtures_arm'] = "foo"
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)
        self.assertNotEqual(config.run.fixtures.arm.apiKey, "foo")

    def test_does_not_override_with_long_env_var(self):
        os.environ['NCTF_run_fixtures_arm_apiKey_whoops'] = "foo"
        with self.assertRaises(InvalidConfigOverrideError):
            ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, use_env_vars=True)

    def tearDown(self):
        os.environ.clear()
        os.environ.update(self.old_environ)


class TestConfigFixtureArgOverrides(ConfigFixtureTestCase):
    def test_overrides_with_str_override(self):
        overrides = ['run.fixtures.arm.hostname=foo']
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, overrides=overrides)
        self.assertEqual(config.run.fixtures.arm.hostname, "foo")

    def test_overrides_with_int_override(self):
        overrides = ['run.fixtures.arm.port=123']
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, overrides=overrides)
        self.assertEqual(config.run.fixtures.arm.port, 123)

    def test_overrides_with_bool_override(self):
        overrides = ['run.fixtures.selenium.useProxy=false']
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, overrides=overrides)
        self.assertEqual(config.run.fixtures.selenium.useProxy, False)

    def test_does_not_override_dict_val(self):
        overrides = ['run.fixtures.arm=foo']
        config = ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, overrides=overrides)
        self.assertIsInstance(config.run.fixtures.arm, AttrMap)

    def test_does_not_override_with_long_override(self):
        overrides = ['run.fixtures.arm.hostname.whoops=foo']
        with self.assertRaises(InvalidConfigOverrideError):
            ConfigFixtureFactory().create(run_config_yaml, target_config_yaml, overrides=overrides)


run_sparse_fixtures_yaml = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "run_sparse_fixtures.yaml"))
run_no_fixtures_yaml = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "run_no_fixtures.yaml"))
run_unknown_fixture_yaml = os.path.abspath(os.path.join(config_test_folder, "v1_invalid", "run_unknown_fixture.yaml"))


class TestConfigFixtureRunValidation(ConfigFixtureTestCase):
    def test_valid_run_config_sparse_fixtures(self):
        ConfigFixtureFactory().create(run_sparse_fixtures_yaml, target_config_yaml)

    def test_valid_run_config_no_fixtures(self):
        ConfigFixtureFactory().create(run_no_fixtures_yaml, target_config_yaml)

    def test_invalid_run_config_unknown_fixture(self):
        with self.assertRaises(InvalidConfigError):
            ConfigFixtureFactory().create(run_unknown_fixture_yaml, target_config_yaml)


class TestConfigFixtureTargetNcos(ConfigFixtureTestCase):
    def test_not_null(self):
        not_null = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "target", "ncos", "not_null.yaml"))
        ConfigFixtureFactory().create(run_config_yaml, not_null)

    def test_null(self):
        null = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "target", "ncos", "null.yaml"))
        ConfigFixtureFactory().create(run_config_yaml, null)


target_sparse_services_yaml = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "target_sparse_services.yaml"))
target_no_services_yaml = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "target_no_services.yaml"))
target_any_predefined_auth_yaml = os.path.abspath(
    os.path.join(config_test_folder, "v1_valid", "target_any_predefined_auth.yaml"))
target_unknown_service_yaml = os.path.abspath(os.path.join(config_test_folder, "v1_invalid", "target_unknown_service.yaml"))


class TestConfigFixtureTargetValidation(ConfigFixtureTestCase):
    def test_valid_target_config_sparse_services(self):
        ConfigFixtureFactory().create(run_config_yaml, target_sparse_services_yaml)

    def test_valid_target_config_no_services(self):
        ConfigFixtureFactory().create(run_config_yaml, target_no_services_yaml)

    def test_valid_target_config_any_predefined_auth(self):
        ConfigFixtureFactory().create(run_config_yaml, target_any_predefined_auth_yaml)

    def test_invalid_target_config_unknown_service(self):
        with self.assertRaises(InvalidConfigError):
            ConfigFixtureFactory().create(run_config_yaml, target_unknown_service_yaml)


class TestConfigFixtureRunFwLoader2(ConfigFixtureTestCase):
    def test_artifactory_1(self):
        artifactory_1 = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "run", "fwloader2", "artifactory_1.yaml"))

        ConfigFixtureFactory().create(artifactory_1, target_no_services_yaml)

    def test_local_1(self):
        local_1 = os.path.abspath(os.path.join(config_test_folder, "v1_valid", "run", "fwloader2", "local_1.yaml"))

        ConfigFixtureFactory().create(local_1, target_no_services_yaml)
