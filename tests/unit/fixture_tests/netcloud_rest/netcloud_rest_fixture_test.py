import unittest
from unittest.mock import MagicMock
from unittest.mock import patch

from nctf.test_lib.accounts.rest.fixture import AccServRESTClient
from nctf.test_lib.ncm.rest.client import NCMv1RESTClient
from nctf.test_lib.ncm.rest.v2_client import NCMv2RESTClient
from nctf.test_lib.ncp.rest.client import PAPIv1RESTClient
from nctf.test_lib.netcloud.fixtures.netcloud_account.actor import NetCloudActor
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTClient
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixtureError


class NetCloudRESTFixtureTestCase(unittest.TestCase):
    pass


class TestNetCloudRESTFixture(NetCloudRESTFixtureTestCase):
    def test_init(self):
        fixture = NetCloudRESTFixture(
            "https",
            "foo-accounts",
            443,
            "https",
            "foo-ncm",
            443,
            "https",
            "foo-papi",
            443,
            x_cp_api_id='foo123',
            x_cp_api_key='bar321')
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        self.assertIsInstance(client, NetCloudRESTClient)
        self.assertIsInstance(client.accounts, AccServRESTClient)
        self.assertIsInstance(client.ncm, NCMv1RESTClient)
        self.assertIsInstance(client.papi, PAPIv1RESTClient)
        self.assertIsInstance(client.ncmv2, NCMv2RESTClient)

    def test_init_acc_serv_rest_client(self):
        fixture = NetCloudRESTFixture(accounts_protocol="https", accounts_hostname="foo-accounts", accounts_port=443)
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        self.assertIsInstance(client.accounts, AccServRESTClient)

    def test_no_init_acc_serv_rest_client(self):
        fixture = NetCloudRESTFixture()
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        with self.assertRaises(NetCloudRESTFixtureError):
            self.assertIsInstance(client.accounts, AccServRESTClient)

    def test_init_ncm_rest_client(self):
        fixture = NetCloudRESTFixture(ncm_protocol="https", ncm_hostname="foo-ncm", ncm_port=443)
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        self.assertIsInstance(client.ncm, NCMv1RESTClient)

    def test_no_init_ncm_rest_client(self):
        fixture = NetCloudRESTFixture()
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        with self.assertRaises(NetCloudRESTFixtureError):
            self.assertIsInstance(client.ncm, NCMv1RESTClient)

    def test_init_papi_rest_client(self):
        fixture = NetCloudRESTFixture(papi_protocol="https", papi_hostname="foo-papi", papi_port=443)
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        self.assertIsInstance(client.papi, PAPIv1RESTClient)

    def test_no_init_papi_rest_client(self):
        fixture = NetCloudRESTFixture()
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        with self.assertRaises(NetCloudRESTFixtureError):
            self.assertIsInstance(client.papi, PAPIv1RESTClient)

    def test_get_client_no_actor(self):
        AccServRESTClient.authenticate = MagicMock()
        fixture = NetCloudRESTFixture("https", "foo-accounts", 443, "https", "foo-ncm", 443, "https", "foo-papi", 443)
        fixture.get_client()
        self.assertFalse(AccServRESTClient.authenticate.called)

    def test_get_client_with_actor(self):
        AccServRESTClient.authenticate = MagicMock()
        fixture = NetCloudRESTFixture("https", "foo-accounts", 443, "https", "foo-ncm", 443, "https", "foo-papi", 443)

        fixture.get_client(NetCloudActor('foo', 'bar'))
        self.assertTrue(AccServRESTClient.authenticate.called)

    def test_init_ncmv2_rest_client(self):
        fixture = NetCloudRESTFixture(
            ncm_protocol="https", ncm_hostname="foo-ncm", ncm_port=443, x_cp_api_id='foo123', x_cp_api_key='bar321')
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        self.assertIsInstance(client.ncmv2, NCMv2RESTClient)

    def test_no_init_ncmv2_rest_client(self):
        fixture = NetCloudRESTFixture()
        client = fixture.get_client()

        self.assertIsInstance(fixture, NetCloudRESTFixture)
        with self.assertRaises(NetCloudRESTFixtureError):
            self.assertIsInstance(client.ncmv2, NCMv2RESTClient)
