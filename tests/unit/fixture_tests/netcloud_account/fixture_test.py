import copy
import time
import unittest
from unittest.mock import MagicMock

from nctf.nctf_typing import NetCloudActor
from nctf.test_lib.accounts.rest.client import AccountsEndpoint as AccountsAccountsEndpoint
from nctf.test_lib.accounts.rest.client import AccServRESTClient
from nctf.test_lib.accounts.rest.client import PasswordTokensEndpoint as AccountsPasswordTokensEndpoint
from nctf.test_lib.accounts.rest.client import UsersEndpoint as AccountsUsersEndpoint
from nctf.test_lib.core.email import EmailFixture
from nctf.test_lib.core.email import EmailService
from nctf.test_lib.core.email.base import EmailMessage
from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as NCA


class TestNetCloudAccountFixture(unittest.TestCase):
    def setUp(self):
        # Mocks
        time.sleep = MagicMock()  # Disable sleep since we have a hard wait for email currently.
        AccountsAccountsEndpoint.create = MagicMock()
        AccountsUsersEndpoint.create = MagicMock()
        AccountsPasswordTokensEndpoint.update = MagicMock()
        mock_message = EmailMessage(
            id_="foo",
            subject="bar",
            sender="noreply@cradlepoint.com",
            body='foobar <a href=".+?passwordTokens/test123"><span class="teal-text">HERE</span></a> bazbang')
        MailhogInbox.get_messages_by_subject = MagicMock(return_value=[mock_message])
        AccServRESTClient.post = MagicMock()
        self.nca_fixture = NCA.NetCloudAccountFixture(
            creation_method=NCA.CreationMethod.ACCOUNTS,
            use_activation_email=True,
            accounts_api_protocol="https",
            accounts_api_hostname="foo",
            accounts_api_port=443,
            accounts_privileged_username="foo",
            accounts_privileged_password="foo",
            email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"),
            ncm_api_protocol="https",
            ncm_api_hostname="foo",
            ncm_api_port=443)

        self.mock_account_json = {
            "data": {
                "type": "accounts",
                "id": "0016C000004FgtX",
                "attributes": {
                    "name": "Journey",
                    "contact": "uK9wrbjl7nIDl2KO",
                    "sessionLength": 7200,
                    "enhancedLoginSecurityEnabled": False,
                    "federatedId": "Cradlepoint",
                    "identityProviderCertificate": None,
                    "mfaRequired": False
                },
                "links": {
                    "self": "https://accounts-qa2.cradlepointecm.com/api/v1/accounts/0016C000004FgtX"
                }
            }
        }

        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return copy.deepcopy(self.json_data)

        AccServRESTClient.get = MagicMock(return_value=MockResponse(self.mock_account_json, 200))
        # Validate on the args passed into put, not on whether the put does the update, which it doesn't, just return the
        # canned json response so that the parsers of the json don't blow up
        AccServRESTClient.put = MagicMock(return_value=MockResponse(self.mock_account_json, 200))
        self.mock_ncm_users_get_json = {
            "success":
            True,
            "data": [{
                "resource_uri": "/api/v1/users/332244/",
                "username": "RK4iGXEvEQVi5ZWS",
                "first_name": "Melissa",
                "last_name": "Archer",
                "email": "melissa_archer_fj8psc1a@cradlepoint.mailinator.com",
                "is_active": True,
                "last_login": None,
                "date_joined": "2018-10-18T21:43:58+00:00",
                "profile": "/api/v1/profiles/331452/",
                "authorizations": "/api/v1/users/332244/authorizations/",
                "id": "332244"
            }],
            "meta": {
                "offset": 0,
                "limit": 20,
                "total_count": 1,
                "previous": None,
                "next": None
            }
        }
        NCMv1RESTClient.get = MagicMock(return_value=MockResponse(self.mock_ncm_users_get_json, 200))
        NCMv1RESTClient.post = MagicMock()

    def validate_actor(self, actor):
        self.assertTrue(AccountsAccountsEndpoint.create.called)
        self.assertTrue(AccountsUsersEndpoint.create.called)
        self.assertTrue(AccountsPasswordTokensEndpoint.update.called)
        self.assertIsNotNone(actor.username)
        self.assertEqual(actor.password, "Password1!")
        self.assertEquals(actor.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)

    def validate_account(self, account):
        self.assertTrue(AccountsAccountsEndpoint.create.called)
        self.assertTrue(AccountsUsersEndpoint.create.called)
        self.assertTrue(AccountsPasswordTokensEndpoint.update.called)
        self.assertTrue(account.accserv.name == "Journey")
        self.assertTrue(account.accserv.contact == "uK9wrbjl7nIDl2KO")
        self.assertTrue(account.accserv.default_applications == ['ecm'])
        self.assertTrue(account.accserv.contact == "uK9wrbjl7nIDl2KO")
        self.assertFalse(account.accserv.enhanced_login_security_enabled)
        self.assertTrue(account.accserv.federated_id == "Cradlepoint")
        self.assertIsNone(account.accserv.identity_provider_cert)
        self.assertFalse(account.accserv.mfa_required)
        self.assertTrue(account.accserv.session_length == 7200)
        self.assertIsNotNone(account.accserv.tenant_id)
        self.assertEqual(account.tenant_id, account.accserv.tenant_id)
        self.validate_actor(account.admin)

    def test_nca_fixture_create_activate(self):
        # UI activation is not implemented
        with self.assertRaises(NotImplementedError):
            new_admin = self.nca_fixture.create(activate=NCA.ActivationMethod.UI)

        # Check activation equal API, 'api' and 'API'
        new_admin = self.nca_fixture.create(activate=NCA.ActivationMethod.API)
        self.validate_actor(new_admin)
        new_admin = self.nca_fixture.create(activate='api')
        self.validate_actor(new_admin)
        new_admin = self.nca_fixture.create(activate='API')
        self.validate_actor(new_admin)

        # Check activation equal False
        with self.assertRaises(ValueError) as e:
            new_admin = self.nca_fixture.create(activate=False)
        self.assertTrue("NetCloud Account Admin cannot accept the TOS if he does not activate" in e.exception.args[0])

        # Check activation being an invalid parameter
        with self.assertRaises(ValueError) as e:
            new_admin = self.nca_fixture.create(activate=True)
        self.assertTrue("Invalid ActivationMethod: True" in e.exception.args[0])

    def test_nca_fixture_create_accept_tos(self):
        # Check accept_tos equal False
        new_admin = self.nca_fixture.create(accept_tos=False)
        self.validate_actor(new_admin)

        # Check accept_tos equal API, 'api' and 'API'
        new_admin = self.nca_fixture.create(accept_tos=NCA.AcceptTOSMethod.API)
        self.validate_actor(new_admin)
        new_admin = self.nca_fixture.create(accept_tos='api')
        self.validate_actor(new_admin)
        new_admin = self.nca_fixture.create(accept_tos='API')
        self.validate_actor(new_admin)

        # Check activation being an invalid parameter
        with self.assertRaises(ValueError) as e:
            new_admin = self.nca_fixture.create(accept_tos=True)
        self.assertTrue("Invalid AcceptTOSMethod: True" in e.exception.args[0])

    def test_create_via_accounts_params_no_api_hostname(self):
        with self.assertRaises(ValueError):
            NCA.NetCloudAccountFixture(
                creation_method=NCA.CreationMethod.ACCOUNTS,
                use_activation_email=False,
                accounts_api_protocol="https",
                accounts_api_hostname=None,
                accounts_api_port=443,
                accounts_privileged_username="foo",
                accounts_privileged_password="foo",
                email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"))

    def test_create_via_accounts_params_no_api_protocol(self):
        with self.assertRaises(ValueError):
            NCA.NetCloudAccountFixture(
                creation_method=NCA.CreationMethod.ACCOUNTS,
                use_activation_email=False,
                accounts_api_protocol=None,
                accounts_api_hostname="foo",
                accounts_api_port=443,
                accounts_privileged_username="foo",
                accounts_privileged_password="foo",
                email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"))

    def test_create_via_accounts_params_no_api_port(self):
        with self.assertRaises(ValueError):
            NCA.NetCloudAccountFixture(
                creation_method=NCA.CreationMethod.ACCOUNTS,
                use_activation_email=False,
                accounts_api_protocol="https",
                accounts_api_hostname="foo",
                accounts_api_port=None,
                accounts_privileged_username="foo",
                accounts_privileged_password="foo",
                email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"))

    def test_create_via_accounts_params_no_username(self):
        with self.assertRaises(ValueError):
            NCA.NetCloudAccountFixture(
                creation_method=NCA.CreationMethod.ACCOUNTS,
                use_activation_email=False,
                accounts_api_protocol="https",
                accounts_api_hostname="foo",
                accounts_api_port=443,
                accounts_privileged_username=None,
                accounts_privileged_password="foo",
                email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"))

    def test_create_via_accounts_params_no_password(self):
        with self.assertRaises(ValueError):
            NCA.NetCloudAccountFixture(
                creation_method=NCA.CreationMethod.ACCOUNTS,
                use_activation_email=False,
                accounts_api_protocol="https",
                accounts_api_hostname="foo",
                accounts_api_port=443,
                accounts_privileged_username="foo",
                accounts_privileged_password=None,
                email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"))

    def test_create_params_activate_false_accept_tos_true(self):
        nca_fixture = NCA.NetCloudAccountFixture(
            creation_method=NCA.CreationMethod.ACCOUNTS,
            use_activation_email=False,
            accounts_api_protocol="https",
            accounts_api_hostname="foo",
            accounts_api_port=443,
            accounts_privileged_username="foo",
            accounts_privileged_password="foo",
            email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"))
        admin = nca_fixture.create(accept_tos=False, activate="api")

    def test_create(self):
        new_admin = self.nca_fixture.create()
        self.validate_actor(new_admin)

        # creation without activating or validating the admin
        new_admin = self.nca_fixture.create(activate=False, accept_tos=False)
        self.assertIsNotNone(new_admin)
        self.assertEquals(new_admin.user_state, NCA.NetCloudActor.UserState.CREATED)
        new_admin.activate(authenticate=False)
        self.assertEquals(new_admin.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        new_admin.accept_tos()
        self.assertEquals(new_admin.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)
        self.validate_actor(new_admin)

    def test_create_account(self):
        new_account = self.nca_fixture.create_account()
        self.assertIsNotNone(new_account)
        new_admin = new_account.admin
        self.assertIsNotNone(new_admin)
        self.validate_actor(new_admin)

        # creation without activating or validating the admin
        new_account = self.nca_fixture.create_account(activate=False, accept_tos=False)
        self.assertIsNotNone(new_account)
        new_admin = new_account.admin
        self.assertIsNotNone(new_admin)
        self.assertEquals(new_admin.user_state, NCA.NetCloudActor.UserState.CREATED)
        new_admin.activate(authenticate=False)
        self.assertEquals(new_admin.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        new_admin.accept_tos()
        self.assertEquals(new_admin.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)
        self.validate_actor(new_admin)

        # create NetCloudAccount only passing the fixture and password
        account = self.nca_fixture.create_account(admin_password="Password1!")
        self.validate_account(account)

        # create passing all params
        account = self.nca_fixture.create_account(
            activate=NCA.ActivationMethod.API,
            admin_password="Password1!",
            accept_tos=NCA.AcceptTOSMethod.API,
            entitlements=['ECM_PRIME_1YR'])
        self.validate_account(account)

        # create NetCloudAccount without activating the admin, not accepting TOS
        account = self.nca_fixture.create_account(activate=False, accept_tos=False)
        self.assertEqual(account.admin.user_state, NetCloudActor.UserState.CREATED)

        # create NetCloudAccount without activating the admin and accepting TOS should raise error
        with self.assertRaises(ValueError) as e:
            account = self.nca_fixture.create_account(activate=False, accept_tos=True)
        self.assertTrue("cannot accept the TOS if he does not activate" in e.exception.args[0])

        # UI activation_method is not implemented
        with self.assertRaises(NotImplementedError) as e:
            account = self.nca_fixture.create_account(activate=NCA.ActivationMethod.UI, accept_tos=False)

        # activation_method is invalid
        with self.assertRaises(ValueError) as e:
            account = self.nca_fixture.create_account(activate="Hello", accept_tos=False)
        self.assertTrue("not a valid ActivationMethod" in e.exception.args[0])

        # UI accept_TOS is not implemented
        with self.assertRaises(NotImplementedError) as e:
            account = self.nca_fixture.create_account(activate=NCA.ActivationMethod.API, accept_tos=NCA.AcceptTOSMethod.UI)

        # accept TOS method is invalid
        with self.assertRaises(ValueError) as e:
            account = self.nca_fixture.create_account(activate=NCA.ActivationMethod.API, accept_tos='Hello')
        self.assertTrue("not a valid AcceptTOSMethod" in e.exception.args[0])

        # create NetCloudAccount passing entitlements without activating and accepting TOS
        account = self.nca_fixture.create_account(activate=False, accept_tos=False, entitlements=['ECM_PRIME_1YR'])
        self.assertEqual(account.admin.user_state, NetCloudActor.UserState.CREATED)
        account.admin.activate("Password1!", authenticate=False)
        self.assertEqual(account.admin.user_state, NetCloudActor.UserState.ACTIVATED)
        account.admin.accept_tos()
        account.admin.authenticate()
        self.validate_account(account)
