import copy
import time
import typing
import unittest
from unittest.mock import MagicMock

from nctf.nctf_typing import NetCloudActor
from nctf.test_lib.accounts.rest.client import AccountsEndpoint as AccountsAccountsEndpoint
from nctf.test_lib.accounts.rest.client import AccServRESTClient
from nctf.test_lib.accounts.rest.client import PasswordTokensEndpoint as AccountsPasswordTokensEndpoint
from nctf.test_lib.accounts.rest.client import UsersEndpoint as AccountsUsersEndpoint
from nctf.test_lib.core.email import EmailFixture
from nctf.test_lib.core.email import EmailService
from nctf.test_lib.core.email.base import EmailMessage
from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as NCA
from nctf.test_lib.netcloud.fixtures.netcloud_account import Roles


class TestNetCloudActor(unittest.TestCase):
    def setUp(self):
        # Mocks
        time.sleep = MagicMock()  # Disable sleep since we have a hard wait for email currently.
        AccountsAccountsEndpoint.create = MagicMock()
        AccountsUsersEndpoint.create = MagicMock()
        AccountsPasswordTokensEndpoint.update = MagicMock()
        mock_message = EmailMessage(
            id_="foo",
            subject="bar",
            sender="noreply@cradlepoint.com",
            body='foobar <a href=".+?passwordTokens/test123"><span class="teal-text">HERE</span></a> bazbang')
        MailhogInbox.get_messages_by_subject = MagicMock(return_value=[mock_message])
        AccServRESTClient.post = MagicMock()

        self.mock_user_json = {
            "data": {
                "type": "users",
                "id": "uK9wrbjl7nIDl2KO",
                "attributes": {
                    "login": "julie_peters_fs1jppm2@mailinator.com",
                    "email": "julie_peters_fs1jppm2@mailinator.com",
                    "firstName": "Julie",
                    "lastName": "Peters",
                    "tenantId": "0016C00000AuFAZ",
                    "applications": ["ecm"],
                    "role": "rootAdmin",
                    "isActive": True,
                    "mfaEnabled": False,
                    "lastLogin": "2018-08-02T22:26:38.096569Z",
                    "isLocked": False,
                    "isFederated": False
                },
                "relationships": {
                    "roles": {
                        "data": [{
                            "type": "roles",
                            "id": "rootAdmin"
                        }]
                    },
                    "authorizations": {
                        "data": [{
                            "type": "authorizations",
                            "id": "116051"
                        }],
                        "meta": {
                            "count": 1
                        }
                    }
                },
                "links": {
                    "self": "https://accounts-qa2.cradlepointecm.com/api/v1/users/uK9wrbjl7nIDl2KO"
                }
            }
        }

        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return copy.deepcopy(self.json_data)

        AccServRESTClient.get = MagicMock(return_value=MockResponse(self.mock_user_json, 200))
        # Validate on the args passed into put, not on whether the put does the update, which it doesn't, just return the
        # canned json response so that the parsers of the json don't blow up
        AccServRESTClient.put = MagicMock(return_value=MockResponse(self.mock_user_json, 200))
        self.mock_ncm_users_get_json = {
            "success":
            True,
            "data": [{
                "resource_uri": "/api/v1/users/332244/",
                "username": "RK4iGXEvEQVi5ZWS",
                "first_name": "Julie",
                "last_name": "Peters",
                "email": "julie_peters_fs1jppm2@mailinator.com",
                "is_active": True,
                "last_login": None,
                "date_joined": "2018-10-18T21:43:58+00:00",
                "profile": "/api/v1/profiles/331452/",
                "authorizations": "/api/v1/users/332244/authorizations/",
                "id": "332244"
            }],
            "meta": {
                "offset": 0,
                "limit": 20,
                "total_count": 1,
                "previous": None,
                "next": None
            }
        }
        NCMv1RESTClient.get = MagicMock(return_value=MockResponse(self.mock_ncm_users_get_json, 200))
        NCMv1RESTClient.post = MagicMock()
        self.nca_fixture = NCA.NetCloudAccountFixture(
            creation_method=NCA.CreationMethod.ACCOUNTS,
            use_activation_email=True,
            accounts_api_protocol="https",
            accounts_api_hostname="foo",
            accounts_api_port=443,
            accounts_privileged_username="foo",
            accounts_privileged_password="foo",
            email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"),
            ncm_api_protocol="https",
            ncm_api_hostname="foo",
            ncm_api_port=443)
        self.account = self.nca_fixture.create_account()

    def validate_actor(self, actor):
        self.assertTrue(AccountsAccountsEndpoint.create.called)
        self.assertTrue(AccountsUsersEndpoint.create.called)
        self.assertTrue(AccountsPasswordTokensEndpoint.update.called)
        self.assertIsNotNone(actor.username)
        self.assertEqual(actor.password, "Password1!")
        self.assertEquals(actor.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)

    def test_netcloud_actor_instantiation_params(self):

        # create NetCloudActor passing only a username and password - pre-defined user use case
        # This is the ONLY acceptable use case of directly instantiating a NetCloudActor outside of account implementation!
        user = NetCloudActor('cp.jrny@gmail.com', 'Quality805!')
        self.assertEqual(user.username, "cp.jrny@gmail.com")
        self.assertEqual(user.password, "Quality805!")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        # test the user state progression
        user.authenticate(
            accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
            accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
            accounts_api_port=self.nca_fixture.accounts_api_port)
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)

        # create NetCloudActor passing in a username and an account
        # The inability to transition to CREATED, activate and authenticate is what makes this an unacceptable way to
        # instantiate an existing or one with the given email user....call account.create_user(username='username') or
        # account.get_user_by_login('username') instead.
        user = NetCloudActor('cp.jrny@gmail.com', netcloud_account=self.account)
        self.assertIsNone(user.password)
        self.assertEqual(user.username, "cp.jrny@gmail.com")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.NONEXISTENT)
        with self.assertRaises(RuntimeError) as e:
            user.activate(password="Password1!", authenticate=False)
        self.assertTrue("Needs to be 'CREATED' from account.create_user()" in e.exception.args[0])
        with self.assertRaises(RuntimeError) as e:
            user.authenticate("Password1!")
        self.assertTrue("needs to be activated before it can be authenticated" in e.exception.args[0])

        # create NetCloudActor passing in a username, password and an account - an existing user use case
        # The inability to activate and authenticate is what makes this an unacceptable way to
        # instantiate an existing user....call account.get_user_by_login() instead.
        user = NetCloudActor('cp.jrny@gmail.com', 'Quality805!', netcloud_account=self.account)
        self.assertEqual(user.username, "cp.jrny@gmail.com")
        self.assertEqual(user.password, "Quality805!")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.NONEXISTENT)
        # test user state transitions -
        # Try to transition to ACTIVATED
        with self.assertRaises(RuntimeError) as e:
            user.activate(password="Password1!", authenticate=False)
        self.assertTrue("Needs to be 'CREATED' from account.create_user()" in e.exception.args[0])
        # Try to transition to AUTHENTICATED, since this is assumed to be an existing user, it should work
        with self.assertRaises(RuntimeError) as e:
            user.authenticate("Password1!")
        self.assertTrue("needs to be activated before it can be authenticated" in e.exception.args[0])

        # create NetCloudActor with only username - unable to transition to CREATED
        user = NetCloudActor('cp.jrny@gmail.com')
        self.assertEqual(user.username, "cp.jrny@gmail.com")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.NONEXISTENT)
        # test user state transitions -
        # Try to transition to ACTIVATED
        with self.assertRaises(RuntimeError) as e:
            user.activate(password="Password1!", authenticate=False)
        self.assertTrue("Needs to be 'CREATED' from account.create_user()" in e.exception.args[0])
        # Try to transition to AUTHENTICATED, since this is assumed to be an existing user, it should work
        with self.assertRaises(RuntimeError) as e:
            user.authenticate("Password1!")
        self.assertTrue("needs to be activated before it can be authenticated" in e.exception.args[0])

        # create NetCloudActor with username and assuming it is a pre-existing user that has not been activated
        user = NetCloudActor('cp.jrny@gmail.com', user_state=NCA.NetCloudActor.UserState.CREATED)
        self.assertEqual(user.username, "cp.jrny@gmail.com")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.CREATED)
        # test user state transitions -
        with self.assertRaises(ValueError) as e:
            user.activate(
                password="Password1!",
                authenticate=False,
                accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
                accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
                accounts_api_port=self.nca_fixture.accounts_api_port)
        self.assertTrue("Cannot activate without a password token" in e.exception.args[0])
        user.activate(
            password="Password1!",
            password_token="junk",
            authenticate=False,
            accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
            accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
            accounts_api_port=self.nca_fixture.accounts_api_port)
        user.authenticate("Password1!")
        self.validate_actor(user)

        # create NetCloudActor with username and assuming it is a pre-existing user that has been activated
        user = NetCloudActor('cp.jrny@gmail.com', user_state=NCA.NetCloudActor.UserState.ACTIVATED)
        self.assertEqual(user.username, "cp.jrny@gmail.com")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        # test user state transitions -
        user.authenticate(
            "Password1!",
            accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
            accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
            accounts_api_port=self.nca_fixture.accounts_api_port)

    def test_activate(self):
        # Can't activate a user that has been instantiated out of thin air.
        user = NetCloudActor('cp.jrny@gmail.com', netcloud_account=self.account)
        with self.assertRaises(RuntimeError) as e:
            user.activate(password="Password1!")
        self.assertTrue("Needs to be 'CREATED' from account.create_user()" in e.exception.args[0])

        # Can't activate a pre-exiting CREATED user that has not been given a password token
        user = NetCloudActor('cp.jrny@gmail.com', user_state=NCA.NetCloudActor.UserState.CREATED)
        with self.assertRaises(ValueError) as e:
            user.activate(
                password="Password1!",
                authenticate=False,
                accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
                accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
                accounts_api_port=self.nca_fixture.accounts_api_port)
        self.assertTrue("Cannot activate without a password token" in e.exception.args[0])
        user.activate(
            password="Password1!",
            password_token="junk",
            authenticate=False,
            accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
            accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
            accounts_api_port=self.nca_fixture.accounts_api_port)
        user.authenticate()
        self.validate_actor(user)

        # Activate a pre-exiting CREATED user that has been given a password token
        user = NetCloudActor('cp.jrny@gmail.com', password_token="junk", user_state=NCA.NetCloudActor.UserState.CREATED)
        user.activate(
            password="Password1!",
            authenticate=False,
            accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
            accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
            accounts_api_port=self.nca_fixture.accounts_api_port)
        user.authenticate()
        self.validate_actor(user)

        # UI activation_method is not implemented
        user = self.account.create_user(activation_method=False, authenticate=False)
        with self.assertRaises(NotImplementedError):
            user.activate(password="Password1!", activation_method=NCA.ActivationMethod.UI)

        # Check activation_method equal API, 'api' and 'API'
        user.activate(activation_method=NCA.ActivationMethod.API, password="Password1!")
        self.validate_actor(user)
        user = self.account.create_user(activation_method=False, authenticate=False)
        user.activate(activation_method='api', password="Password1!")
        self.validate_actor(user)
        user = self.account.create_user(activation_method=False, authenticate=False)
        user.activate(activation_method='API', password="Password1!")
        self.validate_actor(user)

        # Check activation_method being an invalid parameter
        user = self.account.create_user(activation_method=False, authenticate=False)
        with self.assertRaises(ValueError) as e:
            user.activate(activation_method=False, password="Password1!")
        self.assertTrue("Invalid ActivationMethod: False" in e.exception.args[0])

        # Activating a user created from an account, without a password uses the default password
        user = self.account.create_user(activation_method=False, authenticate=False)
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.CREATED)
        user.activate()
        self.validate_actor(user)

        # Activating a user created from an account, without passing a password when user instantiated with a password is ok
        user = self.account.create_user(password="Password1!", activation_method=False, authenticate=False)
        user.activate(activation_method=NCA.ActivationMethod.API)
        self.validate_actor(user)

        # Activating a user created from an account, without authenticating
        user = self.account.create_user(activation_method=False, authenticate=False)
        user.activate(password="Password1!", activation_method=NCA.ActivationMethod.API, authenticate=False)
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        user.authenticate()
        self.validate_actor(user)

    def test_authenticate(self):
        # Pre-defined users created outside of an account need to have accounts service endpoints to authenticate
        user = NetCloudActor('cp.jrny@gmail.com', "Password1!")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        user.authenticate(
            accounts_api_protocol=self.nca_fixture.accounts_api_protocol,
            accounts_api_hostname=self.nca_fixture.accounts_api_hostname,
            accounts_api_port=self.nca_fixture.accounts_api_port)
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)
        self.validate_actor(user)

        # Authenticate without AccServRESTClient endpoints for pre-defined users not associated with an account raises
        user = NetCloudActor('cp.jrny@gmail.com', "Password1!")
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.ACTIVATED)
        with self.assertRaises(ValueError) as e:
            user.authenticate()
        self.assertTrue("needs to have AccServRESTClient endpoints" in e.exception.args[0])

        # Pre-existing user instantiated with an account
        user = NetCloudActor('cp.jrny@gmail.com', "Password1!", netcloud_account=self.account)
        self.assertEquals(user.user_state, NCA.NetCloudActor.UserState.NONEXISTENT)
        with self.assertRaises(RuntimeError) as e:
            user.authenticate()
        self.assertTrue("needs to be activated before it can be authenticated" in e.exception.args[0])

        # Authenticate a random created user by passing in a password
        user = self.account.create_user(password="Password1!", authenticate=False)
        user.authenticate(password="Password1!")
        self.validate_actor(user)

        # Authenticate a random created user without passing in a password since it was created/activated with one.
        user = self.account.create_user(password="Password1!", authenticate=False)
        user.authenticate()
        self.validate_actor(user)

        # Authenticate a random created user that doesn't have a password
        user = self.account.create_user(password="Password1!", authenticate=False)
        # Artificially None out the password
        user.password = None
        user.accserv.password = None
        with self.assertRaises(RuntimeError) as e:
            user.authenticate()
        self.assertTrue("need to authenticate with a username and password" in e.exception.args[0])

    def test_attribute_gets(self):
        user = self.account.create_user(password="Password1!", authenticate=False)
        self.assertEquals(user.accserv.get_accounts_user_record().json(), self.mock_user_json)
        self.assertEquals(user.accserv.tenant_id, self.mock_user_json["data"]["attributes"]["tenantId"])
        self.assertEquals(user.accserv.email, self.mock_user_json["data"]["attributes"]["email"])
        self.assertEquals(user.accserv.login, self.mock_user_json["data"]["attributes"]["login"])
        self.assertIs(user.netcloud_account, self.account)
        self.assertEquals(user.accserv.mfa_enabled, self.mock_user_json["data"]["attributes"]["mfaEnabled"])
        self.assertEquals(user.accserv.last_name, self.mock_user_json["data"]["attributes"]["lastName"])
        self.assertEquals(user.accserv.first_name, self.mock_user_json["data"]["attributes"]["firstName"])
        self.assertEquals(user.accserv.role.value, self.mock_user_json["data"]["attributes"]["role"])
        self.assertEquals(user.accserv.is_active, self.mock_user_json["data"]["attributes"]["isActive"])
        self.assertEquals(user.accserv.applications, self.mock_user_json["data"]["attributes"]["applications"])
        self.assertEquals(user.accserv.is_federated, self.mock_user_json["data"]["attributes"]["isFederated"])

    def validate_update_args(self, attr: str, value: any, put_json: typing.Dict) -> bool:
        expected_json = copy.deepcopy(self.mock_user_json)
        expected_json["data"]["attributes"][attr] = value
        return expected_json == put_json

    def test_attribute_puts(self):
        user = self.account.create_user(password="Password1!", authenticate=False)
        user.accserv.tenant_id = "test string"
        self.assertTrue(self.validate_update_args("tenantId", "test string", AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.email = "new_email@new.com"
        self.assertTrue(self.validate_update_args("email", "new_email@new.com", AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.mfa_enabled = True
        self.assertTrue(self.validate_update_args("mfaEnabled", True, AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.last_name = "qrstuv"
        self.assertTrue(self.validate_update_args("lastName", "qrstuv", AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.first_name = "wxyzabc"
        self.assertTrue(self.validate_update_args("firstName", "wxyzabc", AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.role = Roles.ADMIN
        self.assertTrue(self.validate_update_args("role", Roles.ADMIN.value, AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.is_active = False
        self.assertTrue(self.validate_update_args("isActive", False, AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.applications = ['xyz']
        self.assertTrue(self.validate_update_args("applications", ['xyz'], AccServRESTClient.put.call_args[1]["json"]))
        user.accserv.is_federated = True
        self.assertTrue(self.validate_update_args("isFederated", True, AccServRESTClient.put.call_args[1]["json"]))
