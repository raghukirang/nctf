import copy
import time
import typing
import unittest
from unittest.mock import MagicMock

from nctf.nctf_typing import NetCloudActor
from nctf.test_lib.accounts.rest.client import AccountsEndpoint as AccountsAccountsEndpoint
from nctf.test_lib.accounts.rest.client import AccServRESTClient
from nctf.test_lib.accounts.rest.client import PasswordTokensEndpoint as AccountsPasswordTokensEndpoint
from nctf.test_lib.accounts.rest.client import UsersEndpoint as AccountsUsersEndpoint
from nctf.test_lib.core.email import EmailFixture
from nctf.test_lib.core.email import EmailService
from nctf.test_lib.core.email.base import EmailMessage
from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_account as NCA
from nctf.test_lib.netcloud.fixtures.netcloud_account import NetCloudAccount


class TestNetCloudAccount(unittest.TestCase):
    def setUp(self):
        # Mocks
        time.sleep = MagicMock()  # Disable sleep since we have a hard wait for email currently.
        AccountsPasswordTokensEndpoint.update = MagicMock()
        mock_message = EmailMessage(
            id_="foo",
            subject="bar",
            sender="noreply@cradlepoint.com",
            body='foobar <a href=".+?passwordTokens/test123"><span class="teal-text">HERE</span></a> bazbang')
        MailhogInbox.get_messages_by_subject = MagicMock(return_value=[mock_message])
        AccServRESTClient.post = MagicMock()
        self.nca_fixture = NCA.NetCloudAccountFixture(
            creation_method=NCA.CreationMethod.ACCOUNTS,
            use_activation_email=True,
            accounts_api_protocol="https",
            accounts_api_hostname="foo",
            accounts_api_port=443,
            accounts_privileged_username="foo",
            accounts_privileged_password="foo",
            email_fixture=EmailFixture(service=EmailService.MAILHOG, api_root="foo"),
            ncm_api_protocol="https",
            ncm_api_hostname="foo",
            ncm_api_port=443)
        self.mock_user_json = {
            "data": {
                "type": "users",
                "id": "uK9wrbjl7nIDl2KO",
                "attributes": {
                    "login": "julie_peters_fs1jppm2@mailinator.com",
                    "email": "julie_peters_fs1jppm2@mailinator.com",
                    "firstName": "Julie",
                    "lastName": "Peters",
                    "tenantId": "0016C00000AuFAZ",
                    "applications": ["ecm"],
                    "role": "rootAdmin",
                    "isActive": True,
                    "mfaEnabled": False,
                    "lastLogin": "2018-08-02T22:26:38.096569Z",
                    "isLocked": False,
                    "isFederated": False
                },
                "relationships": {
                    "roles": {
                        "data": [{
                            "type": "roles",
                            "id": "rootAdmin"
                        }]
                    },
                    "authorizations": {
                        "data": [{
                            "type": "authorizations",
                            "id": "116051"
                        }],
                        "meta": {
                            "count": 1
                        }
                    }
                },
                "links": {
                    "self": "https://accounts-qa2.cradlepointecm.com/api/v1/users/uK9wrbjl7nIDl2KO"
                }
            }
        }

        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return copy.deepcopy(self.json_data)

        AccountsUsersEndpoint.get_user = MagicMock(return_value=MockResponse(self.mock_user_json, 200))
        # Validate on the args passed into put, not on whether the put does the update, which it doesn't, just return the
        # canned json response so that the parsers of the json don't blow up
        AccountsUsersEndpoint.put = MagicMock(return_value=MockResponse(self.mock_user_json, 200))

        self.mock_account_json = {
            "data": {
                "type": "accounts",
                "id": "0016C000004FgtX",
                "attributes": {
                    "name": "Journey",
                    "contact": "uK9wrbjl7nIDl2KO",
                    "sessionLength": 7200,
                    "enhancedLoginSecurityEnabled": False,
                    "federatedId": "Cradlepoint",
                    "identityProviderCertificate": None,
                    "mfaRequired": False
                },
                "links": {
                    "self": "https://accounts-qa2.cradlepointecm.com/api/v1/accounts/0016C000004FgtX"
                }
            }
        }

        class MockResponse:
            def __init__(self, json_data, status_code):
                self.json_data = json_data
                self.status_code = status_code

            def json(self):
                return copy.deepcopy(self.json_data)

        AccountsAccountsEndpoint.detail = MagicMock(return_value=MockResponse(self.mock_account_json, 200))
        # Validate on the args passed into put, not on whether the put does the update, which it doesn't, just return the
        # canned json response so that the parsers of the json don't blow up
        AccountsAccountsEndpoint.update = MagicMock(return_value=MockResponse(self.mock_account_json, 200))
        self.mock_ncm_users_get_json = {
            "success":
            True,
            "data": [{
                "resource_uri": "/api/v1/users/332244/",
                "username": "RK4iGXEvEQVi5ZWS",
                "first_name": "Julie",
                "last_name": "Peters",
                "email": "julie_peters_fs1jppm2@mailinator.com",
                "is_active": True,
                "last_login": None,
                "date_joined": "2018-10-18T21:43:58+00:00",
                "profile": "/api/v1/profiles/331452/",
                "authorizations": "/api/v1/users/332244/authorizations/",
                "id": "332244"
            }],
            "meta": {
                "offset": 0,
                "limit": 20,
                "total_count": 1,
                "previous": None,
                "next": None
            }
        }
        NCMv1RESTClient.get = MagicMock(return_value=MockResponse(self.mock_ncm_users_get_json, 200))
        NCMv1RESTClient.post = MagicMock()

    def validate_actor(self, actor):
        self.assertIsNotNone(actor.username)
        self.assertIsNotNone(actor.password)
        self.assertEquals(actor.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)

    def test_netcloud_account_instantiation_params(self):

        # create NetCloudAccount only passing the fixture
        account = NetCloudAccount(self.nca_fixture)
        self.assertIsNotNone(account.admin)
        self.assertIsNotNone(account.tenant_id)
        self.assertIsNotNone(account.admin.username)
        self.assertEquals(account.admin.password, "Password1!")
        self.assertEquals(account.admin.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)

        # create NetCloudAccount only passing the fixture and an actor with a username and password
        account = NetCloudAccount(self.nca_fixture, admin_username="username@mail.com", admin_password="Password1!")
        self.assertIsNotNone(account.admin)
        self.assertIsNotNone(account.tenant_id)
        self.assertEquals(account.admin.username, "username@mail.com")
        self.assertEquals(account.admin.password, 'Password1!')
        self.assertEquals(account.admin.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)

        # create NetCloudAccount only passing the fixture and an actor with a username
        account = NetCloudAccount(self.nca_fixture, admin_username="username@mail.com")
        self.assertIsNotNone(account.admin)
        self.assertIsNotNone(account.tenant_id)
        self.assertEquals(account.admin.password, "Password1!")
        self.assertEquals(account.admin.user_state, NCA.NetCloudActor.UserState.AUTHENTICATED)
        self.assertEquals(account.admin.username, "username@mail.com")

    def test_account_properties_get(self):
        account = self.nca_fixture.create_account()
        assert account.accserv.name == self.mock_account_json["data"]["attributes"]["name"]
        assert account.accserv.contact == self.mock_account_json["data"]["attributes"]["contact"]
        assert account.accserv.session_length == self.mock_account_json["data"]["attributes"]["sessionLength"]
        assert account.accserv.enhanced_login_security_enabled == \
               self.mock_account_json["data"]["attributes"]["enhancedLoginSecurityEnabled"]
        # federated_id should be "new federated id" based on the ok status return on the put above, but it didn't change
        assert account.accserv.federated_id == self.mock_account_json["data"]["attributes"]["federatedId"]
        assert account.accserv.identity_provider_cert == self.mock_account_json["data"]["attributes"][
            "identityProviderCertificate"]
        assert account.accserv.mfa_required == self.mock_account_json["data"]["attributes"]["mfaRequired"]

    def test_account_properties_update(self):
        account = self.nca_fixture.create_account()
        account.accserv.contact = "test string"
        self.assertEqual("test string", AccountsAccountsEndpoint.update.call_args[1]["contact"])
        account.accserv.session_length = 1313
        self.assertEqual(1313, AccountsAccountsEndpoint.update.call_args[1]["session_length"])
        account.accserv.enhanced_login_security_enabled = True
        self.assertEqual(True, AccountsAccountsEndpoint.update.call_args[1]["enhanced_login_security_enabled"])
        account.accserv.federated_id = "new federated id"
        self.assertEqual("new federated id", AccountsAccountsEndpoint.update.call_args[1]["federated_id"])
        account.accserv.identity_provider_cert = "don't know the format of this"
        self.assertTrue("don't know the format of this",
                        AccountsAccountsEndpoint.update.call_args[1]["identity_provider_cert"])
        account.accserv.mfa_required = True
        self.assertEqual(True, AccountsAccountsEndpoint.update.call_args[1]["mfa_required"])

    def test_create_user(self):
        # create random user
        account = self.nca_fixture.create_account()
        user = account.create_user()
        self.validate_actor(user)

        # create a random user passing with a password
        account = self.nca_fixture.create_account()
        user = account.create_user(password="Password1!")
        self.validate_actor(user)

        # create a user with a username and password should raise without having a first and last name
        account = self.nca_fixture.create_account()
        with self.assertRaises(ValueError) as e:
            user = account.create_user("cp.jrny@gmail.com", "Password1!")
        self.assertTrue("Cannot create a user" in e.exception.args[0])
        with self.assertRaises(ValueError) as e:
            user = account.create_user("cp.jrny@gmail.com", "Password1!", first_name="Joe")
        self.assertTrue("Cannot create a user" in e.exception.args[0])
        with self.assertRaises(ValueError) as e:
            user = account.create_user("cp.jrny@gmail.com", "Password1!", last_name="Schmo")
        self.assertTrue("Cannot create a user" in e.exception.args[0])

        # create a user with a username and password and a first and last name
        account = self.nca_fixture.create_account()
        user = account.create_user(username="cp.jrny@gmail.com", password="Password1!", first_name="Joe", last_name="Schmo")
        self.validate_actor(user)
        self.assertTrue(user.username == "cp.jrny@gmail.com")

        # create a user but stop before activation
        account = self.nca_fixture.create_account()
        user = account.create_user(activation_method=False)
        self.assertTrue(user.user_state == NCA.NetCloudActor.UserState.CREATED)
        # continue the state progression by activating and authenticating
        user.activate()
        self.validate_actor(user)

        # create a user but stop before authentication
        account = self.nca_fixture.create_account()
        user = account.create_user(authenticate=False)
        self.assertTrue(user.user_state == NCA.NetCloudActor.UserState.ACTIVATED)
        # continue the state progression by activating and authenticating
        user.authenticate()
        self.validate_actor(user)


#  get_users, get_user_by_login(), delete_user() and delete_user_by_id() are all tested via integration tests

    def test_get_password_token_valid_body(self):
        token = NetCloudAccount._get_password_token('foobar <a href=".+?passwordTokens/test123"><span class="teal-text">HERE</span></a> bazbang')
        self.assertEqual(token, 'test123')

    def test_get_password_token_invalid_body(self):
        token = NetCloudAccount._get_password_token('foo bar baz bang')
        self.assertEqual(token, None)
