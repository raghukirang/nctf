import os
import unittest
from datetime import datetime
from unittest.mock import patch
from nctf.test_lib.ncos.base.capability import NCOSRouterCapabilities
from nctf.test_lib.ncos.base.router import NCOSRouter
from nctf.test_lib.ncos.base.router import NCOSRouterLog
from semantic_version import Version


class NCOSRouterTestCase(unittest.TestCase):

    def setUp(self):
        self.ncos_router = NCOSRouter()


class TestNCOSRouterLogs(NCOSRouterTestCase):

    @staticmethod
    def basic_logs_data():
        mock_log_data = [
            [1540633435, 'INFO', 'udhcpc[31799]', 'Sending renew 192.168.99.101 => 192.168.99.1...', None],
            [1540633435, 'INFO', 'udhcpc[31799]', 'Lease of 192.168.99.101 obtained, lease time 43200', None],
            [1540655035, 'INFO', 'udhcpc[31799]', 'Sending renew 192.168.99.101 => 192.168.99.1...', None],
            [1540655035, 'BOGUS', 'udhcpc[31799]', 'Lease of 192.168.99.101 obtained, lease time 43200', None],
            [1540671479, 'BOGUS', 'logsys', 'Last message repeated 2 times.', None],
            [1540671479, 'BOGUS', 'upgrade', 'overwrite supported: False, downgrade supported: None', None]
        ]
        return mock_log_data

    @staticmethod
    def basic_logs():
        data = TestNCOSRouterLogs.basic_logs_data()
        result = [NCOSRouterLog(time=x[0], level=x[1], source=x[2], message=x[3]) for x in data]
        return result

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    def test_get_logs(self, mock_router_rest_api_status_log_get, mock_authenticate):

        mock_response = dict()
        mock_response['data'] = self.basic_logs_data()
        mock_response['success'] = True
        mock_router_rest_api_status_log_get.return_value = mock_response

        logs = self.ncos_router.get_logs()
        self.assertEqual(logs[2].time, datetime.fromtimestamp(1540655035))
        self.assertEqual(logs[2].level, 'INFO')
        self.assertEqual(logs[2].source, 'udhcpc[31799]')
        self.assertEqual(logs[2].message, 'Sending renew 192.168.99.101 => 192.168.99.1...')

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    @patch('nctf.test_lib.ncos.base.router.NCOSRouter.get_router_capabilities')
    def test_write_logs_to_file(self, mock_capabilities, mock_router_rest_api_status_log_get, mock_authenticate):
        fake_capabilities = NCOSRouterCapabilities()
        fake_capabilities.default_lan_only = False
        fake_capabilities.lan_ports = 1
        fake_capabilities.wan_ports = 1
        mock_capabilities.return_value = fake_capabilities

        mock_response = dict()
        mock_response['data'] = self.basic_logs_data()
        mock_response['success'] = True
        mock_router_rest_api_status_log_get.return_value = mock_response

        path_to_this_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)))
        path_to_log = os.path.join(path_to_this_dir, 'tst-log.txt')

        self.ncos_router.write_logs_to_file(path_to_file=path_to_log)

        with open(path_to_log) as f:
            line = f.readline()
            self.assertIn('INFO udhcpc[31799] Sending renew 192.168.99.101 => 192.168.99.1...', line)

    def test_filter_logs(self):
        logs = self.basic_logs()

        # multiple matches, case insensitive, using a string
        assert len(NCOSRouterLog.filter_log(search="ReNeW", logs=logs)) == 2

        # string search, no matches
        assert len(NCOSRouterLog.filter_log(search="nothing matches this", logs=logs)) == 0

        # lambda search matches
        assert len(NCOSRouterLog.filter_log(search=lambda l: l.level == "BOGUS", logs=logs)) == 3

        # lambda search, nothing matched
        assert len(NCOSRouterLog.filter_log(search=lambda l: l.time == 12345, logs=logs)) == 0

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.update')
    def test_clear_router_logs(self, mock_router_rest_api_control_log_update, mock_authenticate):

        mock_response = dict()
        mock_response['data'] = {}
        mock_response['success'] = True
        mock_router_rest_api_control_log_update.return_value = mock_response

        self.ncos_router.clear_logs()
        mock_router_rest_api_control_log_update.assert_called_with('', {'clear': True})

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.update')
    def test_unregister(self, mock_router_rest_api_control_ecm_unregister_update, mock_authenticate):
        mock_response = dict()
        mock_response['data'] = {}
        mock_response['success'] = True
        mock_router_rest_api_control_ecm_unregister_update.return_value = mock_response

        self.ncos_router.unregister()
        mock_router_rest_api_control_ecm_unregister_update.assert_called_with(update='true')

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    def test_fw_version(self, mock_router_rest_api_status_fw_info_get, mock_authenticate):
        mock_response = dict()
        mock_response['data'] = {'major_version': 6, 'minor_version': 6, 'patch_version': 0}
        mock_response['success'] = True
        mock_router_rest_api_status_fw_info_get.return_value = mock_response

        fw_version = self.ncos_router.fw_version
        assert isinstance(fw_version, Version)
        assert fw_version == Version('6.6.0')
        mock_router_rest_api_status_fw_info_get.assert_called()

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.update')
    def test_register(self, mock_router_rest_api_control_ecm_register_update, mock_router_rest_api_status_ecm_state_get, mock_authenticate):
        mock_response = dict()
        mock_response['data'] = {}
        mock_response['success'] = True
        mock_router_rest_api_control_ecm_register_update.return_value = mock_response

        mock_response1 = dict()
        mock_response1['data'] = 'connecting'
        mock_response1['success'] = True
        mock_response2 = dict()
        mock_response2['data'] = 'connected'
        mock_response2['success'] = True
        fake_responses = [mock_response1, mock_response2]
        mock_router_rest_api_status_ecm_state_get.side_effect = fake_responses

        self.ncos_router.register('foo', 'bar1234')

        mock_router_rest_api_status_ecm_state_get.assert_called()
        mock_router_rest_api_control_ecm_register_update.assert_called()

    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.RouterRESTAPI.authenticate')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.get')
    @patch('nctf.test_lib.ncos.fixtures.router_api.fixture.ConfigureEndpoint.update')
    @patch('nctf.test_lib.ncos.base.router.NCOSRouter.get_router_capabilities')
    def test_reboot(self, mock_capabilities, mock_router_rest_api_control_system_update, mock_router_rest_api_status_system_get, mock_authenticate):
        fake_capabilities = NCOSRouterCapabilities()
        fake_capabilities.default_lan_only = False
        fake_capabilities.lan_ports = 1
        fake_capabilities.wan_ports = 1
        mock_capabilities.return_value = fake_capabilities

        mock_response = dict()
        mock_response['data'] = {}
        mock_response['success'] = True
        mock_router_rest_api_control_system_update.return_value = mock_response

        mock_router_rest_api_status_system_get.side_effect = [
            {'success': True, 'data': 'foo1234'},  # initial bootid
            {'success': True, 'data': 'foo1234'},  # bootid we didn't reboot yet
            {'success': True, 'data': 'bar4321'},  # bootid we did reboot
            {'success': True, 'data': 'started'},   # license service started
            {'success': True, 'data': 'ethernet-wan'},  # /status/wan/primary_device
            {'success': True, 'data': 'connected'}  # /status/wan/connection_state
            ]

        self.ncos_router.reboot()

        mock_router_rest_api_control_system_update.assert_called()
        mock_router_rest_api_status_system_get.assert_called()
