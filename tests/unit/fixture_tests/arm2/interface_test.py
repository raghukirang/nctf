import json
import logging
import sys
import unittest
from unittest.mock import MagicMock

from arm2.client.objects import ARMLeasedWifiClient
from arm2.client.objects import ARMWiFiClient
import requests_mock

from nctf.test_lib.core.arm2.interface import ARMInterface
from nctf.test_lib.core.arm2.interface import DeviceCapability
from nctf.test_lib.core.arm2.interface import DevicePool
from nctf.test_lib.core.arm2.interface import ROUTER_CAPABILITIES
from nctf.test_lib.core.arm2.router import ARMRouter
from nctf.test_lib.core.arm2.wifi_client import ARMWifiClient
from nctf.test_lib.core.arm2.wifi_router import ARMWifiRouter

logger = logging.getLogger('arm.interface')
logger.level = logging.INFO


class ARMInterfaceTestCase(unittest.TestCase):
    def setUp(self):
        self.host = 'http://foo.com'
        self.port = 80
        self.path = '/api/v1/'
        self.server = 'http://foo.com:80/api/v1/'
        self.arm = ARMInterface(host=self.host, port=self.port, api_path=self.path, api_key='special_key', lease_wait=3)


class TestARMInterface(ARMInterfaceTestCase):
    def setUp(self):
        super().setUp()
        self.leased_router_json = {
            "data": [{
                "id": "r1",
                "dirty": False,
                "mac": "203044aa0001",
                "lan_client": "client2",
                "type": "AER3100",
                "gateway": 'foo.com',
                "remote_admin_port": 8100,
                "ssh_port": 22,
                "authkey": "key1",
                "root_authkey": "key2",
                "hw_map": "map",
                "lease": None,
                "unhealthy": False
            }]
        }
        self.leased_wifi_router_json = {
            "data": [{
                "id": "wr1",
                "model": "AER3100",
                "dirty": False,
                "ssh_host": "",
                "ssh_port": 22,
                "remote_admin_host": 'foo.com',
                "remote_admin_port": 8100,
                "remote_admin_user": "user",
                "remote_admin_pass": "pass1",
                "root_pass": "pass2",
                "lease": None,
                "unhealthy": False
            }]
        }
        self.wifi_router_json = {
            "data": [{
                "id": "wr1",
                "model": "AER3100",
                "dirty": False,
                "unhealthy": False,
                "lease": None
            }]
        }
        self.wifi_client_json = {
            "data": [{
                "id": "Galaxy_S7_1",
                "type": "android",
                "dirty": False,
                "unhealthy": False,
                "wifi_client_interfaces": [],
                "lease": None
            }]
        }
        self.arm_leased_wifi_client = ARMLeasedWifiClient()
        self.arm_leased_wifi_client.connection_config = {
            "device_id": "id",
            "ssh_username": "name",
            "ssh_password": "pass",
            "ssh_host": "foo.bar.com",
            "ssh_port": 22,
            "model": "Galaxy_S7"
        }
        self.arm_leased_wifi_client.type = "android"
        self.arm_leased_wifi_client.id = "Galaxy_S7_1"
        self.arm_leased_wifi_client.lease = {
            "id": 160151,
            "leased_at": "2017-10-11T20:42:02",
            "lessee": {
                "id": 42,
                "name": "Arm_Maintenance",
                "email": None,
                "admin": True
            }
        }

    def test_get_router(self):
        self.arm.lease_router = MagicMock(return_value=self.leased_router_json['data'][0])
        self.arm.health_check_router = MagicMock
        with requests_mock.mock() as m:
            m.register_uri('GET', '/api/status/fw_info', json={"data": {"foo": "bar"}, "success": True})
            m.register_uri('GET', '/api', json={"data": {"foo": "bar"}, "success": True})
            router = self.arm.get_router()
        self.assertEqual(router.product, "AER3100")

    def test_get_wifi_router(self):
        self.arm.lease_router = MagicMock(return_value=self.leased_wifi_router_json['data'][0])
        self.arm.health_check_wifi_router = MagicMock
        with requests_mock.mock() as m:
            m.get('/api/status/fw_info', json={'success': True}, status_code=200)
            m.put('/api/config/system/logging', json={'success': True}, status_code=200)
            m.put('/api/control/log', json={'success': True, 'clear': True}, status_code=200)
            router = self.arm.get_wifi_router()
            self.assertEqual(router.model, "AER3100")

    def test_get_capability_routers(self):
        with open(ROUTER_CAPABILITIES) as json_file:
            parsed_json = json.load(json_file)
        wifi_list = [item['name'] for item in parsed_json['devices'] if item[DeviceCapability.WIFI.value]]
        self.arm.get_arm_router_list = MagicMock(return_value=self.leased_wifi_router_json['data'])
        router_list = self.arm.get_capability_routers([DeviceCapability.WIFI], pool=DevicePool.WIFI_ROUTERS)
        self.arm.get_arm_router_list = MagicMock(return_value=self.leased_router_json['data'])
        router_list += self.arm.get_capability_routers([DeviceCapability.WIFI], pool=DevicePool.ROUTERS)
        for router in router_list:
            self.assertIn(router, wifi_list)

    def test_is_valid_type(self):
        self.arm.get_arm_router_list = MagicMock(return_value=self.leased_wifi_router_json['data'])
        self.arm.is_valid_type(router_type='model', pool=DevicePool.WIFI_ROUTERS)

    def test_is_valid_id(self):
        self.arm.get_arm_router_list = MagicMock(return_value=self.leased_router_json['data'])
        self.arm.is_valid_type(router_type='type')

    def test_get_arm_router_list(self):
        with requests_mock.mock() as m:
            url = self.server + 'routers/'
            m.get(url, json=self.wifi_router_json, status_code=200)
            self.arm.session.get(url)
            routers = self.arm.get_arm_router_list()
            self.assertListEqual(routers, self.wifi_router_json['data'])

    def test_health_check_router(self):
        leased_router = self.leased_router_json['data'][0]
        with requests_mock.mock() as m:
            m.register_uri('GET', '/api', json={"data": {"foo": "bar"}, "success": True})
            m.register_uri('GET', '/api/status/fw_info', json={"data": {"foo": "bar"}, "success": True})
            arm_router = ARMRouter(
                manager=self.arm,
                product=leased_router['type'],
                router_id=leased_router['id'],
                mac=leased_router['mac'],
                gateway=leased_router['gateway'],
                authkey=leased_router['authkey'],
                root_authkey=leased_router['root_authkey'],
                remote_admin_port=leased_router['remote_admin_port'],
                lan_client=leased_router['lan_client'],
                ssh_port=leased_router['ssh_port'])
        with requests_mock.mock() as m:
            url = "http://{}:{}".format(arm_router.gateway, arm_router.remote_admin_port)
            m.register_uri('GET', '{}/api/status/fw_info'.format(url), json={"data": {"foo": "bar"}, "success": True})
            stream_handler = logging.StreamHandler(sys.stdout)
            logger.addHandler(stream_handler)
            m.get(url, json=self.leased_router_json, status_code=200)
            try:
                with self.assertLogs() as cm:
                    self.arm.health_check_router(router=arm_router)
                    self.assertEqual(
                        cm.output[0],
                        "INFO:test_lib.core.arm:<ARMRouter(router_id='r1', product='AER3100', ecm_client_id='None')> appears to be healthy."
                    )
            finally:
                logger.removeHandler(stream_handler)

    def test_health_check_wifi_router(self):
        leased_router = self.leased_wifi_router_json['data'][0]

        with requests_mock.mock() as m:
            m.get('/api/status/fw_info', json={'success': True}, status_code=200)
            m.put('/api/config/system/logging', json={'success': True}, status_code=200)
            m.put('/api/control/log', json={'success': True, 'clear': True}, status_code=200)
            arm_router = ARMWifiRouter(
                manager=self.arm,
                model=leased_router['model'],
                router_id=leased_router['id'],
                root_pass=leased_router['root_pass'],
                remote_admin_host=leased_router['remote_admin_host'],
                remote_admin_port=leased_router['remote_admin_port'],
                remote_admin_user=leased_router['remote_admin_user'],
                remote_admin_pass=leased_router['remote_admin_pass'],
                ssh_host=leased_router['ssh_host'],
                ssh_port=leased_router['ssh_port'])
            url = "http://{}:{}".format(arm_router.remote_admin_host, arm_router.remote_admin_port)
            m.register_uri('GET', '{}/api/status/fw_info'.format(url), json={"data": {"foo": "bar"}, "success": True})

            stream_handler = logging.StreamHandler(sys.stdout)
            logger.addHandler(stream_handler)
            m.get(url, json=self.leased_router_json, status_code=200)
            try:
                with self.assertLogs() as cm:
                    self.arm.health_check_wifi_router(router=arm_router)
                    self.assertEqual(
                        cm.output[0],
                        "INFO:test_lib.core.arm:<ARMWifiRouter(router_id='wr1', model='AER3100')> appears to be healthy.")
            finally:
                logger.removeHandler(stream_handler)

    def test_release_router(self):
        leased_router = self.leased_router_json['data'][0]
        with requests_mock.mock() as m:
            m.register_uri('GET', '/api', json={"data": {"foo": "bar"}, "success": True})
            m.register_uri('GET', '/api/status/fw_info', json={"data": {"foo": "bar"}, "success": True})
            arm_router = ARMRouter(
                manager=self.arm,
                product=leased_router['type'],
                router_id=leased_router['id'],
                mac=leased_router['mac'],
                gateway=leased_router['gateway'],
                authkey=leased_router['authkey'],
                root_authkey=leased_router['root_authkey'],
                remote_admin_port=leased_router['remote_admin_port'],
                lan_client=leased_router['lan_client'],
                ssh_port=leased_router['ssh_port'])
        with requests_mock.mock() as m:
            url = self.server + 'routers/r1/lease/'
            m.delete(url, json=self.wifi_router_json, status_code=204)
            self.arm.session.delete(url)
            self.assertIsNone(self.arm.release_router(arm_router))

    def test_release_wifi_router(self):
        leased_router = self.leased_wifi_router_json['data'][0]
        with requests_mock.mock() as m:
            url = self.server + 'wifi_routers/wr1/lease/'
            m.delete(url, json=self.wifi_router_json, status_code=204)
            m.get('/api/status/fw_info', json={'success': True}, status_code=200)
            m.put('/api/config/system/logging', json={'success': True}, status_code=200)
            m.put('/api/control/log', json={'success': True, 'clear': True}, status_code=200)
            arm_router = ARMWifiRouter(
                manager=self.arm,
                model=leased_router['model'],
                router_id=leased_router['id'],
                root_pass=leased_router['root_pass'],
                remote_admin_host=leased_router['remote_admin_host'],
                remote_admin_port=leased_router['remote_admin_port'],
                remote_admin_user=leased_router['remote_admin_user'],
                remote_admin_pass=leased_router['remote_admin_pass'],
                ssh_host=leased_router['ssh_host'],
                ssh_port=leased_router['ssh_port'])
            self.arm.session.delete(url)
            self.assertIsNone(self.arm.release_wifi_router(arm_router))

    def test_lease_router(self):
        with requests_mock.mock() as m:
            url = self.server + 'wifi_routers/wr1/lease/'
            m.put(url, json=self.wifi_router_json, status_code=200)
            self.arm.get_arm_router_list = MagicMock(return_value=self.leased_wifi_router_json['data'])
            lease = self.arm.lease_router(pool=DevicePool.WIFI_ROUTERS)
            self.assertEqual(lease[0], self.wifi_router_json['data'][0])

    def test_get_wifi_client(self):
        arm_client = ARMWifiClient(
            manager=self.arm,
            connection_config={
                "device_id": "id",
                "ssh_username": "name",
                "ssh_password": "pass",
                "ssh_host": "foo.bar.com",
                "ssh_port": 22,
                "model": "Galaxy_S7"
            },
            client_id="Galaxy_S7_1",
            client_type="android")
        client = ARMWiFiClient()
        client.id = 'Galaxy_S7_1'
        client.type = 'android'
        client_list = [client]
        self.arm.client.wifi_clients.get_many = MagicMock(return_value=client_list)
        self.arm.lease_wifi_client = MagicMock(return_value=self.arm_leased_wifi_client)
        wifi_client = self.arm.get_wifi_client()
        self.assertIsInstance(wifi_client, ARMWifiClient)
        self.assertDictEqual(arm_client.connection_config, wifi_client.connection_config)

    def test_lease_wifi_client(self):
        client = ARMWiFiClient()
        client.id = 'Galaxy_S7_1'
        client.type = 'android'
        self.arm.client.wifi_clients.create_lease = MagicMock(return_value=self.arm_leased_wifi_client)
        lease = self.arm.lease_wifi_client(client, 'reason')
        self.assertIsInstance(lease, ARMLeasedWifiClient)
        self.assertDictEqual(self.arm_leased_wifi_client.lease, lease.lease)

    def test_get_available_wifi_clients_list(self):
        client = ARMWiFiClient()
        client.id = 'Galaxy_S7_1'
        client.type = 'android'
        client_list = [client]
        self.arm.client.wifi_clients.get_many = MagicMock(return_value=client_list)
        wifi_client_list = self.arm.get_available_wifi_clients_list()
        self.assertListEqual(client_list, wifi_client_list)

    def test_release_client(self):
        arm_client = ARMWifiClient(
            manager=self.arm,
            connection_config={
                "device_id": "id",
                "ssh_username": "name",
                "ssh_password": "pass",
                "ssh_host": "foo.bar.com",
                "ssh_port": 22,
                "model": "Galaxy_S7"
            },
            client_id="Galaxy_S7_1",
            client_type="android")
        self.arm.client.wifi_clients.delete_lease = MagicMock
        self.assertIsNone(self.arm.release_wifi_client(arm_client))

    def test_release_all_clients(self):
        self.arm.release_wifi_client = MagicMock
        self.assertIsNone(self.arm.release_all_wifi_clients())
