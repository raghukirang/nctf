import unittest
from unittest.mock import MagicMock

from nctf.test_lib.ncos.fixtures.wifi_client.connectors import AndroidWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors import IOSWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors import LinuxWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.connectors import OSXWiFiClient
from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture
from nctf.test_lib.ncos.fixtures.wifi_client.fixture import NoWiFiClientsAvailableError


class WiFiClientFixtureTestCase(unittest.TestCase):
    def setUp(self):
        pass


class TestLinuxWiFiClient(WiFiClientFixtureTestCase):
    def setUp(self):
        super().setUp()

    def test_parse_scan(self):
        stdout = [
            "AER3100-18f-5g:00\:30\:44\:1D\:81\:91:2:WPA2:36",
            "CP-CORP:04\:BD\:88\:73\:5B\:20:32:WPA2 802.1X:11"
        ]
        scan_results = LinuxWiFiClient._parse_scan(stdout)
        self.assertEqual(scan_results[0].ssid, 'AER3100-18f-5g')
        self.assertEqual(scan_results[0].bssid, '00:30:44:1D:81:91')
        self.assertEqual(scan_results[0].security, 'WPA2')
        self.assertEqual(scan_results[0].rssi, '2')
        self.assertEqual(scan_results[0].channel, 36)
        self.assertEqual(scan_results[1].ssid, 'CP-CORP')
        self.assertEqual(scan_results[1].bssid, '04:BD:88:73:5B:20')
        self.assertEqual(scan_results[1].security, 'WPA2 802.1X')
        self.assertEqual(scan_results[1].rssi, '32')
        self.assertEqual(scan_results[1].channel, 11)


class TestOSXWiFiClient(WiFiClientFixtureTestCase):
    def setUp(self):
        super().setUp()

    def test_parse_status(self):
        stdout = [
            "agrCtlRSSI: -49", "agrExtRSSI: 0", "agrCtlNoise: -84", "agrExtNoise: 0", "state: running",
            "op mode: station ", "lastTxRate: 867", "maxRate: 1300", "lastAssocStatus: 0", "802.11 auth: open",
            "link auth: wpa2-psk", "BSSID: 4:bd:88:73:5b:31", "SSID: CP-Guest", "MCS: 9", "channel: 36,80"
        ]
        status = OSXWiFiClient._parse_status(stdout)
        self.assertEqual(status.ssid, 'CP-Guest')
        self.assertEqual(status.bssid, '4:bd:88:73:5b:31')
        self.assertEqual(status.security, 'wpa2-psk')
        self.assertEqual(status.wifi_state, 'running')

    def test_parse_scan(self):
        stdout = [
            "SSID BSSID             RSSI CHANNEL HT CC SECURITY (auth/unicast/group)\n",
            "             MBR1400-d47 00:30:44:16:8d:47 -58  1       Y  -- WPA(PSK/AES/AES) WPA2(PSK/AES/AES)\n",
            "             2100-Feb23 00:30:44:19:23:8e -41  1       Y  -- WPA(PSK/AES/AES) WPA2(PSK/AES/AES)\n",
            "               lampe 00:30:44:1e:c3:f3 -53  4       Y  -- WPA2(PSK/AES/AES)\n"
        ]
        scan = OSXWiFiClient._parse_scan(stdout)
        self.assertEqual(scan[0].ssid, 'MBR1400-d47')
        self.assertEqual(scan[2].security, 'WPA2(PSK/AES/AES)')


class TestAndroidWiFiClient(WiFiClientFixtureTestCase):
    def setUp(self):
        super().setUp()

    def test_parse_status(self):
        stdout = [
            "ACTION:wifi_status:BEGINRESULT:ssid\t'\"CP-Guest\"'\tbssid\t'04:bd:88:73:5b:31'\trssi\t'-56'\tstate\t'OBTAINING_IPADDR'\twifi_status\t'1'\twifi_connection_state\t'CONNECTED':ENDRESULT"
        ]
        status = AndroidWiFiClient._parse_status(stdout)
        self.assertEqual(status.ssid, "CP-Guest")
        self.assertEqual(status.bssid, "04:bd:88:73:5b:31")
        self.assertEqual(status.wifi_state, "1")
        self.assertEqual(status.ssid_state, "CONNECTED")

    def test_parse_scan(self):
        stdout = [
            "ACTION:wifi_scan:BEGINRESULT:\n",
            "ssid\t'2100-14d'\tbssid\t'00:30:44:1a:61:4e'\trssi\t'-56'\tcapabilities\t'[WPA-PSK-CCMP][WPA2-PSK-CCMP][ESS]'\n",
            "ssid\t'AER1600-0f8'\tbssid\t'00:30:44:20:50:f9'\trssi\t'-57'\tcapabilities\t'[WPA2-PSK-CCMP][ESS]'\n",
            "ssid\t'CP-Guest'\tbssid\t'04:bd:88:73:5b:21'\trssi\t'-57'\tcapabilities\t'[WPA2-PSK-CCMP][ESS]'\n",
            "ssid\t'CP-CORP'\tbssid\t'04:bd:88:73:5b:20'\trssi\t'-57'\tcapabilities\t'[WPA2-EAP-CCMP][ESS]'\n",
            ":ENDRESULT\n"
        ]
        scan_results = AndroidWiFiClient._parse_scan(stdout)
        self.assertEqual(scan_results[0].ssid, '2100-14d')
        self.assertEqual(scan_results[1].bssid, '00:30:44:20:50:f9')
        self.assertEqual(scan_results[2].rssi, '-57')
