import json
import os
from unittest import TestCase
from unittest.mock import MagicMock

from nctf.test_lib.core.email.mailinator import MailinatorInbox

MOCKS_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'mocks')


class EmailInboxUnitTestCase(TestCase):
    def setUp(self):
        super().setUp()
        with open(os.path.join(MOCKS_PATH, 'mailinator', 'mock_email.json'), encoding='utf-8') as f:
            self.mock_email_json = json.load(f)
        with open(os.path.join(MOCKS_PATH, 'mailinator', 'mock_message_1.json'), encoding='utf-8') as f:
            self.mock_message_1_json = json.load(f)
        with open(os.path.join(MOCKS_PATH, 'mailinator', 'mock_message_2.json'), encoding='utf-8') as f:
            self.mock_message_2_json = json.load(f)
        with open(os.path.join(MOCKS_PATH, 'mailinator', 'mock_message_3.json'), encoding='utf-8') as f:
            self.mock_message_3_json = json.load(f)
        with open(os.path.join(MOCKS_PATH, 'mailinator', 'mock_message_4.json'), encoding='utf-8') as f:
            self.mock_message_4_json = json.load(f)


class TestMailinatorInbox(EmailInboxUnitTestCase):
    def setUp(self):
        super().setUp()

        mock_email_response = MagicMock()
        mock_email_response.json = MagicMock(return_value=self.mock_email_json)

        inbox_endpoint_mock = MagicMock(return_value=mock_email_response)

        mock_message_1_response = MagicMock()
        mock_message_1_response.json = MagicMock(return_value=self.mock_message_1_json)
        mock_message_2_response = MagicMock()
        mock_message_2_response.json = MagicMock(return_value=self.mock_message_2_json)
        mock_message_3_response = MagicMock()
        mock_message_3_response.json = MagicMock(return_value=self.mock_message_3_json)
        mock_message_4_response = MagicMock()
        mock_message_4_response.json = MagicMock(return_value=self.mock_message_4_json)

        email_endpoint_mock = MagicMock(
            side_effect=[mock_message_1_response, mock_message_2_response, mock_message_3_response, mock_message_4_response])
        self.inbox = MailinatorInbox(name="baz", api_root="https://foobar.baz", token="foo_token")
        self.inbox.client.email.get = email_endpoint_mock
        self.inbox.client.inbox.get = inbox_endpoint_mock

    def test_get_messages(self):
        messages = self.inbox.get_messages()
        self.assertEquals(len(messages), 4)

    def test_get_messages_by_subject(self):
        messages = self.inbox.get_messages_by_subject("Our first radioinfo email alert for 2018")
        self.assertEquals(len(messages), 1)

    def test_get_messages_by_sender(self):
        messages = self.inbox.get_messages_by_sender("blackoak@email.promos.play4funnetwork.com")
        self.assertEquals(len(messages), 1)

    def test_get_messages_by_sender_and_subject(self):
        messages = self.inbox.get_messages_by_sender_and_subject("info@twitter.com",
                                                                 "Follow National Post, CTV News and NHL on Twitter!")
        self.assertEquals(len(messages), 1)
