from unittest import TestCase

from nctf.test_lib.core.email import EmailFixture
from nctf.test_lib.core.email import EmailService
from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.core.email.mailinator import MailinatorInbox


class EmailFixtureUnitTestCase(TestCase):
    def setUp(self):
        super().setUp()


class TestEmailFixture(EmailFixtureUnitTestCase):
    def test_get_inbox_random(self):
        fixture = EmailFixture(EmailService.MAILINATOR, "https://foobar.baz", mailinator_token="foo")
        inbox = fixture.get_inbox()
        self.assertIsNotNone(inbox.name)

    def test_get_inbox_mailinator(self):
        fixture = EmailFixture(EmailService.MAILINATOR, "https://foobar.baz", mailinator_token="foo")
        inbox = fixture.get_inbox()
        self.assertIsInstance(inbox, MailinatorInbox)

    def test_get_inbox_mailhog(self):
        fixture = EmailFixture(EmailService.MAILHOG, "https://foobar.baz")
        inbox = fixture.get_inbox()
        self.assertIsInstance(inbox, MailhogInbox)

    def test_get_inbox_no_mailinator_token(self):
        with self.assertRaises(ValueError):
            EmailFixture(EmailService.MAILINATOR, "https://foobar.baz")
