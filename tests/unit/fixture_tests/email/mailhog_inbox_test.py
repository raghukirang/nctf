import json
import os
from unittest import TestCase
from unittest.mock import MagicMock

from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.core.email.mailhog import SearchEndpoint

MOCKS_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'mocks')


class EmailInboxUnitTestCase(TestCase):
    def setUp(self):
        super().setUp()


class TestMailhogInbox(EmailInboxUnitTestCase):
    def setUp(self):
        super().setUp()
        with open(os.path.join(MOCKS_PATH, 'mailhog', 'mock_search.json')) as f:
            self.mock_search_json = json.load(f)

        mock_search_response = MagicMock()
        mock_search_response.json = MagicMock(return_value=self.mock_search_json)

        SearchEndpoint.get = MagicMock(return_value=mock_search_response)
        self.inbox = MailhogInbox(name="jfw", api_root="https://foobar.baz")

    def test_get_messages(self):
        messages = self.inbox.get_messages()
        self.assertEquals(len(messages), 2)

    def test_get_messages_by_subject(self):
        messages = self.inbox.get_messages_by_subject(subject="test_get_messages subject bar")
        self.assertEquals(len(messages), 1)

    def test_get_messages_by_sender(self):
        messages = self.inbox.get_messages_by_sender(sender="test_get_messages_bar@mailhog.com")
        self.assertEquals(len(messages), 1)

    def test_get_messages_by_sender_and_subject(self):
        messages = self.inbox.get_messages_by_sender_and_subject(
            sender="test_get_messages_foo@mailhog.com", subject="test_get_messages subject foo")
        self.assertEquals(len(messages), 1)
