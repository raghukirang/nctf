import unittest

import requests_mock

from nctf.test_lib.ncos.fixtures.router_api.fixture import ConfigureEndpoint
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI


class RouterRESTAPITestCase(unittest.TestCase):
    def setUp(self):
        self.rest_api = RouterRESTAPI(api_root="http://foo.bar.router")
        with requests_mock.mock() as m:
            m.register_uri('GET', '/api/status/fw_info', json={"data": {"foo": "bar"}, "success": True})
            self.rest_api.authenticate(user_name="admin", password="quickbrownfox")


class TestRouterRESTAPI(RouterRESTAPITestCase):
    def setUp(self):

        super().setUp()
        self.endpoint = ConfigureEndpoint(self.rest_api, "api/path/to/the")

    def test_get_name(self):
        with requests_mock.mock() as m:
            m.register_uri(
                'GET', 'http://foo.bar.router:80/api/path/to/the/thing', json={
                    "data": {
                        "foo": "bar"
                    },
                    "success": True
                })
            response = self.endpoint.get(name="thing")
            self.assertEqual(response["data"], {"foo": "bar"})

    def test_get_no_name(self):
        with requests_mock.mock() as m:
            m.register_uri('GET', 'http://foo.bar.router:80/api/path/to/the', json={"data": {"foo": "bar"}, "success": True})
            response = self.endpoint.get()
            self.assertEqual(response["data"], {"foo": "bar"})

    def test_update(self):
        with requests_mock.mock() as m:
            m.register_uri('PUT', 'http://foo.bar.router:80/api/path/to/the', json={"data": {"foo": "bar"}, "success": True})
            response = self.endpoint.update(update={"foo": "bar"})
            self.assertEqual(response["data"], {"foo": "bar"})
