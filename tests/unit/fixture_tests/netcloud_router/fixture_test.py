import time
import unittest
from unittest.mock import MagicMock
from unittest.mock import patch

from nctf.test_lib.core.arm4.fixture import ARMv4Fixture
from nctf.test_lib.core.arm4.fixture import ARMv4Router
from nctf.test_lib.ncm.rest import NCMv1RESTClient
from nctf.test_lib.netcloud.fixtures import netcloud_router as NCR
from nctf.test_lib.netcloud.fixtures.netcloud_router.router import CreationMethod


class TestNetCloudRouterFixture(unittest.TestCase):
    def setUp(self):
        # Mocks
        time.sleep = MagicMock()  # Disable sleep since we have a hard wait for email currently.
        router = ARMv4Router(router_data=MagicMock())
        router.get_rest_client = MagicMock()
        router.admin_user = 'mock_admin_user'
        router.admin_pass = 'mock_admin_pass'
        router.remote_admin_host = 'mock_admin_host'
        router.remote_admin_ssh_port = 321
        router.remote_admin_port = 123
        config_fixture = MagicMock()
        request = MagicMock()
        request.node = MagicMock()
        request.node.name = "unit_test_netcloud_router_fixture"
        ARMv4Fixture.lease_router = MagicMock(return_value=router)
        self.ncr_fixture = NCR.NetCloudRouterFixture(
            config_fixture=config_fixture,
            creation_method=CreationMethod.ARMV4,
            request=request,
            arm4_fixture=ARMv4Fixture(request.node.name, 'foo', 'bar123'))

    def validate_router(self, router):
        self.assertIsNotNone(router.ncos)
        self.assertIsNone(router.ncm)
        self.assertEqual(router.creation_method, NCR.CreationMethod.ARMV4)

    def test_ncr_fixture_create(self):
        # VIRTNETWORK is not implemented
        self.ncr_fixture.creation_method = NCR.CreationMethod.VIRTNETWORK
        with self.assertRaises(NotImplementedError):
            new_router = self.ncr_fixture.create()

        # LOCAL is not implemented
        self.ncr_fixture.creation_method = NCR.CreationMethod.LOCAL
        with self.assertRaises(NotImplementedError):
            new_router = self.ncr_fixture.create()

        self.ncr_fixture.creation_method = NCR.CreationMethod.ARMV4

        new_router = self.ncr_fixture.create()
        self.validate_router(new_router)

    def test_no_arm_fixture(self):
        with self.assertRaises(ValueError):
            NCR.NetCloudRouterFixture(
                config_fixture=self.ncr_fixture.config_fixture,
                creation_method=NCR.CreationMethod.ARMV4,
                request=self.ncr_fixture.request)

    def test_register(self):
        new_router = self.ncr_fixture.create()
        self.validate_router(new_router)
        rest_client = NCMv1RESTClient('https', 'foo.com', 443)

        new_router.ncos.set_stream_server = MagicMock()
        new_router.ncos.register = MagicMock()
        new_router.ncos.get_value = MagicMock()
        new_router.ncos.get_value.return_value = 'foo.com'

        new_router.register(stream_host="foo.com", ncm_username='foo', ncm_password='bar123', ncm_rest_client=rest_client)
        self.assertIsNotNone(new_router.ncm)
