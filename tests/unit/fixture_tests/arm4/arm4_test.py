from unittest import TestCase
from unittest.mock import patch
from unittest.mock import MagicMock
from unittest.mock import create_autospec

from arm4.serialization import Router as RouterDataObj
from arm4.serialization import RouterControl as RouterCtrlDataObj
from arm4.serialization import RouterCapability
import paramiko

from nctf.test_lib.core.arm4.fixture import ARMv4LanClient
from nctf.test_lib.core.arm4.fixture import ARMv4Router
from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPI


class ARMv4UnitTestCase(TestCase):
    def setUp(self):
        super().setUp()

    @staticmethod
    def get_mock_router_control_data_obj():
        data = RouterCtrlDataObj()
        data.ssh_host = "mock_ssh_host"
        data.ssh_port = "mock_ssh_port"
        data.ssh_user = "mock_ssh_user"
        data.ssh_pass = "mock_ssh_pass"
        return data

    @staticmethod
    def get_mock_router_data_obj():
        data = RouterDataObj()
        data.admin_user = 'mock_admin_user'
        data.admin_pass = 'mock_admin_pass'
        data.remote_admin_host = 'mock_admin_host'
        data.remote_admin_ssh_port = 321
        data.remote_admin_port = 123

        data.capability = RouterCapability()
        data.capability_id = "LookImARouter"
        data.capability.id = data.capability_id
        return data


class TestARMv4Fixture(ARMv4UnitTestCase):
    def test_1(self):
        pass


class TestARMv4Router(ARMv4UnitTestCase):
    @patch('nctf.test_lib.ncos.base.router.SSHClient')
    def test_get_ssh_client(self, mock_ssh):
        router = ARMv4Router(self.get_mock_router_data_obj())
        mocked_client = MagicMock()
        mock_ssh.return_value = mocked_client
        mocked_client.connect = MagicMock()
        router.get_ssh_client()

    def test_get_rest_client(self):
        router = ARMv4Router(self.get_mock_router_data_obj())

        # Mock the authenticate method of the RouterRESTAPI since it is called by "get_rest_client"
        with patch.object(RouterRESTAPI, 'authenticate', return_value=None) as mock_auth:
            api = router.get_rest_client()

            assert type(api) is RouterRESTAPI
            mock_auth.assert_called_once_with('mock_admin_user', 'mock_admin_pass')

    @patch('nctf.test_lib.utils.sshclient.sshclient_paramiko.SSHShellClient.do_complex_cmd')
    @patch('nctf.test_lib.utils.sshclient.sshclient_paramiko.SSHShellClient.connect')
    def test_exec_command(self, mock_connect, mock_exec_command):
        mock_connect.return_value = None
        mock_exec_command.return_value = ["now with 0% packet loss!!!"]

        router = ARMv4Router(self.get_mock_router_data_obj())

        # validate a single command
        out = router.exec_command('ping -c 1 www.google.com')
        assert " 0% packet loss" in out

        mock_exec_command.side_effect = [["first result", "second result", "third result"]]

        # validate a command sequence
        out = router.exec_command(command=["one", "two", "three"])
        assert "first result" in out
        assert "second result" in out
        assert "third result" in out


class TestARMv4LANClient(ARMv4UnitTestCase):
    """Currently the only LANClient delivered from ARMv4 is the RouterControl"""

    @patch('nctf.test_lib.ncos.base.client.SSHClient')
    def test_get_ssh_client(self, mock_ssh):
        mocked_client = MagicMock()
        mock_ssh.return_value = mocked_client
        mocked_client.connect = MagicMock()
        lan_client = ARMv4LanClient(self.get_mock_router_control_data_obj())

        # Mock the connect method of the SSHClient since it is called by "get_ssh_client"
        lan_client.get_ssh_client()

        mocked_client.connect.assert_called_once_with(
            username='mock_ssh_user',
            password='mock_ssh_pass',
            hostname='mock_ssh_host',
            port='mock_ssh_port',
            allow_agent=False,
            look_for_keys=False)

    @patch('nctf.test_lib.utils.sshclient.sshclient_paramiko.SSHShellClient.do_complex_cmd')
    def test_exec_command(self, mock_exec_command):
        mock_exec_command.return_value = ["mock_output"]

        lan_client = ARMv4LanClient(self.get_mock_router_control_data_obj())

        # single command
        r = lan_client.exec_command('ping -c 1 www.google.com')

        mock_exec_command.assert_called_once()
        self.assertEqual(r, 'mock_output')

        mock_exec_command.side_effect = [["first", "second", "third"]]
        # command sequence
        r = lan_client.exec_command(['one', 'two', 'three'])
        assert "first" in r
        assert "second" in r
        assert "third" in r

    @staticmethod
    def exec_has_command(mock: MagicMock, command: str) -> bool:
        for call in mock.mock_calls:
            for param in call:
                if "cmd" in param:
                    for cmd in param["cmd"]:
                        if command in cmd:
                            return True
        return False

    @patch('nctf.test_lib.utils.sshclient.sshclient_paramiko.SSHShellClient.do_complex_cmd')
    def test_router_exec_command(self, mock_exec_command):
        mock_exec_command.return_value = ["mock_output"]

        lan_client = ARMv4LanClient(self.get_mock_router_control_data_obj())
        router = ARMv4Router(self.get_mock_router_data_obj())

        # case 1: basic run a command using the lan client, direct, no namespace
        lan_client.linux_namespace = ""
        r = lan_client.router_exec_command(command='ping -c 1 www.google.com', router=router)

        mock_exec_command.assert_called_once()
        self.assertEqual(r, 'mock_output')
        self.assertFalse(self.exec_has_command(mock_exec_command, "sudo ip netns"))
        self.assertTrue(self.exec_has_command(mock_exec_command, "ssh mock_admin_user@"))
        self.assertTrue(self.exec_has_command(mock_exec_command, "ping -c 1 www.google.com"))

        mock_exec_command.reset_mock()

        # case 2: multiple commands, this time with a namespace
        mock_exec_command.side_effect = [["first\r\nssh admin", "second", "third"]]
        lan_client.linux_namespace = "test_ns"

        r = lan_client.router_exec_command(command=['one', 'two', 'three'], router=router)
        self.assertTrue(self.exec_has_command(mock_exec_command, "sudo ip netns exec test_ns"))
        self.assertTrue(self.exec_has_command(mock_exec_command, "ssh mock_admin_user@"))
        self.assertTrue(self.exec_has_command(mock_exec_command, "one"))
        self.assertTrue(self.exec_has_command(mock_exec_command, "two"))
        self.assertTrue(self.exec_has_command(mock_exec_command, "three"))
        assert "first" in r
        assert "second" in r
        assert "third" in r


class TestARMv4WiFiClient(ARMv4UnitTestCase):
    def test_get_ssh_client(self):
        pass

    def test_exec_command(self):
        pass
