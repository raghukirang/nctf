import unittest

import pytest

from nctf.libs.salesforce.common_lib import BundleProducts
from nctf.libs.salesforce.common_lib import FeatureLists
from nctf.libs.salesforce.common_lib import ProductCodes
from nctf.libs.salesforce.common_lib import ProductInfo
from nctf.libs.salesforce.common_lib import Products


@pytest.mark.common
@pytest.mark.salesforce
class TestSalesforceProductData(unittest.TestCase):
    """Sanity-checks for locally-maintained Salesforce Product data representations."""

    def test_every_product_has_matching_product_code(self) -> None:
        for product_name in Products.__members__:
            self.assertTrue(product_name in ProductCodes.__members__,
                            msg="No matching ProductCodes entry for Product '{}'".format(product_name))

    def test_every_bundleproduct_has_matching_product_code(self) -> None:
        for product_name in BundleProducts.__members__:
            self.assertTrue(product_name in ProductCodes.__members__,
                            msg="No matching ProductCodes entry for Product '{}'".format(product_name))

    def test_every_product_code_has_matching_product_or_bundleproduct(self) -> None:
        for product_name in ProductCodes.__members__:
            self.assertTrue(product_name in Products.__members__ or product_name in BundleProducts.__members__,
                            msg="No matching Products entry for ProductCode '{}'".format(product_name))

    def test_no_overlap_in_products_vs_bundleproducts(self) -> None:
        for product_name in Products.__members__:
            self.assertTrue(product_name not in BundleProducts.__members__,
                            msg="Overlapping products in both Products and BundleProducts: '{}'".format(product_name))

    def test_product_name_has_same_product_code_name(self) -> None:
        for product_name in Products.__members__:
            product_code_name = getattr(Products, product_name).value.product_code.name
            self.assertEqual(product_code_name, product_name,
                             msg="Product name '{}' has conflicting ProductCode '{}'".format(product_name,
                                                                                             product_code_name))

    def test_bundleproduct_name_has_same_product_code_name(self) -> None:
        for product_name in BundleProducts.__members__:
            product_code_name = getattr(BundleProducts, product_name).value.product_code.name
            self.assertEqual(product_code_name, product_name,
                             msg="BundleProduct name '{}' has conflicting ProductCode '{}'".format(product_name,
                                                                                                   product_code_name))

    def test_products_structure(self) -> None:
        for product in Products:
            self.assertIsInstance(product, Products)
            self.assertIsInstance(product.value, ProductInfo)
            self.assertIsInstance(product.value.product_code, ProductCodes)
            self.assertIsInstance(product.value.feature_list, FeatureLists)
            self.assertIsInstance(product.value.feature_list.value.feature_list.value, list)

    def test_products_contain_features(self) -> None:
        for product in Products:
            self.assertGreaterEqual(len(product.value.feature_list.value), 1,
                                    msg="Product '{}' does not contain features!")
