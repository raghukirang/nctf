import unittest
import uuid

import pytest

from nctf.libs.salesforce.common_lib import FeatureListInfo
from nctf.libs.salesforce.common_lib import FeatureLists
from nctf.libs.salesforce.common_lib import FeatureUUIDs
from nctf.libs.salesforce.common_lib import Products
from nctf.libs.salesforce.common_lib import ProductFeatures


@pytest.mark.common
@pytest.mark.salesforce
class TestSalesforceFeatureData(unittest.TestCase):
    """Sanity-checks for locally-maintained Salesforce Feature and Feature List data representations."""

    def test_featurelists_structure(self) -> None:
        for feat_list in FeatureLists:
            self.assertTrue(isinstance(feat_list, FeatureLists))
            self.assertTrue(isinstance(feat_list.value, FeatureListInfo))
            self.assertTrue(isinstance(feat_list.value.feature_list, ProductFeatures))
            self.assertTrue(isinstance(feat_list.value.feature_list.value, list))
            self.assertTrue(isinstance(feat_list.value.list_name, str))

    def test_lists_have_features(self) -> None:
        for product in Products:
            self.assertGreaterEqual(len(product.value.feature_list.value), 1,
                                    msg="Product '{}' does not contain features!")

    def test_feature_uuids_are_well_formed(self) -> None:
        for feature in FeatureUUIDs:
            uuid.UUID(feature.value, version=4)  # Will raise if cannot be parsed
            self.assertTrue(feature.value.islower(), msg="UUID must be lowercase: '{}'".format(feature.value))

    def test_every_featurelist_has_matching_productfeatures(self) -> None:
        for feat_list in FeatureLists.__members__:
            self.assertTrue(feat_list in ProductFeatures.__members__,
                            msg="No matching ProductFeatures entry for FeatureList '{}'".format(feat_list))

    def test_every_productfeatures_has_matching_featurelist(self) -> None:
        for feat_list in ProductFeatures.__members__:
            self.assertTrue(feat_list in FeatureLists.__members__,
                            msg="No matching FeatureList entry for ProductFeatures '{}'".format(feat_list))

