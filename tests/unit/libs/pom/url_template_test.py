import unittest
from unittest.mock import MagicMock

from nctf.test_lib.base import pages


class POMURLTemplateTest(unittest.TestCase):
    def setUp(self):
        self.mock_driver = MagicMock(spec=pages.UIWebDriver)
        self.base_url = 'http://mock.mock:80'

    def test_plain_url_on_this(self):
        self.mock_driver.current_url = 'http://mock.mock:80'
        test_page = pages.UIPage(self.mock_driver, 'TestPage', self.base_url)
        self.assertTrue(test_page.is_on_this())

        self.mock_driver.current_url = 'http://mock.mock'
        self.assertTrue(test_page.is_on_this())

        self.mock_driver.current_url = 'http://mock.mock/'
        self.assertTrue(test_page.is_on_this())

    def test_relative_mid_parameter_positive_match(self):
        class TestPage(pages.UIPage):
            URL_TEMPLATE = '/network/{network_id:d}/settings'

        test_page = TestPage(self.mock_driver, 'TestPage', self.base_url)
        self.mock_driver.current_url = 'http://mock.mock:80/network/1234/settings'
        self.assertTrue(test_page.is_on_this())

    def test_relative_mid_parameter_negative_matches(self):
        class TestPage(pages.UIPage):
            URL_TEMPLATE = '/network/{network_id:d}/settings'

        test_page = TestPage(self.mock_driver, 'TestPage', self.base_url)

        # Try base
        self.mock_driver.current_url = 'http://mock.mock:80'
        self.assertFalse(test_page.is_on_this())

        # Try obviously wrong
        self.mock_driver.current_url = 'http://mock.mock:80/foo/bar/baz'
        self.assertFalse(test_page.is_on_this())

        # Try alpha (expecting numeric)
        self.mock_driver.current_url = 'http://mock.mock:80/network/foo/settings'
        self.assertFalse(test_page.is_on_this())

        # Try alphanumeric (expecting numeric)
        self.mock_driver.current_url = 'http://mock.mock:80/network/1foo3/settings'
        self.assertFalse(test_page.is_on_this())

        # Try missing param section
        self.mock_driver.current_url = 'http://mock.mock:80/network//settings'
        self.assertFalse(test_page.is_on_this())

        # Try truncated
        self.mock_driver.current_url = 'http://mock.mock:80/network/123/setting'
        self.assertFalse(test_page.is_on_this())

        # Try extra
        self.mock_driver.current_url = 'http://mock.mock:80/network/123/settings2'
        self.assertFalse(test_page.is_on_this())

    def test_relative_trailing_parameter_positive_match(self):
        class TestPage(pages.UIPage):
            URL_TEMPLATE = '/network/{network_id:d}'

        test_page = TestPage(self.mock_driver, 'TestPage', self.base_url)
        self.mock_driver.current_url = 'http://mock.mock:80/network/1234'
        self.assertTrue(test_page.is_on_this())

    def test_relative_trailing_parameter_negative_matches(self):
        class TestPage(pages.UIPage):
            URL_TEMPLATE = '/network/{network_id:d}'

        test_page = TestPage(self.mock_driver, 'TestPage', self.base_url)

        # Try base
        self.mock_driver.current_url = 'http://mock.mock:80'
        self.assertFalse(test_page.is_on_this())

        # Try obviously wrong
        self.mock_driver.current_url = 'http://mock.mock:80/foo/bar/baz'
        self.assertFalse(test_page.is_on_this())

        # Try alpha (expecting numeric)
        self.mock_driver.current_url = 'http://mock.mock:80/network/foo'
        self.assertFalse(test_page.is_on_this())

        # Try alphanumeric (expecting numeric)
        self.mock_driver.current_url = 'http://mock.mock:80/network/1foo3'
        self.assertFalse(test_page.is_on_this())

        # Try missing param section
        self.mock_driver.current_url = 'http://mock.mock:80/network/'
        self.assertFalse(test_page.is_on_this())

        # Try extra
        self.mock_driver.current_url = 'http://mock.mock:80/network/123/settings'
        self.assertFalse(test_page.is_on_this())

    def test_mixed_kwarg_spec(self):
        class TestPage(pages.UIPage):
            URL_TEMPLATE = '/first/{first}/second/{second}'

        self.mock_driver.get = MagicMock()
        test_page = TestPage(self.mock_driver, 'TestPage', self.base_url, first='foo')
        with self.assertRaises(KeyError) as ke:
            test_page.open()
        self.assertIn('first', str(ke.exception))
        self.assertIn('foo', str(ke.exception))
        self.assertNotIn('second', str(ke.exception))

        self.mock_driver.current_url = 'http://mock.mock/first/foo/second/bar'
        test_page.open(second='bar')
        self.assertEqual(test_page.page_url, 'http://mock.mock:80/first/foo/second/bar')
        self.assertTrue(test_page.is_on_this())