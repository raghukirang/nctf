import unittest

import requests_mock

from nctf.test_lib.ncp.rest.legacy.rest_endpoint import RESTAPIEndpoint
from nctf.test_lib.ncp.rest.legacy.rest_interface import RESTAPIInterface


class RestEndpointTestCase(unittest.TestCase):
    def setUp(self):
        self.api_root = "http://mock.mock.mock"
        self.api = RESTAPIInterface(self.api_root, 30)

        self.one_data_template = {"data": {}, "success": None}

        self.many_data_template = {"meta": {}, "data": [], "success": None}


class TestRestEndpoint(RestEndpointTestCase):
    def test_handle_api_uri_slashes(self):
        endpoint = RESTAPIEndpoint(self.api, '/api/v1/mock')
        self.assertEquals(endpoint._api_uri, '/api/v1/mock')

        endpoint = RESTAPIEndpoint(self.api, 'api/v1/mock')
        self.assertEquals(endpoint._api_uri, '/api/v1/mock')

    def test_format_target_handle_resource_id(self):
        endpoint = RESTAPIEndpoint(self.api, '/api/v1/mock')
        target = endpoint._format_target('1')
        self.assertEquals(target, '/api/v1/mock/1')

        endpoint = RESTAPIEndpoint(self.api, '/api/v1/mock/')
        target = endpoint._format_target('1')
        self.assertEquals(target, '/api/v1/mock/1')

    def test_get(self):
        endpoint = RESTAPIEndpoint(self.api, 'api/v1/mock')
        with requests_mock.mock() as m:
            complete_uri = self.api_root + '/api/v1/mock'
            m.register_uri('GET', complete_uri)
            r = endpoint._get()
            self.assertEquals(r.url, complete_uri)

    def test_post(self):
        endpoint = RESTAPIEndpoint(self.api, 'api/v1/mock')
        with requests_mock.mock() as m:
            complete_uri = self.api_root + '/api/v1/mock'
            m.register_uri('POST', complete_uri)
            r = endpoint._post()
            self.assertEquals(r.url, complete_uri)

    def test_put(self):
        endpoint = RESTAPIEndpoint(self.api, 'api/v1/mock')
        with requests_mock.mock() as m:
            complete_uri = self.api_root + '/api/v1/mock'
            m.register_uri('PUT', complete_uri)
            r = endpoint._put()
            self.assertEquals(r.url, complete_uri)

    def test_patch(self):
        endpoint = RESTAPIEndpoint(self.api, 'api/v1/mock')
        with requests_mock.mock() as m:
            complete_uri = self.api_root + '/api/v1/mock'
            m.register_uri('PATCH', complete_uri)
            r = endpoint._patch()
            self.assertEquals(r.url, complete_uri)

    def test_delete(self):
        endpoint = RESTAPIEndpoint(self.api, 'api/v1/mock')
        with requests_mock.mock() as m:
            complete_uri = self.api_root + '/api/v1/mock'
            m.register_uri('DELETE', complete_uri)
            r = endpoint._delete()
            self.assertEquals(r.url, complete_uri)
