import unittest

import requests
import requests_mock

from nctf.test_lib.ncp.rest.legacy.exceptions import UnexpectedStatusError
from nctf.test_lib.ncp.rest.legacy.rest_interface import RESTAPIInterface


class RESTAPIInterfaceTestCase(unittest.TestCase):
    one_data = {"meta": {}, "data": {}}

    many_data = {"meta": {}, "data": []}

    def setUp(self):
        self.session = requests.Session()
        self.api_root = "https://mock.mock.mock"
        self.timeout = 30


class TestRESTAPIInterface(RESTAPIInterfaceTestCase):
    def test_default_session_state(self):
        api = RESTAPIInterface("http://mock.mock.mock", 30)
        self.assertIsInstance(api._session, requests.Session)
        self.assertEqual(api._session.headers, requests.Session().headers)
        self.assertEqual(api._session.cookies, requests.Session().cookies)
        self.assertEqual(api._session.auth, requests.Session().auth)

    def test_predefined_session_preserved(self):
        self.session.headers.update({"X-CUSTOM": "CustomValue"})
        api = RESTAPIInterface("http://mock.mock.mock", 30, self.session)
        self.assertEqual(api._session.headers["X-CUSTOM"], "CustomValue")
        self.assertIs(api._session, self.session)

    def test_raises_unexpected_status_code(self):
        api = RESTAPIInterface(self.api_root, self.timeout, self.session)
        complete_uri = self.api_root + '/api/v1/routers/'

        with requests_mock.mock() as m, self.assertRaises(UnexpectedStatusError):
            m.register_uri('GET', complete_uri, status_code=404)
            api.get('/api/v1/routers/', expected_status_code=200)

    def test_normalize_url(self):
        self.assertEqual(RESTAPIInterface.normalize_url("http://moc.com:80"), "http://moc.com")
        self.assertEqual(RESTAPIInterface.normalize_url("https://moc.com:443"), "https://moc.com")
        self.assertEqual(RESTAPIInterface.normalize_url("http://moc.com:443"), "http://moc.com:443")
        self.assertEqual(RESTAPIInterface.normalize_url("https://moc.com:80"), "https://moc.com:80")
        self.assertEqual(RESTAPIInterface.normalize_url("http://localhost:8080/"), "http://localhost:8080/")
        self.assertEqual(RESTAPIInterface.normalize_url("http://localhost:80/"), "http://localhost/")

    def test_base_url_normalized(self):
        api = RESTAPIInterface("http://moc.com:80", self.timeout)
        self.assertEqual(api.base_url, "http://moc.com")
