import unittest

import requests_mock

from nctf.test_lib.ncp.rest.legacy.overlay import OverlayAPIInterface
from nctf.test_lib.ncp.rest.legacy.overlay.endpoints import BasicOverlayEndpoint


class OverlayAPIInterfaceTestCase(unittest.TestCase):
    def setUp(self):
        self.one_data_template = {"meta": {}, "data": {}, "error": {}}

        self.many_data_template = {"meta": {}, "data": [], "error": {}}
        self.api_root = "https://mock.mock.mock"
        self.timeout = 30
        self.overlay_api = OverlayAPIInterface(self.api_root, self.timeout)


class TestOverlayAPIEndpoint(OverlayAPIInterfaceTestCase):
    def setUp(self):
        super().setUp()
        self.endpoint = BasicOverlayEndpoint(self.overlay_api, '/api/v1/mock')

    def test_detail(self):
        with requests_mock.mock() as m:
            uri = self.api_root + '/api/v1/mock/1'
            mock_json = self.one_data_template
            mock_json['data'] = {"foo": "bar"}
            m.register_uri('GET', uri, json=mock_json, status_code=200)
            self.endpoint.detail('1', expected_status_code=200)

    def test_list(self):
        with requests_mock.mock() as m:
            uri = self.api_root + '/api/v1/mock'
            mock_json = self.many_data_template
            mock_json['data'].append({"foo": "bar"})
            m.register_uri('GET', uri, json=mock_json, status_code=200)
            self.endpoint.list(expected_status_code=200)


class TestOverlayAPINetworksEndpoint(OverlayAPIInterfaceTestCase):
    pass
