import re
import unittest

from nctf.test_lib.ncm.fixtures.stream_client import StreamClientFixture


class StreamClientTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def check_mac(self, mac):
        if re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", mac.lower()):
            return True
        return False


class TestStreamClientInterface(StreamClientTestCase):
    def test_random_mac_address(self):
        for i in range(100):
            mac = StreamClientFixture.generate_mac_address()
            self.assertEquals(True, self.check_mac(mac))
