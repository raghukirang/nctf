import os
import time
from smtplib import SMTP
from unittest import TestCase

from nctf.test_lib.core.email.mailhog import MailhogInbox
from nctf.test_lib.core.email.mailhog import MailhogRESTClient


class MailHogIntegrationTest(TestCase):
    mailhog_protocol = "http"
    mailhog_hostname = os.environ.get("TEST_MAILHOG_HOSTNAME", "localhost")
    mailhog_smtp_port = 1025
    mailhog_api_port = 8025
    mailhog_api_root = "{}://{}:{}".format(mailhog_protocol, mailhog_hostname, mailhog_api_port)

    def send_mail(self, subject, body, from_address, to_addresses):
        message = 'Subject: {}\n\n{}'.format(subject, body)
        smtp = SMTP(host=self.mailhog_hostname, port=self.mailhog_smtp_port)
        smtp.sendmail(msg=message, from_addr=from_address, to_addrs=to_addresses)


class TestMailHogRESTClient(MailHogIntegrationTest):
    def setUp(self):
        super().setUp()
        now = time.time()
        self.inbox_name = str(now)
        self.rest_client = MailhogRESTClient(api_root=self.mailhog_api_root)

    def test_endpoint_search(self):
        """Test we can establish a connection and get something back. We don't care what."""
        r_json = self.rest_client.search.get(params={'kind': 'to', 'query': 'test_endpoint_search@jfw.com'})
        self.assertIsNotNone(r_json)


class TestMailHogInbox(MailHogIntegrationTest):
    def setUp(self):
        super().setUp()
        now = time.time()
        self.inbox_name = str(now)
        self.inbox = MailhogInbox(name=self.inbox_name, api_root=self.mailhog_api_root)

    def test_get_messages(self):
        """
        Mailhog has no concept of inboxes. We need to be sure that when we create a MailhogInbox and get_messages()
        we only get messages of the specified inbox name.

        """
        # This email we should NOT see in our inbox.
        self.send_mail(subject="test_get_messages subject",
                       body="test_get_messages body",
                       from_address="test_get_messages@jfw.com",
                       to_addresses=["{}@mailhog.comnot".format(self.inbox_name)])
        # This email we should see in our inbox.
        self.send_mail(subject="test_get_messages subject",
                       body="test_get_messages body",
                       from_address="test_get_messages@jfw.com",
                       to_addresses=["{}@mailhog.com".format(self.inbox_name)])
        # This email we should NOT see in our inbox.
        self.send_mail(subject="test_get_messages subject",
                       body="test_get_messages body",
                       from_address="test_get_messages@jfw.com",
                       to_addresses=["not{}@mailhog.com".format(self.inbox_name)])
        time.sleep(1)  # Give mailhog a brief amount of time to receive the email.

        messages = self.inbox.get_messages()
        self.assertEqual(len(messages), 1)
        message = messages[0]
        self.assertEqual(message.sender, "test_get_messages@jfw.com")
        self.assertEqual(message.subject, "test_get_messages subject")
        self.assertEqual(message.body, "test_get_messages body")
