from unittest import TestCase

from nctf.test_lib.core.email.mailinator import MailinatorInbox
from nctf.test_lib.core.email.mailinator import MailinatorRESTClient
"""
Sending emails to Mailinator is non-trivial and would require relaying and/or using something like Amazon SES.
For now we'll just stick to making sure we don't blow up when we make a request.
"""


class MailinatorIntegrationTest(TestCase):
    api_token = "f3b2ad0e54cb404b891c56e9d9499139"
    mailinator_api_root = "https://www.mailinator.com"


class TestMailinatorRESTClient(MailinatorIntegrationTest):
    def setUp(self):
        super().setUp()
        self.rest_client = MailinatorRESTClient(api_root=self.mailinator_api_root)

    def test_endpoint_email(self):
        """Test we can establish a connection and get something back. We don't care what."""
        self.rest_client.email.get(params={"id": "invalid_message_id", "token": self.api_token})

    def test_endpoint_inbox(self):
        """Test we can establish a connection and get something back. We don't care what."""
        self.rest_client.inbox.get(params={"to": "nctf", "token": self.api_token})


class TestMailinatorInbox(MailinatorIntegrationTest):
    def setUp(self):
        super().setUp()
        self.inbox = MailinatorInbox(name="baz", api_root=self.mailinator_api_root, token=self.api_token)

    def test_get_messages(self):
        """
        Test we can establish a connection and handle serialization of whatever we get back for Mailinator.
        On average this will use ~5 API calls since the 'baz' inbox typically has some messages in it.

        It would be optimal if we could send some test messages first, but that work is non-trivial for Mailinator
        because of their anti-spam email policies.

        """
        messages = self.inbox.get_messages()
        self.assertIsNotNone(messages)
