import parse
import pytest

import nctf.test_lib.base.pages as JWP
import nctf.test_lib.sample_website.pages as sample_pages

from .base_ui_test_case_base import BaseUITestCaseBase


class BaseUITestCase(BaseUITestCaseBase):
    """When running locally it is expected that 'docker-compose run -p 7080:5000 sample_site' has been run first. 
    Of course, all of the environment variables for secrets needs to be set up first"""

    ###################
    # The Tests:

    def test_journey_page_URLs(self):
        page = self.home_page
        assert page.wait_for_on_this()
        assert (page.driver.title == "The Internet")

        # TODO: Need to figure out how to do an absolute URL on docker when it depends on IPs and they change
        # #  test a UIPage that has an absolute URL
        # page = sample_pages.ABTestPage(driver=self.driver, name="ab test_page")
        # page.open()
        # assert page.wait_for_on_this()

    def test_journey_page_default_timeout(self):

        # no DEFAULT_TIMEOUT specified in SampleWebsiteIndexPage,
        # so actual page timeout should be the same as the UIPage DEFAULT_TIMEOUT
        assert sample_pages.SampleWebsiteIndexPage.DEFAULT_TIMEOUT == JWP.UIPage.DEFAULT_TIMEOUT

        page = self.home_page
        assert page.timeout == sample_pages.SampleWebsiteIndexPage.DEFAULT_TIMEOUT

        # construct the home page with a timeout different than the UIPage DEFAULT_TIMEOUT
        timeout = JWP.UIPage.DEFAULT_TIMEOUT + 7
        assert timeout != JWP.UIPage.DEFAULT_TIMEOUT

        page = sample_pages.SampleWebsiteIndexPage(
            driver=self.driver, name="Another HOME PAGE", base_url=self.sample_website_url, timeout=timeout).open()
        assert page.timeout == timeout

        # cause a construction of a page whose DEFAULT_TIMEOUT is different than the UIPage default
        assert sample_pages.SampleEditorPage.DEFAULT_TIMEOUT != JWP.UIPage.DEFAULT_TIMEOUT
        editor_page = page.content.editor_link.click()
        assert editor_page.timeout == sample_pages.SampleEditorPage.DEFAULT_TIMEOUT

        # construct the editor page with a timeout different than the editor page default and UIPage default
        timeout = JWP.UIPage.DEFAULT_TIMEOUT + sample_pages.SampleEditorPage.DEFAULT_TIMEOUT
        assert timeout != JWP.UIPage.DEFAULT_TIMEOUT
        assert timeout != sample_pages.SampleEditorPage.DEFAULT_TIMEOUT

        editor_page = sample_pages.SampleEditorPage(
            driver=self.driver, name="Another EDITOR PAGE", base_url=self.sample_website_url, timeout=timeout).open()

        assert editor_page.timeout == timeout

    def test_journey_region_on_page(self):
        page = self.home_page
        content_region = page.content
        assert content_region.heading.get_text() == 'Welcome to the-internet'
        assert content_region.title.get_text() == 'Available Examples'

    def test_journey_region_dynamic_construction_on_page(self):
        page = self.home_page

        # build a content region dynamically by passing in the already located element
        content_element = page.find_element(JWP.By.ID, 'content')
        content_dynamic = sample_pages.SampleWebsiteBasePage.ContentRegion(
            page, "content region dynamic", strategy=None, locator=None, root_element=content_element)
        assert content_dynamic.strategy is None
        assert content_dynamic.locator is None

        # validate aspects of the dynamically built region vs the default constructed one
        content_region = page.content
        assert content_dynamic._element_ == content_region._element_
        assert content_dynamic.get_text() == content_region.get_text()
        heading_element = content_dynamic.find_element(strategy=JWP.By.CLASS_NAME, locator="heading")
        assert heading_element == content_region.heading._element

    def test_journey_region_on_region_by_absolute_xpath(self):
        footer = self.home_page.footer
        assert footer.center.title.get_text() == 'Powered by Elemental Selenium'

    def test_journey_region_on_region_by_relative_xpath(self):
        footer = self.home_page.footer
        assert footer.center_by_relative_xpath.title.get_text() == 'Powered by Elemental Selenium'

    def test_journey_region_on_region_by_construction(self):
        footer = self.home_page.footer
        assert footer.center_by_construction.title.get_text() == 'Powered by Elemental Selenium'

    def test_journey_region_on_region_by_relative_xpath_construction(self):
        footer = self.home_page.footer
        assert footer.center_by_relative_xpath_construction.title.get_text() == 'Powered by Elemental Selenium'

    def test_jwe_on_page(self):
        login_page = self.home_page.content.login_link.click()
        assert login_page.login_button.get_text() == 'Login'

    def test_jwe_on_region(self):
        login_page = self.home_page.content.login_link.click()
        assert login_page.content.heading.get_text() == 'Login Page'
        assert login_page.content.heading_by_relative_xpath.get_text() == 'Login Page'

        # using different By to find the same element
        username = login_page.content.username
        assert username.is_clickable()
        assert username._element == login_page.content.username_by_id._element
        assert username._element == login_page.content.username_by_name._element
        assert login_page.flash_messages._element == login_page.flash_messages_by_class()._element

        link = login_page.footer.center.link
        element = link._element
        assert element == login_page.footer.center.link_by_full_link_text._element
        assert element == login_page.footer.center.link_by_partial_link_text._element
        assert element == login_page.footer.center.link_by_tag_name._element
        assert element == login_page.footer.center.link_by_xpath._element
        assert element != login_page.footer.center.link_by_page_rooted_xpath._element

        # by searching from the page using the tag name of the link, we expect it to find a different element
        assert element != login_page.find_element(JWP.By.TAG_NAME, login_page.footer.center.link_by_tag_name.locator)

        # by searching from the page using the xpath of the link, we expect it to find a different element
        assert element != login_page.find_element(JWP.By.XPATH, login_page.footer.center.link_by_xpath.locator)

        # using page rooted xpath attempt for link yielded the same element as finding it from the page
        assert login_page.footer.center.link_by_page_rooted_xpath._element == \
            login_page.find_element(JWP.By.XPATH, login_page.footer.center.link_by_xpath.locator)

    def test_can_click_link_on_page(self):
        home_page = self.home_page
        abpage = home_page.content.ab_testing_link.click()
        assert abpage.is_on_this()

    def test_all_JWE_actions_on_page(self):
        home_page = self.home_page
        login_page = home_page.content.login_link.click()
        login_button = login_page.login_button
        assert login_button.get_text() == 'Login'

        with pytest.raises(JWP.ActionNotPossibleError):
            login_button.send_keys("some text")

        element = login_button._element
        raw_element = login_button._debug_get_raw_web_element()
        assert element == raw_element
        assert login_button._element == login_button._debug_get_raw_web_element()

        # make sure that the helpers return the expected values got from the raw element esp. wrt. switching iframes

        is_selected = raw_element.is_selected()
        is_displayed = login_button._debug_get_raw_web_element().is_displayed()
        is_enabled = login_button._debug_get_raw_web_element().is_enabled()

        login_button.wait_for_displayed()
        login_button.wait_for_present()
        login_button.wait_for_clickable()

        assert login_button.is_selected() == is_selected
        assert login_button.is_displayed() == is_displayed
        assert login_button.is_clickable() == is_displayed & is_enabled
        assert login_button.is_present() == is_enabled

        login_button.get_attribute("id")

        login_button.click()
        assert "Your username is invalid" in login_page.flash_messages.get_text()

    def test_all_JWE_actions_on_region(self):
        login_page = self.home_page.content.login_link.click()
        username = login_page.content.username

        assert username._element == username._debug_get_raw_web_element()

        username.clear()
        username.send_keys("Darren")
        assert "Darren" in username.get_attribute("value")

        # make sure that the helpers return the expected values got from the raw element esp. wrt. switching iframes
        is_selected = username._debug_get_raw_web_element().is_selected()
        is_displayed = username._debug_get_raw_web_element().is_displayed()
        is_enabled = username._debug_get_raw_web_element().is_enabled()

        username.wait_for_displayed()
        username.wait_for_present()
        username.wait_for_clickable()

        assert username.is_selected() == is_selected
        assert username.is_displayed() == is_displayed
        assert username.is_clickable() == is_displayed & is_enabled
        assert username.is_present() == is_enabled

        username.click()

    def test_dropdown(self):
        home_page = self.home_page
        edit_page = home_page.content.editor_link.click()

        # makes the menu drop down
        edit_pull_down = edit_page.content.menu_bar.edit.click()

        # check the ability to select various menu items
        assert edit_pull_down.undo.is_clickable() is True
        assert edit_pull_down.undo.is_selected() is False
        assert edit_pull_down.undo.is_present() is True
        assert edit_pull_down.undo.is_displayed() is True
        assert edit_pull_down.copy.is_clickable() is True
        assert edit_pull_down.copy.is_selected() is False
        assert edit_pull_down.copy.is_present() is True
        assert edit_pull_down.copy.is_displayed() is True

        # drop up the menu, but do the click from the edit.
        edit_page.content.menu_bar.edit.click()

        edit_page.content.menu_bar.edit.click().copy.click()

        body = edit_page.content.menu_bar.file.click().new_document.click()
        assert body.get_text() == ''
        body.set_text("this is a test")

        # PyCharm should show autocomplete for body
        assert body.text_area.get_text() == "this is a test"
        body = edit_page.content.menu_bar.edit.click().undo.click()
        assert body.text_area.get_text() == ''

    def test_frame(self):
        edit_page = self.home_page.content.editor_link.click()
        assert edit_page.wait_for_on_this()

        # get the text area inside the body region that is an iframe
        content = edit_page.content
        body = content.body
        ta = body.text_area

        # validate access to the JWE via its interfaces
        assert 'Your content goes here.' in ta.get_text()

        ta.clear()
        assert ta.get_text() == ''

        ta.wait_for_displayed(3)
        ta.wait_for_clickable(3)
        ta.wait_for_present(3)

        ta.send_keys("some text")
        assert "some text" in ta.get_text()

        # make sure that the helpers return the expected values got from the raw element esp. wrt. switching iframes
        with edit_page.content.body.frame:
            is_selected = ta._debug_get_raw_web_element().is_selected()
            is_displayed = ta._debug_get_raw_web_element().is_displayed()
            is_enabled = ta._debug_get_raw_web_element().is_enabled()
            raw_element = ta._debug_get_raw_web_element()

        assert ta._element == raw_element

        assert ta.is_selected() == is_selected
        assert ta.is_displayed() == is_displayed
        assert ta.is_clickable() == is_displayed & is_enabled
        assert ta.is_present() == is_enabled

        ta.click()
        ta.get_attribute("id")

        # try dotting from the page down
        edit_page.content.body.write("edit_page.content.body")
        assert "edit_page.content.body" in edit_page.content.body.text_area.get_text()
        assert "edit_page.content.body" in ta.get_text()
        with edit_page.content.body.frame:
            assert "edit_page.content.body" in ta._debug_get_raw_web_element().text

        # the actual web element will give a stale exception until we are in the correct frame
        ta._debug_reset_raw_web_element()
        e1 = ta._element
        e2 = ta._debug_get_raw_web_element()
        with pytest.raises(JWP.StaleElementReferenceException):
            assert "edit_page.content.body" in e1.text
        with edit_page.content.body.frame:
            assert "edit_page.content.body" in e1.text
            assert "edit_page.content.body" in e2.text
            e3 = ta._debug_get_raw_web_element()
            assert "edit_page.content.body" in e3.text

        # try access through the region-iframe
        body = edit_page.content.body
        body.text_area.clear()
        assert body.text_area.get_text() == ''
        body.write("only_body")
        assert body.text_area.get_text() == "only_body"
        with body.frame:
            assert ta._debug_get_raw_web_element().text == "only_body"

    def test_nested_frames(self):
        frames_page = self.home_page.content.frames_link.click()
        assert frames_page.wait_for_on_this()

        nested_frames_page = frames_page.nested_frames.click()
        nested_frames_page.is_on_this()  # since the click will wait for the page

        bottom_frame = nested_frames_page.bottom_frame
        assert "BOTTOM" in bottom_frame.body.get_text()

        # nothing in the top frame so difficult to validate anything
        top_frame = nested_frames_page.top_frame
        assert top_frame

        # the left middle and right frames are all specified slightly differently as frames.
        # the left is a page wrapped in another class that defines the frame

        # look at it as a page
        left_frame_page = sample_pages.LeftFramePage(
            driver=self.driver, name="test_left_frame_page", base_url=self.sample_website_url).open()
        assert "LEFT" in left_frame_page.body.get_text()

        # look at it as a frame in a page
        top_frame = self.home_page.content.frames_link.click().nested_frames.click().top_frame
        left_frame = top_frame.left_frame
        assert "LEFT" in left_frame.body.get_text()

        # the right is a region defining a frame_locator
        right_frame = top_frame.right_frame
        assert "RIGHT" in right_frame.body.get_text()

        # check raw web element access while validating access from the region-frame, call _element since it will
        # get into the frame and then find the element.  Using the _debug_get_raw_web_element will return None since we
        # delay actually getting the web element until we use it.
        rfbe = right_frame.body._element
        with pytest.raises(JWP.StaleElementReferenceException):
            assert "RIGHT" in rfbe.text

        raw_rfbe = right_frame.body._debug_get_raw_web_element()
        assert raw_rfbe is None

        with right_frame.frame:
            assert "RIGHT" in rfbe.text

        # the middle is a region wrapping another region with the wrapper defining a frame_locator
        # get a JWE that is in a region and access it like you would any other JWE and show that frame entry and exit
        # is happening correctly
        middle_body = top_frame.middle_frame.body
        assert "MIDDLE" in middle_body.get_text()

        # The JWE will have a raw web element since we accessed in via get_text() above and if we get into the frame
        # manually with a with, we can access through the webelment to get its text
        with top_frame.middle_frame.frame:
            assert "MIDDLE" in middle_body._debug_get_raw_web_element().text

    def test_links(self):
        # This should be able to click & wait all the way through if the Link classes are implemented correctly.
        # Additionally, auto-complete should work all the way through in PyCharm.

        assert self.home_page.content.dynamic_loading_link.click().example_2_link.click().\
            start_button.click().text_content.is_displayed()

    def test_URLs(self):
        # Test URL types
        example1_page = self.home_page.content.dynamic_loading_link.click().example_1_link.click()
        assert parse.parse(
            JWP.normalize_url(example1_page.page_url),
            JWP.normalize_url(example1_page.driver.current_url))['id'] == '1'
        assert parse.parse(example1_page.page_url, example1_page.driver.current_url)['id'] == '1'
        button = self.home_page.content.dynamic_loading_link.click().example_1_link.click().start_button
        text_content = button.click()
        assert text_content.is_displayed()
