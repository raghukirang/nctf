import pytest

import nctf.test_lib.base.pages as JWP
import nctf.test_lib.sample_website.pages as sample_pages
import nctf.test_lib.utils.waiter as waiter
from selenium.common.exceptions import StaleElementReferenceException

from .base_ui_test_case_base import BaseUITestCaseBase


class NetCloudUITabTestCase(BaseUITestCaseBase):
    """When running locally it is expected that 'docker-compose run -p 7080:5000 sample_site' has been run first.
    Of course, all of the environment variables for secrets needs to be set up first"""

    ###################
    # The Tests:

    def test_tabs(self):
        home_page = self.home_page
        tabs_page = sample_pages.SampleWindowLoadingPage(driver=home_page.driver.new_tab(), name="tabs_page").open()
        another_home_page = sample_pages.SampleWebsiteIndexPage(driver=tabs_page.driver.new_tab(), name="2nd home page").open()
        tabs_page.restore_window()
        new_window_tab1 = tabs_page.click_here_link.click()
        assert new_window_tab1.is_on_this()
        assert new_window_tab1.some_text.get_text() == "New Window"
        tabs_page.restore_window()
        home_page.restore_window()
        another_home_page.restore_window()
        driver = home_page.driver
        driver.new_tab()
        driver.next_window()
        driver.next_window()
        driver.next_window()
        driver.next_window()
        driver.close_window()
        driver.switch_to.window(driver.window_handles[0])
        driver.close_window()
        driver.switch_to.window(driver.window_handles[0])
        driver.close_window()
        driver.switch_to.window(driver.window_handles[0])
        driver.close_window()
        pass

    def test_button_on_page_that_opens_tabs(self):
        page = self.home_page
        tabs_page = page.content.multiple_windows_link.click()
        # calling save_window() is optional since the window that the browser uses to create the page will be saved
        # to the page upon its creation
        tabs_page.save_window()
        new_window_tab1 = tabs_page.click_here_link.click()
        assert new_window_tab1.is_on_this()
        assert new_window_tab1.some_text.get_text() == "New Window"
        tabs_page.restore_window()
        assert tabs_page.is_on_this()
        new_window_tab2 = tabs_page.click_here_link.click()
        assert new_window_tab2.is_on_this()

        assert new_window_tab1.restore_window().is_on_this()
        assert tabs_page.restore_window().is_on_this()
        assert new_window_tab2.restore_window().is_on_this()
        new_window_tab2.driver.close()

        assert new_window_tab1.restore_window().is_on_this()
        assert tabs_page.restore_window().is_on_this()
