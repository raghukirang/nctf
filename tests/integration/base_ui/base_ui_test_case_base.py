import ast
import logging
import os
import socket
import sys
from typing import Union
import unittest

from attrdict import AttrMap
from nctf.test_lib.base.fixtures.ui_fixture import BaseUIFixture
import nctf.test_lib.base.pages as JWP
import nctf.test_lib.sample_website.pages as sample_pages


class BaseUITestCaseBase(unittest.TestCase):
    """When running locally it is expected that 'docker-compose run -p 7080:5000 sample_site' has been run first. 
    Of course, all of the environment variables for secrets needs to be set up first"""

    def setUp(self):
        super().setUp()
        sample_website_hostname = os.environ.get("TEST_SAMPLE_WEBSITE_HOSTNAME", "localhost")
        if sample_website_hostname != "localhost":
            # The sample website doesn't like domain names to be in the requests so substitute the IP.
            sample_website_ip = socket.gethostbyname(sample_website_hostname)
            self.services_info = JWP.ServicesInfo(
                AttrMap({
                    'sample_website': {
                        'hostname': sample_website_ip,
                        'protocol': 'http',
                        'port': 5000
                    }
                }))
        else:
            self.services_info = JWP.ServicesInfo(
                AttrMap({
                    'sample_website': {
                        'hostname': 'localhost',
                        'protocol': 'http',
                        'port': 7080
                    }
                }))
        # To see logging output, set TEST_SEND_LOGS_TO_STDOUT environment variable to True and be sure
        # to add '-s' to pytest invocation or this won't work!
        self.send_logs_to_stdout = ast.literal_eval(os.environ.get("TEST_SEND_LOGS_TO_STDOUT", "False"))

        if self.send_logs_to_stdout:
            root = logging.getLogger()
            root.setLevel(logging.DEBUG)

            ch = logging.StreamHandler(sys.stdout)
            ch.setLevel(logging.DEBUG)
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            ch.setFormatter(formatter)
            root.addHandler(ch)

        # starting from a CHROME copy of config, hard-code or override with environment variables
        config = JWP.UIWebDriverConfig.CHROME.copy()
        config['display_height'] = 1980
        config['display_width'] = 1020
        config['needle_save_baseline'] = False
        config['timeout'] = 30
        config['display_visible'] = ast.literal_eval(os.environ.get("DISPLAY_VISIBLE", "False"))

        self.ui_fixture = BaseUIFixture(config=config, services_info=self.services_info, proxy=None)
        self.__driver = None

    def tearDown(self):
        self.__driver = None
        self.ui_fixture.teardown()
        super().tearDown()

    @property
    def sample_website_url(self) -> str:
        return self.services_info.get_url('sample_website')

    @property
    def driver(self) -> Union[None, JWP.UIWebDriver]:
        if self.__driver is None:
            self.__driver = self.ui_fixture.get_driver()
        return self.__driver

    @property
    def home_page(self) -> sample_pages.SampleWebsiteIndexPage:
        return sample_pages.SampleWebsiteIndexPage(driver=self.driver, name="test_page").open()
