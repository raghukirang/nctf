import pytest

import nctf.test_lib.base.pages as JWP
import nctf.test_lib.sample_website.pages as sample_pages
import nctf.test_lib.utils.waiter as waiter

from .base_ui_test_case_base import BaseUITestCaseBase


class NetCloudUITestCase(BaseUITestCaseBase):
    """When running locally it is expected that docker-compose run -p 7080:5000 test_website has been run first."""

    ####################
    #  The Tests:

    def test_stale_page_with_same_elements(self):
        """
        Validate that the page.wait_for_on_this() reflects when the browser is on that page and not.  Validate
        that the regions and elements and their saved copies behave correctly:
          -  dotting through the page to get at underlying elements that really exist on the current page will work
          -  dotting through the page to elements no long on the current page throw exceptions
          -  accessing saved copies of elements from the original page always throw exceptions upon leaving that page
        """
        home_page = self.home_page
        assert home_page.wait_for_on_this()
        assert home_page.footer.center.title.get_text() == "Powered by Elemental Selenium"
        footer = home_page.footer
        footer_title = footer.center.title
        assert footer_title.get_text() == "Powered by Elemental Selenium"
        assert home_page.footer._element_.text == "Powered by Elemental Selenium"

        # navigate to another page
        home_url = home_page.driver.current_url
        home_page.driver.get(home_url + "/login")
        assert home_page.is_on_this() is False

        # this works since the page we went to has these elements in common with the home page
        assert home_page.footer.center.title.get_text() == "Powered by Elemental Selenium"
        assert home_page.footer.center.title.get_text() == "Powered by Elemental Selenium"
        assert home_page.flash_messages.get_text() == ''

        # elements not in common with the current page will throw ActionNotPossibleError exceptions
        with pytest.raises(JWP.ActionNotPossibleError):
            assert home_page.content.ab_testing_link.get_text()
        with pytest.raises(JWP.ActionNotPossibleError):
            assert home_page.content.title.get_text() == "Available Examples"
        with pytest.raises(JWP.ActionNotPossibleError):
            assert home_page.content.login_link.click()
        # accessing the fields of the would be stale page doesn't instantiate the region or elements so this
        # does not throw any exceptions
        assert home_page.content.dynamic_loading_link
        assert home_page.content.dynamic_loading_link._debug_get_raw_web_element() is None

        # saved element references that are stale will throw JWP.ActionNotPossibleError exceptions when attempting to
        # execute a method on them
        with pytest.raises(JWP.ActionNotPossibleError):
            assert footer_title.get_text() == "Powered by Elemental Selenium"
        with pytest.raises(JWP.ActionNotPossibleError):
            footer_title.clear()
        with pytest.raises(JWP.ActionNotPossibleError):
            footer_title.click()
        with pytest.raises(JWP.ActionNotPossibleError):
            footer_title.get_attribute('')

        # saved element references that are stale will throw JWP.RegionIsStale exceptions when attempting to
        # execute an 'is' method on them
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title.is_displayed()
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title.is_clickable()
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title.is_enabled()
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title.is_present()
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title.is_selected()

        # accessing through a stale region will throw RegionIsStale exception
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title = footer.center.title

        # go back to the home page
        home_page.driver.get(home_url)

        # this works since by coincidence we went back to the page, home_page, that page represents
        assert home_page.wait_for_on_this()

        # Still expect the squirreled away element and region to give exceptions
        with pytest.raises(JWP.ActionNotPossibleError):
            assert footer_title.get_text() == "Powered by Elemental Selenium"
        with pytest.raises(JWP.StaleElementReferenceException):
            footer_title = footer.center.title

        # these will work, since we reevaluated the page and dotting through to get the regions and elements
        # recreates all of the regions and elements from scratch
        assert home_page.content.heading.get_text() == "Welcome to the-internet"
        assert home_page.content.title.get_text() == "Available Examples"
        assert home_page.footer.center.title.get_text() == "Powered by Elemental Selenium"

    def test_stale_jwe_on_page(self):
        login_page = self.home_page.content.login_link.click()
        login_button = login_page.login_button
        assert login_button.get_text() == 'Login'

        # get the actual underlying WebElement and validate that it is still enabled
        assert login_button._debug_get_raw_web_element().is_enabled()

        # we change the browser current page to be the home page
        home_page = self.home_page
        assert home_page.wait_for_on_this()

        # this causes the login_button to no longer be valid.
        # We show this be expecting the underlying WebElement to throw a StaleElementReference exception
        # and expecting that our access via the JWE login_button throws a NoSuchElement exception.
        with pytest.raises(JWP.StaleElementReferenceException):
            assert login_button._debug_get_raw_web_element().is_enabled()
        with pytest.raises(JWP.ActionNotPossibleError):
            assert login_button.get_text() == 'Login'

        # try to make the JWE login_button legit, by making the current browser page be the login page
        login_page = home_page.content.login_link.click()
        assert login_page.wait_for_on_this()

        # show that the underlying WebElement to the login_button is still stale
        with pytest.raises(JWP.StaleElementReferenceException):
            assert login_button._debug_get_raw_web_element().is_enabled()

        # assert that access still throws StaleElementReference exceptions
        with pytest.raises(JWP.ActionNotPossibleError):
            assert login_button.get_text() == 'Login'
            assert login_button._debug_get_raw_web_element().is_enabled()

    def test_stale_same_selector_different_content_region_on_page(self):
        page = self.home_page
        footer = page.footer

        # get the actual underlying WebElement and validate that it is enabled
        assert footer._element.is_enabled()
        assert footer.center.title.get_text() == "Powered by Elemental Selenium"

        # we change the browser current page to be the login page
        login_page = page.content.login_link.click()
        assert login_page.wait_for_on_this()

        # This causes the footer to be no longer valid
        # The underlying WebElement will throw a StaleElementReference exception
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer._element.is_enabled()
        # accessing through the JWE will cause a JWP.ElementIsStaleError exception
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer.is_enabled()
        # try to access an element contained on the stale footer region that we tried earlier
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer.center.title.get_text() == "Powered by Elemental Selenium"
        # try an element on the stale region that we haven't tried yet
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer.center.link.get_text() == "Elemental Selenium"

        # but now we make the content region quasi legit, by making the current browser page be the home page
        home_page = self.home_page
        assert home_page.wait_for_on_this()

        # show that the underlying WebElement to the footer is still stale
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer._element.is_enabled()

        # assert that access still throws StaleElementReference exceptions
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer._element.is_enabled()
            assert footer.center.title.get_text() == "Powered by Elemental Selenium"
            assert footer.center.link.get_text() == "Elemental Selenium"

    def test_stale_same_selector_same_content_region_on_page(self):
        page = self.home_page
        footer = page.footer

        # get the actual underlying WebElement and validate that it is enabled
        assert footer._element.is_enabled()
        assert footer.center.title.get_text() == "Powered by Elemental Selenium"

        # we change the browser current page to be the login page
        login_page = page.content.login_link.click()
        assert login_page.wait_for_on_this()

        # this causes the login_button to no longer be valid.
        # We show this be expecting the underlying WebElement to throw a StaleElementReference exception
        # and expecting that our access via the JWE login_button throws a NoSuchElement exception.
        with pytest.raises(Exception):
            assert footer._element.is_enabled()
        # try an element on the region that we already had gotten via the login.click call above
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer.center.title.get_text() == "Powered by Elemental Selenium"
        # try an element on the region that we haven't touched yet
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer.center.link.get_text() == "Elemental Selenium"

        # but now we make the content region quasi legit, by making the current browser page be the homeå page
        home_page = self.home_page
        assert home_page.wait_for_on_this()

        # show that the underlying WebElement to the login_button is still stale
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer._element.is_enabled()

        # assert that access still throws StaleElementReference exceptions
        with pytest.raises(JWP.StaleElementReferenceException):
            assert footer._element.is_enabled()
            assert footer.center.title.get_text() == "Powered by Elemental Selenium"
            assert footer.center.link.get_text() == "Elemental Selenium"

    def test_stale_journey_region_on_page(self):
        page = self.home_page

        # build a content region dynamically by passing in the already located element
        content_element = page.find_element(JWP.By.ID, 'content')
        content_dynamic = sample_pages.SampleWebsiteBasePage.ContentRegion(
            page, "content region dynamic", strategy=None, locator=None, root_element=content_element)
        assert content_dynamic.strategy is None
        assert content_dynamic.locator is None

        # validate aspects of the dynamically built region vs the default constructed one
        page = self.home_page
        content_region = page.content
        assert content_dynamic._element_ != content_region._element_
        with pytest.raises(JWP.StaleElementReferenceException):
            n = content_dynamic.find_element(strategy=JWP.By.CLASS_NAME, locator="heading")
