import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))

#sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..', '..'))


def pytest_addoption(parser):
    def directory(str_d):
        if not (os.path.isdir(str_d)):
            raise IOError("Path {} is not a directory.".format(str_d))
        if not (os.path.exists(str_d)):
            raise IOError("Path {} artifacts does not exist.".format(str_d))
        return str_d

    parser.addoption(
        "--artifacts",
        action="store",
        required=False,
        type=directory,
        default=os.path.dirname(os.path.realpath(__file__)),
        help="The path where test artifacts shall be saved. Defaults to the directory of your conftest.py")

    parser.addoption(
        "--screenshots",
        action="store",
        required=False,
        type=directory,
        default=None,
        help="The path where needle baseline screenshots are stored.")

    # Disable Browsermob proxy
    parser.addoption(
        "--disable-proxy", action="store_false", dest='browser_mob_proxy', help="Disable the Browsermob proxy.")

    # Enable Browsermob proxy
    parser.addoption(
        "--enable-proxy", action="store_true", dest='browser_mob_proxy', help="Enable the Browsermob proxy.")
