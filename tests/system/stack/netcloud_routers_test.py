import logging
import os

from faker import Faker
import pytest

from nctf import nctf_typing

logger = logging.getLogger('netcloud_router_test')


def validate_router(router):
    assert router.ncos
    assert router.ncos.remote_admin_port
    assert router.ncos.admin_password
    assert router.ncm is None


def test_netcloud_create_router(
        netcloud_router_fixture: nctf_typing.NetCloudRouterFixture, config_fixture: nctf_typing.ConfigFixture,
        netcloud_rest_fixture: nctf_typing.NetCloudRESTFixture, netcloud_account_fixture: nctf_typing.NetCloudAccountFixture,
        netcloud_ui_fixture: nctf_typing.NetCloudUIFixture):

    logger.info('starting netcloud create random router')
    router = netcloud_router_fixture.create(product_name='3200v')
    account = netcloud_account_fixture.create_random_account()
    router.register_with_account(account)

    user = account.admin
    ncm = user.ncm_rest_client

    devices_page = netcloud_ui_fixture.start().open().login(account.admin)
    devices_page.routers_panel.router_table.get_rows()[0].cells['CheckBox'].ui_element.click()
    devices_page.routers_panel.router_table.device_selection_checkbox(router.ncm.router_id)

    groups_page = devices_page.side_nav_bar.groups_button.click()

    # Add group via the UI
    group_name = f"foo_group_{Faker().pystr(min_chars=6, max_chars=8)}"
    groups_page = groups_page.add_group(group_name=group_name, product_name=router.product, firmware_name=router.fw_version)
    groups_page.groups_panel.groups_table.select_group(group_name)
    assert groups_page.groups_panel.groups_table.is_group_selected(group_name)
    gid = groups_page.groups_panel.groups_table.get_rows('Name', group_name)[0].group_id
    assert gid

    # Add a group via API
    group_name = f'RM-1413 Group {Faker().pystr(min_chars=6, max_chars=8)}'
    product_list = ncm.products.list(params={'name': router.product})
    product_id = product_list.json()['data'][0]['id']
    firmware_list = ncm.firmwares.list(params={'product': product_id, 'version': '6.5.2'})
    firmware_id = firmware_list.json()['data'][0]['id']
    account_id = ncm.accounts.list().json()['data'][0]['id']
    group = ncm.groups.create(name=group_name, account_id=account_id, product_id=product_id, firmware_id=firmware_id)
    group_id = group.json()['data']['id']

    # Move router into group
    router.ncm.move_to_group(group_id)
    router.ncm.wait_for_config_sync()

    # Edit group config to add password
    diff = [{'system': {'users': {'0': {'password': 'Password1!'}}}}, []]
    ncm.groups.update(group_id, payload={'configuration': diff})

    # router.wait_for_config_sync_in_ecm()

    # Update group firmware
    firmware_list = ncm.firmwares.list(params={'product': product_id, 'version': '6.5.4'})
    firmware_id = firmware_list.json()['data'][0]['id']
    ncm.groups.update(group_id, firmware_id=firmware_id)
    pass


def test_router_sdk(tlogger, netcloud_account_fixture: nctf_typing.NetCloudAccountFixture,
                    netcloud_ui_fixture: nctf_typing.NetCloudUIFixture,
                    netcloud_router_fixture: nctf_typing.NetCloudRouterFixture, config_fixture: nctf_typing.ConfigFixture,
                    netcloud_rest_fixture: nctf_typing.NetCloudRESTFixture):
    """This is a 'happy path' SDK journey. It uploads an app, creates a group,
    installs the app to the group, moves the router to the group,
    waits for the device to start the app, then deletes the app and verifies that it is removed
    """
    admin = netcloud_account_fixture.create(entitlements=['NETCLOUD_BRANCH_ROUTERS_1YR'])

    netcloud_rest_client = netcloud_rest_fixture.get_client(admin)
    router = None
    try:
        router = netcloud_router_fixture.create(product_name='3200v')
    except nctf_typing.NetCloudRouterFixtureException as netcloud_router_exception:
        # Skip this test if we are unable to lease a router
        pytest.skip(msg=str(netcloud_router_exception))

    router.register(
        stream_host=config_fixture.target.services.stream.hostname,
        ncm_username=admin.username,
        ncm_password=admin.password,
        ncm_rest_client=netcloud_rest_client.ncm)
    tlogger.info(router)

    router.ncm.change_name_to_id()
    nctf_typing.wait(120, 1).for_call(lambda: router.ncm.state).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    login_page.login(admin)

    netcloud_api = netcloud_rest_fixture.get_client()
    netcloud_api.authenticate_actor(admin)
    account_id = netcloud_api.ncm.accounts.parent_account_id()
    product_id = router.ncm.product_id
    firmware_id = router.ncm.actual_firmware_id

    # I upload a valid app
    app_bundle_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'testfiles', "valid", 'RouterSDKDemo.tar.gz'))
    with open(app_bundle_path, 'rb') as app_bundle:
        netcloud_api.ncm.sdk_versions._post(files={'archive': app_bundle})

    # Create a group
    group_name = "grouper"
    group = netcloud_api.ncm.groups.create(group_name, account_id, product_id, firmware_id).json()
    group_id = netcloud_api.ncm.get('/api/v1/groups/').json()['data'][0]['id']

    # Move router to group
    router_id = netcloud_api.ncm.routers.router_id()
    netcloud_api.ncm.routers.move_to_group(router_id=router_id, group_id=group['data']['id'])

    def get_app_version_data():
        data = netcloud_api.ncm.get('/api/v1/device_app_versions/', 200).json()['data']
        if len(data) > 0:
            return data
        else:
            return None

    # Add app to group
    data = nctf_typing.wait(120, 5).for_call(get_app_version_data).to_not_be(None)
    app_version_id = data[0]['id']
    payload = {
        "account": f"/api/v1/accounts/{account_id}/",
        "app_version": f"/api/v1/device_app_versions/{app_version_id}/",
        "group": f"/api/v1/groups/{group_id}/"
    }
    netcloud_api.ncm.post(json=payload, path='/api/v1/device_app_bindings/', expected_status_code=201)

    # Check app status
    nctf_typing.wait(
        120, 5).for_call(lambda: netcloud_api.ncm.get('/api/v1/device_app_states/', 200).json()['success']).to_equal(True)

    # Remove app from group api call.
    app_payload = {
        "account": f"/api/v1/accounts/{account_id}/",
        "app_version": f"/api/v1/device_app_versions/{app_version_id}/",
        "group": f"/api/v1/groups/{group_id}/",
        "resource_uri": f"/api/v1/device_app_bindings/{app_version_id}"
    }
    netcloud_api.ncm.delete(json=app_payload, path=f'/api/v1/device_app_bindings/')

    # Checking no apps installed
    nctf_typing.wait(120, 5).for_call(
        lambda: len(netcloud_api.ncm.get(f'/api/v1/groups/{group_id}/device_app_bindings').json()['data'])).to_equal(0)


def test_5_check_fw_doesnt_brick_router(request, tlogger, netcloud_router_fixture: nctf_typing.NetCloudRouterFixture,
                                        config_fixture: nctf_typing.ConfigFixture):
    # Lease a router from the pool specified by run.fixtures.arm4.defaultPoolId
    router = netcloud_router_fixture.create(product_name='')

    #router = arm4_fixture.lease_router_by_product_name(product_name='AER3150', reason=request.node.name)

    # Load the target firmware specified by target.services.ncos.*
    router.ncos.load_fw()

    # Demonstrate we can now talk to the device and the FW matches!
    api = router.get_rest_client()
    fw_info = api.status_fw_info.get()
    tlogger.info(fw_info)
    assert fw_info['success']
    assert config_fixture.target.services.ncos.buildType.upper() in fw_info['data']['build_type'].upper()
    assert fw_info['data']['build_version'].upper() in config_fixture.target.services.ncos.gitSha.upper()
