import logging
import time

from faker import Faker
import pytest

from nctf import nctf_typing as nctf

logger = logging.getLogger('netcloud_ui_test')


def test_netcloud_ui_login(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('starting login test')

    login_page = netcloud_ui_fixture.start()
    login_page.open()
    chromedriver_version = login_page.driver.capabilities['chrome']['chromedriverVersion']
    chrome_version = login_page.driver.capabilities['version']
    logger.info('\n***********************************************************************************************'
                '\nChrome Version = {}\nChromedriver Vesion = {}'
                '\n***********************************************************************************************'.format(
                    chrome_version, chromedriver_version))
    login_page = login_page.netcloud_logo.click()
    devices_page = login_page.login(netcloud_account_fixture.create())
    profile_dialog = devices_page.account_dropdown.click().profile_button.click()
    dev_pg = profile_dialog.cancel_button.click()
    assert dev_pg


# # Redundant test, but useful for debugging purposes
#
# def test_netcloud_ui_login_new_account(netcloud_account_fixture: nctf.NetCloudAccountFixture,
#                                        netcloud_ui_fixture: nctf.NetCloudUIFixture):
#     logger.info('starting routers page test')
#     admin = netcloud_account_fixture.create()
#     login_page = netcloud_ui_fixture.start()
#     login_page.open()
#     devices_page = login_page.login(admin)
#     assert devices_page.is_on_this()


def test_netcloud_ui_routers(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                             netcloud_router_fixture: nctf.NetCloudRouterFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture,
                             netcloud_rest_fixture: nctf.NetCloudRESTFixture, config_fixture: nctf.ConfigFixture):

    logger.info('starting routers page test')

    account = netcloud_account_fixture.create_account()
    router = netcloud_router_fixture.create(product_name='3200v')
    router.register_with_account(account)

    # devices_page = netcloud_ui_fixture.start().open().login(admin)
    # devices_page.routers_panel.router_table.get_rows()[0].cells['CheckBox'].ui_element.click()
    # devices_page.routers_panel.router_table.device_selection_checkbox(router.ncm.router_id)
    #
    # groups_page = devices_page.side_nav_bar.groups_button.click()
    #
    # # Add group
    # group_name = f"foo_group_{Faker().pystr(min_chars=6, max_chars=8)}"
    # groups_page = groups_page.add_group(group_name=group_name, product_name=router.product, firmware_name=router.fw_version)
    # groups_page.groups_panel.groups_table.select_group(group_name)
    # assert groups_page.groups_panel.groups_table.is_group_selected(group_name)
    pass


def test_netcloud_ui_frame(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('starting ncm ncp frame test')

    # Show the usage of an iframe by first logging in to NCM
    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(netcloud_account_fixture.create())
    assert devices_page.wait_for_on_this()

    # Navigate the browser to the overlay page directly
    overlay_page = nctf.ncp_pages.OverlayPage(devices_page.driver).open()
    assert overlay_page.is_on_this()

    # Navigate to the overlay page as an iframe on the NCP Page
    devices_page = nctf.ncm_pages.RoutersPage(devices_page.driver).open()
    # the open implies we waited for the page to load, so just assert is on
    assert devices_page.is_on_this()
    ncp_page = devices_page.side_nav_bar.ncp_button.click()
    assert ncp_page.is_on_this()


def test_qa3_netcloud_networks(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                               netcloud_ui_fixture: nctf.NetCloudUIFixture):
    """ This test needs to run on QA3
    """
    logger.info('starting ncm networks test')

    # Show the usage of an iframe by first logging in to NCM
    # devices_page = netcloud_ui_fixture.start().open().login(nctf.NetCloudActor('duinclint@gmail.com', 'Bzed8733'))
    devices_page = netcloud_ui_fixture.start().open().login(netcloud_account_fixture.create())

    # Navigate to the NCM networks page and grab the NetworksHomePage through the overlay home as an iframe
    networks_home = devices_page.side_nav_bar.networks_button.click().overlay_home
    assert networks_home.is_on_this()

    # Get an element from within the iframe and assert something about it
    add_button = networks_home.add_button
    assert add_button.wait_for_clickable()

    # Doing something with the raw web element without being in the correct frame will throw an exception
    with pytest.raises(nctf.StaleElementReferenceException):
        add_button._element_.is_displayed()

    # Use the frame associated with the add_button within a with statement
    with add_button.frame:
        assert add_button._element_.is_displayed()

    # Navigate to the Sockeye add networks and set a network name and validate that it got set
    add_network_page = add_button.click()
    add_network_page.network_name_text_field.set_text('test network')
    assert add_network_page.network_name_text_field.get_attribute('value') == 'test network'

    # Validate that the parent's page is the NCM NetworksPage and that some element on that outer page is relevant
    assert isinstance(add_network_page.parent.page, nctf.ncm_pages.NetworksPage)
    assert add_network_page.parent.page.side_nav_bar.dashboard_button.is_displayed()

    # Validate that trying to look at an element on the NCM NetworkPage through the overlay_home property is going to
    # throw a NoSuchElementException
    with pytest.raises(nctf.NoSuchElementException):
        assert add_network_page.parent.page.overlay_home.add_button._element

    devices_page = add_network_page.parent.page.side_nav_bar.devices_button.click()

    # since the browser has been logged in, create the networks page directly from Sockeye and navigate to it
    sockeye_home = nctf.sockeye_pages.NetworksHomePage(devices_page.driver).open()
    assert sockeye_home.is_on_this()
    add_network_page = sockeye_home.add_button.click()
    assert add_network_page.is_on_this()
    network_name = add_network_page.network_name_text_field
    network_name.set_text('darren')
    assert network_name.get_attribute('value') == 'darren'
    networks_page = add_network_page.cancel_button.click()
    assert networks_page.is_on_this()

    # now go back to the ncm version of those pages

    networks_page = nctf.ncm_pages.NetworksPage(driver=devices_page.driver, path='vpn').open()
    networks_home = networks_page.overlay_home
    assert networks_home.is_on_this()
    add_button = networks_home.add_button
    assert add_button.is_displayed()
    with pytest.raises(nctf.StaleElementReferenceException):
        add_button._element_.is_displayed()
    with add_button.frame:
        assert add_button._element_.is_displayed()
    add_network_page = networks_home.add_button.click()
    network_name = add_network_page.network_name_text_field
    network_name.set_text('darren')
    assert network_name.get_attribute('value') == 'darren'
    with pytest.raises(nctf.StaleElementReferenceException):
        value = network_name._element_.get_attribute('value')
        assert value == 'darren'
    with network_name.frame:
        value = network_name._element_.get_attribute('value')
        assert value == 'darren'

    networks_page = add_network_page.cancel_button.click()
    assert networks_page.is_on_this()


def test_netcloud_tabs(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('starting tabs test')
    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(netcloud_account_fixture.create())
    accts_users_page = nctf.ncm_pages.AccountsAndUsersPage(driver=devices_page.driver.new_tab()).open()
    assert accts_users_page.is_on_this()
    devices_page.restore_window()
    assert devices_page.is_on_this()
    assert accts_users_page.is_on_this() is False
    devices_page.driver.next_window()
    assert accts_users_page.is_on_this()
    assert devices_page.is_on_this() is False


def test_router_dev_mode_group(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                               netcloud_ui_fixture: nctf.NetCloudUIFixture, arm_fixture: nctf.ARMInterface,
                               config_fixture: nctf.ConfigFixture, netcloud_rest_fixture: nctf.NetCloudRESTFixture):
    """
    Test for adding a router to Developer Mode when a router is in a group, expect a failure
    """

    admin = netcloud_account_fixture.create()
    router = None
    try:
        router = arm_fixture.get_router(desired_product='3200v')
    except nctf.ARMException as arm_exception:
        pytest.skip(msg=str(arm_exception))

    # Register our router to NCM
    router.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_username=admin.username,
        ecm_password=admin.password,
        ecm_use_sso=True)

    router.rename_router_to_ecm_client_id()
    router.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router.ecm_data()['state']).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    # Click the add button in Dev Mode Routers grid
    tools_page = devices_page.side_nav_bar.tools_button.click().opt_in()

    # Select the router, put it into dev mode
    router_picker_dialog = tools_page.router_apps_region.dev_mode_routers_add_button.click().maximize_button.click()
    devices_table = router_picker_dialog.devices_table
    devices_table.get_rows()[0].cells['CheckBox'].ui_element.click()
    add_devices_popup = router_picker_dialog.save_button.click()
    tools_page = add_devices_popup.yes_button.click()
    # tools_page.spinner.wait_for_spinner()

    # Reboot the router to apply Developer Mode license
    router.reboot()
    router.wait_for_reachable()

    # Add group
    groups_page = tools_page.side_nav_bar.groups_button.click()
    group_name = f"foo_group_{Faker().pystr(min_chars=6, max_chars=8)}"
    groups_page = groups_page.add_group(group_name=group_name, product_name=router.product, firmware_name=router.fw_version)

    # Attempt to add router to group
    devices_page = groups_page.side_nav_bar.devices_button.click()
    routers_table = devices_page.routers_panel.router_table
    routers_table.get_rows()[0].cells['CheckBox'].ui_element.click()
    devices_page.routers_panel.toolbar.move_button.click()


def test_sdk_the_admin_opts_out_of_sdk(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                                       netcloud_ui_fixture: nctf.NetCloudUIFixture, arm_fixture: nctf.ARMInterface,
                                       config_fixture: nctf.ConfigFixture, netcloud_rest_fixture: nctf.NetCloudRESTFixture):
    """
    The admin opts out with a router in dev mode and an app installed
    """

    admin = netcloud_account_fixture.create()
    router = None
    try:
        router = arm_fixture.get_router()
    except nctf.ARMException as arm_exception:
        # Skip this test if we are unable to lease a router
        pytest.skip(msg=str(arm_exception))

    # Register our router to NCM
    router.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_username=admin.username,
        ecm_password=admin.password,
        ecm_use_sso=True)

    router.rename_router_to_ecm_client_id()
    router.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router.ecm_data()['state']).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    tools_page = devices_page.side_nav_bar.tools_button.click()
    tools_page.opt_in()
    # the apps grid is empty
    tools_page.purge_apps_with_retry()
    router_picker = tools_page.router_apps_region.dev_mode_routers_add_button.click()
    router_picker.devices_table.device_selection_checkbox(device_id=router.ecm_client_id).click()
    confirmation_dialog = router_picker.save_button.click()
    tools_page = confirmation_dialog.yes_button.click()
    table = tools_page.router_apps_region.dev_mode_router_table
    router_name_from_table = table.get_rows()[0].cells['Name'].text
    assert router_name_from_table == router.ecm_client_id, \
        "The device [{}] was not found in the Developer Mode Routers table.".format(router.ecm_client_id)


def test_groups(netcloud_account_fixture: nctf.NetCloudAccountFixture, arm_fixture: nctf.ARMInterface,
                config_fixture: nctf.ConfigFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):

    admin = netcloud_account_fixture.create()
    router = None
    try:
        router = arm_fixture.get_router()
    except nctf.ARMException as arm_exception:
        # Skip this test if we are unable to lease a router
        pytest.skip(msg=str(arm_exception))

    # Register our router to NCM
    router.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_username=admin.username,
        ecm_password=admin.password,
        ecm_use_sso=True)

    router.rename_router_to_ecm_client_id()
    router.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router.ecm_data()['state']).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    groups_page = devices_page.side_nav_bar.groups_button.click()
    group_name = "foo_group_{}".format(Faker().pystr(min_chars=6, max_chars=8))
    groups_page = groups_page.add_group(group_name=group_name, product_name=router.product, firmware_name=router.fw_version)

    groups_page.groups_panel.groups_table.get_rows()[0].select()
    config_editor = groups_page.groups_panel.config_dropdown.click().edit_button.click()
    remote_ui = config_editor.config_body
    net_menu = remote_ui.side_nav_bar.networking_menu

    local_ip_nw_button = net_menu.click().local_networks.click().local_ip_networks
    local_ip_nw = local_ip_nw_button.click()

    name = 'New LAN'
    ipv4_addr = '192.168.1.1'
    local_network_editor = local_ip_nw.add_button.click()

    local_network_editor.name_field.send_keys(name)
    sidebar = local_network_editor.sidebar
    ipv4_settings_region = sidebar.ipv4_settings.click()
    ip_field = ipv4_settings_region.ip_field
    ip_field.send_keys(ipv4_addr)
    time.sleep(0.5)  # Otherwise it is too quick and throws error: missing required field(s): ['ip_address']
    save_window = local_network_editor.save_button.click()

    if 'Settings were successfully saved' not in save_window.message.get_text():
        logger.error("Failed to detect expected save message.")
        #fw_issues['failed_fw_list'].append(fw)

    local_ip_nw = save_window.ok_button.click()

    pending_changes = config_editor.view_pending_changes_button.click()
    config_editor = pending_changes.close_button.click()
    pending_changes = config_editor.view_pending_changes_button.click()
    config_editor = pending_changes.close_button.click()
    pending_changes = config_editor.view_pending_changes_button.click()
    config_editor = pending_changes.close_button.click()
    confirmation_dialog = config_editor.discard_changes_button.click()
    devices_page = confirmation_dialog.ok_button.click()
    assert devices_page
