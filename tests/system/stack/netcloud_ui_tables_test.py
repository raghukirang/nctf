import logging
import os
import random
import string
import typing

from faker import Faker
from nctf import nctf_typing as nctf
import pytest

logger = logging.getLogger('netcloud_ui_test')


def test_ui_network_vpn(arm_fixture: nctf.ARMInterface, netcloud_ui_fixture: nctf.NetCloudUIFixture,
                        netcloud_account_fixture: nctf.NetCloudAccountFixture, config_fixture: nctf.ConfigFixture):

    # Create a new NetCloud Account
    # pp = pprint.PrettyPrinter(width=41, compact=True)
    new_admin = netcloud_account_fixture.create()

    # Login the new admin using the Accounts Services REST API
    # accounts_api = accserv_rest_fixture.get_client()
    # accounts_api.authenticate(new_admin.username, new_admin.password)

    # Create Account - Setup our predefined user
    # predefined_user = nctf.NetCloudActor(username=new_admin.username, password=new_admin.password)

    # Add Router to account -Lease a router from ARM - Register our router to NCM
    try:
        router_hub = arm_fixture.get_router(desired_product='3200v')
    except nctf.ARMException as arm_exception:
        pytest.skip(msg=str(arm_exception))

    assert router_hub
    router_hub.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_username=new_admin.username,
        ecm_password=new_admin.password,
        ecm_use_sso=True)

    # Add Router to account -Lease a router from ARM - Register our router to NCM
    router_spoke = arm_fixture.get_router(desired_product='3200v')
    router_spoke.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_username=new_admin.username,
        ecm_password=new_admin.password,
        ecm_use_sso=True)

    # New Routers must be given time to sync
    router_hub.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router_hub.ecm_data()['state']).to_equal('online')
    router_spoke.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router_spoke.ecm_data()['state']).to_equal('online')

    # The spoke cannot have the same primary lan ip address as the hub
    lan_data = router_spoke.api.config_lan.get(name='0')["data"]
    lan_data["ip_address"] = "192.168.20.1"
    result = router_spoke.api.config_lan.update(name='0', update=lan_data, service_restart_timeout=180)
    assert result["success"], "Failed attempt to change primary network lan address"

    # Login
    login_page = netcloud_ui_fixture.start()
    login_page.open()

    # Login to NCM UI and wait for devices page to load
    devices_page = login_page.login(new_admin)

    # Navigate to the GROUPS page and wait for it to load
    networks_page = devices_page.side_nav_bar.networks_button.click()

    # Add Network -> Add VPN
    add_vpn_page = networks_page.overlay_home.add_button.click()
    chars = string.ascii_uppercase + string.ascii_lowercase
    rand_name = (''.join(random.choice(chars) for _ in range(5))) + "-test_ui_network_vpn"
    add_vpn_page.network_name_text_field.click()
    add_vpn_page.network_name_text_field.send_keys(rand_name)
    add_vpn_page.add_hub(router_hub.name)
    add_vpn_page.add_spoke(router_spoke.name)
    confirmation_popup = add_vpn_page.build_button.click()
    new_network_page = confirmation_popup.confirm_button.click()

    # Return to main Networks page so we can verify our new network is listed
    networks_page = new_network_page.networks_breadcrumb_button.click()
    assert networks_page.networks_table.num_rows == 1, "Only one network was added: table should only have one item"
    network_table_rows = networks_page.networks_table.get_rows(key='Network Name', value=rand_name)
    network_table_rows[0].select()
    confirmation_popup = networks_page.delete_button.click()
    networks_page = confirmation_popup.delete_button.click()
    assert networks_page.is_on_this()


def validate_table(table: nctf.UITable, headers: typing.List[str], rows_selectable: bool):
    """Validates a table

    Args:
        table: The UITable to validate.
        headers: A list of strings representing the text of the column headers
        rows_selectable: Can clicking the first cell of a row cause the row to be selected

    Returns:
        A validated table that has one row selected
    """
    # Validate header access
    logger.info('Validating table {}...'.format(table))
    table_headers = table.headers
    for header_name in headers:
        col_header = table_headers[header_name]
        assert header_name in col_header.header_text,\
            "header_name={} column header='{}'".format(header_name, col_header.header_text)
    logger.info('...headers are valid.'.format(table))

    # Validate caching
    ct = table.cached_table

    # Make sure that all of the correct waiting for the table to load is being done
    table = table.refresh()
    ct = table.cached_table
    if len(ct) > 0:
        assert ct[len(ct) - 1].cells[headers[0]]
        table = table.refresh()
        ct = table.cached_table
        assert ct[len(ct) - 1].cells[headers[0]]
        num_rows = len(ct)
        assert num_rows != 0

        # Validate access to all of the cells in all of the rows
        count = 0
        for row in ct:
            assert not row.is_selected()
            for header_name in headers:
                assert row.cells[header_name].text, \
                    "row={} column header='{}' text='{}'".format(count, header_name, row.cells[header_name].text)
            count += 1
        assert num_rows == count
        logger.info('...access to all cells validated.'.format(table))

        # Given the set of rows, choose the last one and select it, note that the cached version won't be updated
        if len(headers) > 2 and rows_selectable:
            saved_row = ct[count - 1]
            saved_row.cells[headers[0]].ui_element.click()
            assert saved_row.is_selected() is False

            # Take another snapshot of the table, see if the selection has been made and that the details button is clickable.
            ct = table.cached_table
            for row in filter(lambda t: t.is_selected(), ct):
                for header_name in headers:
                    assert saved_row.cells[header_name].text.strip() == row.cells[header_name].text.strip()

            # Use get_rows to find the same row by its first column
            row = None
            found = False
            for row in table.get_rows(key=headers[1], value=saved_row.cells[headers[1]].text):
                for header_name in headers:
                    found = found or saved_row.cells[header_name].text.strip() == row.cells[header_name].text.strip()
                if found:
                    break
            assert found
            assert row

            # Validate scrolling
            if table.is_scrollable():
                scrolling_table = typing.cast(nctf.EmberTable, table)
                scrolling_table.page_down()
                scrolling_table.page_down()
                scrolling_table.page_down()
                scrolling_table.page_down()
                row.ui_element.scroll_to()
                scrolling_table.scroll_to_top()
            logger.info('...scrolling validated.'.format(table))

        # Validate paging
        if table.is_pageable():
            paging_table = typing.cast(nctf.UIPagingTable, table)
            total_pages = paging_table.total_pages
            if total_pages > 1:
                paging_table = paging_table.next_page().previous_page().last_page().first_page().goto_page(total_pages)
            assert paging_table
            logger.info('...paging validated.'.format(table))

        logger.info('...{} validated.'.format(table))


def test_network_interfaces_table(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                                  netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('Starting ncm networks interfaces table test')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(netcloud_account_fixture.create())
    network_interfaces_page = devices_page.network_interfaces_button.click()
    netif_table = network_interfaces_page.network_interfaces_panel.network_interfaces_table

    netif_headers = [
        'CheckBox', 'Last known online status', 'Signal Strength', 'Name', 'Cellular RSSI', 'SINR', 'ECIO', 'Service Type',
        'Carrier', 'Registered Carrier', 'IPv4 Address', 'Model', 'Router Full Product', 'Bytes In', 'Bytes Out', 'MDN',
        'Modem FW Status'
    ]

    validate_table(netif_table, netif_headers, True)
    headers = netif_table.headers
    header = headers['Name']
    assert 'Name' == header.get_text()
    ct = netif_table.cached_table

    # Test filtering the cached_table using filter
    mrows = filter(lambda r: r.cells['Carrier'].text == 'Verizon', ct)
    for row in mrows:
        assert row.cells['Carrier'].text == 'Verizon'

    # Test filtering the cached_table using list comprehensions
    for row in [row for row in ct if row.cells['Carrier'] == 'Verizon']:
        assert row.cells['Carrier'].text == 'Verizon'

    # Test using get_rows
    for row in netif_table.get_rows('Carrier', 'Verizon'):
        assert row.cells['Model'].text

    dashboard = network_interfaces_page.side_nav_bar.dashboard_button.click()
    assert dashboard.is_on_this()
    geoview_page = dashboard.dashboard_nav_bar.geoview_tab.click()
    assert geoview_page.is_on_this()
    devices_page = geoview_page.side_nav_bar.devices_button.click()
    assert devices_page.is_on_this()


def test_sockeye_table(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    """This test needs to run on QA3"""
    logger.info('starting sockeye ember table test')

    # Show the usage of an ember table by first logging in to NCM
    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(netcloud_account_fixture.create())

    # Navigate to the NCM networks page and grab the NetworksHomePage through the overlay home as an iframe
    networks_home = devices_page.side_nav_bar.networks_button.click().overlay_home
    assert networks_home.is_on_this()
    assert networks_home.networks_table
    networks_table = networks_home.networks_table

    sockeye_headers = [
        'Checkbox', 'Network Status', 'Network Name', 'Spokes', 'Online Spokes (VPN)', 'Online Spokes (no VPN)',
        'Offline Spokes'
    ]

    validate_table(networks_table, sockeye_headers, True)
    header = networks_table.headers['Spokes']
    assert 'Spokes' == header.get_text()
    for row in networks_table.get_rows():
        spokes = row.cells['Spokes'].text.lower()
        logger.info('spokes=' + spokes)


def test_ember_tables(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('Starting extjs and ember tables on the same page test')

    user = netcloud_account_fixture.create()
    # Log into NCM
    devices_page = netcloud_ui_fixture.start().open().login(user)

    # Open the als home page directly to test it out before testing it embedded in an iframe
    als_home = nctf.als_pages.ActivitiesLogPage(driver=devices_page.driver, path_tbd='').open()
    activity_log_table = als_home.toolbar.refresh_button.click()
    assert activity_log_table
    assert not als_home.toolbar.details_button.is_clickable()
    assert "Custom Report" in als_home.toolbar.export_dropdown.click().get_text()
    als_home.toolbar.export_dropdown.click()

    als_headers = ['Checkbox', 'Timestamp', 'Activity Type', 'Description']

    validate_table(activity_log_table, als_headers, True)
    assert als_home.toolbar.details_button.is_clickable()

    selected_activities = list(filter(lambda t: t.is_selected(), activity_log_table.cached_table))
    assert len(selected_activities) == 1

    selected_activities[0].cells['Checkbox'].ui_element.click()
    assert not als_home.toolbar.details_button.is_clickable()

    # Validate the ALS table as contained on the NCM AlertsLogPage
    ncm_alerts_logs_page = nctf.ncm_pages.AlertsAndLogsPage(driver=devices_page.driver, path_tbd='').open()
    activity_log_page = ncm_alerts_logs_page.activity_log_tab.click()
    activity_log_panel = activity_log_page.activity_log_panel
    validate_table(activity_log_panel.activities_log, ['Checkbox', 'Timestamp', 'Activity Type', 'Description'], True)
    assert activity_log_panel.toolbar.details_button.is_clickable()

    ncm_alerts_logs_page = activity_log_page.alert_log_tab.click()
    # ncm_alerts_logs_page = devices_page.side_nav_bar.alerts_and_logs_button.click()

    alert_log_panel = ncm_alerts_logs_page.alert_log_panel

    alert_headers = [
        'CheckBox', 'Timestamp', 'Type', 'MAC Address', 'Group', 'Product', 'Device Name', 'Description', 'Account'
    ]

    validate_table(alert_log_panel.table, alert_headers, True)


def fill_with_users(account: nctf.NetCloudAccount) -> []:
    users = []
    for activation in [False, nctf.ActivationMethod.API]:
        for authenticate in [True, False]:
            if activation is False:
                authenticate = False
            for applications in [['ecm', 'nce']]:
                for is_active in [True, False]:
                    if is_active is False:
                        activation = False
                        authenticate = False
                    for is_locked in [False]:
                        for role in nctf.Roles:
                            users.append(
                                account.create_user(
                                    role=role,
                                    activation_method=activation,
                                    applications=applications,
                                    is_active=is_active,
                                    is_locked=is_locked,
                                    authenticate=authenticate))


def test_accounts_and_users_tables(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                                   netcloud_ui_fixture: nctf.NetCloudUIFixture) -> None:

    # Get NCM SSO User
    # account = netcloud_account_fixture.get_predefined_account()
    account = netcloud_account_fixture.create_account()
    user = account.admin
    fill_with_users(account)

    # Log in through the NCM UI
    logger.info('starting login test')
    devices_page = netcloud_ui_fixture.start().open().login(user)

    # Open the accounts page directly as served up by accounts
    users_page = nctf.accts_pages.UsersPage(driver=devices_page.driver, tenant_id=account.tenant_id).open()
    add_user_page = users_page.toolbar.add_dropdown.click().user_button.click()
    users_page = add_user_page.cancel_button.click()
    add_collaborator_page = users_page.toolbar.add_dropdown.click().collaborator_button.click()
    users_page = add_collaborator_page.cancel_button.click()
    users_table = users_page.account_users_table

    alert_headers = ['Role', 'Enabled', 'Name', 'First Name', 'Last Name', 'Email', 'Last Login']
    validate_table(users_table, alert_headers, True)

    ct = users_table.cached_table

    assert len(ct) > 1

    ct[1].ui_element.click()

    # Now validate those same things via the NCM UI
    account_and_users_page = nctf.ncm_pages.AccountsAndUsersPage(driver=devices_page.driver).open()
    users_panel = account_and_users_page.account_users_panel
    users_table = users_panel.account_users_table
    assert users_table
    add_user_page = users_panel.toolbar.add_dropdown.click().user_button.click()
    users_page = add_user_page.cancel_button.click()
    add_collaborator_page = users_page.toolbar.add_dropdown.click().collaborator_button.click()
    users_page = add_collaborator_page.cancel_button.click()
    ct = users_page.account_users_table.cached_table
    ct[1].ui_element.click()

    ncp_permissions_page = nctf.ncp_pages.NCPPermissionsPage(driver=devices_page.driver, tenant_id=account.tenant_id).open()

    ncp_table = ncp_permissions_page.ncp_permissions_table
    ncp_permissions_headers = ['Role', 'User', 'Email', 'Networks']
    validate_table(ncp_table, ncp_permissions_headers, True)

    # Test ncp_table in context of the NCM page
    account_and_users_page = nctf.ncm_pages.AccountsAndUsersPage(driver=devices_page.driver).open()
    ncp_permissions_page = account_and_users_page.ncp_permissions_page_button.click()

    ncp_table = ncp_permissions_page.ncp_permissions_panel.ncp_permissions_table
    ncp_permissions_headers = ['Role', 'User', 'Email', 'Networks']
    validate_table(ncp_table, ncp_permissions_headers, True)


def test_schedule_table(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('Starting schedule table test')

    user = netcloud_account_fixture.create()
    page = netcloud_ui_fixture.start().open().login(user)
    schedules_page = page.side_nav_bar.scheduler_button.click().schedules_page_button.click()
    schedule_name = 'test darren'
    schedules_page.add_default_schedule(schedule_name)
    schedules_table = schedules_page.schedules_panel.schedules_table
    schedules_table_headers = ['Name', 'Description', 'Start On']
    validate_table(schedules_table, schedules_table_headers, rows_selectable=True)
    schedules_table.row_name_field(schedule_name).click()
    schedules_page.delete_schedule(schedule_name)
    assert schedules_page.schedules_panel.schedules_table.num_rows == 0


def test_verify_admin_can_add_users_with_all_roles(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                                                   netcloud_ui_fixture: nctf.NetCloudUIFixture) -> None:
    # Get NCM SSO User
    user = netcloud_account_fixture.get_predefined_account().admin

    # Log in through the NCM UI
    devices_page = netcloud_ui_fixture.start().open().login(user)

    accounts_users_page = devices_page.side_nav_bar.accounts_and_users_button.click()
    # Open iframe of Accounts Users tab
    # accounts_users_page = acct_pages.UsersPage(driver=devices_page.driver,
    #                                   tenant_id=netcloud_account_fixture.sf_account.sf_attributes['ID__c']).open()

    add_user_page = accounts_users_page.account_users_panel.toolbar.add_dropdown.click().user_button.click()

    # Create new user
    firstname = netcloud_account_fixture.email_fixture.get_random_firstname()
    lastname = netcloud_account_fixture.email_fixture.get_random_lastname()
    postfix = netcloud_account_fixture.email_fixture.get_random_postfix()
    inbox_name = netcloud_account_fixture.email_fixture.get_inbox_name(firstname, lastname, postfix)
    inbox = netcloud_account_fixture.email_fixture.get_inbox(inbox_name)
    email = inbox.address
    account_and_users_page = add_user_page.add_user(firstname, lastname, email, True)

    ncm_permissions_page = account_and_users_page.parent.ncm_permissions_page_button.click()

    # Select account
    ncm_account_row = ncm_permissions_page.permissions_panel.ncm_permissions_table.get_rows(
        "Name", user.netcloud_account.accserv.name)
    ncm_account_row[0].ui_element.click()

    # Add new user to account
    add_users_dialog = ncm_permissions_page.permissions_panel.toolbar.add_dropdown.click().user_button.click()
    assert isinstance(add_users_dialog, nctf.ncm_pages.NCMPermissionsAddUserDialog)
    add_users_dialog.another_ncm_account_radio_button.click()
    add_users_dialog.existing_users_radio_button.click()
    add_users_dialog.role_dropdown.click()
    add_user_table_row = add_users_dialog.add_ncm_user_table.get_rows('Email Address', email)
    add_user_table_row[0].cells['CheckBox'].ui_element.click()
    ncm_permissions_page = \
        add_users_dialog.role_dropdown.click().role_selector('Administrator').click().save_changes_button.click()

    accounts_users_page = ncm_permissions_page.account_users_page_button.click()
    # Select new user
    ncm_user_row = accounts_users_page.account_users_panel.account_users_table.get_rows('First Name', firstname)
    ncm_user_row[0].ui_element.click()

    # Edit user role
    edit_page = accounts_users_page.account_users_panel.toolbar.edit_user_button.click()
    edit_page.role_dropdown.click().user_administrator_button.click()
    edit_page.role_dropdown.click().role_selector('User')
    users_page = edit_page.role_dropdown.click().role_selector('User Administrator').save_button.click()

    # Assert new role
    ncm_user_row = users_page.account_users_table.get_rows('First Name', firstname)
    assert ncm_user_row[0].cells['Role'].text == 'User Administrator'


def test_router_apps_table(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture,
                           netcloud_rest_fixture: nctf.NetCloudRESTFixture):

    admin = netcloud_account_fixture.get_predefined_account().admin
    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    tools_page = devices_page.side_nav_bar.tools_button.click()
    tools_page = tools_page.opt_in()
    # the apps grid is empty

    tools_page = tools_page.purge_apps_with_retry()

    # I upload a valid app
    app_bundle_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'testfiles', "valid", 'RouterSDKDemo.tar.gz'))
    with open(app_bundle_path, 'rb') as app_bundle:
        netcloud_api = netcloud_rest_fixture.get_client()
        netcloud_api.authenticate_actor(admin)
        netcloud_api.ncm.sdk_versions._post(files={'archive': app_bundle})

    apps_table = tools_page.wait_for_apps_to_load()

    apps_headers = ['CheckBox', 'Name', 'Description', 'Version', 'Date Uploaded', 'Installed On Group', 'Status']
    validate_table(apps_table, apps_headers, True)
    rows = apps_table.get_rows('Name', 'RouterSDKDemo')
    assert len(rows) == 1
    demo_app = rows[0]
    assert demo_app.is_selected()
    assert demo_app.cells['Description'].text == 'Router SDK Demo Application'
    pass


def test_router_and_picker_dialog_tables(netcloud_account_fixture: nctf.NetCloudAccountFixture, arm_fixture: nctf.ARMInterface,
                                         netcloud_ui_fixture: nctf.NetCloudUIFixture, config_fixture: nctf.ConfigFixture):
    """This is a 'happy path' SDK journey. It uploads an app, installs the app
    on a physical device, waits for the device to start the app and validates
    the custom app-generated alert.

    """
    logger.info('Starting devices routers table and devices picker dialog table test')

    admin = netcloud_account_fixture.create()
    assert admin.password == "Password1!"  # Default password for all new accounts.
    router = None
    try:
        router = arm_fixture.get_router(desired_product='3200v')
    except nctf.ARMException as arm_exception:
        pytest.skip(msg=str(arm_exception))

    # Register our router to NCM
    router.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_username=admin.username,
        ecm_password=admin.password,
        ecm_use_sso=True)

    router.rename_router_to_ecm_client_id()
    router.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router.ecm_data()['state']).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    router_table = devices_page.routers_panel.router_table

    router_headers = [
        'CheckBox', 'Online Status', 'Router Status', 'Individually Configured', 'Name', 'Description', 'Asset Identifier',
        'Product', 'MAC Address', 'IP Address', 'Group', 'NetCloud OS', 'Configuration', 'NCOS Status', 'Account'
    ]

    validate_table(router_table, router_headers, True)
    headers = router_table.headers
    header = headers['Name']
    assert 'Name' == header.get_text()
    ct = router_table.cached_table

    # Test filtering the cached_table using filter
    mrows = filter(lambda r: r.cells['Product'].text == '3200v', ct)
    for row in mrows:
        assert row.cells['Product'].text == '3200v'

    # Test filtering the cached_table using list comprehensions
    for row in [row for row in ct if row.cells['Product'] == '3200v']:
        assert row.cells['Product'].text == '3200v'

    # Test using get_rows
    for row in router_table.get_rows('Product', '3200v'):
        client_key = row.cells['MAC Address'].text.lower()
        logger.info('client key=' + client_key)

    tools_page = devices_page.side_nav_bar.tools_button.click()
    tools_page = tools_page.opt_in()
    router_picker_dialog = tools_page.router_apps_region.dev_mode_routers_add_button.click()

    # Maximize the dialog to get all of the columns visible
    router_picker_dialog.maximize_button.click()
    devices_table = router_picker_dialog.devices_table
    devices_table_headers = [
        'CheckBox', 'Online Status', 'Router Status', 'Individually Configured', 'Name', 'Description', 'Asset Identifier',
        'Product', 'MAC Address', 'IP Address', 'Group', 'NetCloud OS', 'Configuration', 'NCOS Status', 'Account'
    ]
    validate_table(devices_table, devices_table_headers, True)


def test_remote_device_tables(netcloud_account_fixture: nctf.NetCloudAccountFixture, arm_fixture: nctf.ARMInterface,
                              netcloud_ui_fixture: nctf.NetCloudUIFixture, config_fixture: nctf.ConfigFixture):
    logger.info('Starting remote UI on router tables test')

    admin = netcloud_account_fixture.create()

    router = None
    try:
        router = arm_fixture.get_router(desired_product='3200v')
    except nctf.ARMException as arm_exception:
        pytest.skip(msg=str(arm_exception))

    # Register our router to NCM
    router.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_username=admin.username,
        ecm_password=admin.password,
        ecm_use_sso=True)

    router.rename_router_to_ecm_client_id()
    router.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router.ecm_data()['state']).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    router_table = devices_page.routers_panel.router_table
    router_table.get_rows()[0].select()
    dialog = devices_page.routers_panel.toolbar.configuration_dropdown.click().edit_configuration_button.click()
    side_help = dialog.config_body.side_help
    side_help.collapse_right_button.click()

    # networking_menu = dialog.config_body.side_nav_bar.networking_menu.click()
    # local_ip_dialog = networking_menu.local_networks.click().local_ip_networks.click()
    pass


def test_accounts_users_ncmpermissions_tree(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                                            netcloud_ui_fixture: nctf.NetCloudUIFixture) -> None:

    # Get NCM SSO User
    account = netcloud_account_fixture.get_predefined_account()
    user = account.admin
    new_user = account.create_user(applications=['ecm', 'nce'])
    # Log in through the NCM UI
    devices_page = netcloud_ui_fixture.start().open().login(user)

    accounts_users_page = devices_page.side_nav_bar.accounts_and_users_button.click()

    ncm_permissions_page = accounts_users_page.ncm_permissions_page_button.click()
    table = ncm_permissions_page.permissions_panel.ncm_permissions_table
    table = table.expand_all()
    find_row = table.get_rows("First Name", "Darren")
    assert find_row
    table.get_rows()[1].select()

    cached_tree = table.cached_tree.row.collapse()
    assert cached_tree
    cached_tree = table.expand_all().cached_tree
    assert cached_tree
    cached_tree = table.cached_tree.row.collapse()
    assert cached_tree
    cached_tree = table.expand_all().cached_tree
    assert cached_tree

    # move_dialog = ncm_permissions_page.permissions_panel.toolbar.move_button.click()

    # select_group_dialog = ncm_permissions_page.permissions_panel.toolbar.move_button.click()
    # assert select_group_dialog


def test_table_headers(netcloud_account_fixture: nctf.NetCloudAccountFixture, netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('Starting schedule table test')

    user = netcloud_account_fixture.create()
    page = netcloud_ui_fixture.start().open().login(user)
    table = page.routers_panel.router_table

    assert table


def test_account_group_tree(tlogger, netcloud_account_fixture: nctf.NetCloudAccountFixture,
                            netcloud_ui_fixture: nctf.NetCloudUIFixture, arm_fixture: nctf.ARMInterface,
                            config_fixture: nctf.ConfigFixture, netcloud_rest_fixture: nctf.NetCloudRESTFixture):
    """
    The admin opts out with a router in dev mode and an app installed
    """

    admin = netcloud_account_fixture.create()
    assert admin.password == "Password1!"  # Default password for all new accounts.
    router = None
    try:
        router = arm_fixture.get_router()
    except nctf.ARMException as arm_exception:
        # Skip this test if we are unable to lease a router
        pytest.skip(msg=str(arm_exception))

    # Register our router to NCM
    router.register_with_ecm(
        ecm_host=config_fixture.target.services.ncm.hostname,
        ecm_accounts_host=config_fixture.target.services.accounts.hostname,
        ecm_stream_host=config_fixture.target.services.stream.hostname,
        ecm_username=admin.username,
        ecm_password=admin.password,
        ecm_use_sso=True)

    router.rename_router_to_ecm_client_id()
    router.wait_for_config_sync_in_ecm()
    nctf.wait(120, 1).for_call(lambda: router.ecm_data()['state']).to_equal('online')

    login_page = netcloud_ui_fixture.start().open()
    devices_page = login_page.login(admin)

    # Click the add button in Dev Mode Routers grid
    tools_page = devices_page.side_nav_bar.tools_button.click()
    tools_page.opt_in()

    # Select the router, put it into dev mode
    router_picker = tools_page.router_apps_region.dev_mode_routers_add_button.click()
    router_picker.devices_table.device_selection_checkbox(device_id=router.ecm_client_id).click()
    confirmation_dialog = router_picker.save_button.click()
    tools_page = confirmation_dialog.yes_button.click()

    # after this we do a reboot, but we need to wait for the ecm service to restart - ECM-15555
    # Reboot the router to apply Developer Mode license
    router.reboot()
    router.wait_for_reachable()

    table = tools_page.router_apps_region.dev_mode_router_table
    router_name_from_table = table.get_rows()[0].cells['Name'].text
    assert router_name_from_table == router.ecm_client_id, \
        "The device [{}] was not found in the Developer Mode Routers table.".format(router.ecm_client_id)

    # Add group
    group_name = f"foo_group_{Faker().pystr(min_chars=6, max_chars=8)}"
    groups_page = tools_page.side_nav_bar.groups_button.click()
    groups_page = groups_page.add_group(group_name=group_name, product_name=router.product, firmware_name=router.fw_version)

    # Attempt to add router to group
    devices_page = groups_page.side_nav_bar.devices_button.click()

    if devices_page.toggle_tree.is_displayed():
        devices_page.toggle_tree_button.click()
    routers_table = devices_page.routers_panel.router_table
    routers_table.get_rows('Name', router_name_from_table)[0].cells['CheckBox'].ui_element.click()
    select_group_dialog = devices_page.routers_panel.toolbar.move_button.click()
    tree_picker = select_group_dialog.account_group_tree.expand_all()
    tree_picker.get_rows('Name', regex=group_name)[0].select()
    device_page = select_group_dialog.ok_button.click()
    # I get an error that routers in dev mode cannot be put into groups.
    assert devices_page
