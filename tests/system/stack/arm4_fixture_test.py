import os

from nctf import nctf_typing


def test_arm4_fixture_collect_triage_logs(arm4_fixture: nctf_typing.ARMv4Fixture):
    """Validate we can collect triage logs after a test fails by calling the arm4 fixture
    full stack test, leases routers, uses actual routers to obtain logs."""

    # 3 variations in workflow for collecting triage (wan_no_client, wan_with_client, lan_only_client)
    cloud_router = arm4_fixture.lease_router(product_name="3200v")
    wan_router, _ = arm4_fixture.lease_simple_network(capabilities={"default_lan_only": False})
    lan_router, _ = arm4_fixture.lease_simple_network(capabilities={"default_lan_only": True})

    temp = "/tmp/triage_test"
    os.makedirs(temp, exist_ok=True)
    for file in os.listdir(temp):
        os.unlink(path=os.path.join(temp, file))

    arm4_fixture.collect_triage_logs(filename_prefix="test_arm4_fixture_collect_triage_logs",
                                     directory=temp)

    files_collected = os.listdir(temp)
    assert len(files_collected) == 3

    # cleanup *after* we validated they were created ok.
    for file in os.listdir(temp):
        os.unlink(path=os.path.join(temp, file))
    os.rmdir(temp)
