import logging

from nctf import nctf_typing as nctf
from nctf.libs.common_library import random_string
from nctf.test_lib.base.rest.exceptions import UnexpectedStatusError
import pytest

logger = logging.getLogger('netcloud_ui_test')


def validate_actor(actor):
    assert actor.accserv.username is not None
    assert actor.accserv.password == "Password1!"
    assert actor.user_state == nctf.NetCloudActor.UserState.AUTHENTICATED
    assert actor.accserv.tenant_id == actor.netcloud_account.tenant_id


def validate_account(account: nctf.NetCloudAccount):
    assert account.accserv.name is not None
    assert account.accserv.contact == account.admin.accserv.id
    assert account.accserv.enhanced_login_security_enabled is False
    assert account.accserv.federated_id == "Cradlepoint"
    assert account.accserv.identity_provider_cert is None
    assert account.accserv.mfa_required is False
    assert account.accserv.session_length == 900
    assert account.tenant_id is not None
    validate_actor(account.admin)


def validate_predefined_account(account: nctf.NetCloudAccount):
    assert account.accserv.name == "Journey"
    assert account.accserv.contact == account.admin.accserv.id
    assert account.accserv.enhanced_login_security_enabled is False
    assert account.accserv.federated_id == "Cradlepoint"
    assert account.accserv.identity_provider_cert is None
    assert account.accserv.mfa_required is False
    assert account.accserv.session_length == 7200
    assert account.tenant_id is not None
    assert account.admin.username is not None
    assert account.admin.password == "Quality805!"
    assert account.admin.user_state == nctf.NetCloudActor.UserState.AUTHENTICATED
    assert account.admin.accserv.tenant_id == account.admin.netcloud_account.tenant_id


def test_netcloud_get_predefined_account(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud get predefined account')
    account = netcloud_account_fixture.get_predefined_account('sso')
    validate_predefined_account(account)


def test_netcloud_get_predefined_account_by_creds(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud get predefined account by admin creds')
    account = netcloud_account_fixture.get_account_by_admin_creds(username='cp.jrny@gmail.com', password='Quality805!')
    validate_predefined_account(account)


def test_netcloud_create_random_account(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud create random account')
    account = netcloud_account_fixture.create_account()
    validate_account(account)
    new_account_instance = netcloud_account_fixture.get_account_by_admin_creds(account.admin.username, account.admin.password)
    validate_account(new_account_instance)
    assert account.accserv.name == new_account_instance.accserv.name
    assert account.tenant_id == new_account_instance.tenant_id
    assert account.admin.username == new_account_instance.admin.username
    assert account.admin.accserv.id == new_account_instance.admin.accserv.id


def test_netcloud_create_account_with_name(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud create account with name')
    name = random_string(20, 'Account_', False)
    account = netcloud_account_fixture.create_account(name=name)
    validate_account(account)
    assert account.accserv.name == name


def test_netcloud_create_account_with_admin_states(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud create account with admin states')
    account = netcloud_account_fixture.create_account()
    user = account.create_user(activation_method=False, authenticate=False)
    assert user.user_state == nctf.NetCloudActor.UserState.CREATED
    user.activate(authenticate=False)
    assert user.user_state == nctf.NetCloudActor.UserState.ACTIVATED
    user.authenticate()
    assert user.user_state == nctf.NetCloudActor.UserState.AUTHENTICATED


def test_netcloud_create_account_with_all_params(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud create account with all parameters')
    name = random_string(20, 'Account_', False)
    firstname = netcloud_account_fixture.email_fixture.get_random_firstname()
    lastname = netcloud_account_fixture.email_fixture.get_random_lastname()
    inbox_name = netcloud_account_fixture.email_fixture.get_inbox_name(firstname, lastname)
    inbox = netcloud_account_fixture.email_fixture.get_inbox(inbox_name)
    email = inbox.address

    account = netcloud_account_fixture.create_account(
        name=name,
        activate=nctf.ActivationMethod.API,
        accept_tos=nctf.AcceptTOSMethod.API,
        admin_username=email,
        admin_first_name=firstname,
        admin_last_name=lastname,
        admin_password="Password1!",
        entitlements=["ECM_PRIME_1YR"])
    validate_account(account)
    assert account.admin.accserv.first_name == firstname
    assert account.admin.accserv.last_name == lastname
    assert account.admin.accserv.username == email
    assert account.admin.username == email


def test_account_properties(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud account getting and setting properties')
    name = random_string(20, 'Account_', False)
    account = netcloud_account_fixture.create_account(name=name)
    new_contact = account.create_user(password="Password1!", role=nctf.Roles.ROOT_ADMIN)
    account.accserv.contact = new_contact.accserv.id
    new_session_len = account.accserv.session_length + 13
    account.accserv.session_length = new_session_len
    account.accserv.enhanced_login_security_enabled = True
    account.accserv.federated_id = "new federated id"
    # account.identity_provider_cert = don't know the format of this
    account.accserv.mfa_required = True

    assert account.accserv.name == name
    assert account.accserv.contact == new_contact.accserv.id
    assert account.accserv.session_length == new_session_len
    assert account.accserv.enhanced_login_security_enabled is True
    # federated_id should be "new federated id" based on the ok status return on the put above, but it didn't change
    assert account.accserv.federated_id == "Cradlepoint"
    assert account.accserv.identity_provider_cert is None
    assert account.accserv.mfa_required is True
    assert account.accserv.tenant_id is not None
    account.accserv.mfa_required = False
    account.enhanced_login_security_enabled = False


def test_account_crfd_user(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud account create read find delete user')
    account = netcloud_account_fixture.create_account()
    first_name = random_string(20, 'First_', False)
    last_name = random_string(20, 'Last_', False)
    inbox_name = "{}_{}".format(first_name, last_name)
    inbox = netcloud_account_fixture.email_fixture.get_inbox(inbox_name)
    username = inbox.address
    user = account.create_user(
        username=username,
        first_name=first_name,
        last_name=last_name,
        password="Password1!",
        role=nctf.Roles.USER,
        applications=['nce'],
        is_active=True,
        mfa_enabled=False,
        is_locked=False,
        is_federated=False)
    validate_actor(user)
    assert user.accserv.username == username
    assert user.accserv.first_name == first_name
    assert user.accserv.last_name == last_name
    assert user.accserv.applications == ['ecm']  # apparently application values are ignored, always ecm
    # Find the user in the account
    find_user = account.get_user_by_login(user.username)
    find_user.authenticate("Password1!")
    assert find_user.accserv.id == user.accserv.id
    assert find_user.username == user.username
    assert find_user.accserv.first_name == user.accserv.first_name
    assert find_user.accserv.last_name == user.accserv.last_name
    assert find_user.password == user.password
    assert find_user.accserv.role == user.accserv.role
    assert find_user.accserv.applications == user.accserv.applications
    assert find_user.accserv.login == user.accserv.login
    # Delete the user in the account
    account.delete_user(user)
    find_user = account.get_user_by_login(username)
    assert find_user is None
    all_users = account.get_users()
    assert len(all_users) == 1
    assert all_users[0].authenticate("Password1!").accserv.id == account.admin.accserv.id


def test_user_change_password(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud user change password')
    account = netcloud_account_fixture.create_account()
    user = account.create_user()
    username = user.username
    validate_actor(user)
    user.change_password("Password1!", "NewPassword1!")
    user.authenticate()
    assert user.accserv.get_accounts_user_record() is not None
    # Find the user in the account
    find_user = account.get_user_by_login(username)
    find_user.authenticate("NewPassword1!")
    assert find_user.accserv.id == user.accserv.id


def test_user_update(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting netcloud user update properties')
    account = netcloud_account_fixture.create_account()
    user = account.create_user(role=nctf.Roles.ADMIN)
    username = user.username
    validate_actor(user)
    with pytest.raises(UnexpectedStatusError) as e:
        user.accserv.role = nctf.Roles.USER
    assert "Status code: 403 (Forbidden)" in e.value.args[0]
    user.accserv.first_name = "newfirstname"
    # Find the user in the account
    find_user = account.get_user_by_login(username)
    find_user.authenticate("Password1!")
    assert find_user.accserv.id == user.accserv.id
    assert find_user.accserv.role == nctf.Roles.ADMIN
    assert find_user.accserv.first_name == "newfirstname"


def test_user_update_record(netcloud_account_fixture: nctf.NetCloudAccountFixture,
                            netcloud_ui_fixture: nctf.NetCloudUIFixture):
    logger.info('starting netcloud user update properties')
    account = netcloud_account_fixture.create_account()
    user = account.create_user(role=nctf.Roles.ADMIN)

    home_page = netcloud_ui_fixture.start().open().login(account.admin)
    assert home_page

    firstname = "updated_{}".format(user.accserv.first_name)
    lastname = "updated_{}".format(user.accserv.last_name)

    user.accserv.update_accounts_user_record(first_name=firstname, last_name=lastname)

    validate_actor(user)
    assert user.accserv.first_name == firstname
    assert user.accserv.last_name == lastname

    accounts_users_page = home_page.side_nav_bar.accounts_and_users_button.click()
    assert accounts_users_page


def test_create_user_in_created_account(netcloud_account_fixture: nctf.NetCloudAccountFixture):
    logger.info('starting create user in created account')
    account = netcloud_account_fixture.create_account(entitlements=["ECM_PRIME_1YR", "NETCLOUD_BRANCH_ROUTERS_1YR"])
    user = account.create_user(role=nctf.Roles.ADMIN)
    user.accept_tos()
    username = user.username
    validate_actor(user)
    find_user = account.get_user_by_login(username)
    find_user.authenticate("Password1!")
