# import logging
# import pprint
#
# import pytest
#
# from journey_framework.test_lib.portal import pages
# from journey_framework.test_lib.portal.fixtures import PartnerPortalUIFixture
#
# logger = logging.getLogger('partner_portal_ui_tests')
#
#
# class Users():
#     """Portal Actor
#         username is the portal user name
#         password is the portal password
#         displayname is the Profile display name
#         role must be {exec, admin, buyer, company, user
#         co_name is the SFDC name for the account
#         acc_id is the SFDC account ID
#         con_id is the contact ID
#         (self, username: str, password: str, displayname: str, acc_id: str, con_id: str, userdata: PortalUserData)
#     """
#     thutchinson = pages.PortalActor(
#         username="thutchinson@dhm2m.com.staging",
#         password="987654321",
#         displayname="Tom Hutchinson",
#         account_identifier='0015000000MsQDd',
#         contactId='00350000028d9R4',
#         userdata=pages.PortalUserData(),
#         accountdata=pages.PortalAccountData())
#
#
# def test_partner_portal_profile_dropdown_navigation(partner_portal_ui_fixture: PartnerPortalUIFixture):
#     """
#     This tests the Home Screen Profile Navigation
#     It passes if:
#         The user can log into the Portal
#         The Profile dropdown exists
#         All the expect menu item can be selected
#         Selecting a menu item displays the correct page
#         The logged-in user name is correct
#         No extra menu items are found
#     """
#     pp = pprint.PrettyPrinter(indent=4)
#     # Login to Portal
#     current_user = Users.thutchinson
#     # Login method returns an object of type PortalPage.
#     # PortalPage is the base class modeling the Portal Home Page
#     login_page = partner_portal_ui_fixture.start()
#     login_page.open()
#     assert login_page.wait_for_on_this()
#     home_page = login_page.login(portal_user_with_valid_creds=current_user)
#
#     # Test Header -> Profile Dropdown MenuItems and verify the correct page loaded
#     home_page.portal_header.profile_dropdown.wait_for_region_to_load()
#     found_user_name = home_page.portal_header.profile_dropdown.logged_in_user_name
#     assert current_user.display_name == found_user_name, "Expected loged in user to be:" + current_user.display_name + " Found:" + found_user_name
#
#     # The nav_to method returns a specific page type,
#     # that page type uses the URL_TEMPLATE attribute to make the correct url for the expected page
#     profile_page = home_page.portal_header.profile_dropdown.menu_item_profile.click().menu_item_my_profile.click()
#     # As the url for this page type is unique, the "is_on_this" test will be valid
#     assert profile_page.is_on_this(), (
#         'After pressing the "Profile" -> "My Profile" menu item, we expect to see the MyProflie Page displayed')
#
#     company_page = profile_page.portal_header.profile_dropdown.menu_item_profile.click().menu_item_my_company.click()
#     assert company_page.is_on_this(), (
#         'After pressing the "Profile"->"MyCompany" menu item, we expect to see the MyCompany Page displayed')
#
#     support_page = company_page.portal_header.profile_dropdown.menu_item_profile.click().menu_item_contact_support.click()
#     assert support_page.is_on_this(), (
#         'After pressing the "Profile"->"ContactSupport" menu item, we expect to see ContactSupportPage displayed')
#
#     # Because each page object is derived from PortalPage, this pattern is possible
#     home_page = support_page.portal_header.profile_dropdown.menu_item_profile.click().menu_item_home.click()
#     assert home_page.is_on_this(), (
#         'After pressing the "Profile" -> "Home" menu item, we expect to see the Home Page displayed')
#
#     items = home_page.portal_header.profile_dropdown.get_menu_items_text
#     assert len(items) == 5, (
#         "List size mis-match, expected 5 items but found:" + str(len(items)) + " Found items:" + pp.pformat(items))
#
#     logout_page = home_page.portal_header.profile_dropdown.menu_item_profile.click().menu_item_logout.click()
#     assert logout_page.is_on_this(), (
#         'After pressing the "Profile" -> "Logout" menu item, we expect to see the Cradlepoint Home Page displayed')
