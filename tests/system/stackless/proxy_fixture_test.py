from nctf.test_lib.core.proxy import BrowserMobProxyFixture


def test_proxy_fixture_starts_dead(proxy: BrowserMobProxyFixture) -> None:
    assert proxy.is_running is False


def test_proxy_fixture_can_start(proxy: BrowserMobProxyFixture) -> None:
    proxy.start()
