def test_import_nctf_typing():
    from nctf import nctf_typing

    # fixtures
    from nctf.test_lib.accounts.rest.fixture import AccServRESTFixture
    from nctf.test_lib.core.arm2.interface import ARMInterface
    from nctf.test_lib.core.arm4 import ARMv4Fixture
    from nctf.test_lib.core.config.fixture import ConfigFixture
    from nctf.test_lib.core.email.fixture import EmailFixture
    from nctf.test_lib.ncos.fixtures.firmware_loader.v1.fixture import FirmwareLoaderFixture
    from nctf.test_lib.ncos.fixtures.router_api.fixture import RouterRESTAPIFixture
    from nctf.test_lib.ncos.fixtures.wifi_client.fixture import WiFiClientFixture
    from nctf.test_lib.ncm.fixtures.stream_client.fixture import StreamClientFixture
    from nctf.test_lib.netcloud.fixtures.netcloud_account.fixture import NetCloudAccountFixture
    from nctf.test_lib.netcloud.fixtures.netcloud_ui.fixture import NetCloudUIFixture
    from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTFixture
    from nctf.test_lib.portal.fixtures.fixture import PartnerPortalUIFixture
    from nctf.test_lib.salesforce.fixture import _SalesforceConnection

    assert nctf_typing.AccServRESTFixture is AccServRESTFixture
    assert nctf_typing.ARMInterface is ARMInterface
    assert nctf_typing.ARMv4Fixture is ARMv4Fixture
    assert nctf_typing.ConfigFixture is ConfigFixture
    assert nctf_typing.EmailFixture is EmailFixture
    assert nctf_typing.FirmwareLoaderFixture is FirmwareLoaderFixture
    assert nctf_typing.RouterRESTAPIFixture is RouterRESTAPIFixture
    assert nctf_typing.WiFiClientFixture is WiFiClientFixture
    assert nctf_typing.StreamClientFixture is StreamClientFixture
    assert nctf_typing.NetCloudAccountFixture is NetCloudAccountFixture
    assert nctf_typing.NetCloudUIFixture is NetCloudUIFixture
    assert nctf_typing.NetCloudRESTFixture is NetCloudRESTFixture
    assert nctf_typing.PartnerPortalUIFixture is PartnerPortalUIFixture
    assert nctf_typing._SalesforceConnection is _SalesforceConnection

    # meta fixtures
    from nctf.test_lib.meta.fixtures import WifiMetaFixture

    assert nctf_typing.WifiMetaFixture is WifiMetaFixture

    # useful classes
    from nctf.test_lib.netcloud.fixtures.netcloud_account.actor import NetCloudActor

    assert nctf_typing.NetCloudActor is NetCloudActor
