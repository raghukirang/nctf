from nctf.test_lib.core.config import ConfigFixture

"""config_fixture"""


def test_set_run_conf_override(config_fixture: ConfigFixture):
    assert config_fixture.run.fixtures.arm.port == 123


def test_set_target_conf_override(config_fixture: ConfigFixture):
    assert config_fixture.target.services.ncm.protocol == "http"
