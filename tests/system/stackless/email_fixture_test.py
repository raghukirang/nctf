def test_email_fixture(email_fixture):
    assert email_fixture is not None


def test_email_fixture_get_inbox(email_fixture):
    inbox = email_fixture.get_inbox()
    assert inbox is not None
