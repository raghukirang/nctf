from nctf.test_lib.core.arm4 import ARMv4Fixture


def test_arm4_fixture(arm4_fixture: ARMv4Fixture):
    assert arm4_fixture is not None


def test_arm4_fixture_default_pool_id(arm4_fixture: ARMv4Fixture):
    """Assert the run configuration value 'defaultPoolId' made it to the arm4_client"""
    assert arm4_fixture.arm_client.routers.default_pool_id is not None
