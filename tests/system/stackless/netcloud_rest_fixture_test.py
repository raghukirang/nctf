from nctf.test_lib.accounts.rest import AccServRESTClient
from nctf.test_lib.netcloud.fixtures.rest.fixture import NetCloudRESTClient


def test_netcloud_rest_fixture(netcloud_rest_fixture):
    assert netcloud_rest_fixture is not None
    client = netcloud_rest_fixture.get_client()
    assert isinstance(client, NetCloudRESTClient)


def test_accserv_rest_fixture(accserv_rest_fixture):
    assert accserv_rest_fixture is not None
    client = accserv_rest_fixture.get_client()
    assert isinstance(client, AccServRESTClient)
