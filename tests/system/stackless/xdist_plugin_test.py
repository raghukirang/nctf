import os


def test_xdist_plugin_logging(tlogger):
    tlogger.info("This is just a test if xdist works!")

    path_to_artifacts = os.path.join(os.path.dirname(__file__), "artifacts")
    master_log = os.path.join(path_to_artifacts, "nctf-master.log")
    gw0_log = os.path.join(path_to_artifacts, "nctf-gw0.log")
    gw1_log = os.path.join(path_to_artifacts, "nctf-gw1.log")

    assert os.path.isfile(master_log)
    assert os.path.isfile(gw0_log)
    assert os.path.isfile(gw1_log)
