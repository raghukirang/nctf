from nctf import nctf_typing


def test_wifi_meta_fixture(wifi_meta_fixture: nctf_typing.WifiMetaFixture):
    assert isinstance(wifi_meta_fixture, nctf_typing.WifiMetaFixture)
