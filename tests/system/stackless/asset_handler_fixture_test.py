def test_asset_handler_fixture_find_in_project_dir(asset_handler_fixture):
    from io import TextIOWrapper
    f = asset_handler_fixture.get_asset_file("test_project_assets_dir.json")
    assert type(f) is TextIOWrapper


def test_asset_handler_fixture_find_in_test_dir(asset_handler_fixture):
    from io import TextIOWrapper
    f = asset_handler_fixture.get_asset_file("test_asset_test_dir.json")
    assert type(f) is TextIOWrapper


def test_asset_handler_fixture_get_json(asset_handler_fixture):
    j = asset_handler_fixture.get_asset_json("test_asset_test_dir.json")
    assert type(j) is dict
    assert j['foo'] == 'bar'


def test_asset_handler_fixture_get_file_path(asset_handler_fixture):
    p = asset_handler_fixture.get_asset_file_path("test_asset_test_dir.json")
    assert type(p) is str
    assert "/" in p  # can't be a full path if it doesn't have a slash in it.  Don't want just a filename.
