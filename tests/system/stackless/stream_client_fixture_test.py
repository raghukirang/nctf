from nctf.test_lib.ncm.fixtures.stream_client import StreamClientFixture


def test_stream_client_fixture(stream_client_fixture):
    assert isinstance(stream_client_fixture, StreamClientFixture)
