from nctf.test_lib.bsf.fixtures.bsf_object import BSFObject


# Need to hack the BSFObject otherwise code will execute that relies on BSF.
def mock_init(*args, **kwags):
    return


BSFObject.__init__ = mock_init


def test_bsf_entitlement_fixture(bsf_entitlement_fixture):
    assert bsf_entitlement_fixture is not None


def test_bsf_license_fixture(bsf_license_fixture):
    assert bsf_license_fixture is not None


def test_bsf_order_fixture(bsf_order_fixture):
    assert bsf_order_fixture is not None


def test_bsf_product_subscription_fixture(bsf_product_subscription_fixture):
    assert bsf_product_subscription_fixture is not None


def test_bsf_sfconnector_fixture(bsf_sfconnector_fixture):
    assert bsf_sfconnector_fixture is not None
