# Virtnetwork System Tests
## Warning
This tests must be run as root, which is why they are isolated from the rest of the tests.
They will create root-owned artifacts.
## Limitations
These tests can only be run on a system that supports nested virtualization. See: https://cradlepoint.atlassian.net/wiki/spaces/IT/pages/294650169/Nested+Virtualization+Limitations