import json
from nctf import nctf_typing


def test_virtnetwork_fixture(virtconsole_fixture: nctf_typing.VirtRouterConsole, virtnetwork_fixture, asset_handler_fixture):

    network_def = asset_handler_fixture.get_asset_json("test_net.json")

    network = virtnetwork_fixture.setup(network_def, asset_handler_fixture)

    virtconsole_handler = virtconsole_fixture.get_virtconsole_handler(coi=network.networking, rtr='rtr1')

    return_value, output = virtconsole_handler.get_data("config/lan/0")
    output_json = json.loads(output)
    assert return_value is True
    assert '_id_' in output_json
