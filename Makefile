SHELL := /bin/bash
ROOT=$(shell pwd)
PYTHON = python3

VENV ?= local_venv
WHEELS_VENV ?= wheels_venv

PKG_REQS := $(ROOT)/config/requirements.txt
DEV_REQS := $(ROOT)/config/requirements-dev.txt

default: build
all: packages

.PHONY: hooks

get-firmware:
	./get_firmware_files.sh ~/mnt/guido


### PACKAGES ###

packages: $(VENV)
$(VENV): $(PKG_REQS)
ifdef VIRTUAL_ENV
	$(error Deactivate your virtualenv before proceeding (type "deactivate"))
endif
	$(PYTHON) -m venv $@
	@echo "Making packages from PyPi..."
	. $(VENV)/bin/activate && pip install -U pip==9.0.1 wheel==0.30.0
	. $(VENV)/bin/activate && pip install -Ur $(PKG_REQS) -Ur $(DEV_REQS)

packages-clean:
	rm -rf $(VENV)

docker-wheels:
	docker-compose run make wheels


### DEV ###

dev: hooks

hooks:
	@chmod +x $(ROOT)/dev/hooks/pre-commit
	@ln -sf $(ROOT)/dev/hooks/pre-commit .git/hooks/pre-commit

### STYLE ###
style: isort yapf

STAGED_PY_FILES = $(shell find . -name '*.py' -print0 | xargs -0 git diff --cached --name-only --diff-filter=ACM -z | xargs -0)

isort:
ifneq ($(STAGED_PY_FILES),)
	@echo "isorting $(STAGED_PY_FILES)"
	@python3 -m isort --apply $(STAGED_PY_FILES)
else
	@echo "No STAGED_PY_FILES found. Skipping isort..."
endif

yapf:
ifneq ($(STAGED_PY_FILES),)
	@echo "yapfing $(STAGED_PY_FILES)"
	@python3 -m yapf --parallel --in-place $(STAGED_PY_FILES)
else
	@echo "No STAGED_PY_FILES found. Skipping yapf..."
endif


### LINT ###

docker-lint:
	@echo "Not yet implemented..."

lint: lint-clean flake8

flake8: lint-clean
	@python3 -m flake8 --exit-zero --config=$(ROOT)/setup.cfg $(ROOT)/nctf; \


### TEST ###

test: coverage-clean test-clean packages test-unit test-integration test-system

test-unit:
	( \
		. $(ROOT)/$(VENV)/bin/activate; \
		py.test -n2 --cov-config .coveragerc --cov-report html:cov_html --cov-report xml --cov=nctf \
		--junit-xml=$(ROOT)/unit-test-results.xml $(ROOT)/tests/unit; \
	)

test-integration: test-services
	( \
		. $(ROOT)/$(VENV)/bin/activate; \
		py.test -n2 -v --artifacts=$(ROOT)/tests/integration/artifacts \
		--junit-xml=$(ROOT)/integration-test-results.xml $(ROOT)/tests/integration; \
	)

test-services: test-services-clean
	@docker-compose run --service-ports -d --name mailhog mailhog
	@docker-compose run --service-ports -d --name sample_site sample_site

test-system: test-system-packages test-system-stackless

test-system-packages:
	$(PYTHON) -m venv test_$(VENV) \
	&& . $(ROOT)/test_$(VENV)/bin/activate \
	&& pip install pip==9.0.1 wheel==0.30.0 \
	&& pip install --process-dependency-links .

test-system-stackless:
	. $(ROOT)/test_$(VENV)/bin/activate \
	&& export ECM_ROOT_USERNAME=test_root_username \
	&& export GUIDO_PATH=test_guido_path \
	&& cd tests/system/stackless \
	&& py.test -n2 -v \
	--arm-api-key=test1234 \
	--run-conf=test_run_conf \
	--target-conf=$(ROOT)/tests/system/stackless/test_target_conf.yaml \
	--set-conf run.fixtures.arm.port=123 --set-conf target.services.ncm.protocol=http \
	--assets=$(ROOT)/tests/system/stackless/project_assets --junit-xml=$(ROOT)/stackless-system-test-results.xml \
	--artifacts=$(ROOT)/tests/system/stackless/artifacts -n2 \
	&& pip uninstall -y nctf

# not partner_portal_ui_test and not test_qa3

test-system-stack:	test-system-packages
	. $(ROOT)/test_$(VENV)/bin/activate \
	&& cd tests/system/stack \
	&& py.test -v -n4 -k "not partner_portal_ui_test and not test_qa3" \
	--arm-api-key=test1234 \
	--run-conf=test_run_conf \
	--target-conf=test_target_conf \
	--junit-xml=$(ROOT)/stack-system-test-results.xml \
	--artifacts=$(ROOT)/tests/system/stack/artifacts \
	 -s --log-sys-out \
	&& pip uninstall -y nctf

test-system-virtnetwork:
	. $(ROOT)/test_$(VENV)/bin/activate \
	&& cd tests/system/virtnetwork \
	&& py.test -v \
	--run-conf=test_run_conf \
	--target-conf=test_target_conf \
	--junit-xml=$(ROOT)/virtnetwork-system-test-results.xml \
	--artifacts=$(ROOT)/tests/system/virtnetwork/artifacts

#### CLEAN ####

clean: python-clean lint-clean coverage-clean test-clean cache-clean test-services-clean

test-services-clean:
	@docker rm -f mailhog || true
	@docker rm -f sample_site || true

python-clean:
	@find . -name "*.pyc" -or -name "*.pyo" | xargs rm -f

lint-clean:
	@rm flake8-results.log || true

coverage-clean:
	@rm .coverage || true
	@rm coverage.xml || true

test-clean:
	@rm unit-test-results.xml || true
	@rm integration-test-results.xml || true
	@rm *-system-test-results.xml || true
	@rm -rf test_$(VENV) || true

cache-clean:
	@find $(ROOT) -name ".pytest_cache" -type d -exec rm -r "{}" \; || true
	@find $(ROOT) -name ".cache" -type d -exec rm -r "{}" \; || true
	@find $(ROOT) -name "__pycache__" -type d -exec rm -r "{}" \; || true

docs-clean:
	@rm -fr docs/source
	@rm -fr docs/_build

### DOCKER UTILS ###

docker-purge-containers:
	docker rm -fv $$(docker ps -a -q) || echo "No containers found to purge"

docker-purge-images:
	docker rmi -f $$(docker images -q) || echo "No images found to purge"

docker-purge-everything: docker-purge-containers docker-purge-images


#### BUILD ####

build: packages

# If you are on a mac and would like to develop, do a build-mac to set up your development vs make build since env-setup
# only runs on docker and ubuntu
build-mac: packages
	. $(ROOT)/$(VENV)/bin/activate && pip install --process-dependency-links .

docker-build:
	docker-compose build
	docker-compose down
	docker-compose run build

docs: docs-clean packages
	( \
        . $(ROOT)/$(VENV)/bin/activate; \
        pip uninstall -y nctf || echo "skip" && pip install --process-dependency-links . ; \
		sphinx-apidoc -f -M -e -o $(ROOT)/docs/source $(ROOT)/nctf; \
		$(MAKE) -C $(ROOT)/docs/ clean html; \
	)

### VIRTNETWORK UTILS ###

# Run this to reclaim all files owned by root when the virnetwork_fixture is used.
chown-this:
	sudo chown -R $$USER:$$USER $(ROOT)
