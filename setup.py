# yapf: disable
import os
from sys import platform

from setuptools import find_packages
from setuptools import setup
from setuptools.command.install import install

from __version__ import version


class InstallCommand(install):
    """Extension of the InstallCommand so we can add the --no-pytest option

    Note: This stuff is horribly documented. Used these as a reference:
        https://stackoverflow.com/questions/18725137/how-to-obtain-arguments-passed-to-setup-py-from-pip-with-install-option
        https://stackoverflow.com/questions/677577/distutils-how-to-pass-a-user-defined-parameter-to-setup-py

        Also, this will not work when installing from the CLI due to https://github.com/pypa/pip/issues/1883

    """
    description = "Handles whether to install pytest entry points or not."
    user_options = install.user_options + [
        ('no-pytest', None, "Disable installation of pytest-isms.")
    ]

    def initialize_options(self):
        install.initialize_options(self)
        self.no_pytest = 0

    def finalize_options(self):
        print(f"Value of --no-pytest is {self.no_pytest}")
        install.finalize_options(self)

    def run(self):
        if self.no_pytest:
            self.distribution.entry_points = {}
        install.run(self)


# Get the absolute path of the requirements file and parse the requirements.
requirements_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'config', 'requirements.txt'))
install_requires = []
dependency_links = []
with open(requirements_path) as fp:
    reqs = fp.readlines()
for req in reqs:
    if 'git+' in req:
        req = req.split('-e ')[1]
        pgk = req.split('#')[1]
        dependency_links.append(req.strip())
        my_pkg = pgk.replace('egg=', '')
        install_requires.append(my_pkg.replace('-', '==').strip())
    else:
        install_requires.append(req.strip())

entry_points={
        'pytest11': [
            'proxy_fixture = nctf.pytest_lib.core.proxy.fixture',
            'base_ui_fixture = nctf.pytest_lib.base.fixtures.base_ui',
            'netcloud_ui_fixture = nctf.pytest_lib.netcloud.fixtures.netcloud_ui',
            'partner_portal_ui_fixture = nctf.pytest_lib.partner_portal.fixtures.partner_portal_ui',
            'fw_loader = nctf.pytest_lib.ncos.fixtures.fw_loader',
            'router_rest = nctf.pytest_lib.ncos.fixtures.router_rest',
            'wifi_client = nctf.pytest_lib.ncos.fixtures.wifi_client',
            'salesforce_fixture = nctf.pytest_lib.salesforce.fixture',
            'pertino_salesforce_fixture = nctf.pytest_lib.pertino_salesforce.fixture',
            'bsf_fixtures = nctf.pytest_lib.bsf.fixtures',
            'asset_handler_fixture = nctf.pytest_lib.core.asset_handler.fixture',
            'stream_client_fixture = nctf.pytest_lib.ncm.fixtures.stream_client.fixture',
            'tlogger = nctf.pytest_lib.core.tlogger.fixture',
            'core_plugins = nctf.pytest_lib.core.plugins.plugins',
            'testrail_xml_plugin = nctf.pytest_lib.core.testrail.xml',
            'arm_fixture = nctf.pytest_lib.core.arm2.fixture',
            'arm2_redo_fixture = nctf.pytest_lib.core.arm2_redo.fixture',
            'arm4_fixture = nctf.pytest_lib.core.arm4.fixture',
            'config_fixture = nctf.pytest_lib.core.config.fixture',
            'email_fixture = nctf.pytest_lib.core.email.fixture',
            'netcloud_account_fixture = nctf.pytest_lib.netcloud.fixtures.netcloud_account',
            'netcloud_rest_fixture = nctf.pytest_lib.netcloud.fixtures.rest',
            'accserv_rest_fixture = nctf.pytest_lib.accounts.fixtures.rest',
            'wifi_meta_fixture = nctf.pytest_lib.meta.fixtures.wifi',
            'netcloud_router_fixture = nctf.pytest_lib.netcloud.fixtures.netcloud_router',
        ]
    }

# virtnetwork is only compatible with Linux (due to KVM+QEMU). Windows and macOS should not install this code.
if platform == "linux":
    entry_points["pytest11"].append('virtnetwork = nctf.pytest_lib.ncos.fixtures.virtnetwork',)

setup(
    cmdclass={
      'install': InstallCommand,
    },
    name='nctf',
    version=version,  # Managed by automation
    description='NetCloud Test Framework',
    author='Austin Beatty, Ashley Gilbert, Darren Binder, Gabe Carnell, Matthew Norman, Roland Demeter',
    author_email='mnorman@cradlepoint.com',
    url='https://engops-prod-gitlab-v1.private.aws.cradlepointecm.com/Infrastructure/nctf',
    include_package_data=True,
    package_dir={'nctf': 'nctf'},
    packages=find_packages(),
    package_data={
        'nctf.assets': ['*.json'],
        'nctf.test_lib.libs': ['router_capabilities.json'],
        'nctf.test_lib.core.config.legacy': ['*.json'],
        'nctf.pytest_lib.core.tlogger': ['default.json'],
        'nctf.test_lib.ncos.fixtures.virtnetwork': ['*'],
        'nctf.test_lib.ncos.fixtures.firmware_loader': ['get_firmware_files.sh']
    },
    entry_points=entry_points,
    install_requires=install_requires,
    dependency_links=dependency_links,
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ])
